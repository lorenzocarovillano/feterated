/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.ActNotPol1Dao;
import com.federatedinsurance.crs.commons.data.dao.Sysdummy1Dao;
import com.federatedinsurance.crs.copy.SqlcaTs030199;
import com.federatedinsurance.crs.ws.DfhcommareaXz0g0001;
import com.federatedinsurance.crs.ws.Xz0g0001Data;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0G0001<br>
 * <pre>AUTHOR.       Alan Witikko.
 * DATE-WRITTEN. 03 JUN 2007.
 * ***************************************************************
 *   PROGRAM TITLE - BATCH COBOL FRAMEWORK PROXY PROGRAM FOR     *
 *                     UOW : GetCancellationPendingNoticeCnt     *
 *                     OPERATIONS : GetPCNCountByAccount         *
 *                                                               *
 *   PLATFORM - HOST CICS - CALLED FROM A BATCH PROGRAM          *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED FROM A COBOL BATCH PROGRAM      *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         INPUT AND OUTPUT ARE SENT TO MAIN     *
 *                         PROXY PROGRAM THROUGH MEMORY AND      *
 *                         DFHCOMMAREA (PROXY-PROGRAM-COMMON)    *
 *                                                               *
 *   NOTE - THIS PROGRAM IS A TEMPORARY MODULE. IT WILL          *
 *          EVENTUALLY BE REPLACED BY A TRUE COBOL FRAMEWORK     *
 *          UNIT OF WORK. FOR NOW THOUGH, IT WILL BE A SIMPLE    *
 *          SUBPROGRAM.                                          *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    07JUL05   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #     DATE     EMP ID              DESCRIPTION          *
 *  --------  --------- -------   -------------------------------*
 *  TO07614   03JUN08   E404ASW   INITIAL PROGRAM                *
 *  TO07614   11NOV08   E404GRK   ADDED ACT_NOT_TYP_CD = 'IMP' TO*
 *                                THE COUNT CRITERIA             *
 *  TO07614   18NOV08   E404GRK   ADDED IMP_CNC_TYP_CD <> 'E' TO *
 *                                THE COUNT CRITERIA             *
 * TO07602-18 17NOV09   E404ABL   DEFAULT RC TO 0                *
 *  PP03495   04SEP12   E404RAH   CHANGED TO CHECK SQLCODE INSTEAD
 *                                OF SQLSTATE WHEN USING SQL TO DO
 *                                DATE ARITHMETIC.               *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0g0001 extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private SqlcaTs030199 sqlca = new SqlcaTs030199();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ExecContext execContext = null;
	private Sysdummy1Dao sysdummy1Dao = new Sysdummy1Dao(dbAccessStatus);
	private ActNotPol1Dao actNotPol1Dao = new ActNotPol1Dao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Xz0g0001Data ws = new Xz0g0001Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXz0g0001 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXz0g0001 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		programExit();
		return 0;
	}

	public static Xz0g0001 getInstance() {
		return (Programs.getInstance(Xz0g0001.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-GET-PCN-CNT
		//              THRU 3000-EXIT.
		getPcnCnt();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  VERIFY THE INPUT SENT CONTAINS AN ACCOUNT NUMBER.            *
	 * ***************************************************************
	 *  RETURN CODE NEEDS TO BE DEFAULTED TO GOOD FOR IVORY</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: MOVE CF-RC-GOOD-RETURN      TO XZT01O-EO-ERROR-CD
		dfhcommarea.setXzt01oEoErrorCdFormatted(ws.getConstantFields().getRcGoodReturnFormatted());
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
		//***************************************************************
		// RETRIEVE DATES TO BE USED FOR THE SQL.                       *
		//***************************************************************
		// COB_CODE: PERFORM 2200-RETRIEVE-DATES
		//              THRU 2200-EXIT.
		retrieveDates();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>**********************************************************
	 *   VERIFY WE HAVE AN ACCOUNT NUMBER TO LOOK UP            *
	 * **********************************************************</pre>*/
	private void verifyInput() {
		// COB_CODE: IF (XZT01I-SI-ACT-NBR = SPACES)
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getXzt01iSiActNbr())) {
			// COB_CODE: MOVE CF-PROGRAM-NAME    TO EA-03-PROGRAM-NAME
			ws.getEa03MissingInput().setProgramName(ws.getConstantFields().getProgramName());
			// COB_CODE: MOVE CF-PN-2100         TO EA-03-PARAGRAPH-NBR
			ws.getEa03MissingInput().setParagraphNbr(ws.getConstantFields().getPn2100());
			// COB_CODE: MOVE EA-03-MISSING-INPUT
			//                                   TO XZT01O-EO-ERROR-MSG
			dfhcommarea.setXzt01oEoErrorMsg(ws.getEa03MissingInput().getEa03MissingInputFormatted());
			// COB_CODE: MOVE CF-RC-MISSING-ACT  TO XZT01O-EO-ERROR-CD
			dfhcommarea.setXzt01oEoErrorCdFormatted(ws.getConstantFields().getRcMissingActFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
	}

	/**Original name: 2200-RETRIEVE-DATES<br>
	 * <pre>**********************************************************
	 *   RUN SQL TO GET CURRENT DATE AND DATE MINUS 12 MONTHS   *
	 * **********************************************************</pre>*/
	private void retrieveDates() {
		// COB_CODE: EXEC SQL
		//               SELECT CURRENT DATE,
		//                      CURRENT DATE - 12 MONTHS
		//                 INTO :SA-CURRENT-DT,
		//                      :SA-ONE-YEAR-AGO-DT
		//                 FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		sysdummy1Dao.selectRec1(ws);
		// COB_CODE: IF SQLCODE NOT = +0
		//                  THRU 9910-EXIT
		//           END-IF.
		if (sqlca.getSqlcode() != 0) {
			// COB_CODE: MOVE CF-PN-2200         TO SA-PARAGRAPH-NBR
			ws.setSaParagraphNbr(ws.getConstantFields().getPn2200());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3000-GET-PCN-CNT<br>*/
	private void getPcnCnt() {
		// COB_CODE: PERFORM 3100-SET-INPUT
		//              THRU 3100-EXIT.
		setInput();
		// COB_CODE: PERFORM 3200-GET-CNT
		//              THRU 3200-EXIT.
		getCnt();
		// COB_CODE: PERFORM 3300-SET-OUTPUT
		//              THRU 3300-EXIT.
		setOutput();
	}

	/**Original name: 3100-SET-INPUT<br>
	 * <pre>******************************************************
	 *  SET UP INPUT VARIABLES                              *
	 * ******************************************************</pre>*/
	private void setInput() {
		// COB_CODE: MOVE XZT01I-SI-ACT-NBR      TO CSR-ACT-NBR OF DCLACT-NOT.
		ws.getDclactNot().setCsrActNbr(dfhcommarea.getXzt01iSiActNbr());
	}

	/**Original name: 3200-GET-CNT<br>
	 * <pre>**********************************************************
	 *   GET THE COUNT FROM THE TABLE USING ACT NBR, DATES AND  *
	 *   STATUS OF '70' (ACTIVE).                               *
	 * **********************************************************</pre>*/
	private void getCnt() {
		// COB_CODE: EXEC SQL
		//              SELECT COUNT (A.CSR_ACT_NBR)
		//                INTO :SA-PCN-CNT
		//                FROM ACT_NOT A
		//               WHERE A.CSR_ACT_NBR = :DCLACT-NOT.CSR-ACT-NBR
		//                 AND A.NOT_DT <= :SA-CURRENT-DT
		//                 AND A.NOT_DT >= :SA-ONE-YEAR-AGO-DT
		//                 AND A.ACT_NOT_STA_CD = '70'
		//                 AND A.ACT_NOT_TYP_CD = 'IMP'
		//                 AND NOT EXISTS (SELECT '1'
		//                                   FROM ACT_NOT_POL B
		//                                  WHERE B.CSR_ACT_NBR = A.CSR_ACT_NBR
		//                                    AND B.NOT_PRC_TS = A.NOT_PRC_TS
		//                                    AND B.POL_BIL_STA_CD = 'E')
		//                 AND NOT EXISTS (SELECT '1'
		//                                   FROM ACT_NOT_POL C
		//                                  WHERE C.CSR_ACT_NBR = A.CSR_ACT_NBR
		//                                    AND C.NOT_PRC_TS = A.NOT_PRC_TS
		//                                    AND C.POL_BIL_STA_CD = 'N'
		//                                    AND EXISTS
		//                                   (SELECT '1'
		//                                      FROM ACT_NOT_POL D
		//                                     WHERE D.CSR_ACT_NBR = C.CSR_ACT_NBR
		//                                       AND D.POL_NBR = C.POL_NBR
		//                                       AND D.NOT_PRC_TS <> C.NOT_PRC_TS
		//                                       AND D.NOT_EFF_DT = C.NOT_EFF_DT))
		//           END-EXEC.
		ws.setSaPcnCnt(actNotPol1Dao.selectRec1(ws.getDclactNot().getCsrActNbr(), ws.getSaCurrentDt(), ws.getSaOneYearAgoDt(), ws.getSaPcnCnt()));
		// COB_CODE: IF SQLSTATE NOT = CF-SC-GOOD-RETURN
		//                  THRU 9910-EXIT
		//           END-IF.
		if (!Conditions.eq(sqlca.getSqlstate(), ws.getConstantFields().getScGoodReturn())) {
			// COB_CODE: MOVE CF-PN-3200         TO SA-PARAGRAPH-NBR
			ws.setSaParagraphNbr(ws.getConstantFields().getPn3200());
			// COB_CODE: PERFORM 9910-SELECT-ERROR
			//              THRU 9910-EXIT
			selectError();
		}
	}

	/**Original name: 3300-SET-OUTPUT<br>
	 * <pre>******************************************************
	 *  SET THE OUTPUT TO BE RETURNED                       *
	 * ******************************************************</pre>*/
	private void setOutput() {
		// COB_CODE: MOVE SA-PCN-CNT             TO XZT01O-PC-PCN-CNT.
		dfhcommarea.setXzt01oPcPcnCnt(TruncAbs.toShort(ws.getSaPcnCnt(), 3));
		// COB_CODE: MOVE XZT01I-SI-ACT-NBR      TO XZT01O-PC-ACT-NBR.
		dfhcommarea.setXzt01oPcActNbr(dfhcommarea.getXzt01iSiActNbr());
	}

	/**Original name: 9910-SELECT-ERROR<br>
	 * <pre>******************************************************
	 *  SET UP ERROR ON THE SELECT                          *
	 * ******************************************************</pre>*/
	private void selectError() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO EA-05-PROGRAM-NAME.
		ws.getEa05Db2ErrorOnSelect().setProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: MOVE SA-PARAGRAPH-NBR       TO EA-05-PARAGRAPH-NBR.
		ws.getEa05Db2ErrorOnSelect().setParagraphNbr(ws.getSaParagraphNbr());
		// COB_CODE: MOVE SQLCODE                TO EA-05-SQLCODE.
		ws.getEa05Db2ErrorOnSelect().setSqlcode(sqlca.getSqlcode());
		// COB_CODE: MOVE SQLSTATE               TO EA-05-SQLSTATE.
		ws.getEa05Db2ErrorOnSelect().setSqlstate(sqlca.getSqlstate());
		// COB_CODE: MOVE CF-RC-DB2-ERROR        TO XZT01O-EO-ERROR-CD.
		dfhcommarea.setXzt01oEoErrorCdFormatted(ws.getConstantFields().getRcDb2ErrorFormatted());
		// COB_CODE: MOVE EA-05-DB2-ERROR-ON-SELECT
		//                                       TO XZT01O-EO-ERROR-MSG.
		dfhcommarea.setXzt01oEoErrorMsg(ws.getEa05Db2ErrorOnSelect().getEa05Db2ErrorOnSelectFormatted());
		// COB_CODE: GO TO 1000-PROGRAM-EXIT.
		programExit();
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
