/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: CI-FUNCTION<br>
 * Variable: CI-FUNCTION from copybook TS54701<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class CiFunction {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char OPEN_PIPE = '1';
	public static final char CALL_CICS_PGM_NO_SYNC = '2';
	public static final char CALL_CICS_PGM_SYNC = '3';
	public static final char CLOSE_PIPE = '4';
	public static final char GET_TOKEN_INF = '5';

	//==== METHODS ====
	public void setCiFunction(char ciFunction) {
		this.value = ciFunction;
	}

	public char getCiFunction() {
		return this.value;
	}

	public void setOpenPipe() {
		value = OPEN_PIPE;
	}

	public void setCallCicsPgmSync() {
		value = CALL_CICS_PGM_SYNC;
	}

	public void setClosePipe() {
		value = CLOSE_PIPE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CI_FUNCTION = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
