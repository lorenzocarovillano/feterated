/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.collection.LazyArrayCopy;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Xzc05iStaCdLis;
import com.federatedinsurance.crs.ws.occurs.Xzc05oCerInf;

/**Original name: XZC050C1<br>
 * Variable: XZC050C1 from copybook XZC050C1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc050c1 {

	//==== PROPERTIES ====
	public static final int I_STA_CD_LIS_MAXOCCURS = 2;
	public static final int O_CER_INF_MAXOCCURS = 2500;
	//Original name: XZC05I-ACT-NBR
	private String iActNbr = DefaultValues.stringVal(Len.I_ACT_NBR);
	//Original name: XZC05I-NOT-PRC-TS
	private String iNotPrcTs = DefaultValues.stringVal(Len.I_NOT_PRC_TS);
	//Original name: XZC05I-STA-CD-LIS
	private Xzc05iStaCdLis[] iStaCdLis = new Xzc05iStaCdLis[I_STA_CD_LIS_MAXOCCURS];
	//Original name: FILLER-XZC050-PROGRAM-INPUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: XZC05O-CER-INF
	private LazyArrayCopy<Xzc05oCerInf> oCerInf = new LazyArrayCopy<>(new Xzc05oCerInf(), 1, O_CER_INF_MAXOCCURS);
	//Original name: XZC05O-ERROR-CODE
	private Xz08coRetCd oErrorCode = new Xz08coRetCd();
	//Original name: XZC05O-ERROR-MESSAGE
	private String oErrorMessage = DefaultValues.stringVal(Len.O_ERROR_MESSAGE);
	//Original name: FILLER-XZC050-PROGRAM-OUTPUT
	private String flr2 = DefaultValues.stringVal(Len.FLR2);

	//==== CONSTRUCTORS ====
	public Xzc050c1() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int iStaCdLisIdx = 1; iStaCdLisIdx <= I_STA_CD_LIS_MAXOCCURS; iStaCdLisIdx++) {
			iStaCdLis[iStaCdLisIdx - 1] = new Xzc05iStaCdLis();
		}
	}

	public void setXzc050ProgramInputBytes(byte[] buffer, int offset) {
		int position = offset;
		iActNbr = MarshalByte.readString(buffer, position, Len.I_ACT_NBR);
		position += Len.I_ACT_NBR;
		iNotPrcTs = MarshalByte.readString(buffer, position, Len.I_NOT_PRC_TS);
		position += Len.I_NOT_PRC_TS;
		for (int idx = 1; idx <= I_STA_CD_LIS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				iStaCdLis[idx - 1].setiStaCdLisBytes(buffer, position);
				position += Xzc05iStaCdLis.Len.I_STA_CD_LIS;
			} else {
				iStaCdLis[idx - 1].initiStaCdLisSpaces();
				position += Xzc05iStaCdLis.Len.I_STA_CD_LIS;
			}
		}
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzc050ProgramInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iActNbr, Len.I_ACT_NBR);
		position += Len.I_ACT_NBR;
		MarshalByte.writeString(buffer, position, iNotPrcTs, Len.I_NOT_PRC_TS);
		position += Len.I_NOT_PRC_TS;
		for (int idx = 1; idx <= I_STA_CD_LIS_MAXOCCURS; idx++) {
			iStaCdLis[idx - 1].getiStaCdLisBytes(buffer, position);
			position += Xzc05iStaCdLis.Len.I_STA_CD_LIS;
		}
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiActNbr(String iActNbr) {
		this.iActNbr = Functions.subString(iActNbr, Len.I_ACT_NBR);
	}

	public String getiActNbr() {
		return this.iActNbr;
	}

	public String getiActNbrFormatted() {
		return Functions.padBlanks(getiActNbr(), Len.I_ACT_NBR);
	}

	public void setiNotPrcTs(String iNotPrcTs) {
		this.iNotPrcTs = Functions.subString(iNotPrcTs, Len.I_NOT_PRC_TS);
	}

	public String getiNotPrcTs() {
		return this.iNotPrcTs;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setXzc050ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		setoServiceOutputBytes(buffer, position);
		position += Len.O_SERVICE_OUTPUT;
		oErrorCode.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		oErrorMessage = MarshalByte.readString(buffer, position, Len.O_ERROR_MESSAGE);
		position += Len.O_ERROR_MESSAGE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR2);
	}

	public byte[] getXzc050ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		getoServiceOutputBytes(buffer, position);
		position += Len.O_SERVICE_OUTPUT;
		MarshalByte.writeShort(buffer, position, oErrorCode.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		MarshalByte.writeString(buffer, position, oErrorMessage, Len.O_ERROR_MESSAGE);
		position += Len.O_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		return buffer;
	}

	public void setoServiceOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= O_CER_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				oCerInf.get(idx - 1).setoCerInfBytes(buffer, position);
				position += Xzc05oCerInf.Len.O_CER_INF;
			} else {
				Xzc05oCerInf temp_oCerInf = new Xzc05oCerInf();
				temp_oCerInf.initoCerInfSpaces();
				getoCerInfObj().fill(temp_oCerInf);
				position += Xzc05oCerInf.Len.O_CER_INF * (O_CER_INF_MAXOCCURS - idx + 1);
				break;
			}
		}
	}

	public byte[] getoServiceOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= O_CER_INF_MAXOCCURS; idx++) {
			oCerInf.get(idx - 1).getoCerInfBytes(buffer, position);
			position += Xzc05oCerInf.Len.O_CER_INF;
		}
		return buffer;
	}

	public void setoErrorMessage(String oErrorMessage) {
		this.oErrorMessage = Functions.subString(oErrorMessage, Len.O_ERROR_MESSAGE);
	}

	public String getoErrorMessage() {
		return this.oErrorMessage;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR2);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public Xzc05iStaCdLis getiStaCdLis(int idx) {
		return iStaCdLis[idx - 1];
	}

	public Xzc05oCerInf getoCerInf(int idx) {
		return oCerInf.get(idx - 1);
	}

	public LazyArrayCopy<Xzc05oCerInf> getoCerInfObj() {
		return oCerInf;
	}

	public Xz08coRetCd getoErrorCode() {
		return oErrorCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int I_ACT_NBR = 9;
		public static final int I_NOT_PRC_TS = 26;
		public static final int FLR1 = 100;
		public static final int O_ERROR_MESSAGE = 500;
		public static final int FLR2 = 500;
		public static final int XZC050_PROGRAM_INPUT = I_ACT_NBR + I_NOT_PRC_TS + Xzc050c1.I_STA_CD_LIS_MAXOCCURS * Xzc05iStaCdLis.Len.I_STA_CD_LIS
				+ FLR1;
		public static final int O_SERVICE_OUTPUT = Xzc050c1.O_CER_INF_MAXOCCURS * Xzc05oCerInf.Len.O_CER_INF;
		public static final int XZC050_PROGRAM_OUTPUT = O_SERVICE_OUTPUT + Xz08coRetCd.Len.XZ08CO_RET_CD + O_ERROR_MESSAGE + FLR2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
