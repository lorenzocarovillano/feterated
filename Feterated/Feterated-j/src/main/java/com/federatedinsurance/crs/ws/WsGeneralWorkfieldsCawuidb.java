/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsBuildingStatusSw;

/**Original name: WS-GENERAL-WORKFIELDS<br>
 * Variable: WS-GENERAL-WORKFIELDS from program CAWUIDB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsGeneralWorkfieldsCawuidb {

	//==== PROPERTIES ====
	//Original name: WS-BUILDING-STATUS-SW
	private WsBuildingStatusSw buildingStatusSw = new WsBuildingStatusSw();
	//Original name: WS-DATA-DISP
	private String dataDisp = DefaultValues.stringVal(Len.DATA_DISP);
	//Original name: WS-COUNT
	private int count = DefaultValues.BIN_INT_VAL;
	//Original name: WS-DATA-TABLE
	private String dataTable = DefaultValues.stringVal(Len.DATA_TABLE);
	//Original name: WS-TEMP-GEN-ID
	private String tempGenId = DefaultValues.stringVal(Len.TEMP_GEN_ID);
	//Original name: WS-BUILDING-ATTEMPTS
	private short buildingAttempts = DefaultValues.SHORT_VAL;
	//Original name: WS-MAX-ATTEMPTS
	private short maxAttempts = ((short) 3);
	//Original name: WS-GENERATED-ID-LEN
	private short generatedIdLen = DefaultValues.SHORT_VAL;
	//Original name: WS-USERID-CHAR
	private short useridChar = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-GENERATED-ID-CHAR
	private short generatedIdChar = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setDataDisp(long dataDisp) {
		this.dataDisp = PicFormatter.display("-Z(8)9").format(dataDisp).toString();
	}

	public long getDataDisp() {
		return PicParser.display("-Z(8)9").parseLong(this.dataDisp);
	}

	public String getDataDispFormatted() {
		return this.dataDisp;
	}

	public String getDataDispAsString() {
		return getDataDispFormatted();
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCount() {
		return this.count;
	}

	public void setDataTable(String dataTable) {
		this.dataTable = Functions.subString(dataTable, Len.DATA_TABLE);
	}

	public String getDataTable() {
		return this.dataTable;
	}

	public String getDataTableFormatted() {
		return Functions.padBlanks(getDataTable(), Len.DATA_TABLE);
	}

	public void setTempGenId(String tempGenId) {
		this.tempGenId = Functions.subString(tempGenId, Len.TEMP_GEN_ID);
	}

	public String getTempGenId() {
		return this.tempGenId;
	}

	public void setBuildingAttempts(short buildingAttempts) {
		this.buildingAttempts = buildingAttempts;
	}

	public short getBuildingAttempts() {
		return this.buildingAttempts;
	}

	public short getMaxAttempts() {
		return this.maxAttempts;
	}

	public void setGeneratedIdLen(short generatedIdLen) {
		this.generatedIdLen = generatedIdLen;
	}

	public short getGeneratedIdLen() {
		return this.generatedIdLen;
	}

	public void setUseridChar(short useridChar) {
		this.useridChar = useridChar;
	}

	public short getUseridChar() {
		return this.useridChar;
	}

	public void setGeneratedIdChar(short generatedIdChar) {
		this.generatedIdChar = generatedIdChar;
	}

	public short getGeneratedIdChar() {
		return this.generatedIdChar;
	}

	public WsBuildingStatusSw getBuildingStatusSw() {
		return buildingStatusSw;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATA_DISP = 10;
		public static final int DATA_TABLE = 18;
		public static final int TEMP_GEN_ID = 32;
		public static final int GENERATED_ID_LEN = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
