/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalErrTranSupV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_ERR_TRAN_SUP_V]
 * 
 */
public class HalErrTranSupVDao extends BaseSqlDao<IHalErrTranSupV> {

	private final IRowMapper<IHalErrTranSupV> selectRecRm = buildNamedRowMapper(IHalErrTranSupV.class, "ptyNbr", "txt");

	public HalErrTranSupVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalErrTranSupV> getToClass() {
		return IHalErrTranSupV.class;
	}

	public IHalErrTranSupV selectRec(String appNm, String hetsFailAcyCd, String hetsErrAtnCd, String hetsTypErr, IHalErrTranSupV iHalErrTranSupV) {
		return buildQuery("selectRec").bind("appNm", appNm).bind("hetsFailAcyCd", hetsFailAcyCd).bind("hetsErrAtnCd", hetsErrAtnCd)
				.bind("hetsTypErr", hetsTypErr).rowMapper(selectRecRm).singleResult(iHalErrTranSupV);
	}

	public IHalErrTranSupV selectRec1(String appNm, String hetsFailAcyCd, IHalErrTranSupV iHalErrTranSupV) {
		return buildQuery("selectRec1").bind("appNm", appNm).bind("hetsFailAcyCd", hetsFailAcyCd).rowMapper(selectRecRm)
				.singleResult(iHalErrTranSupV);
	}
}
