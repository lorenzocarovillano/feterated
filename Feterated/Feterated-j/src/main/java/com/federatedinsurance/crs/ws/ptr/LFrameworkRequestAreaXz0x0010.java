/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0010<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0010 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0010() {
	}

	public LFrameworkRequestAreaXz0x0010(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc005ActNotWrdRowFormatted(String data) {
		writeString(Pos.XZC005Q_ACT_NOT_WRD_ROW, data, Len.XZC005Q_ACT_NOT_WRD_ROW);
	}

	public String getXzc005ActNotWrdRowFormatted() {
		return readFixedString(Pos.XZC005Q_ACT_NOT_WRD_ROW, Len.XZC005Q_ACT_NOT_WRD_ROW);
	}

	public void setXzc005qActNotWrdCsumFormatted(String xzc005qActNotWrdCsum) {
		writeString(Pos.XZC005Q_ACT_NOT_WRD_CSUM, Trunc.toUnsignedNumeric(xzc005qActNotWrdCsum, Len.XZC005Q_ACT_NOT_WRD_CSUM),
				Len.XZC005Q_ACT_NOT_WRD_CSUM);
	}

	/**Original name: XZC005Q-ACT-NOT-WRD-CSUM<br>*/
	public int getXzc005qActNotWrdCsum() {
		return readNumDispUnsignedInt(Pos.XZC005Q_ACT_NOT_WRD_CSUM, Len.XZC005Q_ACT_NOT_WRD_CSUM);
	}

	public String getXzc005rActNotWrdCsumFormatted() {
		return readFixedString(Pos.XZC005Q_ACT_NOT_WRD_CSUM, Len.XZC005Q_ACT_NOT_WRD_CSUM);
	}

	public void setXzc005qCsrActNbrKcre(String xzc005qCsrActNbrKcre) {
		writeString(Pos.XZC005Q_CSR_ACT_NBR_KCRE, xzc005qCsrActNbrKcre, Len.XZC005Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC005Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc005qCsrActNbrKcre() {
		return readString(Pos.XZC005Q_CSR_ACT_NBR_KCRE, Len.XZC005Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc005qNotPrcTsKcre(String xzc005qNotPrcTsKcre) {
		writeString(Pos.XZC005Q_NOT_PRC_TS_KCRE, xzc005qNotPrcTsKcre, Len.XZC005Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC005Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc005qNotPrcTsKcre() {
		return readString(Pos.XZC005Q_NOT_PRC_TS_KCRE, Len.XZC005Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc005qPolNbrKcre(String xzc005qPolNbrKcre) {
		writeString(Pos.XZC005Q_POL_NBR_KCRE, xzc005qPolNbrKcre, Len.XZC005Q_POL_NBR_KCRE);
	}

	/**Original name: XZC005Q-POL-NBR-KCRE<br>*/
	public String getXzc005qPolNbrKcre() {
		return readString(Pos.XZC005Q_POL_NBR_KCRE, Len.XZC005Q_POL_NBR_KCRE);
	}

	public void setXzc005qStWrdSeqCdKcre(String xzc005qStWrdSeqCdKcre) {
		writeString(Pos.XZC005Q_ST_WRD_SEQ_CD_KCRE, xzc005qStWrdSeqCdKcre, Len.XZC005Q_ST_WRD_SEQ_CD_KCRE);
	}

	/**Original name: XZC005Q-ST-WRD-SEQ-CD-KCRE<br>*/
	public String getXzc005qStWrdSeqCdKcre() {
		return readString(Pos.XZC005Q_ST_WRD_SEQ_CD_KCRE, Len.XZC005Q_ST_WRD_SEQ_CD_KCRE);
	}

	public void setXzc005qTransProcessDt(String xzc005qTransProcessDt) {
		writeString(Pos.XZC005Q_TRANS_PROCESS_DT, xzc005qTransProcessDt, Len.XZC005Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC005Q-TRANS-PROCESS-DT<br>*/
	public String getXzc005qTransProcessDt() {
		return readString(Pos.XZC005Q_TRANS_PROCESS_DT, Len.XZC005Q_TRANS_PROCESS_DT);
	}

	public void setXzc005qCsrActNbr(String xzc005qCsrActNbr) {
		writeString(Pos.XZC005Q_CSR_ACT_NBR, xzc005qCsrActNbr, Len.XZC005Q_CSR_ACT_NBR);
	}

	/**Original name: XZC005Q-CSR-ACT-NBR<br>*/
	public String getXzc005qCsrActNbr() {
		return readString(Pos.XZC005Q_CSR_ACT_NBR, Len.XZC005Q_CSR_ACT_NBR);
	}

	public void setXzc005qNotPrcTs(String xzc005qNotPrcTs) {
		writeString(Pos.XZC005Q_NOT_PRC_TS, xzc005qNotPrcTs, Len.XZC005Q_NOT_PRC_TS);
	}

	/**Original name: XZC005Q-NOT-PRC-TS<br>*/
	public String getXzc005qNotPrcTs() {
		return readString(Pos.XZC005Q_NOT_PRC_TS, Len.XZC005Q_NOT_PRC_TS);
	}

	public void setXzc005qPolNbr(String xzc005qPolNbr) {
		writeString(Pos.XZC005Q_POL_NBR, xzc005qPolNbr, Len.XZC005Q_POL_NBR);
	}

	/**Original name: XZC005Q-POL-NBR<br>*/
	public String getXzc005qPolNbr() {
		return readString(Pos.XZC005Q_POL_NBR, Len.XZC005Q_POL_NBR);
	}

	public void setXzc005qStWrdSeqCd(String xzc005qStWrdSeqCd) {
		writeString(Pos.XZC005Q_ST_WRD_SEQ_CD, xzc005qStWrdSeqCd, Len.XZC005Q_ST_WRD_SEQ_CD);
	}

	/**Original name: XZC005Q-ST-WRD-SEQ-CD<br>*/
	public String getXzc005qStWrdSeqCd() {
		return readString(Pos.XZC005Q_ST_WRD_SEQ_CD, Len.XZC005Q_ST_WRD_SEQ_CD);
	}

	public void setXzc005qCsrActNbrCi(char xzc005qCsrActNbrCi) {
		writeChar(Pos.XZC005Q_CSR_ACT_NBR_CI, xzc005qCsrActNbrCi);
	}

	public void setXzc005qCsrActNbrCiFormatted(String xzc005qCsrActNbrCi) {
		setXzc005qCsrActNbrCi(Functions.charAt(xzc005qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC005Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc005qCsrActNbrCi() {
		return readChar(Pos.XZC005Q_CSR_ACT_NBR_CI);
	}

	public void setXzc005qNotPrcTsCi(char xzc005qNotPrcTsCi) {
		writeChar(Pos.XZC005Q_NOT_PRC_TS_CI, xzc005qNotPrcTsCi);
	}

	public void setXzc005qNotPrcTsCiFormatted(String xzc005qNotPrcTsCi) {
		setXzc005qNotPrcTsCi(Functions.charAt(xzc005qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC005Q-NOT-PRC-TS-CI<br>*/
	public char getXzc005qNotPrcTsCi() {
		return readChar(Pos.XZC005Q_NOT_PRC_TS_CI);
	}

	public void setXzc005qPolNbrCi(char xzc005qPolNbrCi) {
		writeChar(Pos.XZC005Q_POL_NBR_CI, xzc005qPolNbrCi);
	}

	public void setXzc005qPolNbrCiFormatted(String xzc005qPolNbrCi) {
		setXzc005qPolNbrCi(Functions.charAt(xzc005qPolNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC005Q-POL-NBR-CI<br>*/
	public char getXzc005qPolNbrCi() {
		return readChar(Pos.XZC005Q_POL_NBR_CI);
	}

	public void setXzc005qStWrdSeqCdCi(char xzc005qStWrdSeqCdCi) {
		writeChar(Pos.XZC005Q_ST_WRD_SEQ_CD_CI, xzc005qStWrdSeqCdCi);
	}

	public void setXzc005qStWrdSeqCdCiFormatted(String xzc005qStWrdSeqCdCi) {
		setXzc005qStWrdSeqCdCi(Functions.charAt(xzc005qStWrdSeqCdCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC005Q-ST-WRD-SEQ-CD-CI<br>*/
	public char getXzc005qStWrdSeqCdCi() {
		return readChar(Pos.XZC005Q_ST_WRD_SEQ_CD_CI);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0005 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC005Q_ACT_NOT_WRD_ROW = L_FW_REQ_XZ0C0005;
		public static final int XZC005Q_ACT_NOT_WRD_FIXED = XZC005Q_ACT_NOT_WRD_ROW;
		public static final int XZC005Q_ACT_NOT_WRD_CSUM = XZC005Q_ACT_NOT_WRD_FIXED;
		public static final int XZC005Q_CSR_ACT_NBR_KCRE = XZC005Q_ACT_NOT_WRD_CSUM + Len.XZC005Q_ACT_NOT_WRD_CSUM;
		public static final int XZC005Q_NOT_PRC_TS_KCRE = XZC005Q_CSR_ACT_NBR_KCRE + Len.XZC005Q_CSR_ACT_NBR_KCRE;
		public static final int XZC005Q_POL_NBR_KCRE = XZC005Q_NOT_PRC_TS_KCRE + Len.XZC005Q_NOT_PRC_TS_KCRE;
		public static final int XZC005Q_ST_WRD_SEQ_CD_KCRE = XZC005Q_POL_NBR_KCRE + Len.XZC005Q_POL_NBR_KCRE;
		public static final int XZC005Q_ACT_NOT_WRD_DATES = XZC005Q_ST_WRD_SEQ_CD_KCRE + Len.XZC005Q_ST_WRD_SEQ_CD_KCRE;
		public static final int XZC005Q_TRANS_PROCESS_DT = XZC005Q_ACT_NOT_WRD_DATES;
		public static final int XZC005Q_ACT_NOT_WRD_KEY = XZC005Q_TRANS_PROCESS_DT + Len.XZC005Q_TRANS_PROCESS_DT;
		public static final int XZC005Q_CSR_ACT_NBR = XZC005Q_ACT_NOT_WRD_KEY;
		public static final int XZC005Q_NOT_PRC_TS = XZC005Q_CSR_ACT_NBR + Len.XZC005Q_CSR_ACT_NBR;
		public static final int XZC005Q_POL_NBR = XZC005Q_NOT_PRC_TS + Len.XZC005Q_NOT_PRC_TS;
		public static final int XZC005Q_ST_WRD_SEQ_CD = XZC005Q_POL_NBR + Len.XZC005Q_POL_NBR;
		public static final int XZC005Q_ACT_NOT_WRD_KEY_CI = XZC005Q_ST_WRD_SEQ_CD + Len.XZC005Q_ST_WRD_SEQ_CD;
		public static final int XZC005Q_CSR_ACT_NBR_CI = XZC005Q_ACT_NOT_WRD_KEY_CI;
		public static final int XZC005Q_NOT_PRC_TS_CI = XZC005Q_CSR_ACT_NBR_CI + Len.XZC005Q_CSR_ACT_NBR_CI;
		public static final int XZC005Q_POL_NBR_CI = XZC005Q_NOT_PRC_TS_CI + Len.XZC005Q_NOT_PRC_TS_CI;
		public static final int XZC005Q_ST_WRD_SEQ_CD_CI = XZC005Q_POL_NBR_CI + Len.XZC005Q_POL_NBR_CI;
		public static final int XZC005Q_ACT_NOT_WRD_DATA = XZC005Q_ST_WRD_SEQ_CD_CI + Len.XZC005Q_ST_WRD_SEQ_CD_CI;
		public static final int FLR1 = XZC005Q_ACT_NOT_WRD_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC005Q_ACT_NOT_WRD_CSUM = 9;
		public static final int XZC005Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC005Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC005Q_POL_NBR_KCRE = 32;
		public static final int XZC005Q_ST_WRD_SEQ_CD_KCRE = 32;
		public static final int XZC005Q_TRANS_PROCESS_DT = 10;
		public static final int XZC005Q_CSR_ACT_NBR = 9;
		public static final int XZC005Q_NOT_PRC_TS = 26;
		public static final int XZC005Q_POL_NBR = 25;
		public static final int XZC005Q_ST_WRD_SEQ_CD = 5;
		public static final int XZC005Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC005Q_NOT_PRC_TS_CI = 1;
		public static final int XZC005Q_POL_NBR_CI = 1;
		public static final int XZC005Q_ST_WRD_SEQ_CD_CI = 1;
		public static final int XZC005Q_ACT_NOT_WRD_FIXED = XZC005Q_ACT_NOT_WRD_CSUM + XZC005Q_CSR_ACT_NBR_KCRE + XZC005Q_NOT_PRC_TS_KCRE
				+ XZC005Q_POL_NBR_KCRE + XZC005Q_ST_WRD_SEQ_CD_KCRE;
		public static final int XZC005Q_ACT_NOT_WRD_DATES = XZC005Q_TRANS_PROCESS_DT;
		public static final int XZC005Q_ACT_NOT_WRD_KEY = XZC005Q_CSR_ACT_NBR + XZC005Q_NOT_PRC_TS + XZC005Q_POL_NBR + XZC005Q_ST_WRD_SEQ_CD;
		public static final int XZC005Q_ACT_NOT_WRD_KEY_CI = XZC005Q_CSR_ACT_NBR_CI + XZC005Q_NOT_PRC_TS_CI + XZC005Q_POL_NBR_CI
				+ XZC005Q_ST_WRD_SEQ_CD_CI;
		public static final int FLR1 = 1;
		public static final int XZC005Q_ACT_NOT_WRD_DATA = FLR1;
		public static final int XZC005Q_ACT_NOT_WRD_ROW = XZC005Q_ACT_NOT_WRD_FIXED + XZC005Q_ACT_NOT_WRD_DATES + XZC005Q_ACT_NOT_WRD_KEY
				+ XZC005Q_ACT_NOT_WRD_KEY_CI + XZC005Q_ACT_NOT_WRD_DATA;
		public static final int L_FW_REQ_XZ0C0005 = XZC005Q_ACT_NOT_WRD_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0005;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
