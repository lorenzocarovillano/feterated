/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: TS020DRV<br>
 * Copybook: TS020DRV from copybook TS020DRV<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Ts020drv {

	//==== PROPERTIES ====
	//Original name: COMMUNICATION-SHELL-COMMON
	private CommunicationShellCommon communicationShellCommon = new CommunicationShellCommon();
	//Original name: DRIVER-SPECIFIC-DATA
	private DriverSpecificData driverSpecificData = new DriverSpecificData();

	//==== METHODS ====
	public CommunicationShellCommon getCommunicationShellCommon() {
		return communicationShellCommon;
	}

	public DriverSpecificData getDriverSpecificData() {
		return driverSpecificData;
	}
}
