/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0R9060<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0r9060 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9061_MAXOCCURS = 100;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0r9060() {
	}

	public LFrameworkResponseAreaXz0r9060(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza960MaxPolRows(short xza960MaxPolRows) {
		writeBinaryShort(Pos.XZA960_MAX_POL_ROWS, xza960MaxPolRows);
	}

	/**Original name: XZA960-MAX-POL-ROWS<br>*/
	public short getXza960MaxPolRows() {
		return readBinaryShort(Pos.XZA960_MAX_POL_ROWS);
	}

	public void setXza960CsrActNbr(String xza960CsrActNbr) {
		writeString(Pos.XZA960_CSR_ACT_NBR, xza960CsrActNbr, Len.XZA960_CSR_ACT_NBR);
	}

	/**Original name: XZA960-CSR-ACT-NBR<br>*/
	public String getXza960CsrActNbr() {
		return readString(Pos.XZA960_CSR_ACT_NBR, Len.XZA960_CSR_ACT_NBR);
	}

	public void setXza960NotPrcTs(String xza960NotPrcTs) {
		writeString(Pos.XZA960_NOT_PRC_TS, xza960NotPrcTs, Len.XZA960_NOT_PRC_TS);
	}

	/**Original name: XZA960-NOT-PRC-TS<br>*/
	public String getXza960NotPrcTs() {
		return readString(Pos.XZA960_NOT_PRC_TS, Len.XZA960_NOT_PRC_TS);
	}

	public void setXza960NotDt(String xza960NotDt) {
		writeString(Pos.XZA960_NOT_DT, xza960NotDt, Len.XZA960_NOT_DT);
	}

	/**Original name: XZA960-NOT-DT<br>*/
	public String getXza960NotDt() {
		return readString(Pos.XZA960_NOT_DT, Len.XZA960_NOT_DT);
	}

	public void setXza960TotFeeAmt(AfDecimal xza960TotFeeAmt) {
		writeDecimal(Pos.XZA960_TOT_FEE_AMT, xza960TotFeeAmt.copy());
	}

	/**Original name: XZA960-TOT-FEE-AMT<br>*/
	public AfDecimal getXza960TotFeeAmt() {
		return readDecimal(Pos.XZA960_TOT_FEE_AMT, Len.Int.XZA960_TOT_FEE_AMT, Len.Fract.XZA960_TOT_FEE_AMT);
	}

	public void setXza960StAbb(String xza960StAbb) {
		writeString(Pos.XZA960_ST_ABB, xza960StAbb, Len.XZA960_ST_ABB);
	}

	/**Original name: XZA960-ST-ABB<br>*/
	public String getXza960StAbb() {
		return readString(Pos.XZA960_ST_ABB, Len.XZA960_ST_ABB);
	}

	public void setXza960DsyPrtGrp(String xza960DsyPrtGrp) {
		writeString(Pos.XZA960_DSY_PRT_GRP, xza960DsyPrtGrp, Len.XZA960_DSY_PRT_GRP);
	}

	/**Original name: XZA960-DSY-PRT-GRP<br>*/
	public String getXza960DsyPrtGrp() {
		return readString(Pos.XZA960_DSY_PRT_GRP, Len.XZA960_DSY_PRT_GRP);
	}

	public String getlFwRespXz0a9061Formatted(int lFwRespXz0a9061Idx) {
		int position = Pos.lFwRespXz0a9061(lFwRespXz0a9061Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9061);
	}

	public void setXza961rPolNbr(int xza961rPolNbrIdx, String xza961rPolNbr) {
		int position = Pos.xza961PolNbr(xza961rPolNbrIdx - 1);
		writeString(position, xza961rPolNbr, Len.XZA961_POL_NBR);
	}

	/**Original name: XZA961R-POL-NBR<br>*/
	public String getXza961rPolNbr(int xza961rPolNbrIdx) {
		int position = Pos.xza961PolNbr(xza961rPolNbrIdx - 1);
		return readString(position, Len.XZA961_POL_NBR);
	}

	public void setXza961rPolTypCd(int xza961rPolTypCdIdx, String xza961rPolTypCd) {
		int position = Pos.xza961PolTypCd(xza961rPolTypCdIdx - 1);
		writeString(position, xza961rPolTypCd, Len.XZA961_POL_TYP_CD);
	}

	/**Original name: XZA961R-POL-TYP-CD<br>*/
	public String getXza961rPolTypCd(int xza961rPolTypCdIdx) {
		int position = Pos.xza961PolTypCd(xza961rPolTypCdIdx - 1);
		return readString(position, Len.XZA961_POL_TYP_CD);
	}

	public void setXza961rPolTypDes(int xza961rPolTypDesIdx, String xza961rPolTypDes) {
		int position = Pos.xza961PolTypDes(xza961rPolTypDesIdx - 1);
		writeString(position, xza961rPolTypDes, Len.XZA961_POL_TYP_DES);
	}

	/**Original name: XZA961R-POL-TYP-DES<br>*/
	public String getXza961rPolTypDes(int xza961rPolTypDesIdx) {
		int position = Pos.xza961PolTypDes(xza961rPolTypDesIdx - 1);
		return readString(position, Len.XZA961_POL_TYP_DES);
	}

	public void setXza961rPriRskStAbb(int xza961rPriRskStAbbIdx, String xza961rPriRskStAbb) {
		int position = Pos.xza961PriRskStAbb(xza961rPriRskStAbbIdx - 1);
		writeString(position, xza961rPriRskStAbb, Len.XZA961_PRI_RSK_ST_ABB);
	}

	/**Original name: XZA961R-PRI-RSK-ST-ABB<br>*/
	public String getXza961rPriRskStAbb(int xza961rPriRskStAbbIdx) {
		int position = Pos.xza961PriRskStAbb(xza961rPriRskStAbbIdx - 1);
		return readString(position, Len.XZA961_PRI_RSK_ST_ABB);
	}

	public void setXza961rNotEffDt(int xza961rNotEffDtIdx, String xza961rNotEffDt) {
		int position = Pos.xza961NotEffDt(xza961rNotEffDtIdx - 1);
		writeString(position, xza961rNotEffDt, Len.XZA961_NOT_EFF_DT);
	}

	/**Original name: XZA961R-NOT-EFF-DT<br>*/
	public String getXza961rNotEffDt(int xza961rNotEffDtIdx) {
		int position = Pos.xza961NotEffDt(xza961rNotEffDtIdx - 1);
		return readString(position, Len.XZA961_NOT_EFF_DT);
	}

	public void setXza961rPolEffDt(int xza961rPolEffDtIdx, String xza961rPolEffDt) {
		int position = Pos.xza961PolEffDt(xza961rPolEffDtIdx - 1);
		writeString(position, xza961rPolEffDt, Len.XZA961_POL_EFF_DT);
	}

	/**Original name: XZA961R-POL-EFF-DT<br>*/
	public String getXza961rPolEffDt(int xza961rPolEffDtIdx) {
		int position = Pos.xza961PolEffDt(xza961rPolEffDtIdx - 1);
		return readString(position, Len.XZA961_POL_EFF_DT);
	}

	public void setXza961rPolExpDt(int xza961rPolExpDtIdx, String xza961rPolExpDt) {
		int position = Pos.xza961PolExpDt(xza961rPolExpDtIdx - 1);
		writeString(position, xza961rPolExpDt, Len.XZA961_POL_EXP_DT);
	}

	/**Original name: XZA961R-POL-EXP-DT<br>*/
	public String getXza961rPolExpDt(int xza961rPolExpDtIdx) {
		int position = Pos.xza961PolExpDt(xza961rPolExpDtIdx - 1);
		return readString(position, Len.XZA961_POL_EXP_DT);
	}

	public void setXza961rPolDueAmt(int xza961rPolDueAmtIdx, AfDecimal xza961rPolDueAmt) {
		int position = Pos.xza961PolDueAmt(xza961rPolDueAmtIdx - 1);
		writeDecimal(position, xza961rPolDueAmt.copy());
	}

	/**Original name: XZA961R-POL-DUE-AMT<br>*/
	public AfDecimal getXza961rPolDueAmt(int xza961rPolDueAmtIdx) {
		int position = Pos.xza961PolDueAmt(xza961rPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.XZA961R_POL_DUE_AMT, Len.Fract.XZA961R_POL_DUE_AMT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A9060 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA960_GET_ACT_NOT_DETAIL = L_FW_RESP_XZ0A9060;
		public static final int XZA960_MAX_POL_ROWS = XZA960_GET_ACT_NOT_DETAIL;
		public static final int XZA960_CSR_ACT_NBR = XZA960_MAX_POL_ROWS + Len.XZA960_MAX_POL_ROWS;
		public static final int XZA960_NOT_PRC_TS = XZA960_CSR_ACT_NBR + Len.XZA960_CSR_ACT_NBR;
		public static final int XZA960_NOT_DT = XZA960_NOT_PRC_TS + Len.XZA960_NOT_PRC_TS;
		public static final int XZA960_TOT_FEE_AMT = XZA960_NOT_DT + Len.XZA960_NOT_DT;
		public static final int XZA960_ST_ABB = XZA960_TOT_FEE_AMT + Len.XZA960_TOT_FEE_AMT;
		public static final int XZA960_DSY_PRT_GRP = XZA960_ST_ABB + Len.XZA960_ST_ABB;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9061(int idx) {
			return XZA960_DSY_PRT_GRP + Len.XZA960_DSY_PRT_GRP + idx * Len.L_FW_RESP_XZ0A9061;
		}

		public static int xza961GetPolListDetail(int idx) {
			return lFwRespXz0a9061(idx);
		}

		public static int xza961PolNbr(int idx) {
			return xza961GetPolListDetail(idx);
		}

		public static int xza961PolTypCd(int idx) {
			return xza961PolNbr(idx) + Len.XZA961_POL_NBR;
		}

		public static int xza961PolTypDes(int idx) {
			return xza961PolTypCd(idx) + Len.XZA961_POL_TYP_CD;
		}

		public static int xza961PriRskStAbb(int idx) {
			return xza961PolTypDes(idx) + Len.XZA961_POL_TYP_DES;
		}

		public static int xza961NotEffDt(int idx) {
			return xza961PriRskStAbb(idx) + Len.XZA961_PRI_RSK_ST_ABB;
		}

		public static int xza961PolEffDt(int idx) {
			return xza961NotEffDt(idx) + Len.XZA961_NOT_EFF_DT;
		}

		public static int xza961PolExpDt(int idx) {
			return xza961PolEffDt(idx) + Len.XZA961_POL_EFF_DT;
		}

		public static int xza961PolDueAmt(int idx) {
			return xza961PolExpDt(idx) + Len.XZA961_POL_EXP_DT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA960_MAX_POL_ROWS = 2;
		public static final int XZA960_CSR_ACT_NBR = 9;
		public static final int XZA960_NOT_PRC_TS = 26;
		public static final int XZA960_NOT_DT = 10;
		public static final int XZA960_TOT_FEE_AMT = 10;
		public static final int XZA960_ST_ABB = 2;
		public static final int XZA960_DSY_PRT_GRP = 5;
		public static final int XZA961_POL_NBR = 25;
		public static final int XZA961_POL_TYP_CD = 3;
		public static final int XZA961_POL_TYP_DES = 30;
		public static final int XZA961_PRI_RSK_ST_ABB = 2;
		public static final int XZA961_NOT_EFF_DT = 10;
		public static final int XZA961_POL_EFF_DT = 10;
		public static final int XZA961_POL_EXP_DT = 10;
		public static final int XZA961_POL_DUE_AMT = 10;
		public static final int XZA961_GET_POL_LIST_DETAIL = XZA961_POL_NBR + XZA961_POL_TYP_CD + XZA961_POL_TYP_DES + XZA961_PRI_RSK_ST_ABB
				+ XZA961_NOT_EFF_DT + XZA961_POL_EFF_DT + XZA961_POL_EXP_DT + XZA961_POL_DUE_AMT;
		public static final int L_FW_RESP_XZ0A9061 = XZA961_GET_POL_LIST_DETAIL;
		public static final int XZA960_GET_ACT_NOT_DETAIL = XZA960_MAX_POL_ROWS + XZA960_CSR_ACT_NBR + XZA960_NOT_PRC_TS + XZA960_NOT_DT
				+ XZA960_TOT_FEE_AMT + XZA960_ST_ABB + XZA960_DSY_PRT_GRP;
		public static final int L_FW_RESP_XZ0A9060 = XZA960_GET_ACT_NOT_DETAIL;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A9060
				+ LFrameworkResponseAreaXz0r9060.L_FW_RESP_XZ0A9061_MAXOCCURS * L_FW_RESP_XZ0A9061;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA960_TOT_FEE_AMT = 8;
			public static final int XZA961R_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZA960_TOT_FEE_AMT = 2;
			public static final int XZA961R_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
