/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-XZ0C0005-LAYOUT<br>
 * Variable: WS-XZ0C0005-LAYOUT from program XZ0F0005<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0c0005Layout extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC005-ACT-NOT-WRD-CSUM
	private String actNotWrdCsum = DefaultValues.stringVal(Len.ACT_NOT_WRD_CSUM);
	//Original name: XZC005-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC005-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);
	//Original name: XZC005-POL-NBR-KCRE
	private String polNbrKcre = DefaultValues.stringVal(Len.POL_NBR_KCRE);
	//Original name: XZC005-ST-WRD-SEQ-CD-KCRE
	private String stWrdSeqCdKcre = DefaultValues.stringVal(Len.ST_WRD_SEQ_CD_KCRE);
	//Original name: XZC005-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC005-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC005-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC005-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZC005-ST-WRD-SEQ-CD
	private String stWrdSeqCd = DefaultValues.stringVal(Len.ST_WRD_SEQ_CD);
	//Original name: XZC005-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC005-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC005-POL-NBR-CI
	private char polNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC005-ST-WRD-SEQ-CD-CI
	private char stWrdSeqCdCi = DefaultValues.CHAR_VAL;
	/**Original name: FILLER-XZC005-ACT-NOT-WRD-DATA<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:
	 * *  NO DATA OTHER THAN KEY FIELDS - BUT NEED TO LEAVE THE
	 * *  07 DATA ELEMENT IN ORDER FOR THE COMPILE TO WORK.</pre>*/
	private char flr1 = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0C0005_LAYOUT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0c0005LayoutBytes(buf);
	}

	public void setWsXz0c0005LayoutFormatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0C0005_LAYOUT];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0C0005_LAYOUT);
		setWsXz0c0005LayoutBytes(buffer, 1);
	}

	public String getWsXz0c0005LayoutFormatted() {
		return getActNotWrdRowFormatted();
	}

	public void setWsXz0c0005LayoutBytes(byte[] buffer) {
		setWsXz0c0005LayoutBytes(buffer, 1);
	}

	public byte[] getWsXz0c0005LayoutBytes() {
		byte[] buffer = new byte[Len.WS_XZ0C0005_LAYOUT];
		return getWsXz0c0005LayoutBytes(buffer, 1);
	}

	public void setWsXz0c0005LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotWrdRowBytes(buffer, position);
	}

	public byte[] getWsXz0c0005LayoutBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotWrdRowBytes(buffer, position);
		return buffer;
	}

	public String getActNotWrdRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotWrdRowBytes());
	}

	/**Original name: XZC005-ACT-NOT-WRD-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0005 - ACT_NOT_WRD TABLE                                   *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 18 Nov 2008 E404GRK   GENERATED                        *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getActNotWrdRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_WRD_ROW];
		return getActNotWrdRowBytes(buffer, 1);
	}

	public void setActNotWrdRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setActNotWrdFixedBytes(buffer, position);
		position += Len.ACT_NOT_WRD_FIXED;
		setActNotWrdDatesBytes(buffer, position);
		position += Len.ACT_NOT_WRD_DATES;
		setActNotWrdKeyBytes(buffer, position);
		position += Len.ACT_NOT_WRD_KEY;
		setActNotWrdKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_WRD_KEY_CI;
		setActNotWrdDataBytes(buffer, position);
	}

	public byte[] getActNotWrdRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getActNotWrdFixedBytes(buffer, position);
		position += Len.ACT_NOT_WRD_FIXED;
		getActNotWrdDatesBytes(buffer, position);
		position += Len.ACT_NOT_WRD_DATES;
		getActNotWrdKeyBytes(buffer, position);
		position += Len.ACT_NOT_WRD_KEY;
		getActNotWrdKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_WRD_KEY_CI;
		getActNotWrdDataBytes(buffer, position);
		return buffer;
	}

	public void setActNotWrdFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotWrdCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_WRD_CSUM);
		position += Len.ACT_NOT_WRD_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		polNbrKcre = MarshalByte.readString(buffer, position, Len.POL_NBR_KCRE);
		position += Len.POL_NBR_KCRE;
		stWrdSeqCdKcre = MarshalByte.readString(buffer, position, Len.ST_WRD_SEQ_CD_KCRE);
	}

	public byte[] getActNotWrdFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotWrdCsum, Len.ACT_NOT_WRD_CSUM);
		position += Len.ACT_NOT_WRD_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		position += Len.NOT_PRC_TS_KCRE;
		MarshalByte.writeString(buffer, position, polNbrKcre, Len.POL_NBR_KCRE);
		position += Len.POL_NBR_KCRE;
		MarshalByte.writeString(buffer, position, stWrdSeqCdKcre, Len.ST_WRD_SEQ_CD_KCRE);
		return buffer;
	}

	public void setActNotWrdCsumFormatted(String actNotWrdCsum) {
		this.actNotWrdCsum = Trunc.toUnsignedNumeric(actNotWrdCsum, Len.ACT_NOT_WRD_CSUM);
	}

	public int getActNotWrdCsum() {
		return NumericDisplay.asInt(this.actNotWrdCsum);
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public void setPolNbrKcre(String polNbrKcre) {
		this.polNbrKcre = Functions.subString(polNbrKcre, Len.POL_NBR_KCRE);
	}

	public String getPolNbrKcre() {
		return this.polNbrKcre;
	}

	public void setStWrdSeqCdKcre(String stWrdSeqCdKcre) {
		this.stWrdSeqCdKcre = Functions.subString(stWrdSeqCdKcre, Len.ST_WRD_SEQ_CD_KCRE);
	}

	public String getStWrdSeqCdKcre() {
		return this.stWrdSeqCdKcre;
	}

	public void setActNotWrdDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotWrdDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setActNotWrdKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		stWrdSeqCd = MarshalByte.readString(buffer, position, Len.ST_WRD_SEQ_CD);
	}

	public byte[] getActNotWrdKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, stWrdSeqCd, Len.ST_WRD_SEQ_CD);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setStWrdSeqCd(String stWrdSeqCd) {
		this.stWrdSeqCd = Functions.subString(stWrdSeqCd, Len.ST_WRD_SEQ_CD);
	}

	public String getStWrdSeqCd() {
		return this.stWrdSeqCd;
	}

	public void setActNotWrdKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		stWrdSeqCdCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotWrdKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, stWrdSeqCdCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setPolNbrCi(char polNbrCi) {
		this.polNbrCi = polNbrCi;
	}

	public char getPolNbrCi() {
		return this.polNbrCi;
	}

	public void setStWrdSeqCdCi(char stWrdSeqCdCi) {
		this.stWrdSeqCdCi = stWrdSeqCdCi;
	}

	public char getStWrdSeqCdCi() {
		return this.stWrdSeqCdCi;
	}

	public void setActNotWrdDataBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotWrdDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		return buffer;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0c0005LayoutBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_WRD_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int POL_NBR_KCRE = 32;
		public static final int ST_WRD_SEQ_CD_KCRE = 32;
		public static final int ACT_NOT_WRD_FIXED = ACT_NOT_WRD_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE + POL_NBR_KCRE + ST_WRD_SEQ_CD_KCRE;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int ACT_NOT_WRD_DATES = TRANS_PROCESS_DT;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int ST_WRD_SEQ_CD = 5;
		public static final int ACT_NOT_WRD_KEY = CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + ST_WRD_SEQ_CD;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int POL_NBR_CI = 1;
		public static final int ST_WRD_SEQ_CD_CI = 1;
		public static final int ACT_NOT_WRD_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + POL_NBR_CI + ST_WRD_SEQ_CD_CI;
		public static final int FLR1 = 1;
		public static final int ACT_NOT_WRD_DATA = FLR1;
		public static final int ACT_NOT_WRD_ROW = ACT_NOT_WRD_FIXED + ACT_NOT_WRD_DATES + ACT_NOT_WRD_KEY + ACT_NOT_WRD_KEY_CI + ACT_NOT_WRD_DATA;
		public static final int WS_XZ0C0005_LAYOUT = ACT_NOT_WRD_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
