/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UbocUowLockStrategyCd;
import com.federatedinsurance.crs.ws.enums.UtcsAuditInd;
import com.federatedinsurance.crs.ws.enums.UtcsCnLvlSecInd;
import com.federatedinsurance.crs.ws.enums.UtcsDataPrivacyInd;

/**Original name: UTCS-UOW-TRANS-CONFIG-INFO<br>
 * Variable: UTCS-UOW-TRANS-CONFIG-INFO from copybook HALLUTCS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class UtcsUowTransConfigInfo {

	//==== PROPERTIES ====
	//Original name: UTCS-UOW-NAME
	private String uowName = DefaultValues.stringVal(Len.UOW_NAME);
	//Original name: UTCS-UOW-MODULE
	private String uowModule = DefaultValues.stringVal(Len.UOW_MODULE);
	//Original name: UTCS-FOLDER-LOKS-DEL-TIME
	private int folderLoksDelTime = DefaultValues.BIN_INT_VAL;
	//Original name: UTCS-UOW-LOCK-ACTION-CD
	private UbocUowLockStrategyCd uowLockActionCd = new UbocUowLockStrategyCd();
	//Original name: UTCS-CN-LVL-SEC-IND
	private UtcsCnLvlSecInd cnLvlSecInd = new UtcsCnLvlSecInd();
	//Original name: UTCS-CONTROL-LVL-SEC-MOD-NI
	private char controlLvlSecModNi = DefaultValues.CHAR_VAL;
	//Original name: UTCS-CONTROL-LVL-SEC-MODULE
	private String controlLvlSecModule = DefaultValues.stringVal(Len.CONTROL_LVL_SEC_MODULE);
	//Original name: UTCS-DATA-PRIVACY-IND
	private UtcsDataPrivacyInd dataPrivacyInd = new UtcsDataPrivacyInd();
	//Original name: UTCS-AUDIT-IND
	private UtcsAuditInd auditInd = new UtcsAuditInd();

	//==== METHODS ====
	public void initUtcsUowTransConfigInfoSpaces() {
		uowName = "";
		uowModule = "";
		folderLoksDelTime = Types.INVALID_BINARY_INT_VAL;
		uowLockActionCd.setUbocUowLockStrategyCd(Types.SPACE_CHAR);
		cnLvlSecInd.setCnLvlSecInd(Types.SPACE_CHAR);
		controlLvlSecModNi = Types.SPACE_CHAR;
		controlLvlSecModule = "";
		dataPrivacyInd.setDataPrivacyInd(Types.SPACE_CHAR);
		auditInd.setAuditInd(Types.SPACE_CHAR);
	}

	public void setUowName(String uowName) {
		this.uowName = Functions.subString(uowName, Len.UOW_NAME);
	}

	public String getUowName() {
		return this.uowName;
	}

	public void setUowModule(String uowModule) {
		this.uowModule = Functions.subString(uowModule, Len.UOW_MODULE);
	}

	public String getUowModule() {
		return this.uowModule;
	}

	public void setFolderLoksDelTime(int folderLoksDelTime) {
		this.folderLoksDelTime = folderLoksDelTime;
	}

	public int getFolderLoksDelTime() {
		return this.folderLoksDelTime;
	}

	public void setControlLvlSecModNi(char controlLvlSecModNi) {
		this.controlLvlSecModNi = controlLvlSecModNi;
	}

	public char getControlLvlSecModNi() {
		return this.controlLvlSecModNi;
	}

	public void setControlLvlSecModule(String controlLvlSecModule) {
		this.controlLvlSecModule = Functions.subString(controlLvlSecModule, Len.CONTROL_LVL_SEC_MODULE);
	}

	public String getControlLvlSecModule() {
		return this.controlLvlSecModule;
	}

	public UtcsAuditInd getAuditInd() {
		return auditInd;
	}

	public UtcsCnLvlSecInd getCnLvlSecInd() {
		return cnLvlSecInd;
	}

	public UtcsDataPrivacyInd getDataPrivacyInd() {
		return dataPrivacyInd;
	}

	public UbocUowLockStrategyCd getUowLockActionCd() {
		return uowLockActionCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UOW_NAME = 32;
		public static final int UOW_MODULE = 32;
		public static final int CONTROL_LVL_SEC_MODULE = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
