/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.data.fto;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.buffer.IBuffer;
import com.bphx.ctu.af.core.buffer.IByteMarshall;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.io.file.FileRecord;
import com.bphx.ctu.af.util.Functions;

/**Original name: I-START-DATE-FILE<br>
 * File: I-START-DATE-FILE from program XZ004000<br>
 * Generated as a class for rule FTO.<br>*/
public class IStartDateFileTO extends FileRecord implements IByteMarshall {

	//==== PROPERTIES ====
	//Original name: SD-START-TIMESTAMP
	private String sdStartTimestamp = DefaultValues.stringVal(Len.SD_START_TIMESTAMP);
	//Original name: FILLER-START-DATE-RECORD
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setStartDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		sdStartTimestamp = MarshalByte.readString(buffer, position, Len.SD_START_TIMESTAMP);
		position += Len.SD_START_TIMESTAMP;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getStartDateRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, sdStartTimestamp, Len.SD_START_TIMESTAMP);
		position += Len.SD_START_TIMESTAMP;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setSdStartTimestamp(String sdStartTimestamp) {
		this.sdStartTimestamp = Functions.subString(sdStartTimestamp, Len.SD_START_TIMESTAMP);
	}

	public String getSdStartTimestamp() {
		return this.sdStartTimestamp;
	}

	public String getSdStartTimestampFormatted() {
		return Functions.padBlanks(getSdStartTimestamp(), Len.SD_START_TIMESTAMP);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	@Override
	public void getData(byte[] destination, int offset) {
		getStartDateRecordBytes(destination, offset);
	}

	@Override
	public void setData(byte[] data, int offset, int length) {
		setStartDateRecordBytes(data, offset);
	}

	@Override
	public int getLength() {
		return Len.START_DATE_RECORD;
	}

	@Override
	public IBuffer copy() {
		IStartDateFileTO copyTO = new IStartDateFileTO();
		copyTO.assign(this);
		return copyTO;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SD_START_TIMESTAMP = 26;
		public static final int FLR1 = 54;
		public static final int START_DATE_RECORD = SD_START_TIMESTAMP + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
