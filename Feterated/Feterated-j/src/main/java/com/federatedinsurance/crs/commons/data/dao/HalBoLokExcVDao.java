/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalBoLokExcV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_BO_LOK_EXC_V]
 * 
 */
public class HalBoLokExcVDao extends BaseSqlDao<IHalBoLokExcV> {

	private Cursor wsCursor11;
	private final IRowMapper<IHalBoLokExcV> fetchWsCursor11Rm = buildNamedRowMapper(IHalBoLokExcV.class, "contextTxt", "lokExcMdu");

	public HalBoLokExcVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalBoLokExcV> getToClass() {
		return IHalBoLokExcV.class;
	}

	public DbAccessStatus openWsCursor11(String uowNm, String busObjNm) {
		wsCursor11 = buildQuery("openWsCursor11").bind("uowNm", uowNm).bind("busObjNm", busObjNm).open();
		return dbStatus;
	}

	public DbAccessStatus closeWsCursor11() {
		return closeCursor(wsCursor11);
	}

	public IHalBoLokExcV fetchWsCursor11(IHalBoLokExcV iHalBoLokExcV) {
		return fetch(wsCursor11, iHalBoLokExcV, fetchWsCursor11Rm);
	}
}
