/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [HAL_UOW_FUN_SEC_V]
 * 
 */
public interface IHalUowFunSecV extends BaseSqlTo {

	/**
	 * Host Variable HUFSH-UOW-NM
	 * 
	 */
	String getHufshUowNm();

	void setHufshUowNm(String hufshUowNm);

	/**
	 * Host Variable HUFSH-SEC-GRP-NM
	 * 
	 */
	String getHufshSecGrpNm();

	void setHufshSecGrpNm(String hufshSecGrpNm);

	/**
	 * Host Variable WS-AUTH-USERID-FOR-CURSOR
	 * 
	 */
	String getWsAuthUseridForCursor();

	void setWsAuthUseridForCursor(String wsAuthUseridForCursor);

	/**
	 * Host Variable WS-FULL-CONTEXT
	 * 
	 */
	String getWsFullContext();

	void setWsFullContext(String wsFullContext);

	/**
	 * Host Variable WS-CONTEXT-FIRST-10-FOR-CSR
	 * 
	 */
	String getWsContextFirst10ForCsr();

	void setWsContextFirst10ForCsr(String wsContextFirst10ForCsr);

	/**
	 * Host Variable HUFSH-SEC-ASC-TYP
	 * 
	 */
	String getHufshSecAscTyp();

	void setHufshSecAscTyp(String hufshSecAscTyp);

	/**
	 * Host Variable HUFSH-FUN-SEQ-NBR
	 * 
	 */
	short getFunSeqNbr();

	void setFunSeqNbr(short funSeqNbr);

	/**
	 * Host Variable HUFSH-AUT-ACTION-CD
	 * 
	 */
	String getAutActionCd();

	void setAutActionCd(String autActionCd);

	/**
	 * Host Variable HUFSH-AUT-FUN-NM
	 * 
	 */
	String getAutFunNm();

	void setAutFunNm(String autFunNm);

	/**
	 * Host Variable HUFSH-LNK-TO-MDU-NM
	 * 
	 */
	String getLnkToMduNm();

	void setLnkToMduNm(String lnkToMduNm);

	/**
	 * Host Variable HUFSH-APP-NM
	 * 
	 */
	String getAppNm();

	void setAppNm(String appNm);
};
