/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_POL]
 * 
 */
public class ActNotPolDao extends BaseSqlDao<IActNotPol> {

	private Cursor polLstByNotCsr;
	private Cursor policyCsr;
	private Cursor policyCsr1;
	private Cursor actNotPolCsr;
	private final IRowMapper<IActNotPol> fetchPolLstByNotCsrRm = buildNamedRowMapper(IActNotPol.class, "csrActNbr", "notPrcTs", "polNbr", "polTypCd",
			"polPriRskStAbb", "notEffDtObj", "polEffDt", "polExpDt", "polDueAmtObj", "ninCltId", "ninAdrId", "wfStartedIndObj", "polBilStaCdObj");
	private final IRowMapper<IActNotPol> fetchPolicyCsrRm = buildNamedRowMapper(IActNotPol.class, "polNbr", "polTypCd", "polPriRskStAbb",
			"notEffDtObj", "polEffDt", "polExpDt", "polDueAmtObj");
	private final IRowMapper<IActNotPol> fetchPolicyCsr1Rm = buildNamedRowMapper(IActNotPol.class, "polNbr", "polTypCd", "polPriRskStAbb", "polEffDt",
			"polExpDt");

	public ActNotPolDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPol> getToClass() {
		return IActNotPol.class;
	}

	public DbAccessStatus updateRec(String accountNbr, String notPrcTs, String polNbr) {
		return buildQuery("updateRec").bind("accountNbr", accountNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).executeUpdate();
	}

	public DbAccessStatus openPolLstByNotCsr(String csrActNbr, String notPrcTs) {
		polLstByNotCsr = buildQuery("openPolLstByNotCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotPol fetchPolLstByNotCsr(IActNotPol iActNotPol) {
		return fetch(polLstByNotCsr, iActNotPol, fetchPolLstByNotCsrRm);
	}

	public DbAccessStatus closePolLstByNotCsr() {
		return closeCursor(polLstByNotCsr);
	}

	public DbAccessStatus openPolicyCsr(String csrActNbr, String notPrcTs) {
		policyCsr = buildQuery("openPolicyCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotPol fetchPolicyCsr(IActNotPol iActNotPol) {
		return fetch(policyCsr, iActNotPol, fetchPolicyCsrRm);
	}

	public DbAccessStatus closePolicyCsr() {
		return closeCursor(policyCsr);
	}

	public DbAccessStatus openPolicyCsr1(String csrActNbr, String notPrcTs) {
		policyCsr1 = buildQuery("openPolicyCsr1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotPol fetchPolicyCsr1(IActNotPol iActNotPol) {
		return fetch(policyCsr1, iActNotPol, fetchPolicyCsr1Rm);
	}

	public DbAccessStatus closePolicyCsr1() {
		return closeCursor(policyCsr1);
	}

	public String selectRec(String csrActNbr, String notPrcTs, String polNbr, String dft) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).scalarResultString(dft);
	}

	public DbAccessStatus openActNotPolCsr(String csrActNbr, String notPrcTs, String polNbr) {
		actNotPolCsr = buildQuery("openActNotPolCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotPolCsr() {
		return closeCursor(actNotPolCsr);
	}

	public IActNotPol fetchActNotPolCsr(IActNotPol iActNotPol) {
		return fetch(actNotPolCsr, iActNotPol, fetchPolLstByNotCsrRm);
	}

	public DbAccessStatus insertRec(IActNotPol iActNotPol) {
		return buildQuery("insertRec").bind(iActNotPol).executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, String polNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotPol selectRec1(String csrActNbr, String notPrcTs, String polNbr, IActNotPol iActNotPol) {
		return buildQuery("selectRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).singleResult(iActNotPol);
	}

	public char selectRec2(String csrActNbr, String notPrcTs, char dft) {
		return buildQuery("selectRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).scalarResultChar(dft);
	}

	public IActNotPol selectRec3(String csrActNbr, String notPrcTs, IActNotPol iActNotPol) {
	}

	public IActNotPol selectRec4(String csrActNbr, String notPrcTs, IActNotPol iActNotPol) {
		return buildQuery("selectRec4").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).singleResult(iActNotPol);
	}

	public DbAccessStatus updateRec1(IActNotPol iActNotPol) {
		return buildQuery("updateRec1").bind(iActNotPol).executeUpdate();
	}

	public String selectRec5(String xzh002CsrActNbr, String notPrcTs, String polNbr, String dft) {
		return buildQuery("selectRec5").bind("xzh002CsrActNbr", xzh002CsrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr)
				.scalarResultString(dft);
	}
}
