/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0R0005<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0r0005Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0r0005 constantFields = new ConstantFieldsXz0r0005();
	//Original name: FILLER-EA-01-PROGRAM-LINK-FAILED
	private String flr1 = "CALL TO";
	//Original name: EA-01-FAILED-LINK-PGM-NAME
	private String ea01FailedLinkPgmName = DefaultValues.stringVal(Len.EA01_FAILED_LINK_PGM_NAME);
	//Original name: FILLER-EA-01-PROGRAM-LINK-FAILED-1
	private String flr2 = " FAILED.";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG
	private String flr3 = "XZ0R0005 -";
	//Original name: FILLER-EA-02-INVALID-OPERATION-MSG-1
	private String flr4 = "INVALID OPER:";
	//Original name: EA-02-INVALID-OPERATION-NAME
	private String ea02InvalidOperationName = DefaultValues.stringVal(Len.EA02_INVALID_OPERATION_NAME);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0r0005 workingStorageArea = new WorkingStorageAreaXz0r0005();
	//Original name: TS020TBL
	private Ts020tbl ts020tbl = new Ts020tbl();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getEa01ProgramLinkFailedFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ProgramLinkFailedBytes());
	}

	/**Original name: EA-01-PROGRAM-LINK-FAILED<br>*/
	public byte[] getEa01ProgramLinkFailedBytes() {
		byte[] buffer = new byte[Len.EA01_PROGRAM_LINK_FAILED];
		return getEa01ProgramLinkFailedBytes(buffer, 1);
	}

	public byte[] getEa01ProgramLinkFailedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, ea01FailedLinkPgmName, Len.EA01_FAILED_LINK_PGM_NAME);
		position += Len.EA01_FAILED_LINK_PGM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setEa01FailedLinkPgmName(String ea01FailedLinkPgmName) {
		this.ea01FailedLinkPgmName = Functions.subString(ea01FailedLinkPgmName, Len.EA01_FAILED_LINK_PGM_NAME);
	}

	public String getEa01FailedLinkPgmName() {
		return this.ea01FailedLinkPgmName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getEa02InvalidOperationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidOperationMsgBytes());
	}

	/**Original name: EA-02-INVALID-OPERATION-MSG<br>*/
	public byte[] getEa02InvalidOperationMsgBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_OPERATION_MSG];
		return getEa02InvalidOperationMsgBytes(buffer, 1);
	}

	public byte[] getEa02InvalidOperationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
		return buffer;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa02InvalidOperationName(String ea02InvalidOperationName) {
		this.ea02InvalidOperationName = Functions.subString(ea02InvalidOperationName, Len.EA02_INVALID_OPERATION_NAME);
	}

	public String getEa02InvalidOperationName() {
		return this.ea02InvalidOperationName;
	}

	public void setTableFormatterDataFormatted(String data) {
		byte[] buffer = new byte[Len.TABLE_FORMATTER_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public String getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public ConstantFieldsXz0r0005 getConstantFields() {
		return constantFields;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WorkingStorageAreaXz0r0005 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_FAILED_LINK_PGM_NAME = 8;
		public static final int EA02_INVALID_OPERATION_NAME = 32;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int FLR3 = 11;
		public static final int FLR4 = 14;
		public static final int EA02_INVALID_OPERATION_MSG = EA02_INVALID_OPERATION_NAME + FLR3 + FLR4;
		public static final int TABLE_FORMATTER_DATA = Ts020tbl.Len.TABLE_FORMATTER_PARMS;
		public static final int FLR1 = 8;
		public static final int EA01_PROGRAM_LINK_FAILED = EA01_FAILED_LINK_PGM_NAME + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
