/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.CiErrorInformation;
import com.federatedinsurance.crs.ws.enums.CiFunction;
import com.federatedinsurance.crs.ws.enums.LCmCicsApplId;
import com.federatedinsurance.crs.ws.redefines.CiTransactionSpecificData;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: CICS-PIPE-INTERFACE-CONTRACT<br>
 * Variable: CICS-PIPE-INTERFACE-CONTRACT from copybook TS54701<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class CicsPipeInterfaceContract extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: CI-FUNCTION
	private CiFunction ciFunction = new CiFunction();
	/**Original name: CI-CICS-APPL-ID<br>
	 * <pre> BELOW IS A LIST OF COMMONLY USED CICS REGIONS.
	 *   NOTE:  THERE IS NOT ANY LIMIT IN CONNECTING TO THESE REGIONS
	 *            ONLY.</pre>*/
	private LCmCicsApplId ciCicsApplId = new LCmCicsApplId();
	//Original name: CI-ERROR-INFORMATION
	private CiErrorInformation ciErrorInformation = new CiErrorInformation();
	//Original name: CI-TRANSACTION-SPECIFIC-DATA
	private CiTransactionSpecificData ciTransactionSpecificData = new CiTransactionSpecificData();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CICS_PIPE_INTERFACE_CONTRACT;
	}

	@Override
	public void deserialize(byte[] buf) {
		setCicsPipeInterfaceContractBytes(buf);
	}

	public String getCicsPipeInterfaceContractFormatted() {
		return MarshalByteExt.bufferToStr(getCicsPipeInterfaceContractBytes());
	}

	public void setCicsPipeInterfaceContractBytes(byte[] buffer) {
		setCicsPipeInterfaceContractBytes(buffer, 1);
	}

	public byte[] getCicsPipeInterfaceContractBytes() {
		byte[] buffer = new byte[Len.CICS_PIPE_INTERFACE_CONTRACT];
		return getCicsPipeInterfaceContractBytes(buffer, 1);
	}

	public void setCicsPipeInterfaceContractBytes(byte[] buffer, int offset) {
		int position = offset;
		ciFunction.setCiFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		ciCicsApplId.setCicsApplId(MarshalByte.readString(buffer, position, LCmCicsApplId.Len.CICS_APPL_ID));
		position += LCmCicsApplId.Len.CICS_APPL_ID;
		ciErrorInformation.setCiErrorInformationBytes(buffer, position);
		position += CiErrorInformation.Len.CI_ERROR_INFORMATION;
		ciTransactionSpecificData.setCiTransactionSpecificDataFromBuffer(buffer, position);
	}

	public byte[] getCicsPipeInterfaceContractBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, ciFunction.getCiFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciCicsApplId.getCicsApplId(), LCmCicsApplId.Len.CICS_APPL_ID);
		position += LCmCicsApplId.Len.CICS_APPL_ID;
		ciErrorInformation.getCiErrorInformationBytes(buffer, position);
		position += CiErrorInformation.Len.CI_ERROR_INFORMATION;
		ciTransactionSpecificData.getCiTransactionSpecificDataAsBuffer(buffer, position);
		return buffer;
	}

	public LCmCicsApplId getCiCicsApplId() {
		return ciCicsApplId;
	}

	public CiErrorInformation getCiErrorInformation() {
		return ciErrorInformation;
	}

	public CiFunction getCiFunction() {
		return ciFunction;
	}

	public CiTransactionSpecificData getCiTransactionSpecificData() {
		return ciTransactionSpecificData;
	}

	@Override
	public byte[] serialize() {
		return getCicsPipeInterfaceContractBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_PIPE_INTERFACE_CONTRACT = CiFunction.Len.CI_FUNCTION + LCmCicsApplId.Len.CICS_APPL_ID
				+ CiErrorInformation.Len.CI_ERROR_INFORMATION + CiTransactionSpecificData.Len.CI_TRANSACTION_SPECIFIC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
