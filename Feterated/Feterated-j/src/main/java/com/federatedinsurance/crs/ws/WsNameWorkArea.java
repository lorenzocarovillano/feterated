/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.federatedinsurance.crs.ws.redefines.WsNameWork;

/**Original name: WS-NAME-WORK-AREA<br>
 * Variable: WS-NAME-WORK-AREA from program CIWBNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsNameWorkArea {

	//==== PROPERTIES ====
	//Original name: WS-NAME-WORK
	private WsNameWork wsNameWork = new WsNameWork();

	//==== METHODS ====
	public WsNameWork getWsNameWork() {
		return wsNameWork;
	}
}
