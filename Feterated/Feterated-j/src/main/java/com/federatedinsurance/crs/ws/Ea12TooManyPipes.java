/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-12-TOO-MANY-PIPES<br>
 * Variable: EA-12-TOO-MANY-PIPES from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea12TooManyPipes {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-12-TOO-MANY-PIPES
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-12-TOO-MANY-PIPES-1
	private String flr2 = "TOO MANY";
	//Original name: FILLER-EA-12-TOO-MANY-PIPES-2
	private String flr3 = "PIPES USED.";
	//Original name: FILLER-EA-12-TOO-MANY-PIPES-3
	private String flr4 = "MAX ALLOWED IS";
	//Original name: EA-12-MAX-PIPES
	private String ea12MaxPipes = DefaultValues.stringVal(Len.EA12_MAX_PIPES);
	//Original name: FILLER-EA-12-TOO-MANY-PIPES-4
	private String flr5 = " PIPES.";

	//==== METHODS ====
	public String getEa12TooManyPipesFormatted() {
		return MarshalByteExt.bufferToStr(getEa12TooManyPipesBytes());
	}

	public byte[] getEa12TooManyPipesBytes() {
		byte[] buffer = new byte[Len.EA12_TOO_MANY_PIPES];
		return getEa12TooManyPipesBytes(buffer, 1);
	}

	public byte[] getEa12TooManyPipesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, ea12MaxPipes, Len.EA12_MAX_PIPES);
		position += Len.EA12_MAX_PIPES;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setEa12MaxPipes(long ea12MaxPipes) {
		this.ea12MaxPipes = PicFormatter.display("Z(2)9").format(ea12MaxPipes).toString();
	}

	public long getEa12MaxPipes() {
		return PicParser.display("Z(2)9").parseLong(this.ea12MaxPipes);
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA12_MAX_PIPES = 3;
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR3 = 13;
		public static final int FLR4 = 15;
		public static final int FLR5 = 7;
		public static final int EA12_TOO_MANY_PIPES = EA12_MAX_PIPES + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
