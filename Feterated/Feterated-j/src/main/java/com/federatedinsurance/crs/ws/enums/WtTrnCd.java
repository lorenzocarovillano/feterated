/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WT-TRN-CD<br>
 * Variable: WT-TRN-CD from copybook XZC00401<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WtTrnCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.TRN_CD);
	public static final String NP = "NP";
	public static final String RN = "RN";
	public static final String CN = "CN";
	public static final String RE = "RE";
	public static final String AJ = "AJ";
	public static final String AZ = "AZ";
	public static final String TM = "TM";
	public static final String XP = "XP";
	public static final String RV = "RV";
	public static final String AA = "AA";
	public static final String RA = "RA";

	//==== METHODS ====
	public void setTrnCd(String trnCd) {
		this.value = Functions.subString(trnCd, Len.TRN_CD);
	}

	public String getTrnCd() {
		return this.value;
	}

	public void setCn() {
		value = CN;
	}

	public void setRe() {
		value = RE;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRN_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
