/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: SQL-TIMESTAMP-FOR<br>
 * Variable: SQL-TIMESTAMP-FOR from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class SqlTimestampFor {

	//==== PROPERTIES ====
	//Original name: SQL-CCYY
	private String ccyy = DefaultValues.stringVal(Len.CCYY);
	//Original name: FILLER-SQL-TIMESTAMP-FOR
	private char flr1 = DefaultValues.CHAR_VAL;
	//Original name: SQL-MONTH
	private String month = DefaultValues.stringVal(Len.MONTH);
	//Original name: FILLER-SQL-TIMESTAMP-FOR-1
	private char flr2 = DefaultValues.CHAR_VAL;
	//Original name: SQL-DAY
	private String day = DefaultValues.stringVal(Len.DAY);
	//Original name: FILLER-SQL-TIMESTAMP-FOR-2
	private char flr3 = DefaultValues.CHAR_VAL;
	//Original name: SQL-TIME
	private String timeFld = DefaultValues.stringVal(Len.TIME_FLD);
	//Original name: FILLER-SQL-TIMESTAMP-FOR-3
	private char flr4 = DefaultValues.CHAR_VAL;
	//Original name: SQL-MSEC
	private String msec = DefaultValues.stringVal(Len.MSEC);

	//==== METHODS ====
	public void setSqlTimestamp(String sqlTimestamp) {
		int position = 1;
		byte[] buffer = getSqlTimestampForBytes();
		MarshalByte.writeString(buffer, position, sqlTimestamp, Len.SQL_TIMESTAMP);
		setSqlTimestampForBytes(buffer);
	}

	/**Original name: SQL-TIMESTAMP<br>
	 * <pre>****EXEC SQL BEGIN DECLARE SECTION END-EXEC.
	 * ****EXEC SQL END DECLARE SECTION END-EXEC.
	 * **  EXEC SQL BEGIN DECLARE SECTION END-EXEC.</pre>*/
	public String getSqlTimestamp() {
		int position = 1;
		return MarshalByte.readString(getSqlTimestampForBytes(), position, Len.SQL_TIMESTAMP);
	}

	public void setSqlTimestampForBytes(byte[] buffer) {
		setSqlTimestampForBytes(buffer, 1);
	}

	/**Original name: SQL-TIMESTAMP-FOR<br>
	 * <pre>**  EXEC SQL END DECLARE SECTION END-EXEC.</pre>*/
	public byte[] getSqlTimestampForBytes() {
		byte[] buffer = new byte[Len.SQL_TIMESTAMP_FOR];
		return getSqlTimestampForBytes(buffer, 1);
	}

	public void setSqlTimestampForBytes(byte[] buffer, int offset) {
		int position = offset;
		ccyy = MarshalByte.readFixedString(buffer, position, Len.CCYY);
		position += Len.CCYY;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		month = MarshalByte.readFixedString(buffer, position, Len.MONTH);
		position += Len.MONTH;
		flr2 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		day = MarshalByte.readFixedString(buffer, position, Len.DAY);
		position += Len.DAY;
		flr3 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		timeFld = MarshalByte.readString(buffer, position, Len.TIME_FLD);
		position += Len.TIME_FLD;
		flr4 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		msec = MarshalByte.readFixedString(buffer, position, Len.MSEC);
	}

	public byte[] getSqlTimestampForBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ccyy, Len.CCYY);
		position += Len.CCYY;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, month, Len.MONTH);
		position += Len.MONTH;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, day, Len.DAY);
		position += Len.DAY;
		MarshalByte.writeChar(buffer, position, flr3);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, timeFld, Len.TIME_FLD);
		position += Len.TIME_FLD;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, msec, Len.MSEC);
		return buffer;
	}

	public short getCcyy() {
		return NumericDisplay.asShort(this.ccyy);
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public short getMonth() {
		return NumericDisplay.asShort(this.month);
	}

	public void setFlr2(char flr2) {
		this.flr2 = flr2;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public short getDay() {
		return NumericDisplay.asShort(this.day);
	}

	public void setFlr3(char flr3) {
		this.flr3 = flr3;
	}

	public char getFlr3() {
		return this.flr3;
	}

	public void setTimeFld(String timeFld) {
		this.timeFld = Functions.subString(timeFld, Len.TIME_FLD);
	}

	public String getTimeFld() {
		return this.timeFld;
	}

	public void setFlr4(char flr4) {
		this.flr4 = flr4;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public int getMsec() {
		return NumericDisplay.asInt(this.msec);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CCYY = 4;
		public static final int MONTH = 2;
		public static final int DAY = 2;
		public static final int TIME_FLD = 8;
		public static final int MSEC = 6;
		public static final int FLR1 = 1;
		public static final int SQL_TIMESTAMP_FOR = CCYY + MONTH + DAY + TIME_FLD + MSEC + 4 * FLR1;
		public static final int SQL_TIMESTAMP = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int SQL_TIMESTAMP = 26;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
