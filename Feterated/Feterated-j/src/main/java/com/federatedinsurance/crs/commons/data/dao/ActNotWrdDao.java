/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotWrd;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_WRD]
 * 
 */
public class ActNotWrdDao extends BaseSqlDao<IActNotWrd> {

	private Cursor actNotWrdCsr;
	private final IRowMapper<IActNotWrd> fetchActNotWrdCsrRm = buildNamedRowMapper(IActNotWrd.class, "csrActNbr", "notPrcTs", "polNbr", "stWrdSeqCd");

	public ActNotWrdDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotWrd> getToClass() {
		return IActNotWrd.class;
	}

	public DbAccessStatus openActNotWrdCsr(String csrActNbr, String notPrcTs, String polNbr, String stWrdSeqCd) {
		actNotWrdCsr = buildQuery("openActNotWrdCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr)
				.bind("stWrdSeqCd", stWrdSeqCd).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotWrdCsr() {
		return closeCursor(actNotWrdCsr);
	}

	public IActNotWrd fetchActNotWrdCsr(IActNotWrd iActNotWrd) {
		return fetch(actNotWrdCsr, iActNotWrd, fetchActNotWrdCsrRm);
	}

	public DbAccessStatus insertRec(String csrActNbr, String notPrcTs, String polNbr, String stWrdSeqCd) {
		return buildQuery("insertRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("stWrdSeqCd", stWrdSeqCd)
				.executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, String polNbr, String stWrdSeqCd) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("stWrdSeqCd", stWrdSeqCd)
				.executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs, String polNbr) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).executeDelete();
	}

	public DbAccessStatus deleteRec2(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotWrd selectRec(String csrActNbr, String notPrcTs, String polNbr, String stWrdSeqCd, IActNotWrd iActNotWrd) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("stWrdSeqCd", stWrdSeqCd)
				.singleResult(iActNotWrd);
	}

	public String selectRec1(String csrActNbr, String notPrcTs, String polNbr, String stWrdSeqCd, String dft) {
		return buildQuery("selectRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("polNbr", polNbr).bind("stWrdSeqCd", stWrdSeqCd)
				.scalarResultString(dft);
	}
}
