/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

/**Original name: SW-SIGN-ON-ATTEMPTED-FLAG<br>
 * Variable: SW-SIGN-ON-ATTEMPTED-FLAG from program TS030099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SwSignOnAttemptedFlag {

	//==== PROPERTIES ====
	private char value = '0';
	public static final char NOT_ATTEMPTED = '0';
	public static final char WAS_ATTEMPTED = '1';

	//==== METHODS ====
	public void setSignOnAttemptedFlag(char signOnAttemptedFlag) {
		this.value = signOnAttemptedFlag;
	}

	public char getSignOnAttemptedFlag() {
		return this.value;
	}

	public boolean isWasAttempted() {
		return value == WAS_ATTEMPTED;
	}

	public void setWasAttempted() {
		value = WAS_ATTEMPTED;
	}
}
