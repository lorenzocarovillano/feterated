/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0X9080<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0x9080 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9080_MAXOCCURS = 2500;

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0x9080() {
	}

	public LFrameworkResponseAreaXz0x9080(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza980rMaxTtyRows(int xza980rMaxTtyRowsIdx, short xza980rMaxTtyRows) {
		int position = Pos.xza980rMaxTtyRows(xza980rMaxTtyRowsIdx - 1);
		writeBinaryShort(position, xza980rMaxTtyRows);
	}

	/**Original name: XZA980R-MAX-TTY-ROWS<br>*/
	public short getXza980rMaxTtyRows(int xza980rMaxTtyRowsIdx) {
		int position = Pos.xza980rMaxTtyRows(xza980rMaxTtyRowsIdx - 1);
		return readBinaryShort(position);
	}

	public void setXza980rCsrActNbr(int xza980rCsrActNbrIdx, String xza980rCsrActNbr) {
		int position = Pos.xza980rCsrActNbr(xza980rCsrActNbrIdx - 1);
		writeString(position, xza980rCsrActNbr, Len.XZA980R_CSR_ACT_NBR);
	}

	/**Original name: XZA980R-CSR-ACT-NBR<br>*/
	public String getXza980rCsrActNbr(int xza980rCsrActNbrIdx) {
		int position = Pos.xza980rCsrActNbr(xza980rCsrActNbrIdx - 1);
		return readString(position, Len.XZA980R_CSR_ACT_NBR);
	}

	public void setXza980rNotPrcTs(int xza980rNotPrcTsIdx, String xza980rNotPrcTs) {
		int position = Pos.xza980rNotPrcTs(xza980rNotPrcTsIdx - 1);
		writeString(position, xza980rNotPrcTs, Len.XZA980R_NOT_PRC_TS);
	}

	/**Original name: XZA980R-NOT-PRC-TS<br>*/
	public String getXza980rNotPrcTs(int xza980rNotPrcTsIdx) {
		int position = Pos.xza980rNotPrcTs(xza980rNotPrcTsIdx - 1);
		return readString(position, Len.XZA980R_NOT_PRC_TS);
	}

	public void setXza980rUserid(int xza980rUseridIdx, String xza980rUserid) {
		int position = Pos.xza980rUserid(xza980rUseridIdx - 1);
		writeString(position, xza980rUserid, Len.XZA980R_USERID);
	}

	/**Original name: XZA980R-USERID<br>*/
	public String getXza980rUserid(int xza980rUseridIdx) {
		int position = Pos.xza980rUserid(xza980rUseridIdx - 1);
		return readString(position, Len.XZA980R_USERID);
	}

	public void setXza980rClientId(int xza980rClientIdIdx, String xza980rClientId) {
		int position = Pos.xza980rClientId(xza980rClientIdIdx - 1);
		writeString(position, xza980rClientId, Len.XZA980R_CLIENT_ID);
	}

	/**Original name: XZA980R-CLIENT-ID<br>*/
	public String getXza980rClientId(int xza980rClientIdIdx) {
		int position = Pos.xza980rClientId(xza980rClientIdIdx - 1);
		return readString(position, Len.XZA980R_CLIENT_ID);
	}

	public void setXza980rAdrId(int xza980rAdrIdIdx, String xza980rAdrId) {
		int position = Pos.xza980rAdrId(xza980rAdrIdIdx - 1);
		writeString(position, xza980rAdrId, Len.XZA980R_ADR_ID);
	}

	/**Original name: XZA980R-ADR-ID<br>*/
	public String getXza980rAdrId(int xza980rAdrIdIdx) {
		int position = Pos.xza980rAdrId(xza980rAdrIdIdx - 1);
		return readString(position, Len.XZA980R_ADR_ID);
	}

	public void setXza980rRecTypCd(int xza980rRecTypCdIdx, String xza980rRecTypCd) {
		int position = Pos.xza980rRecTypCd(xza980rRecTypCdIdx - 1);
		writeString(position, xza980rRecTypCd, Len.XZA980R_REC_TYP_CD);
	}

	/**Original name: XZA980R-REC-TYP-CD<br>*/
	public String getXza980rRecTypCd(int xza980rRecTypCdIdx) {
		int position = Pos.xza980rRecTypCd(xza980rRecTypCdIdx - 1);
		return readString(position, Len.XZA980R_REC_TYP_CD);
	}

	public void setXza980rRecTypDes(int xza980rRecTypDesIdx, String xza980rRecTypDes) {
		int position = Pos.xza980rRecTypDes(xza980rRecTypDesIdx - 1);
		writeString(position, xza980rRecTypDes, Len.XZA980R_REC_TYP_DES);
	}

	/**Original name: XZA980R-REC-TYP-DES<br>*/
	public String getXza980rRecTypDes(int xza980rRecTypDesIdx) {
		int position = Pos.xza980rRecTypDes(xza980rRecTypDesIdx - 1);
		return readString(position, Len.XZA980R_REC_TYP_DES);
	}

	public void setXza980rName(int xza980rNameIdx, String xza980rName) {
		int position = Pos.xza980rName(xza980rNameIdx - 1);
		writeString(position, xza980rName, Len.XZA980R_NAME);
	}

	/**Original name: XZA980R-NAME<br>*/
	public String getXza980rName(int xza980rNameIdx) {
		int position = Pos.xza980rName(xza980rNameIdx - 1);
		return readString(position, Len.XZA980R_NAME);
	}

	public void setXza980rAdrLin1(int xza980rAdrLin1Idx, String xza980rAdrLin1) {
		int position = Pos.xza980rAdrLin1(xza980rAdrLin1Idx - 1);
		writeString(position, xza980rAdrLin1, Len.XZA980R_ADR_LIN1);
	}

	/**Original name: XZA980R-ADR-LIN1<br>*/
	public String getXza980rAdrLin1(int xza980rAdrLin1Idx) {
		int position = Pos.xza980rAdrLin1(xza980rAdrLin1Idx - 1);
		return readString(position, Len.XZA980R_ADR_LIN1);
	}

	public void setXza980rAdrLin2(int xza980rAdrLin2Idx, String xza980rAdrLin2) {
		int position = Pos.xza980rAdrLin2(xza980rAdrLin2Idx - 1);
		writeString(position, xza980rAdrLin2, Len.XZA980R_ADR_LIN2);
	}

	/**Original name: XZA980R-ADR-LIN2<br>*/
	public String getXza980rAdrLin2(int xza980rAdrLin2Idx) {
		int position = Pos.xza980rAdrLin2(xza980rAdrLin2Idx - 1);
		return readString(position, Len.XZA980R_ADR_LIN2);
	}

	public void setXza980rCityNm(int xza980rCityNmIdx, String xza980rCityNm) {
		int position = Pos.xza980rCityNm(xza980rCityNmIdx - 1);
		writeString(position, xza980rCityNm, Len.XZA980R_CITY_NM);
	}

	/**Original name: XZA980R-CITY-NM<br>*/
	public String getXza980rCityNm(int xza980rCityNmIdx) {
		int position = Pos.xza980rCityNm(xza980rCityNmIdx - 1);
		return readString(position, Len.XZA980R_CITY_NM);
	}

	public void setXza980rStateAbb(int xza980rStateAbbIdx, String xza980rStateAbb) {
		int position = Pos.xza980rStateAbb(xza980rStateAbbIdx - 1);
		writeString(position, xza980rStateAbb, Len.XZA980R_STATE_ABB);
	}

	/**Original name: XZA980R-STATE-ABB<br>*/
	public String getXza980rStateAbb(int xza980rStateAbbIdx) {
		int position = Pos.xza980rStateAbb(xza980rStateAbbIdx - 1);
		return readString(position, Len.XZA980R_STATE_ABB);
	}

	public void setXza980rPstCd(int xza980rPstCdIdx, String xza980rPstCd) {
		int position = Pos.xza980rPstCd(xza980rPstCdIdx - 1);
		writeString(position, xza980rPstCd, Len.XZA980R_PST_CD);
	}

	/**Original name: XZA980R-PST-CD<br>*/
	public String getXza980rPstCd(int xza980rPstCdIdx) {
		int position = Pos.xza980rPstCd(xza980rPstCdIdx - 1);
		return readString(position, Len.XZA980R_PST_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9080(int idx) {
			return L_FRAMEWORK_RESPONSE_AREA + idx * Len.L_FW_RESP_XZ0A9080;
		}

		public static int xza980rTtyInfoRow(int idx) {
			return lFwRespXz0a9080(idx);
		}

		public static int xza980rMaxTtyRows(int idx) {
			return xza980rTtyInfoRow(idx);
		}

		public static int xza980rCsrActNbr(int idx) {
			return xza980rMaxTtyRows(idx) + Len.XZA980R_MAX_TTY_ROWS;
		}

		public static int xza980rNotPrcTs(int idx) {
			return xza980rCsrActNbr(idx) + Len.XZA980R_CSR_ACT_NBR;
		}

		public static int xza980rUserid(int idx) {
			return xza980rNotPrcTs(idx) + Len.XZA980R_NOT_PRC_TS;
		}

		public static int xza980rTtyList(int idx) {
			return xza980rUserid(idx) + Len.XZA980R_USERID;
		}

		public static int xza980rClientId(int idx) {
			return xza980rTtyList(idx);
		}

		public static int xza980rAdrId(int idx) {
			return xza980rClientId(idx) + Len.XZA980R_CLIENT_ID;
		}

		public static int xza980rRecTypCd(int idx) {
			return xza980rAdrId(idx) + Len.XZA980R_ADR_ID;
		}

		public static int xza980rRecTypDes(int idx) {
			return xza980rRecTypCd(idx) + Len.XZA980R_REC_TYP_CD;
		}

		public static int xza980rName(int idx) {
			return xza980rRecTypDes(idx) + Len.XZA980R_REC_TYP_DES;
		}

		public static int xza980rAdrLin1(int idx) {
			return xza980rName(idx) + Len.XZA980R_NAME;
		}

		public static int xza980rAdrLin2(int idx) {
			return xza980rAdrLin1(idx) + Len.XZA980R_ADR_LIN1;
		}

		public static int xza980rCityNm(int idx) {
			return xza980rAdrLin2(idx) + Len.XZA980R_ADR_LIN2;
		}

		public static int xza980rStateAbb(int idx) {
			return xza980rCityNm(idx) + Len.XZA980R_CITY_NM;
		}

		public static int xza980rPstCd(int idx) {
			return xza980rStateAbb(idx) + Len.XZA980R_STATE_ABB;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA980R_MAX_TTY_ROWS = 2;
		public static final int XZA980R_CSR_ACT_NBR = 9;
		public static final int XZA980R_NOT_PRC_TS = 26;
		public static final int XZA980R_USERID = 8;
		public static final int XZA980R_CLIENT_ID = 64;
		public static final int XZA980R_ADR_ID = 64;
		public static final int XZA980R_REC_TYP_CD = 5;
		public static final int XZA980R_REC_TYP_DES = 13;
		public static final int XZA980R_NAME = 120;
		public static final int XZA980R_ADR_LIN1 = 45;
		public static final int XZA980R_ADR_LIN2 = 45;
		public static final int XZA980R_CITY_NM = 30;
		public static final int XZA980R_STATE_ABB = 2;
		public static final int XZA980R_PST_CD = 13;
		public static final int XZA980R_TTY_LIST = XZA980R_CLIENT_ID + XZA980R_ADR_ID + XZA980R_REC_TYP_CD + XZA980R_REC_TYP_DES + XZA980R_NAME
				+ XZA980R_ADR_LIN1 + XZA980R_ADR_LIN2 + XZA980R_CITY_NM + XZA980R_STATE_ABB + XZA980R_PST_CD;
		public static final int XZA980R_TTY_INFO_ROW = XZA980R_MAX_TTY_ROWS + XZA980R_CSR_ACT_NBR + XZA980R_NOT_PRC_TS + XZA980R_USERID
				+ XZA980R_TTY_LIST;
		public static final int L_FW_RESP_XZ0A9080 = XZA980R_TTY_INFO_ROW;
		public static final int L_FRAMEWORK_RESPONSE_AREA = LFrameworkResponseAreaXz0x9080.L_FW_RESP_XZ0A9080_MAXOCCURS * L_FW_RESP_XZ0A9080;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
