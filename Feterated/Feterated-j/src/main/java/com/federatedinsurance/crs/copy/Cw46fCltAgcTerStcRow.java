/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW46F-CLT-AGC-TER-STC-ROW<br>
 * Variable: CW46F-CLT-AGC-TER-STC-ROW from copybook CAWLF046<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw46fCltAgcTerStcRow {

	//==== PROPERTIES ====
	//Original name: CW46F-CLT-AGC-TER-STC-CSUM
	private String cltAgcTerStcCsum = DefaultValues.stringVal(Len.CLT_AGC_TER_STC_CSUM);
	//Original name: CW46F-TER-CLT-ID-KCRE
	private String terCltIdKcre = DefaultValues.stringVal(Len.TER_CLT_ID_KCRE);
	//Original name: CW46F-TER-LEVEL-CD-KCRE
	private String terLevelCdKcre = DefaultValues.stringVal(Len.TER_LEVEL_CD_KCRE);
	//Original name: CW46F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW46F-TER-CLT-ID
	private String terCltId = DefaultValues.stringVal(Len.TER_CLT_ID);
	//Original name: CW46F-TER-LEVEL-CD
	private String terLevelCd = DefaultValues.stringVal(Len.TER_LEVEL_CD);
	//Original name: CW46F-TER-CLT-ID-CI
	private char terCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-TER-LEVEL-CD-CI
	private char terLevelCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW46F-CLT-AGC-TER-STC-DATA
	private Cw46fCltAgcTerStcData cltAgcTerStcData = new Cw46fCltAgcTerStcData();

	//==== METHODS ====
	public void setCw46fCltAgcTerStcRowFormatted(String data) {
		byte[] buffer = new byte[Len.CW46F_CLT_AGC_TER_STC_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CW46F_CLT_AGC_TER_STC_ROW);
		setCw46fCltAgcTerStcRowBytes(buffer, 1);
	}

	public String getCw46fCltAgcTerStcRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw46fCltAgcTerStcRowBytes());
	}

	public byte[] getCw46fCltAgcTerStcRowBytes() {
		byte[] buffer = new byte[Len.CW46F_CLT_AGC_TER_STC_ROW];
		return getCw46fCltAgcTerStcRowBytes(buffer, 1);
	}

	public void setCw46fCltAgcTerStcRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setCltAgcTerStcFixedBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_FIXED;
		setCltAgcTerStcDatesBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_DATES;
		setCltAgcTerStcKeyBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_KEY;
		setCltAgcTerStcKeyCiBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_KEY_CI;
		cltAgcTerStcData.setCltAgcTerStcDataBytes(buffer, position);
	}

	public byte[] getCw46fCltAgcTerStcRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getCltAgcTerStcFixedBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_FIXED;
		getCltAgcTerStcDatesBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_DATES;
		getCltAgcTerStcKeyBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_KEY;
		getCltAgcTerStcKeyCiBytes(buffer, position);
		position += Len.CLT_AGC_TER_STC_KEY_CI;
		cltAgcTerStcData.getCltAgcTerStcDataBytes(buffer, position);
		return buffer;
	}

	public void setCltAgcTerStcFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		cltAgcTerStcCsum = MarshalByte.readFixedString(buffer, position, Len.CLT_AGC_TER_STC_CSUM);
		position += Len.CLT_AGC_TER_STC_CSUM;
		terCltIdKcre = MarshalByte.readString(buffer, position, Len.TER_CLT_ID_KCRE);
		position += Len.TER_CLT_ID_KCRE;
		terLevelCdKcre = MarshalByte.readString(buffer, position, Len.TER_LEVEL_CD_KCRE);
	}

	public byte[] getCltAgcTerStcFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cltAgcTerStcCsum, Len.CLT_AGC_TER_STC_CSUM);
		position += Len.CLT_AGC_TER_STC_CSUM;
		MarshalByte.writeString(buffer, position, terCltIdKcre, Len.TER_CLT_ID_KCRE);
		position += Len.TER_CLT_ID_KCRE;
		MarshalByte.writeString(buffer, position, terLevelCdKcre, Len.TER_LEVEL_CD_KCRE);
		return buffer;
	}

	public void setTerCltIdKcre(String terCltIdKcre) {
		this.terCltIdKcre = Functions.subString(terCltIdKcre, Len.TER_CLT_ID_KCRE);
	}

	public String getTerCltIdKcre() {
		return this.terCltIdKcre;
	}

	public void setTerLevelCdKcre(String terLevelCdKcre) {
		this.terLevelCdKcre = Functions.subString(terLevelCdKcre, Len.TER_LEVEL_CD_KCRE);
	}

	public String getTerLevelCdKcre() {
		return this.terLevelCdKcre;
	}

	public void setCltAgcTerStcDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getCltAgcTerStcDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setCltAgcTerStcKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		terCltId = MarshalByte.readString(buffer, position, Len.TER_CLT_ID);
		position += Len.TER_CLT_ID;
		terLevelCd = MarshalByte.readString(buffer, position, Len.TER_LEVEL_CD);
	}

	public byte[] getCltAgcTerStcKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, terCltId, Len.TER_CLT_ID);
		position += Len.TER_CLT_ID;
		MarshalByte.writeString(buffer, position, terLevelCd, Len.TER_LEVEL_CD);
		return buffer;
	}

	public void setTerCltId(String terCltId) {
		this.terCltId = Functions.subString(terCltId, Len.TER_CLT_ID);
	}

	public String getTerCltId() {
		return this.terCltId;
	}

	public void setTerLevelCd(String terLevelCd) {
		this.terLevelCd = Functions.subString(terLevelCd, Len.TER_LEVEL_CD);
	}

	public String getTerLevelCd() {
		return this.terLevelCd;
	}

	public void setCltAgcTerStcKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		terCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terLevelCdCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getCltAgcTerStcKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, terCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terLevelCdCi);
		return buffer;
	}

	public void setTerCltIdCi(char terCltIdCi) {
		this.terCltIdCi = terCltIdCi;
	}

	public char getTerCltIdCi() {
		return this.terCltIdCi;
	}

	public void setTerLevelCdCi(char terLevelCdCi) {
		this.terLevelCdCi = terLevelCdCi;
	}

	public char getTerLevelCdCi() {
		return this.terLevelCdCi;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLT_AGC_TER_STC_CSUM = 9;
		public static final int TER_CLT_ID_KCRE = 32;
		public static final int TER_LEVEL_CD_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int TER_CLT_ID = 20;
		public static final int TER_LEVEL_CD = 4;
		public static final int CLT_AGC_TER_STC_FIXED = CLT_AGC_TER_STC_CSUM + TER_CLT_ID_KCRE + TER_LEVEL_CD_KCRE;
		public static final int CLT_AGC_TER_STC_DATES = TRANS_PROCESS_DT;
		public static final int CLT_AGC_TER_STC_KEY = TER_CLT_ID + TER_LEVEL_CD;
		public static final int TER_CLT_ID_CI = 1;
		public static final int TER_LEVEL_CD_CI = 1;
		public static final int CLT_AGC_TER_STC_KEY_CI = TER_CLT_ID_CI + TER_LEVEL_CD_CI;
		public static final int CW46F_CLT_AGC_TER_STC_ROW = CLT_AGC_TER_STC_FIXED + CLT_AGC_TER_STC_DATES + CLT_AGC_TER_STC_KEY
				+ CLT_AGC_TER_STC_KEY_CI + Cw46fCltAgcTerStcData.Len.CLT_AGC_TER_STC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
