/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-NAME-PARSED-SW<br>
 * Variable: WS-NAME-PARSED-SW from program CIWBNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsNameParsedSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char DONE = '1';
	public static final char NOT_DONE = '0';

	//==== METHODS ====
	public void setWsNameParsedSw(char wsNameParsedSw) {
		this.value = wsNameParsedSw;
	}

	public char getWsNameParsedSw() {
		return this.value;
	}

	public boolean isDone() {
		return value == DONE;
	}

	public void setDone() {
		value = DONE;
	}

	public void setNotDone() {
		value = NOT_DONE;
	}
}
