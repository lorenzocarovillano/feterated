/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-64-USR-CICS-SERVER-ERROR<br>
 * Variable: EA-64-USR-CICS-SERVER-ERROR from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea64UsrCicsServerError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-64-USR-CICS-SERVER-ERROR
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-64-USR-CICS-SERVER-ERROR-1
	private String flr2 = "MESSAGE FROM";
	//Original name: FILLER-EA-64-USR-CICS-SERVER-ERROR-2
	private String flr3 = "SERVER:";
	//Original name: FILLER-EA-64-SERVER-MESSAGE
	private String flr4 = DefaultValues.stringVal(Len.FLR4);
	//Original name: FILLER-EA-64-SERVER-MESSAGE-1
	private String flr5 = DefaultValues.stringVal(Len.FLR5);
	public static final String MSG_CUT_OFF = "...";

	//==== METHODS ====
	public String getEa64UsrCicsServerErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa64UsrCicsServerErrorBytes());
	}

	public byte[] getEa64UsrCicsServerErrorBytes() {
		byte[] buffer = new byte[Len.EA64_USR_CICS_SERVER_ERROR];
		return getEa64UsrCicsServerErrorBytes(buffer, 1);
	}

	public byte[] getEa64UsrCicsServerErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		getMessageBytes(buffer, position);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setMessageFormatted(String data) {
		byte[] buffer = new byte[Len.MESSAGE];
		MarshalByte.writeString(buffer, 1, data, Len.MESSAGE);
		setMessageBytes(buffer, 1);
	}

	public void setMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		flr4 = MarshalByte.readString(buffer, position, Len.FLR4);
		position += Len.FLR4;
		flr5 = MarshalByte.readString(buffer, position, Len.FLR5);
	}

	public byte[] getMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public void setFlr4(String flr4) {
		this.flr4 = Functions.subString(flr4, Len.FLR4);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setFlr5(String flr5) {
		this.flr5 = Functions.subString(flr5, Len.FLR5);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setMsgCutOff() {
		flr5 = MSG_CUT_OFF;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR4 = 96;
		public static final int FLR5 = 3;
		public static final int MESSAGE = FLR4 + FLR5;
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 8;
		public static final int EA64_USR_CICS_SERVER_ERROR = MESSAGE + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
