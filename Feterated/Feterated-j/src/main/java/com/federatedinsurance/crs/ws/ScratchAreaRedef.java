/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.PositionInProcess;

/**Original name: SCRATCH-AREA-REDEF<br>
 * Variable: SCRATCH-AREA-REDEF from program TS020100<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class ScratchAreaRedef {

	//==== PROPERTIES ====
	//Original name: POSITION-IN-PROCESS
	private PositionInProcess positionInProcess = new PositionInProcess();
	//Original name: DATE-EXTENDED-ERROR
	private String dateExtendedError = DefaultValues.stringVal(Len.DATE_EXTENDED_ERROR);
	//Original name: FILLER-SCRATCH-AREA-REDEF
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setScratchAreaRedefBytes(byte[] buffer, int offset) {
		int position = offset;
		positionInProcess.setPositionInProcess(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		dateExtendedError = MarshalByte.readString(buffer, position, Len.DATE_EXTENDED_ERROR);
		position += Len.DATE_EXTENDED_ERROR;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getScratchAreaRedefBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, positionInProcess.getPositionInProcess());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateExtendedError, Len.DATE_EXTENDED_ERROR);
		position += Len.DATE_EXTENDED_ERROR;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void initScratchAreaRedefSpaces() {
		positionInProcess.setPositionInProcess(Types.SPACE_CHAR);
		dateExtendedError = "";
		flr1 = "";
	}

	public void setDateExtendedError(String dateExtendedError) {
		this.dateExtendedError = Functions.subString(dateExtendedError, Len.DATE_EXTENDED_ERROR);
	}

	public String getDateExtendedError() {
		return this.dateExtendedError;
	}

	public String getDateExtendedErrorFormatted() {
		return Functions.padBlanks(getDateExtendedError(), Len.DATE_EXTENDED_ERROR);
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_EXTENDED_ERROR = 4;
		public static final int FLR1 = 295;
		public static final int SCRATCH_AREA_REDEF = PositionInProcess.Len.POSITION_IN_PROCESS + DATE_EXTENDED_ERROR + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
