/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: SUBSCRIPTS<br>
 * Variable: SUBSCRIPTS from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SubscriptsMu0x0004 {

	//==== PROPERTIES ====
	//Original name: SS-PHN-IDX
	private short phnIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short PHN_IDX_MAX = ((short) 5);
	//Original name: SS-ADR-IDX
	private short adrIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short ADR_IDX_MAX = ((short) 3);
	//Original name: SS-ACT-IDX
	private short actIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short ACT_IDX_MAX = ((short) 50);
	/**Original name: SS-PRI-CD-IDX<br>
	 * <pre>                        VALUE +3.</pre>*/
	private short priCdIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short PRI_CD_IDX_MAX = ((short) 6);
	/**Original name: SS-SE-SIC-IDX<br>
	 * <pre>                        VALUE +2.</pre>*/
	private short seSicIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SE_SIC_IDX_MAX = ((short) 4);
	/**Original name: SS-CLT-RLT-IDX<br>
	 * <pre>**  CHANGED NAME FROM SS-UW-IDX TO REPRESENT USAGE BETTER.</pre>*/
	private short cltRltIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short CLT_RLT_IDX_MAX = ((short) 7);

	//==== METHODS ====
	public void setPhnIdx(short phnIdx) {
		this.phnIdx = phnIdx;
	}

	public short getPhnIdx() {
		return this.phnIdx;
	}

	public boolean isPhnIdxMax() {
		return phnIdx == PHN_IDX_MAX;
	}

	public void setAdrIdx(short adrIdx) {
		this.adrIdx = adrIdx;
	}

	public short getAdrIdx() {
		return this.adrIdx;
	}

	public boolean isAdrIdxMax() {
		return adrIdx == ADR_IDX_MAX;
	}

	public void setActIdx(short actIdx) {
		this.actIdx = actIdx;
	}

	public short getActIdx() {
		return this.actIdx;
	}

	public boolean isActIdxMax() {
		return actIdx == ACT_IDX_MAX;
	}

	public void setPriCdIdx(short priCdIdx) {
		this.priCdIdx = priCdIdx;
	}

	public short getPriCdIdx() {
		return this.priCdIdx;
	}

	public boolean isPriCdIdxMax() {
		return priCdIdx == PRI_CD_IDX_MAX;
	}

	public void setSeSicIdx(short seSicIdx) {
		this.seSicIdx = seSicIdx;
	}

	public short getSeSicIdx() {
		return this.seSicIdx;
	}

	public boolean isSeSicIdxMax() {
		return seSicIdx == SE_SIC_IDX_MAX;
	}

	public void setCltRltIdx(short cltRltIdx) {
		this.cltRltIdx = cltRltIdx;
	}

	public short getCltRltIdx() {
		return this.cltRltIdx;
	}

	public boolean isCltRltIdxMax() {
		return cltRltIdx == CLT_RLT_IDX_MAX;
	}
}
