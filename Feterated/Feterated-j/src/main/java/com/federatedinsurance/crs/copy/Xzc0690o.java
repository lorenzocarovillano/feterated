/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.PioPolTrm;

/**Original name: XZC0690O<br>
 * Variable: XZC0690O from copybook XZC0690O<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc0690o {

	//==== PROPERTIES ====
	public static final int POL_TRM_MAXOCCURS = 351;
	public static final int ERR_MSG_MAXOCCURS = 10;
	public static final int WNG_MSG_MAXOCCURS = 10;
	//Original name: PIO-RET-CODE
	private Xz08coRetCd retCode = new Xz08coRetCd();
	//Original name: PIO-ERR-MSG
	private String[] errMsg = new String[ERR_MSG_MAXOCCURS];
	//Original name: PIO-WNG-MSG
	private String[] wngMsg = new String[WNG_MSG_MAXOCCURS];
	//Original name: PIO-POL-TRM
	private PioPolTrm[] polTrm = new PioPolTrm[POL_TRM_MAXOCCURS];
	//Original name: PIO-ERROR-INDICATOR
	private String errorIndicator = DefaultValues.stringVal(Len.ERROR_INDICATOR);
	//Original name: PIO-FAULT-CODE
	private String faultCode = DefaultValues.stringVal(Len.FAULT_CODE);
	//Original name: PIO-FAULT-STRING
	private String faultString = DefaultValues.stringVal(Len.FAULT_STRING);
	//Original name: PIO-FAULT-ACTOR
	private String faultActor = DefaultValues.stringVal(Len.FAULT_ACTOR);
	//Original name: PIO-FAULT-DETAIL
	private String faultDetail = DefaultValues.stringVal(Len.FAULT_DETAIL);
	//Original name: PIO-NON-SOAP-FAULT-ERR-TXT
	private String nonSoapFaultErrTxt = DefaultValues.stringVal(Len.NON_SOAP_FAULT_ERR_TXT);

	//==== CONSTRUCTORS ====
	public Xzc0690o() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int errMsgIdx = 1; errMsgIdx <= ERR_MSG_MAXOCCURS; errMsgIdx++) {
			setErrMsg(errMsgIdx, DefaultValues.stringVal(Len.ERR_MSG));
		}
		for (int wngMsgIdx = 1; wngMsgIdx <= WNG_MSG_MAXOCCURS; wngMsgIdx++) {
			setWngMsg(wngMsgIdx, DefaultValues.stringVal(Len.WNG_MSG));
		}
		for (int polTrmIdx = 1; polTrmIdx <= POL_TRM_MAXOCCURS; polTrmIdx++) {
			polTrm[polTrmIdx - 1] = new PioPolTrm();
		}
	}

	public void setGetPolTrmLisByActBytes(byte[] buffer, int offset) {
		int position = offset;
		setReturnMessageBytes(buffer, position);
		position += Len.RETURN_MESSAGE;
		setPolTrmLisBytes(buffer, position);
	}

	public byte[] getGetPolTrmLisByActBytes(byte[] buffer, int offset) {
		int position = offset;
		getReturnMessageBytes(buffer, position);
		position += Len.RETURN_MESSAGE;
		getPolTrmLisBytes(buffer, position);
		return buffer;
	}

	public void setReturnMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		retCode.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		setErrLisBytes(buffer, position);
		position += Len.ERR_LIS;
		setWngLisBytes(buffer, position);
	}

	public byte[] getReturnMessageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeShort(buffer, position, retCode.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		getErrLisBytes(buffer, position);
		position += Len.ERR_LIS;
		getWngLisBytes(buffer, position);
		return buffer;
	}

	public void setErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setErrMsg(idx, MarshalByte.readString(buffer, position, Len.ERR_MSG));
				position += Len.ERR_MSG;
			} else {
				setErrMsg(idx, "");
				position += Len.ERR_MSG;
			}
		}
	}

	public byte[] getErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getErrMsg(idx), Len.ERR_MSG);
			position += Len.ERR_MSG;
		}
		return buffer;
	}

	public void setErrMsg(int errMsgIdx, String errMsg) {
		this.errMsg[errMsgIdx - 1] = Functions.subString(errMsg, Len.ERR_MSG);
	}

	public String getErrMsg(int errMsgIdx) {
		return this.errMsg[errMsgIdx - 1];
	}

	public void setWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setWngMsg(idx, MarshalByte.readString(buffer, position, Len.WNG_MSG));
				position += Len.WNG_MSG;
			} else {
				setWngMsg(idx, "");
				position += Len.WNG_MSG;
			}
		}
	}

	public byte[] getWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getWngMsg(idx), Len.WNG_MSG);
			position += Len.WNG_MSG;
		}
		return buffer;
	}

	public void setWngMsg(int wngMsgIdx, String wngMsg) {
		this.wngMsg[wngMsgIdx - 1] = Functions.subString(wngMsg, Len.WNG_MSG);
	}

	public String getWngMsg(int wngMsgIdx) {
		return this.wngMsg[wngMsgIdx - 1];
	}

	public void setPolTrmLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_TRM_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				polTrm[idx - 1].setPolTrmBytes(buffer, position);
				position += PioPolTrm.Len.POL_TRM;
			} else {
				polTrm[idx - 1].initPolTrmSpaces();
				position += PioPolTrm.Len.POL_TRM;
			}
		}
	}

	public byte[] getPolTrmLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_TRM_MAXOCCURS; idx++) {
			polTrm[idx - 1].getPolTrmBytes(buffer, position);
			position += PioPolTrm.Len.POL_TRM;
		}
		return buffer;
	}

	public void setErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		errorIndicator = MarshalByte.readString(buffer, position, Len.ERROR_INDICATOR);
		position += Len.ERROR_INDICATOR;
		faultCode = MarshalByte.readString(buffer, position, Len.FAULT_CODE);
		position += Len.FAULT_CODE;
		faultString = MarshalByte.readString(buffer, position, Len.FAULT_STRING);
		position += Len.FAULT_STRING;
		faultActor = MarshalByte.readString(buffer, position, Len.FAULT_ACTOR);
		position += Len.FAULT_ACTOR;
		faultDetail = MarshalByte.readString(buffer, position, Len.FAULT_DETAIL);
		position += Len.FAULT_DETAIL;
		nonSoapFaultErrTxt = MarshalByte.readString(buffer, position, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public byte[] getErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorIndicator, Len.ERROR_INDICATOR);
		position += Len.ERROR_INDICATOR;
		MarshalByte.writeString(buffer, position, faultCode, Len.FAULT_CODE);
		position += Len.FAULT_CODE;
		MarshalByte.writeString(buffer, position, faultString, Len.FAULT_STRING);
		position += Len.FAULT_STRING;
		MarshalByte.writeString(buffer, position, faultActor, Len.FAULT_ACTOR);
		position += Len.FAULT_ACTOR;
		MarshalByte.writeString(buffer, position, faultDetail, Len.FAULT_DETAIL);
		position += Len.FAULT_DETAIL;
		MarshalByte.writeString(buffer, position, nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
		return buffer;
	}

	public void setErrorIndicator(String errorIndicator) {
		this.errorIndicator = Functions.subString(errorIndicator, Len.ERROR_INDICATOR);
	}

	public String getErrorIndicator() {
		return this.errorIndicator;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = Functions.subString(faultCode, Len.FAULT_CODE);
	}

	public String getFaultCode() {
		return this.faultCode;
	}

	public void setFaultString(String faultString) {
		this.faultString = Functions.subString(faultString, Len.FAULT_STRING);
	}

	public String getFaultString() {
		return this.faultString;
	}

	public void setFaultActor(String faultActor) {
		this.faultActor = Functions.subString(faultActor, Len.FAULT_ACTOR);
	}

	public String getFaultActor() {
		return this.faultActor;
	}

	public void setFaultDetail(String faultDetail) {
		this.faultDetail = Functions.subString(faultDetail, Len.FAULT_DETAIL);
	}

	public String getFaultDetail() {
		return this.faultDetail;
	}

	public void setNonSoapFaultErrTxt(String nonSoapFaultErrTxt) {
		this.nonSoapFaultErrTxt = Functions.subString(nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public String getNonSoapFaultErrTxt() {
		return this.nonSoapFaultErrTxt;
	}

	public PioPolTrm getPolTrm(int idx) {
		return polTrm[idx - 1];
	}

	public Xz08coRetCd getRetCode() {
		return retCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERR_MSG = 500;
		public static final int WNG_MSG = 500;
		public static final int ERROR_INDICATOR = 30;
		public static final int FAULT_CODE = 30;
		public static final int FAULT_STRING = 256;
		public static final int FAULT_ACTOR = 256;
		public static final int FAULT_DETAIL = 256;
		public static final int NON_SOAP_FAULT_ERR_TXT = 256;
		public static final int ERR_LIS = Xzc0690o.ERR_MSG_MAXOCCURS * ERR_MSG;
		public static final int WNG_LIS = Xzc0690o.WNG_MSG_MAXOCCURS * WNG_MSG;
		public static final int RETURN_MESSAGE = Xz08coRetCd.Len.XZ08CO_RET_CD + ERR_LIS + WNG_LIS;
		public static final int POL_TRM_LIS = Xzc0690o.POL_TRM_MAXOCCURS * PioPolTrm.Len.POL_TRM;
		public static final int GET_POL_TRM_LIS_BY_ACT = RETURN_MESSAGE + POL_TRM_LIS;
		public static final int ERROR_INFORMATION = ERROR_INDICATOR + FAULT_CODE + FAULT_STRING + FAULT_ACTOR + FAULT_DETAIL + NON_SOAP_FAULT_ERR_TXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
