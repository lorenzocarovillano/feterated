/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallresp;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS020200<br>
 * Generated as a class for rule WS.<br>*/
public class Ts020200Data {

	//==== PROPERTIES ====
	//Original name: WS-PROGRAM-NAME
	private String wsProgramName = "TS020200";
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLRESP
	private Hallresp hallresp = new Hallresp();
	//Original name: RESPONSE-DATA-BUFFER
	private String responseDataBuffer = DefaultValues.stringVal(Len.RESPONSE_DATA_BUFFER);
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public String getWsProgramName() {
		return this.wsProgramName;
	}

	public String getWsProgramNameFormatted() {
		return Functions.padBlanks(getWsProgramName(), Len.WS_PROGRAM_NAME);
	}

	public void setResponseUmtModuleDataFormatted(String data) {
		byte[] buffer = new byte[Len.RESPONSE_UMT_MODULE_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.RESPONSE_UMT_MODULE_DATA);
		setResponseUmtModuleDataBytes(buffer, 1);
	}

	public String getResponseUmtModuleDataFormatted() {
		return MarshalByteExt.bufferToStr(getResponseUmtModuleDataBytes());
	}

	/**Original name: RESPONSE-UMT-MODULE-DATA<br>
	 * <pre>** ROUTINE TO READ UDAT.</pre>*/
	public byte[] getResponseUmtModuleDataBytes() {
		byte[] buffer = new byte[Len.RESPONSE_UMT_MODULE_DATA];
		return getResponseUmtModuleDataBytes(buffer, 1);
	}

	public void setResponseUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallresp.setConstantsBytes(buffer, position);
		position += Hallresp.Len.CONSTANTS;
		hallresp.setInputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_LINKAGE;
		hallresp.setOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.setInputOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		responseDataBuffer = MarshalByte.readString(buffer, position, Len.RESPONSE_DATA_BUFFER);
	}

	public byte[] getResponseUmtModuleDataBytes(byte[] buffer, int offset) {
		int position = offset;
		hallresp.getConstantsBytes(buffer, position);
		position += Hallresp.Len.CONSTANTS;
		hallresp.getInputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_LINKAGE;
		hallresp.getOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.OUTPUT_LINKAGE;
		hallresp.getInputOutputLinkageBytes(buffer, position);
		position += Hallresp.Len.INPUT_OUTPUT_LINKAGE;
		MarshalByte.writeString(buffer, position, responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
		return buffer;
	}

	public void setResponseDataBuffer(String responseDataBuffer) {
		this.responseDataBuffer = Functions.subString(responseDataBuffer, Len.RESPONSE_DATA_BUFFER);
	}

	public String getResponseDataBuffer() {
		return this.responseDataBuffer;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallresp getHallresp() {
		return hallresp;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESPONSE_DATA_BUFFER = 5000;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int RESPONSE_UMT_MODULE_DATA = Hallresp.Len.CONSTANTS + Hallresp.Len.INPUT_LINKAGE + Hallresp.Len.OUTPUT_LINKAGE
				+ Hallresp.Len.INPUT_OUTPUT_LINKAGE + RESPONSE_DATA_BUFFER;
		public static final int WS_PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
