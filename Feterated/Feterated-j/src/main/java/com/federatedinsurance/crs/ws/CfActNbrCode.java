/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-ACT-NBR-CODE<br>
 * Variable: CF-ACT-NBR-CODE from program MU0X0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfActNbrCode {

	//==== PROPERTIES ====
	//Original name: CF-AN-PROSPECT-ACCOUNT
	private String prospectAccount = "PSAC";
	//Original name: CF-AN-PANDC-ACCOUNT
	private String pandcAccount = "PCAC";
	//Original name: CF-AN-SELF-INSURED-ACT
	private String selfInsuredAct = "SIAC";
	//Original name: CF-AN-SPLIT-BILLED-ACT
	private String splitBilledAct = "SPAC";
	//Original name: CF-AN-PRS-LINES-ACT
	private String prsLinesAct = "PLAC";

	//==== METHODS ====
	public String getProspectAccount() {
		return this.prospectAccount;
	}

	public String getPandcAccount() {
		return this.pandcAccount;
	}

	public String getSelfInsuredAct() {
		return this.selfInsuredAct;
	}

	public String getSplitBilledAct() {
		return this.splitBilledAct;
	}

	public String getPrsLinesAct() {
		return this.prsLinesAct;
	}
}
