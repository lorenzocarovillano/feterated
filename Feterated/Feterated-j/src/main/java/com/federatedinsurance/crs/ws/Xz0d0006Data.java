/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNotFrm;
import com.federatedinsurance.crs.copy.DclhalBoAudTgrV;
import com.federatedinsurance.crs.copy.DclhalBoMduXrfV;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclhalUowObjHierV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Halluchs;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Halluidg;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0n0006;
import com.federatedinsurance.crs.copy.Xzc004ActNotPolFrmRow;
import com.federatedinsurance.crs.copy.Xzc006ActNotFrmRow;
import com.federatedinsurance.crs.copy.Xzc007ActNotFrmRecRow;
import com.federatedinsurance.crs.copy.Xzh001ActNotRow;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0D0006<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0d0006Data {

	//==== PROPERTIES ====
	/**Original name: NI-FRM-SEQ-NBR<br>
	 * <pre>****************************************************************
	 * **CSC *  START OF:                           BUSINESS FRAMEWORK
	 * **CSC *                                      BUSINESS FRAMEWORK
	 * **CSC *  WORKING-STORAGE SPECIFIC            BUSINESS FRAMEWORK
	 * **CSC *  TO THIS MODULE                      BUSINESS FRAMEWORK
	 * ****************************************************************
	 *  NULL INDICATORS</pre>*/
	private short niFrmSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-SPECIFIC-MISC
	private WsSpecificMiscXz0d0006 wsSpecificMisc = new WsSpecificMiscXz0d0006();
	//Original name: XZC006-ACT-NOT-FRM-ROW
	private Xzc006ActNotFrmRow xzc006ActNotFrmRow = new Xzc006ActNotFrmRow();
	//Original name: XZC004-ACT-NOT-POL-FRM-ROW
	private Xzc004ActNotPolFrmRow xzc004ActNotPolFrmRow = new Xzc004ActNotPolFrmRow();
	//Original name: XZC007-ACT-NOT-FRM-REC-ROW
	private Xzc007ActNotFrmRecRow xzc007ActNotFrmRecRow = new Xzc007ActNotFrmRecRow();
	//Original name: XZ0N0006
	private Xz0n0006 xz0n0006 = new Xz0n0006();
	//Original name: XZH006-ACT-NOT-FRM-ROW
	private DclactNotFrm xzh006ActNotFrmRow = new DclactNotFrm();
	//Original name: XZH001-ACT-NOT-ROW
	private Xzh001ActNotRow xzh001ActNotRow = new Xzh001ActNotRow();
	//Original name: WS-BDO-WORK-FIELDS
	private WsBdoWorkFields wsBdoWorkFields = new WsBdoWorkFields();
	//Original name: WS-BDO-SWITCHES
	private WsBdoSwitches wsBdoSwitches = new WsBdoSwitches();
	//Original name: DCLHAL-BO-AUD-TGR-V
	private DclhalBoAudTgrV dclhalBoAudTgrV = new DclhalBoAudTgrV();
	//Original name: HALLUCHS
	private Halluchs halluchs = new Halluchs();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: HALRLODR-LOCK-DRIVER-STORAGE
	private WsHalrlodrLinkage halrlodrLockDriverStorage = new WsHalrlodrLinkage();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: HALLUSW
	private Hallusw hallusw = new Hallusw();
	//Original name: HALLUDAT
	private Halludat halludat = new Halludat();
	//Original name: HALLUHDR
	private Halluhdr halluhdr = new Halluhdr();
	//Original name: CIDP-TABLE-INFO
	private CidpTableInfo cidpTableInfo = new CidpTableInfo();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALRMON-PERF-MONITOR-STORAGE
	private HalrmonPerfMonitorStorage halrmonPerfMonitorStorage = new HalrmonPerfMonitorStorage();
	//Original name: DATE-STRUCTURE
	private DateStructureCawpcorc dateStructure = new DateStructureCawpcorc();
	//Original name: WS-SE3-CUR-ISO-DATE
	private String wsSe3CurIsoDate = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_DATE);
	//Original name: WS-SE3-CUR-ISO-TIME
	private String wsSe3CurIsoTime = DefaultValues.stringVal(Len.WS_SE3_CUR_ISO_TIME);
	//Original name: DCLHAL-UOW-OBJ-HIER-V
	private DclhalUowObjHierV dclhalUowObjHierV = new DclhalUowObjHierV();
	//Original name: DCLHAL-BO-MDU-XRF-V
	private DclhalBoMduXrfV dclhalBoMduXrfV = new DclhalBoMduXrfV();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public short getNiFrmSeqNbr() {
		return this.niFrmSeqNbr;
	}

	public String getWsActNotPolFrmCopybookFormatted() {
		return xzc004ActNotPolFrmRow.getXzc004ActNotPolFrmRowFormatted();
	}

	public String getWsActNotFrmRecCopybookFormatted() {
		return xzc007ActNotFrmRecRow.getXzc007ActNotFrmRecRowFormatted();
	}

	public void setHalouchsLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.HALOUCHS_LINKAGE);
		setHalouchsLinkageBytes(buffer, 1);
	}

	public String getHalouchsLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHalouchsLinkageBytes());
	}

	/**Original name: HALOUCHS-LINKAGE<br>*/
	public byte[] getHalouchsLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUCHS_LINKAGE];
		return getHalouchsLinkageBytes(buffer, 1);
	}

	public void setHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluchs.setLength2(MarshalByte.readBinaryShort(buffer, position));
		position += Types.SHORT_SIZE;
		halluchs.setStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		halluchs.checkSum = MarshalByte.readFixedString(buffer, position, Halluchs.Len.CHECK_SUM);
	}

	public byte[] getHalouchsLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, halluchs.getLength2());
		position += Types.SHORT_SIZE;
		halluchs.getStringFldBytes(buffer, position);
		position += Halluchs.Len.STRING_FLD;
		MarshalByte.writeString(buffer, position, halluchs.checkSum, Halluchs.Len.CHECK_SUM);
		return buffer;
	}

	public String getHaloukrpLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getHaloukrpLinkageBytes());
	}

	/**Original name: HALOUKRP-LINKAGE<br>
	 * <pre>*         07 FILLER                       PIC  X(4).</pre>*/
	public byte[] getHaloukrpLinkageBytes() {
		byte[] buffer = new byte[Len.HALOUKRP_LINKAGE];
		return getHaloukrpLinkageBytes(buffer, 1);
	}

	public byte[] getHaloukrpLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallukrp.getInputFieldsBytes(buffer, position);
		position += Hallukrp.Len.INPUT_FIELDS;
		hallukrp.getInputOutputFieldsBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-DATA-UMT-AREA<br>
	 * <pre>* DATA RESPONSE UMT AREA</pre>*/
	public byte[] getWsDataUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_DATA_UMT_AREA];
		return getWsDataUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsDataUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		halludat.getCommonBytes(buffer, position);
		return buffer;
	}

	public void initWsDataUmtAreaSpaces() {
		halludat.initHalludatSpaces();
	}

	public void initWsHdrUmtAreaSpaces() {
		halluhdr.initHalluhdrSpaces();
	}

	public void setWsDataPrivacyInfoFormatted(String data) {
		byte[] buffer = new byte[Len.WS_DATA_PRIVACY_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_DATA_PRIVACY_INFO);
		setWsDataPrivacyInfoBytes(buffer, 1);
	}

	public String getWsDataPrivacyInfoFormatted() {
		return getCidpPassedInfoFormatted();
	}

	public void setWsDataPrivacyInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setCidpPassedInfoBytes(buffer, position);
	}

	public String getCidpPassedInfoFormatted() {
		return MarshalByteExt.bufferToStr(getCidpPassedInfoBytes());
	}

	/**Original name: CIDP-PASSED-INFO<br>
	 * <pre>****************************************************************
	 *  HALLCIDP                                                      *
	 *  COMMON INTERFACE TO DATA PRIVACY                              *
	 *  USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
	 *  DATA PRIVACY CHECKING.                                        *
	 * ****************************************************************
	 *  SI#       PRGRMR  DATE       DESCRIPTION                      *
	 *  --------- ------- ---------- ---------------------------------*
	 *  SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
	 *  14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
	 *                               ENHANCED DATA PRIVACY AND SET    *
	 *                               DEFAULTS PROCESSING.             *
	 *                               (ARCHITECTURE 2.3)               *
	 * ****************************************************************</pre>*/
	public byte[] getCidpPassedInfoBytes() {
		byte[] buffer = new byte[Len.CIDP_PASSED_INFO];
		return getCidpPassedInfoBytes(buffer, 1);
	}

	public void setCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.setCidpTableInfoBytes(buffer, position);
	}

	public byte[] getCidpPassedInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		cidpTableInfo.getCidpTableInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsSe3CurIsoDateTimeFormatted(String data) {
		byte[] buffer = new byte[Len.WS_SE3_CUR_ISO_DATE_TIME];
		MarshalByte.writeString(buffer, 1, data, Len.WS_SE3_CUR_ISO_DATE_TIME);
		setWsSe3CurIsoDateTimeBytes(buffer, 1);
	}

	public void setWsSe3CurIsoDateTimeBytes(byte[] buffer, int offset) {
		int position = offset;
		wsSe3CurIsoDate = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_DATE);
		position += Len.WS_SE3_CUR_ISO_DATE;
		wsSe3CurIsoTime = MarshalByte.readString(buffer, position, Len.WS_SE3_CUR_ISO_TIME);
	}

	public void setWsSe3CurIsoDate(String wsSe3CurIsoDate) {
		this.wsSe3CurIsoDate = Functions.subString(wsSe3CurIsoDate, Len.WS_SE3_CUR_ISO_DATE);
	}

	public String getWsSe3CurIsoDate() {
		return this.wsSe3CurIsoDate;
	}

	public void setWsSe3CurIsoTime(String wsSe3CurIsoTime) {
		this.wsSe3CurIsoTime = Functions.subString(wsSe3CurIsoTime, Len.WS_SE3_CUR_ISO_TIME);
	}

	public String getWsSe3CurIsoTime() {
		return this.wsSe3CurIsoTime;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public CidpTableInfo getCidpTableInfo() {
		return cidpTableInfo;
	}

	public DateStructureCawpcorc getDateStructure() {
		return dateStructure;
	}

	public DclhalBoAudTgrV getDclhalBoAudTgrV() {
		return dclhalBoAudTgrV;
	}

	public DclhalBoMduXrfV getDclhalBoMduXrfV() {
		return dclhalBoMduXrfV;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclhalUowObjHierV getDclhalUowObjHierV() {
		return dclhalUowObjHierV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Halluchs getHalluchs() {
		return halluchs;
	}

	public Halludat getHalludat() {
		return halludat;
	}

	public Halluhdr getHalluhdr() {
		return halluhdr;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	public Hallusw getHallusw() {
		return hallusw;
	}

	public WsHalrlodrLinkage getHalrlodrLockDriverStorage() {
		return halrlodrLockDriverStorage;
	}

	public HalrmonPerfMonitorStorage getHalrmonPerfMonitorStorage() {
		return halrmonPerfMonitorStorage;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WsBdoSwitches getWsBdoSwitches() {
		return wsBdoSwitches;
	}

	public WsBdoWorkFields getWsBdoWorkFields() {
		return wsBdoWorkFields;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsSpecificMiscXz0d0006 getWsSpecificMisc() {
		return wsSpecificMisc;
	}

	public Xz0n0006 getXz0n0006() {
		return xz0n0006;
	}

	public Xzc004ActNotPolFrmRow getXzc004ActNotPolFrmRow() {
		return xzc004ActNotPolFrmRow;
	}

	public Xzc006ActNotFrmRow getXzc006ActNotFrmRow() {
		return xzc006ActNotFrmRow;
	}

	public Xzc007ActNotFrmRecRow getXzc007ActNotFrmRecRow() {
		return xzc007ActNotFrmRecRow;
	}

	public Xzh001ActNotRow getXzh001ActNotRow() {
		return xzh001ActNotRow;
	}

	public DclactNotFrm getXzh006ActNotFrmRow() {
		return xzh006ActNotFrmRow;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_SE3_CUR_ISO_DATE_TIME = WS_SE3_CUR_ISO_DATE + WS_SE3_CUR_ISO_TIME;
		public static final int WS_DATA_UMT_AREA = Halludat.Len.COMMON;
		public static final int CIDP_PASSED_INFO = CidpTableInfo.Len.CIDP_TABLE_INFO;
		public static final int WS_DATA_PRIVACY_INFO = CIDP_PASSED_INFO;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int HALOUKRP_LINKAGE = Hallukrp.Len.INPUT_FIELDS + Hallukrp.Len.INPUT_OUTPUT_FIELDS;
		public static final int HALOUCHS_LINKAGE = Halluchs.Len.LENGTH2 + Halluchs.Len.STRING_FLD + Halluchs.Len.CHECK_SUM;
		public static final int WS_ACT_NOT_FRM_REC_COPYBOOK = Xzc007ActNotFrmRecRow.Len.XZC007_ACT_NOT_FRM_REC_ROW;
		public static final int WS_ACT_NOT_POL_FRM_COPYBOOK = Xzc004ActNotPolFrmRow.Len.XZC004_ACT_NOT_POL_FRM_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
