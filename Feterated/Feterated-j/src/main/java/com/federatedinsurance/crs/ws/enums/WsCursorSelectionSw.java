/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-CURSOR-SELECTION-SW<br>
 * Variable: WS-CURSOR-SELECTION-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCursorSelectionSw {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CURSOR_SELECTION_SW);
	public static final String INVALID_RETRIEVE_CURSOR = "000";
	public static final String RETRIEVE_WITH_CURSOR1 = "001";
	public static final String RETRIEVE_WITH_CURSOR2 = "002";
	public static final String RETRIEVE_WITH_CURSOR3 = "003";
	public static final String RETRIEVE_WITH_CURSOR4 = "004";
	public static final String RETRIEVE_WITH_CURSOR5 = "005";
	public static final String RETRIEVE_WITH_CURSOR6 = "006";
	public static final String RETRIEVE_WITH_CURSOR7 = "007";
	public static final String RETRIEVE_WITH_CURSOR8 = "008";
	public static final String RETRIEVE_WITH_CURSOR9 = "009";
	public static final String RETRIEVE_WITH_CURSOR10 = "010";

	//==== METHODS ====
	public void setCursorSelectionSw(short cursorSelectionSw) {
		this.value = NumericDisplay.asString(cursorSelectionSw, Len.CURSOR_SELECTION_SW);
	}

	public void setCursorSelectionSwFormatted(String cursorSelectionSw) {
		this.value = Trunc.toUnsignedNumeric(cursorSelectionSw, Len.CURSOR_SELECTION_SW);
	}

	public short getCursorSelectionSw() {
		return NumericDisplay.asShort(this.value);
	}

	public String getCursorSelectionSwFormatted() {
		return this.value;
	}

	public String getCursorSelectionSwAsString() {
		return getCursorSelectionSwFormatted();
	}

	public void setInvalidRetrieveCursor() {
		setCursorSelectionSwFormatted(INVALID_RETRIEVE_CURSOR);
	}

	public void setRetrieveWithCursor1() {
		setCursorSelectionSwFormatted(RETRIEVE_WITH_CURSOR1);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CURSOR_SELECTION_SW = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
