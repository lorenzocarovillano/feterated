/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-LOG-WARNING-OR-ERROR-SW<br>
 * Variable: WS-LOG-WARNING-OR-ERROR-SW from program HALOUIDG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsLogWarningOrErrorSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WARNING = 'W';
	public static final char ERROR = 'E';

	//==== METHODS ====
	public void setWsLogWarningOrErrorSw(char wsLogWarningOrErrorSw) {
		this.value = wsLogWarningOrErrorSw;
	}

	public char getWsLogWarningOrErrorSw() {
		return this.value;
	}

	public boolean isWarning() {
		return value == WARNING;
	}

	public void setWarning() {
		value = WARNING;
	}

	public boolean isError() {
		return value == ERROR;
	}

	public void setError() {
		value = ERROR;
	}
}
