/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SA-TSXXXXBL-FILE-NAME<br>
 * Variable: SA-TSXXXXBL-FILE-NAME from program TS030099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class SaTsxxxxblFileName extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: SA-TSXXXXBL-REPORT-NUMBER
	private String reportNumber = DefaultValues.stringVal(Len.REPORT_NUMBER);
	//Original name: SA-TSXXXXBL-OFFICE-LOCATION
	private String officeLocation = DefaultValues.stringVal(Len.OFFICE_LOCATION);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.TSXXXXBL_FILE_NAME;
	}

	@Override
	public void deserialize(byte[] buf) {
		setTsxxxxblFileNameBytes(buf);
	}

	public String getTsxxxxblFileNameFormatted() {
		return MarshalByteExt.bufferToStr(getTsxxxxblFileNameBytes());
	}

	public void setTsxxxxblFileNameBytes(byte[] buffer) {
		setTsxxxxblFileNameBytes(buffer, 1);
	}

	public byte[] getTsxxxxblFileNameBytes() {
		byte[] buffer = new byte[Len.TSXXXXBL_FILE_NAME];
		return getTsxxxxblFileNameBytes(buffer, 1);
	}

	public void setTsxxxxblFileNameBytes(byte[] buffer, int offset) {
		int position = offset;
		reportNumber = MarshalByte.readString(buffer, position, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		officeLocation = MarshalByte.readString(buffer, position, Len.OFFICE_LOCATION);
	}

	public byte[] getTsxxxxblFileNameBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, reportNumber, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		MarshalByte.writeString(buffer, position, officeLocation, Len.OFFICE_LOCATION);
		return buffer;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = Functions.subString(reportNumber, Len.REPORT_NUMBER);
	}

	public String getReportNumber() {
		return this.reportNumber;
	}

	public void setOfficeLocation(String officeLocation) {
		this.officeLocation = Functions.subString(officeLocation, Len.OFFICE_LOCATION);
	}

	public String getOfficeLocation() {
		return this.officeLocation;
	}

	@Override
	public byte[] serialize() {
		return getTsxxxxblFileNameBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REPORT_NUMBER = 6;
		public static final int OFFICE_LOCATION = 2;
		public static final int TSXXXXBL_FILE_NAME = REPORT_NUMBER + OFFICE_LOCATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
