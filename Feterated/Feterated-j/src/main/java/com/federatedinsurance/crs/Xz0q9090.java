/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.ws.DfhcommareaTs020100;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.Xz0q9090Data;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestAreaXz0q9090;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: XZ0Q9090<br>
 * <pre>AUTHOR.       GARY LIEDTKE.
 * DATE-WRITTEN. 30 JAN 2009.
 * ***************************************************************
 *   PROGRAM TITLE - FRAMEWORK REQUEST MODULE FOR                *
 *                   UOW        : XZ_GET_NOT_CO_LIST             *
 *                   OPERATIONS : GetActiveNotificationCopyList  *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROL THE COMMUNICATION SHELL REQUEST PROCESS  *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM A FRAMEWORK MAIN DRIVER *
 *                        PROGRAM(TS020000). NAME OF THE REQUEST *
 *                        MODULE IS PASSED AS A PARAMETER TO THE *
 *                        MAIN DRIVER.                           *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA (FOR   *
 *                         COMMON PARAMETERS) AND CICS MAIN      *
 *                         STORAGE (OPERATION SPECIFIC DATA)     *
 *                         OUTPUT TO REQUEST UMT AND DFHCOMMAREA *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
 *  YJ249    27APR07   E404NEM   STD CHGS                        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  ------- ---------- -------   ------------------------------- *
 *  TO07614 01/30/2009 E404GCL   INITIAL PROGRAM                 *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0q9090 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>***************************************************************
	 *  START OF:                                                    *
	 *      GENERAL BPO/COMM SHELL PROGRAM WORKING-STORAGE           *
	 *      (INCLUDED IN ALL BPOS/COMM SHELL PROGRAMS)               *
	 * ***************************************************************
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Xz0q9090Data ws = new Xz0q9090Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020100 dfhcommarea;
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* FRAMEWORK REQUEST/INPUT LAYOUT FOR THIS OPERATION</pre>*/
	private LFrameworkRequestAreaXz0q9090 lFrameworkRequestArea = new LFrameworkRequestAreaXz0q9090(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020100 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xz0q9090 getInstance() {
		return (Programs.getInstance(Xz0q9090.class));
	}

	/**Original name: 1000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CREATE-OPERATION-REQ.
		createOperationReq();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM STARTUP/INITIALIZATION PROCESSING                    *
	 * ***************************************************************
	 *  INITIALIZE ERROR PROCESSING FIELDS</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO.
		initEstoStoreInfo();
		initEstoReturnInfo();
		// CREATE NECESSARY UOW SWITCHES FOR THIS OPERATION
		// COB_CODE: PERFORM 2100-PARSE-REQUEST-SWITCHES.
		parseRequestSwitches();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-DETERMINE-OPERATION.
		determineOperation();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-PARSE-REQUEST-SWITCHES_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  SET SWITCHES FOR BUSINESS DATA OBJECTS                       *
	 *  SET 'RETURN ALL' INITIALLY                                   *
	 *  MODIFY TO SET EACH INDIVIDUAL SWITCH AS NEEDED               *
	 * ***************************************************************</pre>*/
	private void parseRequestSwitches() {
		TpOutputData tsQueueData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID            TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: MOVE 'RETURN ALL'           TO USW-BUS-OBJ-SWITCH.
		ws.getHallusw().setBusObjSwitch("RETURN ALL");
		// COB_CODE: EXEC CICS
		//               WRITEQ TS MAIN
		//               QNAME(CSC-REQ-SWITCHES-TSQ)
		//               FROM (WS-USW-MSG)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(Xz0q9090Data.Len.WS_USW_MSG);
		tsQueueData.setData(ws.getWsUswMsgBytes());
		TsQueueManager.insert(execContext, dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getCscReqSwitchesTsqFormatted(),
				tsQueueData);
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   GO TO 2100-EXIT
		//               WHEN OTHER
		//                   GO TO 2100-EXIT
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: GO TO 2100-EXIT
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR    TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE CSC-REQ-SWITCHES-TSQ
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrObjectName(dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getReqSwitchesTsq());
			// COB_CODE: MOVE '2100-PARSE-REQUEST-SWITCHES'
			//                               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2100-PARSE-REQUEST-SWITCHES");
			// COB_CODE: MOVE 'WRITE REQUEST SWITCHES TSQ FAILED IN TS020100'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE REQUEST SWITCHES TSQ FAILED IN TS020100");
			// COB_CODE: STRING 'UBOC-MSG-ID='
			//                  UBOC-MSG-ID
			//                  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UBOC-MSG-ID=",
					dfhcommarea.getUbocCommInfo().getUbocMsgIdFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
	}

	/**Original name: 2200-DETERMINE-OPERATION_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  VALIDATE THAT THIS MODULE CAN BE CALLED FOR THIS OPERATION.  *
	 *  IF NOT, RAISE A LOGGABLE ERROR.                              *
	 * ***************************************************************</pre>*/
	private void determineOperation() {
		// COB_CODE: MOVE CSC-OPERATION          TO WS-OPERATION-NAME.
		ws.getWorkingStorageArea().getOperationName().setOperationName(dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getOperation());
		// COB_CODE: IF WS-VALID-OPERATION
		//               GO TO 2200-EXIT
		//           END-IF.
		if (ws.getWorkingStorageArea().getOperationName().isValidOperation()) {
			// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
			//                                   TO MA-INPUT-POINTER
			lFrameworkRequestArea = ((pointerManager
					.resolve(dfhcommarea.getCommunicationShellCommon().getMaInputPointer(), LFrameworkRequestAreaXz0q9090.class)));
			// COB_CODE: GO TO 2200-EXIT
			return;
		}
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-BUS-PROCESS-FAILED TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
		// COB_CODE: MOVE CF-INVALID-OPERATION   TO EFAL-ACTION-BUFFER.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalActionBuffer(ws.getCfInvalidOperationFormatted());
		// COB_CODE: MOVE WS-PROGRAM-NAME        TO EFAL-ERR-OBJECT-NAME.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE '2200-DETERMINE-OPERATION'
		//                                       TO EFAL-ERR-PARAGRAPH.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-DETERMINE-OPERATION");
		// COB_CODE: MOVE WS-OPERATION-NAME      TO EA-01-INVALID-OPERATION-NAME.
		ws.setEa01InvalidOperationName(ws.getWorkingStorageArea().getOperationName().getOperationName());
		// COB_CODE: MOVE EA-01-INVALID-OPERATION-MSG
		//                                       TO EFAL-ERR-COMMENT
		//                                          EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getEa01InvalidOperationMsgFormatted());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getEa01InvalidOperationMsgFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 3000-CREATE-OPERATION-REQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CREATE FRAMEWORK REQUEST ROWS ON REQUEST UMT                   *
	 *  A SEPARATE SECTION IS INCLUDED FOR EACH ROW CREATED            *
	 *  CODE THE SPECIFIC SECTIONS NEEDED TO CREATE THE REQUEST ROWS   *
	 *  SPECIFIC TO THIS OPERATION.                                    *
	 * *****************************************************************</pre>*/
	private void createOperationReq() {
		// COB_CODE: PERFORM 3100-GET-ACTIVE-NOT-CO-ROW.
		getActiveNotCoRow();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-GET-ACTIVE-NOT-CO-ROW_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  LINK TO COMMON REQUEST UMT UPDATE MODULE TO INSERT REQUEST FOR *
	 *  XZ_GET_NOT_CO_LIST.                                            *
	 * *****************************************************************</pre>*/
	private void getActiveNotCoRow() {
		Halrurqa halrurqa = null;
		// COB_CODE: SET HALRURQA-WRITE-FUNC     TO TRUE.
		ws.getWsHalrurqaLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE CF-BUS-OBJ-NM          TO HALRURQA-BUS-OBJ-NM.
		ws.getWsHalrurqaLinkage().setBusObjNm(ws.getCfBusObjNm());
		// COB_CODE: MOVE 'FETCH'                TO HALRURQA-ACTION-CODE.
		ws.getWsHalrurqaLinkage().setActionCode("FETCH");
		// COB_CODE: MOVE LENGTH OF XZA990-NOT-CO-INFO
		//                                       TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		ws.getWsHalrurqaLinkage().setBusObjDataLength(((short) LFrameworkRequestAreaXz0q9090.Len.NOT_CO_INFO));
		// COB_CODE: CALL HALRURQA-HALRURQA-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRURQA-LINKAGE
		//                XZA990-NOT-CO-INFO.
		halrurqa = Halrurqa.getInstance();
		halrurqa.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrurqaLinkage(), lFrameworkRequestArea);
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3100-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWorkingStorageArea().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkingStorageArea().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0Q9090", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setUbocAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setUbocAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
