/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xza980TtyList;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9080-ROW<br>
 * Variable: WS-XZ0A9080-ROW from program XZ0B9080<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9080Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA980-MAX-TTY-ROWS
	private short maxTtyRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA980-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZA980-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZA980-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);
	//Original name: XZA980-TTY-LIST
	private Xza980TtyList ttyList = new Xza980TtyList();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9080_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9080RowBytes(buf);
	}

	public String getWsXz0a9080RowFormatted() {
		return getTtyInfoRowFormatted();
	}

	public void setWsXz0a9080RowBytes(byte[] buffer) {
		setWsXz0a9080RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9080RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9080_ROW];
		return getWsXz0a9080RowBytes(buffer, 1);
	}

	public void setWsXz0a9080RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setTtyInfoRowBytes(buffer, position);
	}

	public byte[] getWsXz0a9080RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getTtyInfoRowBytes(buffer, position);
		return buffer;
	}

	public String getTtyInfoRowFormatted() {
		return MarshalByteExt.bufferToStr(getTtyInfoRowBytes());
	}

	/**Original name: XZA980-TTY-INFO-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A9080 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_THIRD_PARTY_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  29JAN2009 E404DLP    NEW                          *
	 *  TO07614  09MAR2009 E404DLP    CHANGE ADR-SEQ-NBR TO ADR-ID *
	 *  20163.20 13JUL2018 E404DMW    UPDATED ADDRESS AND CLIENT   *
	 *                                IDS FROM 20 TO 64            *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getTtyInfoRowBytes() {
		byte[] buffer = new byte[Len.TTY_INFO_ROW];
		return getTtyInfoRowBytes(buffer, 1);
	}

	public void setTtyInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxTtyRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
		position += Len.USERID;
		ttyList.setTtyListBytes(buffer, position);
	}

	public byte[] getTtyInfoRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxTtyRows);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		position += Len.USERID;
		ttyList.getTtyListBytes(buffer, position);
		return buffer;
	}

	public void setMaxTtyRows(short maxTtyRows) {
		this.maxTtyRows = maxTtyRows;
	}

	public short getMaxTtyRows() {
		return this.maxTtyRows;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public Xza980TtyList getTtyList() {
		return ttyList;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9080RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_TTY_ROWS = 2;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int USERID = 8;
		public static final int TTY_INFO_ROW = MAX_TTY_ROWS + CSR_ACT_NBR + NOT_PRC_TS + USERID + Xza980TtyList.Len.TTY_LIST;
		public static final int WS_XZ0A9080_ROW = TTY_INFO_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
