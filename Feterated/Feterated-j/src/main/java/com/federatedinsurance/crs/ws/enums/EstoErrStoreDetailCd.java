/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ESTO-ERR-STORE-DETAIL-CD<br>
 * Variable: ESTO-ERR-STORE-DETAIL-CD from copybook HALLESTO<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class EstoErrStoreDetailCd {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.STORE_DETAIL_CD);
	public static final String NO_ERROR = "00";
	public static final String ESTO_FAILED_APP_UNKNOWN = "01";
	public static final String ESTO_STORE_ID_EMPTY = "02";
	public static final String ESTO_RECORDING_LEVEL_INVALID = "03";
	public static final String ESTO_RECORDING_LEVEL_EMPTY = "04";
	public static final String ESTO_FAILURE_TYPE_INVALID = "05";
	public static final String ESTO_FAILURE_TYPE_EMPTY = "06";
	public static final String ESTO_FAILURE_ACTION_INVALID = "07";
	public static final String ESTO_FAILURE_ACTION_NOT_FILLED = "08";
	public static final String ESTO_DETAIL_BUFFER_NOT_FILLED = "09";
	public static final String ESTO_CALL_ETRA_SW_NOT_SET = "10";
	public static final String ESTO_DET_LVL_GUID_NOT_FILLED = "11";
	public static final String ESTO_FAILED_PLTF_NOT_FILLED = "12";
	public static final String ESTO_FAILED_MOD_NOT_FILLED = "13";
	public static final String ESTO_ERR_PARA_NOT_FILLED = "14";
	public static final String ESTO_ERR_COMM_NOT_FILLED = "15";
	public static final String ESTO_UOW_NAME_NOT_FILLED = "16";
	public static final String ESTO_USERID_NOT_FILLED = "17";
	public static final String ESTO_ETRA_RETURN_DATA_INVALID = "18";
	public static final String ESTO_CALL_HALOETRA_FAILED = "19";
	public static final String ESTO_READ_VSAM_FAILED = "20";
	public static final String ESTO_STARTBR_VSAM_FAILED = "21";
	public static final String ESTO_READNXT_VSAM_FAILED = "22";
	public static final String ESTO_ENDBR_VSAM_FAILED = "23";
	public static final String ESTO_WRITE_VSAM_FAILED = "24";
	public static final String ESTO_FORMATTIME_CALL_FAILED = "25";
	public static final String ESTO_ACTION_NOT_FILLED = "26";
	public static final String ESTO_DB2_SQLCODE_NOT_FILLED = "27";
	public static final String ESTO_CICS_ERR_RESP_NOT_FILLED = "28";
	public static final String ESTO_SUP_TAB_UNIQUE_DB2_ERR = "29";
	public static final String ESTO_SUP_TAB_DEFAULT_NOT_FND = "30";
	public static final String ESTO_SUP_TAB_NON_UQUE_DB2_ERR = "31";
	public static final String ESTO_WRITE_DETAIL_UMT_FAILED = "32";
	public static final String ESTO_STARTBR_ALT_IDX_FAILED = "33";
	public static final String ESTO_WRITE_DET_VSAM_FAILED = "34";

	//==== METHODS ====
	public void setStoreDetailCd(short storeDetailCd) {
		this.value = NumericDisplay.asString(storeDetailCd, Len.STORE_DETAIL_CD);
	}

	public void setStoreDetailCdFormatted(String storeDetailCd) {
		this.value = Trunc.toUnsignedNumeric(storeDetailCd, Len.STORE_DETAIL_CD);
	}

	public short getStoreDetailCd() {
		return NumericDisplay.asShort(this.value);
	}

	public String getStoreDetailCdFormatted() {
		return this.value;
	}

	public String getStoreDetailCdAsString() {
		return getStoreDetailCdFormatted();
	}

	public void setEstoFailedAppUnknown() {
		setStoreDetailCdFormatted(ESTO_FAILED_APP_UNKNOWN);
	}

	public void setEstoStoreIdEmpty() {
		setStoreDetailCdFormatted(ESTO_STORE_ID_EMPTY);
	}

	public void setEstoRecordingLevelInvalid() {
		setStoreDetailCdFormatted(ESTO_RECORDING_LEVEL_INVALID);
	}

	public void setEstoRecordingLevelEmpty() {
		setStoreDetailCdFormatted(ESTO_RECORDING_LEVEL_EMPTY);
	}

	public void setEstoFailureTypeInvalid() {
		setStoreDetailCdFormatted(ESTO_FAILURE_TYPE_INVALID);
	}

	public void setEstoFailureTypeEmpty() {
		setStoreDetailCdFormatted(ESTO_FAILURE_TYPE_EMPTY);
	}

	public void setEstoFailureActionInvalid() {
		setStoreDetailCdFormatted(ESTO_FAILURE_ACTION_INVALID);
	}

	public void setEstoFailureActionNotFilled() {
		setStoreDetailCdFormatted(ESTO_FAILURE_ACTION_NOT_FILLED);
	}

	public void setEstoDetailBufferNotFilled() {
		setStoreDetailCdFormatted(ESTO_DETAIL_BUFFER_NOT_FILLED);
	}

	public void setEstoCallEtraSwNotSet() {
		setStoreDetailCdFormatted(ESTO_CALL_ETRA_SW_NOT_SET);
	}

	public void setEstoDetLvlGuidNotFilled() {
		setStoreDetailCdFormatted(ESTO_DET_LVL_GUID_NOT_FILLED);
	}

	public void setEstoFailedPltfNotFilled() {
		setStoreDetailCdFormatted(ESTO_FAILED_PLTF_NOT_FILLED);
	}

	public void setEstoFailedModNotFilled() {
		setStoreDetailCdFormatted(ESTO_FAILED_MOD_NOT_FILLED);
	}

	public void setEstoErrParaNotFilled() {
		setStoreDetailCdFormatted(ESTO_ERR_PARA_NOT_FILLED);
	}

	public void setEstoErrCommNotFilled() {
		setStoreDetailCdFormatted(ESTO_ERR_COMM_NOT_FILLED);
	}

	public void setEstoEtraReturnDataInvalid() {
		setStoreDetailCdFormatted(ESTO_ETRA_RETURN_DATA_INVALID);
	}

	public void setEstoCallHaloetraFailed() {
		setStoreDetailCdFormatted(ESTO_CALL_HALOETRA_FAILED);
	}

	public void setEstoStartbrVsamFailed() {
		setStoreDetailCdFormatted(ESTO_STARTBR_VSAM_FAILED);
	}

	public void setEstoReadnxtVsamFailed() {
		setStoreDetailCdFormatted(ESTO_READNXT_VSAM_FAILED);
	}

	public void setEstoEndbrVsamFailed() {
		setStoreDetailCdFormatted(ESTO_ENDBR_VSAM_FAILED);
	}

	public void setEstoWriteVsamFailed() {
		setStoreDetailCdFormatted(ESTO_WRITE_VSAM_FAILED);
	}

	public void setEstoFormattimeCallFailed() {
		setStoreDetailCdFormatted(ESTO_FORMATTIME_CALL_FAILED);
	}

	public void setEstoActionNotFilled() {
		setStoreDetailCdFormatted(ESTO_ACTION_NOT_FILLED);
	}

	public void setEstoDb2SqlcodeNotFilled() {
		setStoreDetailCdFormatted(ESTO_DB2_SQLCODE_NOT_FILLED);
	}

	public void setEstoCicsErrRespNotFilled() {
		setStoreDetailCdFormatted(ESTO_CICS_ERR_RESP_NOT_FILLED);
	}

	public void setEstoSupTabUniqueDb2Err() {
		setStoreDetailCdFormatted(ESTO_SUP_TAB_UNIQUE_DB2_ERR);
	}

	public void setEstoSupTabNonUqueDb2Err() {
		setStoreDetailCdFormatted(ESTO_SUP_TAB_NON_UQUE_DB2_ERR);
	}

	public void setEstoStartbrAltIdxFailed() {
		setStoreDetailCdFormatted(ESTO_STARTBR_ALT_IDX_FAILED);
	}

	public void setEstoWriteDetVsamFailed() {
		setStoreDetailCdFormatted(ESTO_WRITE_DET_VSAM_FAILED);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STORE_DETAIL_CD = 2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
