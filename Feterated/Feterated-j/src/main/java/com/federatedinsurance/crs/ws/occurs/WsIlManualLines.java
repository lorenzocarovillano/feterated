/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-IL-MANUAL-LINES<br>
 * Variables: WS-IL-MANUAL-LINES from program XZ0P9000<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsIlManualLines {

	//==== PROPERTIES ====
	//Original name: WS-IL-ML-INS-LIN-CD
	private String wsIlMlInsLinCd = DefaultValues.stringVal(Len.WS_IL_ML_INS_LIN_CD);

	//==== METHODS ====
	public void setWsIlMlInsLinCd(String wsIlMlInsLinCd) {
		this.wsIlMlInsLinCd = Functions.subString(wsIlMlInsLinCd, Len.WS_IL_ML_INS_LIN_CD);
	}

	public String getWsIlMlInsLinCd() {
		return this.wsIlMlInsLinCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_IL_ML_INS_LIN_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
