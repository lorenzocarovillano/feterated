/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xz009iServiceInputs;
import com.federatedinsurance.crs.copy.Xz009oTrsDtlRsp;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program XZC09090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaXzc09090 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZC09I-SERVICE-INPUTS
	private Xz009iServiceInputs xzc09iServiceInputs = new Xz009iServiceInputs();
	//Original name: XZC09O-SERVICE-OUTPUTS
	private Xz009oTrsDtlRsp xzc09oServiceOutputs = new Xz009oTrsDtlRsp();
	//Original name: XZC09O-ERROR-INDICATOR
	private String xzc09oErrorIndicator = DefaultValues.stringVal(Len.XZC09O_ERROR_INDICATOR);
	//Original name: XZC09O-FAULT-CODE
	private String xzc09oFaultCode = DefaultValues.stringVal(Len.XZC09O_FAULT_CODE);
	//Original name: XZC09O-FAULT-STRING
	private String xzc09oFaultString = DefaultValues.stringVal(Len.XZC09O_FAULT_STRING);
	//Original name: XZC09O-FAULT-ACTOR
	private String xzc09oFaultActor = DefaultValues.stringVal(Len.XZC09O_FAULT_ACTOR);
	//Original name: XZC09O-FAULT-DETAIL
	private String xzc09oFaultDetail = DefaultValues.stringVal(Len.XZC09O_FAULT_DETAIL);
	//Original name: XZC09O-NON-SOAP-FAULT-ERR-TXT
	private String xzc09oNonSoapFaultErrTxt = DefaultValues.stringVal(Len.XZC09O_NON_SOAP_FAULT_ERR_TXT);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc09iServiceInputs.setXzc09iServiceInputsBytes(buffer, position);
		position += Xz009iServiceInputs.Len.XZC09I_SERVICE_INPUTS;
		xzc09oServiceOutputs.setXzc09oServiceOutputsBytes(buffer, position);
		position += Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS;
		setXzc09oErrorInformationBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc09iServiceInputs.getXzc09iServiceInputsBytes(buffer, position);
		position += Xz009iServiceInputs.Len.XZC09I_SERVICE_INPUTS;
		xzc09oServiceOutputs.getXzc09oServiceOutputsBytes(buffer, position);
		position += Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS;
		getXzc09oErrorInformationBytes(buffer, position);
		return buffer;
	}

	public void setXzc09oErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc09oErrorIndicator = MarshalByte.readString(buffer, position, Len.XZC09O_ERROR_INDICATOR);
		position += Len.XZC09O_ERROR_INDICATOR;
		xzc09oFaultCode = MarshalByte.readString(buffer, position, Len.XZC09O_FAULT_CODE);
		position += Len.XZC09O_FAULT_CODE;
		xzc09oFaultString = MarshalByte.readString(buffer, position, Len.XZC09O_FAULT_STRING);
		position += Len.XZC09O_FAULT_STRING;
		xzc09oFaultActor = MarshalByte.readString(buffer, position, Len.XZC09O_FAULT_ACTOR);
		position += Len.XZC09O_FAULT_ACTOR;
		xzc09oFaultDetail = MarshalByte.readString(buffer, position, Len.XZC09O_FAULT_DETAIL);
		position += Len.XZC09O_FAULT_DETAIL;
		xzc09oNonSoapFaultErrTxt = MarshalByte.readString(buffer, position, Len.XZC09O_NON_SOAP_FAULT_ERR_TXT);
	}

	public byte[] getXzc09oErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc09oErrorIndicator, Len.XZC09O_ERROR_INDICATOR);
		position += Len.XZC09O_ERROR_INDICATOR;
		MarshalByte.writeString(buffer, position, xzc09oFaultCode, Len.XZC09O_FAULT_CODE);
		position += Len.XZC09O_FAULT_CODE;
		MarshalByte.writeString(buffer, position, xzc09oFaultString, Len.XZC09O_FAULT_STRING);
		position += Len.XZC09O_FAULT_STRING;
		MarshalByte.writeString(buffer, position, xzc09oFaultActor, Len.XZC09O_FAULT_ACTOR);
		position += Len.XZC09O_FAULT_ACTOR;
		MarshalByte.writeString(buffer, position, xzc09oFaultDetail, Len.XZC09O_FAULT_DETAIL);
		position += Len.XZC09O_FAULT_DETAIL;
		MarshalByte.writeString(buffer, position, xzc09oNonSoapFaultErrTxt, Len.XZC09O_NON_SOAP_FAULT_ERR_TXT);
		return buffer;
	}

	public void setXzc09oErrorIndicator(String xzc09oErrorIndicator) {
		this.xzc09oErrorIndicator = Functions.subString(xzc09oErrorIndicator, Len.XZC09O_ERROR_INDICATOR);
	}

	public String getXzc09oErrorIndicator() {
		return this.xzc09oErrorIndicator;
	}

	public void setXzc09oFaultCode(String xzc09oFaultCode) {
		this.xzc09oFaultCode = Functions.subString(xzc09oFaultCode, Len.XZC09O_FAULT_CODE);
	}

	public String getXzc09oFaultCode() {
		return this.xzc09oFaultCode;
	}

	public void setXzc09oFaultString(String xzc09oFaultString) {
		this.xzc09oFaultString = Functions.subString(xzc09oFaultString, Len.XZC09O_FAULT_STRING);
	}

	public String getXzc09oFaultString() {
		return this.xzc09oFaultString;
	}

	public void setXzc09oFaultActor(String xzc09oFaultActor) {
		this.xzc09oFaultActor = Functions.subString(xzc09oFaultActor, Len.XZC09O_FAULT_ACTOR);
	}

	public String getXzc09oFaultActor() {
		return this.xzc09oFaultActor;
	}

	public void setXzc09oFaultDetail(String xzc09oFaultDetail) {
		this.xzc09oFaultDetail = Functions.subString(xzc09oFaultDetail, Len.XZC09O_FAULT_DETAIL);
	}

	public String getXzc09oFaultDetail() {
		return this.xzc09oFaultDetail;
	}

	public void setXzc09oNonSoapFaultErrTxt(String xzc09oNonSoapFaultErrTxt) {
		this.xzc09oNonSoapFaultErrTxt = Functions.subString(xzc09oNonSoapFaultErrTxt, Len.XZC09O_NON_SOAP_FAULT_ERR_TXT);
	}

	public String getXzc09oNonSoapFaultErrTxt() {
		return this.xzc09oNonSoapFaultErrTxt;
	}

	public Xz009iServiceInputs getXzc09iServiceInputs() {
		return xzc09iServiceInputs;
	}

	public Xz009oTrsDtlRsp getXzc09oServiceOutputs() {
		return xzc09oServiceOutputs;
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC09O_ERROR_INDICATOR = 30;
		public static final int XZC09O_FAULT_CODE = 30;
		public static final int XZC09O_FAULT_STRING = 256;
		public static final int XZC09O_FAULT_ACTOR = 256;
		public static final int XZC09O_FAULT_DETAIL = 256;
		public static final int XZC09O_NON_SOAP_FAULT_ERR_TXT = 256;
		public static final int XZC09O_ERROR_INFORMATION = XZC09O_ERROR_INDICATOR + XZC09O_FAULT_CODE + XZC09O_FAULT_STRING + XZC09O_FAULT_ACTOR
				+ XZC09O_FAULT_DETAIL + XZC09O_NON_SOAP_FAULT_ERR_TXT;
		public static final int DFHCOMMAREA = Xz009iServiceInputs.Len.XZC09I_SERVICE_INPUTS + Xz009oTrsDtlRsp.Len.XZC09O_SERVICE_OUTPUTS
				+ XZC09O_ERROR_INFORMATION;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
