/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.ws.enums.SaReturnCode;
import com.federatedinsurance.crs.ws.enums.SaSqlcode;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaXz001000 implements IActNotPol {

	//==== PROPERTIES ====
	//Original name: SA-SQLCODE
	private SaSqlcode sqlcode = new SaSqlcode();
	//Original name: SA-RETURN-CODE
	private SaReturnCode returnCode = new SaReturnCode();
	//Original name: SA-ACCOUNT-NBR
	private String accountNbr = DefaultValues.stringVal(Len.ACCOUNT_NBR);
	//Original name: SA-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: SA-PARAGRAPH
	private String paragraph = DefaultValues.stringVal(Len.PARAGRAPH);
	/**Original name: SA-POL-NBR<br>
	 * <pre>    IF THE SIZE OF ANY OF THESE FIELDS CHANGE THE CORRESPONDING
	 *     TABLE FIELD NEEDS TO BE CHANGED.</pre>*/
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: SA-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);

	//==== METHODS ====
	public void setAccountNbr(String accountNbr) {
		this.accountNbr = Functions.subString(accountNbr, Len.ACCOUNT_NBR);
	}

	public String getAccountNbr() {
		return this.accountNbr;
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	@Override
	public String getNotEffDt() {
		return this.notEffDt;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = Functions.subString(paragraph, Len.PARAGRAPH);
	}

	public String getParagraph() {
		return this.paragraph;
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public String getNinAdrId() {
		throw new FieldNotMappedException("ninAdrId");
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		throw new FieldNotMappedException("ninAdrId");
	}

	@Override
	public String getNinCltId() {
		throw new FieldNotMappedException("ninCltId");
	}

	@Override
	public void setNinCltId(String ninCltId) {
		throw new FieldNotMappedException("ninCltId");
	}

	@Override
	public String getNotEffDtObj() {
		return getNotEffDt();
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		setNotEffDt(notEffDtObj);
	}

	@Override
	public char getPolBilStaCd() {
		throw new FieldNotMappedException("polBilStaCd");
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		throw new FieldNotMappedException("polBilStaCd");
	}

	@Override
	public Character getPolBilStaCdObj() {
		return (getPolBilStaCd());
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		setPolBilStaCd((polBilStaCdObj));
	}

	@Override
	public AfDecimal getPolDueAmt() {
		throw new FieldNotMappedException("polDueAmt");
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		throw new FieldNotMappedException("polDueAmt");
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		return getPolDueAmt().toString();
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
	}

	@Override
	public String getPolEffDt() {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		throw new FieldNotMappedException("polEffDt");
	}

	@Override
	public String getPolExpDt() {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		throw new FieldNotMappedException("polExpDt");
	}

	@Override
	public String getPolPriRskStAbb() {
		throw new FieldNotMappedException("polPriRskStAbb");
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		throw new FieldNotMappedException("polPriRskStAbb");
	}

	@Override
	public String getPolTypCd() {
		throw new FieldNotMappedException("polTypCd");
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		throw new FieldNotMappedException("polTypCd");
	}

	public SaReturnCode getReturnCode() {
		return returnCode;
	}

	public SaSqlcode getSqlcode() {
		return sqlcode;
	}

	@Override
	public char getWfStartedInd() {
		throw new FieldNotMappedException("wfStartedInd");
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		throw new FieldNotMappedException("wfStartedInd");
	}

	@Override
	public Character getWfStartedIndObj() {
		return (getWfStartedInd());
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		setWfStartedInd((wfStartedIndObj));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NOT_EFF_DT = 10;
		public static final int POL_NBR = 25;
		public static final int NOT_PRC_TS = 26;
		public static final int ACCOUNT_NBR = 9;
		public static final int PARAGRAPH = 5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
