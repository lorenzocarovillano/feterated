/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IHalLokObjDetV;
import com.federatedinsurance.crs.copy.DclhalLokObjDet;
import com.federatedinsurance.crs.copy.DclhalLokObjHdr;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.Hallukrp;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALRLOMG<br>
 * Generated as a class for rule WS.<br>*/
public class HalrlomgData implements IHalLokObjDetV {

	//==== PROPERTIES ====
	//Original name: DCLHAL-LOK-OBJ-DET
	private DclhalLokObjDet dclhalLokObjDet = new DclhalLokObjDet();
	//Original name: DCLHAL-LOK-OBJ-HDR
	private DclhalLokObjHdr dclhalLokObjHdr = new DclhalLokObjHdr();
	//Original name: WS-WORK-AREAS
	private WsWorkAreasHalrlomg wsWorkAreas = new WsWorkAreasHalrlomg();
	//Original name: WS-WORKFIELDS
	private WsWorkfieldsHalrlomg wsWorkfields = new WsWorkfieldsHalrlomg();
	//Original name: WS-TSQ-FIELDS
	private WsTsqFields wsTsqFields = new WsTsqFields();
	//Original name: WS-LOCK-TCH-KEY
	private String wsLockTchKey = DefaultValues.stringVal(Len.WS_LOCK_TCH_KEY);
	//Original name: WS-LOCK-APPID
	private String wsLockAppid = DefaultValues.stringVal(Len.WS_LOCK_APPID);
	//Original name: WS-LOCK-TYPE-CODE
	private char wsLockTypeCode = DefaultValues.CHAR_VAL;
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: HALLUKRP
	private Hallukrp hallukrp = new Hallukrp();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void initWsUowLockTsqLayoutSpaces() {
		initWsUowLockRowSpaces();
	}

	public void setWsUowLockRowBytes(byte[] buffer) {
		setWsUowLockRowBytes(buffer, 1);
	}

	/**Original name: WS-UOW-LOCK-ROW<br>*/
	public byte[] getWsUowLockRowBytes() {
		byte[] buffer = new byte[Len.WS_UOW_LOCK_ROW];
		return getWsUowLockRowBytes(buffer, 1);
	}

	public void setWsUowLockRowBytes(byte[] buffer, int offset) {
		int position = offset;
		wsLockTchKey = MarshalByte.readString(buffer, position, Len.WS_LOCK_TCH_KEY);
		position += Len.WS_LOCK_TCH_KEY;
		wsLockAppid = MarshalByte.readString(buffer, position, Len.WS_LOCK_APPID);
		position += Len.WS_LOCK_APPID;
		wsLockTypeCode = MarshalByte.readChar(buffer, position);
	}

	public byte[] getWsUowLockRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, wsLockTchKey, Len.WS_LOCK_TCH_KEY);
		position += Len.WS_LOCK_TCH_KEY;
		MarshalByte.writeString(buffer, position, wsLockAppid, Len.WS_LOCK_APPID);
		position += Len.WS_LOCK_APPID;
		MarshalByte.writeChar(buffer, position, wsLockTypeCode);
		return buffer;
	}

	public void initWsUowLockRowSpaces() {
		wsLockTchKey = "";
		wsLockAppid = "";
		wsLockTypeCode = Types.SPACE_CHAR;
	}

	public void setWsLockTchKey(String wsLockTchKey) {
		this.wsLockTchKey = Functions.subString(wsLockTchKey, Len.WS_LOCK_TCH_KEY);
	}

	public String getWsLockTchKey() {
		return this.wsLockTchKey;
	}

	public void setWsLockAppid(String wsLockAppid) {
		this.wsLockAppid = Functions.subString(wsLockAppid, Len.WS_LOCK_APPID);
	}

	public String getWsLockAppid() {
		return this.wsLockAppid;
	}

	public void setWsLockTypeCode(char wsLockTypeCode) {
		this.wsLockTypeCode = wsLockTypeCode;
	}

	public void setWsLockTypeCodeFormatted(String wsLockTypeCode) {
		setWsLockTypeCode(Functions.charAt(wsLockTypeCode, Types.CHAR_SIZE));
	}

	public char getWsLockTypeCode() {
		return this.wsLockTypeCode;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public String getAppId() {
		return dclhalLokObjDet.getAppId();
	}

	@Override
	public void setAppId(String appId) {
		this.dclhalLokObjDet.setAppId(appId);
	}

	public DclhalLokObjDet getDclhalLokObjDet() {
		return dclhalLokObjDet;
	}

	public DclhalLokObjHdr getDclhalLokObjHdr() {
		return dclhalLokObjHdr;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public Hallukrp getHallukrp() {
		return hallukrp;
	}

	@Override
	public char getLokTypeCd() {
		return dclhalLokObjDet.getLokTypeCd();
	}

	@Override
	public void setLokTypeCd(char lokTypeCd) {
		this.dclhalLokObjDet.setLokTypeCd(lokTypeCd);
	}

	@Override
	public String getSessionId() {
		return dclhalLokObjDet.getSessionId();
	}

	@Override
	public void setSessionId(String sessionId) {
		this.dclhalLokObjDet.setSessionId(sessionId);
	}

	@Override
	public String getTableNm() {
		throw new FieldNotMappedException("tableNm");
	}

	@Override
	public void setTableNm(String tableNm) {
		throw new FieldNotMappedException("tableNm");
	}

	@Override
	public String getTchKey() {
		return dclhalLokObjDet.getTchKey();
	}

	@Override
	public void setTchKey(String tchKey) {
		this.dclhalLokObjDet.setTchKey(tchKey);
	}

	@Override
	public String getUpdByMsgId() {
		return dclhalLokObjDet.getUpdByMsgId();
	}

	@Override
	public void setUpdByMsgId(String updByMsgId) {
		this.dclhalLokObjDet.setUpdByMsgId(updByMsgId);
	}

	@Override
	public String getUserId() {
		throw new FieldNotMappedException("userId");
	}

	@Override
	public void setUserId(String userId) {
		throw new FieldNotMappedException("userId");
	}

	@Override
	public String getUserid() {
		return dclhalLokObjDet.getUserid();
	}

	@Override
	public void setUserid(String userid) {
		this.dclhalLokObjDet.setUserid(userid);
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	@Override
	public short getWsTmoValue() {
		return wsWorkfields.getTmoValue();
	}

	@Override
	public void setWsTmoValue(short wsTmoValue) {
		this.wsWorkfields.setTmoValue(wsTmoValue);
	}

	public WsTsqFields getWsTsqFields() {
		return wsTsqFields;
	}

	public WsWorkAreasHalrlomg getWsWorkAreas() {
		return wsWorkAreas;
	}

	public WsWorkfieldsHalrlomg getWsWorkfields() {
		return wsWorkfields;
	}

	@Override
	public String getZappedBy() {
		return dclhalLokObjDet.getZappedBy();
	}

	@Override
	public void setZappedBy(String zappedBy) {
		this.dclhalLokObjDet.setZappedBy(zappedBy);
	}

	@Override
	public char getZappedInd() {
		return dclhalLokObjDet.getZappedInd();
	}

	@Override
	public void setZappedInd(char zappedInd) {
		this.dclhalLokObjDet.setZappedInd(zappedInd);
	}

	@Override
	public String getZappedTs() {
		return dclhalLokObjDet.getZappedTs();
	}

	@Override
	public void setZappedTs(String zappedTs) {
		this.dclhalLokObjDet.setZappedTs(zappedTs);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_LOCK_TCH_KEY = 126;
		public static final int WS_LOCK_APPID = 10;
		public static final int WS_APPLID = 8;
		public static final int WS_LOCK_TYPE_CODE = 1;
		public static final int WS_UOW_LOCK_ROW = WS_LOCK_TCH_KEY + WS_LOCK_APPID + WS_LOCK_TYPE_CODE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
