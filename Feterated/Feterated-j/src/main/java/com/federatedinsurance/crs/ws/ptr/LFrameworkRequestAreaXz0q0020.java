/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q0020<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q0020 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q0020() {
	}

	public LFrameworkRequestAreaXz0q0020(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc008ActNotPolRecRowFormatted(String data) {
		writeString(Pos.XZC008_ACT_NOT_POL_REC_ROW, data, Len.XZC008_ACT_NOT_POL_REC_ROW);
	}

	public String getXzc008ActNotPolRecRowFormatted() {
		return readFixedString(Pos.XZC008_ACT_NOT_POL_REC_ROW, Len.XZC008_ACT_NOT_POL_REC_ROW);
	}

	public void setXzc008qActNotPolRecCsumFormatted(String xzc008qActNotPolRecCsum) {
		writeString(Pos.XZC008_ACT_NOT_POL_REC_CSUM, Trunc.toUnsignedNumeric(xzc008qActNotPolRecCsum, Len.XZC008_ACT_NOT_POL_REC_CSUM),
				Len.XZC008_ACT_NOT_POL_REC_CSUM);
	}

	/**Original name: XZC008Q-ACT-NOT-POL-REC-CSUM<br>*/
	public int getXzc008qActNotPolRecCsum() {
		return readNumDispUnsignedInt(Pos.XZC008_ACT_NOT_POL_REC_CSUM, Len.XZC008_ACT_NOT_POL_REC_CSUM);
	}

	public String getXzc008rActNotPolRecCsumFormatted() {
		return readFixedString(Pos.XZC008_ACT_NOT_POL_REC_CSUM, Len.XZC008_ACT_NOT_POL_REC_CSUM);
	}

	public void setXzc008qCsrActNbrKcre(String xzc008qCsrActNbrKcre) {
		writeString(Pos.XZC008_CSR_ACT_NBR_KCRE, xzc008qCsrActNbrKcre, Len.XZC008_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC008Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc008qCsrActNbrKcre() {
		return readString(Pos.XZC008_CSR_ACT_NBR_KCRE, Len.XZC008_CSR_ACT_NBR_KCRE);
	}

	public void setXzc008qNotPrcTsKcre(String xzc008qNotPrcTsKcre) {
		writeString(Pos.XZC008_NOT_PRC_TS_KCRE, xzc008qNotPrcTsKcre, Len.XZC008_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC008Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc008qNotPrcTsKcre() {
		return readString(Pos.XZC008_NOT_PRC_TS_KCRE, Len.XZC008_NOT_PRC_TS_KCRE);
	}

	public void setXzc008qPolNbrKcre(String xzc008qPolNbrKcre) {
		writeString(Pos.XZC008_POL_NBR_KCRE, xzc008qPolNbrKcre, Len.XZC008_POL_NBR_KCRE);
	}

	/**Original name: XZC008Q-POL-NBR-KCRE<br>*/
	public String getXzc008qPolNbrKcre() {
		return readString(Pos.XZC008_POL_NBR_KCRE, Len.XZC008_POL_NBR_KCRE);
	}

	public void setXzc008qRecSeqNbrKcre(String xzc008qRecSeqNbrKcre) {
		writeString(Pos.XZC008_REC_SEQ_NBR_KCRE, xzc008qRecSeqNbrKcre, Len.XZC008_REC_SEQ_NBR_KCRE);
	}

	/**Original name: XZC008Q-REC-SEQ-NBR-KCRE<br>*/
	public String getXzc008qRecSeqNbrKcre() {
		return readString(Pos.XZC008_REC_SEQ_NBR_KCRE, Len.XZC008_REC_SEQ_NBR_KCRE);
	}

	public void setXzc008qTransProcessDt(String xzc008qTransProcessDt) {
		writeString(Pos.XZC008_TRANS_PROCESS_DT, xzc008qTransProcessDt, Len.XZC008_TRANS_PROCESS_DT);
	}

	/**Original name: XZC008Q-TRANS-PROCESS-DT<br>*/
	public String getXzc008qTransProcessDt() {
		return readString(Pos.XZC008_TRANS_PROCESS_DT, Len.XZC008_TRANS_PROCESS_DT);
	}

	public void setXzc008qCsrActNbr(String xzc008qCsrActNbr) {
		writeString(Pos.XZC008_CSR_ACT_NBR, xzc008qCsrActNbr, Len.XZC008_CSR_ACT_NBR);
	}

	/**Original name: XZC008Q-CSR-ACT-NBR<br>*/
	public String getXzc008qCsrActNbr() {
		return readString(Pos.XZC008_CSR_ACT_NBR, Len.XZC008_CSR_ACT_NBR);
	}

	public void setXzc008qNotPrcTs(String xzc008qNotPrcTs) {
		writeString(Pos.XZC008_NOT_PRC_TS, xzc008qNotPrcTs, Len.XZC008_NOT_PRC_TS);
	}

	/**Original name: XZC008Q-NOT-PRC-TS<br>*/
	public String getXzc008qNotPrcTs() {
		return readString(Pos.XZC008_NOT_PRC_TS, Len.XZC008_NOT_PRC_TS);
	}

	public void setXzc008qPolNbr(String xzc008qPolNbr) {
		writeString(Pos.XZC008_POL_NBR, xzc008qPolNbr, Len.XZC008_POL_NBR);
	}

	/**Original name: XZC008Q-POL-NBR<br>*/
	public String getXzc008qPolNbr() {
		return readString(Pos.XZC008_POL_NBR, Len.XZC008_POL_NBR);
	}

	public void setXzc008qRecSeqNbrSign(char xzc008qRecSeqNbrSign) {
		writeChar(Pos.XZC008_REC_SEQ_NBR_SIGN, xzc008qRecSeqNbrSign);
	}

	/**Original name: XZC008Q-REC-SEQ-NBR-SIGN<br>*/
	public char getXzc008qRecSeqNbrSign() {
		return readChar(Pos.XZC008_REC_SEQ_NBR_SIGN);
	}

	public void setXzc008qRecSeqNbrFormatted(String xzc008qRecSeqNbr) {
		writeString(Pos.XZC008_REC_SEQ_NBR, Trunc.toUnsignedNumeric(xzc008qRecSeqNbr, Len.XZC008_REC_SEQ_NBR), Len.XZC008_REC_SEQ_NBR);
	}

	/**Original name: XZC008Q-REC-SEQ-NBR<br>*/
	public int getXzc008qRecSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC008_REC_SEQ_NBR, Len.XZC008_REC_SEQ_NBR);
	}

	public String getXzc008rRecSeqNbrFormatted() {
		return readFixedString(Pos.XZC008_REC_SEQ_NBR, Len.XZC008_REC_SEQ_NBR);
	}

	public void setXzc008qCsrActNbrCi(char xzc008qCsrActNbrCi) {
		writeChar(Pos.XZC008_CSR_ACT_NBR_CI, xzc008qCsrActNbrCi);
	}

	public void setXzc008qCsrActNbrCiFormatted(String xzc008qCsrActNbrCi) {
		setXzc008qCsrActNbrCi(Functions.charAt(xzc008qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC008Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc008qCsrActNbrCi() {
		return readChar(Pos.XZC008_CSR_ACT_NBR_CI);
	}

	public void setXzc008qNotPrcTsCi(char xzc008qNotPrcTsCi) {
		writeChar(Pos.XZC008_NOT_PRC_TS_CI, xzc008qNotPrcTsCi);
	}

	public void setXzc008qNotPrcTsCiFormatted(String xzc008qNotPrcTsCi) {
		setXzc008qNotPrcTsCi(Functions.charAt(xzc008qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC008Q-NOT-PRC-TS-CI<br>*/
	public char getXzc008qNotPrcTsCi() {
		return readChar(Pos.XZC008_NOT_PRC_TS_CI);
	}

	public void setXzc008qPolNbrCi(char xzc008qPolNbrCi) {
		writeChar(Pos.XZC008_POL_NBR_CI, xzc008qPolNbrCi);
	}

	public void setXzc008qPolNbrCiFormatted(String xzc008qPolNbrCi) {
		setXzc008qPolNbrCi(Functions.charAt(xzc008qPolNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC008Q-POL-NBR-CI<br>*/
	public char getXzc008qPolNbrCi() {
		return readChar(Pos.XZC008_POL_NBR_CI);
	}

	public void setXzc008qRecSeqNbrCi(char xzc008qRecSeqNbrCi) {
		writeChar(Pos.XZC008_REC_SEQ_NBR_CI, xzc008qRecSeqNbrCi);
	}

	public void setXzc008qRecSeqNbrCiFormatted(String xzc008qRecSeqNbrCi) {
		setXzc008qRecSeqNbrCi(Functions.charAt(xzc008qRecSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC008Q-REC-SEQ-NBR-CI<br>*/
	public char getXzc008qRecSeqNbrCi() {
		return readChar(Pos.XZC008_REC_SEQ_NBR_CI);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0008 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC008_ACT_NOT_POL_REC_ROW = L_FW_REQ_XZ0C0008;
		public static final int XZC008_ACT_NOT_POL_REC_FIXED = XZC008_ACT_NOT_POL_REC_ROW;
		public static final int XZC008_ACT_NOT_POL_REC_CSUM = XZC008_ACT_NOT_POL_REC_FIXED;
		public static final int XZC008_CSR_ACT_NBR_KCRE = XZC008_ACT_NOT_POL_REC_CSUM + Len.XZC008_ACT_NOT_POL_REC_CSUM;
		public static final int XZC008_NOT_PRC_TS_KCRE = XZC008_CSR_ACT_NBR_KCRE + Len.XZC008_CSR_ACT_NBR_KCRE;
		public static final int XZC008_POL_NBR_KCRE = XZC008_NOT_PRC_TS_KCRE + Len.XZC008_NOT_PRC_TS_KCRE;
		public static final int XZC008_REC_SEQ_NBR_KCRE = XZC008_POL_NBR_KCRE + Len.XZC008_POL_NBR_KCRE;
		public static final int XZC008_ACT_NOT_POL_REC_DATES = XZC008_REC_SEQ_NBR_KCRE + Len.XZC008_REC_SEQ_NBR_KCRE;
		public static final int XZC008_TRANS_PROCESS_DT = XZC008_ACT_NOT_POL_REC_DATES;
		public static final int XZC008_ACT_NOT_POL_REC_KEY = XZC008_TRANS_PROCESS_DT + Len.XZC008_TRANS_PROCESS_DT;
		public static final int XZC008_CSR_ACT_NBR = XZC008_ACT_NOT_POL_REC_KEY;
		public static final int XZC008_NOT_PRC_TS = XZC008_CSR_ACT_NBR + Len.XZC008_CSR_ACT_NBR;
		public static final int XZC008_POL_NBR = XZC008_NOT_PRC_TS + Len.XZC008_NOT_PRC_TS;
		public static final int XZC008_REC_SEQ_NBR_SIGN = XZC008_POL_NBR + Len.XZC008_POL_NBR;
		public static final int XZC008_REC_SEQ_NBR = XZC008_REC_SEQ_NBR_SIGN + Len.XZC008_REC_SEQ_NBR_SIGN;
		public static final int XZC008_ACT_NOT_POL_REC_KEY_CI = XZC008_REC_SEQ_NBR + Len.XZC008_REC_SEQ_NBR;
		public static final int XZC008_CSR_ACT_NBR_CI = XZC008_ACT_NOT_POL_REC_KEY_CI;
		public static final int XZC008_NOT_PRC_TS_CI = XZC008_CSR_ACT_NBR_CI + Len.XZC008_CSR_ACT_NBR_CI;
		public static final int XZC008_POL_NBR_CI = XZC008_NOT_PRC_TS_CI + Len.XZC008_NOT_PRC_TS_CI;
		public static final int XZC008_REC_SEQ_NBR_CI = XZC008_POL_NBR_CI + Len.XZC008_POL_NBR_CI;
		public static final int XZC008_ACT_NOT_POL_REC_DATA = XZC008_REC_SEQ_NBR_CI + Len.XZC008_REC_SEQ_NBR_CI;
		public static final int FLR1 = XZC008_ACT_NOT_POL_REC_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC008_ACT_NOT_POL_REC_CSUM = 9;
		public static final int XZC008_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC008_NOT_PRC_TS_KCRE = 32;
		public static final int XZC008_POL_NBR_KCRE = 32;
		public static final int XZC008_REC_SEQ_NBR_KCRE = 32;
		public static final int XZC008_TRANS_PROCESS_DT = 10;
		public static final int XZC008_CSR_ACT_NBR = 9;
		public static final int XZC008_NOT_PRC_TS = 26;
		public static final int XZC008_POL_NBR = 25;
		public static final int XZC008_REC_SEQ_NBR_SIGN = 1;
		public static final int XZC008_REC_SEQ_NBR = 5;
		public static final int XZC008_CSR_ACT_NBR_CI = 1;
		public static final int XZC008_NOT_PRC_TS_CI = 1;
		public static final int XZC008_POL_NBR_CI = 1;
		public static final int XZC008_REC_SEQ_NBR_CI = 1;
		public static final int XZC008_ACT_NOT_POL_REC_FIXED = XZC008_ACT_NOT_POL_REC_CSUM + XZC008_CSR_ACT_NBR_KCRE + XZC008_NOT_PRC_TS_KCRE
				+ XZC008_POL_NBR_KCRE + XZC008_REC_SEQ_NBR_KCRE;
		public static final int XZC008_ACT_NOT_POL_REC_DATES = XZC008_TRANS_PROCESS_DT;
		public static final int XZC008_ACT_NOT_POL_REC_KEY = XZC008_CSR_ACT_NBR + XZC008_NOT_PRC_TS + XZC008_POL_NBR + XZC008_REC_SEQ_NBR_SIGN
				+ XZC008_REC_SEQ_NBR;
		public static final int XZC008_ACT_NOT_POL_REC_KEY_CI = XZC008_CSR_ACT_NBR_CI + XZC008_NOT_PRC_TS_CI + XZC008_POL_NBR_CI
				+ XZC008_REC_SEQ_NBR_CI;
		public static final int FLR1 = 1;
		public static final int XZC008_ACT_NOT_POL_REC_DATA = FLR1;
		public static final int XZC008_ACT_NOT_POL_REC_ROW = XZC008_ACT_NOT_POL_REC_FIXED + XZC008_ACT_NOT_POL_REC_DATES + XZC008_ACT_NOT_POL_REC_KEY
				+ XZC008_ACT_NOT_POL_REC_KEY_CI + XZC008_ACT_NOT_POL_REC_DATA;
		public static final int L_FW_REQ_XZ0C0008 = XZC008_ACT_NOT_POL_REC_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0008;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
