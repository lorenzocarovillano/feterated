/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-INSUFFICIENT-DAYS<br>
 * Variable: EA-01-INSUFFICIENT-DAYS from program XZ0P90K0<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01InsufficientDays {

	//==== PROPERTIES ====
	/**Original name: FILLER-EA-01-INSUFFICIENT-DAYS<br>
	 * <pre>*    DO NOT CHANGE THIS ERROR MESSAGE WITHOUT
	 * *    CONTACTING THE BUSINESSWORKS TEAM.
	 * * THIS ERROR MESSAGE CAN ONLY BE USED WHEN THE
	 * * CALCULATED NOTIFICATION EFFECTIVE DATE IS
	 * * GREATER THAN OR EQUAL TO THE POLICY EXPIRATION DATE</pre>*/
	private String flr1 = "Insufficient";
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-1
	private String flr2 = "days to";
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-2
	private String flr3 = "expire.";
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-3
	private String flr4 = "Account =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-4
	private String flr5 = " CNDT=";
	//Original name: EA-01-NOT-EFF-DT-MM
	private String notEffDtMm = DefaultValues.stringVal(Len.NOT_EFF_DT_MM);
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-5
	private char flr6 = '/';
	//Original name: EA-01-NOT-EFF-DT-DD
	private String notEffDtDd = DefaultValues.stringVal(Len.NOT_EFF_DT_DD);
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-6
	private char flr7 = '/';
	//Original name: EA-01-NOT-EFF-DT-YYYY
	private String notEffDtYyyy = DefaultValues.stringVal(Len.NOT_EFF_DT_YYYY);
	//Original name: FILLER-EA-01-INSUFFICIENT-DAYS-7
	private char flr8 = '.';

	//==== METHODS ====
	public String getEa01InsufficientDaysFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InsufficientDaysBytes());
	}

	public byte[] getEa01InsufficientDaysBytes() {
		byte[] buffer = new byte[Len.EA01_INSUFFICIENT_DAYS];
		return getEa01InsufficientDaysBytes(buffer, 1);
	}

	public byte[] getEa01InsufficientDaysBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, notEffDtMm, Len.NOT_EFF_DT_MM);
		position += Len.NOT_EFF_DT_MM;
		MarshalByte.writeChar(buffer, position, flr6);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, notEffDtDd, Len.NOT_EFF_DT_DD);
		position += Len.NOT_EFF_DT_DD;
		MarshalByte.writeChar(buffer, position, flr7);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, notEffDtYyyy, Len.NOT_EFF_DT_YYYY);
		position += Len.NOT_EFF_DT_YYYY;
		MarshalByte.writeChar(buffer, position, flr8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setNotEffDtMm(String notEffDtMm) {
		this.notEffDtMm = Functions.subString(notEffDtMm, Len.NOT_EFF_DT_MM);
	}

	public String getNotEffDtMm() {
		return this.notEffDtMm;
	}

	public char getFlr6() {
		return this.flr6;
	}

	public void setNotEffDtDd(String notEffDtDd) {
		this.notEffDtDd = Functions.subString(notEffDtDd, Len.NOT_EFF_DT_DD);
	}

	public String getNotEffDtDd() {
		return this.notEffDtDd;
	}

	public char getFlr7() {
		return this.flr7;
	}

	public void setNotEffDtYyyy(String notEffDtYyyy) {
		this.notEffDtYyyy = Functions.subString(notEffDtYyyy, Len.NOT_EFF_DT_YYYY);
	}

	public String getNotEffDtYyyy() {
		return this.notEffDtYyyy;
	}

	public char getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_EFF_DT_MM = 2;
		public static final int NOT_EFF_DT_DD = 2;
		public static final int NOT_EFF_DT_YYYY = 4;
		public static final int FLR1 = 13;
		public static final int FLR2 = 8;
		public static final int FLR3 = 9;
		public static final int FLR4 = 10;
		public static final int FLR5 = 6;
		public static final int FLR6 = 1;
		public static final int EA01_INSUFFICIENT_DAYS = CSR_ACT_NBR + NOT_EFF_DT_MM + NOT_EFF_DT_DD + NOT_EFF_DT_YYYY + FLR1 + FLR2 + FLR3 + FLR4
				+ FLR5 + 3 * FLR6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
