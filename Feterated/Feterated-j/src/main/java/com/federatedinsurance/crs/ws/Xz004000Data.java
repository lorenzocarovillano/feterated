/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.commons.data.to.IWcNotRpt;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclpolDtaExt;
import com.federatedinsurance.crs.copy.DclwcNotRpt;
import com.federatedinsurance.crs.copy.WtPolEffYyyyMmDd;
import com.federatedinsurance.crs.copy.WtWcPrcMmDdYyyy;
import com.federatedinsurance.crs.copy.Xzc00401;
import com.federatedinsurance.crs.ws.enums.WtTrnCd;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ004000<br>
 * Generated as a class for rule WS.<br>*/
public class Xz004000Data implements IWcNotRpt {

	//==== PROPERTIES ====
	//Original name: AA-TRN-CRS-SEQ-NBR
	private short aaTrnCrsSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz004000 constantFields = new ConstantFieldsXz004000();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz004000 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz004000();
	//Original name: NI-WNR-NOT-EFF-DT
	private short niWnrNotEffDt = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-WNR-NOT-TYP-CD
	private short niWnrNotTypCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: PRINT-LINE
	private PrintLine printLine = new PrintLine();
	//Original name: FILLER-RP-PRTR
	private String flr1 = "    PRTR";
	//Original name: SAVE-AREA
	private SaveAreaXz004000 saveArea = new SaveAreaXz004000();
	//Original name: SWITCHES
	private SwitchesXz004000 switches = new SwitchesXz004000();
	//Original name: XZC00401
	private Xzc00401 xzc00401 = new Xzc00401();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLPOL-DTA-EXT
	private DclpolDtaExt dclpolDtaExt = new DclpolDtaExt();
	//Original name: DCLWC-NOT-RPT
	private DclwcNotRpt dclwcNotRpt = new DclwcNotRpt();

	//==== METHODS ====
	public void setAaTrnCrsSeqNbr(short aaTrnCrsSeqNbr) {
		this.aaTrnCrsSeqNbr = aaTrnCrsSeqNbr;
	}

	public short getAaTrnCrsSeqNbr() {
		return this.aaTrnCrsSeqNbr;
	}

	public void setNiWnrNotEffDt(short niWnrNotEffDt) {
		this.niWnrNotEffDt = niWnrNotEffDt;
	}

	public short getNiWnrNotEffDt() {
		return this.niWnrNotEffDt;
	}

	public void setNiWnrNotTypCd(short niWnrNotTypCd) {
		this.niWnrNotTypCd = niWnrNotTypCd;
	}

	public short getNiWnrNotTypCd() {
		return this.niWnrNotTypCd;
	}

	/**Original name: RP-PRTR<br>*/
	public byte[] getRpPrtrBytes() {
		byte[] buffer = new byte[Len.RP_PRTR];
		return getRpPrtrBytes(buffer, 1);
	}

	public byte[] getRpPrtrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getWcTriggerRecordFormatted() {
		return MarshalByteExt.bufferToStr(getWcTriggerRecordBytes());
	}

	/**Original name: WC-TRIGGER-RECORD<br>*/
	public byte[] getWcTriggerRecordBytes() {
		byte[] buffer = new byte[Len.WC_TRIGGER_RECORD];
		return getWcTriggerRecordBytes(buffer, 1);
	}

	public byte[] getWcTriggerRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc00401.polNbr, Xzc00401.Len.POL_NBR);
		position += Xzc00401.Len.POL_NBR;
		xzc00401.getPolEffYyyyMmDd().getPolEffYyyyMmDdBytes(buffer, position);
		position += WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD;
		MarshalByte.writeString(buffer, position, xzc00401.getIssAcyTs(), Xzc00401.Len.ISS_ACY_TS);
		position += Xzc00401.Len.ISS_ACY_TS;
		xzc00401.getTrnPrcYyyyMmDd().getPolEffYyyyMmDdBytes(buffer, position);
		position += WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD;
		xzc00401.getTrnEffYyyyMmDd().getPolEffYyyyMmDdBytes(buffer, position);
		position += WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD;
		MarshalByte.writeString(buffer, position, xzc00401.getTrnCd().getTrnCd(), WtTrnCd.Len.TRN_CD);
		position += WtTrnCd.Len.TRN_CD;
		xzc00401.getWcPrcMmDdYyyy().getWcPrcMmDdYyyyBytes(buffer, position);
		position += WtWcPrcMmDdYyyy.Len.WC_PRC_MM_DD_YYYY;
		MarshalByte.writeChar(buffer, position, xzc00401.getTrnCrsInitInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, xzc00401.trnCrsSeqNbr, Xzc00401.Len.TRN_CRS_SEQ_NBR);
		position += Xzc00401.Len.TRN_CRS_SEQ_NBR;
		MarshalByte.writeString(buffer, position, xzc00401.getFlr1(), Xzc00401.Len.FLR1);
		return buffer;
	}

	public void initWcTriggerRecordSpaces() {
		xzc00401.initXzc00401Spaces();
	}

	public ConstantFieldsXz004000 getConstantFields() {
		return constantFields;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclpolDtaExt getDclpolDtaExt() {
		return dclpolDtaExt;
	}

	public DclwcNotRpt getDclwcNotRpt() {
		return dclwcNotRpt;
	}

	public ErrorAndAdviceMessagesXz004000 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	@Override
	public String getNotEffDt() {
		return dclwcNotRpt.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.dclwcNotRpt.setNotEffDt(notEffDt);
	}

	@Override
	public String getNotEffDtObj() {
		if (getNiWnrNotEffDt() >= 0) {
			return getNotEffDt();
		} else {
			return null;
		}
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		if (notEffDtObj != null) {
			setNotEffDt(notEffDtObj);
			setNiWnrNotEffDt(((short) 0));
		} else {
			setNiWnrNotEffDt(((short) -1));
		}
	}

	@Override
	public String getNotTypCd() {
		return dclwcNotRpt.getNotTypCd();
	}

	@Override
	public void setNotTypCd(String notTypCd) {
		this.dclwcNotRpt.setNotTypCd(notTypCd);
	}

	@Override
	public String getNotTypCdObj() {
		if (getNiWnrNotTypCd() >= 0) {
			return getNotTypCd();
		} else {
			return null;
		}
	}

	@Override
	public void setNotTypCdObj(String notTypCdObj) {
		if (notTypCdObj != null) {
			setNotTypCd(notTypCdObj);
			setNiWnrNotTypCd(((short) 0));
		} else {
			setNiWnrNotTypCd(((short) -1));
		}
	}

	@Override
	public String getPolEffDt() {
		return dclwcNotRpt.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.dclwcNotRpt.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolNbr() {
		return dclwcNotRpt.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclwcNotRpt.setPolNbr(polNbr);
	}

	public PrintLine getPrintLine() {
		return printLine;
	}

	public SaveAreaXz004000 getSaveArea() {
		return saveArea;
	}

	@Override
	public String getStaMdfTs() {
		return dclwcNotRpt.getStaMdfTs();
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		this.dclwcNotRpt.setStaMdfTs(staMdfTs);
	}

	public SwitchesXz004000 getSwitches() {
		return switches;
	}

	public Xzc00401 getXzc00401() {
		return xzc00401;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 12;
		public static final int RP_PRTR = FLR1;
		public static final int WC_TRIGGER_RECORD = Xzc00401.Len.POL_NBR + WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD + Xzc00401.Len.ISS_ACY_TS
				+ WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD + WtPolEffYyyyMmDd.Len.POL_EFF_YYYY_MM_DD + WtTrnCd.Len.TRN_CD
				+ WtWcPrcMmDdYyyy.Len.WC_PRC_MM_DD_YYYY + Xzc00401.Len.TRN_CRS_INIT_IND + Xzc00401.Len.TRN_CRS_SEQ_NBR + Xzc00401.Len.FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
