/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-FA-FORMATTED-ADDRESS-LINES<br>
 * Variable: L-FA-FORMATTED-ADDRESS-LINES from copybook TS52901<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class LFaFormattedAddressLines {

	//==== PROPERTIES ====
	//Original name: L-FA-FA-LINE-1
	private String line1 = DefaultValues.stringVal(Len.LINE1);
	//Original name: L-FA-FA-LINE-2
	private String line2 = DefaultValues.stringVal(Len.LINE2);
	//Original name: L-FA-FA-LINE-3
	private String line3 = DefaultValues.stringVal(Len.LINE3);
	//Original name: L-FA-FA-LINE-4
	private String line4 = DefaultValues.stringVal(Len.LINE4);
	//Original name: L-FA-FA-LINE-5
	private String line5 = DefaultValues.stringVal(Len.LINE5);
	//Original name: L-FA-FA-LINE-6
	private String line6 = DefaultValues.stringVal(Len.LINE6);
	//Original name: FILLER-L-FA-FORMATTED-ADDRESS-LINES
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setFormattedAddressLinesBytes(byte[] buffer) {
		setFormattedAddressLinesBytes(buffer, 1);
	}

	public void setFormattedAddressLinesBytes(byte[] buffer, int offset) {
		int position = offset;
		line1 = MarshalByte.readString(buffer, position, Len.LINE1);
		position += Len.LINE1;
		line2 = MarshalByte.readString(buffer, position, Len.LINE2);
		position += Len.LINE2;
		line3 = MarshalByte.readString(buffer, position, Len.LINE3);
		position += Len.LINE3;
		line4 = MarshalByte.readString(buffer, position, Len.LINE4);
		position += Len.LINE4;
		line5 = MarshalByte.readString(buffer, position, Len.LINE5);
		position += Len.LINE5;
		line6 = MarshalByte.readString(buffer, position, Len.LINE6);
		position += Len.LINE6;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getFormattedAddressLinesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, line1, Len.LINE1);
		position += Len.LINE1;
		MarshalByte.writeString(buffer, position, line2, Len.LINE2);
		position += Len.LINE2;
		MarshalByte.writeString(buffer, position, line3, Len.LINE3);
		position += Len.LINE3;
		MarshalByte.writeString(buffer, position, line4, Len.LINE4);
		position += Len.LINE4;
		MarshalByte.writeString(buffer, position, line5, Len.LINE5);
		position += Len.LINE5;
		MarshalByte.writeString(buffer, position, line6, Len.LINE6);
		position += Len.LINE6;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setLine1(String line1) {
		this.line1 = Functions.subString(line1, Len.LINE1);
	}

	public String getLine1() {
		return this.line1;
	}

	public void setLine2(String line2) {
		this.line2 = Functions.subString(line2, Len.LINE2);
	}

	public String getLine2() {
		return this.line2;
	}

	public void setLine3(String line3) {
		this.line3 = Functions.subString(line3, Len.LINE3);
	}

	public String getLine3() {
		return this.line3;
	}

	public void setLine4(String line4) {
		this.line4 = Functions.subString(line4, Len.LINE4);
	}

	public String getLine4() {
		return this.line4;
	}

	public void setLine5(String line5) {
		this.line5 = Functions.subString(line5, Len.LINE5);
	}

	public String getLine5() {
		return this.line5;
	}

	public void setLine6(String line6) {
		this.line6 = Functions.subString(line6, Len.LINE6);
	}

	public String getLine6() {
		return this.line6;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LINE1 = 45;
		public static final int LINE2 = 45;
		public static final int LINE3 = 45;
		public static final int LINE4 = 45;
		public static final int LINE5 = 45;
		public static final int LINE6 = 45;
		public static final int FLR1 = 100;
		public static final int FORMATTED_ADDRESS_LINES = LINE1 + LINE2 + LINE3 + LINE4 + LINE5 + LINE6 + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
