/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.WsCheckMonth;
import com.federatedinsurance.crs.ws.enums.WsDateFormat;
import com.federatedinsurance.crs.ws.enums.WsLeapYearSw;
import com.federatedinsurance.crs.ws.enums.WsTimeSw;
import com.federatedinsurance.crs.ws.redefines.WsDateArray;
import com.federatedinsurance.crs.ws.redefines.WsMonthLit;
import com.federatedinsurance.crs.ws.redefines.WsMonthTable;
import com.federatedinsurance.crs.ws.redefines.WsTimeArray;
import com.federatedinsurance.crs.ws.redefines.WsVarDateArray;
import com.federatedinsurance.crs.ws.redefines.WsWorkDay;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-WORK-FIELDS<br>
 * Variable: WS-WORK-FIELDS from program XPIODAT<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkFieldsXpiodat {

	//==== PROPERTIES ====
	//Original name: SQLCODE-DISP
	private String sqlcodeDisp = DefaultValues.stringVal(Len.SQLCODE_DISP);
	//Original name: WS-OUT-FORMAT
	private String wsOutFormat = DefaultValues.stringVal(Len.WS_OUT_FORMAT);
	private static final String[] DATE_FORMAT_AMPM = new String[] { "J", "G", "G1", "G7", "M", "M1", "M7", "D", "D1", "D7", "AD", "AM", "AS", "GU",
			"TI", "TU" };
	//Original name: WS-DATE-FORMAT
	private WsDateFormat wsDateFormat = new WsDateFormat();
	//Original name: WS-DATE-ADJ-FORMAT-RED
	private WsDateAdjFormatRed wsDateAdjFormatRed = new WsDateAdjFormatRed();
	//Original name: WS-WORK-DAY
	private WsWorkDay wsWorkDay = new WsWorkDay();
	//Original name: WS-WORK-MONTH
	private String wsWorkMonth = DefaultValues.stringVal(Len.WS_WORK_MONTH);
	//Original name: WS-WORK-YEAR-REDCY
	private WsWorkYearRedcy wsWorkYearRedcy = new WsWorkYearRedcy();
	//Original name: WS-WORK-TIME
	private WsWorkTime wsWorkTime = new WsWorkTime();
	//Original name: WS-DATE1-FIELDS
	private WsDate1Fields wsDate1Fields = new WsDate1Fields();
	//Original name: WS-DATE2-FIELDS
	private WsDate1Fields wsDate2Fields = new WsDate1Fields();
	//Original name: WS-VAR-DATE-ARRAY
	private WsVarDateArray wsVarDateArray = new WsVarDateArray();
	//Original name: WS-DATE-ARRAY
	private WsDateArray wsDateArray = new WsDateArray();
	//Original name: WS-TIME-ARRAY
	private WsTimeArray wsTimeArray = new WsTimeArray();
	//Original name: WS-CHECK-DAY
	private short wsCheckDay = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-MONTH
	private WsCheckMonth wsCheckMonth = new WsCheckMonth();
	//Original name: WS-CHECK-YEAR
	private short wsCheckYear = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-LEAP-YEAR
	private short wsCheckLeapYear = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-HOURS
	private short wsCheckHours = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-HOURS1
	private short wsCheckHours1 = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-HOURS2
	private short wsCheckHours2 = DefaultValues.SHORT_VAL;
	//Original name: WS-CHECK-AMPM
	private String wsCheckAmpm = DefaultValues.stringVal(Len.WS_CHECK_AMPM);
	//Original name: WS-DIVIDE-QUOTIENT
	private short wsDivideQuotient = DefaultValues.SHORT_VAL;
	//Original name: WS-DIVIDE-REMAINDER
	private short wsDivideRemainder = DefaultValues.SHORT_VAL;
	//Original name: WS-START
	private short wsStart = DefaultValues.SHORT_VAL;
	//Original name: WS-DAYS-COUNT
	private short wsDaysCount = DefaultValues.SHORT_VAL;
	//Original name: WS-DAY-OF-YEAR
	private short wsDayOfYear = DefaultValues.SHORT_VAL;
	//Original name: WS-DAYS-REMAINING
	private short wsDaysRemaining = DefaultValues.SHORT_VAL;
	//Original name: WS-MONTHS-REMAINING
	private short wsMonthsRemaining = DefaultValues.SHORT_VAL;
	//Original name: WS-DAYS-IN-YEAR
	private short wsDaysInYear = DefaultValues.SHORT_VAL;
	//Original name: WS-ADJUST-NUMBER
	private int wsAdjustNumber = DefaultValues.INT_VAL;
	//Original name: WS-ADJUSTED-DAY
	private int wsAdjustedDay = DefaultValues.INT_VAL;
	//Original name: WS-ADJUSTED-MONTH
	private int wsAdjustedMonth = DefaultValues.INT_VAL;
	//Original name: WS-ADJUSTED-YEAR
	private int wsAdjustedYear = DefaultValues.INT_VAL;
	//Original name: WS-CENTURY-DAY
	private long wsCenturyDay = DefaultValues.LONG_VAL;
	//Original name: WS-CENTURY-DAY1
	private long wsCenturyDay1 = DefaultValues.LONG_VAL;
	//Original name: WS-CENTURY-DAY2
	private long wsCenturyDay2 = DefaultValues.LONG_VAL;
	//Original name: WS-CENTURY-YEAR
	private long wsCenturyYear = DefaultValues.LONG_VAL;
	//Original name: WS-DIFFERENCE
	private long wsDifference = DefaultValues.LONG_VAL;
	//Original name: WS-ABSTIME-MSECS
	private int wsAbstimeMsecs = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SYSTEM-YEAR-B
	private int wsSystemYearB = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SYSTEM-MONTH-B
	private int wsSystemMonthB = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SYSTEM-DAY-B
	private int wsSystemDayB = DefaultValues.BIN_INT_VAL;
	//Original name: WS-SYSTEM-YEAR-RED
	private WsSystemYearRed wsSystemYearRed = new WsSystemYearRed();
	//Original name: WS-SYSTEM-MONTH
	private String wsSystemMonth = DefaultValues.stringVal(Len.WS_SYSTEM_MONTH);
	//Original name: WS-SYSTEM-DAY
	private String wsSystemDay = DefaultValues.stringVal(Len.WS_SYSTEM_DAY);
	//Original name: WS-SYSTEM-TIME-RED
	private WsSystemTimeRed wsSystemTimeRed = new WsSystemTimeRed();
	//Original name: WS-SYSTEM-MSECS
	private String wsSystemMsecs = DefaultValues.stringVal(Len.WS_SYSTEM_MSECS);
	//Original name: WS-COMMA
	private char wsComma = ',';
	//Original name: WS-DASH
	private char wsDash = '-';
	//Original name: WS-PERIOD
	private char wsPeriod = '.';
	//Original name: WS-NEGATIVE
	private char wsNegative = '-';
	//Original name: WS-POSITIVE
	private char wsPositive = '+';
	//Original name: WS-SLASH
	private char wsSlash = '/';
	//Original name: WS-DELIMITER
	private char wsDelimiter = DefaultValues.CHAR_VAL;
	//Original name: WS-TIME-SW
	private WsTimeSw wsTimeSw = new WsTimeSw();
	//Original name: WS-MONTH-SW
	private char wsMonthSw = DefaultValues.CHAR_VAL;
	public static final char WS_MONTH_FOUND = 'Y';
	//Original name: WS-DAYS-SW
	private char wsDaysSw = DefaultValues.CHAR_VAL;
	public static final char WS_DAYS_FOUND = 'Y';
	//Original name: WS-SWAP-DATE-SW
	private char wsSwapDateSw = DefaultValues.CHAR_VAL;
	public static final char WS_DATE_SWAPPED = 'Y';
	//Original name: WS-LEAP-YEAR-SW
	private WsLeapYearSw wsLeapYearSw = new WsLeapYearSw();
	//Original name: WS-MONTH-TABLE
	private WsMonthTable wsMonthTable = new WsMonthTable();
	//Original name: WS-MONTH-LIT
	private WsMonthLit wsMonthLit = new WsMonthLit();
	//Original name: WS-KEEP-ADJUST-SIGN
	private char wsKeepAdjustSign = DefaultValues.CHAR_VAL;
	//Original name: WS-KEEP-ADJUST-NUMB
	private String wsKeepAdjustNumb = DefaultValues.stringVal(Len.WS_KEEP_ADJUST_NUMB);
	//Original name: WS-SAVE-CHECK-YEAR
	private short wsSaveCheckYear = DefaultValues.SHORT_VAL;
	//Original name: WS-SAVE-CHECK-MONTH
	private short wsSaveCheckMonth = DefaultValues.SHORT_VAL;
	//Original name: WS-SAVE-CHECK-DAY
	private short wsSaveCheckDay = DefaultValues.SHORT_VAL;
	//Original name: WS-SAVE-CHECK-HOURS
	private short wsSaveCheckHours = DefaultValues.SHORT_VAL;

	//==== METHODS ====
	public void setSqlcodeDisp(long sqlcodeDisp) {
		this.sqlcodeDisp = PicFormatter.display("-9(3)").format(sqlcodeDisp).toString();
	}

	public long getSqlcodeDisp() {
		return PicParser.display("-9(3)").parseLong(this.sqlcodeDisp);
	}

	public String getSqlcodeDispFormatted() {
		return this.sqlcodeDisp;
	}

	public void setWsOutFormat(String wsOutFormat) {
		this.wsOutFormat = Functions.subString(wsOutFormat, Len.WS_OUT_FORMAT);
	}

	public String getWsOutFormat() {
		return this.wsOutFormat;
	}

	public boolean isDateFormatAmpm() {
		return ArrayUtils.contains(DATE_FORMAT_AMPM, wsOutFormat);
	}

	public void setWsWorkMonth(short wsWorkMonth) {
		this.wsWorkMonth = NumericDisplay.asString(wsWorkMonth, Len.WS_WORK_MONTH);
	}

	public void setWsWorkMonthFormatted(String wsWorkMonth) {
		this.wsWorkMonth = Trunc.toUnsignedNumeric(wsWorkMonth, Len.WS_WORK_MONTH);
	}

	public short getWsWorkMonth() {
		return NumericDisplay.asShort(this.wsWorkMonth);
	}

	public String getWsWorkMonthFormatted() {
		return this.wsWorkMonth;
	}

	public void setWsCheckDay(short wsCheckDay) {
		this.wsCheckDay = wsCheckDay;
	}

	public short getWsCheckDay() {
		return this.wsCheckDay;
	}

	public void setWsCheckYear(short wsCheckYear) {
		this.wsCheckYear = wsCheckYear;
	}

	public short getWsCheckYear() {
		return this.wsCheckYear;
	}

	public void setWsCheckLeapYear(short wsCheckLeapYear) {
		this.wsCheckLeapYear = wsCheckLeapYear;
	}

	public short getWsCheckLeapYear() {
		return this.wsCheckLeapYear;
	}

	public void setWsCheckHours(short wsCheckHours) {
		this.wsCheckHours = wsCheckHours;
	}

	public short getWsCheckHours() {
		return this.wsCheckHours;
	}

	public void setWsCheckHours1(short wsCheckHours1) {
		this.wsCheckHours1 = wsCheckHours1;
	}

	public short getWsCheckHours1() {
		return this.wsCheckHours1;
	}

	public void setWsCheckHours2(short wsCheckHours2) {
		this.wsCheckHours2 = wsCheckHours2;
	}

	public short getWsCheckHours2() {
		return this.wsCheckHours2;
	}

	public void setWsCheckAmpm(String wsCheckAmpm) {
		this.wsCheckAmpm = Functions.subString(wsCheckAmpm, Len.WS_CHECK_AMPM);
	}

	public String getWsCheckAmpm() {
		return this.wsCheckAmpm;
	}

	public void setWsDivideQuotient(short wsDivideQuotient) {
		this.wsDivideQuotient = wsDivideQuotient;
	}

	public short getWsDivideQuotient() {
		return this.wsDivideQuotient;
	}

	public void setWsDivideRemainder(short wsDivideRemainder) {
		this.wsDivideRemainder = wsDivideRemainder;
	}

	public short getWsDivideRemainder() {
		return this.wsDivideRemainder;
	}

	public void setWsStart(short wsStart) {
		this.wsStart = wsStart;
	}

	public short getWsStart() {
		return this.wsStart;
	}

	public void setWsDaysCount(short wsDaysCount) {
		this.wsDaysCount = wsDaysCount;
	}

	public short getWsDaysCount() {
		return this.wsDaysCount;
	}

	public void setWsDayOfYear(short wsDayOfYear) {
		this.wsDayOfYear = wsDayOfYear;
	}

	public short getWsDayOfYear() {
		return this.wsDayOfYear;
	}

	public void setWsDaysRemaining(short wsDaysRemaining) {
		this.wsDaysRemaining = wsDaysRemaining;
	}

	public short getWsDaysRemaining() {
		return this.wsDaysRemaining;
	}

	public void setWsMonthsRemaining(short wsMonthsRemaining) {
		this.wsMonthsRemaining = wsMonthsRemaining;
	}

	public short getWsMonthsRemaining() {
		return this.wsMonthsRemaining;
	}

	public void setWsDaysInYear(short wsDaysInYear) {
		this.wsDaysInYear = wsDaysInYear;
	}

	public short getWsDaysInYear() {
		return this.wsDaysInYear;
	}

	public void setWsAdjustNumber(int wsAdjustNumber) {
		this.wsAdjustNumber = wsAdjustNumber;
	}

	public void setWsAdjustNumberFromBuffer(byte[] buffer) {
		wsAdjustNumber = MarshalByte.readPackedAsInt(buffer, 1, Len.Int.WS_ADJUST_NUMBER, 0);
	}

	public int getWsAdjustNumber() {
		return this.wsAdjustNumber;
	}

	public void setWsAdjustedDay(int wsAdjustedDay) {
		this.wsAdjustedDay = wsAdjustedDay;
	}

	public int getWsAdjustedDay() {
		return this.wsAdjustedDay;
	}

	public void setWsAdjustedMonth(int wsAdjustedMonth) {
		this.wsAdjustedMonth = wsAdjustedMonth;
	}

	public int getWsAdjustedMonth() {
		return this.wsAdjustedMonth;
	}

	public void setWsAdjustedYear(int wsAdjustedYear) {
		this.wsAdjustedYear = wsAdjustedYear;
	}

	public int getWsAdjustedYear() {
		return this.wsAdjustedYear;
	}

	public void setWsCenturyDay(long wsCenturyDay) {
		this.wsCenturyDay = wsCenturyDay;
	}

	public long getWsCenturyDay() {
		return this.wsCenturyDay;
	}

	public void setWsCenturyDay1(long wsCenturyDay1) {
		this.wsCenturyDay1 = wsCenturyDay1;
	}

	public long getWsCenturyDay1() {
		return this.wsCenturyDay1;
	}

	public void setWsCenturyDay2(long wsCenturyDay2) {
		this.wsCenturyDay2 = wsCenturyDay2;
	}

	public long getWsCenturyDay2() {
		return this.wsCenturyDay2;
	}

	public void setWsCenturyYear(long wsCenturyYear) {
		this.wsCenturyYear = wsCenturyYear;
	}

	public long getWsCenturyYear() {
		return this.wsCenturyYear;
	}

	public void setWsDifference(long wsDifference) {
		this.wsDifference = wsDifference;
	}

	public long getWsDifference() {
		return this.wsDifference;
	}

	public void setWsAbstimeMsecs(int wsAbstimeMsecs) {
		this.wsAbstimeMsecs = wsAbstimeMsecs;
	}

	public int getWsAbstimeMsecs() {
		return this.wsAbstimeMsecs;
	}

	public void setWsSystemYearB(int wsSystemYearB) {
		this.wsSystemYearB = wsSystemYearB;
	}

	public int getWsSystemYearB() {
		return this.wsSystemYearB;
	}

	public void setWsSystemMonthB(int wsSystemMonthB) {
		this.wsSystemMonthB = wsSystemMonthB;
	}

	public int getWsSystemMonthB() {
		return this.wsSystemMonthB;
	}

	public void setWsSystemDayB(int wsSystemDayB) {
		this.wsSystemDayB = wsSystemDayB;
	}

	public int getWsSystemDayB() {
		return this.wsSystemDayB;
	}

	public void setWsSystemMonth(short wsSystemMonth) {
		this.wsSystemMonth = NumericDisplay.asString(wsSystemMonth, Len.WS_SYSTEM_MONTH);
	}

	public short getWsSystemMonth() {
		return NumericDisplay.asShort(this.wsSystemMonth);
	}

	public String getWsSystemMonthFormatted() {
		return this.wsSystemMonth;
	}

	public void setWsSystemDay(short wsSystemDay) {
		this.wsSystemDay = NumericDisplay.asString(wsSystemDay, Len.WS_SYSTEM_DAY);
	}

	public short getWsSystemDay() {
		return NumericDisplay.asShort(this.wsSystemDay);
	}

	public String getWsSystemDayFormatted() {
		return this.wsSystemDay;
	}

	public void setWsSystemMsecs(int wsSystemMsecs) {
		this.wsSystemMsecs = NumericDisplay.asString(wsSystemMsecs, Len.WS_SYSTEM_MSECS);
	}

	public int getWsSystemMsecs() {
		return NumericDisplay.asInt(this.wsSystemMsecs);
	}

	public String getWsSystemMsecsFormatted() {
		return this.wsSystemMsecs;
	}

	public char getWsComma() {
		return this.wsComma;
	}

	public char getWsDash() {
		return this.wsDash;
	}

	public char getWsPeriod() {
		return this.wsPeriod;
	}

	public char getWsNegative() {
		return this.wsNegative;
	}

	public char getWsPositive() {
		return this.wsPositive;
	}

	public char getWsSlash() {
		return this.wsSlash;
	}

	public void setWsDelimiter(char wsDelimiter) {
		this.wsDelimiter = wsDelimiter;
	}

	public char getWsDelimiter() {
		return this.wsDelimiter;
	}

	public void setWsMonthSw(char wsMonthSw) {
		this.wsMonthSw = wsMonthSw;
	}

	public void setWsMonthSwFormatted(String wsMonthSw) {
		setWsMonthSw(Functions.charAt(wsMonthSw, Types.CHAR_SIZE));
	}

	public char getWsMonthSw() {
		return this.wsMonthSw;
	}

	public boolean isWsMonthFound() {
		return wsMonthSw == WS_MONTH_FOUND;
	}

	public void setWsDaysSw(char wsDaysSw) {
		this.wsDaysSw = wsDaysSw;
	}

	public void setWsDaysSwFormatted(String wsDaysSw) {
		setWsDaysSw(Functions.charAt(wsDaysSw, Types.CHAR_SIZE));
	}

	public char getWsDaysSw() {
		return this.wsDaysSw;
	}

	public boolean isWsDaysFound() {
		return wsDaysSw == WS_DAYS_FOUND;
	}

	public void setWsSwapDateSw(char wsSwapDateSw) {
		this.wsSwapDateSw = wsSwapDateSw;
	}

	public void setWsSwapDateSwFormatted(String wsSwapDateSw) {
		setWsSwapDateSw(Functions.charAt(wsSwapDateSw, Types.CHAR_SIZE));
	}

	public char getWsSwapDateSw() {
		return this.wsSwapDateSw;
	}

	public boolean isWsDateSwapped() {
		return wsSwapDateSw == WS_DATE_SWAPPED;
	}

	public void setWsKeepAdjustSign(char wsKeepAdjustSign) {
		this.wsKeepAdjustSign = wsKeepAdjustSign;
	}

	public char getWsKeepAdjustSign() {
		return this.wsKeepAdjustSign;
	}

	public void setWsKeepAdjustNumbFormatted(String wsKeepAdjustNumb) {
		this.wsKeepAdjustNumb = Trunc.toUnsignedNumeric(wsKeepAdjustNumb, Len.WS_KEEP_ADJUST_NUMB);
	}

	public int getWsKeepAdjustNumb() {
		return NumericDisplay.asInt(this.wsKeepAdjustNumb);
	}

	public String getWsKeepAdjustNumbFormatted() {
		return this.wsKeepAdjustNumb;
	}

	public void setWsSaveCheckYear(short wsSaveCheckYear) {
		this.wsSaveCheckYear = wsSaveCheckYear;
	}

	public short getWsSaveCheckYear() {
		return this.wsSaveCheckYear;
	}

	public void setWsSaveCheckMonth(short wsSaveCheckMonth) {
		this.wsSaveCheckMonth = wsSaveCheckMonth;
	}

	public short getWsSaveCheckMonth() {
		return this.wsSaveCheckMonth;
	}

	public void setWsSaveCheckDay(short wsSaveCheckDay) {
		this.wsSaveCheckDay = wsSaveCheckDay;
	}

	public short getWsSaveCheckDay() {
		return this.wsSaveCheckDay;
	}

	public void setWsSaveCheckHours(short wsSaveCheckHours) {
		this.wsSaveCheckHours = wsSaveCheckHours;
	}

	public short getWsSaveCheckHours() {
		return this.wsSaveCheckHours;
	}

	public WsCheckMonth getWsCheckMonth() {
		return wsCheckMonth;
	}

	public WsDate1Fields getWsDate1Fields() {
		return wsDate1Fields;
	}

	public WsDate1Fields getWsDate2Fields() {
		return wsDate2Fields;
	}

	public WsDateAdjFormatRed getWsDateAdjFormatRed() {
		return wsDateAdjFormatRed;
	}

	public WsDateArray getWsDateArray() {
		return wsDateArray;
	}

	public WsDateFormat getWsDateFormat() {
		return wsDateFormat;
	}

	public WsLeapYearSw getWsLeapYearSw() {
		return wsLeapYearSw;
	}

	public WsMonthLit getWsMonthLit() {
		return wsMonthLit;
	}

	public WsMonthTable getWsMonthTable() {
		return wsMonthTable;
	}

	public WsSystemTimeRed getWsSystemTimeRed() {
		return wsSystemTimeRed;
	}

	public WsSystemYearRed getWsSystemYearRed() {
		return wsSystemYearRed;
	}

	public WsTimeArray getWsTimeArray() {
		return wsTimeArray;
	}

	public WsTimeSw getWsTimeSw() {
		return wsTimeSw;
	}

	public WsVarDateArray getWsVarDateArray() {
		return wsVarDateArray;
	}

	public WsWorkDay getWsWorkDay() {
		return wsWorkDay;
	}

	public WsWorkTime getWsWorkTime() {
		return wsWorkTime;
	}

	public WsWorkYearRedcy getWsWorkYearRedcy() {
		return wsWorkYearRedcy;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQLCODE_DISP = 4;
		public static final int WS_OUT_FORMAT = 2;
		public static final int WS_WORK_MONTH = 2;
		public static final int WS_PROCESS_DATE = 10;
		public static final int WS_CHECK_AMPM = 2;
		public static final int WS_SYSTEM_MONTH = 2;
		public static final int WS_SYSTEM_DAY = 2;
		public static final int FLR1 = 2;
		public static final int WS_SYSTEM_MSECS = 6;
		public static final int WS_KEEP_ADJUST_NUMB = 5;
		public static final int WS_ADJUST_NUMBER = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_ADJUST_NUMBER = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
