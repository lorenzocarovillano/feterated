/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.UbocApplyDataPrivacySw;
import com.federatedinsurance.crs.ws.enums.UbocSecAutNbr;

/**Original name: UBOC-SEC-AND-DATA-PRIV-INFO<br>
 * Variable: UBOC-SEC-AND-DATA-PRIV-INFO from copybook HALLUBOC<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class UbocSecAndDataPrivInfo {

	//==== PROPERTIES ====
	//Original name: UBOC-APPLY-DATA-PRIVACY-SW
	private UbocApplyDataPrivacySw applyDataPrivacySw = new UbocApplyDataPrivacySw();
	//Original name: UBOC-SECURITY-CONTEXT
	private String securityContext = DefaultValues.stringVal(Len.SECURITY_CONTEXT);
	public static final String NO_CONTEXT = "NO_CONTEXT";
	//Original name: UBOC-SEC-AUT-NBR
	private UbocSecAutNbr secAutNbr = new UbocSecAutNbr();
	//Original name: UBOC-SEC-ASSOCIATION-TYPE
	private String secAssociationType = DefaultValues.stringVal(Len.SEC_ASSOCIATION_TYPE);

	//==== METHODS ====
	public void setUbocSecAndDataPrivInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		applyDataPrivacySw.setApplyDataPrivacySw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		securityContext = MarshalByte.readString(buffer, position, Len.SECURITY_CONTEXT);
		position += Len.SECURITY_CONTEXT;
		secAutNbr.value = MarshalByte.readFixedString(buffer, position, UbocSecAutNbr.Len.SEC_AUT_NBR);
		position += UbocSecAutNbr.Len.SEC_AUT_NBR;
		secAssociationType = MarshalByte.readString(buffer, position, Len.SEC_ASSOCIATION_TYPE);
	}

	public byte[] getUbocSecAndDataPrivInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, applyDataPrivacySw.getApplyDataPrivacySw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, securityContext, Len.SECURITY_CONTEXT);
		position += Len.SECURITY_CONTEXT;
		MarshalByte.writeString(buffer, position, secAutNbr.value, UbocSecAutNbr.Len.SEC_AUT_NBR);
		position += UbocSecAutNbr.Len.SEC_AUT_NBR;
		MarshalByte.writeString(buffer, position, secAssociationType, Len.SEC_ASSOCIATION_TYPE);
		return buffer;
	}

	public void setSecurityContext(String securityContext) {
		this.securityContext = Functions.subString(securityContext, Len.SECURITY_CONTEXT);
	}

	public String getSecurityContext() {
		return this.securityContext;
	}

	public boolean isNoContext() {
		return securityContext.equals(NO_CONTEXT);
	}

	public void setNoContext() {
		securityContext = NO_CONTEXT;
	}

	public void setSecAssociationType(String secAssociationType) {
		this.secAssociationType = Functions.subString(secAssociationType, Len.SEC_ASSOCIATION_TYPE);
	}

	public String getSecAssociationType() {
		return this.secAssociationType;
	}

	public UbocApplyDataPrivacySw getApplyDataPrivacySw() {
		return applyDataPrivacySw;
	}

	public UbocSecAutNbr getSecAutNbr() {
		return secAutNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SECURITY_CONTEXT = 20;
		public static final int SEC_ASSOCIATION_TYPE = 8;
		public static final int UBOC_SEC_AND_DATA_PRIV_INFO = UbocApplyDataPrivacySw.Len.APPLY_DATA_PRIVACY_SW + SECURITY_CONTEXT
				+ UbocSecAutNbr.Len.SEC_AUT_NBR + SEC_ASSOCIATION_TYPE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
