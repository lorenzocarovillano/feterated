/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-WORK-YEAR-REDCY<br>
 * Variable: WS-WORK-YEAR-REDCY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_DYNAMIC_STRUCT_ON_BUFFER.<br>*/
public class WsWorkYearRedcy {

	//==== PROPERTIES ====
	//Original name: WS-WORK-YEAR-CENTURY
	private String yearCentury = DefaultValues.stringVal(Len.YEAR_CENTURY);
	//Original name: WS-WORK-YEAR-YEAR
	private String yearYear = DefaultValues.stringVal(Len.YEAR_YEAR);

	//==== METHODS ====
	public void setWsWorkYear(short wsWorkYear) {
		int position = 1;
		byte[] buffer = getWsWorkYearRedcyBytes();
		MarshalByte.writeShort(buffer, position, wsWorkYear, Len.Int.WS_WORK_YEAR, SignType.NO_SIGN);
		setWsWorkYearRedcyBytes(buffer);
	}

	public void setWsWorkYearFormatted(String wsWorkYear) {
		int position = 1;
		byte[] buffer = getWsWorkYearRedcyBytes();
		MarshalByte.writeString(buffer, position, Trunc.toNumeric(wsWorkYear, Len.WS_WORK_YEAR), Len.WS_WORK_YEAR);
		setWsWorkYearRedcyBytes(buffer);
	}

	/**Original name: WS-WORK-YEAR<br>*/
	public short getWsWorkYear() {
		int position = 1;
		return MarshalByte.readShort(getWsWorkYearRedcyBytes(), position, Len.Int.WS_WORK_YEAR, SignType.NO_SIGN);
	}

	public String getWsWorkYearFormatted() {
		int position = 1;
		return MarshalByte.readFixedString(getWsWorkYearRedcyBytes(), position, Len.WS_WORK_YEAR);
	}

	public void setWsWorkYearRedcyBytes(byte[] buffer) {
		setWsWorkYearRedcyBytes(buffer, 1);
	}

	/**Original name: WS-WORK-YEAR-REDCY<br>*/
	public byte[] getWsWorkYearRedcyBytes() {
		byte[] buffer = new byte[Len.WS_WORK_YEAR_REDCY];
		return getWsWorkYearRedcyBytes(buffer, 1);
	}

	public void setWsWorkYearRedcyBytes(byte[] buffer, int offset) {
		int position = offset;
		yearCentury = MarshalByte.readFixedString(buffer, position, Len.YEAR_CENTURY);
		position += Len.YEAR_CENTURY;
		yearYear = MarshalByte.readFixedString(buffer, position, Len.YEAR_YEAR);
	}

	public byte[] getWsWorkYearRedcyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, yearCentury, Len.YEAR_CENTURY);
		position += Len.YEAR_CENTURY;
		MarshalByte.writeString(buffer, position, yearYear, Len.YEAR_YEAR);
		return buffer;
	}

	public void setYearCenturyFormatted(String yearCentury) {
		this.yearCentury = Trunc.toUnsignedNumeric(yearCentury, Len.YEAR_CENTURY);
	}

	public short getYearCentury() {
		return NumericDisplay.asShort(this.yearCentury);
	}

	public void setYearYearFormatted(String yearYear) {
		this.yearYear = Trunc.toUnsignedNumeric(yearYear, Len.YEAR_YEAR);
	}

	public short getYearYear() {
		return NumericDisplay.asShort(this.yearYear);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int YEAR_CENTURY = 2;
		public static final int YEAR_YEAR = 2;
		public static final int WS_WORK_YEAR_REDCY = YEAR_CENTURY + YEAR_YEAR;
		public static final int WS_WORK_YEAR = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_WORK_YEAR = 4;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
