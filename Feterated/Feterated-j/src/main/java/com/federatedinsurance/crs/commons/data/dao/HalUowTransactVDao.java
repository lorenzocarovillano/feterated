/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalUowTransactV;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [HAL_UOW_TRANSACT_V]
 * 
 */
public class HalUowTransactVDao extends BaseSqlDao<IHalUowTransactV> {

	private final IRowMapper<IHalUowTransactV> selectByHutcUowNm1Rm = buildNamedRowMapper(IHalUowTransactV.class, "hutcUowNm", "mcmMduNm",
			"lokTmoItv", "lokSgyCd", "secInd", "secMduNmObj", "dtaPvcInd", "auditInd");

	public HalUowTransactVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalUowTransactV> getToClass() {
		return IHalUowTransactV.class;
	}

	public String selectByHutcUowNm(String hutcUowNm, String dft) {
		return buildQuery("selectByHutcUowNm").bind("hutcUowNm", hutcUowNm).scalarResultString(dft);
	}

	public IHalUowTransactV selectByHutcUowNm1(String hutcUowNm, IHalUowTransactV iHalUowTransactV) {
		return buildQuery("selectByHutcUowNm1").bind("hutcUowNm", hutcUowNm).rowMapper(selectByHutcUowNm1Rm).singleResult(iHalUowTransactV);
	}
}
