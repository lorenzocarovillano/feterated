/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-WORK-DAY<br>
 * Variable: WS-WORK-DAY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsWorkDay extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public WsWorkDay() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_WORK_DAY;
	}

	public void setWsWorkDay(short wsWorkDay) {
		writeShort(Pos.WS_WORK_DAY, wsWorkDay, Len.Int.WS_WORK_DAY, SignType.NO_SIGN);
	}

	/**Original name: WS-WORK-DAY<br>*/
	public short getWsWorkDay() {
		return readNumDispUnsignedShort(Pos.WS_WORK_DAY, Len.WS_WORK_DAY);
	}

	public void setDay2(short day2) {
		writeShort(Pos.DAY2, day2, Len.Int.DAY2, SignType.NO_SIGN);
	}

	public void setDay2Formatted(String day2) {
		writeString(Pos.DAY2, Trunc.toUnsignedNumeric(day2, Len.DAY2), Len.DAY2);
	}

	/**Original name: WS-WORK-DAY2<br>*/
	public short getDay2() {
		return readNumDispUnsignedShort(Pos.DAY2, Len.DAY2);
	}

	public String getDay2Formatted() {
		return readFixedString(Pos.DAY2, Len.DAY2);
	}

	public void setDay3(short day3) {
		writeShort(Pos.DAY3, day3, Len.Int.DAY3, SignType.NO_SIGN);
	}

	public void setDay3Formatted(String day3) {
		writeString(Pos.DAY3, Trunc.toUnsignedNumeric(day3, Len.DAY3), Len.DAY3);
	}

	/**Original name: WS-WORK-DAY3<br>*/
	public short getDay3() {
		return readNumDispUnsignedShort(Pos.DAY3, Len.DAY3);
	}

	public String getDay3Formatted() {
		return readFixedString(Pos.DAY3, Len.DAY3);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_WORK_DAY = 1;
		public static final int WS_WORK_DAY_RED2 = 1;
		public static final int DAY2 = WS_WORK_DAY_RED2;
		public static final int FLR1 = DAY2 + Len.DAY2;
		public static final int WS_WORK_DAY_RED3 = 1;
		public static final int DAY3 = WS_WORK_DAY_RED3;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int DAY2 = 2;
		public static final int WS_WORK_DAY = 3;
		public static final int DAY3 = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int WS_WORK_DAY = 3;
			public static final int DAY2 = 2;
			public static final int DAY3 = 3;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
