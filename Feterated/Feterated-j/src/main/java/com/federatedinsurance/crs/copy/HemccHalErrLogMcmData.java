/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: HEMCC-HAL-ERR-LOG-MCM-DATA<br>
 * Variable: HEMCC-HAL-ERR-LOG-MCM-DATA from copybook HALLCEMC<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HemccHalErrLogMcmData {

	//==== PROPERTIES ====
	/**Original name: HEMCC-HEMC-MCM-NM-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char hemcMcmNmCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-MCM-NM
	private String hemcMcmNm = DefaultValues.stringVal(Len.HEMC_MCM_NM);
	//Original name: HEMCC-HEMC-PRI-BOBJ-NM-CI
	private char hemcPriBobjNmCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-PRI-BOBJ-NM
	private String hemcPriBobjNm = DefaultValues.stringVal(Len.HEMC_PRI_BOBJ_NM);
	//Original name: HEMCC-HEMC-STG-TYP-CD-CI
	private char hemcStgTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-STG-TYP-CD
	private char hemcStgTypCd = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-PASS-ACY-CD-CI
	private char hemcPassAcyCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-PASS-ACY-CD
	private String hemcPassAcyCd = DefaultValues.stringVal(Len.HEMC_PASS_ACY_CD);
	//Original name: HEMCC-HEMC-NBR-REQ-ROWS-CI
	private char hemcNbrReqRowsCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-REQ-ROWS-SIGN
	private char hemcNbrReqRowsSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-REQ-ROWS
	private String hemcNbrReqRows = DefaultValues.stringVal(Len.HEMC_NBR_REQ_ROWS);
	//Original name: HEMCC-HEMC-NBR-SWI-ROWS-CI
	private char hemcNbrSwiRowsCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-SWI-ROWS-SIGN
	private char hemcNbrSwiRowsSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-SWI-ROWS
	private String hemcNbrSwiRows = DefaultValues.stringVal(Len.HEMC_NBR_SWI_ROWS);
	//Original name: HEMCC-HEMC-NBR-RSP-HDR-CI
	private char hemcNbrRspHdrCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-HDR-SIGN
	private char hemcNbrRspHdrSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-HDR
	private String hemcNbrRspHdr = DefaultValues.stringVal(Len.HEMC_NBR_RSP_HDR);
	//Original name: HEMCC-HEMC-NBR-RSP-DTA-CI
	private char hemcNbrRspDtaCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-DTA-SIGN
	private char hemcNbrRspDtaSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-DTA
	private String hemcNbrRspDta = DefaultValues.stringVal(Len.HEMC_NBR_RSP_DTA);
	//Original name: HEMCC-HEMC-NBR-RSP-WNG-CI
	private char hemcNbrRspWngCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-WNG-SIGN
	private char hemcNbrRspWngSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-WNG
	private String hemcNbrRspWng = DefaultValues.stringVal(Len.HEMC_NBR_RSP_WNG);
	//Original name: HEMCC-HEMC-NBR-RSP-NLBE-CI
	private char hemcNbrRspNlbeCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-NLBE-SIGN
	private char hemcNbrRspNlbeSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-RSP-NLBE
	private String hemcNbrRspNlbe = DefaultValues.stringVal(Len.HEMC_NBR_RSP_NLBE);
	//Original name: HEMCC-HEMC-NBR-PRC-HDR-CI
	private char hemcNbrPrcHdrCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-HDR-SIGN
	private char hemcNbrPrcHdrSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-HDR
	private String hemcNbrPrcHdr = DefaultValues.stringVal(Len.HEMC_NBR_PRC_HDR);
	//Original name: HEMCC-HEMC-NBR-PRC-DTA-CI
	private char hemcNbrPrcDtaCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-DTA-SIGN
	private char hemcNbrPrcDtaSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-DTA
	private String hemcNbrPrcDta = DefaultValues.stringVal(Len.HEMC_NBR_PRC_DTA);
	//Original name: HEMCC-HEMC-NBR-PRC-WNG-CI
	private char hemcNbrPrcWngCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-WNG-SIGN
	private char hemcNbrPrcWngSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-WNG
	private String hemcNbrPrcWng = DefaultValues.stringVal(Len.HEMC_NBR_PRC_WNG);
	//Original name: HEMCC-HEMC-NBR-PRC-NLBE-CI
	private char hemcNbrPrcNlbeCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-NLBE-SIGN
	private char hemcNbrPrcNlbeSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-NBR-PRC-NLBE
	private String hemcNbrPrcNlbe = DefaultValues.stringVal(Len.HEMC_NBR_PRC_NLBE);
	//Original name: HEMCC-HEMC-LOCK-TYPE-CD-CI
	private char hemcLockTypeCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-LOCK-TYPE-CD
	private String hemcLockTypeCd = DefaultValues.stringVal(Len.HEMC_LOCK_TYPE_CD);
	//Original name: HEMCC-LOCK-ATN-CD-CI
	private char lockAtnCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-LOCK-ATN-CD
	private char lockAtnCd = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-LOCK-DEL-TM-CI
	private char lockDelTmCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-LOCK-DEL-TM-SIGN
	private char lockDelTmSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-LOCK-DEL-TM
	private String lockDelTm = DefaultValues.stringVal(Len.LOCK_DEL_TM);
	//Original name: HEMCC-HEMC-LOCK-CTL-IND-CI
	private char hemcLockCtlIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-LOCK-CTL-IND
	private char hemcLockCtlInd = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-DTA-PVC-IND-CI
	private char hemcDtaPvcIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-DTA-PVC-IND
	private char hemcDtaPvcInd = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-SEC-CTXT-IND-CI
	private char hemcSecCtxtIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-SEC-CTXT-IND-NI
	private char hemcSecCtxtIndNi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-SEC-CTXT-IND
	private String hemcSecCtxtInd = DefaultValues.stringVal(Len.HEMC_SEC_CTXT_IND);
	//Original name: HEMCC-HEMC-AUT-NBR-CI
	private char hemcAutNbrCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUT-NBR-SIGN
	private char hemcAutNbrSign = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUT-NBR
	private String hemcAutNbr = DefaultValues.stringVal(Len.HEMC_AUT_NBR);
	//Original name: HEMCC-HEMC-ASC-TYP-CD-CI
	private char hemcAscTypCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-ASC-TYP-CD-NI
	private char hemcAscTypCdNi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-ASC-TYP-CD
	private String hemcAscTypCd = DefaultValues.stringVal(Len.HEMC_ASC_TYP_CD);
	//Original name: HEMCC-HEMC-DP-RET-CD-CI
	private char hemcDpRetCdCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-DP-RET-CD-NI
	private char hemcDpRetCdNi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-DP-RET-CD
	private String hemcDpRetCd = DefaultValues.stringVal(Len.HEMC_DP_RET_CD);
	//Original name: HEMCC-HEMC-AUDIT-IND-CI
	private char hemcAuditIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUDIT-IND
	private char hemcAuditInd = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUDIT-BOBJ-NM-CI
	private char hemcAuditBobjNmCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUDIT-BOBJ-NM-NI
	private char hemcAuditBobjNmNi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUDIT-BOBJ-NM
	private String hemcAuditBobjNm = DefaultValues.stringVal(Len.HEMC_AUDIT_BOBJ_NM);
	//Original name: HEMCC-HEMC-AUD-DTA-TXT-CI
	private char hemcAudDtaTxtCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUD-DTA-TXT-NI
	private char hemcAudDtaTxtNi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-AUD-DTA-TXT
	private String hemcAudDtaTxt = DefaultValues.stringVal(Len.HEMC_AUD_DTA_TXT);
	//Original name: HEMCC-HEMC-KEEP-CLR-IND-CI
	private char hemcKeepClrIndCi = DefaultValues.CHAR_VAL;
	//Original name: HEMCC-HEMC-KEEP-CLR-IND
	private char hemcKeepClrInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public byte[] getHalErrLogMcmDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, hemcMcmNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcMcmNm, Len.HEMC_MCM_NM);
		position += Len.HEMC_MCM_NM;
		MarshalByte.writeChar(buffer, position, hemcPriBobjNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcPriBobjNm, Len.HEMC_PRI_BOBJ_NM);
		position += Len.HEMC_PRI_BOBJ_NM;
		MarshalByte.writeChar(buffer, position, hemcStgTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcStgTypCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcPassAcyCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcPassAcyCd, Len.HEMC_PASS_ACY_CD);
		position += Len.HEMC_PASS_ACY_CD;
		MarshalByte.writeChar(buffer, position, hemcNbrReqRowsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrReqRowsSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrReqRows, Len.HEMC_NBR_REQ_ROWS);
		position += Len.HEMC_NBR_REQ_ROWS;
		MarshalByte.writeChar(buffer, position, hemcNbrSwiRowsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrSwiRowsSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrSwiRows, Len.HEMC_NBR_SWI_ROWS);
		position += Len.HEMC_NBR_SWI_ROWS;
		MarshalByte.writeChar(buffer, position, hemcNbrRspHdrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrRspHdrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrRspHdr, Len.HEMC_NBR_RSP_HDR);
		position += Len.HEMC_NBR_RSP_HDR;
		MarshalByte.writeChar(buffer, position, hemcNbrRspDtaCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrRspDtaSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrRspDta, Len.HEMC_NBR_RSP_DTA);
		position += Len.HEMC_NBR_RSP_DTA;
		MarshalByte.writeChar(buffer, position, hemcNbrRspWngCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrRspWngSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrRspWng, Len.HEMC_NBR_RSP_WNG);
		position += Len.HEMC_NBR_RSP_WNG;
		MarshalByte.writeChar(buffer, position, hemcNbrRspNlbeCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrRspNlbeSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrRspNlbe, Len.HEMC_NBR_RSP_NLBE);
		position += Len.HEMC_NBR_RSP_NLBE;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcHdrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcHdrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrPrcHdr, Len.HEMC_NBR_PRC_HDR);
		position += Len.HEMC_NBR_PRC_HDR;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcDtaCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcDtaSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrPrcDta, Len.HEMC_NBR_PRC_DTA);
		position += Len.HEMC_NBR_PRC_DTA;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcWngCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcWngSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrPrcWng, Len.HEMC_NBR_PRC_WNG);
		position += Len.HEMC_NBR_PRC_WNG;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcNlbeCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcNbrPrcNlbeSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcNbrPrcNlbe, Len.HEMC_NBR_PRC_NLBE);
		position += Len.HEMC_NBR_PRC_NLBE;
		MarshalByte.writeChar(buffer, position, hemcLockTypeCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcLockTypeCd, Len.HEMC_LOCK_TYPE_CD);
		position += Len.HEMC_LOCK_TYPE_CD;
		MarshalByte.writeChar(buffer, position, lockAtnCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lockAtnCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lockDelTmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, lockDelTmSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, lockDelTm, Len.LOCK_DEL_TM);
		position += Len.LOCK_DEL_TM;
		MarshalByte.writeChar(buffer, position, hemcLockCtlIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcLockCtlInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcDtaPvcIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcDtaPvcInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcSecCtxtIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcSecCtxtIndNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcSecCtxtInd, Len.HEMC_SEC_CTXT_IND);
		position += Len.HEMC_SEC_CTXT_IND;
		MarshalByte.writeChar(buffer, position, hemcAutNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAutNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcAutNbr, Len.HEMC_AUT_NBR);
		position += Len.HEMC_AUT_NBR;
		MarshalByte.writeChar(buffer, position, hemcAscTypCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAscTypCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcAscTypCd, Len.HEMC_ASC_TYP_CD);
		position += Len.HEMC_ASC_TYP_CD;
		MarshalByte.writeChar(buffer, position, hemcDpRetCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcDpRetCdNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcDpRetCd, Len.HEMC_DP_RET_CD);
		position += Len.HEMC_DP_RET_CD;
		MarshalByte.writeChar(buffer, position, hemcAuditIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAuditInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAuditBobjNmCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAuditBobjNmNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcAuditBobjNm, Len.HEMC_AUDIT_BOBJ_NM);
		position += Len.HEMC_AUDIT_BOBJ_NM;
		MarshalByte.writeChar(buffer, position, hemcAudDtaTxtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcAudDtaTxtNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, hemcAudDtaTxt, Len.HEMC_AUD_DTA_TXT);
		position += Len.HEMC_AUD_DTA_TXT;
		MarshalByte.writeChar(buffer, position, hemcKeepClrIndCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, hemcKeepClrInd);
		return buffer;
	}

	public void initHalErrLogMcmDataSpaces() {
		hemcMcmNmCi = Types.SPACE_CHAR;
		hemcMcmNm = "";
		hemcPriBobjNmCi = Types.SPACE_CHAR;
		hemcPriBobjNm = "";
		hemcStgTypCdCi = Types.SPACE_CHAR;
		hemcStgTypCd = Types.SPACE_CHAR;
		hemcPassAcyCdCi = Types.SPACE_CHAR;
		hemcPassAcyCd = "";
		hemcNbrReqRowsCi = Types.SPACE_CHAR;
		hemcNbrReqRowsSign = Types.SPACE_CHAR;
		hemcNbrReqRows = "";
		hemcNbrSwiRowsCi = Types.SPACE_CHAR;
		hemcNbrSwiRowsSign = Types.SPACE_CHAR;
		hemcNbrSwiRows = "";
		hemcNbrRspHdrCi = Types.SPACE_CHAR;
		hemcNbrRspHdrSign = Types.SPACE_CHAR;
		hemcNbrRspHdr = "";
		hemcNbrRspDtaCi = Types.SPACE_CHAR;
		hemcNbrRspDtaSign = Types.SPACE_CHAR;
		hemcNbrRspDta = "";
		hemcNbrRspWngCi = Types.SPACE_CHAR;
		hemcNbrRspWngSign = Types.SPACE_CHAR;
		hemcNbrRspWng = "";
		hemcNbrRspNlbeCi = Types.SPACE_CHAR;
		hemcNbrRspNlbeSign = Types.SPACE_CHAR;
		hemcNbrRspNlbe = "";
		hemcNbrPrcHdrCi = Types.SPACE_CHAR;
		hemcNbrPrcHdrSign = Types.SPACE_CHAR;
		hemcNbrPrcHdr = "";
		hemcNbrPrcDtaCi = Types.SPACE_CHAR;
		hemcNbrPrcDtaSign = Types.SPACE_CHAR;
		hemcNbrPrcDta = "";
		hemcNbrPrcWngCi = Types.SPACE_CHAR;
		hemcNbrPrcWngSign = Types.SPACE_CHAR;
		hemcNbrPrcWng = "";
		hemcNbrPrcNlbeCi = Types.SPACE_CHAR;
		hemcNbrPrcNlbeSign = Types.SPACE_CHAR;
		hemcNbrPrcNlbe = "";
		hemcLockTypeCdCi = Types.SPACE_CHAR;
		hemcLockTypeCd = "";
		lockAtnCdCi = Types.SPACE_CHAR;
		lockAtnCd = Types.SPACE_CHAR;
		lockDelTmCi = Types.SPACE_CHAR;
		lockDelTmSign = Types.SPACE_CHAR;
		lockDelTm = "";
		hemcLockCtlIndCi = Types.SPACE_CHAR;
		hemcLockCtlInd = Types.SPACE_CHAR;
		hemcDtaPvcIndCi = Types.SPACE_CHAR;
		hemcDtaPvcInd = Types.SPACE_CHAR;
		hemcSecCtxtIndCi = Types.SPACE_CHAR;
		hemcSecCtxtIndNi = Types.SPACE_CHAR;
		hemcSecCtxtInd = "";
		hemcAutNbrCi = Types.SPACE_CHAR;
		hemcAutNbrSign = Types.SPACE_CHAR;
		hemcAutNbr = "";
		hemcAscTypCdCi = Types.SPACE_CHAR;
		hemcAscTypCdNi = Types.SPACE_CHAR;
		hemcAscTypCd = "";
		hemcDpRetCdCi = Types.SPACE_CHAR;
		hemcDpRetCdNi = Types.SPACE_CHAR;
		hemcDpRetCd = "";
		hemcAuditIndCi = Types.SPACE_CHAR;
		hemcAuditInd = Types.SPACE_CHAR;
		hemcAuditBobjNmCi = Types.SPACE_CHAR;
		hemcAuditBobjNmNi = Types.SPACE_CHAR;
		hemcAuditBobjNm = "";
		hemcAudDtaTxtCi = Types.SPACE_CHAR;
		hemcAudDtaTxtNi = Types.SPACE_CHAR;
		hemcAudDtaTxt = "";
		hemcKeepClrIndCi = Types.SPACE_CHAR;
		hemcKeepClrInd = Types.SPACE_CHAR;
	}

	public char getHemcMcmNmCi() {
		return this.hemcMcmNmCi;
	}

	public void setHemcMcmNm(String hemcMcmNm) {
		this.hemcMcmNm = Functions.subString(hemcMcmNm, Len.HEMC_MCM_NM);
	}

	public String getHemcMcmNm() {
		return this.hemcMcmNm;
	}

	public char getHemcPriBobjNmCi() {
		return this.hemcPriBobjNmCi;
	}

	public void setHemcPriBobjNm(String hemcPriBobjNm) {
		this.hemcPriBobjNm = Functions.subString(hemcPriBobjNm, Len.HEMC_PRI_BOBJ_NM);
	}

	public String getHemcPriBobjNm() {
		return this.hemcPriBobjNm;
	}

	public char getHemcStgTypCdCi() {
		return this.hemcStgTypCdCi;
	}

	public void setHemcStgTypCd(char hemcStgTypCd) {
		this.hemcStgTypCd = hemcStgTypCd;
	}

	public char getHemcStgTypCd() {
		return this.hemcStgTypCd;
	}

	public char getHemcPassAcyCdCi() {
		return this.hemcPassAcyCdCi;
	}

	public void setHemcPassAcyCd(String hemcPassAcyCd) {
		this.hemcPassAcyCd = Functions.subString(hemcPassAcyCd, Len.HEMC_PASS_ACY_CD);
	}

	public String getHemcPassAcyCd() {
		return this.hemcPassAcyCd;
	}

	public char getHemcNbrReqRowsCi() {
		return this.hemcNbrReqRowsCi;
	}

	public void setHemcNbrReqRowsSign(char hemcNbrReqRowsSign) {
		this.hemcNbrReqRowsSign = hemcNbrReqRowsSign;
	}

	public char getHemcNbrReqRowsSign() {
		return this.hemcNbrReqRowsSign;
	}

	public void setHemcNbrReqRowsFormatted(String hemcNbrReqRows) {
		this.hemcNbrReqRows = Trunc.toUnsignedNumeric(hemcNbrReqRows, Len.HEMC_NBR_REQ_ROWS);
	}

	public int getHemcNbrReqRows() {
		return NumericDisplay.asInt(this.hemcNbrReqRows);
	}

	public char getHemcNbrSwiRowsCi() {
		return this.hemcNbrSwiRowsCi;
	}

	public void setHemcNbrSwiRowsSign(char hemcNbrSwiRowsSign) {
		this.hemcNbrSwiRowsSign = hemcNbrSwiRowsSign;
	}

	public char getHemcNbrSwiRowsSign() {
		return this.hemcNbrSwiRowsSign;
	}

	public void setHemcNbrSwiRowsFormatted(String hemcNbrSwiRows) {
		this.hemcNbrSwiRows = Trunc.toUnsignedNumeric(hemcNbrSwiRows, Len.HEMC_NBR_SWI_ROWS);
	}

	public int getHemcNbrSwiRows() {
		return NumericDisplay.asInt(this.hemcNbrSwiRows);
	}

	public char getHemcNbrRspHdrCi() {
		return this.hemcNbrRspHdrCi;
	}

	public void setHemcNbrRspHdrSign(char hemcNbrRspHdrSign) {
		this.hemcNbrRspHdrSign = hemcNbrRspHdrSign;
	}

	public char getHemcNbrRspHdrSign() {
		return this.hemcNbrRspHdrSign;
	}

	public void setHemcNbrRspHdrFormatted(String hemcNbrRspHdr) {
		this.hemcNbrRspHdr = Trunc.toUnsignedNumeric(hemcNbrRspHdr, Len.HEMC_NBR_RSP_HDR);
	}

	public int getHemcNbrRspHdr() {
		return NumericDisplay.asInt(this.hemcNbrRspHdr);
	}

	public char getHemcNbrRspDtaCi() {
		return this.hemcNbrRspDtaCi;
	}

	public void setHemcNbrRspDtaSign(char hemcNbrRspDtaSign) {
		this.hemcNbrRspDtaSign = hemcNbrRspDtaSign;
	}

	public char getHemcNbrRspDtaSign() {
		return this.hemcNbrRspDtaSign;
	}

	public void setHemcNbrRspDtaFormatted(String hemcNbrRspDta) {
		this.hemcNbrRspDta = Trunc.toUnsignedNumeric(hemcNbrRspDta, Len.HEMC_NBR_RSP_DTA);
	}

	public int getHemcNbrRspDta() {
		return NumericDisplay.asInt(this.hemcNbrRspDta);
	}

	public char getHemcNbrRspWngCi() {
		return this.hemcNbrRspWngCi;
	}

	public void setHemcNbrRspWngSign(char hemcNbrRspWngSign) {
		this.hemcNbrRspWngSign = hemcNbrRspWngSign;
	}

	public char getHemcNbrRspWngSign() {
		return this.hemcNbrRspWngSign;
	}

	public void setHemcNbrRspWngFormatted(String hemcNbrRspWng) {
		this.hemcNbrRspWng = Trunc.toUnsignedNumeric(hemcNbrRspWng, Len.HEMC_NBR_RSP_WNG);
	}

	public int getHemcNbrRspWng() {
		return NumericDisplay.asInt(this.hemcNbrRspWng);
	}

	public char getHemcNbrRspNlbeCi() {
		return this.hemcNbrRspNlbeCi;
	}

	public void setHemcNbrRspNlbeSign(char hemcNbrRspNlbeSign) {
		this.hemcNbrRspNlbeSign = hemcNbrRspNlbeSign;
	}

	public char getHemcNbrRspNlbeSign() {
		return this.hemcNbrRspNlbeSign;
	}

	public void setHemcNbrRspNlbeFormatted(String hemcNbrRspNlbe) {
		this.hemcNbrRspNlbe = Trunc.toUnsignedNumeric(hemcNbrRspNlbe, Len.HEMC_NBR_RSP_NLBE);
	}

	public int getHemcNbrRspNlbe() {
		return NumericDisplay.asInt(this.hemcNbrRspNlbe);
	}

	public char getHemcNbrPrcHdrCi() {
		return this.hemcNbrPrcHdrCi;
	}

	public void setHemcNbrPrcHdrSign(char hemcNbrPrcHdrSign) {
		this.hemcNbrPrcHdrSign = hemcNbrPrcHdrSign;
	}

	public char getHemcNbrPrcHdrSign() {
		return this.hemcNbrPrcHdrSign;
	}

	public void setHemcNbrPrcHdrFormatted(String hemcNbrPrcHdr) {
		this.hemcNbrPrcHdr = Trunc.toUnsignedNumeric(hemcNbrPrcHdr, Len.HEMC_NBR_PRC_HDR);
	}

	public int getHemcNbrPrcHdr() {
		return NumericDisplay.asInt(this.hemcNbrPrcHdr);
	}

	public char getHemcNbrPrcDtaCi() {
		return this.hemcNbrPrcDtaCi;
	}

	public void setHemcNbrPrcDtaSign(char hemcNbrPrcDtaSign) {
		this.hemcNbrPrcDtaSign = hemcNbrPrcDtaSign;
	}

	public char getHemcNbrPrcDtaSign() {
		return this.hemcNbrPrcDtaSign;
	}

	public void setHemcNbrPrcDtaFormatted(String hemcNbrPrcDta) {
		this.hemcNbrPrcDta = Trunc.toUnsignedNumeric(hemcNbrPrcDta, Len.HEMC_NBR_PRC_DTA);
	}

	public int getHemcNbrPrcDta() {
		return NumericDisplay.asInt(this.hemcNbrPrcDta);
	}

	public char getHemcNbrPrcWngCi() {
		return this.hemcNbrPrcWngCi;
	}

	public void setHemcNbrPrcWngSign(char hemcNbrPrcWngSign) {
		this.hemcNbrPrcWngSign = hemcNbrPrcWngSign;
	}

	public char getHemcNbrPrcWngSign() {
		return this.hemcNbrPrcWngSign;
	}

	public void setHemcNbrPrcWngFormatted(String hemcNbrPrcWng) {
		this.hemcNbrPrcWng = Trunc.toUnsignedNumeric(hemcNbrPrcWng, Len.HEMC_NBR_PRC_WNG);
	}

	public int getHemcNbrPrcWng() {
		return NumericDisplay.asInt(this.hemcNbrPrcWng);
	}

	public char getHemcNbrPrcNlbeCi() {
		return this.hemcNbrPrcNlbeCi;
	}

	public void setHemcNbrPrcNlbeSign(char hemcNbrPrcNlbeSign) {
		this.hemcNbrPrcNlbeSign = hemcNbrPrcNlbeSign;
	}

	public char getHemcNbrPrcNlbeSign() {
		return this.hemcNbrPrcNlbeSign;
	}

	public void setHemcNbrPrcNlbeFormatted(String hemcNbrPrcNlbe) {
		this.hemcNbrPrcNlbe = Trunc.toUnsignedNumeric(hemcNbrPrcNlbe, Len.HEMC_NBR_PRC_NLBE);
	}

	public int getHemcNbrPrcNlbe() {
		return NumericDisplay.asInt(this.hemcNbrPrcNlbe);
	}

	public char getHemcLockTypeCdCi() {
		return this.hemcLockTypeCdCi;
	}

	public void setHemcLockTypeCd(String hemcLockTypeCd) {
		this.hemcLockTypeCd = Functions.subString(hemcLockTypeCd, Len.HEMC_LOCK_TYPE_CD);
	}

	public String getHemcLockTypeCd() {
		return this.hemcLockTypeCd;
	}

	public char getLockAtnCdCi() {
		return this.lockAtnCdCi;
	}

	public void setLockAtnCd(char lockAtnCd) {
		this.lockAtnCd = lockAtnCd;
	}

	public char getLockAtnCd() {
		return this.lockAtnCd;
	}

	public char getLockDelTmCi() {
		return this.lockDelTmCi;
	}

	public void setLockDelTmSign(char lockDelTmSign) {
		this.lockDelTmSign = lockDelTmSign;
	}

	public char getLockDelTmSign() {
		return this.lockDelTmSign;
	}

	public void setLockDelTmFormatted(String lockDelTm) {
		this.lockDelTm = Trunc.toUnsignedNumeric(lockDelTm, Len.LOCK_DEL_TM);
	}

	public long getLockDelTm() {
		return NumericDisplay.asLong(this.lockDelTm);
	}

	public char getHemcLockCtlIndCi() {
		return this.hemcLockCtlIndCi;
	}

	public void setHemcLockCtlInd(char hemcLockCtlInd) {
		this.hemcLockCtlInd = hemcLockCtlInd;
	}

	public char getHemcLockCtlInd() {
		return this.hemcLockCtlInd;
	}

	public char getHemcDtaPvcIndCi() {
		return this.hemcDtaPvcIndCi;
	}

	public void setHemcDtaPvcInd(char hemcDtaPvcInd) {
		this.hemcDtaPvcInd = hemcDtaPvcInd;
	}

	public char getHemcDtaPvcInd() {
		return this.hemcDtaPvcInd;
	}

	public char getHemcSecCtxtIndCi() {
		return this.hemcSecCtxtIndCi;
	}

	public void setHemcSecCtxtIndNi(char hemcSecCtxtIndNi) {
		this.hemcSecCtxtIndNi = hemcSecCtxtIndNi;
	}

	public char getHemcSecCtxtIndNi() {
		return this.hemcSecCtxtIndNi;
	}

	public void setHemcSecCtxtInd(String hemcSecCtxtInd) {
		this.hemcSecCtxtInd = Functions.subString(hemcSecCtxtInd, Len.HEMC_SEC_CTXT_IND);
	}

	public String getHemcSecCtxtInd() {
		return this.hemcSecCtxtInd;
	}

	public char getHemcAutNbrCi() {
		return this.hemcAutNbrCi;
	}

	public void setHemcAutNbrSign(char hemcAutNbrSign) {
		this.hemcAutNbrSign = hemcAutNbrSign;
	}

	public char getHemcAutNbrSign() {
		return this.hemcAutNbrSign;
	}

	public void setHemcAutNbrFormatted(String hemcAutNbr) {
		this.hemcAutNbr = Trunc.toUnsignedNumeric(hemcAutNbr, Len.HEMC_AUT_NBR);
	}

	public int getHemcAutNbr() {
		return NumericDisplay.asInt(this.hemcAutNbr);
	}

	public char getHemcAscTypCdCi() {
		return this.hemcAscTypCdCi;
	}

	public void setHemcAscTypCdNi(char hemcAscTypCdNi) {
		this.hemcAscTypCdNi = hemcAscTypCdNi;
	}

	public char getHemcAscTypCdNi() {
		return this.hemcAscTypCdNi;
	}

	public void setHemcAscTypCd(String hemcAscTypCd) {
		this.hemcAscTypCd = Functions.subString(hemcAscTypCd, Len.HEMC_ASC_TYP_CD);
	}

	public String getHemcAscTypCd() {
		return this.hemcAscTypCd;
	}

	public char getHemcDpRetCdCi() {
		return this.hemcDpRetCdCi;
	}

	public void setHemcDpRetCdNi(char hemcDpRetCdNi) {
		this.hemcDpRetCdNi = hemcDpRetCdNi;
	}

	public char getHemcDpRetCdNi() {
		return this.hemcDpRetCdNi;
	}

	public void setHemcDpRetCd(String hemcDpRetCd) {
		this.hemcDpRetCd = Functions.subString(hemcDpRetCd, Len.HEMC_DP_RET_CD);
	}

	public String getHemcDpRetCd() {
		return this.hemcDpRetCd;
	}

	public char getHemcAuditIndCi() {
		return this.hemcAuditIndCi;
	}

	public void setHemcAuditInd(char hemcAuditInd) {
		this.hemcAuditInd = hemcAuditInd;
	}

	public char getHemcAuditInd() {
		return this.hemcAuditInd;
	}

	public char getHemcAuditBobjNmCi() {
		return this.hemcAuditBobjNmCi;
	}

	public void setHemcAuditBobjNmNi(char hemcAuditBobjNmNi) {
		this.hemcAuditBobjNmNi = hemcAuditBobjNmNi;
	}

	public char getHemcAuditBobjNmNi() {
		return this.hemcAuditBobjNmNi;
	}

	public void setHemcAuditBobjNm(String hemcAuditBobjNm) {
		this.hemcAuditBobjNm = Functions.subString(hemcAuditBobjNm, Len.HEMC_AUDIT_BOBJ_NM);
	}

	public String getHemcAuditBobjNm() {
		return this.hemcAuditBobjNm;
	}

	public char getHemcAudDtaTxtCi() {
		return this.hemcAudDtaTxtCi;
	}

	public void setHemcAudDtaTxtNi(char hemcAudDtaTxtNi) {
		this.hemcAudDtaTxtNi = hemcAudDtaTxtNi;
	}

	public char getHemcAudDtaTxtNi() {
		return this.hemcAudDtaTxtNi;
	}

	public String getHemcAudDtaTxt() {
		return this.hemcAudDtaTxt;
	}

	public char getHemcKeepClrIndCi() {
		return this.hemcKeepClrIndCi;
	}

	public void setHemcKeepClrInd(char hemcKeepClrInd) {
		this.hemcKeepClrInd = hemcKeepClrInd;
	}

	public void setHemcKeepClrIndFormatted(String hemcKeepClrInd) {
		setHemcKeepClrInd(Functions.charAt(hemcKeepClrInd, Types.CHAR_SIZE));
	}

	public char getHemcKeepClrInd() {
		return this.hemcKeepClrInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HEMC_MCM_NM = 32;
		public static final int HEMC_PRI_BOBJ_NM = 32;
		public static final int HEMC_PASS_ACY_CD = 20;
		public static final int HEMC_NBR_REQ_ROWS = 5;
		public static final int HEMC_NBR_SWI_ROWS = 5;
		public static final int HEMC_NBR_RSP_HDR = 5;
		public static final int HEMC_NBR_RSP_DTA = 5;
		public static final int HEMC_NBR_RSP_WNG = 5;
		public static final int HEMC_NBR_RSP_NLBE = 5;
		public static final int HEMC_NBR_PRC_HDR = 5;
		public static final int HEMC_NBR_PRC_DTA = 5;
		public static final int HEMC_NBR_PRC_WNG = 5;
		public static final int HEMC_NBR_PRC_NLBE = 5;
		public static final int HEMC_LOCK_TYPE_CD = 10;
		public static final int LOCK_DEL_TM = 10;
		public static final int HEMC_SEC_CTXT_IND = 20;
		public static final int HEMC_AUT_NBR = 5;
		public static final int HEMC_ASC_TYP_CD = 8;
		public static final int HEMC_DP_RET_CD = 10;
		public static final int HEMC_AUDIT_BOBJ_NM = 32;
		public static final int HEMC_AUD_DTA_TXT = 100;
		public static final int HEMC_MCM_NM_CI = 1;
		public static final int HEMC_PRI_BOBJ_NM_CI = 1;
		public static final int HEMC_STG_TYP_CD_CI = 1;
		public static final int HEMC_STG_TYP_CD = 1;
		public static final int HEMC_PASS_ACY_CD_CI = 1;
		public static final int HEMC_NBR_REQ_ROWS_CI = 1;
		public static final int HEMC_NBR_REQ_ROWS_SIGN = 1;
		public static final int HEMC_NBR_SWI_ROWS_CI = 1;
		public static final int HEMC_NBR_SWI_ROWS_SIGN = 1;
		public static final int HEMC_NBR_RSP_HDR_CI = 1;
		public static final int HEMC_NBR_RSP_HDR_SIGN = 1;
		public static final int HEMC_NBR_RSP_DTA_CI = 1;
		public static final int HEMC_NBR_RSP_DTA_SIGN = 1;
		public static final int HEMC_NBR_RSP_WNG_CI = 1;
		public static final int HEMC_NBR_RSP_WNG_SIGN = 1;
		public static final int HEMC_NBR_RSP_NLBE_CI = 1;
		public static final int HEMC_NBR_RSP_NLBE_SIGN = 1;
		public static final int HEMC_NBR_PRC_HDR_CI = 1;
		public static final int HEMC_NBR_PRC_HDR_SIGN = 1;
		public static final int HEMC_NBR_PRC_DTA_CI = 1;
		public static final int HEMC_NBR_PRC_DTA_SIGN = 1;
		public static final int HEMC_NBR_PRC_WNG_CI = 1;
		public static final int HEMC_NBR_PRC_WNG_SIGN = 1;
		public static final int HEMC_NBR_PRC_NLBE_CI = 1;
		public static final int HEMC_NBR_PRC_NLBE_SIGN = 1;
		public static final int HEMC_LOCK_TYPE_CD_CI = 1;
		public static final int LOCK_ATN_CD_CI = 1;
		public static final int LOCK_ATN_CD = 1;
		public static final int LOCK_DEL_TM_CI = 1;
		public static final int LOCK_DEL_TM_SIGN = 1;
		public static final int HEMC_LOCK_CTL_IND_CI = 1;
		public static final int HEMC_LOCK_CTL_IND = 1;
		public static final int HEMC_DTA_PVC_IND_CI = 1;
		public static final int HEMC_DTA_PVC_IND = 1;
		public static final int HEMC_SEC_CTXT_IND_CI = 1;
		public static final int HEMC_SEC_CTXT_IND_NI = 1;
		public static final int HEMC_AUT_NBR_CI = 1;
		public static final int HEMC_AUT_NBR_SIGN = 1;
		public static final int HEMC_ASC_TYP_CD_CI = 1;
		public static final int HEMC_ASC_TYP_CD_NI = 1;
		public static final int HEMC_DP_RET_CD_CI = 1;
		public static final int HEMC_DP_RET_CD_NI = 1;
		public static final int HEMC_AUDIT_IND_CI = 1;
		public static final int HEMC_AUDIT_IND = 1;
		public static final int HEMC_AUDIT_BOBJ_NM_CI = 1;
		public static final int HEMC_AUDIT_BOBJ_NM_NI = 1;
		public static final int HEMC_AUD_DTA_TXT_CI = 1;
		public static final int HEMC_AUD_DTA_TXT_NI = 1;
		public static final int HEMC_KEEP_CLR_IND_CI = 1;
		public static final int HEMC_KEEP_CLR_IND = 1;
		public static final int HAL_ERR_LOG_MCM_DATA = HEMC_MCM_NM_CI + HEMC_MCM_NM + HEMC_PRI_BOBJ_NM_CI + HEMC_PRI_BOBJ_NM + HEMC_STG_TYP_CD_CI
				+ HEMC_STG_TYP_CD + HEMC_PASS_ACY_CD_CI + HEMC_PASS_ACY_CD + HEMC_NBR_REQ_ROWS_CI + HEMC_NBR_REQ_ROWS_SIGN + HEMC_NBR_REQ_ROWS
				+ HEMC_NBR_SWI_ROWS_CI + HEMC_NBR_SWI_ROWS_SIGN + HEMC_NBR_SWI_ROWS + HEMC_NBR_RSP_HDR_CI + HEMC_NBR_RSP_HDR_SIGN + HEMC_NBR_RSP_HDR
				+ HEMC_NBR_RSP_DTA_CI + HEMC_NBR_RSP_DTA_SIGN + HEMC_NBR_RSP_DTA + HEMC_NBR_RSP_WNG_CI + HEMC_NBR_RSP_WNG_SIGN + HEMC_NBR_RSP_WNG
				+ HEMC_NBR_RSP_NLBE_CI + HEMC_NBR_RSP_NLBE_SIGN + HEMC_NBR_RSP_NLBE + HEMC_NBR_PRC_HDR_CI + HEMC_NBR_PRC_HDR_SIGN + HEMC_NBR_PRC_HDR
				+ HEMC_NBR_PRC_DTA_CI + HEMC_NBR_PRC_DTA_SIGN + HEMC_NBR_PRC_DTA + HEMC_NBR_PRC_WNG_CI + HEMC_NBR_PRC_WNG_SIGN + HEMC_NBR_PRC_WNG
				+ HEMC_NBR_PRC_NLBE_CI + HEMC_NBR_PRC_NLBE_SIGN + HEMC_NBR_PRC_NLBE + HEMC_LOCK_TYPE_CD_CI + HEMC_LOCK_TYPE_CD + LOCK_ATN_CD_CI
				+ LOCK_ATN_CD + LOCK_DEL_TM_CI + LOCK_DEL_TM_SIGN + LOCK_DEL_TM + HEMC_LOCK_CTL_IND_CI + HEMC_LOCK_CTL_IND + HEMC_DTA_PVC_IND_CI
				+ HEMC_DTA_PVC_IND + HEMC_SEC_CTXT_IND_CI + HEMC_SEC_CTXT_IND_NI + HEMC_SEC_CTXT_IND + HEMC_AUT_NBR_CI + HEMC_AUT_NBR_SIGN
				+ HEMC_AUT_NBR + HEMC_ASC_TYP_CD_CI + HEMC_ASC_TYP_CD_NI + HEMC_ASC_TYP_CD + HEMC_DP_RET_CD_CI + HEMC_DP_RET_CD_NI + HEMC_DP_RET_CD
				+ HEMC_AUDIT_IND_CI + HEMC_AUDIT_IND + HEMC_AUDIT_BOBJ_NM_CI + HEMC_AUDIT_BOBJ_NM_NI + HEMC_AUDIT_BOBJ_NM + HEMC_AUD_DTA_TXT_CI
				+ HEMC_AUD_DTA_TXT_NI + HEMC_AUD_DTA_TXT + HEMC_KEEP_CLR_IND_CI + HEMC_KEEP_CLR_IND;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
