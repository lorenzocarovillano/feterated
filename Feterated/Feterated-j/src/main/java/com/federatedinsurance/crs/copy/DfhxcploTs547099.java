/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import static com.bphx.ctu.af.util.StringUtil.parseHexChar;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.ExciDplRetareaTs547099;
import com.federatedinsurance.crs.ws.ExciReturnCode;

/**Original name: DFHXCPLO<br>
 * Variable: DFHXCPLO from copybook DFHXCPLO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class DfhxcploTs547099 {

	//==== PROPERTIES ====
	//Original name: EXCI-RETURN-CODE
	private ExciReturnCode exciReturnCode = new ExciReturnCode();
	//Original name: EXCI-DPL-RETAREA
	private ExciDplRetareaTs547099 exciDplRetarea = new ExciDplRetareaTs547099();
	/**Original name: VERSION-1<br>
	 * <pre>****************************************************************
	 *    CALL API Parameter values
	 * ****************************************************************
	 * ****************************************************************
	 *   Constants for use with VERSION_NUMBER parameter
	 * ****************************************************************</pre>*/
	private long version1 = 1L;
	//Original name: DPL-REQUEST
	private long dplRequest = 6L;
	/**Original name: NOSYNCONRETURN<br>
	 * <pre>****************************************************************
	 *   Constants for use with DPL_OPTS parameter
	 * ****************************************************************</pre>*/
	private char nosynconreturn = parseHexChar("00");
	//Original name: SYNCONRETURN
	private char synconreturn = parseHexChar("80");

	//==== METHODS ====
	public void setVersion1(long version1) {
		this.version1 = version1;
	}

	public void setVersion1FromBuffer(byte[] buffer) {
		version1 = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getVersion1() {
		return this.version1;
	}

	public String getVersion1Formatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getVersion1()).toString();
	}

	public void setDplRequest(long dplRequest) {
		this.dplRequest = dplRequest;
	}

	public void setDplRequestFromBuffer(byte[] buffer) {
		dplRequest = MarshalByte.readBinaryUnsignedInt(buffer, 1);
	}

	public long getDplRequest() {
		return this.dplRequest;
	}

	public String getDplRequestFormatted() {
		return PicFormatter.display(new PicParams("9(8)").setUsage(PicUsage.BINARY)).format(getDplRequest()).toString();
	}

	public char getNosynconreturn() {
		return this.nosynconreturn;
	}

	public char getSynconreturn() {
		return this.synconreturn;
	}

	public ExciDplRetareaTs547099 getExciDplRetarea() {
		return exciDplRetarea;
	}

	public ExciReturnCode getExciReturnCode() {
		return exciReturnCode;
	}
}
