/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-FIRST-NAME-FOUND-SW<br>
 * Variable: WS-FIRST-NAME-FOUND-SW from program CIWBNSRB<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFirstNameFoundSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char FOUND = '1';
	public static final char NOT_FOUND = '0';

	//==== METHODS ====
	public void setWsFirstNameFoundSw(char wsFirstNameFoundSw) {
		this.value = wsFirstNameFoundSw;
	}

	public char getWsFirstNameFoundSw() {
		return this.value;
	}

	public void setFound() {
		value = FOUND;
	}

	public boolean isNotFound() {
		return value == NOT_FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}
}
