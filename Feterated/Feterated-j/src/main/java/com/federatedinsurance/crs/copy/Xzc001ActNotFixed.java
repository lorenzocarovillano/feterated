/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: XZC001-ACT-NOT-FIXED<br>
 * Variable: XZC001-ACT-NOT-FIXED from copybook XZ0C0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc001ActNotFixed {

	//==== PROPERTIES ====
	//Original name: XZC001-ACT-NOT-CSUM
	private String actNotCsum = DefaultValues.stringVal(Len.ACT_NOT_CSUM);
	//Original name: XZC001-CSR-ACT-NBR-KCRE
	private String csrActNbrKcre = DefaultValues.stringVal(Len.CSR_ACT_NBR_KCRE);
	//Original name: XZC001-NOT-PRC-TS-KCRE
	private String notPrcTsKcre = DefaultValues.stringVal(Len.NOT_PRC_TS_KCRE);

	//==== METHODS ====
	public void setActNotFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotCsum = MarshalByte.readFixedString(buffer, position, Len.ACT_NOT_CSUM);
		position += Len.ACT_NOT_CSUM;
		csrActNbrKcre = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		notPrcTsKcre = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS_KCRE);
	}

	public byte[] getActNotFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotCsum, Len.ACT_NOT_CSUM);
		position += Len.ACT_NOT_CSUM;
		MarshalByte.writeString(buffer, position, csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
		position += Len.CSR_ACT_NBR_KCRE;
		MarshalByte.writeString(buffer, position, notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
		return buffer;
	}

	public void initActNotFixedSpaces() {
		actNotCsum = "";
		csrActNbrKcre = "";
		notPrcTsKcre = "";
	}

	public void setActNotCsumFormatted(String actNotCsum) {
		this.actNotCsum = Trunc.toUnsignedNumeric(actNotCsum, Len.ACT_NOT_CSUM);
	}

	public int getActNotCsum() {
		return NumericDisplay.asInt(this.actNotCsum);
	}

	public String getActNotCsumFormatted() {
		return this.actNotCsum;
	}

	public String getActNotCsumAsString() {
		return getActNotCsumFormatted();
	}

	public void setCsrActNbrKcre(String csrActNbrKcre) {
		this.csrActNbrKcre = Functions.subString(csrActNbrKcre, Len.CSR_ACT_NBR_KCRE);
	}

	public String getCsrActNbrKcre() {
		return this.csrActNbrKcre;
	}

	public String getCsrActNbrKcreFormatted() {
		return Functions.padBlanks(getCsrActNbrKcre(), Len.CSR_ACT_NBR_KCRE);
	}

	public void setNotPrcTsKcre(String notPrcTsKcre) {
		this.notPrcTsKcre = Functions.subString(notPrcTsKcre, Len.NOT_PRC_TS_KCRE);
	}

	public String getNotPrcTsKcre() {
		return this.notPrcTsKcre;
	}

	public String getNotPrcTsKcreFormatted() {
		return Functions.padBlanks(getNotPrcTsKcre(), Len.NOT_PRC_TS_KCRE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_CSUM = 9;
		public static final int CSR_ACT_NBR_KCRE = 32;
		public static final int NOT_PRC_TS_KCRE = 32;
		public static final int ACT_NOT_FIXED = ACT_NOT_CSUM + CSR_ACT_NBR_KCRE + NOT_PRC_TS_KCRE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
