/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-66-USR-DFHXCOPT-LOAD-MSG<br>
 * Variable: EA-66-USR-DFHXCOPT-LOAD-MSG from program TS548099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea66UsrDfhxcoptLoadMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG
	private String flr1 = "TS548099 -";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-1
	private String flr2 = "LOAD OF EXCI";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-2
	private String flr3 = "MODULE FAILED.";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-3
	private String flr4 = "THE EXCI";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-4
	private String flr5 = "LIBRARY WAS";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-5
	private String flr6 = "POSSIBLY NOT";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-6
	private String flr7 = "INCLUDED IN";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-7
	private String flr8 = "JCL. VERIFY";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-8
	private String flr9 = "AND CHECK";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-9
	private String flr10 = "TS548099";
	//Original name: FILLER-EA-66-USR-DFHXCOPT-LOAD-MSG-10
	private String flr11 = "DOCUMENTATION.";

	//==== METHODS ====
	public String getEa66UsrDfhxcoptLoadMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa66UsrDfhxcoptLoadMsgBytes());
	}

	public byte[] getEa66UsrDfhxcoptLoadMsgBytes() {
		byte[] buffer = new byte[Len.EA66_USR_DFHXCOPT_LOAD_MSG];
		return getEa66UsrDfhxcoptLoadMsgBytes(buffer, 1);
	}

	public byte[] getEa66UsrDfhxcoptLoadMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR11);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public String getFlr11() {
		return this.flr11;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 11;
		public static final int FLR2 = 13;
		public static final int FLR3 = 15;
		public static final int FLR4 = 9;
		public static final int FLR5 = 12;
		public static final int FLR9 = 10;
		public static final int FLR11 = 14;
		public static final int EA66_USR_DFHXCOPT_LOAD_MSG = FLR11 + FLR1 + 2 * FLR2 + FLR3 + 2 * FLR4 + 3 * FLR5 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
