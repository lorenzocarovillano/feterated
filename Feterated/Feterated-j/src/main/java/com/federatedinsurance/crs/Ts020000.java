/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import static java.lang.Math.abs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TpSession;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.tp.handle.ContinueConditionHandler;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.MathFunctions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.date.DateFunctions;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVDao;
import com.federatedinsurance.crs.commons.data.dao.HalMsgTransprtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowPrcSeqVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowTransactVDao;
import com.federatedinsurance.crs.commons.data.dao.Sysdummy1Dao;
import com.federatedinsurance.crs.copy.CscGeneralParms;
import com.federatedinsurance.crs.copy.Halludat;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.MdrvDriverInfo;
import com.federatedinsurance.crs.copy.MtcsMsgTransportInfo;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UbocCommInfo;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.copy.UtcsUowTransConfigInfo;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.DfhcommareaTs020000;
import com.federatedinsurance.crs.ws.Ts020000Data;
import com.federatedinsurance.crs.ws.UbocRecord;
import com.federatedinsurance.crs.ws.WorkArea;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

//cannot import type RuntimeException, it resides in default package
/**Original name: TS020000<br>
 * <pre>AUTHOR.  DAVE FJORDBAK.
 * DATE-WRITTEN. JUN 2005.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  MESSAGE CONTROL MODULE                     **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -  TO PROCESS INCOMING REQUESTS BY CALLING THE      **
 * *            REQUEST MODULE TO FORMAT THE REQUEST UMT, THEN   **
 * *            CALLING ALL THE PRIMARY BUSINESS OBJECTS         **
 * *            ASSOCIATED WITH THE UOW, THEN CALLING THE        **
 * *            RESPONSE MODULE TO READ THE RESPONSE UMT AND     **
 * *            PUT THE DATA IN THE RETURN COPYBOOK.             **
 * *                                                             **
 * * PROGRAM INITIATION -  TS020000 IS STARTED BY A LINK FROM    **
 * *                       THE IVORY OR COBOL PROXY.             **
 * *                                                             **
 * * DATA ACCESS METHODS - TEMPORARY STORAGE RECORDS             **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #       DATE     PROG    DESCRIPTION                     **
 * * --------  --------- ------  --------------------------------**
 * * TS129     01JUN05   E404DNF NEW MODULE FOR FED COBOL FRAMEWORK
 * * TS129A    26SEP06   E404LJL TURN PERFORMANCE LOGGING OFF
 * * CL00023   13JUN07   E404JSP CHANGED FATAL ERROR MESSAGE (WS)
 * * CL00024   13JUN07   E404JSP LOGGABLE WARN MSG LOGIC CHG (4120)
 * * TL000011  21MAR08   E404DMA CHANGE HOW MESSAGE IDS GENERATE
 * *                             TO MAKE THEM MORE VOLATILE (UNIQUE)
 * * TL000106  17SEP08   E404DMA CHANGE HOW MESSAGE IDS GENERATE
 * *                             TO MAKE THEM EVEN MORE VOLATILE
 * * TL000127  19AUG09   E404DMA UPDATED LOGIC FOR ABEND HANDLING TO
 * *                             HELP PREVENT IMPLICIT COMMITS. NOTE
 * *                             THAT DPL REQUESTS STILL REQUIRE
 * *                             GOOD UNIT OF WORK MANAGEMENT.
 * * TL000377  07JUL13   E404DMB UPDATED SECURITY ERROR HANDLING
 * ****************************************************************</pre>*/
public class Ts020000 extends Program {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private Sysdummy1Dao sysdummy1Dao = new Sysdummy1Dao(dbAccessStatus);
	private HalMsgTransprtVDao halMsgTransprtVDao = new HalMsgTransprtVDao(dbAccessStatus);
	private HalUowTransactVDao halUowTransactVDao = new HalUowTransactVDao(dbAccessStatus);
	private HalUowPrcSeqVDao halUowPrcSeqVDao = new HalUowPrcSeqVDao(dbAccessStatus);
	private HalBoMduXrfVDao halBoMduXrfVDao = new HalBoMduXrfVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Ts020000Data ws = new Ts020000Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020000 dfhcommarea;

	//==== CONSTRUCTORS ====
	public Ts020000() {
		registerListeners();
	}

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020000 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		main1();
		programExit();
		return 0;
	}

	public static Ts020000 getInstance() {
		return (Programs.getInstance(Ts020000.class));
	}

	/**Original name: 1000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * * THE 1000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE   **
	 * * PROCESSING OF THE REQUEST PASSED TO IT.                      **
	 * *****************************************************************</pre>*/
	private void main1() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//             OR
		//              MDRV-MAINDRVR-LOGGABLE-ERRS
		//             OR
		//              MDRV-UOW-LOGGABLE-ERRS
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().isLoggableErrs()
				|| ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		// COB_CODE: IF SW-NO-NONLOG-BUS-ERRORS-FND
		//             AND
		//              NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 3000-PROCESS-REQUEST-MODULE
		//           END-IF.
		if (!ws.getSwitches().isNonlogBusErrFlag() && !ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 3000-PROCESS-REQUEST-MODULE
			processRequestModule();
		}
		// COB_CODE: IF SW-NO-NONLOG-BUS-ERRORS-FND
		//             AND
		//              NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 4000-PROCESS-UOW-PRIMARY-BOS
		//           END-IF.
		if (!ws.getSwitches().isNonlogBusErrFlag() && !ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 4000-PROCESS-UOW-PRIMARY-BOS
			processUowPrimaryBos();
		}
		// COB_CODE: IF UBOC-NBR-DATA-ROWS > 0
		//             AND
		//              SW-NO-NONLOG-BUS-ERRORS-FND
		//             AND
		//              NOT MDRV-UOW-LOGGABLE-ERRS
		//             AND
		//              NOT UBOC-HALT-AND-RETURN
		//               PERFORM 5000-PROCESS-RESPONSE-MODULE
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrDataRows() > 0 && !ws.getSwitches().isNonlogBusErrFlag()
				&& !ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()
				&& !ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 5000-PROCESS-RESPONSE-MODULE
			processResponseModule();
		}
		//*   6000- WILL FIRST CURSOR THRU THE NLBE AND UWRN UMTS AND
		//*   COPY THE MESSAGES TO THE COMMON COPYBOOK, THEN IF THE
		//*   FLAG IS SET TO CLEAR OUT THE UMTS AND THE TSQS, IT WILL
		//*   PROCESS THEM.
		// COB_CODE: PERFORM 6000-CLEAR-UMTS-TSQS.
		clearUmtsTsqs();
		//    MOVE THE DATA TO LINKAGE.
		// COB_CODE: MOVE COMMUNICATION-SHELL-COMMON OF WS-HUB-DATA
		//                                       TO COMMUNICATION-SHELL-COMMON
		//                                          OF DFHCOMMAREA.
		dfhcommarea.getCommunicationShellCommon()
				.setCommunicationShellCommonBytes(ws.getWsHubData().getCommunicationShellCommon().getCommunicationShellCommonBytes());
	}

	/**Original name: 1000-PROGRAM-EXIT<br>
	 * <pre>* TRIGGER HALOUMEL TRANSACTION FOR ERROR LOGGING</pre>*/
	private void programExit() {
		// COB_CODE: IF MDRV-MAINDRVR-LOGGABLE-ERRS
		//             OR
		//              MDRV-MAINDRVR-LOGGABLE-WARNS
		//             OR
		//              MDRV-UOW-LOGGABLE-ERRS
		//             OR
		//              MDRV-UOW-LOGGABLE-WARNS
		//               PERFORM 9300-START-HMEL-TRAN
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().isLoggableErrs()
				|| ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().isLoggableWarns()
				|| ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()
				|| ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableWarns()) {
			// COB_CODE: PERFORM 9300-START-HMEL-TRAN
			startHmelTran();
		}
		// COB_CODE: IF MDRV-SYNCPOINT-IN-MAINDRVR
		//               PERFORM 9901-PROCESS-SYNC
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getSyncpointLocation().isMaindrvr()) {
			// COB_CODE: PERFORM 9901-PROCESS-SYNC
			processSync();
		}
		// COB_CODE: MOVE DSD-PM-BROKER-BEGIN    TO TE-TRAN-START-TIME.
		ws.getTimeElapsedWork().setStartTimeFormatted(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmBrokerBeginFormatted());
		// COB_CODE: COMPUTE TE-TRAN-START-TIME-HTH
		//                                 = (TE-TRAN-HOURS-START * 60 * 60 * 100)
		//                                 + (TE-TRAN-MINS-START * 60 * 100)
		//                                 + (TE-TRAN-SECS-START * 100)
		//                                 +  TE-TRAN-HTHS-START.
		ws.getTimeElapsedWork().setStartTimeHth(
				Trunc.toInt(ws.getTimeElapsedWork().getHoursStart() * 60 * 60 * 100 + ws.getTimeElapsedWork().getMinsStart() * 60 * 100
						+ ws.getTimeElapsedWork().getSecsStart() * 100 + ws.getTimeElapsedWork().getHthsStart(), 8));
		// COB_CODE: PERFORM 9902-PROCESS-ELAPSED-TIME.
		processElapsedTime();
		// COB_CODE: MOVE TE-TRAN-END-TIME       TO DSD-PM-BROKER-END.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().setPmBrokerEndFromBuffer(ws.getTimeElapsedWork().getEndTimeBytes());
		// COB_CODE: COMPUTE DSD-PM-BROKER-ELAPSED = TE-TRANS-ELAPSED-TIME-AMT
		//                                         / 100.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms()
				.setPmBrokerElapsed(new AfDecimal(((((double) (ws.getTimeElapsedWork().getsElapsedTimeAmt()))) / 100), 10, 2));
		// COB_CODE: MOVE CSC-OPERATION OF DFHCOMMAREA
		//                                       TO WA-TD-OPERATION-NAME.
		ws.getWorkArea().getWaTransDataQueInfo().setOperationName(dfhcommarea.getCommunicationShellCommon().getCscGeneralParms().getOperation());
		// COB_CODE: MOVE WA-FORMATTED-DATE      TO WA-TD-CURRENT-DATE-YYYYMMDD.
		ws.getWorkArea().getWaTransDataQueInfo().setCurrentDateYyyymmdd(ws.getWorkArea().getWaFormattedDate().getWaFormattedDateFormatted());
		// COB_CODE: MOVE DSD-PM-BROKER-BEGIN    TO WA-TD-BROKER-BEGIN.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setBrokerBeginFormatted(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmBrokerBeginFormatted());
		// COB_CODE: MOVE DSD-PM-BROKER-END      TO WA-TD-BROKER-END.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setBrokerEndFormatted(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmBrokerEndFormatted());
		// COB_CODE: MOVE DSD-PM-BROKER-ELAPSED  TO WA-TD-BROKER-ELAPSED.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setBrokerElapsed(Trunc.toDecimal(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmBrokerElapsed(), 5, 2));
		// COB_CODE: MOVE DSD-PM-REQHUB-ELAPSED  TO WA-TD-REQHUB-ELAPSED.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setReqhubElapsed(Trunc.toDecimal(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmReqhubElapsed(), 5, 2));
		// COB_CODE: MOVE DSD-PM-UOW-ELAPSED     TO WA-TD-UOW-ELAPSED.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setUowElapsed(Trunc.toDecimal(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmUowElapsed(), 5, 2));
		// COB_CODE: MOVE DSD-PM-RESHUB-ELAPSED  TO WA-TD-RESHUB-ELAPSED.
		ws.getWorkArea().getWaTransDataQueInfo()
				.setReshubElapsed(Trunc.toDecimal(dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().getPmReshubElapsed(), 5, 2));
		//**  DISABLE PERFORMANCE LOGGING
		//**  EXEC CICS WRITEQ TD QUEUE('EXCD')
		//**      FROM(WA-TRANS-DATA-QUE-INFO) NOHANDLE
		//**  END-EXEC.
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PERFORM INITIALIZATION OF VARIABLES.
	 * *****************************************************************</pre>*/
	private void beginningHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE COMMUNICATION-SHELL-COMMON OF DFHCOMMAREA
		//                                       TO COMMUNICATION-SHELL-COMMON
		//                                          OF WS-HUB-DATA.
		ws.getWsHubData().getCommunicationShellCommon()
				.setCommunicationShellCommonBytes(dfhcommarea.getCommunicationShellCommon().getCommunicationShellCommonBytes());
		//*   GET THE TRANSACTION STARTING TIME AT THIS POINT IN CASE THE
		//*   TRANSACTION ELAPSED TIME HAS BEEN REQUESTED.
		// COB_CODE: MOVE ZERO                   TO TE-TRAN-START-TIME
		//                                          TE-TRAN-END-TIME
		//                                          TE-TRAN-START-TIME-HTH
		//                                          TE-TRAN-END-TIME-HTH.
		ws.getTimeElapsedWork().initStartTimeZeroes();
		ws.getTimeElapsedWork().initEndTimeZeroes();
		ws.getTimeElapsedWork().setStartTimeHth(0);
		ws.getTimeElapsedWork().setEndTimeHth(0);
		// COB_CODE: ACCEPT TE-TRAN-START-TIME FROM TIME.
		ws.getTimeElapsedWork().setStartTimeFormatted(CalendarUtil.getTimeHHMMSSMM());
		// COB_CODE: MOVE TE-TRAN-START-TIME     TO DSD-PM-BROKER-BEGIN.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().setPmBrokerBeginFromBuffer(ws.getTimeElapsedWork().getStartTimeBytes());
		// COB_CODE: MOVE FUNCTION CURRENT-DATE  TO WA-CURRENT-DATE.
		ws.getWorkArea().setWaCurrentDate(DateFunctions.getCurrentDate());
		// COB_CODE: MOVE WA-CURRENT-DATE(1:4)   TO WA-FD-YYYY.
		ws.getWorkArea().getWaFormattedDate().setYyyy(ws.getWorkArea().getWaCurrentDateFormatted().substring((1) - 1, 4));
		// COB_CODE: MOVE WA-CURRENT-DATE(5:2)   TO WA-FD-MM.
		ws.getWorkArea().getWaFormattedDate().setMm(ws.getWorkArea().getWaCurrentDateFormatted().substring((5) - 1, 6));
		// COB_CODE: MOVE WA-CURRENT-DATE(7:2)   TO WA-FD-DD.
		ws.getWorkArea().getWaFormattedDate().setDd(ws.getWorkArea().getWaCurrentDateFormatted().substring((7) - 1, 8));
		//***************************************************************
		//*   IF AN ABEND IS ENCOUNTERED IN THIS MODULE, OR BELOW IT
		//*   IN THE CALL CHAIN, ABEND HANDLING SECTION WILL BE PERFORMED.
		//*
		// COB_CODE: EXEC CICS
		//               HANDLE ABEND
		//               LABEL (9999-HANDLE-ABEND)
		//           END-EXEC.
		execContext.addConditionHandler(TpConditionType.ABEND, new ContinueConditionHandler("9999-HANDLE-ABEND_FIRST_SENTENCES"));
		// INITIALIZE ERROR PROCESSING FIELDS
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO
		//                      UBOC-RECORD
		//                      CSC-ERROR-DETAILS OF WS-HUB-DATA.
		initEstoStoreInfo();
		initEstoReturnInfo();
		initUbocRecord();
		initCscErrorDetails();
		// COB_CODE: INITIALIZE MDRV-DRIVER-INFO.
		initDriverInfo();
		// COB_CODE: SET DSD-NO-ERROR-CODE       TO TRUE.
		dfhcommarea.getDriverSpecificData().getErrorReturnCode().setNoErrorCode();
		// COB_CODE: EXEC SQL
		//               SELECT CURRENT TIMESTAMP
		//                 INTO :WA-TIMESTAMP
		//                 FROM SYSIBM.SYSDUMMY1
		//           END-EXEC.
		ws.getWorkArea().getFillerWorkArea().setWaTimestamp(sysdummy1Dao.selectRec2(ws.getWorkArea().getFillerWorkArea().getWaTimestamp()));
		// COB_CODE: STRING WA-TS-ONES-MINUTE
		//                  WA-TS-SEC
		//                  WA-TS-MILLISEC DELIMITED BY SIZE
		//               INTO WA-SEED-X
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(WorkArea.Len.WA_SEED_X, String.valueOf(ws.getWorkArea().getFillerWorkArea().getTsOnesMinute()),
				ws.getWorkArea().getFillerWorkArea().getTsSecFormatted(), ws.getWorkArea().getFillerWorkArea().getTsMillisecFormatted());
		ws.getWorkArea().setWaSeedXFormatted(concatUtil.replaceInString(ws.getWorkArea().getWaSeedXFormatted()));
		// COB_CODE: COMPUTE WA-RANDOM-NUMBER1 = FUNCTION RANDOM (WA-SEED).
		ws.getWorkArea().setWaRandomNumber1(Trunc.toDecimal(
				MathUtil.convertRoundDecimal((MathFunctions.random(((ws.getWorkArea().getWaSeed())))), 8, RoundingMode.ROUND_UP, 31, 8), 8,
				8));
		// COB_CODE: ADD +1                      TO WA-SEED.
		ws.getWorkArea().setWaSeed(Trunc.toInt(1 + ws.getWorkArea().getWaSeed(), 9));
		// COB_CODE: COMPUTE WA-RANDOM-NUMBER2 = FUNCTION RANDOM (WA-SEED).
		ws.getWorkArea().setWaRandomNumber2(Trunc.toDecimal(
				MathUtil.convertRoundDecimal((MathFunctions.random(((ws.getWorkArea().getWaSeed())))), 8, RoundingMode.ROUND_UP, 31, 8), 8,
				8));
		// COB_CODE: ADD +1                      TO WA-SEED.
		ws.getWorkArea().setWaSeed(Trunc.toInt(1 + ws.getWorkArea().getWaSeed(), 9));
		// COB_CODE: COMPUTE WA-RANDOM-NUMBER3 = FUNCTION RANDOM (WA-SEED).
		ws.getWorkArea().setWaRandomNumber3(Trunc.toDecimal(
				MathUtil.convertRoundDecimal((MathFunctions.random(((ws.getWorkArea().getWaSeed())))), 8, RoundingMode.ROUND_UP, 31, 8), 8,
				8));
		// COB_CODE: MOVE EIBTASKN               TO WA-RANDOM-NUMBER4.
		ws.getWorkArea().setWaRandomNumber4(Trunc.toInt(execContext.getTaskNum(), 8));
		// COB_CODE: STRING WA-RANDOM-NUMBER1-X
		//                  WA-RANDOM-NUMBER2-X
		//                  WA-RANDOM-NUMBER3-X
		//                  WA-RANDOM-NUMBER4-X DELIMITED BY SIZE
		//               INTO WA-MESSAGE-ID
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(WorkArea.Len.WA_MESSAGE_ID, ws.getWorkArea().getWaRandomNumber1XFormatted(),
				ws.getWorkArea().getWaRandomNumber2XFormatted(), ws.getWorkArea().getWaRandomNumber3XFormatted(),
				ws.getWorkArea().getWaRandomNumber4XFormatted());
		ws.getWorkArea().setWaMessageId(concatUtil.replaceInString(ws.getWorkArea().getWaMessageIdFormatted()));
		//*   FILL IN THE MAIN DRIVER COPYBOOK ELEMENTS **********
		// COB_CODE: MOVE WA-MESSAGE-ID          TO MDRV-MSG-ID
		//                                          CSC-MSG-ID OF WS-HUB-DATA
		//                                          UBOC-MSG-ID.
		ws.getHallmdrv().getDriverInfo().setMsgId(ws.getWorkArea().getWaMessageId());
		ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().setMsgId(ws.getWorkArea().getWaMessageId());
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMsgId(ws.getWorkArea().getWaMessageId());
		//*   IF THE SESSION-ID HAS BEEN PROVIDED BY THE CALLING APP,
		//*   USE IT.  IF NOT, USE THE GENERATED MESSAGE-ID FOR THE
		//*   SESSION-ID.
		// COB_CODE: IF (CSC-SESSION-ID OF WS-HUB-DATA EQUAL SPACES OR LOW-VALUES)
		//                                          MDRV-SESSION-ID
		//           ELSE
		//                                       TO MDRV-SESSION-ID
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getSessionId()) || Characters.EQ_LOW
				.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getSessionId(), CscGeneralParms.Len.SESSION_ID)) {
			// COB_CODE: MOVE WA-MESSAGE-ID      TO CSC-SESSION-ID OF WS-HUB-DATA
			//                                      MDRV-SESSION-ID
			ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().setSessionId(ws.getWorkArea().getWaMessageId());
			ws.getHallmdrv().getDriverInfo().setSessionId(ws.getWorkArea().getWaMessageId());
		} else {
			// COB_CODE: MOVE CSC-SESSION-ID OF WS-HUB-DATA
			//                                   TO MDRV-SESSION-ID
			ws.getHallmdrv().getDriverInfo().setSessionId(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getSessionId());
		}
		// COB_CODE: MOVE 1                      TO MDRV-MSG-REC-SEQ.
		ws.getHallmdrv().getDriverInfo().setMsgRecSeq(((short) 1));
		// COB_CODE: SET MDRV-MESSAGE-COMPLETE   TO TRUE.
		ws.getHallmdrv().getDriverInfo().getMessageTransferCd().setMessageComplete();
		// COB_CODE: MOVE CSC-UNIT-OF-WORK OF WS-HUB-DATA
		//                                       TO MDRV-UNIT-OF-WORK
		//                                          UBOC-UOW-NAME.
		ws.getHallmdrv().getDriverInfo().setUnitOfWork(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getUnitOfWork());
		ws.getWsHubData().getUbocRecord().getCommInfo()
				.setUbocUowName(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getUnitOfWork());
		// COB_CODE: MOVE CSC-AUTH-USERID OF WS-HUB-DATA
		//                                       TO MDRV-AUTH-USERID.
		ws.getHallmdrv().getDriverInfo().setAuthUserid(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getAuthUserid());
		// COB_CODE: MOVE SPACES                 TO MDRV-TERMINAL-ID.
		ws.getHallmdrv().getDriverInfo().setTerminalId("");
		// COB_CODE: SET MDRV-DELETE-STORE       TO TRUE.
		ws.getHallmdrv().getDriverInfo().getDeleteUowStore().setDeleteStore();
		// COB_CODE: SET MDRV-RETURN-WARNINGS    TO TRUE.
		ws.getHallmdrv().getDriverInfo().getReturnWarningsInd().setReturnWarnings();
		// COB_CODE: MOVE 0                      TO MDRV-TOTAL-LENGTH.
		ws.getHallmdrv().getDriverInfo().setTotalLength(0);
		// COB_CODE: MOVE 0                      TO MDRV-UOW-BUFFER-LENGTH.
		ws.getHallmdrv().getDriverInfo().setUowBufferLength(0);
		// COB_CODE: MOVE 1                      TO MDRV-NUM-CHUNKS.
		ws.getHallmdrv().getDriverInfo().setNumChunks(((short) 1));
		// COB_CODE: SET MDRV-PERFORM-COMMIT     TO TRUE.
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setCommit();
		// COB_CODE: SET MDRV-MAINDRVR-OK        TO TRUE.
		ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setOk();
		// COB_CODE: MOVE SPACES                 TO MDRV-MAIN-DRVR-ERRCODE.
		ws.getHallmdrv().getDriverInfo().setMainDrvrErrcode("");
		// COB_CODE: SET MDRV-UOW-OK             TO TRUE.
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setOk();
		// COB_CODE: MOVE SPACES                 TO MDRV-UNIT-OF-WORK-ERRCODE.
		ws.getHallmdrv().getDriverInfo().setUnitOfWorkErrcode("");
		// COB_CODE: INITIALIZE MDRV-ERROR-DETAILS.
		initErrorDetails();
		// COB_CODE: SET MDRV-LOG-ONLY-NOT-SET   TO TRUE.
		ws.getHallmdrv().getDriverInfo().getLoggableErrLogOnlySw().setNotSet();
		// COB_CODE: IF NOT DSD-BYPASS-SYNCPOINT-IN-MDRV
		//                                       TO TRUE
		//           ELSE
		//                                       TO TRUE
		//           END-IF.
		if (!dfhcommarea.getDriverSpecificData().getBypassSyncpointMdrvInd().isBypassSyncpointInMdrv()) {
			// COB_CODE: SET MDRV-SYNCPOINT-IN-MAINDRVR
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getSyncpointLocation().setMaindrvr();
		} else {
			// COB_CODE: SET MDRV-SYNCPOINT-IN-INVOKER
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getSyncpointLocation().setInvoker();
		}
		// COB_CODE: SET MDRV-REQ-IN-MRQM-UMT    TO TRUE.
		ws.getHallmdrv().getDriverInfo().getRequestMsgProcessing().setReqInMrqmUmt();
		// COB_CODE: SET MDRV-RESP-IN-MRSM-UMT   TO TRUE.
		ws.getHallmdrv().getDriverInfo().getResponseMsgProcessing().setRespInMrsmUmt();
		// COB_CODE: SET MDRV-MSGTRAN-OK         TO TRUE.
		ws.getHallmdrv().getDriverInfo().getMsgtranLoggableProblems().setOk();
		//*   SHUT OFF GENERIC SWITH TO CALCULATE TIME ELAPSED.
		// COB_CODE: MOVE SPACES                 TO MDRV-TRANS-ELAPSED-TIME-SW.
		ws.getHallmdrv().getDriverInfo().getTransElapsedTimeSw().setTransElapsedTimeSw(Types.SPACE_CHAR);
		// COB_CODE: PERFORM 2100-SETUP.
		setup();
	}

	/**Original name: 2100-SETUP_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  THIS SECTION WILL VALIDATE PASSED IN INFO, QUERY UOW    *
	 *  SUPPORT TABLES, BUILD COMMON VARIABLES, AND CHECK THE   *
	 *  SECURITY LEVEL OF THE CALLER.                           *
	 * **********************************************************</pre>*/
	private void setup() {
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE   TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: SET UBOC-LOG-ONLY-NOT-SET   TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setUbocLogOnlyNotSet();
		//* INITIALIZE NUMERIC FIELDS THAT ARE MOVED INTO ERROR STORAGE
		//* THAT HAVE NOT BEEN SET ALREADY.
		// COB_CODE: MOVE ZERO                   TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: PERFORM 2110-VALIDATE-PASSED-INFO.
		validatePassedInfo();
		// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 2120-READ-MSG-TRANSPORT
		//           END-IF.
		if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 2120-READ-MSG-TRANSPORT
			readMsgTransport();
		}
		// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 2130-VALIDATE-MSG-TRANSPORT
		//           END-IF.
		if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 2130-VALIDATE-MSG-TRANSPORT
			validateMsgTransport();
		}
		// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 2140-READ-UOW-TRANSACTION
		//           END-IF.
		if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 2140-READ-UOW-TRANSACTION
			readUowTransaction();
		}
		// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 2150-VALIDATE-UOW-TRANSACT
		//           END-IF.
		if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 2150-VALIDATE-UOW-TRANSACT
			validateUowTransact();
		}
		// COB_CODE: PERFORM 2160-BUILD-UBOC-PARMS.
		buildUbocParms();
		// PROCESSING SUCH AS RETRIEVING THE CLIENT ID FOR THE USER,
		// RETRIEVING AUTHORITY LEVEL AND ASSOCIATION TYPE FOR DATA
		// PRIVACY AND SET DEFAULTS PROCESSING, AND UOW ACCESS AUTHORITY
		// REQUIRE THAT THE UNIT OF WORK TRANSACT SECURITY FLAG IS SET TO
		// 'Y' AND THAT A VALID SECURITY ACCESS MODULE IS SUPPLIED.
		// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
		//               GO TO 2100-SETUP-X
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: GO TO 2100-SETUP-X
			return;
		}
		// COB_CODE: IF UTCS-CHK-CONTROL-LVL-SEC
		//               PERFORM 2170-LINK-TO-SECURITY
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().isChkControlLvlSec()) {
			// COB_CODE: PERFORM 2170-LINK-TO-SECURITY
			linkToSecurity();
		}
	}

	/**Original name: 2110-VALIDATE-PASSED-INFO_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  VERIFY THAT THE INFORMATION SENT THRU LINKAGE IS VALID.*
	 * *********************************************************</pre>*/
	private void validatePassedInfo() {
		// COB_CODE: PERFORM 2111-VALIDATE-MDRV-INFO.
		validateMdrvInfo();
		// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
		//               PERFORM 2112-VALIDATE-COMMON-INFO
		//           END-IF.
		if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: PERFORM 2112-VALIDATE-COMMON-INFO
			validateCommonInfo();
		}
	}

	/**Original name: 2111-VALIDATE-MDRV-INFO_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  VERIFY THAT THE INFORMATION SENT THRU LINKAGE IN THE   *
	 *  MAIN DRIVER COPYBOOK IS VALID.                         *
	 * *********************************************************</pre>*/
	private void validateMdrvInfo() {
		// COB_CODE: IF MDRV-MSG-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 2111-VALIDATE-MDRV-INFO-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getMsgId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getMsgId(), MdrvDriverInfo.Len.MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE '2111-VALIDATE-MDRV-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2111-VALIDATE-MDRV-INFO");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2111-VALIDATE-MDRV-INFO");
			// COB_CODE: MOVE 'MAIN DRIVER STORE ID WAS EMPTY.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MAIN DRIVER STORE ID WAS EMPTY.");
			// COB_CODE: SET EFAL-COMMAREA-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2111-VALIDATE-MDRV-INFO-X
			return;
		}
		// COB_CODE: IF MDRV-UNIT-OF-WORK EQUAL SPACES OR LOW-VALUES
		//               GO TO 2111-VALIDATE-MDRV-INFO-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getUnitOfWork())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getUnitOfWork(), MdrvDriverInfo.Len.UNIT_OF_WORK)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE '2111-VALIDATE-MDRV-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2111-VALIDATE-MDRV-INFO");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2111-VALIDATE-MDRV-INFO");
			// COB_CODE: MOVE 'MAIN DRIVER UOW NAME WAS EMPTY. '
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MAIN DRIVER UOW NAME WAS EMPTY. ");
			// COB_CODE: SET EFAL-COMMAREA-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2111-VALIDATE-MDRV-INFO-X
			return;
		}
		// COB_CODE: IF MDRV-AUTH-USERID EQUAL SPACES OR LOW-VALUES
		//               GO TO 2111-VALIDATE-MDRV-INFO-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getAuthUserid())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getAuthUserid(), MdrvDriverInfo.Len.AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE '2111-VALIDATE-MDRV-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2111-VALIDATE-MDRV-INFO");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2111-VALIDATE-MDRV-INFO");
			// COB_CODE: MOVE 'MAIN DRIVER AUTHORIZED USERID WAS EMPTY. '
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MAIN DRIVER AUTHORIZED USERID WAS EMPTY. ");
			// COB_CODE: SET EFAL-COMMAREA-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2111-VALIDATE-MDRV-INFO-X
			return;
		}
		// COB_CODE: IF MDRV-SESSION-ID EQUAL SPACES OR LOW-VALUES
		//               GO TO 2111-VALIDATE-MDRV-INFO-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getHallmdrv().getDriverInfo().getSessionId())
				|| Characters.EQ_LOW.test(ws.getHallmdrv().getDriverInfo().getSessionId(), MdrvDriverInfo.Len.SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE '2111-VALIDATE-MDRV-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2111-VALIDATE-MDRV-INFO");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2111-VALIDATE-MDRV-INFO");
			// COB_CODE: MOVE 'MAIN DRIVER SESSION ID WAS EMPTY. '
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MAIN DRIVER SESSION ID WAS EMPTY. ");
			// COB_CODE: SET EFAL-COMMAREA-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2111-VALIDATE-MDRV-INFO-X
			return;
		}
	}

	/**Original name: 2112-VALIDATE-COMMON-INFO_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  VERIFY THAT THE INFORMATION SENT THRU LINKAGE IN THE   *
	 *  COMMON COPYBOOK IS VALID.                              *
	 * *********************************************************</pre>*/
	private void validateCommonInfo() {
		// COB_CODE: IF (CSC-OPERATION OF WS-HUB-DATA
		//                                          EQUAL SPACES OR LOW-VALUES)
		//               GO TO 2112-VALIDATE-COMMON-INFO-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getOperation()) || Characters.EQ_LOW
				.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getOperation(), CscGeneralParms.Len.OPERATION)) {
			// COB_CODE: SET FRAM-OPERATION-NAME-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setFramOperationNameBlank();
			// COB_CODE: MOVE '2112-VALIDATE-COMMON-INFO'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2112-VALIDATE-COMMON-INFO");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2112-VALIDATE-COMMON-INFO");
			// COB_CODE: MOVE 'OPERATION NAME IN COMMON COPYBOOK WAS EMPTY.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPERATION NAME IN COMMON COPYBOOK WAS EMPTY.");
			// COB_CODE: SET EFAL-COMMAREA-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2112-VALIDATE-COMMON-INFO-X
			return;
		}
	}

	/**Original name: 2120-READ-MSG-TRANSPORT_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  READ THE MESSAGE TRANSPORT CONFIGURATION INFORMATION    *
	 *  FOR THE CURRENT UNIT OF WORK.                           *
	 * **********************************************************</pre>*/
	private void readMsgTransport() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES                 TO WS-MSG-TRANSPORT-INFO.
		ws.initWsMsgTransportInfoSpaces();
		// COB_CODE: MOVE SPACES                 TO DCLHAL-MSG-TRANSPRT.
		ws.getDclhalMsgTransprt().initDclhalMsgTransprtSpaces();
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK      TO HMTC-UOW-NM.
		ws.getDclhalMsgTransprt().setUowNm(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: EXEC SQL
		//               SELECT UOW_NM,
		//                      HMTC_MSG_DEL_TM,
		//                      HMTC_WNG_DEL_TM,
		//                      HMTC_PASS_LNK_IND,
		//                      STG_TYP_CD,
		//                      HMTC_MDRV_REQ_STG,
		//                      HMTC_MDRV_RSP_STG,
		//                      HMTC_UOW_REQ_STG,
		//                      HMTC_UOW_SWI_STG,
		//                      HMTC_UOW_HDR_STG,
		//                      HMTC_UOW_DTA_STG,
		//                      HMTC_UOW_WNG_STG,
		//                      HMTC_UOW_KRP_STG,
		//                      HMTC_UOW_NLBE_STG
		//               INTO   :HMTC-UOW-NM,
		//                      :HMTC-MSG-DEL-TM,
		//                      :HMTC-WNG-DEL-TM,
		//                      :HMTC-PASS-LNK-IND,
		//                      :HMTC-STG-TYP-CD,
		//                      :HMTC-MDRV-REQ-STG,
		//                      :HMTC-MDRV-RSP-STG,
		//                      :HMTC-UOW-REQ-STG,
		//                      :HMTC-UOW-SWI-STG,
		//                      :HMTC-UOW-HDR-STG,
		//                      :HMTC-UOW-DTA-STG,
		//                      :HMTC-UOW-WNG-STG,
		//                      :HMTC-UOW-KRP-STG,
		//                      :HMTC-UOW-NLBE-STG
		//               FROM HAL_MSG_TRANSPRT_V
		//               WHERE UOW_NM            =: HMTC-UOW-NM
		//           END-EXEC.
		halMsgTransprtVDao.selectByHmtcUowNm(ws.getDclhalMsgTransprt().getUowNm(), ws.getDclhalMsgTransprt());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                                       TO WS-MSG-TRANSPORT-INFO
		//               WHEN OTHER
		//                   GO TO 2120-READ-MSG-TRANSPORT-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: MOVE DCLHAL-MSG-TRANSPRT
			//                               TO WS-MSG-TRANSPORT-INFO
			ws.setWsMsgTransportInfoBytes(ws.getDclhalMsgTransprt().getDclhalMsgTransprtBytes());
			break;

		default:// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: MOVE 'SELECT OF UOW MESSAGE TRANSPORT INFO FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF UOW MESSAGE TRANSPORT INFO FAILED.");
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: STRING 'MTS-UOW-NAME = '   HMTC-UOW-NM       ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "MTS-UOW-NAME = ",
					ws.getDclhalMsgTransprt().getUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_MSG_TRANSPRT_V'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_MSG_TRANSPRT_V");
			// COB_CODE: MOVE '2120-READ-MSG-TRANSPORT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2120-READ-MSG-TRANSPORT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2120-READ-MSG-TRANSPORT");
			// COB_CODE: IF ERD-SQL-RED LESS THAN ZERO
			//               MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getErdSqlRed() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE ERD-SQL-RED    TO EFAL-DB2-ERR-SQLCODE
			//                                  CSC-SQLCODE-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getErdSqlRed(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(sqlca.getErdSqlRed());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2120-READ-MSG-TRANSPORT-X
			return;
		}
	}

	/**Original name: 2130-VALIDATE-MSG-TRANSPORT_FIRST_SENTENCES<br>
	 * <pre>*************************************************************
	 *  VERIFY THAT THE REQUIRED DATA FROM HAL_MSG_TRANSPRT_V      *
	 *  FOR THE UNIT OF WORK IS PRESENT.                           *
	 * *************************************************************</pre>*/
	private void validateMsgTransport() {
		// COB_CODE: IF MTCS-MDR-REQ-STORE EQUAL SPACES OR LOW-VALUES
		//             OR (MTCS-MDR-RSP-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-REQ-MSG-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-REQ-SWITCHES-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-RESP-HEADER-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-RESP-DATA-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-RESP-WARNINGS-STORE EQUAL SPACES
		//                 OR LOW-VALUES)
		//             OR (MTCS-UOW-KEY-REPLACE-STORE EQUAL SPACES OR LOW-VALUES)
		//             OR (MTCS-UOW-RESP-NL-BL-ERRS-STORE EQUAL SPACES
		//                 OR LOW-VALUES)
		//                                            OF WS-HUB-DATA
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getMdrReqStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getMdrReqStore(), MtcsMsgTransportInfo.Len.MDR_REQ_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getMdrRspStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getMdrRspStore(), MtcsMsgTransportInfo.Len.MDR_RSP_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowReqMsgStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowReqMsgStore(), MtcsMsgTransportInfo.Len.UOW_REQ_MSG_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore(), MtcsMsgTransportInfo.Len.UOW_REQ_SWITCHES_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore(), MtcsMsgTransportInfo.Len.UOW_RESP_HEADER_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespDataStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespDataStore(), MtcsMsgTransportInfo.Len.UOW_RESP_DATA_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore(), MtcsMsgTransportInfo.Len.UOW_RESP_WARNINGS_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowKeyReplaceStore(), MtcsMsgTransportInfo.Len.UOW_KEY_REPLACE_STORE)
				|| Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore()) || Characters.EQ_LOW
						.test(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore(), MtcsMsgTransportInfo.Len.UOW_RESP_NL_BL_ERRS_STORE)) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-STORE-NAME-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspStoreNameBlank();
			// COB_CODE: MOVE '2130-VALIDATE-MSG-TRANSPORT'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2130-VALIDATE-MSG-TRANSPORT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2130-VALIDATE-MSG-TRANSPORT");
		}
		// COB_CODE: IF MTCS-MDR-REQ-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getMdrReqStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getMdrReqStore(), MtcsMsgTransportInfo.Len.MDR_REQ_STORE)) {
			// COB_CODE: MOVE 'MDRV REQ MESSAGE UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV REQ MESSAGE UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-MDR-RSP-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getMdrRspStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getMdrRspStore(), MtcsMsgTransportInfo.Len.MDR_RSP_STORE)) {
			// COB_CODE: MOVE 'MDRV RESP MESSAGE UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV RESP MESSAGE UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-REQ-MSG-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowReqMsgStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowReqMsgStore(), MtcsMsgTransportInfo.Len.UOW_REQ_MSG_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW REQ MSG UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW REQ MSG UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-REQ-SWITCHES-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore(), MtcsMsgTransportInfo.Len.UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW REQ SWITCHES UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW REQ SWITCHES UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-RESP-HEADER-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore(), MtcsMsgTransportInfo.Len.UOW_RESP_HEADER_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW RESP HEADER UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW RESP HEADER UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-RESP-DATA-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespDataStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespDataStore(), MtcsMsgTransportInfo.Len.UOW_RESP_DATA_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW RESP DATA UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW RESP DATA UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-RESP-WARNINGS-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore(), MtcsMsgTransportInfo.Len.UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW RESP WARNINGS UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW RESP WARNINGS UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-KEY-REPLACE-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowKeyReplaceStore())
				|| Characters.EQ_LOW.test(ws.getMtcsMsgTransportInfo().getUowKeyReplaceStore(), MtcsMsgTransportInfo.Len.UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW KEY REPLACE UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW KEY REPLACE UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
		// COB_CODE: IF MTCS-UOW-RESP-NL-BL-ERRS-STORE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2130-VALIDATE-MSG-TRANSPORT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore()) || Characters.EQ_LOW
				.test(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore(), MtcsMsgTransportInfo.Len.UOW_RESP_NL_BL_ERRS_STORE)) {
			// COB_CODE: MOVE 'MDRV UOW RESP NBE UMT FILENAME BLANK IN MTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MDRV UOW RESP NBE UMT FILENAME BLANK IN MTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2130-VALIDATE-MSG-TRANSPORT-X
			return;
		}
	}

	/**Original name: 2140-READ-UOW-TRANSACTION_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  READ THE UOW TRANSACTION CONFIGURATION INFORMATION      *
	 *  FOR THE CURRENT UNIT OF WORK.                           *
	 * **********************************************************</pre>*/
	private void readUowTransaction() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE SPACES                 TO WS-UOW-TRANSACTION-INFO.
		ws.initWsUowTransactionInfoSpaces();
		// COB_CODE: MOVE SPACES                 TO DCLHAL-UOW-TRANSACT.
		ws.getDclhalUowTransact().initDclhalUowTransactSpaces();
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK      TO HUTC-UOW-NM.
		ws.getDclhalUowTransact().setUowNm(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: EXEC SQL
		//               SELECT UOW_NM,
		//                      HUTC_MCM_MDU_NM,
		//                      HUTC_LOK_TMO_ITV,
		//                      HUTC_LOK_SGY_CD,
		//                      HUTC_SEC_IND,
		//                      HUTC_SEC_MDU_NM,
		//                      HUTC_DTA_PVC_IND,
		//                      HUTC_AUDIT_IND
		//               INTO   :HUTC-UOW-NM,
		//                      :HUTC-MCM-MDU-NM,
		//                      :HUTC-LOK-TMO-ITV,
		//                      :HUTC-LOK-SGY-CD,
		//                      :HUTC-SEC-IND,
		//                      :HUTC-SEC-MDU-NM
		//                      :HUTC-SEC-MDU-NM-NI,
		//                      :HUTC-DTA-PVC-IND,
		//                      :HUTC-AUDIT-IND
		//               FROM HAL_UOW_TRANSACT_V
		//               WHERE UOW_NM            =: HUTC-UOW-NM
		//           END-EXEC.
		halUowTransactVDao.selectByHutcUowNm1(ws.getDclhalUowTransact().getUowNm(), ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 2140-READ-UOW-TRANSACTION-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: MOVE 'SELECT OF UOW TRANSACTION INFO FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF UOW TRANSACTION INFO FAILED.");
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: STRING 'MTS-UOW-NAME = '   HUTC-UOW-NM       ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "MTS-UOW-NAME = ",
					ws.getDclhalUowTransact().getUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_UOW_TRANSACT_V'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_TRANSACT_V");
			// COB_CODE: MOVE '2140-READ-UOW-TRANSACTION'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2140-READ-UOW-TRANSACTION");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2140-READ-UOW-TRANSACTION");
			// COB_CODE: IF ERD-SQL-RED LESS THAN ZERO
			//               MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getErdSqlRed() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE ERD-SQL-RED    TO EFAL-DB2-ERR-SQLCODE
			//                                  CSC-SQLCODE-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getErdSqlRed(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(sqlca.getErdSqlRed());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2140-READ-UOW-TRANSACTION-X
			return;
		}
		// COB_CODE: MOVE HUTC-UOW-NM            TO UTCS-UOW-NAME.
		ws.getUtcsUowTransConfigInfo().setUowName(ws.getDclhalUowTransact().getUowNm());
		// COB_CODE: MOVE HUTC-MCM-MDU-NM        TO UTCS-UOW-MODULE.
		ws.getUtcsUowTransConfigInfo().setUowModule(ws.getDclhalUowTransact().getMcmMduNm());
		// COB_CODE: MOVE HUTC-LOK-TMO-ITV       TO UTCS-FOLDER-LOKS-DEL-TIME.
		ws.getUtcsUowTransConfigInfo().setFolderLoksDelTime(ws.getDclhalUowTransact().getLokTmoItv());
		// COB_CODE: MOVE HUTC-LOK-SGY-CD        TO UTCS-UOW-LOCK-ACTION-CD.
		ws.getUtcsUowTransConfigInfo().getUowLockActionCd().setUbocUowLockStrategyCd(ws.getDclhalUowTransact().getLokSgyCd());
		// COB_CODE: MOVE HUTC-SEC-IND           TO UTCS-CN-LVL-SEC-IND.
		ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().setCnLvlSecInd(ws.getDclhalUowTransact().getSecInd());
		// COB_CODE: IF  HUTC-SEC-MDU-NM-NI    = -1
		//               MOVE SPACES             TO UTCS-CONTROL-LVL-SEC-MODULE
		//           ELSE
		//               MOVE HUTC-SEC-MDU-NM    TO UTCS-CONTROL-LVL-SEC-MODULE
		//           END-IF.
		if (ws.getDclhalUowTransact().getSecMduNmNi() == -1) {
			// COB_CODE: MOVE COM-COL-IS-NULL    TO UTCS-CONTROL-LVL-SEC-MOD-NI
			ws.getUtcsUowTransConfigInfo().setControlLvlSecModNi(ws.getHallcom().getColIsNull());
			// COB_CODE: MOVE SPACES             TO UTCS-CONTROL-LVL-SEC-MODULE
			ws.getUtcsUowTransConfigInfo().setControlLvlSecModule("");
		} else {
			// COB_CODE: MOVE COM-COL-IS-NOT-NULL
			//                                   TO UTCS-CONTROL-LVL-SEC-MOD-NI
			ws.getUtcsUowTransConfigInfo().setControlLvlSecModNi(ws.getHallcom().getColIsNotNull());
			// COB_CODE: MOVE HUTC-SEC-MDU-NM    TO UTCS-CONTROL-LVL-SEC-MODULE
			ws.getUtcsUowTransConfigInfo().setControlLvlSecModule(ws.getDclhalUowTransact().getSecMduNm());
		}
		// COB_CODE: MOVE HUTC-DTA-PVC-IND       TO UTCS-DATA-PRIVACY-IND.
		ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().setDataPrivacyInd(ws.getDclhalUowTransact().getDtaPvcInd());
		// COB_CODE: MOVE HUTC-AUDIT-IND         TO UTCS-AUDIT-IND.
		ws.getUtcsUowTransConfigInfo().getAuditInd().setAuditInd(ws.getDclhalUowTransact().getAuditInd());
	}

	/**Original name: 2150-VALIDATE-UOW-TRANSACT_FIRST_SENTENCES<br>
	 * <pre>*************************************************************
	 *  VERIFY THAT ALL REQUIRED DATA IS PRESENT IN                *
	 *  HAL_UOW_TRANSACT_V FOR THE UNIT OF WORK.                   *
	 * *************************************************************</pre>*/
	private void validateUowTransact() {
		// COB_CODE: IF (UTCS-UOW-MODULE EQUAL SPACES OR LOW-VALUES)
		//             OR (UTCS-UOW-LOCK-ACTION-CD EQUAL SPACES OR LOW-VALUES)
		//             OR (UTCS-DATA-PRIVACY-IND EQUAL SPACES OR LOW-VALUES)
		//             OR (UTCS-CN-LVL-SEC-IND EQUAL SPACES OR LOW-VALUES)
		//             OR (UTCS-CHK-CONTROL-LVL-SEC
		//                   AND UTCS-CONTROL-LVL-SEC-MODULE
		//                     EQUAL SPACES OR LOW-VALUES)
		//                                            OF WS-HUB-DATA
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getUtcsUowTransConfigInfo().getUowModule())
				|| Characters.EQ_LOW.test(ws.getUtcsUowTransConfigInfo().getUowModule(), UtcsUowTransConfigInfo.Len.UOW_MODULE)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getUowLockActionCd().getUbocUowLockStrategyCd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getUowLockActionCd().getUbocUowLockStrategyCd(), Types.LOW_CHAR_VAL)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().getDataPrivacyInd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().getDataPrivacyInd(), Types.LOW_CHAR_VAL)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().getCnLvlSecInd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().getCnLvlSecInd(), Types.LOW_CHAR_VAL)
				|| ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().isChkControlLvlSec()
						&& Characters.EQ_SPACE.test(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule())
				|| Characters.EQ_LOW.test(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule(),
						UtcsUowTransConfigInfo.Len.CONTROL_LVL_SEC_MODULE)) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: MOVE '2150-VALIDATE-UOW-TRANSACT'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2150-VALIDATE-UOW-TRANSACT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2150-VALIDATE-UOW-TRANSACT");
		}
		// COB_CODE: IF UTCS-UOW-MODULE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2150-VALIDATE-UOW-TRANSACT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getUtcsUowTransConfigInfo().getUowModule())
				|| Characters.EQ_LOW.test(ws.getUtcsUowTransConfigInfo().getUowModule(), UtcsUowTransConfigInfo.Len.UOW_MODULE)) {
			// COB_CODE: SET BUSP-UOW-MODULE-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspUowModuleBlank();
			// COB_CODE: MOVE 'UOW MODULE NAME BLANK IN UTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UOW MODULE NAME BLANK IN UTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2150-VALIDATE-UOW-TRANSACT-X
			return;
		}
		// COB_CODE: IF UTCS-UOW-LOCK-ACTION-CD EQUAL SPACES OR LOW-VALUES
		//               GO TO 2150-VALIDATE-UOW-TRANSACT-X
		//           END-IF.
		if (Conditions.eq(ws.getUtcsUowTransConfigInfo().getUowLockActionCd().getUbocUowLockStrategyCd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getUowLockActionCd().getUbocUowLockStrategyCd(), Types.LOW_CHAR_VAL)) {
			// COB_CODE: SET BUSP-LOCK-ACTION-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspLockActionBlank();
			// COB_CODE: MOVE 'UOW LOCK ACTION CODE BLANK IN UTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UOW LOCK ACTION CODE BLANK IN UTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2150-VALIDATE-UOW-TRANSACT-X
			return;
		}
		// COB_CODE: IF UTCS-DATA-PRIVACY-IND EQUAL SPACES OR LOW-VALUES
		//               GO TO 2150-VALIDATE-UOW-TRANSACT-X
		//           END-IF.
		if (Conditions.eq(ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().getDataPrivacyInd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().getDataPrivacyInd(), Types.LOW_CHAR_VAL)) {
			// COB_CODE: SET BUSP-DATA-PRIV-IND-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspDataPrivIndBlank();
			// COB_CODE: MOVE 'UOW DATA PRIVACY INDICATOR BLANK IN UTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UOW DATA PRIVACY INDICATOR BLANK IN UTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2150-VALIDATE-UOW-TRANSACT-X
			return;
		}
		// COB_CODE: IF UTCS-CN-LVL-SEC-IND EQUAL SPACES OR LOW-VALUES
		//               GO TO 2150-VALIDATE-UOW-TRANSACT-X
		//           END-IF.
		if (Conditions.eq(ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().getCnLvlSecInd(), Types.SPACE_CHAR)
				|| Conditions.eq(ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().getCnLvlSecInd(), Types.LOW_CHAR_VAL)) {
			// COB_CODE: SET BUSP-CN-LVL-SEC-IND-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspCnLvlSecIndBlank();
			// COB_CODE: MOVE 'UOW CONTROL LEVEL SECURITY IND BLANK IN UTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UOW CONTROL LEVEL SECURITY IND BLANK IN UTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2150-VALIDATE-UOW-TRANSACT-X
			return;
		}
		// COB_CODE: IF UTCS-CHK-CONTROL-LVL-SEC
		//             AND UTCS-CONTROL-LVL-SEC-MODULE EQUAL SPACES OR LOW-VALUES
		//               GO TO 2150-VALIDATE-UOW-TRANSACT-X
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getCnLvlSecInd().isChkControlLvlSec()
				&& Characters.EQ_SPACE.test(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule())
				|| Characters.EQ_LOW.test(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule(),
						UtcsUowTransConfigInfo.Len.CONTROL_LVL_SEC_MODULE)) {
			// COB_CODE: SET BUSP-CONT-LVL-SEC-MODULE-BLANK
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspContLvlSecModuleBlank();
			// COB_CODE: MOVE 'UOW CONTROL LEVEL SECURITY MODULE BLANK IN UTCS.'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UOW CONTROL LEVEL SECURITY MODULE BLANK IN UTCS.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2150-VALIDATE-UOW-TRANSACT-X
			return;
		}
	}

	/**Original name: 2160-BUILD-UBOC-PARMS_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  SET UP THE COMMON VARIABLES USED IN HALLUBOC.          *
	 * *********************************************************</pre>*/
	private void buildUbocParms() {
		// COB_CODE: SET UBOC-ERR-LOGGING-OK     TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setUbocErrLoggingOk();
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK      TO UBOC-UOW-NAME.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowName(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: MOVE MDRV-MSG-ID            TO UBOC-MSG-ID.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMsgId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE MDRV-SESSION-ID        TO UBOC-SESSION-ID.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocSessionId(ws.getHallmdrv().getDriverInfo().getSessionId());
		// COB_CODE: MOVE MDRV-AUTH-USERID       TO UBOC-AUTH-USERID.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocAuthUserid(ws.getHallmdrv().getDriverInfo().getAuthUserid());
		// COB_CODE: MOVE MDRV-TERMINAL-ID       TO UBOC-TERMINAL-ID.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocTerminalId(ws.getHallmdrv().getDriverInfo().getTerminalId());
		// COB_CODE: SET NO-PASS-THRU-ACTION OF UBOC-COMM-INFO
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setNoPassThruAction();
		// COB_CODE: MOVE ZERO                   TO UBOC-NBR-HDR-ROWS
		//                                          UBOC-NBR-DATA-ROWS
		//                                          UBOC-NBR-WARNINGS
		//                                          UBOC-NBR-NONLOG-BL-ERRS.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrHdrRows(0);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrDataRows(0);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrWarnings(0);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrNonlogBlErrs(0);
		// COB_CODE: MOVE MTCS-STORE-TYPE-CD     TO UBOC-STORE-TYPE-CD.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocStoreTypeCd().setUbocStoreTypeCd(ws.getMtcsMsgTransportInfo().getStoreTypeCd());
		// COB_CODE: MOVE MTCS-MDR-REQ-STORE     TO UBOC-MDR-REQ-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMdrReqStore(ws.getMtcsMsgTransportInfo().getMdrReqStore());
		// COB_CODE: MOVE MTCS-MDR-RSP-STORE     TO UBOC-MDR-RSP-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMdrRspStore(ws.getMtcsMsgTransportInfo().getMdrRspStore());
		// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE TO UBOC-UOW-REQ-MSG-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqMsgStore(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
		// COB_CODE: MOVE MTCS-UOW-REQ-SWITCHES-STORE
		//                                       TO UBOC-UOW-REQ-SWITCHES-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqSwitchesStore(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore());
		// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
		//                                       TO UBOC-UOW-RESP-HEADER-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespHeaderStore(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
		// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
		//                                       TO UBOC-UOW-RESP-DATA-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespDataStore(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
		// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
		//                                       TO UBOC-UOW-RESP-WARNINGS-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespWarningsStore(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
		// COB_CODE: MOVE MTCS-UOW-KEY-REPLACE-STORE
		//                                       TO UBOC-UOW-KEY-REPLACE-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowKeyReplaceStore(ws.getMtcsMsgTransportInfo().getUowKeyReplaceStore());
		// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
		//                                      TO UBOC-UOW-RESP-NL-BL-ERRS-STORE.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespNlBlErrsStore(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
		// COB_CODE: PERFORM 2161-BUILD-UOW-SWITCH-Q-NAME.
		buildUowSwitchQName();
		// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
		//               GO TO 2160-BUILD-UBOC-PARMS-X
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: GO TO 2160-BUILD-UBOC-PARMS-X
			return;
		}
		// COB_CODE: IF UTCS-APPLY-DATA-PRIVACY
		//                                       TO TRUE
		//           ELSE
		//                                       TO TRUE
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getDataPrivacyInd().isApplyDataPrivacy()) {
			// COB_CODE: SET UBOC-APPLY-DATA-PRIV
			//                                   TO TRUE
			ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setUbocApplyDataPriv();
		} else {
			// COB_CODE: SET UBOC-NO-DATA-PRIVACY
			//                                   TO TRUE
			ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setUbocNoDataPrivacy();
		}
		// COB_CODE: MOVE UTCS-UOW-LOCK-ACTION-CD
		//                                       TO UBOC-UOW-LOCK-STRATEGY-CD.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockStrategyCd()
				.setUbocUowLockStrategyCd(ws.getUtcsUowTransConfigInfo().getUowLockActionCd().getUbocUowLockStrategyCd());
		// COB_CODE: MOVE UTCS-FOLDER-LOKS-DEL-TIME
		//                                      TO UBOC-UOW-LOCK-TIMEOUT-INTERVAL.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowLockTimeoutInterval(ws.getUtcsUowTransConfigInfo().getFolderLoksDelTime());
		// COB_CODE: IF UTCS-NO-LOCK-ACTION
		//               MOVE SPACES             TO UBOC-UOW-LOCK-PROC-TSQ
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getUowLockActionCd().isNoLockAction()) {
			// COB_CODE: MOVE SPACES             TO UBOC-UOW-LOCK-PROC-TSQ
			ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowLockProcTsq("");
		} else {
			// COB_CODE: PERFORM 2162-BUILD-UOW-LOCK-TSQ-NAME
			buildUowLockTsqName();
			// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
			//               GO TO 2160-BUILD-UBOC-PARMS-X
			//           END-IF
			if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
				// COB_CODE: GO TO 2160-BUILD-UBOC-PARMS-X
				return;
			}
		}
		// COB_CODE: IF UTCS-APPLY-AUDITS
		//               SET UBOC-APPLY-AUDITS   TO TRUE
		//           ELSE
		//               SET UBOC-NO-AUDITS      TO TRUE
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getAuditInd().isApplyAudits()) {
			// COB_CODE: SET UBOC-APPLY-AUDITS   TO TRUE
			ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setUbocApplyAudits();
		} else {
			// COB_CODE: SET UBOC-NO-AUDITS      TO TRUE
			ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setUbocNoAudits();
		}
		// COB_CODE: SET UBOC-CLEAR-UOW-STORAGE  TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocKeepClearUowStorageSw().setUbocClearUowStorage();
		//* CLEAR OUT THE APPLICATION TIMESTAMP FIELD.  THIS FIELD IS SET
		//* AND USED BY THE APPLICATION BDOS.
		// COB_CODE: SET UBOC-NO-APP-TS-SET      TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocApplicationTsSw().setUbocNoAppTsSet();
		// COB_CODE: MOVE SPACES                 TO UBOC-APPLICATION-TS.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocApplicationTs("");
	}

	/**Original name: 2161-BUILD-UOW-SWITCH-Q-NAME_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  BUILD THE TSQ NAME USED FOR UOW SWITCH PROCESSING.
	 * *****************************************************************</pre>*/
	private void buildUowSwitchQName() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE 2                      TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(((short) 2));
		// COB_CODE: SET UIDG-RANDOM-GLOBAL-ID   TO TRUE.
		ws.getHalluidg().getUidgIdType().setUidgRandomGlobalId();
		// COB_CODE: MOVE LENGTH OF WS-HALOUIDG-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) Ts020000Data.Len.WS_HALOUIDG_LINKAGE));
		// COB_CODE: MOVE WS-HALOUIDG-LINKAGE    TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getWsHalouidgLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  ('HALOUIDG')
		//                COMMAREA (UBOC-RECORD)
		//                LENGTH   (LENGTH OF UBOC-RECORD)
		//                RESP     (WA-RESPONSE-CODE)
		//                RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUIDG",
				new Halouidg());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 2161-BUILD-UOW-SWITCH-Q-NAME-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUIDG'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUIDG");
			// COB_CODE: MOVE '2161-BUILD-UOW-SWITCH-Q-NAME'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2161-BUILD-UOW-SWITCH-Q-NAME");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2161-BUILD-UOW-SWITCH-Q-NAME");
			// COB_CODE: MOVE 'ERROR LINKING TO HALOUIDG'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR LINKING TO HALOUIDG");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: STRING 'ID TYPE: '
			//                UIDG-ID-TYPE
			//                DELIMITED BY SIZE
			//                INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "ID TYPE: ",
					ws.getHalluidg().getUidgIdType().getUidgIdTypeFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2161-BUILD-UOW-SWITCH-Q-NAME-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2161-BUILD-UOW-SWITCH-Q-NAME-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: GO TO 2161-BUILD-UOW-SWITCH-Q-NAME-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO WS-HALOUIDG-LINKAGE.
		ws.setWsHalouidgLinkageFormatted(ws.getWsHubData().getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: STRING 'HAL'
		//                   UIDG-GENERATED-ID
		//                   DELIMITED BY SIZE
		//                   INTO UBOC-UOW-REQ-SWITCHES-TSQ
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(UbocCommInfo.Len.UBOC_UOW_REQ_SWITCHES_TSQ, "HAL",
				ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted());
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqSwitchesTsq(
				concatUtil.replaceInString(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsqFormatted()));
		// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-TSQ
		//                                       TO CSC-REQ-SWITCHES-TSQ
		//                                            OF WS-HUB-DATA.
		ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms()
				.setReqSwitchesTsq(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsq());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE 1                      TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) 1));
	}

	/**Original name: 2162-BUILD-UOW-LOCK-TSQ-NAME_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  BUILD THE TSQ NAME USED FOR UOW TRANSACTIONAL LOCKING.
	 * *****************************************************************</pre>*/
	private void buildUowLockTsqName() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE 1                      TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(((short) 1));
		// COB_CODE: SET UIDG-RANDOM-GLOBAL-ID   TO TRUE.
		ws.getHalluidg().getUidgIdType().setUidgRandomGlobalId();
		// COB_CODE: MOVE LENGTH OF WS-HALOUIDG-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) Ts020000Data.Len.WS_HALOUIDG_LINKAGE));
		// COB_CODE: MOVE WS-HALOUIDG-LINKAGE    TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getWsHalouidgLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOUIDG')
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUIDG",
				new Halouidg());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 2162-BUILD-UOW-LOCK-TSQ-NAME-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUIDG'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUIDG");
			// COB_CODE: MOVE '2162-BUILD-UOW-LOCK-TSQ-NAME'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2162-BUILD-UOW-LOCK-TSQ-NAME");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2162-BUILD-UOW-LOCK-TSQ-NAME");
			// COB_CODE: MOVE 'ERROR LINKING TO HALOUIDG'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR LINKING TO HALOUIDG");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: STRING 'ID TYPE: '
			//               UIDG-ID-TYPE
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "ID TYPE: ",
					ws.getHalluidg().getUidgIdType().getUidgIdTypeFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2162-BUILD-UOW-LOCK-TSQ-NAME-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2162-BUILD-UOW-LOCK-TSQ-NAME-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: GO TO 2162-BUILD-UOW-LOCK-TSQ-NAME-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO WS-HALOUIDG-LINKAGE.
		ws.setWsHalouidgLinkageFormatted(ws.getWsHubData().getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: STRING 'HAL'
		//                   UIDG-GENERATED-ID
		//                   DELIMITED BY SIZE
		//                   INTO UBOC-UOW-LOCK-PROC-TSQ
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(UbocCommInfo.Len.UBOC_UOW_LOCK_PROC_TSQ, "HAL",
				ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted());
		ws.getWsHubData().getUbocRecord().getCommInfo()
				.setUbocUowLockProcTsq(concatUtil.replaceInString(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted()));
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE 1                      TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) 1));
	}

	/**Original name: 2170-LINK-TO-SECURITY_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  VERIFY THAT THE PASSED IN USER ID IS A VALID SYSTEM     *
	 *  USER.                                                   *
	 *  VERIFY THAT THE PASSED IN USER ID HAS ACCESS TO THE     *
	 *  REQUESTED UNIT OF WORK.                                 *
	 * **********************************************************
	 * * BUILD USEC INFO AREA AND MOVE TO APP DATA BUFFER</pre>*/
	private void linkToSecurity() {
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE SPACES                 TO WS-USEC-INFO.
		ws.initWsUsecInfoSpaces();
		// COB_CODE: SET USEC-RESPONSE-IN-LINKAGE
		//                                       TO TRUE.
		ws.getUsecInputOuputData().getResponseStorageSw().setLinkage();
		// COB_CODE: SET USEC-SEC-OK             TO TRUE.
		ws.getUsecInputOuputData().getReturnCode().setSecOk();
		// COB_CODE: MOVE UBOC-UOW-NAME          TO USEC-UOW-NAME.
		ws.getUsecInputOuputData().setUowName(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID       TO USEC-USER-ID.
		ws.getUsecInputOuputData().setUserId(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: MOVE LENGTH OF WS-USEC-INFO TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) Ts020000Data.Len.WS_USEC_INFO));
		// COB_CODE: SET SECURITY-RETRIEVAL-REQUEST OF UBOC-COMM-INFO
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setSecurityRetrievalRequest();
		// COB_CODE: SET USEC-GET-USER-CLIENT-ID TO TRUE.
		ws.getUsecInputOuputData().getAction().setUserClientId();
		// COB_CODE: MOVE WS-USEC-INFO           TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getWsUsecInfoFormatted());
		// COB_CODE: PERFORM 2171-GET-USER-CLIENT-ID.
		getUserClientId();
		// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
		//               GO TO 2170-LINK-TO-SECURITY-X
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
			// COB_CODE: GO TO 2170-LINK-TO-SECURITY-X
			return;
		}
		// COB_CODE: SET USEC-GET-USER-UOW-AUTH-ASSOC
		//                                       TO TRUE.
		ws.getUsecInputOuputData().getAction().setUserUowAuthAssoc();
		// COB_CODE: MOVE WS-USEC-INFO           TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getWsUsecInfoFormatted());
		// COB_CODE: PERFORM 2172-GET-UOW-AUTH-ASSOC.
		getUowAuthAssoc();
	}

	/**Original name: 2171-GET-USER-CLIENT-ID_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  CALL THE SECURITY RETRIEVAL MODULE TO GET THE LOGGED ON *
	 *  USER'S CLIENT ID.                                       *
	 * **********************************************************</pre>*/
	private void getUserClientId() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 2179-LINK-TO-SECURITY.
		linkToSecurity1();
		// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
		//             OR
		//              MDRV-PERFORM-ROLLBACK
		//               GO TO 2171-GET-USER-CLIENT-ID-X
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()
				|| ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().isRollback()) {
			// COB_CODE: GO TO 2171-GET-USER-CLIENT-ID-X
			return;
		}
		// COB_CODE: IF USEC-USER-CLTID-NOT-FOUND
		//               GO TO 2171-GET-USER-CLIENT-ID-X
		//           END-IF.
		if (ws.getUsecInputOuputData().getReturnCode().isUserCltidNotFound()) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-SECURITY-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecurityFailed();
			// COB_CODE: SET SECU-SEC-SYS-ACCESS-DENIED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setSecuSecSysAccessDenied();
			// COB_CODE: STRING 'UBOC-UOW-NAME = ' UBOC-UOW-NAME ';'
			//                  'UBOC-AUTH-USERID = ' UBOC-AUTH-USERID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME = ", ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"UBOC-AUTH-USERID = ", ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuthUseridFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '2171-GET-USER-CLIENT-ID'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2171-GET-USER-CLIENT-ID");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2171-GET-USER-CLIENT-ID");
			// COB_CODE: MOVE SPACES             TO EFAL-ERR-COMMENT
			//skipped translation for moving SPACES to EFAL-ERR-COMMENT; considered in STRING statement translation below
			// COB_CODE: STRING
			//               'USER NOT FOUND ON IAP SECURITY TABLES. '
			//               'IAP SECURITY RETURN: '
			//                USEC-SMCA-PM-ERROR
			//                DELIMITED BY SIZE
			//                INTO EFAL-ERR-COMMENT
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(new StringBuffer(256).append("USER NOT FOUND ON IAP SECURITY TABLES. ")
					.append("IAP SECURITY RETURN: ").append(ws.getUsecInputOuputData().getSmcaPmError()).toString());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2171-GET-USER-CLIENT-ID-X
			return;
		}
		// COB_CODE: MOVE USEC-CLIENT-ID         TO UBOC-AUTH-USER-CLIENTID.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocAuthUserClientid(ws.getUsecInputOuputData().getClientId());
		// COB_CODE: SET NO-PASS-THRU-ACTION OF UBOC-COMM-INFO
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setNoPassThruAction();
	}

	/**Original name: 2172-GET-UOW-AUTH-ASSOC_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  CALL THE SECURITY RETRIEVAL MODULE TO GET THE LOGGED ON *
	 *  USER'S UOW AUTHORITY LEVEL AND UOW ASSOCIATION TYPE.    *
	 * **********************************************************</pre>*/
	private void getUowAuthAssoc() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 2179-LINK-TO-SECURITY.
		linkToSecurity1();
		// COB_CODE: IF MDRV-UOW-LOGGABLE-ERRS
		//             OR
		//              MDRV-PERFORM-ROLLBACK
		//               GO TO 2172-GET-UOW-AUTH-ASSOC-X
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()
				|| ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().isRollback()) {
			// COB_CODE: GO TO 2172-GET-UOW-AUTH-ASSOC-X
			return;
		}
		// COB_CODE: IF USEC-USER-UOW-AUTH-ASS-NOT-FND
		//               GO TO 2172-GET-UOW-AUTH-ASSOC-X
		//           END-IF.
		if (ws.getUsecInputOuputData().getReturnCode().isUserUowAuthAssNotFnd()) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-SECURITY-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecurityFailed();
			// COB_CODE: SET SECU-SEC-UOW-ACCESS-DENIED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setSecuSecUowAccessDenied();
			// COB_CODE: STRING 'UBOC-UOW-NAME = '  UBOC-UOW-NAME ';'
			//                  'UBOC-AUTH-USERID = '  UBOC-AUTH-USERID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME = ", ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"UBOC-AUTH-USERID = ", ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuthUseridFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '2172-GET-UOW-AUTH-ASSOC'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2172-GET-UOW-AUTH-ASSOC");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2172-GET-UOW-AUTH-ASSOC");
			// COB_CODE: MOVE SPACES             TO EFAL-ERR-COMMENT
			//skipped translation for moving SPACES to EFAL-ERR-COMMENT; considered in STRING statement translation below
			// COB_CODE: STRING
			//               'AUTHORITY AND ASSOCIATION TYPE NOT FOUND FOR'
			//               'USER AND UOW.'
			//                DELIMITED BY SIZE
			//                INTO EFAL-ERR-COMMENT
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("AUTHORITY AND ASSOCIATION TYPE NOT FOUND FOR" + "USER AND UOW.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2172-GET-UOW-AUTH-ASSOC-X
			return;
		}
		// COB_CODE: IF USEC-USER-MISMATCH-FND
		//               GO TO 2172-GET-UOW-AUTH-ASSOC-X
		//           END-IF.
		if (ws.getUsecInputOuputData().getReturnCode().isUserMismatchFnd()) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-SECURITY-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecurityFailed();
			// COB_CODE: SET SECU-SEC-UOW-ACCESS-DENIED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setSecuSecUowAccessDenied();
			// COB_CODE: STRING 'UBOC-UOW-NAME = '  UBOC-UOW-NAME ';'
			//                  'EXECUTING USER ID = '  USEC-USER-ID ';'
			//                  'PASSED USER ID = '  UBOC-AUTH-USERID ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UBOC-UOW-NAME = ", ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowNameFormatted(), ";",
							"EXECUTING USER ID = ", ws.getUsecInputOuputData().getUserIdFormatted(), ";", "PASSED USER ID = ",
							ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuthUseridFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '2172-GET-UOW-AUTH-ASSOC'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2172-GET-UOW-AUTH-ASSOC");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2172-GET-UOW-AUTH-ASSOC");
			// COB_CODE: MOVE SPACES             TO EFAL-ERR-COMMENT
			//skipped translation for moving SPACES to EFAL-ERR-COMMENT; considered in STRING statement translation below
			// COB_CODE: STRING
			//               'AUTHORITY AND ASSOCIATION TYPE NOT FOUND FOR'
			//               ' EXECUTING USER AND UOW.'
			//                DELIMITED BY SIZE
			//                INTO EFAL-ERR-COMMENT
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("AUTHORITY AND ASSOCIATION TYPE NOT FOUND FOR" + " EXECUTING USER AND UOW.");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2172-GET-UOW-AUTH-ASSOC-X
			return;
		}
		// COB_CODE: MOVE USEC-UOW-AUTHORITY-LEVEL
		//                                       TO UBOC-SEC-AUT-NBR.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getSecAutNbr()
				.setUbocSecAutNbrFormatted(ws.getUsecInputOuputData().getUowAuthorityLevel().getUowAuthorityLevelFormatted());
		// COB_CODE: MOVE USEC-UOW-USER-ASSOCIATION-TYPE
		//                                       TO UBOC-SEC-ASSOCIATION-TYPE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo()
				.setSecAssociationType(ws.getUsecInputOuputData().getUowUserAssociationType());
		// COB_CODE: MOVE USEC-GROUP-ID          TO UBOC-SEC-GROUP-NAME.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocSecGroupName(ws.getUsecInputOuputData().getGroupId());
		// COB_CODE: SET NO-PASS-THRU-ACTION OF UBOC-COMM-INFO
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setNoPassThruAction();
	}

	/**Original name: 2179-LINK-TO-SECURITY_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  LINK TO MAIN SECURITY MODULE TO VERIFY THAT THE LOGGED  *
	 *  ON USER HAS ACCESS TO THE REQUESTED UOW, TO RETRIEVE THE*
	 *  LOGGED ON USER'S ACCESS LEVEL, AND GET THE LOGGED ON    *
	 *  USER'S CLIENT ID.                                       *
	 * **********************************************************</pre>*/
	private void linkToSecurity1() {
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (UTCS-CONTROL-LVL-SEC-MODULE)
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                                       TO WS-USEC-INFO
		//               WHEN OTHER
		//                   GO TO 2179-LINK-TO-SECURITY-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: MOVE UBOC-APP-DATA-BUFFER(1:LENGTH OF WS-USEC-INFO)
			//                               TO WS-USEC-INFO
			ws.setWsUsecInfoFormatted(
					ws.getWsHubData().getUbocRecord().getAppDataBufferFormatted().substring((1) - 1, Ts020000Data.Len.WS_USEC_INFO));
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE UTCS-CONTROL-LVL-SEC-MODULE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getUtcsUowTransConfigInfo().getControlLvlSecModule());
			// COB_CODE: MOVE '2179-LINK-TO-SECURITY'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2179-LINK-TO-SECURITY");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("2179-LINK-TO-SECURITY");
			// COB_CODE: MOVE 'BAD EIBRESP WHEN LINKING TO SECURITY MODULE.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP WHEN LINKING TO SECURITY MODULE.");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 2179-LINK-TO-SECURITY-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2179-LINK-TO-SECURITY-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: GO TO 2179-LINK-TO-SECURITY-X
			return;
		}
	}

	/**Original name: 3000-PROCESS-REQUEST-MODULE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  EXECUTE THE REQUEST MODULE THAT WAS PASSED IN THE COMMON      *
	 *  PARMS.  THE REQUEST MODULE WIL SET UP THE UMTS AND TSQ'S      *
	 *  FOR THE UOW PROCESSING.                                       *
	 * ****************************************************************
	 * *   IT IS NOT A REQUIREMENT THAT THERE BE A REQUEST MODULE.
	 * *   A UOW MAY EXIST THAT DOES NOT REQUIRE INPUT.</pre>*/
	private void processRequestModule() {
		// COB_CODE: IF CSC-REQUEST-MODULE OF WS-HUB-DATA
		//                                              EQUAL SPACES OR LOW-VALUES
		//               GO TO 3000-PROCESS-REQUEST-MODULE-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getRequestModule())
				|| Characters.EQ_LOW.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getCscRequestModuleFormatted())) {
			// COB_CODE: MOVE ZERO               TO DSD-PM-REQHUB-ELAPSED
			dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().setPmReqhubElapsed(new AfDecimal(0));
			// COB_CODE: GO TO 3000-PROCESS-REQUEST-MODULE-X
			return;
		}
		// COB_CODE: MOVE ZERO                   TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: ACCEPT TE-TRAN-START-TIME FROM TIME.
		ws.getTimeElapsedWork().setStartTimeFormatted(CalendarUtil.getTimeHHMMSSMM());
		// COB_CODE: COMPUTE TE-TRAN-START-TIME-HTH =
		//                   (TE-TRAN-HOURS-START * 60 * 60 * 100) +
		//                   (TE-TRAN-MINS-START * 60 * 100) +
		//                   (TE-TRAN-SECS-START * 100) +
		//                   TE-TRAN-HTHS-START.
		ws.getTimeElapsedWork().setStartTimeHth(
				Trunc.toInt(ws.getTimeElapsedWork().getHoursStart() * 60 * 60 * 100 + ws.getTimeElapsedWork().getMinsStart() * 60 * 100
						+ ws.getTimeElapsedWork().getSecsStart() * 100 + ws.getTimeElapsedWork().getHthsStart(), 8));
		// COB_CODE: MOVE COM-CICS-COM-LENGTH    TO WA-COMM-LENGTH.
		ws.getWorkArea().setWaCommLength(Trunc.toShort(ws.getHallcom().getCicsComLength(), 4));
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CSC-REQUEST-MODULE OF WS-HUB-DATA)
		//               COMMAREA   (WS-HUB-DATA)
		//               DATALENGTH (LENGTH OF WS-HUB-DATA)
		//               LENGTH     (WA-COMM-LENGTH)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData()).length(ws.getWorkArea().getWaCommLength())
				.link(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getRequestModule());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE MAINDRIVER LINK
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 3000-PROCESS-REQUEST-MODULE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '3000-PROCESS-REQUEST-MODULE'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3000-PROCESS-REQUEST-MODULE");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("3000-PROCESS-REQUEST-MODULE");
			// COB_CODE: MOVE CSC-REQUEST-MODULE OF WS-HUB-DATA
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrObjectName(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getRequestModule());
			// COB_CODE: MOVE 'LINK TO REQUEST MODULE FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO REQUEST MODULE FAILED.");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 3000-PROCESS-REQUEST-MODULE-X
			return;
		}
		//* IF HALT AND RETURN HAS BEEN SET TO TRUE,
		//* STOP PROCESSING BUSINESS OBJECTS
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-PROCESS-REQUEST-MODULE-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-PERFORM-ROLLBACK
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: GO TO 3000-PROCESS-REQUEST-MODULE-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-LOGGABLE-WARNINGS
		//                                       TO TRUE
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isUbocUowLoggableWarnings()) {
			// COB_CODE: SET DSD-WARNING-CODE    TO TRUE
			dfhcommarea.getDriverSpecificData().getErrorReturnCode().setWarningCode();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-WARNS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableWarns();
		}
		// COB_CODE: PERFORM 9902-PROCESS-ELAPSED-TIME.
		processElapsedTime();
		// COB_CODE: COMPUTE DSD-PM-REQHUB-ELAPSED =
		//               TE-TRANS-ELAPSED-TIME-AMT / 100.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms()
				.setPmReqhubElapsed(new AfDecimal(((((double) (ws.getTimeElapsedWork().getsElapsedTimeAmt()))) / 100), 10, 2));
	}

	/**Original name: 4000-PROCESS-UOW-PRIMARY-BOS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE PROCESS SEQUENCE TABLE, RETRIEVE THE PRIMARY BUSINESS*
	 *  OBJECTS FOR THE UOW, AND CALL EACH IN THE PROPER SEQUENCE.    *
	 * ****************************************************************</pre>*/
	private void processUowPrimaryBos() {
		// COB_CODE: ACCEPT TE-TRAN-START-TIME FROM TIME.
		ws.getTimeElapsedWork().setStartTimeFormatted(CalendarUtil.getTimeHHMMSSMM());
		// COB_CODE: COMPUTE TE-TRAN-START-TIME-HTH =
		//                   (TE-TRAN-HOURS-START * 60 * 60 * 100) +
		//                   (TE-TRAN-MINS-START * 60 * 100) +
		//                   (TE-TRAN-SECS-START * 100) +
		//                   TE-TRAN-HTHS-START.
		ws.getTimeElapsedWork().setStartTimeHth(
				Trunc.toInt(ws.getTimeElapsedWork().getHoursStart() * 60 * 60 * 100 + ws.getTimeElapsedWork().getMinsStart() * 60 * 100
						+ ws.getTimeElapsedWork().getSecsStart() * 100 + ws.getTimeElapsedWork().getHthsStart(), 8));
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK      TO HUPS-UOW-NM.
		ws.getDclhalUowPrcSeq().setHupsUowNm(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: MOVE ZERO                   TO WA-LAST-UOW-SEQ-NBR.
		ws.getWorkArea().setWaLastUowSeqNbr(((short) 0));
		// COB_CODE: SET SW-FIRST-PRIMARY-BO     TO TRUE.
		ws.getSwitches().getPrimaryBosFlag().setFirstPrimaryBo();
		// COB_CODE: PERFORM 4100-RETRIEVE-PRIMARY-BOS
		//               UNTIL SW-NO-MORE-PRIMARY-BOS
		//                 OR SW-NONLOG-BUS-ERRORS-FND
		//                 OR MDRV-UOW-LOGGABLE-ERRS
		//                 OR UBOC-HALT-AND-RETURN.
		while (!(ws.getSwitches().getPrimaryBosFlag().isNoMorePrimaryBos() || ws.getSwitches().isNonlogBusErrFlag()
				|| ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()
				|| ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
			retrievePrimaryBos();
		}
		// COB_CODE: PERFORM 9902-PROCESS-ELAPSED-TIME.
		processElapsedTime();
		// COB_CODE: COMPUTE DSD-PM-UOW-ELAPSED =
		//               TE-TRANS-ELAPSED-TIME-AMT / 100.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms()
				.setPmUowElapsed(new AfDecimal(((((double) (ws.getTimeElapsedWork().getsElapsedTimeAmt()))) / 100), 10, 2));
	}

	/**Original name: 4100-RETRIEVE-PRIMARY-BOS_FIRST_SENTENCES<br>
	 * <pre>***********************************************************
	 *  SELECT THE PRIMARY BUSINESS OBJECTS FOR THE CURRENT UOW. *
	 * ***********************************************************</pre>*/
	private void retrievePrimaryBos() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET NO-PASS-THRU-ACTION OF UBOC-COMM-INFO
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setNoPassThruAction();
		// COB_CODE: EXEC SQL
		//               SELECT  UOW_NM
		//                     , UOW_SEQ_NBR
		//                     , ROOT_BOBJ_NM
		//                 INTO :HUPS-UOW-NM
		//                     ,:UOW-SEQ-NBR
		//                     ,:ROOT-BOBJ-NM
		//                 FROM HAL_UOW_PRC_SEQ_V
		//                WHERE UOW_NM      = :HUPS-UOW-NM
		//                  AND UOW_SEQ_NBR = (SELECT MIN(UOW_SEQ_NBR)
		//                                       FROM HAL_UOW_PRC_SEQ_V
		//                                      WHERE UOW_NM
		//                                                 = :HUPS-UOW-NM
		//                                        AND UOW_SEQ_NBR
		//                                                 > :WA-LAST-UOW-SEQ-NBR)
		//           END-EXEC.
		halUowPrcSeqVDao.selectRec(ws.getDclhalUowPrcSeq().getHupsUowNm(), ws.getWorkArea().getWaLastUowSeqNbr(), ws.getDclhalUowPrcSeq());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   END-IF
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 4100-RETRIEVE-PRIMARY-BOS-X
		//               WHEN OTHER
		//                   GO TO 4100-RETRIEVE-PRIMARY-BOS-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: SET SW-NEXT-PRIMARY-BO
			//                               TO TRUE
			ws.getSwitches().getPrimaryBosFlag().setNextPrimaryBo();
			// COB_CODE: MOVE UOW-SEQ-NBR    TO WA-LAST-UOW-SEQ-NBR
			ws.getWorkArea().setWaLastUowSeqNbr(ws.getDclhalUowPrcSeq().getUowSeqNbr());
			// COB_CODE: MOVE DCLHAL-UOW-PRC-SEQ
			//                               TO WS-UOW-PROCESS-SEQ-INFO
			ws.setWsUowProcessSeqInfoBytes(ws.getDclhalUowPrcSeq().getDclhalUowPrcSeqBytes());
			// COB_CODE: MOVE ROOT-BOBJ-NM   TO WA-BUS-OBJ-NM
			ws.getWorkArea().setWaBusObjNm(ws.getDclhalUowPrcSeq().getRootBobjNm());
			// COB_CODE: PERFORM 4110-READ-BOBJ-MDU-XRF
			readBobjMduXrf();
			// COB_CODE: IF NOT MDRV-UOW-LOGGABLE-ERRS
			//              PERFORM 4120-LINK-TO-PRIMARY-BOS
			//           END-IF
			if (!ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().isLoggableErrs()) {
				// COB_CODE: PERFORM 4120-LINK-TO-PRIMARY-BOS
				linkToPrimaryBos();
			}
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: IF SW-FIRST-PRIMARY-BO
			//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
			//           ELSE
			//                               TO TRUE
			//           END-IF
			if (ws.getSwitches().getPrimaryBosFlag().isFirstPrimaryBo()) {
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-DB2-FAILED
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
				// COB_CODE: SET ETRA-DB2-FETCH-CSR
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
				// COB_CODE: STRING 'HUPS-UOW-NM      = '
				//                   HUPS-UOW-NM      ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
						ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V'
				//                           TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
				// COB_CODE: MOVE '4100-RETRIEVE-PRIMARY-BOS'
				//                           TO EFAL-ERR-PARAGRAPH
				//                              CSC-FAILED-PARAGRAPH
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4100-RETRIEVE-PRIMARY-BOS");
				ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4100-RETRIEVE-PRIMARY-BOS");
				// COB_CODE: MOVE 'FETCH ROW NFND FRST TME - PRIMARYBOCUR'
				//                           TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH ROW NFND FRST TME - PRIMARYBOCUR");
				// COB_CODE: IF ERD-SQL-RED LESS THAN ZERO
				//               MOVE '-'    TO EFAL-DB2-ERR-SQLCODE-SIGN
				//           ELSE
				//               MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN
				//           END-IF
				if (sqlca.getErdSqlRed() < 0) {
					// COB_CODE: MOVE '-'    TO EFAL-DB2-ERR-SQLCODE-SIGN
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
				} else {
					// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
				}
				// COB_CODE: MOVE ERD-SQL-RED
				//                           TO EFAL-DB2-ERR-SQLCODE
				//                              CSC-SQLCODE-DISPLAY
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getErdSqlRed(), 10));
				ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(sqlca.getErdSqlRed());
				// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
				logMainDrvrError();
			} else {
				// COB_CODE: SET SW-NO-MORE-PRIMARY-BOS
				//                           TO TRUE
				ws.getSwitches().getPrimaryBosFlag().setNoMorePrimaryBos();
			}
			// COB_CODE: GO TO 4100-RETRIEVE-PRIMARY-BOS-X
			return;

		default:// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: STRING 'HUPS-UOW-NM      = '   HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HUPS-UOW-NM      = ",
					ws.getDclhalUowPrcSeq().getHupsUowNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE 'HAL_UOW_PRC_SEQ_V'
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_PRC_SEQ_V");
			// COB_CODE: MOVE '4100-RETRIEVE-PRIMARY-BOS'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4100-RETRIEVE-PRIMARY-BOS");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4100-RETRIEVE-PRIMARY-BOS");
			// COB_CODE: MOVE 'FETCH CURSOR FAILED - PRIMARYBOCUR'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CURSOR FAILED - PRIMARYBOCUR");
			// COB_CODE: IF ERD-SQL-RED LESS THAN ZERO
			//               MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getErdSqlRed() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE ERD-SQL-RED    TO EFAL-DB2-ERR-SQLCODE
			//                                  CSC-SQLCODE-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getErdSqlRed(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(sqlca.getErdSqlRed());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 4100-RETRIEVE-PRIMARY-BOS-X
			return;
		}
	}

	/**Original name: 4110-READ-BOBJ-MDU-XRF_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  RETRIEVE THE BUS OBJ MODULE FOR THE BUSINESS OBJECT NAME
	 * *****************************************************************</pre>*/
	private void readBobjMduXrf() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WA-BUS-OBJ-NM          TO HBMX-BUS-OBJ-NM.
		ws.getDclhalBoMduXrfV().setBusObjNm(ws.getWorkArea().getWaBusObjNm());
		// COB_CODE: EXEC SQL
		//               SELECT HBMX_BOBJ_MDU_NM
		//                 INTO :HBMX-BOBJ-MDU-NM
		//                 FROM HAL_BO_MDU_XRF_V
		//                WHERE BUS_OBJ_NM = :HBMX-BUS-OBJ-NM
		//           END-EXEC.
		ws.getDclhalBoMduXrfV()
				.setBobjMduNm(halBoMduXrfVDao.selectByHbmxBusObjNm(ws.getDclhalBoMduXrfV().getBusObjNm(), ws.getDclhalBoMduXrfV().getBobjMduNm()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 4110-READ-BOBJ-MDU-XRF-X
		//               WHEN OTHER
		//                   GO TO 4110-READ-BOBJ-MDU-XRF-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQD-DATA-NOT-FOUND OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspReqdDataNotFound();
			// COB_CODE: MOVE '4110-READ-BOBJ-MDU-XRF'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4110-READ-BOBJ-MDU-XRF");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4110-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("EXPECTED ENTRY ON OBJ XREF TAB FOR BUS OBJ");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 4110-READ-BOBJ-MDU-XRF-X
			return;

		default:// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF'
			//                               TO EFAL-ERR-OBJECT-NAME
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF");
			// COB_CODE: MOVE '4110-READ-BOBJ-MDU-XRF'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4110-READ-BOBJ-MDU-XRF");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4110-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'SELECT FROM OBJ XREF TABLE FAILED'
			//                               TO EFAL-ERR-COMMENT
			//                                  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT FROM OBJ XREF TABLE FAILED");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: IF ERD-SQL-RED LESS THAN ZERO
			//               MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
			//           END-IF
			if (sqlca.getErdSqlRed() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-DB2-ERR-SQLCODE-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			}
			// COB_CODE: MOVE ERD-SQL-RED    TO EFAL-DB2-ERR-SQLCODE
			//                                  CSC-SQLCODE-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getErdSqlRed(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(sqlca.getErdSqlRed());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 4110-READ-BOBJ-MDU-XRF-X
			return;
		}
		//* CHECK MODULE NAME PRESENT IN OBJ XREF ROW RETURNED
		// COB_CODE: IF HBMX-BOBJ-MDU-NM = SPACES
		//               GO TO 4110-READ-BOBJ-MDU-XRF-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalBoMduXrfV().getBobjMduNm())) {
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED OF WS-ESTO-INFO
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-REQUIRED-FIELD-BLANK OF WS-ESTO-INFO
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspRequiredFieldBlank();
			// COB_CODE: MOVE '4110-READ-BOBJ-MDU-XRF'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4110-READ-BOBJ-MDU-XRF");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4110-READ-BOBJ-MDU-XRF");
			// COB_CODE: MOVE 'MODULE NAME ON OBJ XREF ROW IS BLANK'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("MODULE NAME ON OBJ XREF ROW IS BLANK");
			// COB_CODE: STRING 'HBMX-BUS-OBJ-NM=' HBMX-BUS-OBJ-NM ';'
			//                  'HBMX-BOBJ-MDU-NM=' HBMX-BOBJ-MDU-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HBMX-BUS-OBJ-NM=",
					ws.getDclhalBoMduXrfV().getBusObjNmFormatted(), ";", "HBMX-BOBJ-MDU-NM=", ws.getDclhalBoMduXrfV().getBobjMduNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 4110-READ-BOBJ-MDU-XRF-X
			return;
		}
		//* USE MODULE NAME FROM OBJ XREF ROW
		// COB_CODE: MOVE HBMX-BOBJ-MDU-NM       TO WA-BUS-OBJ-MDU.
		ws.getWorkArea().setWaBusObjMdu(ws.getDclhalBoMduXrfV().getBobjMduNm());
	}

	/**Original name: 4120-LINK-TO-PRIMARY-BOS_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  LINK TO EACH PRIMARY BUSINESS OBJECT.                   *
	 * **********************************************************</pre>*/
	private void linkToPrimaryBos() {
		// COB_CODE: MOVE ROOT-BOBJ-NM           TO UBOC-PRIMARY-BUS-OBJ.
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocPrimaryBusObj(ws.getDclhalUowPrcSeq().getRootBobjNm());
		// COB_CODE: MOVE SPACES                 TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer("");
		// COB_CODE: MOVE ZERO                   TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) 0));
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (WA-BUS-OBJ-MDU)
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD)
				.link(ws.getWorkArea().getWaBusObjMdu());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 4120-LINK-TO-PRIMARY-BOS-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '4120-LINK-TO-PRIMARY-BOS'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4120-LINK-TO-PRIMARY-BOS");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4120-LINK-TO-PRIMARY-BOS");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM LINK'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM LINK");
			// COB_CODE: MOVE WA-BUS-OBJ-MDU TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkArea().getWaBusObjMdu());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 4120-LINK-TO-PRIMARY-BOS-X
			return;
		}
		//*MAKE SURE UBOC LOGGABLE PROBLEMS FLAG NOT CORRUPTED BY UOW.
		//*MAKE SURE UBOC COUNTERS NOT CORRUPTED BY UOW.
		// COB_CODE: PERFORM 4121-UBOC-CORRUPTION-CHK.
		ubocCorruptionChk();
		//*IF ANYTHING CHECKED SO FAR HAS BEEN CORRUPTED, LOG AN ERROR.
		// COB_CODE: IF SW-UBOC-LINKAGE-CORRUPTED
		//               GO TO 4120-LINK-TO-PRIMARY-BOS-X
		//           END-IF.
		if (ws.getSwitches().isUbocLinkageCheckFlag()) {
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-UBOC-LINKAGE-CORRUPTED
			//                                   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspUbocLinkageCorrupted();
			// COB_CODE: MOVE SPACES             TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING WA-BAD-LOGPROB ';' WA-BAD-HDR ';' WA-BAD-DATA ';'
			//                  WA-BAD-NLBE ';'    WA-BAD-WRNS ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append(ws.getWorkArea().getWaBadLogprobFormatted()).append(";")
							.append(ws.getWorkArea().getWaBadHdrFormatted()).append(";").append(ws.getWorkArea().getWaBadDataFormatted()).append(";")
							.append(ws.getWorkArea().getWaBadNlbeFormatted()).append(";").append(ws.getWorkArea().getWaBadWrnsFormatted()).append(";")
							.toString());
			// COB_CODE: MOVE '4120-LINK-TO-PRIMARY-BOS'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			//                                        OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4120-LINK-TO-PRIMARY-BOS");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("4120-LINK-TO-PRIMARY-BOS");
			// COB_CODE: MOVE 'UBOC LINKAGE CORRUPTED BY UOW - CAPTURED BY MCM'
			//                                   TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC LINKAGE CORRUPTED BY UOW - CAPTURED BY MCM");
			// COB_CODE: MOVE WA-BUS-OBJ-MDU     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkArea().getWaBusObjMdu());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// FOR ERROR DETERMINATION PURPOSES, SAVE WHAT IS IN THE UMTS FOR
			// THIS UOW.
			// COB_CODE: SET UBOC-KEEP-UOW-STORAGE
			//                                   TO TRUE
			ws.getWsHubData().getUbocRecord().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepUowStorage();
			// COB_CODE: GO TO 4120-LINK-TO-PRIMARY-BOS-X
			return;
		}
		//* IF NON LOGGABLE BUSINESS ERRORS HAVE OCCURED THEN DO
		//* STOP PROCESSING BUSINESS OBJECTS
		// COB_CODE: IF UBOC-NBR-NONLOG-BL-ERRS > ZERO
		//               GO TO 4120-LINK-TO-PRIMARY-BOS-X
		//           END-IF.
		if (Characters.GT_ZERO.test(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted())) {
			// COB_CODE: SET MDRV-PERFORM-ROLLBACK
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
			// COB_CODE: SET SW-NONLOG-BUS-ERRORS-FND
			//                                   TO TRUE
			ws.getSwitches().setNonlogBusErrFlag(true);
			// COB_CODE: GO TO 4120-LINK-TO-PRIMARY-BOS-X
			return;
		}
		//* IF HALT AND RETURN HAS BEEN SET TO TRUE,
		//* STOP PROCESSING BUSINESS OBJECTS
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               PERFORM 9100-COPY-UBOC-ERROR-DET
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-PERFORM-ROLLBACK
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
		}
		// COB_CODE: IF NOT UBOC-UOW-OK
		//               END-IF
		//           END-IF.
		if (!ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()) {
			// COB_CODE: MOVE UBOC-OBJECT-LOGGABLE-PROBLEMS
			//                                   TO MDRV-MAINDRV-LOGGABLE-PROBLEMS
			ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setMaindrvLoggableProblems(ws.getWsHubData().getUbocRecord().getCommInfo()
					.getUbocErrorDetails().getUbocObjectLoggableProblems().getUbocObjectLoggableProblems());
			// COB_CODE: IF UBOC-UOW-LOGGABLE-WARNINGS
			//                                   TO TRUE
			//           ELSE
			//               GO TO 4120-LINK-TO-PRIMARY-BOS-X
			//           END-IF
			if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isUbocUowLoggableWarnings()) {
				// COB_CODE: SET DSD-WARNING-CODE
				//                               TO TRUE
				dfhcommarea.getDriverSpecificData().getErrorReturnCode().setWarningCode();
			} else {
				// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
				//                               TO TRUE
				ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
				// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
				copyUbocErrorDet();
				// COB_CODE: GO TO 4120-LINK-TO-PRIMARY-BOS-X
				return;
			}
		}
	}

	/**Original name: 4121-UBOC-CORRUPTION-CHK_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 * CHECK TO SEE IF THE UBOC LOGGABLE PROBLEMS FLAG CORRUPTED BY THE
	 * EXECUTING UOW.
	 * *****************************************************************</pre>*/
	private void ubocCorruptionChk() {
		// COB_CODE: SET SW-UBOC-LINKAGE-OKAY    TO TRUE.
		ws.getSwitches().setUbocLinkageCheckFlag(false);
		// COB_CODE: MOVE SPACES                 TO WA-BAD-LOGPROB
		//                                          WA-BAD-HDR
		//                                          WA-BAD-DATA
		//                                          WA-BAD-NLBE
		//                                          WA-BAD-WRNS.
		ws.getWorkArea().setWaBadLogprob("");
		ws.getWorkArea().setWaBadHdr("");
		ws.getWorkArea().setWaBadData("");
		ws.getWorkArea().setWaBadNlbe("");
		ws.getWorkArea().setWaBadWrns("");
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//             OR UBOC-UOW-LOGGABLE-WARNINGS
		//             OR UBOC-UOW-OK
		//               CONTINUE
		//           ELSE
		//              MOVE 'BAD LOGPROB FLAG'  TO WA-BAD-LOGPROB
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()
				|| ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isUbocUowLoggableWarnings()
				|| ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-UBOC-LINKAGE-CORRUPTED
			//                                    TO TRUE
			ws.getSwitches().setUbocLinkageCheckFlag(true);
			// COB_CODE: MOVE 'BAD LOGPROB FLAG'  TO WA-BAD-LOGPROB
			ws.getWorkArea().setWaBadLogprob("BAD LOGPROB FLAG");
		}
		// COB_CODE: IF UBOC-NBR-HDR-ROWS NOT NUMERIC
		//              MOVE 'BAD HDR'           TO WA-BAD-HDR
		//           END-IF.
		if (!Functions.isNumber(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrHdrRowsFormatted())) {
			// COB_CODE: SET SW-UBOC-LINKAGE-CORRUPTED
			//                                    TO TRUE
			ws.getSwitches().setUbocLinkageCheckFlag(true);
			// COB_CODE: MOVE ZERO                TO UBOC-NBR-HDR-ROWS
			ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrHdrRows(0);
			// COB_CODE: MOVE 'BAD HDR'           TO WA-BAD-HDR
			ws.getWorkArea().setWaBadHdr("BAD HDR");
		}
		// COB_CODE: IF UBOC-NBR-DATA-ROWS NOT NUMERIC
		//              MOVE 'BAD DATA'          TO WA-BAD-DATA
		//           END-IF.
		if (!Functions.isNumber(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrDataRowsFormatted())) {
			// COB_CODE: SET SW-UBOC-LINKAGE-CORRUPTED
			//                                    TO TRUE
			ws.getSwitches().setUbocLinkageCheckFlag(true);
			// COB_CODE: MOVE ZERO                TO UBOC-NBR-DATA-ROWS
			ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrDataRows(0);
			// COB_CODE: MOVE 'BAD DATA'          TO WA-BAD-DATA
			ws.getWorkArea().setWaBadData("BAD DATA");
		}
		// COB_CODE: IF UBOC-NBR-NONLOG-BL-ERRS NOT NUMERIC
		//              MOVE 'BAD NLBE'          TO WA-BAD-NLBE
		//           END-IF.
		if (!Functions.isNumber(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted())) {
			// COB_CODE: SET SW-UBOC-LINKAGE-CORRUPTED
			//                                    TO TRUE
			ws.getSwitches().setUbocLinkageCheckFlag(true);
			// COB_CODE: MOVE ZERO                TO UBOC-NBR-NONLOG-BL-ERRS
			ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrNonlogBlErrs(0);
			// COB_CODE: MOVE 'BAD NLBE'          TO WA-BAD-NLBE
			ws.getWorkArea().setWaBadNlbe("BAD NLBE");
		}
		// COB_CODE: IF UBOC-NBR-WARNINGS NOT NUMERIC
		//              MOVE 'BAD WRNS'          TO WA-BAD-NLBE
		//           END-IF.
		if (!Functions.isNumber(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted())) {
			// COB_CODE: SET SW-UBOC-LINKAGE-CORRUPTED
			//                                    TO TRUE
			ws.getSwitches().setUbocLinkageCheckFlag(true);
			// COB_CODE: MOVE ZERO                TO UBOC-NBR-WARNINGS
			ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrWarnings(0);
			// COB_CODE: MOVE 'BAD WRNS'          TO WA-BAD-NLBE
			ws.getWorkArea().setWaBadNlbe("BAD WRNS");
		}
	}

	/**Original name: 5000-PROCESS-RESPONSE-MODULE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  EXECUTE THE RESPONSE MODULE THAT WAS PASSED IN THE COMMON     *
	 *  PARMS.  THE RESPONSE MODULE WILL READ THE DATA UMT AND        *
	 *  FORMAT THE DATA IN THE RESPONSE COPYBOOK.                     *
	 * ****************************************************************
	 * *   IT IS NOT A REQUIREMENT THAT THERE BE A RESPONSE MODULE.
	 * *   A UOW MAY EXIST THAT DOES NOT REQUIRE OUTPUT.</pre>*/
	private void processResponseModule() {
		// COB_CODE: IF CSC-RESPONSE-MODULE OF WS-HUB-DATA
		//                                              EQUAL SPACES OR LOW-VALUES
		//               GO TO 5000-PROCESS-RESPONSE-MODULE-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getResponseModule())
				|| Characters.EQ_LOW.test(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getCscResponseModuleFormatted())) {
			// COB_CODE: MOVE ZERO               TO DSD-PM-RESHUB-ELAPSED
			dfhcommarea.getDriverSpecificData().getPerfMonitoringParms().setPmReshubElapsed(new AfDecimal(0));
			// COB_CODE: GO TO 5000-PROCESS-RESPONSE-MODULE-X
			return;
		}
		// COB_CODE: ACCEPT TE-TRAN-START-TIME FROM TIME.
		ws.getTimeElapsedWork().setStartTimeFormatted(CalendarUtil.getTimeHHMMSSMM());
		// COB_CODE: COMPUTE TE-TRAN-START-TIME-HTH =
		//                   (TE-TRAN-HOURS-START * 60 * 60 * 100) +
		//                   (TE-TRAN-MINS-START * 60 * 100) +
		//                   (TE-TRAN-SECS-START * 100) +
		//                   TE-TRAN-HTHS-START.
		ws.getTimeElapsedWork().setStartTimeHth(
				Trunc.toInt(ws.getTimeElapsedWork().getHoursStart() * 60 * 60 * 100 + ws.getTimeElapsedWork().getMinsStart() * 60 * 100
						+ ws.getTimeElapsedWork().getSecsStart() * 100 + ws.getTimeElapsedWork().getHthsStart(), 8));
		// COB_CODE: MOVE COM-CICS-COM-LENGTH    TO WA-COMM-LENGTH.
		ws.getWorkArea().setWaCommLength(Trunc.toShort(ws.getHallcom().getCicsComLength(), 4));
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CSC-RESPONSE-MODULE OF WS-HUB-DATA)
		//               COMMAREA   (WS-HUB-DATA)
		//               DATALENGTH (LENGTH OF WS-HUB-DATA)
		//               LENGTH     (WA-COMM-LENGTH)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData()).length(ws.getWorkArea().getWaCommLength())
				.link(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getResponseModule());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 5000-PROCESS-RESPONSE-MODULE-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '5000-PROCESS-RESPONSE-MODULE'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5000-PROCESS-RESPONSE-MODULE");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("5000-PROCESS-RESPONSE-MODULE");
			// COB_CODE: MOVE CSC-RESPONSE-MODULE OF WS-HUB-DATA
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrObjectName(ws.getWsHubData().getCommunicationShellCommon().getCscGeneralParms().getResponseModule());
			// COB_CODE: MOVE 'LINK TO RESPONSE MODULE FAILED.'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO RESPONSE MODULE FAILED.");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 5000-PROCESS-RESPONSE-MODULE-X
			return;
		}
		//* IF HALT AND RETURN HAS BEEN SET TO TRUE,
		//* STOP PROCESSING BUSINESS OBJECTS
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 5000-PROCESS-RESPONSE-MODULE-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: SET MDRV-PERFORM-ROLLBACK
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: GO TO 5000-PROCESS-RESPONSE-MODULE-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-LOGGABLE-WARNINGS
		//                                       TO TRUE
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isUbocUowLoggableWarnings()) {
			// COB_CODE: SET DSD-WARNING-CODE    TO TRUE
			dfhcommarea.getDriverSpecificData().getErrorReturnCode().setWarningCode();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-WARNS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableWarns();
		}
		// COB_CODE: PERFORM 9902-PROCESS-ELAPSED-TIME.
		processElapsedTime();
		// COB_CODE: COMPUTE DSD-PM-RESHUB-ELAPSED =
		//               TE-TRANS-ELAPSED-TIME-AMT / 100.
		dfhcommarea.getDriverSpecificData().getPerfMonitoringParms()
				.setPmReshubElapsed(new AfDecimal(((((double) (ws.getTimeElapsedWork().getsElapsedTimeAmt()))) / 100), 10, 2));
	}

	/**Original name: 6000-CLEAR-UMTS-TSQS_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  READ AND CLEAR OUT THE UMTS AND TSQS USED FOR THIS      *
	 *  UNIT OF WORK.                                           *
	 * **********************************************************</pre>*/
	private void clearUmtsTsqs() {
		// COB_CODE: MOVE 0                      TO WA-NBR-RESP-HDR-ROWS-READ
		//                                          WA-NBR-RESP-DATA-ROWS-READ
		//                                          WA-NBR-RESP-WRN-ROWS-READ
		//                                          WA-NBR-RESP-NBE-ROWS-READ.
		ws.getWorkArea().setWaNbrRespHdrRowsRead(0);
		ws.getWorkArea().setWaNbrRespDataRowsRead(0);
		ws.getWorkArea().setWaNbrRespWrnRowsRead(0);
		ws.getWorkArea().setWaNbrRespNbeRowsRead(0);
		// COB_CODE: IF UBOC-NBR-NONLOG-BL-ERRS > 0
		//               PERFORM 6100-CLEAR-UOW-NLBE-UMT
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs() > 0) {
			// COB_CODE: PERFORM 6100-CLEAR-UOW-NLBE-UMT
			clearUowNlbeUmt();
		}
		// COB_CODE: IF UBOC-NBR-WARNINGS > 0
		//               PERFORM 6200-CLEAR-UOW-UWRN-UMT
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrWarnings() > 0) {
			// COB_CODE: PERFORM 6200-CLEAR-UOW-UWRN-UMT
			clearUowUwrnUmt();
		}
		//*   IF THERE WERE PROBLEMS DURING THE ACUTUAL LOGGING OF AN
		//*   ERROR, WE WILL WANT TO KEEP THE UMTS AND TSQS INTACT
		//*   FOR INVESTIGATIVE PURPOSES.
		// COB_CODE: IF UBOC-KEEP-UOW-STORAGE
		//               GO TO 6000-CLEAR-UMTS-TSQS-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocKeepClearUowStorageSw().isUbocKeepUowStorage()) {
			// COB_CODE: GO TO 6000-CLEAR-UMTS-TSQS-X
			return;
		}
		// COB_CODE: PERFORM 6300-DELETE-UOW-APP-TSQ.
		deleteUowAppTsq();
		// COB_CODE: PERFORM 6400-CLEAR-UOW-UKRP-UMT.
		clearUowUkrpUmt();
		// COB_CODE: PERFORM 6500-CLEAR-UOW-URQM-UMT.
		clearUowUrqmUmt();
		// COB_CODE: PERFORM 6600-CLEAR-UOW-USW-UMT.
		clearUowUswUmt();
		// COB_CODE: PERFORM 6700-DELETE-UOW-SWITCH-TSQ.
		deleteUowSwitchTsq();
		// COB_CODE: PERFORM 6800-DELETE-UOW-LOCK-TSQ.
		deleteUowLockTsq();
		// COB_CODE: IF UBOC-NBR-HDR-ROWS > 0
		//               PERFORM 6900-CLEAR-UOW-UHDR-UMT
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrHdrRows() > 0) {
			// COB_CODE: PERFORM 6900-CLEAR-UOW-UHDR-UMT
			clearUowUhdrUmt();
		}
		// COB_CODE: IF UBOC-NBR-DATA-ROWS > 0
		//               PERFORM 6A00-CLEAR-UOW-UDAT-UMT
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrDataRows() > 0) {
			// COB_CODE: PERFORM 6A00-CLEAR-UOW-UDAT-UMT
			a00ClearUowUdatUmt();
		}
	}

	/**Original name: 6100-CLEAR-UOW-NLBE-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE NLBE UOW LEVEL RESPONSE UMT AND WRITE THE RECORDS    *
	 *  TO THE COMMON COPYBOOK TABLE.  DELETE EACH ROW FROM THE       *
	 *  NLBE UMT AFTER PROCESSING IT.                                 *
	 * ****************************************************************</pre>*/
	private void clearUowNlbeUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS
		//                                       TO DSD-NON-LOGGABLE-ERROR-CNT.
		dfhcommarea.getDriverSpecificData()
				.setNonLoggableErrorCntFormatted(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: IF (DSD-ERROR-RETURN-CODE < CF-NLBE-ERROR-CODE)
		//               SET DSD-NLBE-CODE       TO TRUE
		//           END-IF.
		if (dfhcommarea.getDriverSpecificData().getErrorReturnCode().getErrorReturnCode() < ws.getCfNlbeErrorCode()) {
			// COB_CODE: SET DSD-NLBE-CODE       TO TRUE
			dfhcommarea.getDriverSpecificData().getErrorReturnCode().setNlbeCode();
		}
		// COB_CODE: MOVE MDRV-MSG-ID            TO NLBE-ID.
		ws.getNlbeCommon().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE 0                      TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(((short) 0));
		// COB_CODE: SET SW-START-OF-UOW-NLBE-BUS-ERRS
		//                                       TO TRUE.
		ws.getSwitches().getUowNlbeBusErrsFlag().setStartOfUowNlbeBusErrs();
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-RESP-NL-BL-ERRS-STORE)
		//               RIDFLD       (NLBE-KEY)
		//               KEYLENGTH    (LENGTH OF NLBE-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, NlbeCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6100-CLEAR-UOW-NLBE-UMT-X
		//               WHEN OTHER
		//                   GO TO 6100-CLEAR-UOW-NLBE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
			//                               TO TRUE
			ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
			// COB_CODE: GO TO 6100-CLEAR-UOW-NLBE-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'NLBE-KEY = '       NLBE-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6100-CLEAR-UOW-NLBE-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6100-CLEAR-UOW-NLBE-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6100-CLEAR-UOW-NLBE-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6100-CLEAR-UOW-NLBE-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6110-PROCESS-UOW-NLBE-UMT
		//               UNTIL SW-NO-MORE-UOW-NLBE-BUS-ERRS.
		while (!ws.getSwitches().getUowNlbeBusErrsFlag().isNoMoreUowNlbeBusErrs()) {
			processUowNlbeUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-RESP-NL-BL-ERRS-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6100-CLEAR-UOW-NLBE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'NLBE-KEY = '     NLBE-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6100-CLEAR-UOW-NLBE-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6100-CLEAR-UOW-NLBE-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6100-CLEAR-UOW-NLBE-UMT");
			// COB_CODE: MOVE 'ENDBR OF NL BL ERRS RESPONSE UMT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ENDBR OF NL BL ERRS RESPONSE UMT FAILED");
			// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6100-CLEAR-UOW-NLBE-UMT-X
			return;
		}
	}

	/**Original name: 6110-PROCESS-UOW-NLBE-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE NLBE UOW LEVEL RESPONSE UMT AND WRITE THE RECORDS    *
	 *  TO THE COMMON COPYBOOK TABLE.  DELETE EACH ROW FROM THE       *
	 *  NLBE UMT AFTER PROCESSING IT.                                 *
	 * ****************************************************************</pre>*/
	private void processUowNlbeUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-RESP-NL-BL-ERRS-STORE)
		//               INTO          (WS-NLBE-MSG)
		//               RIDFLD        (NLBE-KEY)
		//               KEYLENGTH     (LENGTH OF NLBE-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, NlbeCommon.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getNlbeCommon().setKeyBytes(iRowData.getKey());
				ws.setWsNlbeMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                   GO TO 6110-PROCESS-UOW-NLBE-UMT-X
		//               WHEN OTHER
		//                   GO TO 6110-PROCESS-UOW-NLBE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF NLBE-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6110-PROCESS-UOW-NLBE-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getNlbeCommon().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: IF UBOC-NBR-NONLOG-BL-ERRS
				//             EQUAL WA-NBR-RESP-NBE-ROWS-READ
				//                           TO TRUE
				//           ELSE
				//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
				//           END-IF
				if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs() == ws.getWorkArea().getWaNbrRespNbeRowsRead()) {
					// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
					//                       TO TRUE
					ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
				} else {
					// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
					//                       TO TRUE
					ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
					// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
					// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
					// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
					// COB_CODE: STRING 'NLBE-KEY = '
					//                   NLBE-KEY ';'
					//               DELIMITED BY SIZE
					//               INTO EFAL-OBJ-DATA-KEY
					//           END-STRING
					concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(),
							";");
					ws.getWsEstoInfo().getEstoDetailBuffer()
							.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
					// COB_CODE: MOVE '6110-PROCESS-UOW-NLBE-UMT'
					//                       TO EFAL-ERR-PARAGRAPH
					//                          CSC-FAILED-PARAGRAPH
					//                            OF WS-HUB-DATA
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6110-PROCESS-UOW-NLBE-UMT");
					ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6110-PROCESS-UOW-NLBE-UMT");
					// COB_CODE: MOVE 'NUM DATA ROWS IN UMT DID NOT MATCH CNT'
					//                       TO EFAL-ERR-COMMENT
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NUM DATA ROWS IN UMT DID NOT MATCH CNT");
					// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
					//                       TO EFAL-ERR-OBJECT-NAME
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
					// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
					logMainDrvrError();
				}
				// COB_CODE: GO TO 6110-PROCESS-UOW-NLBE-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: IF UBOC-NBR-NONLOG-BL-ERRS
			//             EQUAL WA-NBR-RESP-NBE-ROWS-READ
			//                               TO TRUE
			//           ELSE
			//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
			//           END-IF
			if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs() == ws.getWorkArea().getWaNbrRespNbeRowsRead()) {
				// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
				//                           TO TRUE
				ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
			} else {
				// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
				//                           TO TRUE
				ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
				// COB_CODE: STRING 'NLBE-KEY = '     NLBE-KEY ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE '6110-PROCESS-UOW-NLBE-UMT'
				//                           TO EFAL-ERR-PARAGRAPH
				//                              CSC-FAILED-PARAGRAPH
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6110-PROCESS-UOW-NLBE-UMT");
				ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6110-PROCESS-UOW-NLBE-UMT");
				// COB_CODE: MOVE 'END OF FILE IN NLBE UMT -ALL RECS NOT FND.'
				//                           TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("END OF FILE IN NLBE UMT -ALL RECS NOT FND.");
				// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
				//                           TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
				// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
				logMainDrvrError();
			}
			// COB_CODE: GO TO 6110-PROCESS-UOW-NLBE-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
			//                               TO TRUE
			ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'NLBE-KEY = '      NLBE-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6110-PROCESS-UOW-NLBE-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6110-PROCESS-UOW-NLBE-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6110-PROCESS-UOW-NLBE-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNEXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNEXT UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6110-PROCESS-UOW-NLBE-UMT-X
			return;
		}
		// COB_CODE: ADD 1                       TO WA-NBR-RESP-NBE-ROWS-READ.
		ws.getWorkArea().setWaNbrRespNbeRowsRead(Trunc.toInt(1 + ws.getWorkArea().getWaNbrRespNbeRowsRead(), 9));
		// COB_CODE: MOVE NLBE-REC-SEQ           TO SS-NLBE.
		ws.getSubscripts().setNlbe(ws.getNlbeCommon().getRecSeq());
		// COB_CODE: IF (SS-NLBE NOT > SS-MAX-UMT-SUBSCRIPT)
		//               PERFORM 6111-MOVE-NLBE-TO-COPYBOOK
		//           END-IF.
		if (ws.getSubscripts().getNlbe() <= ws.getSubscripts().getMaxUmtSubscript()) {
			// COB_CODE: PERFORM 6111-MOVE-NLBE-TO-COPYBOOK
			moveNlbeToCopybook();
		}
		// COB_CODE: EXEC CICS
		//               DELETE FILE (MTCS-UOW-RESP-NL-BL-ERRS-STORE)
		//               RIDFLD      (NLBE-KEY)
		//               KEYLENGTH   (LENGTH OF NLBE-KEY)
		//               RESP        (WA-RESPONSE-CODE)
		//               RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.delete(iRowData, NlbeCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6110-PROCESS-UOW-NLBE-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-NLBE-BUS-ERRS
			//                               TO TRUE
			ws.getSwitches().getUowNlbeBusErrsFlag().setNoMoreUowNlbeBusErrs();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'NLBE-KEY = '       NLBE-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "NLBE-KEY = ", ws.getNlbeCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6110-PROCESS-UOW-NLBE-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6110-PROCESS-UOW-NLBE-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6110-PROCESS-UOW-NLBE-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-NL-BL-ERRS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespNlBlErrsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6110-PROCESS-UOW-NLBE-UMT-X
			return;
		}
	}

	/**Original name: 6111-MOVE-NLBE-TO-COPYBOOK_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   MOVE THE NON-LOGGABLE TEXT FROM THE HALFNLBE UMT TO THE      *
	 *   NLBE ERROR HANDLING TABLE IN THE COMMON COPYBOOK.            *
	 * ***************************************************************</pre>*/
	private void moveNlbeToCopybook() {
		// COB_CODE: MOVE NLBE-NONLOGGABLE-BP-ERR-TEXT
		//                                       TO DSD-NON-LOG-ERR-MSG (SS-NLBE).
		dfhcommarea.getDriverSpecificData().getNonLoggableErrors(ws.getSubscripts().getNlbe())
				.setDsdNonLogErrMsg(ws.getNlbeCommon().getNonloggableBpErrText());
	}

	/**Original name: 6200-CLEAR-UOW-UWRN-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ THE WARNINGS UOW LEVEL RESPONSE UMT AND WRITE THE RECORDS*
	 *  TO THE COMMON COPYBOOK TABLE.  DELETE EACH ROW FROM THE       *
	 *  WARNINGS UMT AFTER PROCESSING IT.                             *
	 * ****************************************************************</pre>*/
	private void clearUowUwrnUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-NBR-WARNINGS      TO DSD-WARNING-CNT.
		dfhcommarea.getDriverSpecificData().setWarningCntFormatted(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: IF (DSD-ERROR-RETURN-CODE < CF-WARNING-ERROR-CODE)
		//               SET DSD-WARNING-CODE    TO TRUE
		//           END-IF.
		if (dfhcommarea.getDriverSpecificData().getErrorReturnCode().getErrorReturnCode() < ws.getCfWarningErrorCode()) {
			// COB_CODE: SET DSD-WARNING-CODE    TO TRUE
			dfhcommarea.getDriverSpecificData().getErrorReturnCode().setWarningCode();
		}
		// COB_CODE: MOVE MDRV-MSG-ID            TO UWRN-ID.
		ws.getUwrnCommon().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE 0                      TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(((short) 0));
		// COB_CODE: SET SW-START-OF-UOW-WARNINGS
		//                                       TO TRUE.
		ws.getSwitches().getUowWarningsFlag().setStartOfUowWarnings();
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-RESP-WARNINGS-STORE)
		//               RIDFLD       (UWRN-KEY)
		//               KEYLENGTH    (LENGTH OF UWRN-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, UwrnCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6200-CLEAR-UOW-UWRN-UMT-X
		//               WHEN OTHER
		//                   GO TO 6200-CLEAR-UOW-UWRN-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
			//                               TO TRUE
			ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
			// COB_CODE: GO TO 6200-CLEAR-UOW-UWRN-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'UWRN-KEY = '      UWRN-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6200-CLEAR-UOW-UWRN-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6200-CLEAR-UOW-UWRN-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6200-CLEAR-UOW-UWRN-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6200-CLEAR-UOW-UWRN-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6210-PROCESS-UOW-UWRN-UMT
		//             UNTIL SW-NO-MORE-UOW-WARNINGS.
		while (!ws.getSwitches().getUowWarningsFlag().isNoMoreUowWarnings()) {
			processUowUwrnUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-RESP-WARNINGS-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'UWRN-KEY = '      UWRN-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6200-CLEAR-UOW-UWRN-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6200-CLEAR-UOW-UWRN-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6200-CLEAR-UOW-UWRN-UMT");
			// COB_CODE: MOVE 'ENDBR OF WARNINGS RESPONSE UMT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ENDBR OF WARNINGS RESPONSE UMT FAILED");
			// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6210-PROCESS-UOW-UWRN-UMT_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  READ THE UOW LEVEL WARNINGS UMT AND WRITE THE RECORDS TO*
	 *  THE COMMON COPYBOOK TABLE.  DELETE EACH RECORD FROM     *
	 *  THE WARNINGS UMT AFTER PROCESSING EACH RECORD.          *
	 * **********************************************************</pre>*/
	private void processUowUwrnUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-RESP-WARNINGS-STORE)
		//               INTO          (WS-UWRN-MSG)
		//               RIDFLD        (UWRN-KEY)
		//               KEYLENGTH     (LENGTH OF UWRN-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, UwrnCommon.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUwrnCommon().setKeyBytes(iRowData.getKey());
				ws.setWsUwrnMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                   GO TO 6210-PROCESS-UOW-UWRN-UMT-X
		//               WHEN OTHER
		//                   GO TO 6210-PROCESS-UOW-UWRN-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UWRN-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6210-PROCESS-UOW-UWRN-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getUwrnCommon().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: IF UBOC-NBR-WARNINGS
				//             EQUAL WA-NBR-RESP-WRN-ROWS-READ
				//                           TO TRUE
				//           ELSE
				//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
				//           END-IF
				if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrWarnings() == ws.getWorkArea().getWaNbrRespWrnRowsRead()) {
					// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
					//                       TO TRUE
					ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
				} else {
					// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
					//                       TO TRUE
					ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
					// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
					// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
					// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
					// COB_CODE: STRING 'UWRN-KEY = '
					//                   UWRN-KEY ';'
					//               DELIMITED BY SIZE
					//               INTO EFAL-OBJ-DATA-KEY
					//           END-STRING
					concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(),
							";");
					ws.getWsEstoInfo().getEstoDetailBuffer()
							.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
					// COB_CODE: MOVE '6210-PROCESS-UOW-UWRN-UMT'
					//                       TO EFAL-ERR-PARAGRAPH
					//                          CSC-FAILED-PARAGRAPH
					//                            OF WS-HUB-DATA
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6210-PROCESS-UOW-UWRN-UMT");
					ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6210-PROCESS-UOW-UWRN-UMT");
					// COB_CODE: MOVE 'NUM DATA ROWS IN UMT DID NOT MATCH CNT'
					//                       TO EFAL-ERR-COMMENT
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NUM DATA ROWS IN UMT DID NOT MATCH CNT");
					// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
					//                       TO EFAL-ERR-OBJECT-NAME
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
					// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
					logMainDrvrError();
				}
				// COB_CODE: GO TO 6210-PROCESS-UOW-UWRN-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: IF UBOC-NBR-WARNINGS EQUAL WA-NBR-RESP-WRN-ROWS-READ
			//                               TO TRUE
			//           ELSE
			//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
			//           END-IF
			if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrWarnings() == ws.getWorkArea().getWaNbrRespWrnRowsRead()) {
				// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
				//                           TO TRUE
				ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
			} else {
				// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
				//                           TO TRUE
				ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
				// COB_CODE: STRING 'UWRN-KEY = '     UWRN-KEY ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE '6210-PROCESS-UOW-UWRN-UMT'
				//                           TO EFAL-ERR-PARAGRAPH
				//                              CSC-FAILED-PARAGRAPH
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6210-PROCESS-UOW-UWRN-UMT");
				ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6210-PROCESS-UOW-UWRN-UMT");
				// COB_CODE: MOVE 'END OF FILE IN NLBE UMT -ALL RECS NOT FND.'
				//                           TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("END OF FILE IN NLBE UMT -ALL RECS NOT FND.");
				// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
				//                           TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
				// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
				logMainDrvrError();
			}
			// COB_CODE: GO TO 6210-PROCESS-UOW-UWRN-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
			//                               TO TRUE
			ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UWRN-KEY = '      UWRN-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6210-PROCESS-UOW-UWRN-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6210-PROCESS-UOW-UWRN-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6210-PROCESS-UOW-UWRN-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNEXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNEXT UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6210-PROCESS-UOW-UWRN-UMT-X
			return;
		}
		// COB_CODE: ADD 1                       TO WA-NBR-RESP-WRN-ROWS-READ.
		ws.getWorkArea().setWaNbrRespWrnRowsRead(Trunc.toInt(1 + ws.getWorkArea().getWaNbrRespWrnRowsRead(), 9));
		// COB_CODE: MOVE UWRN-REC-SEQ           TO SS-UWRN.
		ws.getSubscripts().setUwrn(ws.getUwrnCommon().getRecSeq());
		// COB_CODE: IF (SS-UWRN NOT > SS-MAX-UMT-SUBSCRIPT)
		//               PERFORM 6211-MOVE-UWRN-TO-COPYBOOK
		//           END-IF.
		if (ws.getSubscripts().getUwrn() <= ws.getSubscripts().getMaxUmtSubscript()) {
			// COB_CODE: PERFORM 6211-MOVE-UWRN-TO-COPYBOOK
			moveUwrnToCopybook();
		}
		// COB_CODE: EXEC CICS
		//                DELETE FILE (MTCS-UOW-RESP-WARNINGS-STORE)
		//                RIDFLD      (UWRN-KEY)
		//                KEYLENGTH   (LENGTH OF UWRN-KEY)
		//                RESP        (WA-RESPONSE-CODE)
		//                RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.delete(iRowData, UwrnCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6210-PROCESS-UOW-UWRN-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-WARNINGS
			//                               TO TRUE
			ws.getSwitches().getUowWarningsFlag().setNoMoreUowWarnings();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'UWRN-KEY = '     UWRN-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UWRN-KEY = ", ws.getUwrnCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6210-PROCESS-UOW-UWRN-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6210-PROCESS-UOW-UWRN-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6210-PROCESS-UOW-UWRN-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: MOVE MTCS-UOW-RESP-WARNINGS-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespWarningsStore());
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6210-PROCESS-UOW-UWRN-UMT-X
			return;
		}
	}

	/**Original name: 6211-MOVE-UWRN-TO-COPYBOOK_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *   MOVE THE WARNING TEXT FROM THE HALFUWRN UMT TO THE           *
	 *   WARNING ERROR HANDLING TABLE IN THE COMMON COPYBOOK.         *
	 * ****************************************************************</pre>*/
	private void moveUwrnToCopybook() {
		// COB_CODE: MOVE UWRN-WARNING-TEXT      TO
		//                                       DSD-WARN-MSG (SS-UWRN).
		dfhcommarea.getDriverSpecificData().getWarnings(ws.getSubscripts().getUwrn()).setDsdWarnMsg(ws.getUwrnCommon().getWarningText());
	}

	/**Original name: 6300-DELETE-UOW-APP-TSQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELETE ANY TSQ THAT MAY HAVE BEEN CREATED BY THE UOW TO CARRY
	 *  OUT ANY APP-SPECIFIC PROCESSING.
	 * *****************************************************************
	 *  RESET THIS FLAG IN CASE A PREVIOUS UOW FAILURE HAS OCCURRED.
	 *  OTHERWISE, THIS SECTION WILL THINK THAT HALOUKRP IS RETURNING
	 *  A FAILURE.</pre>*/
	private void deleteUowAppTsq() {
		ConcatUtil concatUtil = null;
		// COB_CODE: SET UBOC-CONTINUE-PROCESSING
		//                                       TO TRUE.
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setUbocContinueProcessing();
		// COB_CODE: SET HALOUKRP-RETRIEVE-FUNC  TO TRUE.
		ws.getHallukrp().getFunction().setHaloukrpRetrieveFunc();
		// COB_CODE: MOVE 'TSQNAME'              TO HALOUKRP-KEY-REPL-LABEL.
		ws.getHallukrp().setKeyReplLabel("TSQNAME");
		// COB_CODE: MOVE LENGTH OF HALOUKRP-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) Ts020000Data.Len.HALOUKRP_LINKAGE));
		// COB_CODE: MOVE HALOUKRP-LINKAGE       TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getHaloukrpLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOUKRP')
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUKRP",
				new Haloukrp());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6300-DELETE-UOW-APP-TSQ-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '6300-DELETE-UOW-APP-TSQ'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6300-DELETE-UOW-APP-TSQ");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6300-DELETE-UOW-APP-TSQ");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM LINK TO PROGRAM'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM LINK TO PROGRAM");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE 'HALOUKRP'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUKRP");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6300-DELETE-UOW-APP-TSQ-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 6300-DELETE-UOW-APP-TSQ-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: GO TO 6300-DELETE-UOW-APP-TSQ-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER   TO HALOUKRP-LINKAGE.
		ws.setHaloukrpLinkageFormatted(ws.getWsHubData().getUbocRecord().getAppDataBufferFormatted());
		// COB_CODE: IF HALOUKRP-NOTFND
		//               GO TO 6300-DELETE-UOW-APP-TSQ-X
		//           END-IF.
		if (ws.getHallukrp().getReturnCode().isHaloukrpNotfnd()) {
			// COB_CODE: GO TO 6300-DELETE-UOW-APP-TSQ-X
			return;
		}
		// COB_CODE: MOVE SPACES                 TO WA-UOW-LEVEL-TSQNAME.
		ws.getWorkArea().setWaUowLevelTsqname("");
		// COB_CODE: MOVE HALOUKRP-KEY-REPL-KEY (1:HALOUKRP-KEY-REPL-KEY-LEN)
		//                                       TO WA-UOW-LEVEL-TSQNAME.
		ws.getWorkArea().setWaUowLevelTsqname(ws.getHallukrp().getKeyReplKeyFormatted().substring((1) - 1, ws.getHallukrp().getKeyReplKeyLen()));
		// COB_CODE: EXEC CICS
		//               DELETEQ TS QNAME(WA-UOW-LEVEL-TSQNAME)
		//               RESP            (WA-RESPONSE-CODE)
		//               RESP2           (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWorkArea().getWaUowLevelTsqnameFormatted());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		//* SINCE THIS MODULE DOES NOT CONTROL THE CREATION OF THE TSQ,
		//* IT WILL IGNORE A QIDERR IN CASE THE APPLICATION PROCESSING
		//* FAILED TO CREATE IT OR HAS ALREADY DELETED IT.
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: STRING 'TSQ NAME= '   WA-UOW-LEVEL-TSQNAME  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "TSQ NAME= ",
					ws.getWorkArea().getWaUowLevelTsqnameFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6300-DELETE-UOW-APP-TSQ'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6300-DELETE-UOW-APP-TSQ");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6300-DELETE-UOW-APP-TSQ");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE WA-UOW-LEVEL-TSQNAME
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWorkArea().getWaUowLevelTsqname());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6400-CLEAR-UOW-UKRP-UMT_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  DELETE THE RECORDS ADDED TO THE KEY REPLACEMENT UMT     *
	 * **********************************************************</pre>*/
	private void clearUowUkrpUmt() {
		// COB_CODE: SET HALOUKRP-CLEAR-MSG-ID-FUNC
		//                                       TO TRUE.
		ws.getHallukrp().getFunction().setHaloukrpClearMsgIdFunc();
		// COB_CODE: MOVE LENGTH OF HALOUKRP-LINKAGE
		//                                       TO UBOC-APP-DATA-BUFFER-LENGTH.
		ws.getWsHubData().getUbocRecord().setAppDataBufferLength(((short) Ts020000Data.Len.HALOUKRP_LINKAGE));
		// COB_CODE: MOVE HALOUKRP-LINKAGE       TO UBOC-APP-DATA-BUFFER.
		ws.getWsHubData().getUbocRecord().setAppDataBuffer(ws.getHaloukrpLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOUKRP')
		//               COMMAREA (UBOC-RECORD)
		//               LENGTH   (LENGTH OF UBOC-RECORD)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsHubData().getUbocRecord()).length(UbocRecord.Len.UBOC_RECORD).link("HALOUKRP",
				new Haloukrp());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE '6400-CLEAR-UOW-UKRP-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6400-CLEAR-UOW-UKRP-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6400-CLEAR-UOW-UKRP-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM LINK TO PROGRAM'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM LINK TO PROGRAM");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE 'HALOUKRP'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUKRP");
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 6400-CLEAR-UOW-UKRP-UMT-X
		//           END-IF.
		if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 9100-COPY-UBOC-ERROR-DET
			copyUbocErrorDet();
			// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS
			//                                   TO TRUE
			ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
			// COB_CODE: GO TO 6400-CLEAR-UOW-UKRP-UMT-X
			return;
		}
	}

	/**Original name: 6500-CLEAR-UOW-URQM-UMT_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  DELETE THE RECORDS ADDED TO THE UOW REQUEST UMT        *
	 * *********************************************************</pre>*/
	private void clearUowUrqmUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET SW-START-BROWSE-UOW-REQUEST
		//                                       TO TRUE.
		ws.getSwitches().getClearUowRequestFlag().setStartBrowseUowRequest();
		// COB_CODE: MOVE MDRV-MSG-ID            TO URQM-ID.
		ws.getUrqmCommon().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE LOW-VALUES             TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(LiteralGenerator.create(Types.LOW_CHAR_VAL, UrqmCommon.Len.BUS_OBJ));
		// COB_CODE: MOVE 0                      TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(0);
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-REQ-MSG-STORE)
		//               RIDFLD       (URQM-KEY)
		//               KEYLENGTH    (LENGTH OF URQM-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, UrqmCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6500-CLEAR-UOW-URQM-UMT-X
		//               WHEN OTHER
		//                   GO TO 6500-CLEAR-UOW-URQM-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: GO TO 6500-CLEAR-UOW-URQM-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'URQM-KEY = '    URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6500-CLEAR-UOW-URQM-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6500-CLEAR-UOW-URQM-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6500-CLEAR-UOW-URQM-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6500-CLEAR-UOW-URQM-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6510-PROCESS-UOW-URQM-UMT
		//               UNTIL SW-NO-MORE-UOW-REQUESTS.
		while (!ws.getSwitches().getClearUowRequestFlag().isNoMoreUowRequests()) {
			processUowUrqmUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-REQ-MSG-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'URQM-KEY = '    URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6500-CLEAR-UOW-URQM-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6500-CLEAR-UOW-URQM-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6500-CLEAR-UOW-URQM-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM ENDBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM ENDBR UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6510-PROCESS-UOW-URQM-UMT_FIRST_SENTENCES<br>
	 * <pre>**************************************************
	 *  READ EACH RECORD FROM THE UOW REQUEST UMT, THEN *
	 *  USE THE KEYS TO DELETE THE RECORD FROM THE UMT. *
	 * **************************************************</pre>*/
	private void processUowUrqmUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-REQ-MSG-STORE)
		//               INTO          (WS-URQM-MSG)
		//               RIDFLD        (URQM-KEY)
		//               KEYLENGTH     (LENGTH OF URQM-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, UrqmCommon.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setKeyBytes(iRowData.getKey());
				ws.setWsUrqmMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6510-PROCESS-UOW-URQM-UMT-X
		//               WHEN OTHER
		//                   GO TO 6510-PROCESS-UOW-URQM-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF URQM-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6510-PROCESS-UOW-URQM-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getUrqmCommon().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
				//                           TO TRUE
				ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
				// COB_CODE: GO TO 6510-PROCESS-UOW-URQM-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if ((TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE)
				|| (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: GO TO 6510-PROCESS-UOW-URQM-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'URQM-KEY = '     URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6510-PROCESS-UOW-URQM-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6510-PROCESS-UOW-URQM-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6510-PROCESS-UOW-URQM-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6510-PROCESS-UOW-URQM-UMT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE FILE (MTCS-UOW-REQ-MSG-STORE)
		//               RIDFLD      (URQM-KEY)
		//               KEYLENGTH   (LENGTH OF URQM-KEY)
		//               RESP        (WA-RESPONSE-CODE)
		//               RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowDAO.delete(iRowData, UrqmCommon.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'URQM-KEY = '     URQM-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY = ", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6510-PROCESS-UOW-URQM-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6510-PROCESS-UOW-URQM-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6510-PROCESS-UOW-URQM-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6600-CLEAR-UOW-USW-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************
	 *  BROWSE THE REQUEST SWITCHES UMT TO START THE      *
	 *  PROCESS OF DELETING THE SWITCHES RECORDS.         *
	 * ****************************************************</pre>*/
	private void clearUowUswUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET SW-START-BROWSE-UOW-REQUEST
		//                                       TO TRUE.
		ws.getSwitches().getClearUowRequestFlag().setStartBrowseUowRequest();
		// COB_CODE: MOVE MDRV-MSG-ID            TO USW-ID.
		ws.getHallusw().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE LOW-VALUES             TO USW-BUS-OBJ-SWITCH.
		ws.getHallusw().setBusObjSwitch(LiteralGenerator.create(Types.LOW_CHAR_VAL, Hallusw.Len.BUS_OBJ_SWITCH));
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-REQ-SWITCHES-STORE)
		//               RIDFLD       (USW-KEY)
		//               KEYLENGTH    (LENGTH OF USW-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Hallusw.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6600-CLEAR-UOW-USW-UMT-X
		//               WHEN OTHER
		//                   GO TO 6600-CLEAR-UOW-USW-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: GO TO 6600-CLEAR-UOW-USW-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'USW-KEY = '     USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "USW-KEY = ", ws.getHallusw().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6600-CLEAR-UOW-USW-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6600-CLEAR-UOW-USW-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6600-CLEAR-UOW-USW-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-MSG-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqMsgStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6600-CLEAR-UOW-USW-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6610-PROCESS-UOW-USW-UMT
		//                UNTIL SW-NO-MORE-UOW-REQUESTS.
		while (!ws.getSwitches().getClearUowRequestFlag().isNoMoreUowRequests()) {
			processUowUswUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-REQ-SWITCHES-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'USW-KEY = '     USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "USW-KEY = ", ws.getHallusw().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6600-CLEAR-UOW-USW-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6600-CLEAR-UOW-USW-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6600-CLEAR-UOW-USW-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM ENDBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM ENDBR UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-SWITCHES-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6610-PROCESS-UOW-USW-UMT_FIRST_SENTENCES<br>
	 * <pre>*************************************************************
	 *  READ EACH RECORD IN THE REQUEST SWITCHES UMT AND DELETE IT.*
	 * *************************************************************</pre>*/
	private void processUowUswUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-REQ-SWITCHES-STORE)
		//               INTO          (WS-USW-MSG)
		//               RIDFLD        (USW-KEY)
		//               KEYLENGTH     (LENGTH OF USW-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Hallusw.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setKeyBytes(iRowData.getKey());
				ws.setWsUswMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6610-PROCESS-UOW-USW-UMT-X
		//               WHEN OTHER
		//                   GO TO 6610-PROCESS-UOW-USW-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF USW-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6610-PROCESS-UOW-USW-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getHallusw().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
				//                           TO TRUE
				ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
				// COB_CODE: GO TO 6610-PROCESS-UOW-USW-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if ((TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE)
				|| (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND)) {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: GO TO 6610-PROCESS-UOW-USW-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'USW-KEY = '     USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "USW-KEY = ", ws.getHallusw().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6610-PROCESS-UOW-USW-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6610-PROCESS-UOW-USW-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6610-PROCESS-UOW-USW-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-SWITCHES-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6610-PROCESS-UOW-USW-UMT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETE FILE (MTCS-UOW-REQ-SWITCHES-STORE)
		//               RIDFLD      (USW-KEY)
		//               KEYLENGTH   (LENGTH OF USW-KEY)
		//               RESP        (WA-RESPONSE-CODE)
		//               RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowDAO.delete(iRowData, Hallusw.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-REQUESTS
			//                               TO TRUE
			ws.getSwitches().getClearUowRequestFlag().setNoMoreUowRequests();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'USW-KEY = '      USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "USW-KEY = ", ws.getHallusw().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6610-PROCESS-UOW-USW-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6610-PROCESS-UOW-USW-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6610-PROCESS-UOW-USW-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-REQ-SWITCHES-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6700-DELETE-UOW-SWITCH-TSQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELETE THE TSQ USED FOR UOW SWITCH PROCESSING.
	 * *****************************************************************</pre>*/
	private void deleteUowSwitchTsq() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//             OR UBOC-UOW-REQ-SWITCHES-TSQ EQUAL LOW-VALUES
		//               GO TO 6700-DELETE-UOW-SWITCH-TSQ-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsq())
				|| Characters.EQ_LOW.test(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsqFormatted())) {
			// COB_CODE: GO TO 6700-DELETE-UOW-SWITCH-TSQ-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETEQ TS QNAME(UBOC-UOW-REQ-SWITCHES-TSQ)
		//               RESP            (WA-RESPONSE-CODE)
		//               RESP2           (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsqFormatted());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		//* SINCE THIS MODULE DOES NOT CONTROL THE CREATION OF THE TSQ,
		//* IT WILL IGNORE A QIDERR IN CASE THE APPLICATION PROCESSING
		//* FAILED TO CREATE IT OR HAS ALREADY DELETED IT.
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: STRING 'TSQ NAME= '   UBOC-UOW-REQ-SWITCHES-TSQ  ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "TSQ NAME= ",
					ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsqFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6700-DELETE-UOW-SWITCH-TSQ'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6700-DELETE-UOW-SWITCH-TSQ");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6700-DELETE-UOW-SWITCH-TSQ");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-TSQ
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowReqSwitchesTsq());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6800-DELETE-UOW-LOCK-TSQ_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELETE THE TSQ USED FOR UOW TRANSACTIONAL LOCKING.  SOME UOWS
	 *  END UP WRITING NO LOCKS EVEN THOUGH THEY HAVE SPECIFIED A LOCK
	 *  TYPE.  THEREFORE, DO NOT ERROR IF DFHRESP = QIDERR.
	 * *****************************************************************</pre>*/
	private void deleteUowLockTsq() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UTCS-NO-LOCK-ACTION
		//               GO TO 6800-DELETE-UOW-LOCK-TSQ-X
		//           END-IF.
		if (ws.getUtcsUowTransConfigInfo().getUowLockActionCd().isNoLockAction()) {
			// COB_CODE: GO TO 6800-DELETE-UOW-LOCK-TSQ-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-LOCK-PROC-TSQ EQUAL SPACES
		//             OR UBOC-UOW-LOCK-PROC-TSQ EQUAL LOW-VALUES
		//               GO TO 6800-DELETE-UOW-LOCK-TSQ-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsq())
				|| Characters.EQ_LOW.test(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted())) {
			// COB_CODE: GO TO 6800-DELETE-UOW-LOCK-TSQ-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETEQ TS QNAME(UBOC-UOW-LOCK-PROC-TSQ)
		//               RESP            (WA-RESPONSE-CODE)
		//               RESP2           (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		//* SINCE THIS MODULE DOES NOT CONTROL THE CREATION OF THE TSQ,
		//* IT WILL IGNORE A QIDERR IN CASE THE APPLICATION PROCESSING
		//* FAILED TO CREATE IT OR HAS ALREADY DELETED IT.
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: STRING 'TSQ NAME= '   UBOC-UOW-LOCK-PROC-TSQ   ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "TSQ NAME= ",
					ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsqFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6800-DELETE-UOW-LOCK-TSQ'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6800-DELETE-UOW-LOCK-TSQ");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6800-DELETE-UOW-LOCK-TSQ");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6900-CLEAR-UOW-UHDR-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  BROWSE THE UOW RESPONSE HEADER UMT TO START THE                *
	 *  PROCESS OF DELETING THOSE RECORDS.                             *
	 * *****************************************************************</pre>*/
	private void clearUowUhdrUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE MDRV-MSG-ID            TO UHDR-ID.
		ws.getHalluhdr().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE LOW-VALUES             TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(LiteralGenerator.create(Types.LOW_CHAR_VAL, Halluhdr.Len.BUS_OBJ_NM));
		// COB_CODE: SET SW-START-OF-UOW-HEADERS TO TRUE.
		ws.getSwitches().getUowHeaderFlag().setStartOfUowHeaders();
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-RESP-HEADER-STORE)
		//               RIDFLD       (UHDR-KEY)
		//               KEYLENGTH    (LENGTH OF UHDR-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Halluhdr.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6900-CLEAR-UOW-UHDR-UMT-X
		//               WHEN OTHER
		//                   GO TO 6900-CLEAR-UOW-UHDR-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
			//                               TO TRUE
			ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
			// COB_CODE: GO TO 6900-CLEAR-UOW-UHDR-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'UHDR-KEY = '  UHDR-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6900-CLEAR-UOW-UHDR-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6900-CLEAR-UOW-UHDR-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6900-CLEAR-UOW-UHDR-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6900-CLEAR-UOW-UHDR-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6910-PROCESS-UOW-UHDR-UMT
		//                   UNTIL SW-NO-MORE-UOW-HEADERS.
		while (!ws.getSwitches().getUowHeaderFlag().isNoMoreUowHeaders()) {
			processUowUhdrUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-RESP-HEADER-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'UHDR-KEY = '  UHDR-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6900-CLEAR-UOW-UHDR-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6900-CLEAR-UOW-UHDR-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6900-CLEAR-UOW-UHDR-UMT");
			// COB_CODE: MOVE 'ENDBR OF HEADER RESPONSE UMT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ENDBR OF HEADER RESPONSE UMT FAILED");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6910-PROCESS-UOW-UHDR-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ EACH RECORD IN THE UOW RESPONSE HEADER UMT AND DELETE IT. *
	 * *****************************************************************</pre>*/
	private void processUowUhdrUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-RESP-HEADER-STORE)
		//               INTO          (WS-UHDR-MSG)
		//               RIDFLD        (UHDR-KEY)
		//               KEYLENGTH     (LENGTH OF UHDR-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Halluhdr.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setKeyBytes(iRowData.getKey());
				ws.setWsUhdrMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                   GO TO 6910-PROCESS-UOW-UHDR-UMT-X
		//               WHEN OTHER
		//                   GO TO 6910-PROCESS-UOW-UHDR-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UHDR-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6910-PROCESS-UOW-UHDR-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getHalluhdr().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: IF UBOC-NBR-HDR-ROWS = WA-NBR-RESP-HDR-ROWS-READ
				//                           TO TRUE
				//           ELSE
				//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
				//           END-IF
				if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrHdrRows() == ws.getWorkArea().getWaNbrRespHdrRowsRead()) {
					// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
					//                       TO TRUE
					ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
				} else {
					// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
					//                       TO TRUE
					ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
					// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
					// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
					// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
					// COB_CODE: STRING 'UHDR-KEY = '
					//                   UHDR-KEY ';'
					//               DELIMITED BY SIZE
					//               INTO EFAL-OBJ-DATA-KEY
					//           END-STRING
					concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(),
							";");
					ws.getWsEstoInfo().getEstoDetailBuffer()
							.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
					// COB_CODE: MOVE '6910-PROCESS-UOW-UHDR-UMT'
					//                       TO EFAL-ERR-PARAGRAPH
					//                          CSC-FAILED-PARAGRAPH
					//                            OF WS-HUB-DATA
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6910-PROCESS-UOW-UHDR-UMT");
					ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6910-PROCESS-UOW-UHDR-UMT");
					// COB_CODE: MOVE 'NUM HDR ROWS IN UMT DID NOT MATCH CNT'
					//                       TO EFAL-ERR-COMMENT
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NUM HDR ROWS IN UMT DID NOT MATCH CNT");
					// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
					//                       TO EFAL-ERR-OBJECT-NAME
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
					// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
					logMainDrvrError();
				}
				// COB_CODE: GO TO 6910-PROCESS-UOW-UHDR-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: IF UBOC-NBR-HDR-ROWS = WA-NBR-RESP-HDR-ROWS-READ
			//                               TO TRUE
			//           ELSE
			//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
			//           END-IF
			if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrHdrRows() == ws.getWorkArea().getWaNbrRespHdrRowsRead()) {
				// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
				//                           TO TRUE
				ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
			} else {
				// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
				//                           TO TRUE
				ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
				// COB_CODE: STRING 'UHDR-KEY = '    UHDR-KEY ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE '6910-PROCESS-UOW-UHDR-UMT'
				//                           TO EFAL-ERR-PARAGRAPH
				//                              CSC-FAILED-PARAGRAPH
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6910-PROCESS-UOW-UHDR-UMT");
				ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6910-PROCESS-UOW-UHDR-UMT");
				// COB_CODE: MOVE 'END OF FILE IN HDR UMT - ALL RECS NOT FND.'
				//                           TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("END OF FILE IN HDR UMT - ALL RECS NOT FND.");
				// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
				//                           TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
				// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
				logMainDrvrError();
			}
			// COB_CODE: GO TO 6910-PROCESS-UOW-UHDR-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
			//                               TO TRUE
			ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UHDR-KEY = '      UHDR-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6910-PROCESS-UOW-UHDR-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6910-PROCESS-UOW-UHDR-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6910-PROCESS-UOW-UHDR-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNEXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNEXT UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6910-PROCESS-UOW-UHDR-UMT-X
			return;
		}
		// COB_CODE: ADD 1                       TO WA-NBR-RESP-HDR-ROWS-READ.
		ws.getWorkArea().setWaNbrRespHdrRowsRead(Trunc.toInt(1 + ws.getWorkArea().getWaNbrRespHdrRowsRead(), 9));
		// COB_CODE: EXEC CICS
		//               DELETE FILE (MTCS-UOW-RESP-HEADER-STORE)
		//               RIDFLD      (UHDR-KEY)
		//               KEYLENGTH   (LENGTH OF UHDR-KEY)
		//               RESP        (WA-RESPONSE-CODE)
		//               RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowDAO.delete(iRowData, Halluhdr.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6910-PROCESS-UOW-UHDR-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-HEADERS
			//                               TO TRUE
			ws.getSwitches().getUowHeaderFlag().setNoMoreUowHeaders();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'UHDR-KEY = '     UHDR-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UHDR-KEY = ", ws.getHalluhdr().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6910-PROCESS-UOW-UHDR-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6910-PROCESS-UOW-UHDR-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6910-PROCESS-UOW-UHDR-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-HEADER-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespHeaderStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6910-PROCESS-UOW-UHDR-UMT-X
			return;
		}
	}

	/**Original name: 6A00-CLEAR-UOW-UDAT-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  BROWSE THE UOW RESPONSE DATA UMT TO START THE                 *
	 *  PROCESS OF DELETING THOSE RECORDS.                            *
	 * ****************************************************************</pre>*/
	private void a00ClearUowUdatUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE MDRV-MSG-ID            TO UDAT-ID.
		ws.getHalludat().setId(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: MOVE LOW-VALUES             TO UDAT-BUS-OBJ-NM.
		ws.getHalludat().setBusObjNm(LiteralGenerator.create(Types.LOW_CHAR_VAL, Halludat.Len.BUS_OBJ_NM));
		// COB_CODE: MOVE 0                      TO UDAT-REC-SEQ.
		ws.getHalludat().setRecSeq(0);
		// COB_CODE: SET SW-START-OF-UOW-DATA    TO TRUE.
		ws.getSwitches().getUowDataFlag().setStartOfUowData();
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (MTCS-UOW-RESP-DATA-STORE)
		//               RIDFLD       (UDAT-KEY)
		//               KEYLENGTH    (LENGTH OF UDAT-KEY)
		//               GTEQ
		//               RESP         (WA-RESPONSE-CODE)
		//               RESP2        (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Halludat.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 6A00-CLEAR-UOW-UDAT-UMT-X
		//               WHEN OTHER
		//                   GO TO 6A00-CLEAR-UOW-UDAT-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET SW-NO-MORE-UOW-DATA
			//                               TO TRUE
			ws.getSwitches().getUowDataFlag().setNoMoreUowData();
			// COB_CODE: GO TO 6A00-CLEAR-UOW-UDAT-UMT-X
			return;
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'UDAT-KEY = '      UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6A00-CLEAR-UOW-UDAT-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A00-CLEAR-UOW-UDAT-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A00-CLEAR-UOW-UDAT-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6A00-CLEAR-UOW-UDAT-UMT-X
			return;
		}
		// COB_CODE: PERFORM 6A10-PROCESS-UOW-UDAT-UMT
		//             UNTIL SW-NO-MORE-UOW-DATA.
		while (!ws.getSwitches().getUowDataFlag().isNoMoreUowData()) {
			a10ProcessUowUdatUmt();
		}
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (MTCS-UOW-RESP-DATA-STORE)
		//               RESP       (WA-RESPONSE-CODE)
		//               RESP2      (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-MAIN-DRVR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: STRING 'UDAT-KEY = '  UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6A00-CLEAR-UOW-UDAT-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A00-CLEAR-UOW-UDAT-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A00-CLEAR-UOW-UDAT-UMT");
			// COB_CODE: MOVE 'ENDBR OF RESPONSE DATA UMT FAILED'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ENDBR OF RESPONSE DATA UMT FAILED");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
		}
	}

	/**Original name: 6A10-PROCESS-UOW-UDAT-UMT_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  READ EACH RECORD IN THE UOW RESPONSE DATA UMT AND DELETE IT. *
	 * ***************************************************************</pre>*/
	private void a10ProcessUowUdatUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (MTCS-UOW-RESP-DATA-STORE)
		//               INTO          (WS-UDAT-MSG)
		//               RIDFLD        (UDAT-KEY)
		//               KEYLENGTH     (LENGTH OF UDAT-KEY)
		//               RESP          (WA-RESPONSE-CODE)
		//               RESP2         (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Halludat.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalludat().setKeyBytes(iRowData.getKey());
				ws.setWsUdatMsgBytes(iRowData.getData());
			}
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN DFHRESP(ENDFILE)
		//                   GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
		//               WHEN OTHER
		//                   GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF UDAT-ID NOT EQUAL MDRV-MSG-ID
			//               GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
			//           ELSE
			//               CONTINUE
			//           END-IF
			if (!Conditions.eq(ws.getHalludat().getId(), ws.getHallmdrv().getDriverInfo().getMsgId())) {
				// COB_CODE: IF UBOC-NBR-DATA-ROWS
				//             EQUAL WA-NBR-RESP-DATA-ROWS-READ
				//                           TO TRUE
				//           ELSE
				//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
				//           END-IF
				if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrDataRows() == ws.getWorkArea().getWaNbrRespDataRowsRead()) {
					// COB_CODE: SET SW-NO-MORE-UOW-DATA
					//                       TO TRUE
					ws.getSwitches().getUowDataFlag().setNoMoreUowData();
				} else {
					// COB_CODE: SET SW-NO-MORE-UOW-DATA
					//                       TO TRUE
					ws.getSwitches().getUowDataFlag().setNoMoreUowData();
					// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
					// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
					// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
					//                       TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
					// COB_CODE: STRING 'UDAT-KEY = '
					//                   UDAT-KEY ';'
					//               DELIMITED BY SIZE
					//               INTO EFAL-OBJ-DATA-KEY
					//           END-STRING
					concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(),
							";");
					ws.getWsEstoInfo().getEstoDetailBuffer()
							.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
					// COB_CODE: MOVE '6A10-PROCESS-UOW-UDAT-UMT'
					//                       TO EFAL-ERR-PARAGRAPH
					//                          CSC-FAILED-PARAGRAPH
					//                            OF WS-HUB-DATA
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A10-PROCESS-UOW-UDAT-UMT");
					ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A10-PROCESS-UOW-UDAT-UMT");
					// COB_CODE: MOVE 'NUM DATA ROWS IN UMT DID NOT MATCH CNT'
					//                       TO EFAL-ERR-COMMENT
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("NUM DATA ROWS IN UMT DID NOT MATCH CNT");
					// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
					//                       TO EFAL-ERR-OBJECT-NAME
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
					// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
					logMainDrvrError();
				}
				// COB_CODE: GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
				return;
			} else {
				// COB_CODE: CONTINUE
				//continue
			}
		} else if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.ENDFILE) {
			// COB_CODE: IF UBOC-NBR-DATA-ROWS = WA-NBR-RESP-DATA-ROWS-READ
			//                               TO TRUE
			//           ELSE
			//               PERFORM 9000-LOG-MAIN-DRVR-ERROR
			//           END-IF
			if (ws.getWsHubData().getUbocRecord().getCommInfo().getUbocNbrDataRows() == ws.getWorkArea().getWaNbrRespDataRowsRead()) {
				// COB_CODE: SET SW-NO-MORE-UOW-DATA
				//                           TO TRUE
				ws.getSwitches().getUowDataFlag().setNoMoreUowData();
			} else {
				// COB_CODE: SET SW-NO-MORE-UOW-DATA
				//                           TO TRUE
				ws.getSwitches().getUowDataFlag().setNoMoreUowData();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-BUS-PROCESS-FAILED
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
				// COB_CODE: SET BUSP-INVALID-UMT-REC-CNT
				//                           TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidUmtRecCnt();
				// COB_CODE: STRING 'UDAT-KEY = '    UDAT-KEY ';'
				//               DELIMITED BY SIZE
				//               INTO EFAL-OBJ-DATA-KEY
				//           END-STRING
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: MOVE '6A10-PROCESS-UOW-UDAT-UMT'
				//                           TO EFAL-ERR-PARAGRAPH
				//                              CSC-FAILED-PARAGRAPH
				//                                OF WS-HUB-DATA
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A10-PROCESS-UOW-UDAT-UMT");
				ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A10-PROCESS-UOW-UDAT-UMT");
				// COB_CODE: MOVE 'END OF FILE IN DAT UMT - ALL RECS NOT FND.'
				//                           TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("END OF FILE IN DAT UMT - ALL RECS NOT FND.");
				// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
				//                           TO EFAL-ERR-OBJECT-NAME
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
				// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
				logMainDrvrError();
			}
			// COB_CODE: GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
			return;
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-DATA
			//                               TO TRUE
			ws.getSwitches().getUowDataFlag().setNoMoreUowData();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: STRING 'UDAT-KEY = '      UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6A10-PROCESS-UOW-UDAT-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A10-PROCESS-UOW-UDAT-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A10-PROCESS-UOW-UDAT-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
			return;
		}
		// COB_CODE: ADD 1                       TO WA-NBR-RESP-DATA-ROWS-READ.
		ws.getWorkArea().setWaNbrRespDataRowsRead(Trunc.toInt(1 + ws.getWorkArea().getWaNbrRespDataRowsRead(), 9));
		// COB_CODE: EXEC CICS
		//               DELETE FILE (MTCS-UOW-RESP-DATA-STORE)
		//               RIDFLD      (UDAT-KEY)
		//               KEYLENGTH   (LENGTH OF UDAT-KEY)
		//               RESP        (WA-RESPONSE-CODE)
		//               RESP2       (WA-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, ws.getMtcsMsgTransportInfo().getUowRespDataStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalludat().getKeyBytes());
			iRowDAO.delete(iRowData, Halludat.Len.KEY);
		}
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WA-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET SW-NO-MORE-UOW-DATA
			//                               TO TRUE
			ws.getSwitches().getUowDataFlag().setNoMoreUowData();
			// COB_CODE: SET EFAL-SYSTEM-ERROR
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-UMT
			//                               TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteUmt();
			// COB_CODE: STRING 'UDAT-KEY = '     UDAT-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "UDAT-KEY = ", ws.getHalludat().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '6A10-PROCESS-UOW-UDAT-UMT'
			//                               TO EFAL-ERR-PARAGRAPH
			//                                  CSC-FAILED-PARAGRAPH
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("6A10-PROCESS-UOW-UDAT-UMT");
			ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("6A10-PROCESS-UOW-UDAT-UMT");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE UMT'
			//                               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE UMT");
			// COB_CODE: IF WA-RESPONSE-CODE < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO EFAL-CICS-ERR-RESP
			//                                  CSC-EIBRESP-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: IF WA-RESPONSE-CODE2 < ZERO
			//               MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           ELSE
			//               MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
			//           END-IF
			if (ws.getWorkArea().getWaResponseCode2() < 0) {
				// COB_CODE: MOVE '-'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			} else {
				// COB_CODE: MOVE '+'        TO EFAL-CICS-ERR-RESP2-SIGN
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			}
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO EFAL-CICS-ERR-RESP2
			//                                  CSC-EIBRESP2-DISPLAY
			//                                    OF WS-HUB-DATA
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWorkArea().getWaResponseCode2(), 10));
			ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE MTCS-UOW-RESP-DATA-STORE
			//                               TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getMtcsMsgTransportInfo().getUowRespDataStore());
			// COB_CODE: PERFORM 9000-LOG-MAIN-DRVR-ERROR
			logMainDrvrError();
			// COB_CODE: GO TO 6A10-PROCESS-UOW-UDAT-UMT-X
			return;
		}
	}

	/**Original name: 9000-LOG-MAIN-DRVR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*********************************************************
	 *  SET UP THE INFORMATION NEEDED, THEN CALL THE ERROR     *
	 *  HANDLER PROGRAM.                                       *
	 * *********************************************************</pre>*/
	private void logMainDrvrError() {
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE MDRV-MSG-ID            TO ESTO-STORE-ID
		//                                          EFAL-FAIL-LVL-GUID.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(ws.getHallmdrv().getDriverInfo().getMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(ws.getHallmdrv().getDriverInfo().getMsgId());
		// COB_CODE: SET EFAL-S3-SAVARCH OF WS-ESTO-INFO
		//                                       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		// COB_CODE: SET EFAL-MAINFRAME          TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: MOVE WA-PROGRAM-NAME        TO EFAL-FAILED-MODULE
		//                                          CSC-FAILED-MODULE
		//                                            OF WS-HUB-DATA
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWorkArea().getWaProgramName());
		ws.getWsHubData().getCommunicationShellCommon().setCscFailedModule(ws.getWorkArea().getWaProgramName());
		// COB_CODE: MOVE MDRV-UNIT-OF-WORK      TO EFAL-UNIT-OF-WORK.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(ws.getHallmdrv().getDriverInfo().getUnitOfWork());
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(WA-APPLID)
		//           END-EXEC.
		ws.getWorkArea().setWaApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WA-APPLID              TO EFAL-FAILED-LOCATION-ID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWorkArea().getWaApplid());
		// COB_CODE: MOVE MDRV-AUTH-USERID       TO EFAL-LOGON-USERID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(ws.getHallmdrv().getDriverInfo().getAuthUserid());
		// COB_CODE: MOVE COM-SEC-SYS-ID         TO EFAL-SEC-SYS-ID.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE '+'                    TO EFAL-SEC-SYS-ID-SIGN.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		// COB_CODE: SET MDRV-UOW-LOGGABLE-ERRS  TO TRUE.
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setLoggableErrs();
		// COB_CODE: SET MDRV-PERFORM-ROLLBACK   TO TRUE.
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setRollback();
		// COB_CODE: SET SW-ERROR-WARN-IN-MCM    TO TRUE.
		ws.getSwitches().setErrorOccuredFlag(true);
		// COB_CODE: SET DSD-FATAL-ERROR-CODE    TO TRUE.
		dfhcommarea.getDriverSpecificData().getErrorReturnCode().setFatalErrorCode();
		// COB_CODE: MOVE CSC-FAILED-MODULE OF WS-HUB-DATA
		//                                       TO EA-01-FAILED-MODULE.
		ws.getEa01FatalErrorMsg().setFailedModule(ws.getWsHubData().getCommunicationShellCommon().getCscFailedModule());
		// COB_CODE: MOVE CSC-FAILED-PARAGRAPH OF WS-HUB-DATA
		//                                       TO EA-01-FAILED-PARAGRAPH.
		ws.getEa01FatalErrorMsg().setFailedParagraph(ws.getWsHubData().getCommunicationShellCommon().getCscFailedParagraph());
		// COB_CODE: MOVE CSC-SQLCODE-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-SQLCODE-DISPLAY.
		ws.getEa01FatalErrorMsg().setSqlcodeDisplay(ws.getWsHubData().getCommunicationShellCommon().getCscSqlcodeDisplay());
		// COB_CODE: MOVE CSC-EIBRESP-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-EIBRESP-DISPLAY.
		ws.getEa01FatalErrorMsg().setEibrespDisplay(ws.getWsHubData().getCommunicationShellCommon().getCscEibrespDisplay());
		// COB_CODE: MOVE CSC-EIBRESP2-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-EIBRESP2-DISPLAY.
		ws.getEa01FatalErrorMsg().setEibresp2Display(ws.getWsHubData().getCommunicationShellCommon().getCscEibresp2Display());
		// COB_CODE: MOVE EA-01-FATAL-ERROR-MSG  TO DSD-FATAL-ERROR-MESSAGE.
		dfhcommarea.getDriverSpecificData().setFatalErrorMessage(ws.getEa01FatalErrorMsg().getEa01FatalErrorMsgFormatted());
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  ('HALOESTO')
		//               COMMAREA (WS-ESTO-INFO)
		//               LENGTH   (LENGTH OF WS-ESTO-INFO)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("TS020000", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
	}

	/**Original name: 9100-COPY-UBOC-ERROR-DET_FIRST_SENTENCES<br>
	 * <pre>**********************************************************
	 *  COPY UBOC ERROR DETAILS TO MDRV COMMAREA IF AN ERROR    *
	 *  HAS NOT ALREADY HAPPENED IN MCM                         *
	 * **********************************************************</pre>*/
	private void copyUbocErrorDet() {
		// COB_CODE: IF SW-ERROR-WARN-IN-MCM
		//               GO TO 9100-COPY-UBOC-ERROR-DET-X
		//           END-IF.
		if (ws.getSwitches().isErrorOccuredFlag()) {
			// COB_CODE: GO TO 9100-COPY-UBOC-ERROR-DET-X
			return;
		}
		// COB_CODE: MOVE UBOC-OBJECT-LOGGABLE-PROBLEMS
		//                                       TO MDRV-UOW-LOGGABLE-PROBLEMS.
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setUowLoggableProblems(ws.getWsHubData().getUbocRecord().getCommInfo()
				.getUbocErrorDetails().getUbocObjectLoggableProblems().getUbocObjectLoggableProblems());
		// COB_CODE: MOVE UBOC-ERROR-LOGGING-INFO
		//                                       TO MDRV-ERROR-LOGGING-INFO.
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrorLoggingInfoBytes(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getUbocErrorLoggingInfoBytes());
		// COB_CODE: MOVE UBOC-ERROR-CODE        TO MDRV-UNIT-OF-WORK-ERRCODE.
		ws.getHallmdrv().getDriverInfo()
				.setUnitOfWorkErrcode(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getErrorCode());
		// COB_CODE: MOVE UBOC-FAILED-PARAGRAPH  TO MDRV-FAILED-PARAGRAPH
		//                                          CSC-FAILED-PARAGRAPH
		//                                            OF WS-HUB-DATA.
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedParagraph(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getFailedParagraph());
		ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getFailedParagraph());
		// COB_CODE: MOVE UBOC-SQLCODE-DISPLAY   TO MDRV-SQLCODE-DISPLAY
		//                                          CSC-SQLCODE-DISPLAY
		//                                            OF WS-HUB-DATA.
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setSqlcodeDisplay(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getSqlcodeDisplay());
		ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplay(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getSqlcodeDisplay());
		// COB_CODE: MOVE UBOC-EIBRESP-DISPLAY   TO MDRV-EIBRESP-DISPLAY
		//                                          CSC-EIBRESP-DISPLAY
		//                                            OF WS-HUB-DATA.
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibrespDisplay(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getEibrespDisplay());
		ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplay(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getEibrespDisplay());
		// COB_CODE: MOVE UBOC-EIBRESP2-DISPLAY  TO MDRV-EIBRESP2-DISPLAY
		//                                          CSC-EIBRESP2-DISPLAY
		//                                            OF WS-HUB-DATA.
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibresp2Display(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getEibresp2Display());
		ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2Display(
				ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getEibresp2Display());
		// COB_CODE: MOVE UBOC-FAILED-MODULE     TO MDRV-FAILED-MODULE
		//                                          CSC-FAILED-MODULE
		//                                            OF WS-HUB-DATA.
		ws.getHallmdrv().getDriverInfo().getErrorDetails()
				.setFailedModule(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getFailedModule());
		ws.getWsHubData().getCommunicationShellCommon()
				.setCscFailedModule(ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().getFailedModule());
		// COB_CODE: MOVE CSC-FAILED-MODULE OF WS-HUB-DATA
		//                                       TO EA-01-FAILED-MODULE.
		ws.getEa01FatalErrorMsg().setFailedModule(ws.getWsHubData().getCommunicationShellCommon().getCscFailedModule());
		// COB_CODE: MOVE CSC-FAILED-PARAGRAPH OF WS-HUB-DATA
		//                                       TO EA-01-FAILED-PARAGRAPH.
		ws.getEa01FatalErrorMsg().setFailedParagraph(ws.getWsHubData().getCommunicationShellCommon().getCscFailedParagraph());
		// COB_CODE: MOVE CSC-SQLCODE-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-SQLCODE-DISPLAY.
		ws.getEa01FatalErrorMsg().setSqlcodeDisplay(ws.getWsHubData().getCommunicationShellCommon().getCscSqlcodeDisplay());
		// COB_CODE: MOVE CSC-EIBRESP-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-EIBRESP-DISPLAY.
		ws.getEa01FatalErrorMsg().setEibrespDisplay(ws.getWsHubData().getCommunicationShellCommon().getCscEibrespDisplay());
		// COB_CODE: MOVE CSC-EIBRESP2-DISPLAY OF WS-HUB-DATA
		//                                       TO EA-01-EIBRESP2-DISPLAY.
		ws.getEa01FatalErrorMsg().setEibresp2Display(ws.getWsHubData().getCommunicationShellCommon().getCscEibresp2Display());
		// COB_CODE: MOVE EA-01-FATAL-ERROR-MSG  TO DSD-FATAL-ERROR-MESSAGE.
		dfhcommarea.getDriverSpecificData().setFatalErrorMessage(ws.getEa01FatalErrorMsg().getEa01FatalErrorMsgFormatted());
		// COB_CODE: SET DSD-FATAL-ERROR-CODE    TO TRUE.
		dfhcommarea.getDriverSpecificData().getErrorReturnCode().setFatalErrorCode();
	}

	/**Original name: 9300-START-HMEL-TRAN_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM START 'HMEL' TRANSACTION IF AN LOGGABLE ERROR /WARN  *
	 *  HAS OCCURED.                                                 *
	 * ***************************************************************</pre>*/
	private void startHmelTran() {
		TpOutputData tpQueueData = null;
		// COB_CODE: EXEC CICS START
		//               TRANSID  ('HMEL')
		//               FROM     (MDRV-DRIVER-INFO)
		//               LENGTH   (LENGTH OF MDRV-DRIVER-INFO)
		//               RESP     (WA-RESPONSE-CODE)
		//               RESP2    (WA-RESPONSE-CODE2)
		//           END-EXEC.
		tpQueueData = new TpOutputData();
		tpQueueData.setDataLen(MdrvDriverInfo.Len.DRIVER_INFO);
		tpQueueData.setData(ws.getHallmdrv().getDriverInfo().getDriverInfoBytes());
		TpSession.current().newTask("HMEL").execContext(execContext).parmData(tpQueueData).startCode("SD").execute();
		ws.getWorkArea().setWaResponseCode(execContext.getResp());
		ws.getWorkArea().setWaResponseCode2(execContext.getResp2());
		// COB_CODE:      EVALUATE WA-RESPONSE-CODE
		//                    WHEN DFHRESP(NORMAL)
		//                        CONTINUE
		//                    WHEN OTHER
		//           * IF HALOESTO LINK ERROR THEN RECORD
		//           * THE DETAILS IN MAINDRIVER COMMAREA
		//                        GO TO 9300-START-HMEL-TRAN-X
		//                END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWorkArea().getWaResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// IF HALOESTO LINK ERROR THEN RECORD
			// THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET MDRV-ERR-LOGGING-FAILED
			//                               TO TRUE
			ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET MDRV-WITHIN-MDRV
			//                               TO TRUE
			ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorLoggingLvlSw().setWithinMdrv();
			// COB_CODE: MOVE WA-RESPONSE-CODE
			//                               TO MDRV-ERR-LOG-EIBRESP-DSPLY
			ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibrespDsply(ws.getWorkArea().getWaResponseCode());
			// COB_CODE: MOVE WA-RESPONSE-CODE2
			//                               TO MDRV-ERR-LOG-EIBRESP2-DSPLY
			ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibresp2Dsply(ws.getWorkArea().getWaResponseCode2());
			// COB_CODE: MOVE ZERO           TO MDRV-ERR-LOG-SQLCODE-DSPLY
			ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogSqlcodeDsply(0);
			// COB_CODE: GO TO 9300-START-HMEL-TRAN-X
			return;
		}
	}

	/**Original name: 9901-PROCESS-SYNC_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM COMMIT OR ROLLBACK IF REQUIRED BY THIS MODULE.
	 * ***************************************************************</pre>*/
	private void processSync() {
		// COB_CODE: IF MDRV-PERFORM-ROLLBACK
		//               END-EXEC
		//           ELSE
		//               END-EXEC
		//           END-IF.
		if (ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().isRollback()) {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT ROLLBACK
			//           END-EXEC
			TpSession.current().syncpoint(false);
		} else {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT
			//           END-EXEC
			TpSession.current().syncpoint(true);
		}
	}

	/**Original name: 9902-PROCESS-ELAPSED-TIME_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CALCULATE THE ELASPED TIME FOR THE TRANSACTION.
	 *  NOTE:  CALCULATIONS ARE CURRENTLY IN HUNDRETHS NOT MILLISEC
	 * ***************************************************************</pre>*/
	private void processElapsedTime() {
		// COB_CODE: ACCEPT TE-TRAN-END-TIME FROM TIME.
		ws.getTimeElapsedWork().setEndTimeFormatted(CalendarUtil.getTimeHHMMSSMM());
		// COB_CODE: COMPUTE TE-TRAN-END-TIME-HTH =
		//                   (TE-TRAN-HOURS-END * 60 * 60 * 100) +
		//                   (TE-TRAN-MINS-END * 60 * 100) +
		//                   (TE-TRAN-SECS-END * 100) +
		//                   TE-TRAN-HTHS-END.
		ws.getTimeElapsedWork()
				.setEndTimeHth(Trunc.toInt(ws.getTimeElapsedWork().getHoursEnd() * 60 * 60 * 100 + ws.getTimeElapsedWork().getMinsEnd() * 60 * 100
						+ ws.getTimeElapsedWork().getSecsEnd() * 100 + ws.getTimeElapsedWork().getHthsEnd(), 8));
		// COB_CODE: IF TE-TRAN-END-TIME-HTH >= TE-TRAN-START-TIME-HTH
		//                 GIVING TE-TRANS-ELAPSED-TIME-AMT
		//           ELSE
		//               (8640000 - TE-TRAN-START-TIME-HTH) + TE-TRAN-END-TIME-HTH
		//           END-IF.
		if (ws.getTimeElapsedWork().getEndTimeHth() >= ws.getTimeElapsedWork().getStartTimeHth()) {
			// COB_CODE: SUBTRACT TE-TRAN-START-TIME-HTH
			//             FROM   TE-TRAN-END-TIME-HTH
			//             GIVING TE-TRANS-ELAPSED-TIME-AMT
			ws.getTimeElapsedWork()
					.setsElapsedTimeAmt(Trunc.toInt(abs(ws.getTimeElapsedWork().getEndTimeHth() - ws.getTimeElapsedWork().getStartTimeHth()), 8));
		} else {
			// COB_CODE: COMPUTE TE-TRANS-ELAPSED-TIME-AMT =
			//           (8640000 - TE-TRAN-START-TIME-HTH) + TE-TRAN-END-TIME-HTH
			ws.getTimeElapsedWork().setsElapsedTimeAmt(
					Trunc.toInt(abs(8640000 - ws.getTimeElapsedWork().getStartTimeHth() + ws.getTimeElapsedWork().getEndTimeHth()), 8));
		}
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initUbocRecord() {
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowName("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMsgId("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocAuthUserid("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocAuthUserClientid("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocTerminalId("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocPrimaryBusObj("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocStoreTypeCd().setUbocStoreTypeCd(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMdrReqStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocMdrRspStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqMsgStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqSwitchesStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespHeaderStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespDataStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespWarningsStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowKeyReplaceStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowRespNlBlErrsStore("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocSessionId("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowLockProcTsq("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowReqSwitchesTsq("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocPassThruAction().setUbocPassThruAction("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrHdrRowsFormatted("000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrDataRowsFormatted("000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrWarningsFormatted("000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocNbrNonlogBlErrsFormatted("000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocApplicationTsSw().setUbocApplicationTsSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocApplicationTs("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUowLockStrategyCd().setUbocUowLockStrategyCd(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocUowLockTimeoutInterval(0);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocUserInLockGrpSw().setUbocUserInLockGrpSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocLockGroup("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setFiller3("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getApplyDataPrivacySw().setApplyDataPrivacySw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().setSecurityContext("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getSecAutNbr().setUbocSecAutNbrFormatted("00000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().setSecAssociationType("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setUbocDataPrivRetCode("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setUbocSecGroupName("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setFiller4("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().getApplyAuditsSw().setApplyAuditsSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().setAudtBusObjNm("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocAuditProcessingInfo().setAudtEventData("");
		ws.getWsHubData().getUbocRecord().getCommInfo().setFiller5("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocKeepClearUowStorageSw().setUbocKeepClearUowStorageSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems()
				.setUbocObjectLoggableProblems(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setUbocProcessingStatusSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().setFiller6("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setErrorCode("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocSqlcodeDisplayFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibrespDisplayFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setUbocEibresp2DisplayFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw()
				.setUbocLoggableErrLogOnlySw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().setFiller7("");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw()
				.setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw()
				.setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
				.setUbocErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
				.setUbocErrLogEibrespDsplyFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
				.setUbocErrLogEibresp2DsplyFormatted("0000000000");
		ws.getWsHubData().getUbocRecord().getCommInfo().getUbocErrorDetails().setFiller8("");
		ws.getWsHubData().getUbocRecord().setAppDataBufferLengthFormatted("0000");
		ws.getWsHubData().getUbocRecord().setAppDataBuffer("");
	}

	public void initCscErrorDetails() {
		ws.getWsHubData().getCommunicationShellCommon().setCscFailedModule("");
		ws.getWsHubData().getCommunicationShellCommon().setCscFailedParagraph("");
		ws.getWsHubData().getCommunicationShellCommon().setCscSqlcodeDisplayFormatted("0000000000");
		ws.getWsHubData().getCommunicationShellCommon().setCscEibrespDisplayFormatted("0000000000");
		ws.getWsHubData().getCommunicationShellCommon().setCscEibresp2DisplayFormatted("0000000000");
	}

	public void initDriverInfo() {
		ws.getHallmdrv().getDriverInfo().setMsgId("");
		ws.getHallmdrv().getDriverInfo().setMsgRecSeqFormatted("000");
		ws.getHallmdrv().getDriverInfo().getMessageTransferCd().setMessageTransferCd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setUnitOfWork("");
		ws.getHallmdrv().getDriverInfo().setAuthUserid("");
		ws.getHallmdrv().getDriverInfo().setTerminalId("");
		ws.getHallmdrv().getDriverInfo().getDeleteUowStore().setDeleteUowStore(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getReturnWarningsInd().setReturnWarningsInd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setTotalLengthFormatted("000000000");
		ws.getHallmdrv().getDriverInfo().setNumChunksFormatted("000");
		ws.getHallmdrv().getDriverInfo().getCommitRollbackInd().setCommitRollbackInd(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getMaindrvLoggableProblems().setMaindrvLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setMainDrvrErrcode("");
		ws.getHallmdrv().getDriverInfo().getUowLoggableProblems().setUowLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setUnitOfWorkErrcode("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedModule("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedParagraph("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setSqlcodeDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibrespDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibresp2DisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorsLoggedSw().setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorLoggingLvlSw().setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibrespDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibresp2DsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().setSessionId("");
		ws.getHallmdrv().getDriverInfo().getLoggableErrLogOnlySw().setLoggableErrLogOnlySw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getSyncpointLocation().setSyncpointLocation(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getRequestMsgProcessing().setRequestMsgProcessing(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getResponseMsgProcessing().setResponseMsgProcessing(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getMsgtranLoggableProblems().setMsgtranLoggableProblems(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setMsgtranErrcode("");
		ws.getHallmdrv().getDriverInfo().getTransElapsedTimeSw().setTransElapsedTimeSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().setTransElapsedTimeAmtFormatted("00000000");
	}

	public void initErrorDetails() {
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedModule("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setFailedParagraph("");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setSqlcodeDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibrespDisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setEibresp2DisplayFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorsLoggedSw().setErrorsLoggedSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().getErrorLoggingLvlSw().setErrorLoggingLvlSw(Types.SPACE_CHAR);
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogSqlcodeDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibrespDsplyFormatted("0000000000");
		ws.getHallmdrv().getDriverInfo().getErrorDetails().setErrLogEibresp2DsplyFormatted("0000000000");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void registerListeners() {
		ws.getHallmdrv().getDriverInfo().getUowBufferLength().addListener(ws.getHallmdrv().getMdrvUowMessageByteListener());
	}
}
