/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IStCncWrdRqr;

/**Original name: DCLST-CNC-WRD-RQR<br>
 * Variable: DCLST-CNC-WRD-RQR from copybook XZH00013<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclstCncWrdRqr implements IStCncWrdRqr {

	//==== PROPERTIES ====
	//Original name: ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: POL-COV-CD
	private String polCovCd = DefaultValues.stringVal(Len.POL_COV_CD);
	//Original name: ST-WRD-SEQ-CD
	private String stWrdSeqCd = DefaultValues.stringVal(Len.ST_WRD_SEQ_CD);
	//Original name: ST-WRD-EFF-DT
	private String stWrdEffDt = DefaultValues.stringVal(Len.ST_WRD_EFF_DT);
	//Original name: ST-WRD-EXP-DT
	private String stWrdExpDt = DefaultValues.stringVal(Len.ST_WRD_EXP_DT);

	//==== METHODS ====
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setPolCovCd(String polCovCd) {
		this.polCovCd = Functions.subString(polCovCd, Len.POL_COV_CD);
	}

	public String getPolCovCd() {
		return this.polCovCd;
	}

	@Override
	public void setStWrdSeqCd(String stWrdSeqCd) {
		this.stWrdSeqCd = Functions.subString(stWrdSeqCd, Len.ST_WRD_SEQ_CD);
	}

	@Override
	public String getStWrdSeqCd() {
		return this.stWrdSeqCd;
	}

	public String getStWrdSeqCdFormatted() {
		return Functions.padBlanks(getStWrdSeqCd(), Len.ST_WRD_SEQ_CD);
	}

	public void setStWrdEffDt(String stWrdEffDt) {
		this.stWrdEffDt = Functions.subString(stWrdEffDt, Len.ST_WRD_EFF_DT);
	}

	public String getStWrdEffDt() {
		return this.stWrdEffDt;
	}

	public void setStWrdExpDt(String stWrdExpDt) {
		this.stWrdExpDt = Functions.subString(stWrdExpDt, Len.ST_WRD_EXP_DT);
	}

	public String getStWrdExpDt() {
		return this.stWrdExpDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ST_WRD_SEQ_CD = 5;
		public static final int ST_ABB = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int POL_COV_CD = 4;
		public static final int ST_WRD_EFF_DT = 10;
		public static final int ST_WRD_EXP_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
