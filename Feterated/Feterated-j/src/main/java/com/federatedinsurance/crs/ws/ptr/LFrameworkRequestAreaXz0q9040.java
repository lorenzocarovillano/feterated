/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9040<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9040 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9040() {
	}

	public LFrameworkRequestAreaXz0q9040(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza940qMaxFrmRows(short xza940qMaxFrmRows) {
		writeBinaryShort(Pos.XZA940_MAX_FRM_ROWS, xza940qMaxFrmRows);
	}

	/**Original name: XZA940Q-MAX-FRM-ROWS<br>*/
	public short getXza940qMaxFrmRows() {
		return readBinaryShort(Pos.XZA940_MAX_FRM_ROWS);
	}

	public void setXza940qCsrActNbr(String xza940qCsrActNbr) {
		writeString(Pos.XZA940_CSR_ACT_NBR, xza940qCsrActNbr, Len.XZA940_CSR_ACT_NBR);
	}

	/**Original name: XZA940Q-CSR-ACT-NBR<br>*/
	public String getXza940qCsrActNbr() {
		return readString(Pos.XZA940_CSR_ACT_NBR, Len.XZA940_CSR_ACT_NBR);
	}

	public void setXza940qNotPrcTs(String xza940qNotPrcTs) {
		writeString(Pos.XZA940_NOT_PRC_TS, xza940qNotPrcTs, Len.XZA940_NOT_PRC_TS);
	}

	/**Original name: XZA940Q-NOT-PRC-TS<br>*/
	public String getXza940qNotPrcTs() {
		return readString(Pos.XZA940_NOT_PRC_TS, Len.XZA940_NOT_PRC_TS);
	}

	public void setXza940qPiStartPoint(long xza940qPiStartPoint) {
		writeLong(Pos.XZA940_PI_START_POINT, xza940qPiStartPoint, Len.Int.XZA940Q_PI_START_POINT);
	}

	/**Original name: XZA940Q-PI-START-POINT<br>*/
	public long getXza940qPiStartPoint() {
		return readNumDispLong(Pos.XZA940_PI_START_POINT, Len.XZA940_PI_START_POINT);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9040 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA940_GET_FRM_LIST_KEY = L_FW_REQ_XZ0A9040;
		public static final int XZA940_MAX_FRM_ROWS = XZA940_GET_FRM_LIST_KEY;
		public static final int XZA940_CSR_ACT_NBR = XZA940_MAX_FRM_ROWS + Len.XZA940_MAX_FRM_ROWS;
		public static final int XZA940_NOT_PRC_TS = XZA940_CSR_ACT_NBR + Len.XZA940_CSR_ACT_NBR;
		public static final int XZA940_PAGING_INPUTS = XZA940_NOT_PRC_TS + Len.XZA940_NOT_PRC_TS;
		public static final int XZA940_PI_START_POINT = XZA940_PAGING_INPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA940_MAX_FRM_ROWS = 2;
		public static final int XZA940_CSR_ACT_NBR = 9;
		public static final int XZA940_NOT_PRC_TS = 26;
		public static final int XZA940_PI_START_POINT = 10;
		public static final int XZA940_PAGING_INPUTS = XZA940_PI_START_POINT;
		public static final int XZA940_GET_FRM_LIST_KEY = XZA940_MAX_FRM_ROWS + XZA940_CSR_ACT_NBR + XZA940_NOT_PRC_TS + XZA940_PAGING_INPUTS;
		public static final int L_FW_REQ_XZ0A9040 = XZA940_GET_FRM_LIST_KEY;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9040;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA940Q_PI_START_POINT = 10;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
