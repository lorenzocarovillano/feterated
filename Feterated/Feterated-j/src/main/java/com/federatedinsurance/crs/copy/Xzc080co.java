/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Xz08coAddlItsInfo;

/**Original name: XZC080CO<br>
 * Variable: XZC080CO from copybook XZC080CO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc080co {

	//==== PROPERTIES ====
	public static final int ERR_MSG_MAXOCCURS = 10;
	public static final int WNG_MSG_MAXOCCURS = 10;
	public static final int ADDL_ITS_INFO_MAXOCCURS = 250;
	//Original name: XZC08O-RET-CD
	private Xz08coRetCd retCd = new Xz08coRetCd();
	//Original name: XZC08O-ERR-MSG
	private String[] errMsg = new String[ERR_MSG_MAXOCCURS];
	//Original name: XZC08O-WNG-MSG
	private String[] wngMsg = new String[WNG_MSG_MAXOCCURS];
	//Original name: XZC08O-ADDL-ITS-INFO
	private Xz08coAddlItsInfo[] addlItsInfo = new Xz08coAddlItsInfo[ADDL_ITS_INFO_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xzc080co() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int errMsgIdx = 1; errMsgIdx <= ERR_MSG_MAXOCCURS; errMsgIdx++) {
			setErrMsg(errMsgIdx, DefaultValues.stringVal(Len.ERR_MSG));
		}
		for (int wngMsgIdx = 1; wngMsgIdx <= WNG_MSG_MAXOCCURS; wngMsgIdx++) {
			setWngMsg(wngMsgIdx, DefaultValues.stringVal(Len.WNG_MSG));
		}
		for (int addlItsInfoIdx = 1; addlItsInfoIdx <= ADDL_ITS_INFO_MAXOCCURS; addlItsInfoIdx++) {
			addlItsInfo[addlItsInfoIdx - 1] = new Xz08coAddlItsInfo();
		}
	}

	public void setServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setRetMsgBytes(buffer, position);
		position += Len.RET_MSG;
		for (int idx = 1; idx <= ADDL_ITS_INFO_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				addlItsInfo[idx - 1].setXz08coAddlItsInfoBytes(buffer, position);
				position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			} else {
				addlItsInfo[idx - 1].initXz08coAddlItsInfoSpaces();
				position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			}
		}
	}

	public byte[] getServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getRetMsgBytes(buffer, position);
		position += Len.RET_MSG;
		for (int idx = 1; idx <= ADDL_ITS_INFO_MAXOCCURS; idx++) {
			addlItsInfo[idx - 1].getXz08coAddlItsInfoBytes(buffer, position);
			position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
		}
		return buffer;
	}

	public void setRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		retCd.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		setErrLisBytes(buffer, position);
		position += Len.ERR_LIS;
		setWngLisBytes(buffer, position);
	}

	public byte[] getRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeShort(buffer, position, retCd.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		getErrLisBytes(buffer, position);
		position += Len.ERR_LIS;
		getWngLisBytes(buffer, position);
		return buffer;
	}

	public void setErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setErrMsg(idx, MarshalByte.readString(buffer, position, Len.ERR_MSG));
				position += Len.ERR_MSG;
			} else {
				setErrMsg(idx, "");
				position += Len.ERR_MSG;
			}
		}
	}

	public byte[] getErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getErrMsg(idx), Len.ERR_MSG);
			position += Len.ERR_MSG;
		}
		return buffer;
	}

	public void setErrMsg(int errMsgIdx, String errMsg) {
		this.errMsg[errMsgIdx - 1] = Functions.subString(errMsg, Len.ERR_MSG);
	}

	public String getErrMsg(int errMsgIdx) {
		return this.errMsg[errMsgIdx - 1];
	}

	public String getErrMsgFormatted(int errMsgIdx) {
		return Functions.padBlanks(getErrMsg(errMsgIdx), Len.ERR_MSG);
	}

	public void setWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setWngMsg(idx, MarshalByte.readString(buffer, position, Len.WNG_MSG));
				position += Len.WNG_MSG;
			} else {
				setWngMsg(idx, "");
				position += Len.WNG_MSG;
			}
		}
	}

	public byte[] getWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getWngMsg(idx), Len.WNG_MSG);
			position += Len.WNG_MSG;
		}
		return buffer;
	}

	public void setWngMsg(int wngMsgIdx, String wngMsg) {
		this.wngMsg[wngMsgIdx - 1] = Functions.subString(wngMsg, Len.WNG_MSG);
	}

	public String getWngMsg(int wngMsgIdx) {
		return this.wngMsg[wngMsgIdx - 1];
	}

	public Xz08coAddlItsInfo getAddlItsInfo(int idx) {
		return addlItsInfo[idx - 1];
	}

	public Xz08coRetCd getRetCd() {
		return retCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERR_MSG = 500;
		public static final int WNG_MSG = 500;
		public static final int ERR_LIS = Xzc080co.ERR_MSG_MAXOCCURS * ERR_MSG;
		public static final int WNG_LIS = Xzc080co.WNG_MSG_MAXOCCURS * WNG_MSG;
		public static final int RET_MSG = Xz08coRetCd.Len.XZ08CO_RET_CD + ERR_LIS + WNG_LIS;
		public static final int SERVICE_OUTPUTS = RET_MSG + Xzc080co.ADDL_ITS_INFO_MAXOCCURS * Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
