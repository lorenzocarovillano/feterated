/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: HALRMON-PERF-MONITOR-STORAGE<br>
 * Variable: HALRMON-PERF-MONITOR-STORAGE from program XZ0D0001<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class HalrmonPerfMonitorStorage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRMON-DUMMY-INFO-TEXT
	private char dummyInfoText = Types.SPACE_CHAR;
	//Original name: HALRMON-INPUT-LINKAGE
	private HalrmonInputLinkage inputLinkage = new HalrmonInputLinkage();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.HALRMON_PERF_MONITOR_STORAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setHalrmonPerfMonitorStorageBytes(buf);
	}

	public String getHalrmonPerfMonitorStorageFormatted() {
		return MarshalByteExt.bufferToStr(getHalrmonPerfMonitorStorageBytes());
	}

	public void setHalrmonPerfMonitorStorageBytes(byte[] buffer) {
		setHalrmonPerfMonitorStorageBytes(buffer, 1);
	}

	public byte[] getHalrmonPerfMonitorStorageBytes() {
		byte[] buffer = new byte[Len.HALRMON_PERF_MONITOR_STORAGE];
		return getHalrmonPerfMonitorStorageBytes(buffer, 1);
	}

	public void setHalrmonPerfMonitorStorageBytes(byte[] buffer, int offset) {
		int position = offset;
		dummyInfoText = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		inputLinkage.setInputLinkageBytes(buffer, position);
	}

	public byte[] getHalrmonPerfMonitorStorageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, dummyInfoText);
		position += Types.CHAR_SIZE;
		inputLinkage.getInputLinkageBytes(buffer, position);
		return buffer;
	}

	public void setDummyInfoText(char dummyInfoText) {
		this.dummyInfoText = dummyInfoText;
	}

	public char getDummyInfoText() {
		return this.dummyInfoText;
	}

	public HalrmonInputLinkage getInputLinkage() {
		return inputLinkage;
	}

	@Override
	public byte[] serialize() {
		return getHalrmonPerfMonitorStorageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DUMMY_INFO_TEXT = 1;
		public static final int HALRMON_PERF_MONITOR_STORAGE = DUMMY_INFO_TEXT + HalrmonInputLinkage.Len.INPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
