/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRec;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [ACT_NOT_FRM_REC]
 * 
 */
public class ActNotFrmRecDao extends BaseSqlDao<IActNotFrmRec> {

	private Cursor formRecCsr;
	private Cursor actNotFrmRecCsr;
	private final IRowMapper<IActNotFrmRec> fetchFormRecCsrRm = buildNamedRowMapper(IActNotFrmRec.class, "frmSeqNbr", "recSeqNbr");
	private final IRowMapper<IActNotFrmRec> fetchActNotFrmRecCsrRm = buildNamedRowMapper(IActNotFrmRec.class, "csrActNbr", "notPrcTs", "frmSeqNbr",
			"recSeqNbr");

	public ActNotFrmRecDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotFrmRec> getToClass() {
		return IActNotFrmRec.class;
	}

	public DbAccessStatus openFormRecCsr(String csrActNbr, String notPrcTs) {
		formRecCsr = buildQuery("openFormRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).open();
		return dbStatus;
	}

	public IActNotFrmRec fetchFormRecCsr(IActNotFrmRec iActNotFrmRec) {
		return fetch(formRecCsr, iActNotFrmRec, fetchFormRecCsrRm);
	}

	public DbAccessStatus closeFormRecCsr() {
		return closeCursor(formRecCsr);
	}

	public DbAccessStatus openActNotFrmRecCsr(String csrActNbr, String notPrcTs, short frmSeqNbr, short recSeqNbr) {
		actNotFrmRecCsr = buildQuery("openActNotFrmRecCsr").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("recSeqNbr", recSeqNbr).open();
		return dbStatus;
	}

	public DbAccessStatus closeActNotFrmRecCsr() {
		return closeCursor(actNotFrmRecCsr);
	}

	public IActNotFrmRec fetchActNotFrmRecCsr(IActNotFrmRec iActNotFrmRec) {
		return fetch(actNotFrmRecCsr, iActNotFrmRec, fetchActNotFrmRecCsrRm);
	}

	public DbAccessStatus insertRec(String csrActNbr, String notPrcTs, short frmSeqNbr, short recSeqNbr) {
		return buildQuery("insertRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("recSeqNbr", recSeqNbr).executeInsert();
	}

	public DbAccessStatus deleteRec(String csrActNbr, String notPrcTs, short frmSeqNbr, short recSeqNbr) {
		return buildQuery("deleteRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("recSeqNbr", recSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec1(String csrActNbr, String notPrcTs, short frmSeqNbr) {
		return buildQuery("deleteRec1").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec2(String csrActNbr, String notPrcTs, short recSeqNbr) {
		return buildQuery("deleteRec2").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("recSeqNbr", recSeqNbr).executeDelete();
	}

	public DbAccessStatus deleteRec3(String csrActNbr, String notPrcTs) {
		return buildQuery("deleteRec3").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).executeDelete();
	}

	public IActNotFrmRec selectRec(String csrActNbr, String notPrcTs, short frmSeqNbr, short recSeqNbr, IActNotFrmRec iActNotFrmRec) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("notPrcTs", notPrcTs).bind("frmSeqNbr", frmSeqNbr)
				.bind("recSeqNbr", recSeqNbr).singleResult(iActNotFrmRec);
	}
}
