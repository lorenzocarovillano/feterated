/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-ERR-TIMESTAMP<br>
 * Variable: WS-ERR-TIMESTAMP from program HALOESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsErrTimestamp {

	//==== PROPERTIES ====
	//Original name: WS-ERR-DATE
	private String dateFld = DefaultValues.stringVal(Len.DATE_FLD);
	//Original name: FILLER-WS-ERR-TIMESTAMP
	private char flr1 = '-';
	//Original name: WS-ERR-TIME
	private String timeFld = DefaultValues.stringVal(Len.TIME_FLD);
	//Original name: FILLER-WS-ERR-TIMESTAMP-1
	private char flr2 = '.';
	//Original name: WS-ERR-MILSEC
	private String milsec = DefaultValues.stringVal(Len.MILSEC);

	//==== METHODS ====
	public String getWsErrTimestampFormatted() {
		return MarshalByteExt.bufferToStr(getWsErrTimestampBytes());
	}

	public byte[] getWsErrTimestampBytes() {
		byte[] buffer = new byte[Len.WS_ERR_TIMESTAMP];
		return getWsErrTimestampBytes(buffer, 1);
	}

	public byte[] getWsErrTimestampBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dateFld, Len.DATE_FLD);
		position += Len.DATE_FLD;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, timeFld, Len.TIME_FLD);
		position += Len.TIME_FLD;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, milsec, Len.MILSEC);
		return buffer;
	}

	public void setDateFld(String dateFld) {
		this.dateFld = Functions.subString(dateFld, Len.DATE_FLD);
	}

	public String getDateFld() {
		return this.dateFld;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setTimeFld(String timeFld) {
		this.timeFld = Functions.subString(timeFld, Len.TIME_FLD);
	}

	public String getTimeFld() {
		return this.timeFld;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setMilsec(String milsec) {
		this.milsec = Functions.subString(milsec, Len.MILSEC);
	}

	public String getMilsec() {
		return this.milsec;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_FLD = 10;
		public static final int TIME_FLD = 8;
		public static final int MILSEC = 6;
		public static final int FLR1 = 1;
		public static final int WS_ERR_TIMESTAMP = DATE_FLD + TIME_FLD + MILSEC + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
