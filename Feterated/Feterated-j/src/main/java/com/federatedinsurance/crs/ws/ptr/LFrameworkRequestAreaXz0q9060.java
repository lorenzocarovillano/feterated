/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9060<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9060 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9060() {
	}

	public LFrameworkRequestAreaXz0q9060(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza960qMaxPolRows(short xza960qMaxPolRows) {
		writeBinaryShort(Pos.XZA960_MAX_POL_ROWS, xza960qMaxPolRows);
	}

	/**Original name: XZA960Q-MAX-POL-ROWS<br>*/
	public short getXza960qMaxPolRows() {
		return readBinaryShort(Pos.XZA960_MAX_POL_ROWS);
	}

	public void setXza960qCsrActNbr(String xza960qCsrActNbr) {
		writeString(Pos.XZA960_CSR_ACT_NBR, xza960qCsrActNbr, Len.XZA960_CSR_ACT_NBR);
	}

	/**Original name: XZA960Q-CSR-ACT-NBR<br>*/
	public String getXza960qCsrActNbr() {
		return readString(Pos.XZA960_CSR_ACT_NBR, Len.XZA960_CSR_ACT_NBR);
	}

	public void setXza960qNotPrcTs(String xza960qNotPrcTs) {
		writeString(Pos.XZA960_NOT_PRC_TS, xza960qNotPrcTs, Len.XZA960_NOT_PRC_TS);
	}

	/**Original name: XZA960Q-NOT-PRC-TS<br>*/
	public String getXza960qNotPrcTs() {
		return readString(Pos.XZA960_NOT_PRC_TS, Len.XZA960_NOT_PRC_TS);
	}

	public void setXza960qNotDt(String xza960qNotDt) {
		writeString(Pos.XZA960_NOT_DT, xza960qNotDt, Len.XZA960_NOT_DT);
	}

	/**Original name: XZA960Q-NOT-DT<br>*/
	public String getXza960qNotDt() {
		return readString(Pos.XZA960_NOT_DT, Len.XZA960_NOT_DT);
	}

	public void setXza960qTotFeeAmt(AfDecimal xza960qTotFeeAmt) {
		writeDecimal(Pos.XZA960_TOT_FEE_AMT, xza960qTotFeeAmt.copy());
	}

	/**Original name: XZA960Q-TOT-FEE-AMT<br>*/
	public AfDecimal getXza960qTotFeeAmt() {
		return readDecimal(Pos.XZA960_TOT_FEE_AMT, Len.Int.XZA960Q_TOT_FEE_AMT, Len.Fract.XZA960Q_TOT_FEE_AMT);
	}

	public void setXza960qStAbb(String xza960qStAbb) {
		writeString(Pos.XZA960_ST_ABB, xza960qStAbb, Len.XZA960_ST_ABB);
	}

	/**Original name: XZA960Q-ST-ABB<br>*/
	public String getXza960qStAbb() {
		return readString(Pos.XZA960_ST_ABB, Len.XZA960_ST_ABB);
	}

	public void setXza960qDsyPrtGrp(String xza960qDsyPrtGrp) {
		writeString(Pos.XZA960_DSY_PRT_GRP, xza960qDsyPrtGrp, Len.XZA960_DSY_PRT_GRP);
	}

	/**Original name: XZA960Q-DSY-PRT-GRP<br>*/
	public String getXza960qDsyPrtGrp() {
		return readString(Pos.XZA960_DSY_PRT_GRP, Len.XZA960_DSY_PRT_GRP);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9060 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA960_GET_ACT_NOT_DETAIL = L_FW_REQ_XZ0A9060;
		public static final int XZA960_MAX_POL_ROWS = XZA960_GET_ACT_NOT_DETAIL;
		public static final int XZA960_CSR_ACT_NBR = XZA960_MAX_POL_ROWS + Len.XZA960_MAX_POL_ROWS;
		public static final int XZA960_NOT_PRC_TS = XZA960_CSR_ACT_NBR + Len.XZA960_CSR_ACT_NBR;
		public static final int XZA960_NOT_DT = XZA960_NOT_PRC_TS + Len.XZA960_NOT_PRC_TS;
		public static final int XZA960_TOT_FEE_AMT = XZA960_NOT_DT + Len.XZA960_NOT_DT;
		public static final int XZA960_ST_ABB = XZA960_TOT_FEE_AMT + Len.XZA960_TOT_FEE_AMT;
		public static final int XZA960_DSY_PRT_GRP = XZA960_ST_ABB + Len.XZA960_ST_ABB;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA960_MAX_POL_ROWS = 2;
		public static final int XZA960_CSR_ACT_NBR = 9;
		public static final int XZA960_NOT_PRC_TS = 26;
		public static final int XZA960_NOT_DT = 10;
		public static final int XZA960_TOT_FEE_AMT = 10;
		public static final int XZA960_ST_ABB = 2;
		public static final int XZA960_DSY_PRT_GRP = 5;
		public static final int XZA960_GET_ACT_NOT_DETAIL = XZA960_MAX_POL_ROWS + XZA960_CSR_ACT_NBR + XZA960_NOT_PRC_TS + XZA960_NOT_DT
				+ XZA960_TOT_FEE_AMT + XZA960_ST_ABB + XZA960_DSY_PRT_GRP;
		public static final int L_FW_REQ_XZ0A9060 = XZA960_GET_ACT_NOT_DETAIL;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9060;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA960Q_TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZA960Q_TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
