/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HalrmonFunction;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: HALRMON-INPUT-LINKAGE<br>
 * Variable: HALRMON-INPUT-LINKAGE from copybook HALLMON<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class HalrmonInputLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRMON-FUNCTION
	private HalrmonFunction function = new HalrmonFunction();
	//Original name: HALRMON-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HALRMON-BUS-MOD-NM
	private String busModNm = DefaultValues.stringVal(Len.BUS_MOD_NM);
	//Original name: HALRMON-INFO-LABEL
	private String infoLabel = DefaultValues.stringVal(Len.INFO_LABEL);
	//Original name: HALRMON-INFO-LENGTH
	private short infoLength = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.INPUT_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setInputLinkageBytes(buf);
	}

	public String getInputLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getInputLinkageBytes());
	}

	public void setInputLinkageBytes(byte[] buffer) {
		setInputLinkageBytes(buffer, 1);
	}

	public byte[] getInputLinkageBytes() {
		byte[] buffer = new byte[Len.INPUT_LINKAGE];
		return getInputLinkageBytes(buffer, 1);
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		busModNm = MarshalByte.readString(buffer, position, Len.BUS_MOD_NM);
		position += Len.BUS_MOD_NM;
		infoLabel = MarshalByte.readString(buffer, position, Len.INFO_LABEL);
		position += Len.INFO_LABEL;
		infoLength = MarshalByte.readBinaryShort(buffer, position);
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, busModNm, Len.BUS_MOD_NM);
		position += Len.BUS_MOD_NM;
		MarshalByte.writeString(buffer, position, infoLabel, Len.INFO_LABEL);
		position += Len.INFO_LABEL;
		MarshalByte.writeBinaryShort(buffer, position, infoLength);
		return buffer;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setBusModNm(String busModNm) {
		this.busModNm = Functions.subString(busModNm, Len.BUS_MOD_NM);
	}

	public String getBusModNm() {
		return this.busModNm;
	}

	public void setInfoLabel(String infoLabel) {
		this.infoLabel = Functions.subString(infoLabel, Len.INFO_LABEL);
	}

	public String getInfoLabel() {
		return this.infoLabel;
	}

	public void setInfoLength(short infoLength) {
		this.infoLength = infoLength;
	}

	public short getInfoLength() {
		return this.infoLength;
	}

	public HalrmonFunction getFunction() {
		return function;
	}

	@Override
	public byte[] serialize() {
		return getInputLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int BUS_MOD_NM = 32;
		public static final int INFO_LABEL = 32;
		public static final int INFO_LENGTH = 2;
		public static final int INPUT_LINKAGE = HalrmonFunction.Len.FUNCTION + BUS_OBJ_NM + BUS_MOD_NM + INFO_LABEL + INFO_LENGTH;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
