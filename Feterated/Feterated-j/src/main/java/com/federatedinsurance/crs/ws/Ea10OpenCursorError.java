/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-10-OPEN-CURSOR-ERROR<br>
 * Variable: EA-10-OPEN-CURSOR-ERROR from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea10OpenCursorError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR
	private String flr1 = "XZ001000 -";
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-1
	private String flr2 = "DB2 OPEN";
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-2
	private String flr3 = "CURSOR";
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-3
	private String flr4 = "ERROR OCCURRED";
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-4
	private String flr5 = "FOR CURSOR";
	//Original name: EA-10-CSR-NAME
	private String csrName = DefaultValues.stringVal(Len.CSR_NAME);
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-5
	private String flr6 = " SQLCODE =";
	//Original name: EA-10-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-6
	private String flr7 = ".  (PARAGRAPH #";
	//Original name: EA-10-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-10-OPEN-CURSOR-ERROR-7
	private String flr8 = ").";

	//==== METHODS ====
	public String getEa10OpenCursorErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa10OpenCursorErrorBytes());
	}

	public byte[] getEa10OpenCursorErrorBytes() {
		byte[] buffer = new byte[Len.EA10_OPEN_CURSOR_ERROR];
		return getEa10OpenCursorErrorBytes(buffer, 1);
	}

	public byte[] getEa10OpenCursorErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, csrName, Len.CSR_NAME);
		position += Len.CSR_NAME;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position += Len.SQLCODE;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setCsrName(String csrName) {
		this.csrName = Functions.subString(csrName, Len.CSR_NAME);
	}

	public String getCsrName() {
		return this.csrName;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("++(4)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("++(4)9").parseLong(this.sqlcode);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_NAME = 18;
		public static final int SQLCODE = 6;
		public static final int PARAGRAPH_NBR = 5;
		public static final int FLR1 = 11;
		public static final int FLR2 = 9;
		public static final int FLR3 = 7;
		public static final int FLR4 = 15;
		public static final int FLR8 = 2;
		public static final int EA10_OPEN_CURSOR_ERROR = CSR_NAME + SQLCODE + PARAGRAPH_NBR + 3 * FLR1 + FLR2 + FLR3 + 2 * FLR4 + FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
