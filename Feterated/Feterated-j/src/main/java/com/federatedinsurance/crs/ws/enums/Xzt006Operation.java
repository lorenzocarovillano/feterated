/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZT006-OPERATION<br>
 * Variable: XZT006-OPERATION from copybook XZ0Z0006<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzt006Operation {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.OPERATION);
	public static final String DLT_ACT_NOT = "DeleteAccountNotification";
	public static final String ADD_ACT_NOT = "AddAccountNotification";
	public static final String UPD_ACT_NOT = "UpdateAccountNotification";

	//==== METHODS ====
	public void setOperation(String operation) {
		this.value = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.value;
	}

	public void setDltActNot() {
		value = DLT_ACT_NOT;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
