/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: DATE-STRUCTURE<br>
 * Variable: DATE-STRUCTURE from copybook XPXLDAT<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DateStructureCawpcorc extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DATE-FUNCTION
	private char dateFunction = DefaultValues.CHAR_VAL;
	/**Original name: DATE-LANGUAGE<br>
	 * <pre>              C = DATE CONVERSION                                00012
	 *               D = DATE DIFFERENCE                                00013</pre>*/
	private String dateLanguage = DefaultValues.stringVal(Len.DATE_LANGUAGE);
	/**Original name: DATE-INP-FORMAT<br>
	 * <pre>              SPACES = ALWAYS                                    00015</pre>*/
	private String dateInpFormat = DefaultValues.stringVal(Len.DATE_INP_FORMAT);
	/**Original name: DATE-OUT-FORMAT<br>
	 * <pre>              J = YYYYDDD/HH:MM:SS                               00017
	 *               G = YYYY/MM/DD/HH:MM:SS                            00018
	 *               G1 = YYYY/MM/DD/HH:MM:SS                           00019
	 *               G7 = YYYY/MM/DD/HH:MM:SS                           00020
	 *               M = MM/DD/YYYY/HH:MM:SS                            00021
	 *               M1 = MM/DD/YYYY/HH:MM:SS                           00022
	 *               M7 = MM/DD/YYYY/HH:MM:SS                           00023
	 *               D = DD/MM/YYYY/HH:MM:SS                            00024
	 *               D1 = DD/MM/YYYY/HH:MM:SS                           00025
	 *               D7 = DD/MM/YYYY/HH:MM:SS                           00026
	 *               B = YYYY-MM-DD-HH.MM.SS.MMMMMM                     00027
	 *               SY= (SYSTEM)                                       00028
	 *               S3= IAP SERIES III DATE                            00029
	 *               - ON WORKSTATION ONLY                              00030
	 *               ?  = (G/M/D) FROM OS/2 CONTROL PANEL               00031
	 *               ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL            00032
	 *               ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL            00033</pre>*/
	private String dateOutFormat = DefaultValues.stringVal(Len.DATE_OUT_FORMAT);
	/**Original name: DATE-ADJ-FORMAT<br>
	 * <pre>              J = YYYYDDD HH:MM:SS                               00035
	 *               G = YYYY/MM/DD HH:MM:SS                            00036
	 *               M = MM/DD/YYYY HH:MM:SS                            00037
	 *               D = DD/MM/YYYY HH:MM:SS                            00038
	 *               W = DAY-OF-WEEK                                    00039
	 *               AM= HH:MM:SS AM MONTH DD, YYYY                     00040
	 *               AD= HH:MM:SS AM DD MONTH YYYY                      00041
	 *               AS= HH:MM:SS AM DD MON YYYY                        00042
	 *               B = YYYY-MM-DD-HH.MM.SS.MMMMMM                     00043
	 *               - ON WORKSTATION ONLY                              00044
	 *               ?  = (G/M/D) FROM OS/2 CONTROL PANEL               00045
	 *               ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL            00046
	 *               ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL            00047
	 *               THIS IS AN INPUT FIELD FOR DIFFERENCE CALULATIONS  00048</pre>*/
	private String dateAdjFormat = DefaultValues.stringVal(Len.DATE_ADJ_FORMAT);
	/**Original name: DATE-RETURN-CODE<br>
	 * <pre>              AD= +/- MINUTES                                    00050
	 *               HD= +/- HOURS                                      00051
	 *               DD= +/- DAYS                                       00052
	 *               WD= +/- WEEKS                                      00053
	 *               MD= +/- MONTHS                                     00054
	 *               YD= +/- YEARS                                      00055
	 *               *B= ABOVE IN FULL WORD BINARY  (HOST ONLY)         00056
	 *               THIS IS AN OUTPUT FIELD FOR DIFFERENCE CALULATIONS 00057</pre>*/
	private String dateReturnCode = DefaultValues.stringVal(Len.DATE_RETURN_CODE);
	/**Original name: DATE-INPUT<br>
	 * <pre>              OK= GOOD NEWS                                      00059
	 *               IF=INVALID FORMAT  (EDITING CHARACTERS)            00060
	 *               ID=INVALID DATE    (>12 MONTHS,>31 DAYS)           00061
	 *               IC=INVALID CODE    (DATE-XXX-FORMAT ERROR)         00062
	 *               UC=UNSUPPORTED     (ON THIS PLATFORM)              00063
	 *               OR=OUT OF RANGE FOR DIFFERENCE CALCULATIONS        00064
	 *               XC=NOT IMPLEMENTED (YET)                           00065</pre>*/
	private String dateInput = DefaultValues.stringVal(Len.DATE_INPUT);
	/**Original name: DATE-OUTPUT<br>
	 * <pre>              DATE TO EDIT OR REFORMAT                           00067</pre>*/
	private String dateOutput = DefaultValues.stringVal(Len.DATE_OUTPUT);
	//Original name: DATE-ADJUST-SIGN
	private char dateAdjustSign = DefaultValues.CHAR_VAL;
	//Original name: DATE-ADJUST-NUMB
	private String dateAdjustNumb = DefaultValues.stringVal(Len.DATE_ADJUST_NUMB);
	/**Original name: DATE-DELIMITER-DATE<br>
	 * <pre>              ADD (OR SUBTRACT) THIS AMOUNT                      00078
	 *             DIFFERENCE FIELD FOR DIFFERENCE CALULATIONS          00079</pre>*/
	private char dateDelimiterDate = DefaultValues.CHAR_VAL;
	/**Original name: DATE-DELIMITER-TIME<br>
	 * <pre>              / OR OTHER EDITING CHARACTER IN DATE               00081
	 *               - ON WORKSTATION ONLY:                             00082
	 *               ? GET DELIMITER FROM OS/2 CONTROL PANEL            00083</pre>*/
	private char dateDelimiterTime = DefaultValues.CHAR_VAL;
	/**Original name: DATE-AM-PM<br>
	 * <pre>              : OR OTHER EDITING CHARACTER IN TIME               00085
	 *               - ON WORKSTATION ONLY:                             00086
	 *               ? GET DELIMITER FROM OS/2 CONTROL PANEL            00087</pre>*/
	private String dateAmPm = DefaultValues.stringVal(Len.DATE_AM_PM);
	//Original name: SCRATCH-AREA-REDEF
	private ScratchAreaRedef scratchAreaRedef = new ScratchAreaRedef();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DATE_STRUCTURE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDateStructureBytes(buf);
	}

	public String getDateStructureFormatted() {
		return MarshalByteExt.bufferToStr(getDateStructureBytes());
	}

	public void setDateStructureBytes(byte[] buffer) {
		setDateStructureBytes(buffer, 1);
	}

	public byte[] getDateStructureBytes() {
		byte[] buffer = new byte[Len.DATE_STRUCTURE];
		return getDateStructureBytes(buffer, 1);
	}

	public void setDateStructureBytes(byte[] buffer, int offset) {
		int position = offset;
		dateFunction = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateLanguage = MarshalByte.readString(buffer, position, Len.DATE_LANGUAGE);
		position += Len.DATE_LANGUAGE;
		dateInpFormat = MarshalByte.readString(buffer, position, Len.DATE_INP_FORMAT);
		position += Len.DATE_INP_FORMAT;
		dateOutFormat = MarshalByte.readString(buffer, position, Len.DATE_OUT_FORMAT);
		position += Len.DATE_OUT_FORMAT;
		dateAdjFormat = MarshalByte.readString(buffer, position, Len.DATE_ADJ_FORMAT);
		position += Len.DATE_ADJ_FORMAT;
		dateReturnCode = MarshalByte.readString(buffer, position, Len.DATE_RETURN_CODE);
		position += Len.DATE_RETURN_CODE;
		dateInput = MarshalByte.readString(buffer, position, Len.DATE_INPUT);
		position += Len.DATE_INPUT;
		dateOutput = MarshalByte.readString(buffer, position, Len.DATE_OUTPUT);
		position += Len.DATE_OUTPUT;
		setDateAdjustBytes(buffer, position);
		position += Len.DATE_ADJUST;
		dateDelimiterDate = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateDelimiterTime = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateAmPm = MarshalByte.readString(buffer, position, Len.DATE_AM_PM);
		position += Len.DATE_AM_PM;
		scratchAreaRedef.setScratchAreaRedefBytes(buffer, position);
	}

	public byte[] getDateStructureBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, dateFunction);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateLanguage, Len.DATE_LANGUAGE);
		position += Len.DATE_LANGUAGE;
		MarshalByte.writeString(buffer, position, dateInpFormat, Len.DATE_INP_FORMAT);
		position += Len.DATE_INP_FORMAT;
		MarshalByte.writeString(buffer, position, dateOutFormat, Len.DATE_OUT_FORMAT);
		position += Len.DATE_OUT_FORMAT;
		MarshalByte.writeString(buffer, position, dateAdjFormat, Len.DATE_ADJ_FORMAT);
		position += Len.DATE_ADJ_FORMAT;
		MarshalByte.writeString(buffer, position, dateReturnCode, Len.DATE_RETURN_CODE);
		position += Len.DATE_RETURN_CODE;
		MarshalByte.writeString(buffer, position, dateInput, Len.DATE_INPUT);
		position += Len.DATE_INPUT;
		MarshalByte.writeString(buffer, position, dateOutput, Len.DATE_OUTPUT);
		position += Len.DATE_OUTPUT;
		getDateAdjustBytes(buffer, position);
		position += Len.DATE_ADJUST;
		MarshalByte.writeChar(buffer, position, dateDelimiterDate);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, dateDelimiterTime);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateAmPm, Len.DATE_AM_PM);
		position += Len.DATE_AM_PM;
		scratchAreaRedef.getScratchAreaRedefBytes(buffer, position);
		return buffer;
	}

	public void initDateStructureSpaces() {
		dateFunction = Types.SPACE_CHAR;
		dateLanguage = "";
		dateInpFormat = "";
		dateOutFormat = "";
		dateAdjFormat = "";
		dateReturnCode = "";
		dateInput = "";
		dateOutput = "";
		initDateAdjustSpaces();
		dateDelimiterDate = Types.SPACE_CHAR;
		dateDelimiterTime = Types.SPACE_CHAR;
		dateAmPm = "";
		scratchAreaRedef.initScratchAreaRedefSpaces();
	}

	public void setDateFunction(char dateFunction) {
		this.dateFunction = dateFunction;
	}

	public void setDateFunctionFormatted(String dateFunction) {
		setDateFunction(Functions.charAt(dateFunction, Types.CHAR_SIZE));
	}

	public char getDateFunction() {
		return this.dateFunction;
	}

	public void setDateLanguage(String dateLanguage) {
		this.dateLanguage = Functions.subString(dateLanguage, Len.DATE_LANGUAGE);
	}

	public String getDateLanguage() {
		return this.dateLanguage;
	}

	public void setDateInpFormat(String dateInpFormat) {
		this.dateInpFormat = Functions.subString(dateInpFormat, Len.DATE_INP_FORMAT);
	}

	public String getDateInpFormat() {
		return this.dateInpFormat;
	}

	public void setDateOutFormat(String dateOutFormat) {
		this.dateOutFormat = Functions.subString(dateOutFormat, Len.DATE_OUT_FORMAT);
	}

	public String getDateOutFormat() {
		return this.dateOutFormat;
	}

	public void setDateAdjFormat(String dateAdjFormat) {
		this.dateAdjFormat = Functions.subString(dateAdjFormat, Len.DATE_ADJ_FORMAT);
	}

	public String getDateAdjFormat() {
		return this.dateAdjFormat;
	}

	public void setDateReturnCode(String dateReturnCode) {
		this.dateReturnCode = Functions.subString(dateReturnCode, Len.DATE_RETURN_CODE);
	}

	public String getDateReturnCode() {
		return this.dateReturnCode;
	}

	public String getDateReturnCodeFormatted() {
		return Functions.padBlanks(getDateReturnCode(), Len.DATE_RETURN_CODE);
	}

	public void setDateInput(String dateInput) {
		this.dateInput = Functions.subString(dateInput, Len.DATE_INPUT);
	}

	public String getDateInput() {
		return this.dateInput;
	}

	public String getDateInputFormatted() {
		return Functions.padBlanks(getDateInput(), Len.DATE_INPUT);
	}

	public void setDateOutput(String dateOutput) {
		this.dateOutput = Functions.subString(dateOutput, Len.DATE_OUTPUT);
	}

	public String getDateOutput() {
		return this.dateOutput;
	}

	public String getDateOutputFormatted() {
		return Functions.padBlanks(getDateOutput(), Len.DATE_OUTPUT);
	}

	public void setDateAdjustBytes(byte[] buffer, int offset) {
		int position = offset;
		dateAdjustSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		dateAdjustNumb = MarshalByte.readFixedString(buffer, position, Len.DATE_ADJUST_NUMB);
	}

	public byte[] getDateAdjustBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, dateAdjustSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dateAdjustNumb, Len.DATE_ADJUST_NUMB);
		return buffer;
	}

	public void initDateAdjustSpaces() {
		dateAdjustSign = Types.SPACE_CHAR;
		dateAdjustNumb = "";
	}

	public void setDateAdjustSign(char dateAdjustSign) {
		this.dateAdjustSign = dateAdjustSign;
	}

	public void setDateAdjustSignFormatted(String dateAdjustSign) {
		setDateAdjustSign(Functions.charAt(dateAdjustSign, Types.CHAR_SIZE));
	}

	public char getDateAdjustSign() {
		return this.dateAdjustSign;
	}

	public void setDateAdjustNumb(int dateAdjustNumb) {
		this.dateAdjustNumb = NumericDisplay.asString(dateAdjustNumb, Len.DATE_ADJUST_NUMB);
	}

	public int getDateAdjustNumb() {
		return NumericDisplay.asInt(this.dateAdjustNumb);
	}

	public void setDateDelimiterDate(char dateDelimiterDate) {
		this.dateDelimiterDate = dateDelimiterDate;
	}

	public char getDateDelimiterDate() {
		return this.dateDelimiterDate;
	}

	public void setDateDelimiterTime(char dateDelimiterTime) {
		this.dateDelimiterTime = dateDelimiterTime;
	}

	public char getDateDelimiterTime() {
		return this.dateDelimiterTime;
	}

	public void setDateAmPm(String dateAmPm) {
		this.dateAmPm = Functions.subString(dateAmPm, Len.DATE_AM_PM);
	}

	public String getDateAmPm() {
		return this.dateAmPm;
	}

	public ScratchAreaRedef getScratchAreaRedef() {
		return scratchAreaRedef;
	}

	@Override
	public byte[] serialize() {
		return getDateStructureBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_FUNCTION = 1;
		public static final int DATE_LANGUAGE = 3;
		public static final int DATE_INP_FORMAT = 2;
		public static final int DATE_OUT_FORMAT = 2;
		public static final int DATE_ADJ_FORMAT = 2;
		public static final int DATE_RETURN_CODE = 2;
		public static final int DATE_INPUT = 30;
		public static final int DATE_OUTPUT = 30;
		public static final int DATE_ADJUST_SIGN = 1;
		public static final int DATE_ADJUST_NUMB = 5;
		public static final int DATE_ADJUST = DATE_ADJUST_SIGN + DATE_ADJUST_NUMB;
		public static final int DATE_DELIMITER_DATE = 1;
		public static final int DATE_DELIMITER_TIME = 1;
		public static final int DATE_AM_PM = 4;
		public static final int DATE_STRUCTURE = DATE_FUNCTION + DATE_LANGUAGE + DATE_INP_FORMAT + DATE_OUT_FORMAT + DATE_ADJ_FORMAT
				+ DATE_RETURN_CODE + DATE_INPUT + DATE_OUTPUT + DATE_ADJUST + DATE_DELIMITER_DATE + DATE_DELIMITER_TIME + DATE_AM_PM
				+ ScratchAreaRedef.Len.SCRATCH_AREA_REDEF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
