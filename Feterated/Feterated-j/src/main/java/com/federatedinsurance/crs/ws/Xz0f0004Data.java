/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.copy.Ts020tbl;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0F0004<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0f0004Data {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0f0004 workingStorageArea = new WorkingStorageAreaXz0f0004();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-XZ0C0004-LAYOUT
	private WsXz0c0004Layout wsXz0c0004Layout = new WsXz0c0004Layout();
	//Original name: TS020TBL
	private Ts020tbl ts020tbl = new Ts020tbl();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setTableFormatterDataFormatted(String data) {
		byte[] buffer = new byte[Len.TABLE_FORMATTER_DATA];
		MarshalByte.writeString(buffer, 1, data, Len.TABLE_FORMATTER_DATA);
		setTableFormatterDataBytes(buffer, 1);
	}

	public String getTableFormatterDataFormatted() {
		return ts020tbl.getTableFormatterParmsFormatted();
	}

	public void setTableFormatterDataBytes(byte[] buffer, int offset) {
		int position = offset;
		ts020tbl.setTableFormatterParmsBytes(buffer, position);
	}

	public Ts020tbl getTs020tbl() {
		return ts020tbl;
	}

	public WorkingStorageAreaXz0f0004 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsXz0c0004Layout getWsXz0c0004Layout() {
		return wsXz0c0004Layout;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA01_FAILED_LINK_PGM_NAME = 8;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int TABLE_FORMATTER_DATA = Ts020tbl.Len.TABLE_FORMATTER_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
