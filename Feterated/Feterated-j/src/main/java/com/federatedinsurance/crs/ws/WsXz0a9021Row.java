/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9021-ROW<br>
 * Variable: WS-XZ0A9021-ROW from program XZ0B9020<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9021Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA921-REC-SORT-ORD-NBR
	private int recSortOrdNbr = DefaultValues.INT_VAL;
	//Original name: XZA921-REC-SEQ-NBR
	private int recSeqNbr = DefaultValues.INT_VAL;
	//Original name: XZA921-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: XZA921-REC-TYP-DES
	private String recTypDes = DefaultValues.stringVal(Len.REC_TYP_DES);
	//Original name: XZA921-CER-NBR
	private String cerNbr = DefaultValues.stringVal(Len.CER_NBR);
	//Original name: XZA921-NM-ADR-LIN-1
	private String nmAdrLin1 = DefaultValues.stringVal(Len.NM_ADR_LIN1);
	//Original name: XZA921-NM-ADR-LIN-2
	private String nmAdrLin2 = DefaultValues.stringVal(Len.NM_ADR_LIN2);
	//Original name: XZA921-NM-ADR-LIN-3
	private String nmAdrLin3 = DefaultValues.stringVal(Len.NM_ADR_LIN3);
	//Original name: XZA921-NM-ADR-LIN-4
	private String nmAdrLin4 = DefaultValues.stringVal(Len.NM_ADR_LIN4);
	//Original name: XZA921-NM-ADR-LIN-5
	private String nmAdrLin5 = DefaultValues.stringVal(Len.NM_ADR_LIN5);
	//Original name: XZA921-NM-ADR-LIN-6
	private String nmAdrLin6 = DefaultValues.stringVal(Len.NM_ADR_LIN6);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9021_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9021RowBytes(buf);
	}

	public String getWsXz0a9021RowFormatted() {
		return getGetRecListDetailFormatted();
	}

	public void setWsXz0a9021RowBytes(byte[] buffer) {
		setWsXz0a9021RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9021RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9021_ROW];
		return getWsXz0a9021RowBytes(buffer, 1);
	}

	public void setWsXz0a9021RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetRecListDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a9021RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetRecListDetailBytes(buffer, position);
		return buffer;
	}

	public String getGetRecListDetailFormatted() {
		return MarshalByteExt.bufferToStr(getGetRecListDetailBytes());
	}

	/**Original name: XZA921-GET-REC-LIST-DETAIL<br>
	 * <pre>*************************************************************
	 *  XZ0A9021 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_REC_LIST                           *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  12JAN2009 E404DLP    NEW                          *
	 *  PP02500  22OCT2012 E404BPO    CHANGE CER-NBR LENGTH        *
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getGetRecListDetailBytes() {
		byte[] buffer = new byte[Len.GET_REC_LIST_DETAIL];
		return getGetRecListDetailBytes(buffer, 1);
	}

	public void setGetRecListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		recSortOrdNbr = MarshalByte.readInt(buffer, position, Len.REC_SORT_ORD_NBR);
		position += Len.REC_SORT_ORD_NBR;
		recSeqNbr = MarshalByte.readInt(buffer, position, Len.REC_SEQ_NBR);
		position += Len.REC_SEQ_NBR;
		recTypCd = MarshalByte.readString(buffer, position, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		recTypDes = MarshalByte.readString(buffer, position, Len.REC_TYP_DES);
		position += Len.REC_TYP_DES;
		cerNbr = MarshalByte.readString(buffer, position, Len.CER_NBR);
		position += Len.CER_NBR;
		nmAdrLin1 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		nmAdrLin2 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		nmAdrLin3 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		nmAdrLin4 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		nmAdrLin5 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		nmAdrLin6 = MarshalByte.readString(buffer, position, Len.NM_ADR_LIN6);
	}

	public byte[] getGetRecListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeInt(buffer, position, recSortOrdNbr, Len.REC_SORT_ORD_NBR);
		position += Len.REC_SORT_ORD_NBR;
		MarshalByte.writeInt(buffer, position, recSeqNbr, Len.REC_SEQ_NBR);
		position += Len.REC_SEQ_NBR;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeString(buffer, position, recTypDes, Len.REC_TYP_DES);
		position += Len.REC_TYP_DES;
		MarshalByte.writeString(buffer, position, cerNbr, Len.CER_NBR);
		position += Len.CER_NBR;
		MarshalByte.writeString(buffer, position, nmAdrLin1, Len.NM_ADR_LIN1);
		position += Len.NM_ADR_LIN1;
		MarshalByte.writeString(buffer, position, nmAdrLin2, Len.NM_ADR_LIN2);
		position += Len.NM_ADR_LIN2;
		MarshalByte.writeString(buffer, position, nmAdrLin3, Len.NM_ADR_LIN3);
		position += Len.NM_ADR_LIN3;
		MarshalByte.writeString(buffer, position, nmAdrLin4, Len.NM_ADR_LIN4);
		position += Len.NM_ADR_LIN4;
		MarshalByte.writeString(buffer, position, nmAdrLin5, Len.NM_ADR_LIN5);
		position += Len.NM_ADR_LIN5;
		MarshalByte.writeString(buffer, position, nmAdrLin6, Len.NM_ADR_LIN6);
		return buffer;
	}

	public void setRecSortOrdNbr(int recSortOrdNbr) {
		this.recSortOrdNbr = recSortOrdNbr;
	}

	public int getRecSortOrdNbr() {
		return this.recSortOrdNbr;
	}

	public void setRecSeqNbr(int recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	public int getRecSeqNbr() {
		return this.recSeqNbr;
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setRecTypDes(String recTypDes) {
		this.recTypDes = Functions.subString(recTypDes, Len.REC_TYP_DES);
	}

	public String getRecTypDes() {
		return this.recTypDes;
	}

	public void setCerNbr(String cerNbr) {
		this.cerNbr = Functions.subString(cerNbr, Len.CER_NBR);
	}

	public String getCerNbr() {
		return this.cerNbr;
	}

	public void setNmAdrLin1(String nmAdrLin1) {
		this.nmAdrLin1 = Functions.subString(nmAdrLin1, Len.NM_ADR_LIN1);
	}

	public String getNmAdrLin1() {
		return this.nmAdrLin1;
	}

	public void setNmAdrLin2(String nmAdrLin2) {
		this.nmAdrLin2 = Functions.subString(nmAdrLin2, Len.NM_ADR_LIN2);
	}

	public String getNmAdrLin2() {
		return this.nmAdrLin2;
	}

	public void setNmAdrLin3(String nmAdrLin3) {
		this.nmAdrLin3 = Functions.subString(nmAdrLin3, Len.NM_ADR_LIN3);
	}

	public String getNmAdrLin3() {
		return this.nmAdrLin3;
	}

	public void setNmAdrLin4(String nmAdrLin4) {
		this.nmAdrLin4 = Functions.subString(nmAdrLin4, Len.NM_ADR_LIN4);
	}

	public String getNmAdrLin4() {
		return this.nmAdrLin4;
	}

	public void setNmAdrLin5(String nmAdrLin5) {
		this.nmAdrLin5 = Functions.subString(nmAdrLin5, Len.NM_ADR_LIN5);
	}

	public String getNmAdrLin5() {
		return this.nmAdrLin5;
	}

	public void setNmAdrLin6(String nmAdrLin6) {
		this.nmAdrLin6 = Functions.subString(nmAdrLin6, Len.NM_ADR_LIN6);
	}

	public String getNmAdrLin6() {
		return this.nmAdrLin6;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9021RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_SORT_ORD_NBR = 5;
		public static final int REC_SEQ_NBR = 5;
		public static final int REC_TYP_CD = 5;
		public static final int REC_TYP_DES = 30;
		public static final int CER_NBR = 25;
		public static final int NM_ADR_LIN1 = 45;
		public static final int NM_ADR_LIN2 = 45;
		public static final int NM_ADR_LIN3 = 45;
		public static final int NM_ADR_LIN4 = 45;
		public static final int NM_ADR_LIN5 = 45;
		public static final int NM_ADR_LIN6 = 45;
		public static final int GET_REC_LIST_DETAIL = REC_SORT_ORD_NBR + REC_SEQ_NBR + REC_TYP_CD + REC_TYP_DES + CER_NBR + NM_ADR_LIN1 + NM_ADR_LIN2
				+ NM_ADR_LIN3 + NM_ADR_LIN4 + NM_ADR_LIN5 + NM_ADR_LIN6;
		public static final int WS_XZ0A9021_ROW = GET_REC_LIST_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
