/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IHalUniversalCtV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [HAL_UNIVERSAL_CT_V]
 * 
 */
public class HalUniversalCtVDao extends BaseSqlDao<IHalUniversalCtV> {

	public HalUniversalCtVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IHalUniversalCtV> getToClass() {
		return IHalUniversalCtV.class;
	}

	public String selectRec(String entryKeyCd, String tblLblTxt, String dft) {
		return buildQuery("selectRec").bind("entryKeyCd", entryKeyCd).bind("tblLblTxt", tblLblTxt).scalarResultString(dft);
	}

	public String selectRec1(String tblLblTxt, String entryKeyCd, String dft) {
		return buildQuery("selectRec1").bind("tblLblTxt", tblLblTxt).bind("entryKeyCd", entryKeyCd).scalarResultString(dft);
	}
}
