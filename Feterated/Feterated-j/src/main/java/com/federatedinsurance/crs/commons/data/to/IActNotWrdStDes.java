/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT_WRD, ST_WRD_DES]
 * 
 */
public interface IActNotWrdStDes extends BaseSqlTo {

	/**
	 * Host Variable ST-WRD-SEQ-CD
	 * 
	 */
	String getSeqCd();

	void setSeqCd(String seqCd);

	/**
	 * Host Variable ST-WRD-FONT-TYP
	 * 
	 */
	String getFontTyp();

	void setFontTyp(String fontTyp);

	/**
	 * Nullable property for ST-WRD-FONT-TYP
	 * 
	 */
	String getFontTypObj();

	void setFontTypObj(String fontTypObj);

	/**
	 * Host Variable ST-WRD-TXT
	 * 
	 */
	String getTxt();

	void setTxt(String txt);
};
