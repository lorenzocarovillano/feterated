/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9090<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9090 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9090() {
	}

	public LFrameworkRequestAreaXz0q9090(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setMaxNotCoRows(short maxNotCoRows) {
		writeBinaryShort(Pos.MAX_NOT_CO_ROWS, maxNotCoRows);
	}

	/**Original name: XZA990Q-MAX-NOT-CO-ROWS<br>*/
	public short getMaxNotCoRows() {
		return readBinaryShort(Pos.MAX_NOT_CO_ROWS);
	}

	public void setActNotTypCd(String actNotTypCd) {
		writeString(Pos.ACT_NOT_TYP_CD, actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	/**Original name: XZA990Q-ACT-NOT-TYP-CD<br>*/
	public String getActNotTypCd() {
		return readString(Pos.ACT_NOT_TYP_CD, Len.ACT_NOT_TYP_CD);
	}

	public void setCoCd(String coCd) {
		writeString(Pos.CO_CD, coCd, Len.CO_CD);
	}

	/**Original name: XZA990Q-CO-CD<br>*/
	public String getCoCd() {
		return readString(Pos.CO_CD, Len.CO_CD);
	}

	public void setCoDes(String coDes) {
		writeString(Pos.CO_DES, coDes, Len.CO_DES);
	}

	/**Original name: XZA990Q-CO-DES<br>*/
	public String getCoDes() {
		return readString(Pos.CO_DES, Len.CO_DES);
	}

	public void setSpeCrtCd(String speCrtCd) {
		writeString(Pos.SPE_CRT_CD, speCrtCd, Len.SPE_CRT_CD);
	}

	/**Original name: XZA990Q-SPE-CRT-CD<br>*/
	public String getSpeCrtCd() {
		return readString(Pos.SPE_CRT_CD, Len.SPE_CRT_CD);
	}

	public void setCoOfsNbr(int coOfsNbr) {
		writeInt(Pos.CO_OFS_NBR, coOfsNbr, Len.Int.CO_OFS_NBR);
	}

	/**Original name: XZA990Q-CO-OFS-NBR<br>*/
	public int getCoOfsNbr() {
		return readNumDispInt(Pos.CO_OFS_NBR, Len.CO_OFS_NBR);
	}

	public void setDtbCd(char dtbCd) {
		writeChar(Pos.DTB_CD, dtbCd);
	}

	/**Original name: XZA990Q-DTB-CD<br>*/
	public char getDtbCd() {
		return readChar(Pos.DTB_CD);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int NOT_CO_INFO = L_FRAMEWORK_REQUEST_AREA;
		public static final int MAX_NOT_CO_ROWS = NOT_CO_INFO;
		public static final int NOT_CO_ROW = MAX_NOT_CO_ROWS + Len.MAX_NOT_CO_ROWS;
		public static final int ACT_NOT_TYP_CD = NOT_CO_ROW;
		public static final int CO_CD = ACT_NOT_TYP_CD + Len.ACT_NOT_TYP_CD;
		public static final int CO_DES = CO_CD + Len.CO_CD;
		public static final int SPE_CRT_CD = CO_DES + Len.CO_DES;
		public static final int CO_OFS_NBR = SPE_CRT_CD + Len.SPE_CRT_CD;
		public static final int DTB_CD = CO_OFS_NBR + Len.CO_OFS_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_NOT_CO_ROWS = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int CO_CD = 5;
		public static final int CO_DES = 30;
		public static final int SPE_CRT_CD = 5;
		public static final int CO_OFS_NBR = 5;
		public static final int DTB_CD = 1;
		public static final int NOT_CO_ROW = ACT_NOT_TYP_CD + CO_CD + CO_DES + SPE_CRT_CD + CO_OFS_NBR + DTB_CD;
		public static final int NOT_CO_INFO = MAX_NOT_CO_ROWS + NOT_CO_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = NOT_CO_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CO_OFS_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
