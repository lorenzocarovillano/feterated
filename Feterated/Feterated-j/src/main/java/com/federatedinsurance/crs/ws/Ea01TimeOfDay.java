/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-TIME-OF-DAY<br>
 * Variable: EA-01-TIME-OF-DAY from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea01TimeOfDay {

	//==== PROPERTIES ====
	//Original name: EA-01-HOURS
	private String hours = DefaultValues.stringVal(Len.HOURS);
	//Original name: FILLER-EA-01-TIME-OF-DAY
	private char flr1 = ':';
	//Original name: EA-01-MINUTES
	private String minutes = DefaultValues.stringVal(Len.MINUTES);
	//Original name: FILLER-EA-01-TIME-OF-DAY-1
	private char flr2 = ':';
	//Original name: EA-01-SECONDS
	private String seconds = DefaultValues.stringVal(Len.SECONDS);

	//==== METHODS ====
	public byte[] getTimeOfDayBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, hours, Len.HOURS);
		position += Len.HOURS;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, minutes, Len.MINUTES);
		position += Len.MINUTES;
		MarshalByte.writeChar(buffer, position, flr2);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, seconds, Len.SECONDS);
		return buffer;
	}

	public void setHours(String hours) {
		this.hours = Functions.subString(hours, Len.HOURS);
	}

	public String getHours() {
		return this.hours;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMinutes(String minutes) {
		this.minutes = Functions.subString(minutes, Len.MINUTES);
	}

	public String getMinutes() {
		return this.minutes;
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setSeconds(String seconds) {
		this.seconds = Functions.subString(seconds, Len.SECONDS);
	}

	public String getSeconds() {
		return this.seconds;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HOURS = 2;
		public static final int MINUTES = 2;
		public static final int SECONDS = 2;
		public static final int FLR1 = 1;
		public static final int TIME_OF_DAY = HOURS + MINUTES + SECONDS + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
