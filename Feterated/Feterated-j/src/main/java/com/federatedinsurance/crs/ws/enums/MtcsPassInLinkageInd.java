/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: MTCS-PASS-IN-LINKAGE-IND<br>
 * Variable: MTCS-PASS-IN-LINKAGE-IND from copybook HALLMTCS<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MtcsPassInLinkageInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char PASS_IN_LINKAGE = 'L';
	public static final char DO_NOT_PASS_IN_LINKAGE = 'N';

	//==== METHODS ====
	public void setPassInLinkageInd(char passInLinkageInd) {
		this.value = passInLinkageInd;
	}

	public char getPassInLinkageInd() {
		return this.value;
	}
}
