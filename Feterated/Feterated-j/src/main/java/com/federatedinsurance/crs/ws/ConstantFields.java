/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program TS548099<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class ConstantFields extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: CF-CICS-APP
	private String cicsApp = "BATCHCLI";
	//Original name: CF-SEPARATOR
	private String separator = ";";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CONSTANT_FIELDS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setConstantFieldsBytes(buf);
	}

	public String getConstantFieldsFormatted() {
		return MarshalByteExt.bufferToStr(getConstantFieldsBytes());
	}

	public void setConstantFieldsBytes(byte[] buffer) {
		setConstantFieldsBytes(buffer, 1);
	}

	public byte[] getConstantFieldsBytes() {
		byte[] buffer = new byte[Len.CONSTANT_FIELDS];
		return getConstantFieldsBytes(buffer, 1);
	}

	public void setConstantFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		cicsApp = MarshalByte.readString(buffer, position, Len.CICS_APP);
		position += Len.CICS_APP;
		separator = MarshalByte.readString(buffer, position, Len.SEPARATOR);
	}

	public byte[] getConstantFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cicsApp, Len.CICS_APP);
		position += Len.CICS_APP;
		MarshalByte.writeString(buffer, position, separator, Len.SEPARATOR);
		return buffer;
	}

	public void setCicsApp(String cicsApp) {
		this.cicsApp = Functions.subString(cicsApp, Len.CICS_APP);
	}

	public String getCicsApp() {
		return this.cicsApp;
	}

	public void setSeparator(String separator) {
		this.separator = Functions.subString(separator, Len.SEPARATOR);
	}

	public String getSeparator() {
		return this.separator;
	}

	public String getSeparatorFormatted() {
		return Functions.padBlanks(getSeparator(), Len.SEPARATOR);
	}

	@Override
	public byte[] serialize() {
		return getConstantFieldsBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_APP = 8;
		public static final int SEPARATOR = 2;
		public static final int CONSTANT_FIELDS = CICS_APP + SEPARATOR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
