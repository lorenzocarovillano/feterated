/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.federatedinsurance.crs.ws.occurs.TpPolInf;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B90P0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b90p0Data {

	//==== PROPERTIES ====
	public static final int TP_POL_INF_MAXOCCURS = 50;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b90p0 constantFields = new ConstantFieldsXz0b90p0();
	//Original name: SS-PC
	private short ssPc = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-EM
	private short ssEm = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-VLD-POL-FLAG
	private boolean swVldPolFlag = false;
	//Original name: SW-FIRST-SELECT-FLAG
	private boolean swFirstSelectFlag = true;
	//Original name: TP-POL-INF
	private TpPolInf[] tpPolInf = new TpPolInf[TP_POL_INF_MAXOCCURS];
	//Original name: IX-TP
	private int ixTp = 1;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b90p0 workingStorageArea = new WorkingStorageAreaXz0b90p0();
	//Original name: WS-XZ0A90P0-ROW
	private WsXz0a90p0Row wsXz0a90p0Row = new WsXz0a90p0Row();
	//Original name: WS-XZ0A90P1-ROW
	private WsXz0a90p1Row wsXz0a90p1Row = new WsXz0a90p1Row();
	//Original name: WS-XZC030C1-ROW
	private DfhcommareaXzc03090 wsXzc030c1Row = new DfhcommareaXzc03090();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== CONSTRUCTORS ====
	public Xz0b90p0Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int tpPolInfIdx = 1; tpPolInfIdx <= TP_POL_INF_MAXOCCURS; tpPolInfIdx++) {
			tpPolInf[tpPolInfIdx - 1] = new TpPolInf();
		}
		initTableOfPolsSpaces();
	}

	public void setSsPc(short ssPc) {
		this.ssPc = ssPc;
	}

	public short getSsPc() {
		return this.ssPc;
	}

	public void setSsEm(short ssEm) {
		this.ssEm = ssEm;
	}

	public short getSsEm() {
		return this.ssEm;
	}

	public void setSwVldPolFlag(boolean swVldPolFlag) {
		this.swVldPolFlag = swVldPolFlag;
	}

	public boolean isSwVldPolFlag() {
		return this.swVldPolFlag;
	}

	public void setSwFirstSelectFlag(boolean swFirstSelectFlag) {
		this.swFirstSelectFlag = swFirstSelectFlag;
	}

	public boolean isSwFirstSelectFlag() {
		return this.swFirstSelectFlag;
	}

	public void initTableOfPolsSpaces() {
		for (int idx = 1; idx <= TP_POL_INF_MAXOCCURS; idx++) {
			tpPolInf[idx - 1].initTpPolInfSpaces();
		}
	}

	public void setIxTp(int ixTp) {
		this.ixTp = ixTp;
	}

	public int getIxTp() {
		return this.ixTp;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ConstantFieldsXz0b90p0 getConstantFields() {
		return constantFields;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	public TpPolInf getTpPolInf(int idx) {
		return tpPolInf[idx - 1];
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b90p0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a90p0Row getWsXz0a90p0Row() {
		return wsXz0a90p0Row;
	}

	public WsXz0a90p1Row getWsXz0a90p1Row() {
		return wsXz0a90p1Row;
	}

	public DfhcommareaXzc03090 getWsXzc030c1Row() {
		return wsXzc030c1Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
