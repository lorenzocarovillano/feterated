/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ003O-EXT-MSG-STA-LIS<br>
 * Variables: XZ003O-EXT-MSG-STA-LIS from copybook XZ03CI1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz003oExtMsgStaLis {

	//==== PROPERTIES ====
	//Original name: XZ003O-EXT-MSG-STA-CD
	private String staCd = DefaultValues.stringVal(Len.STA_CD);
	//Original name: XZ003O-EXT-MSG-STA-DES
	private String staDes = DefaultValues.stringVal(Len.STA_DES);
	//Original name: XZ003O-EXT-MSG-ID-RFR
	private String idRfr = DefaultValues.stringVal(Len.ID_RFR);

	//==== METHODS ====
	public void setExtMsgStaLisBytes(byte[] buffer, int offset) {
		int position = offset;
		staCd = MarshalByte.readString(buffer, position, Len.STA_CD);
		position += Len.STA_CD;
		staDes = MarshalByte.readString(buffer, position, Len.STA_DES);
		position += Len.STA_DES;
		idRfr = MarshalByte.readString(buffer, position, Len.ID_RFR);
	}

	public byte[] getExtMsgStaLisBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, staCd, Len.STA_CD);
		position += Len.STA_CD;
		MarshalByte.writeString(buffer, position, staDes, Len.STA_DES);
		position += Len.STA_DES;
		MarshalByte.writeString(buffer, position, idRfr, Len.ID_RFR);
		return buffer;
	}

	public void initExtMsgStaLisLowValues() {
		staCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.STA_CD);
		staDes = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.STA_DES);
		idRfr = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ID_RFR);
	}

	public void initExtMsgStaLisSpaces() {
		staCd = "";
		staDes = "";
		idRfr = "";
	}

	public void setStaCd(String staCd) {
		this.staCd = Functions.subString(staCd, Len.STA_CD);
	}

	public String getStaCd() {
		return this.staCd;
	}

	public void setStaDes(String staDes) {
		this.staDes = Functions.subString(staDes, Len.STA_DES);
	}

	public String getStaDes() {
		return this.staDes;
	}

	public void setIdRfr(String idRfr) {
		this.idRfr = Functions.subString(idRfr, Len.ID_RFR);
	}

	public String getIdRfr() {
		return this.idRfr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STA_CD = 25;
		public static final int STA_DES = 255;
		public static final int ID_RFR = 36;
		public static final int EXT_MSG_STA_LIS = STA_CD + STA_DES + ID_RFR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
