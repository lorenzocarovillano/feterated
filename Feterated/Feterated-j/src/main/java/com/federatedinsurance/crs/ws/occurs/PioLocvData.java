/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PIO-LOCV-DATA<br>
 * Variables: PIO-LOCV-DATA from copybook XZC0690O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class PioLocvData {

	//==== PROPERTIES ====
	//Original name: PIO-LOCV-CD
	private String pioLocvCd = DefaultValues.stringVal(Len.PIO_LOCV_CD);

	//==== METHODS ====
	public void setLocvDataBytes(byte[] buffer, int offset) {
		int position = offset;
		pioLocvCd = MarshalByte.readString(buffer, position, Len.PIO_LOCV_CD);
	}

	public byte[] getLocvDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, pioLocvCd, Len.PIO_LOCV_CD);
		return buffer;
	}

	public void initLocvDataSpaces() {
		pioLocvCd = "";
	}

	public void setPioLocvCd(String pioLocvCd) {
		this.pioLocvCd = Functions.subString(pioLocvCd, Len.PIO_LOCV_CD);
	}

	public String getPioLocvCd() {
		return this.pioLocvCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PIO_LOCV_CD = 3;
		public static final int LOCV_DATA = PIO_LOCV_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
