/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPolFrm1;

/**Original name: DCLACT-NOT-POL-FRM<br>
 * Variable: DCLACT-NOT-POL-FRM from copybook XZH00005<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotPolFrm implements IActNotPolFrm1 {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: FRM-SEQ-NBR
	private short frmSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);

	//==== METHODS ====
	public String getXzh004ActNotPolFrmRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh004ActNotPolFrmRowBytes());
	}

	public byte[] getXzh004ActNotPolFrmRowBytes() {
		byte[] buffer = new byte[Len.XZH004_ACT_NOT_POL_FRM_ROW];
		return getXzh004ActNotPolFrmRowBytes(buffer, 1);
	}

	public byte[] getXzh004ActNotPolFrmRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeBinaryShort(buffer, position, frmSeqNbr);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		return buffer;
	}

	public void initXzh004ActNotPolFrmRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		frmSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
		polNbr = "";
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setFrmSeqNbr(short frmSeqNbr) {
		this.frmSeqNbr = frmSeqNbr;
	}

	@Override
	public short getFrmSeqNbr() {
		return this.frmSeqNbr;
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int FRM_SEQ_NBR = 2;
		public static final int XZH004_ACT_NOT_POL_FRM_ROW = CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR + POL_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
