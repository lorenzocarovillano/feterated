/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: TO-POL-BY-POL-OWN<br>
 * Variable: TO-POL-BY-POL-OWN from program XZ0B9050<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class ToPolByPolOwn {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TO_POL_BY_POL_OWN);
	//Original name: TO-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: TO-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: TO-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);

	//==== METHODS ====
	public String getToPolByPolOwnFormatted() {
		return MarshalByteExt.bufferToStr(getToPolByPolOwnBytes());
	}

	public byte[] getToPolByPolOwnBytes() {
		byte[] buffer = new byte[Len.TO_POL_BY_POL_OWN];
		return getToPolByPolOwnBytes(buffer, 1);
	}

	public byte[] getToPolByPolOwnBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeDecimal(buffer, position, totFeeAmt.copy());
		return buffer;
	}

	public void initToPolByPolOwnHighValues() {
		polTypCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_TYP_CD);
		polNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR);
		totFeeAmt.setHigh();
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getToPolByPolOwnFormatted()).equals(END_OF_TABLE);
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_TYP_CD = 3;
		public static final int POL_NBR = 25;
		public static final int TOT_FEE_AMT = 10;
		public static final int TO_POL_BY_POL_OWN = POL_TYP_CD + POL_NBR + TOT_FEE_AMT;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
