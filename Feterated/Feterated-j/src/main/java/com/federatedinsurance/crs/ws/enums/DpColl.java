/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DP-COLL<br>
 * Variable: DP-COLL from program TS030299<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DpColl extends SerializableParameter {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.COLL);
	public static final String PRODUCTION = "PROD";
	public static final String DEVELOPMENT = "DEVL";

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.COLL;
	}

	@Override
	public void deserialize(byte[] buf) {
		setCollFromBuffer(buf);
	}

	public void setColl(String coll) {
		this.value = Functions.subString(coll, Len.COLL);
	}

	public void setCollFromBuffer(byte[] buffer) {
		value = MarshalByte.readString(buffer, 1, Len.COLL);
	}

	public String getColl() {
		return this.value;
	}

	public String getCollFormatted() {
		return Functions.padBlanks(getColl(), Len.COLL);
	}

	public void setProduction() {
		value = PRODUCTION;
	}

	public void setDevelopment() {
		value = DEVELOPMENT;
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getColl(), Len.COLL);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int COLL = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int COLL = 18;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int COLL = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
