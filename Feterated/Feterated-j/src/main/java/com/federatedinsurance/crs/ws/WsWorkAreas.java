/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WS-WORK-AREAS<br>
 * Variable: WS-WORK-AREAS from program HALOETRA<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsWorkAreas {

	//==== PROPERTIES ====
	//Original name: WS-RESPONSE-CODE
	private int responseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int responseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: WS-CICS-RESP-CODE
	private String cicsRespCode = DefaultValues.stringVal(Len.CICS_RESP_CODE);
	//Original name: WS-ABEND-ABSTIME
	private long abendAbstime = DefaultValues.LONG_VAL;
	//Original name: WS-ABEND-9-ABSTIME
	private String abend9Abstime = DefaultValues.stringVal(Len.ABEND9_ABSTIME);

	//==== METHODS ====
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode2(int responseCode2) {
		this.responseCode2 = responseCode2;
	}

	public int getResponseCode2() {
		return this.responseCode2;
	}

	public String getCicsRespFormatted() {
		return getCicsRespCodeFormatted();
	}

	public void setCicsRespCodeFormatted(String cicsRespCode) {
		this.cicsRespCode = Trunc.toUnsignedNumeric(cicsRespCode, Len.CICS_RESP_CODE);
	}

	public int getCicsRespCode() {
		return NumericDisplay.asInt(this.cicsRespCode);
	}

	public String getCicsRespCodeFormatted() {
		return this.cicsRespCode;
	}

	public void setAbendAbstime(long abendAbstime) {
		this.abendAbstime = abendAbstime;
	}

	public long getAbendAbstime() {
		return this.abendAbstime;
	}

	public void setAbend9Abstime(long abend9Abstime) {
		this.abend9Abstime = NumericDisplay.asString(abend9Abstime, Len.ABEND9_ABSTIME);
	}

	public long getAbend9Abstime() {
		return NumericDisplay.asLong(this.abend9Abstime);
	}

	public String getAbend9AbstimeFormatted() {
		return this.abend9Abstime;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TEMP_TIMESTAMP = 26;
		public static final int CICS_RESP_CODE = 9;
		public static final int ABEND9_ABSTIME = 15;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
