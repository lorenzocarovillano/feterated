/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT_FRM_REC, ACT_NOT_REC, REC_TYP]
 * 
 */
public interface IActNotFrmRecTyp extends BaseSqlTo {

	/**
	 * Host Variable REC-SEQ-NBR
	 * 
	 */
	short getRecSeqNbr();

	void setRecSeqNbr(short recSeqNbr);

	/**
	 * Host Variable REC-TYP-CD
	 * 
	 */
	String getRecTypCd();

	void setRecTypCd(String recTypCd);

	/**
	 * Host Variable REC-NM
	 * 
	 */
	String getRecNm();

	void setRecNm(String recNm);

	/**
	 * Nullable property for REC-NM
	 * 
	 */
	String getRecNmObj();

	void setRecNmObj(String recNmObj);

	/**
	 * Host Variable LIN-1-ADR
	 * 
	 */
	String getLin1Adr();

	void setLin1Adr(String lin1Adr);

	/**
	 * Nullable property for LIN-1-ADR
	 * 
	 */
	String getLin1AdrObj();

	void setLin1AdrObj(String lin1AdrObj);

	/**
	 * Host Variable LIN-2-ADR
	 * 
	 */
	String getLin2Adr();

	void setLin2Adr(String lin2Adr);

	/**
	 * Nullable property for LIN-2-ADR
	 * 
	 */
	String getLin2AdrObj();

	void setLin2AdrObj(String lin2AdrObj);

	/**
	 * Host Variable CIT-NM
	 * 
	 */
	String getCitNm();

	void setCitNm(String citNm);

	/**
	 * Nullable property for CIT-NM
	 * 
	 */
	String getCitNmObj();

	void setCitNmObj(String citNmObj);

	/**
	 * Host Variable ST-ABB
	 * 
	 */
	String getStAbb();

	void setStAbb(String stAbb);

	/**
	 * Nullable property for ST-ABB
	 * 
	 */
	String getStAbbObj();

	void setStAbbObj(String stAbbObj);

	/**
	 * Host Variable PST-CD
	 * 
	 */
	String getPstCd();

	void setPstCd(String pstCd);

	/**
	 * Nullable property for PST-CD
	 * 
	 */
	String getPstCdObj();

	void setPstCdObj(String pstCdObj);

	/**
	 * Host Variable CER-NBR
	 * 
	 */
	String getCerNbr();

	void setCerNbr(String cerNbr);

	/**
	 * Nullable property for CER-NBR
	 * 
	 */
	String getCerNbrObj();

	void setCerNbrObj(String cerNbrObj);

	/**
	 * Host Variable REC-LNG-DES
	 * 
	 */
	String getRecLngDes();

	void setRecLngDes(String recLngDes);

	/**
	 * Host Variable REC-SR-ORD-NBR
	 * 
	 */
	short getRecSrOrdNbr();

	void setRecSrOrdNbr(short recSrOrdNbr);
};
