/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Ea11FileSize;

/**Original name: EA-11-COM-FILE-TRUNCATION-MSG<br>
 * Variable: EA-11-COM-FILE-TRUNCATION-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea11ComFileTruncationMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-2
	private String flr3 = "COM FILE";
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-3
	private char flr4 = Types.QUOTE_CHAR;
	//Original name: EA-11-COM-FILE-NAME
	private String comFileName = DefaultValues.stringVal(Len.COM_FILE_NAME);
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-4
	private char flr5 = Types.QUOTE_CHAR;
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-5
	private String flr6 = " WAS TRUNCATED";
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-6
	private String flr7 = "TO";
	//Original name: EA-11-FILE-SIZE
	private Ea11FileSize fileSize = new Ea11FileSize();
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-7
	private String flr8 = " BYTES IN";
	//Original name: FILLER-EA-11-COM-FILE-TRUNCATION-MSG-8
	private String flr9 = "LENGTH";

	//==== METHODS ====
	public String getEa11ComFileTruncationMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa11ComFileTruncationMsgBytes());
	}

	public byte[] getEa11ComFileTruncationMsgBytes() {
		byte[] buffer = new byte[Len.EA11_COM_FILE_TRUNCATION_MSG];
		return getEa11ComFileTruncationMsgBytes(buffer, 1);
	}

	public byte[] getEa11ComFileTruncationMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeChar(buffer, position, flr4);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, comFileName, Len.COM_FILE_NAME);
		position += Len.COM_FILE_NAME;
		MarshalByte.writeChar(buffer, position, flr5);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, fileSize.value, Ea11FileSize.Len.FILE_SIZE);
		position += Ea11FileSize.Len.FILE_SIZE;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		position += Len.FLR8;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR9);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public char getFlr4() {
		return this.flr4;
	}

	public void setComFileName(String comFileName) {
		this.comFileName = Functions.subString(comFileName, Len.COM_FILE_NAME);
	}

	public String getComFileName() {
		return this.comFileName;
	}

	public char getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public String getFlr8() {
		return this.flr8;
	}

	public String getFlr9() {
		return this.flr9;
	}

	public Ea11FileSize getFileSize() {
		return fileSize;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int COM_FILE_NAME = 8;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int FLR6 = 15;
		public static final int FLR7 = 3;
		public static final int FLR8 = 10;
		public static final int FLR9 = 6;
		public static final int EA11_COM_FILE_TRUNCATION_MSG = COM_FILE_NAME + Ea11FileSize.Len.FILE_SIZE + 3 * FLR1 + FLR2 + FLR3 + FLR6 + FLR7
				+ FLR8 + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
