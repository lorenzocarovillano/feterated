/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ003O-POL-COV-INF<br>
 * Variables: XZ003O-POL-COV-INF from copybook XZ03CI1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz003oPolCovInf {

	//==== PROPERTIES ====
	public static final int COV_INF_MAXOCCURS = 10;
	//Original name: XZ003O-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZ003O-POL-ID
	private String polId = DefaultValues.stringVal(Len.POL_ID);
	//Original name: XZ003O-LOB-CD
	private String lobCd = DefaultValues.stringVal(Len.LOB_CD);
	//Original name: XZ003O-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZ003O-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZ003O-COV-INF
	private Xz003oCovInf[] covInf = new Xz003oCovInf[COV_INF_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xz003oPolCovInf() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int covInfIdx = 1; covInfIdx <= COV_INF_MAXOCCURS; covInfIdx++) {
			covInf[covInfIdx - 1] = new Xz003oCovInf();
		}
	}

	public void setPolCovInfBytes(byte[] buffer, int offset) {
		int position = offset;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		polId = MarshalByte.readString(buffer, position, Len.POL_ID);
		position += Len.POL_ID;
		lobCd = MarshalByte.readString(buffer, position, Len.LOB_CD);
		position += Len.LOB_CD;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		for (int idx = 1; idx <= COV_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				covInf[idx - 1].setCovInfBytes(buffer, position);
				position += Xz003oCovInf.Len.COV_INF;
			} else {
				covInf[idx - 1].initCovInfSpaces();
				position += Xz003oCovInf.Len.COV_INF;
			}
		}
	}

	public byte[] getPolCovInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polId, Len.POL_ID);
		position += Len.POL_ID;
		MarshalByte.writeString(buffer, position, lobCd, Len.LOB_CD);
		position += Len.LOB_CD;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		for (int idx = 1; idx <= COV_INF_MAXOCCURS; idx++) {
			covInf[idx - 1].getCovInfBytes(buffer, position);
			position += Xz003oCovInf.Len.COV_INF;
		}
		return buffer;
	}

	public void initPolCovInfLowValues() {
		polNbr = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.POL_NBR);
		polId = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.POL_ID);
		lobCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.LOB_CD);
		polEffDt = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.POL_EFF_DT);
		polExpDt = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.POL_EXP_DT);
		for (int idx = 1; idx <= COV_INF_MAXOCCURS; idx++) {
			covInf[idx - 1].initCovInfLowValues();
		}
	}

	public void initPolCovInfSpaces() {
		polNbr = "";
		polId = "";
		lobCd = "";
		polEffDt = "";
		polExpDt = "";
		for (int idx = 1; idx <= COV_INF_MAXOCCURS; idx++) {
			covInf[idx - 1].initCovInfSpaces();
		}
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolId(String polId) {
		this.polId = Functions.subString(polId, Len.POL_ID);
	}

	public String getPolId() {
		return this.polId;
	}

	public void setLobCd(String lobCd) {
		this.lobCd = Functions.subString(lobCd, Len.LOB_CD);
	}

	public String getLobCd() {
		return this.lobCd;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public Xz003oCovInf getCovInf(int idx) {
		return covInf[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 7;
		public static final int POL_ID = 16;
		public static final int LOB_CD = 3;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_COV_INF = POL_NBR + POL_ID + LOB_CD + POL_EFF_DT + POL_EXP_DT
				+ Xz003oPolCovInf.COV_INF_MAXOCCURS * Xz003oCovInf.Len.COV_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
