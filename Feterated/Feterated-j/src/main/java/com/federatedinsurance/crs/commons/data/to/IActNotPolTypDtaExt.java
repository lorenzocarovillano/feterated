/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT, ACT_NOT_POL, ACT_NOT_TYP, POL_DTA_EXT]
 * 
 */
public interface IActNotPolTypDtaExt extends BaseSqlTo {

	/**
	 * Host Variable NOT-EFF-DT
	 * 
	 */
	String getNotEffDt();

	void setNotEffDt(String notEffDt);

	/**
	 * Host Variable ACT-NOT-TYP-CD
	 * 
	 */
	String getActNotTypCd();

	void setActNotTypCd(String actNotTypCd);

	/**
	 * Host Variable NOT-DT
	 * 
	 */
	String getNotDt();

	void setNotDt(String notDt);

	/**
	 * Host Variable EMP-ID
	 * 
	 */
	String getEmpId();

	void setEmpId(String empId);

	/**
	 * Nullable property for EMP-ID
	 * 
	 */
	String getEmpIdObj();

	void setEmpIdObj(String empIdObj);

	/**
	 * Host Variable ACT-NOT-DES
	 * 
	 */
	String getActNotDes();

	void setActNotDes(String actNotDes);
};
