/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotPol;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclstsPolTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9060<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9060Data implements IActNot, IActNotPol {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b9060 constantFields = new ConstantFieldsXz0b9060();
	//Original name: EA-02-NOTHING-FOUND-MSG
	private Ea02NothingFoundMsgXz0b9060 ea02NothingFoundMsg = new Ea02NothingFoundMsgXz0b9060();
	//Original name: EA-03-NO-IMP-CNC-FOUND-MSG
	private Ea03NoImpCncFoundMsg ea03NoImpCncFoundMsg = new Ea03NoImpCncFoundMsg();
	/**Original name: NI-TOT-FEE-AMT<br>
	 * <pre>*  NULL-INDICATORS.</pre>*/
	private short niTotFeeAmt = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-ST-ABB
	private short niStAbb = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-NOT-EFF-DT
	private short niNotEffDt = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-POL-DUE-AMT
	private short niPolDueAmt = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-NOT-PRC-TS-TIME
	private String saNotPrcTsTime = DefaultValues.stringVal(Len.SA_NOT_PRC_TS_TIME);
	//Original name: SW-END-OF-CURSOR-FLAG
	private boolean swEndOfCursorFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9060 workingStorageArea = new WorkingStorageAreaXz0b9060();
	//Original name: WS-XZ0A9060-ROW
	private WsXz0a9060Row wsXz0a9060Row = new WsXz0a9060Row();
	//Original name: WS-XZ0A9061-ROW
	private WsXz0a9061Row wsXz0a9061Row = new WsXz0a9061Row();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-POL
	private DclactNotPol dclactNotPol = new DclactNotPol();
	//Original name: DCLSTS-POL-TYP
	private DclstsPolTyp dclstsPolTyp = new DclstsPolTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setNiTotFeeAmt(short niTotFeeAmt) {
		this.niTotFeeAmt = niTotFeeAmt;
	}

	public short getNiTotFeeAmt() {
		return this.niTotFeeAmt;
	}

	public void setNiStAbb(short niStAbb) {
		this.niStAbb = niStAbb;
	}

	public short getNiStAbb() {
		return this.niStAbb;
	}

	public void setNiNotEffDt(short niNotEffDt) {
		this.niNotEffDt = niNotEffDt;
	}

	public short getNiNotEffDt() {
		return this.niNotEffDt;
	}

	public void setNiPolDueAmt(short niPolDueAmt) {
		this.niPolDueAmt = niPolDueAmt;
	}

	public short getNiPolDueAmt() {
		return this.niPolDueAmt;
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		this.saNotPrcTsTime = Functions.subString(saNotPrcTsTime, Len.SA_NOT_PRC_TS_TIME);
	}

	@Override
	public String getSaNotPrcTsTime() {
		return this.saNotPrcTsTime;
	}

	public void setSwEndOfCursorFlag(boolean swEndOfCursorFlag) {
		this.swEndOfCursorFlag = swEndOfCursorFlag;
	}

	public boolean isSwEndOfCursorFlag() {
		return this.swEndOfCursorFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		throw new FieldNotMappedException("actNotTypCd");
	}

	@Override
	public String getActOwnAdrId() {
		throw new FieldNotMappedException("actOwnAdrId");
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		throw new FieldNotMappedException("actOwnAdrId");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getActTypCd() {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public void setActTypCd(String actTypCd) {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public String getActTypCdObj() {
		return getActTypCd();
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		setActTypCd(actTypCdObj);
	}

	@Override
	public short getAddCncDay() {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public Short getAddCncDayObj() {
		return (getAddCncDay());
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		setAddCncDay((addCncDayObj));
	}

	@Override
	public char getCerHldNotInd() {
		throw new FieldNotMappedException("cerHldNotInd");
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		throw new FieldNotMappedException("cerHldNotInd");
	}

	@Override
	public Character getCerHldNotIndObj() {
		return (getCerHldNotInd());
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		setCerHldNotInd((cerHldNotIndObj));
	}

	public ConstantFieldsXz0b9060 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotPol getDclactNotPol() {
		return dclactNotPol;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclstsPolTyp getDclstsPolTyp() {
		return dclstsPolTyp;
	}

	public Ea02NothingFoundMsgXz0b9060 getEa02NothingFoundMsg() {
		return ea02NothingFoundMsg;
	}

	public Ea03NoImpCncFoundMsg getEa03NoImpCncFoundMsg() {
		return ea03NoImpCncFoundMsg;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public String getNinAdrId() {
		throw new FieldNotMappedException("ninAdrId");
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		throw new FieldNotMappedException("ninAdrId");
	}

	@Override
	public String getNinCltId() {
		throw new FieldNotMappedException("ninCltId");
	}

	@Override
	public void setNinCltId(String ninCltId) {
		throw new FieldNotMappedException("ninCltId");
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		return dclactNot.getNotDt();
	}

	@Override
	public void setNotDt(String notDt) {
		this.dclactNot.setNotDt(notDt);
	}

	@Override
	public String getNotEffDt() {
		return dclactNotPol.getNotEffDt();
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.dclactNotPol.setNotEffDt(notEffDt);
	}

	@Override
	public String getNotEffDtObj() {
		if (getNiNotEffDt() >= 0) {
			return getNotEffDt();
		} else {
			return null;
		}
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		if (notEffDtObj != null) {
			setNotEffDt(notEffDtObj);
			setNiNotEffDt(((short) 0));
		} else {
			setNiNotEffDt(((short) -1));
		}
	}

	@Override
	public String getNotPrcTs() {
		return dclactNot.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.dclactNot.setNotPrcTs(notPrcTs);
	}

	@Override
	public String getPdcNbr() {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public String getPdcNbrObj() {
		return getPdcNbr();
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		setPdcNbr(pdcNbrObj);
	}

	@Override
	public String getPdcNm() {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public void setPdcNm(String pdcNm) {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public String getPdcNmObj() {
		return getPdcNm();
	}

	@Override
	public void setPdcNmObj(String pdcNmObj) {
		setPdcNm(pdcNmObj);
	}

	@Override
	public char getPolBilStaCd() {
		throw new FieldNotMappedException("polBilStaCd");
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		throw new FieldNotMappedException("polBilStaCd");
	}

	@Override
	public Character getPolBilStaCdObj() {
		return (getPolBilStaCd());
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		setPolBilStaCd((polBilStaCdObj));
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return dclactNotPol.getPolDueAmt();
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.dclactNotPol.setPolDueAmt(polDueAmt.copy());
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		if (getNiPolDueAmt() >= 0) {
			return getPolDueAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		if (polDueAmtObj != null) {
			setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
			setNiPolDueAmt(((short) 0));
		} else {
			setNiPolDueAmt(((short) -1));
		}
	}

	@Override
	public String getPolEffDt() {
		return dclactNotPol.getPolEffDt();
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.dclactNotPol.setPolEffDt(polEffDt);
	}

	@Override
	public String getPolExpDt() {
		return dclactNotPol.getPolExpDt();
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.dclactNotPol.setPolExpDt(polExpDt);
	}

	@Override
	public String getPolNbr() {
		return dclactNotPol.getPolNbr();
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.dclactNotPol.setPolNbr(polNbr);
	}

	@Override
	public String getPolPriRskStAbb() {
		return dclactNotPol.getPolPriRskStAbb();
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.dclactNotPol.setPolPriRskStAbb(polPriRskStAbb);
	}

	@Override
	public String getPolTypCd() {
		return dclactNotPol.getPolTypCd();
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.dclactNotPol.setPolTypCd(polTypCd);
	}

	@Override
	public String getReaDes() {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public void setReaDes(String reaDes) {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public String getReaDesObj() {
		return getReaDes();
	}

	@Override
	public void setReaDesObj(String reaDesObj) {
		setReaDes(reaDesObj);
	}

	@Override
	public String getSegCd() {
		throw new FieldNotMappedException("segCd");
	}

	@Override
	public void setSegCd(String segCd) {
		throw new FieldNotMappedException("segCd");
	}

	@Override
	public String getSegCdObj() {
		return getSegCd();
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		setSegCd(segCdObj);
	}

	@Override
	public String getStAbb() {
		return dclactNot.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.dclactNot.setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (getNiStAbb() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			setNiStAbb(((short) 0));
		} else {
			setNiStAbb(((short) -1));
		}
	}

	@Override
	public String getStaMdfTs() {
		throw new FieldNotMappedException("staMdfTs");
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		throw new FieldNotMappedException("staMdfTs");
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		return dclactNot.getTotFeeAmt();
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.dclactNot.setTotFeeAmt(totFeeAmt.copy());
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		if (getNiTotFeeAmt() >= 0) {
			return getTotFeeAmt().toString();
		} else {
			return null;
		}
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		if (totFeeAmtObj != null) {
			setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
			setNiTotFeeAmt(((short) 0));
		} else {
			setNiTotFeeAmt(((short) -1));
		}
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	@Override
	public char getWfStartedInd() {
		throw new FieldNotMappedException("wfStartedInd");
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		throw new FieldNotMappedException("wfStartedInd");
	}

	@Override
	public Character getWfStartedIndObj() {
		return (getWfStartedInd());
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		setWfStartedInd((wfStartedIndObj));
	}

	public WorkingStorageAreaXz0b9060 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9060Row getWsXz0a9060Row() {
		return wsXz0a9060Row;
	}

	public WsXz0a9061Row getWsXz0a9061Row() {
		return wsXz0a9061Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_NOT_PRC_TS_TIME = 8;
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
