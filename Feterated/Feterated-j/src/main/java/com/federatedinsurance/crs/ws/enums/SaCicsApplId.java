/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: SA-CICS-APPL-ID<br>
 * Variable: SA-CICS-APPL-ID from program TS548099<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class SaCicsApplId extends SerializableParameter {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CICS_APPL_ID);
	private static final String[] APPL_ID_IS_BLANK = new String[] { LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CICS_APPL_ID), "",
			LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CICS_APPL_ID) };
	public static final String REGION_IS_NIGHT = "CICSM";
	public static final String REGION_IS_TRANSITION = "CICSX";
	private static final String[] REGION_CAN_TRANSITION = new String[] { "CICS3", "CICS6", "CICS9", "CICSM" };

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.CICS_APPL_ID;
	}

	@Override
	public void deserialize(byte[] buf) {
		setCicsApplIdFromBuffer(buf);
	}

	public void setCicsApplId(String cicsApplId) {
		this.value = Functions.subString(cicsApplId, Len.CICS_APPL_ID);
	}

	public void setCicsApplIdFromBuffer(byte[] buffer) {
		value = MarshalByte.readString(buffer, 1, Len.CICS_APPL_ID);
	}

	public String getCicsApplId() {
		return this.value;
	}

	public String getCicsApplIdFormatted() {
		return Functions.padBlanks(getCicsApplId(), Len.CICS_APPL_ID);
	}

	public boolean isApplIdIsBlank() {
		return ArrayUtils.contains(APPL_ID_IS_BLANK, value);
	}

	public boolean isRegionIsNight() {
		return value.equals(REGION_IS_NIGHT);
	}

	public void setRegionIsNight() {
		value = REGION_IS_NIGHT;
	}

	public void setRegionIsTransition() {
		value = REGION_IS_TRANSITION;
	}

	public boolean isRegionCanTransition() {
		return ArrayUtils.contains(REGION_CAN_TRANSITION, value);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.strToBuffer(getCicsApplId(), Len.CICS_APPL_ID);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_APPL_ID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CICS_APPL_ID = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int CICS_APPL_ID = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
