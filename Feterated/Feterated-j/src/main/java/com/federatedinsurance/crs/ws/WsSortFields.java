/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-SORT-FIELDS<br>
 * Variable: WS-SORT-FIELDS from program XZ0P90C0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsSortFields {

	//==== PROPERTIES ====
	//Original name: WS-SF-LAST-SORTED-CER-NBR
	private String sfLastSortedCerNbr = DefaultValues.stringVal(Len.SF_LAST_SORTED_CER_NBR);
	//Original name: WS-SF-MIN-CER-NBR-FOUND
	private String sfMinCerNbrFound = DefaultValues.stringVal(Len.SF_MIN_CER_NBR_FOUND);
	//Original name: WS-SF-MIN-CER-IDX
	private short sfMinCerIdx = DefaultValues.SHORT_VAL;
	//Original name: WS-CER-NBR-SORT
	private String cerNbrSort = DefaultValues.stringVal(Len.CER_NBR_SORT);

	//==== METHODS ====
	public void setSfLastSortedCerNbr(String sfLastSortedCerNbr) {
		this.sfLastSortedCerNbr = Functions.subString(sfLastSortedCerNbr, Len.SF_LAST_SORTED_CER_NBR);
	}

	public String getSfLastSortedCerNbr() {
		return this.sfLastSortedCerNbr;
	}

	public void setSfMinCerNbrFound(String sfMinCerNbrFound) {
		this.sfMinCerNbrFound = Functions.subString(sfMinCerNbrFound, Len.SF_MIN_CER_NBR_FOUND);
	}

	public String getSfMinCerNbrFound() {
		return this.sfMinCerNbrFound;
	}

	public void setSfMinCerIdx(short sfMinCerIdx) {
		this.sfMinCerIdx = sfMinCerIdx;
	}

	public short getSfMinCerIdx() {
		return this.sfMinCerIdx;
	}

	public void setCerNbrSortFormatted(String cerNbrSort) {
		String field = Functions.leftPad(cerNbrSort, Len.CER_NBR_SORT, Types.SPACE_CHAR);
		this.cerNbrSort = Functions.subString(field, strLen(field) - Len.CER_NBR_SORT + 1, Len.CER_NBR_SORT);
	}

	public String getCerNbrSort() {
		return this.cerNbrSort;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SF_LAST_SORTED_CER_NBR = 25;
		public static final int SF_MIN_CER_NBR_FOUND = 25;
		public static final int CER_NBR_SORT = 25;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
