/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: DGRSTRG-PARMLIST<br>
 * Variable: DGRSTRG-PARMLIST from copybook CISLSTRG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DgrstrgParmlist {

	//==== PROPERTIES ====
	//Original name: DGRSTRG-STRING-POINTER
	private short stringPointer = ((short) 0);

	//==== METHODS ====
	public void setStringPointer(short stringPointer) {
		this.stringPointer = stringPointer;
	}

	public short getStringPointer() {
		return this.stringPointer;
	}
}
