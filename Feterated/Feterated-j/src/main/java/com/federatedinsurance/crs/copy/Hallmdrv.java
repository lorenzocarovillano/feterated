/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.notifier.IValueChangeListener;

/**Original name: HALLMDRV<br>
 * Copybook: HALLMDRV from copybook HALLMDRV<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Hallmdrv {

	//==== PROPERTIES ====
	public static final int UOW_MESSAGE_BYTE_MAXOCCURS = 32000;
	private int mdrvUowMessageByteListenerSize = UOW_MESSAGE_BYTE_MAXOCCURS;
	private IValueChangeListener mdrvUowMessageByteListener = new MdrvUowMessageByteListener();
	//Original name: MDRV-DRIVER-INFO
	private MdrvDriverInfo driverInfo = new MdrvDriverInfo();
	//Original name: MDRV-UOW-MESSAGE-BYTE
	private char[] uowMessageByte = new char[UOW_MESSAGE_BYTE_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Hallmdrv() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int uowMessageByteIdx = 1; uowMessageByteIdx <= UOW_MESSAGE_BYTE_MAXOCCURS; uowMessageByteIdx++) {
			setUowMessageByte(uowMessageByteIdx, DefaultValues.CHAR_VAL);
		}
	}

	public void setUowMessageByte(int uowMessageByteIdx, char uowMessageByte) {
		this.uowMessageByte[uowMessageByteIdx - 1] = uowMessageByte;
	}

	public char getUowMessageByte(int uowMessageByteIdx) {
		return this.uowMessageByte[uowMessageByteIdx - 1];
	}

	public MdrvDriverInfo getDriverInfo() {
		return driverInfo;
	}

	public IValueChangeListener getMdrvUowMessageByteListener() {
		return mdrvUowMessageByteListener;
	}

	//==== INNER CLASSES ====
	/**Original name: MDRV-UOW-MESSAGE-BYTE<br>*/
	public class MdrvUowMessageByteListener implements IValueChangeListener {

		//==== METHODS ====
		@Override
		public void change() {
			mdrvUowMessageByteListenerSize = UOW_MESSAGE_BYTE_MAXOCCURS;
		}

		@Override
		public void change(int value) {
			mdrvUowMessageByteListenerSize = value < 1 ? 0 : (value > UOW_MESSAGE_BYTE_MAXOCCURS ? UOW_MESSAGE_BYTE_MAXOCCURS : value);
		}
	}
}
