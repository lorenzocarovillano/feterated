/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import org.apache.commons.lang3.ArrayUtils;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class SaveAreaTs547099 {

	//==== PROPERTIES ====
	//Original name: SA-MAX-PIPE-COUNT
	private int maxPipeCount = DefaultValues.BIN_INT_VAL;
	//Original name: SA-COMM-LENGTH
	private int commLength = DefaultValues.BIN_INT_VAL;
	//Original name: SA-DATA-LENGTH
	private int dataLength = DefaultValues.BIN_INT_VAL;
	//Original name: SA-CICS-APPL-ID
	private String cicsApplId = DefaultValues.stringVal(Len.CICS_APPL_ID);
	private static final String[] CICS_APPL_ID_IS_BLANK = new String[] { LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CICS_APPL_ID), "",
			LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CICS_APPL_ID) };
	//Original name: SA-CICS-PGM-TO-CALL
	private String cicsPgmToCall = DefaultValues.stringVal(Len.CICS_PGM_TO_CALL);
	private static final String[] CICS_PGM_NM_IS_BLANK = new String[] { LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CICS_PGM_TO_CALL), "",
			LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CICS_PGM_TO_CALL) };
	//Original name: SA-TARGET-TRAN-ID
	private String targetTranId = DefaultValues.stringVal(Len.TARGET_TRAN_ID);
	private static final String[] TARGET_TRAN_ID_IS_BLANK = new String[] { LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.TARGET_TRAN_ID), "",
			LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TARGET_TRAN_ID) };
	//Original name: SA-INPUT-DATA-LEN
	private int inputDataLen = DefaultValues.BIN_INT_VAL;
	//Original name: SA-CICS-PIPE-MAINTENANCE-CNT
	private String cicsPipeMaintenanceCnt = DefaultValues.stringVal(Len.CICS_PIPE_MAINTENANCE_CNT);
	//Original name: SA-SYNCONRETURN-IND
	private char synconreturnInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setMaxPipeCount(int maxPipeCount) {
		this.maxPipeCount = maxPipeCount;
	}

	public int getMaxPipeCount() {
		return this.maxPipeCount;
	}

	public void setCommLength(int commLength) {
		this.commLength = commLength;
	}

	public void setCommLengthFromBuffer(byte[] buffer) {
		commLength = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getCommLength() {
		return this.commLength;
	}

	public String getCommLengthFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getCommLength()).toString();
	}

	public void setDataLength(int dataLength) {
		this.dataLength = dataLength;
	}

	public void setDataLengthFromBuffer(byte[] buffer) {
		dataLength = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getDataLength() {
		return this.dataLength;
	}

	public String getDataLengthFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getDataLength()).toString();
	}

	public void setCicsApplId(String cicsApplId) {
		this.cicsApplId = Functions.subString(cicsApplId, Len.CICS_APPL_ID);
	}

	public String getCicsApplId() {
		return this.cicsApplId;
	}

	public boolean isCicsApplIdIsBlank() {
		return ArrayUtils.contains(CICS_APPL_ID_IS_BLANK, cicsApplId);
	}

	public void setCicsPgmToCall(String cicsPgmToCall) {
		this.cicsPgmToCall = Functions.subString(cicsPgmToCall, Len.CICS_PGM_TO_CALL);
	}

	public String getCicsPgmToCall() {
		return this.cicsPgmToCall;
	}

	public boolean isCicsPgmNmIsBlank() {
		return ArrayUtils.contains(CICS_PGM_NM_IS_BLANK, cicsPgmToCall);
	}

	public void setTargetTranId(String targetTranId) {
		this.targetTranId = Functions.subString(targetTranId, Len.TARGET_TRAN_ID);
	}

	public String getTargetTranId() {
		return this.targetTranId;
	}

	public boolean isTargetTranIdIsBlank() {
		return ArrayUtils.contains(TARGET_TRAN_ID_IS_BLANK, targetTranId);
	}

	public void setInputDataLen(int inputDataLen) {
		this.inputDataLen = inputDataLen;
	}

	public int getInputDataLen() {
		return this.inputDataLen;
	}

	public void setCicsPipeMaintenanceCnt(String cicsPipeMaintenanceCnt) {
		this.cicsPipeMaintenanceCnt = Functions.subString(cicsPipeMaintenanceCnt, Len.CICS_PIPE_MAINTENANCE_CNT);
	}

	public String getCicsPipeMaintenanceCnt() {
		return this.cicsPipeMaintenanceCnt;
	}

	public String getCicsPipeMaintenanceCntFormatted() {
		return Functions.padBlanks(getCicsPipeMaintenanceCnt(), Len.CICS_PIPE_MAINTENANCE_CNT);
	}

	public void setSynconreturnInd(char synconreturnInd) {
		this.synconreturnInd = synconreturnInd;
	}

	public void setSynconreturnIndFromBuffer(byte[] buffer) {
		synconreturnInd = MarshalByte.readChar(buffer, 1);
	}

	public char getSynconreturnInd() {
		return this.synconreturnInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_APPL_ID = 8;
		public static final int CICS_PGM_TO_CALL = 8;
		public static final int TARGET_TRAN_ID = 4;
		public static final int CICS_PIPE_MAINTENANCE_CNT = 200;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
