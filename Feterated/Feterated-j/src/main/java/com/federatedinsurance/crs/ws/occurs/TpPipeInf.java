/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;
import com.federatedinsurance.crs.ws.enums.TpPiOpenCloseTally;
import com.federatedinsurance.crs.ws.enums.TpPiPipeStaCd;
import com.federatedinsurance.crs.ws.enums.TpPiRiRedirPipe;

/**Original name: TP-PIPE-INF<br>
 * Variables: TP-PIPE-INF from program TS547099<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class TpPipeInf {

	//==== PROPERTIES ====
	//Original name: TP-PI-CICS-APPL-ID
	private String cicsApplId = DefaultValues.stringVal(Len.CICS_APPL_ID);
	//Original name: TP-PI-USER-TOKEN
	private int userToken = DefaultValues.BIN_INT_VAL;
	//Original name: TP-PI-PIPE-TOKEN
	private int pipeToken = DefaultValues.BIN_INT_VAL;
	//Original name: TP-PI-OPEN-CLOSE-TALLY
	private TpPiOpenCloseTally openCloseTally = new TpPiOpenCloseTally();
	//Original name: TP-PI-PIPE-STA-CD
	private TpPiPipeStaCd pipeStaCd = new TpPiPipeStaCd();
	//Original name: TP-PI-RI-REDIR-PIPE
	private TpPiRiRedirPipe riRedirPipe = new TpPiRiRedirPipe();
	//Original name: TP-PI-RI-NBR-REDIRECTS
	private short riNbrRedirects = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public void setCicsApplId(String cicsApplId) {
		this.cicsApplId = Functions.subString(cicsApplId, Len.CICS_APPL_ID);
	}

	public String getCicsApplId() {
		return this.cicsApplId;
	}

	public void setUserToken(int userToken) {
		this.userToken = userToken;
	}

	public void setUserTokenFromBuffer(byte[] buffer) {
		userToken = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getUserToken() {
		return this.userToken;
	}

	public String getUserTokenFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getUserToken()).toString();
	}

	public void setPipeToken(int pipeToken) {
		this.pipeToken = pipeToken;
	}

	public void setPipeTokenFromBuffer(byte[] buffer) {
		pipeToken = MarshalByte.readBinaryInt(buffer, 1);
	}

	public int getPipeToken() {
		return this.pipeToken;
	}

	public String getPipeTokenFormatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getPipeToken()).toString();
	}

	public void setRiNbrRedirects(short riNbrRedirects) {
		this.riNbrRedirects = riNbrRedirects;
	}

	public short getRiNbrRedirects() {
		return this.riNbrRedirects;
	}

	public TpPiOpenCloseTally getOpenCloseTally() {
		return openCloseTally;
	}

	public TpPiPipeStaCd getPipeStaCd() {
		return pipeStaCd;
	}

	public TpPiRiRedirPipe getRiRedirPipe() {
		return riRedirPipe;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_APPL_ID = 8;
		public static final int USER_TOKEN = 4;
		public static final int PIPE_TOKEN = 4;
		public static final int RI_NBR_REDIRECTS = 2;
		public static final int REDIRECTION_INF = TpPiRiRedirPipe.Len.RI_REDIR_PIPE + RI_NBR_REDIRECTS;
		public static final int TP_PIPE_INF = CICS_APPL_ID + USER_TOKEN + PIPE_TOKEN + TpPiOpenCloseTally.Len.OPEN_CLOSE_TALLY
				+ TpPiPipeStaCd.Len.PIPE_STA_CD + REDIRECTION_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
