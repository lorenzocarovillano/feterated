/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q90P0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q90p0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q90p0() {
	}

	public LFrameworkRequestAreaXz0q90p0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9p0qMaxPolRows(short xza9p0qMaxPolRows) {
		writeBinaryShort(Pos.XZA9P0_MAX_POL_ROWS, xza9p0qMaxPolRows);
	}

	/**Original name: XZA9P0Q-MAX-POL-ROWS<br>*/
	public short getXza9p0qMaxPolRows() {
		return readBinaryShort(Pos.XZA9P0_MAX_POL_ROWS);
	}

	public void setXza9p0qCsrActNbr(String xza9p0qCsrActNbr) {
		writeString(Pos.XZA9P0_CSR_ACT_NBR, xza9p0qCsrActNbr, Len.XZA9P0_CSR_ACT_NBR);
	}

	/**Original name: XZA9P0Q-CSR-ACT-NBR<br>*/
	public String getXza9p0qCsrActNbr() {
		return readString(Pos.XZA9P0_CSR_ACT_NBR, Len.XZA9P0_CSR_ACT_NBR);
	}

	public void setXza9p0qNotPrcTs(String xza9p0qNotPrcTs) {
		writeString(Pos.XZA9P0_NOT_PRC_TS, xza9p0qNotPrcTs, Len.XZA9P0_NOT_PRC_TS);
	}

	/**Original name: XZA9P0Q-NOT-PRC-TS<br>*/
	public String getXza9p0qNotPrcTs() {
		return readString(Pos.XZA9P0_NOT_PRC_TS, Len.XZA9P0_NOT_PRC_TS);
	}

	public void setXza9p0qUserid(String xza9p0qUserid) {
		writeString(Pos.XZA9P0_USERID, xza9p0qUserid, Len.XZA9P0_USERID);
	}

	/**Original name: XZA9P0Q-USERID<br>*/
	public String getXza9p0qUserid() {
		return readString(Pos.XZA9P0_USERID, Len.XZA9P0_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A90P0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9P0_GET_CERT_POL_LIST_KEY = L_FW_REQ_XZ0A90P0;
		public static final int XZA9P0_MAX_POL_ROWS = XZA9P0_GET_CERT_POL_LIST_KEY;
		public static final int XZA9P0_CSR_ACT_NBR = XZA9P0_MAX_POL_ROWS + Len.XZA9P0_MAX_POL_ROWS;
		public static final int XZA9P0_NOT_PRC_TS = XZA9P0_CSR_ACT_NBR + Len.XZA9P0_CSR_ACT_NBR;
		public static final int XZA9P0_USERID = XZA9P0_NOT_PRC_TS + Len.XZA9P0_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9P0_MAX_POL_ROWS = 2;
		public static final int XZA9P0_CSR_ACT_NBR = 9;
		public static final int XZA9P0_NOT_PRC_TS = 26;
		public static final int XZA9P0_USERID = 8;
		public static final int XZA9P0_GET_CERT_POL_LIST_KEY = XZA9P0_MAX_POL_ROWS + XZA9P0_CSR_ACT_NBR + XZA9P0_NOT_PRC_TS + XZA9P0_USERID;
		public static final int L_FW_REQ_XZ0A90P0 = XZA9P0_GET_CERT_POL_LIST_KEY;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A90P0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
