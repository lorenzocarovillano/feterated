/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CF-CONTAINER-INFO<br>
 * Variable: CF-CONTAINER-INFO from program XZC09090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfContainerInfoXzc09090 {

	//==== PROPERTIES ====
	//Original name: CF-CI-CICS-CHANNEL
	private String cicsChannel = "XZTRSDTLCHN";
	//Original name: CF-CI-INP-CONTAINER
	private String inpContainer = "XZPOLTRSINPUT";
	//Original name: CF-CI-OUP-CONTAINER
	private String oupContainer = "XZPOLTRSOUTPUT";
	//Original name: CF-CI-IVORYH-CONTAINER
	private String ivoryhContainer = "IVORYH";

	//==== METHODS ====
	public String getCicsChannel() {
		return this.cicsChannel;
	}

	public String getCicsChannelFormatted() {
		return Functions.padBlanks(getCicsChannel(), Len.CICS_CHANNEL);
	}

	public String getInpContainer() {
		return this.inpContainer;
	}

	public String getInpContainerFormatted() {
		return Functions.padBlanks(getInpContainer(), Len.INP_CONTAINER);
	}

	public String getOupContainer() {
		return this.oupContainer;
	}

	public String getOupContainerFormatted() {
		return Functions.padBlanks(getOupContainer(), Len.OUP_CONTAINER);
	}

	public String getIvoryhContainer() {
		return this.ivoryhContainer;
	}

	public String getIvoryhContainerFormatted() {
		return Functions.padBlanks(getIvoryhContainer(), Len.IVORYH_CONTAINER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int IVORYH_CONTAINER = 16;
		public static final int CICS_CHANNEL = 16;
		public static final int INP_CONTAINER = 16;
		public static final int OUP_CONTAINER = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
