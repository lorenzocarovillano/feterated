/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.EstoRecordingLevel;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ESTO-INPUT-KEY<br>
 * Variable: ESTO-INPUT-KEY from copybook HALLESTO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class EstoInputKey {

	//==== PROPERTIES ====
	//Original name: ESTO-STORE-ID
	private String storeId = DefaultValues.stringVal(Len.STORE_ID);
	//Original name: ESTO-RECORDING-LEVEL
	private EstoRecordingLevel recordingLevel = new EstoRecordingLevel();
	//Original name: ESTO-ERR-SEQ-NUM
	private String errSeqNum = DefaultValues.stringVal(Len.ERR_SEQ_NUM);

	//==== METHODS ====
	public void setEstoInputKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		storeId = MarshalByte.readString(buffer, position, Len.STORE_ID);
		position += Len.STORE_ID;
		recordingLevel.setRecordingLevel(MarshalByte.readString(buffer, position, EstoRecordingLevel.Len.RECORDING_LEVEL));
		position += EstoRecordingLevel.Len.RECORDING_LEVEL;
		errSeqNum = MarshalByte.readFixedString(buffer, position, Len.ERR_SEQ_NUM);
	}

	public byte[] getEstoInputKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, storeId, Len.STORE_ID);
		position += Len.STORE_ID;
		MarshalByte.writeString(buffer, position, recordingLevel.getRecordingLevel(), EstoRecordingLevel.Len.RECORDING_LEVEL);
		position += EstoRecordingLevel.Len.RECORDING_LEVEL;
		MarshalByte.writeString(buffer, position, errSeqNum, Len.ERR_SEQ_NUM);
		return buffer;
	}

	public void setStoreId(String storeId) {
		this.storeId = Functions.subString(storeId, Len.STORE_ID);
	}

	public String getStoreId() {
		return this.storeId;
	}

	public void setEstoErrSeqNum(int estoErrSeqNum) {
		this.errSeqNum = NumericDisplay.asString(estoErrSeqNum, Len.ERR_SEQ_NUM);
	}

	public void setErrSeqNumFormatted(String errSeqNum) {
		this.errSeqNum = Trunc.toUnsignedNumeric(errSeqNum, Len.ERR_SEQ_NUM);
	}

	public int getErrSeqNum() {
		return NumericDisplay.asInt(this.errSeqNum);
	}

	public String getEstoErrSeqNumFormatted() {
		return this.errSeqNum;
	}

	public EstoRecordingLevel getRecordingLevel() {
		return recordingLevel;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int STORE_ID = 32;
		public static final int ERR_SEQ_NUM = 5;
		public static final int ESTO_INPUT_KEY = STORE_ID + EstoRecordingLevel.Len.RECORDING_LEVEL + ERR_SEQ_NUM;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
