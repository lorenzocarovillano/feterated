/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-CURSOR1-SW<br>
 * Variable: WS-END-OF-CURSOR1-SW from program HALRLOMG<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfCursor1Sw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_CURSOR1 = 'Y';
	public static final char NOT_END_OF_CURSOR1 = 'N';

	//==== METHODS ====
	public void setEndOfCursor1Sw(char endOfCursor1Sw) {
		this.value = endOfCursor1Sw;
	}

	public char getEndOfCursor1Sw() {
		return this.value;
	}

	public boolean isEndOfCursor1() {
		return value == END_OF_CURSOR1;
	}

	public void setEndOfCursor1() {
		value = END_OF_CURSOR1;
	}

	public void setNotEndOfCursor1() {
		value = NOT_END_OF_CURSOR1;
	}
}
