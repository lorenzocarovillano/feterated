/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: WS-DATE-ARRAY<br>
 * Variable: WS-DATE-ARRAY from program XPIODAT<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsDateArray extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int ARRAY_CHAR_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public WsDateArray() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_DATE_ARRAY;
	}

	public void setWsDateArrayFormatted(String data) {
		writeString(Pos.WS_DATE_ARRAY, data, Len.WS_DATE_ARRAY);
	}

	public String getWsDateArrayFormatted() {
		return readFixedString(Pos.WS_DATE_ARRAY, Len.WS_DATE_ARRAY);
	}

	public void initWsDateArrayZeroes() {
		fill(Pos.WS_DATE_ARRAY, Len.WS_DATE_ARRAY, '0');
	}

	public void initWsDateArraySpaces() {
		fill(Pos.WS_DATE_ARRAY, Len.WS_DATE_ARRAY, Types.SPACE_CHAR);
	}

	public void setArrayCharBytes(int arrayCharIdx, byte[] buffer) {
		setArrayCharBytes(arrayCharIdx, buffer, 1);
	}

	/**Original name: WS-DATE-ARRAY-CHAR<br>*/
	public byte[] getArrayCharBytes(int arrayCharIdx) {
		byte[] buffer = new byte[Len.ARRAY_CHAR];
		return getArrayCharBytes(arrayCharIdx, buffer, 1);
	}

	public void setArrayCharBytes(int arrayCharIdx, byte[] buffer, int offset) {
		int position = Pos.wsDateArrayChar(arrayCharIdx - 1);
		setBytes(buffer, offset, Len.ARRAY_CHAR, position);
	}

	public byte[] getArrayCharBytes(int arrayCharIdx, byte[] buffer, int offset) {
		int position = Pos.wsDateArrayChar(arrayCharIdx - 1);
		getBytes(buffer, offset, Len.ARRAY_CHAR, position);
		return buffer;
	}

	public void initArrayCharZeroes(int arrayCharIdx) {
		fill(Pos.wsDateArrayChar((arrayCharIdx - 1)), Len.ARRAY_CHAR, '0');
	}

	public void setCharFld(int charFldIdx, char charFld) {
		int position = Pos.wsDateChar(charFldIdx - 1);
		writeChar(position, charFld);
	}

	/**Original name: WS-DATE-CHAR<br>*/
	public char getCharFld(int charFldIdx) {
		int position = Pos.wsDateChar(charFldIdx - 1);
		return readChar(position);
	}

	public void setArrayJul7yFormatted(String arrayJul7y) {
		writeString(Pos.ARRAY_JUL7Y, Trunc.toUnsignedNumeric(arrayJul7y, Len.ARRAY_JUL7Y), Len.ARRAY_JUL7Y);
	}

	/**Original name: WS-DATE-ARRAY-JUL7Y<br>*/
	public short getArrayJul7y() {
		return readNumDispUnsignedShort(Pos.ARRAY_JUL7Y, Len.ARRAY_JUL7Y);
	}

	public String getArrayJul7yFormatted() {
		return readFixedString(Pos.ARRAY_JUL7Y, Len.ARRAY_JUL7Y);
	}

	public void setArrayJul7dFormatted(String arrayJul7d) {
		writeString(Pos.ARRAY_JUL7D, Trunc.toUnsignedNumeric(arrayJul7d, Len.ARRAY_JUL7D), Len.ARRAY_JUL7D);
	}

	/**Original name: WS-DATE-ARRAY-JUL7D<br>*/
	public short getArrayJul7d() {
		return readNumDispUnsignedShort(Pos.ARRAY_JUL7D, Len.ARRAY_JUL7D);
	}

	public String getArrayJul7dFormatted() {
		return readFixedString(Pos.ARRAY_JUL7D, Len.ARRAY_JUL7D);
	}

	public void setArrayJul7ts(char arrayJul7ts) {
		writeChar(Pos.ARRAY_JUL7TS, arrayJul7ts);
	}

	/**Original name: WS-DATE-ARRAY-JUL7TS<br>*/
	public char getArrayJul7ts() {
		return readChar(Pos.ARRAY_JUL7TS);
	}

	public void setArrayJul7t(String arrayJul7t) {
		writeString(Pos.ARRAY_JUL7T, arrayJul7t, Len.ARRAY_JUL7T);
	}

	/**Original name: WS-DATE-ARRAY-JUL7T<br>*/
	public String getArrayJul7t() {
		return readString(Pos.ARRAY_JUL7T, Len.ARRAY_JUL7T);
	}

	public String getArrayJul7tFormatted() {
		return Functions.padBlanks(getArrayJul7t(), Len.ARRAY_JUL7T);
	}

	public String getArrayJul5yFormatted() {
		return readFixedString(Pos.ARRAY_JUL5Y, Len.ARRAY_JUL5Y);
	}

	public String getArrayJul5dFormatted() {
		return readFixedString(Pos.ARRAY_JUL5D, Len.ARRAY_JUL5D);
	}

	/**Original name: WS-DATE-ARRAY-JUL5TS<br>*/
	public char getArrayJul5ts() {
		return readChar(Pos.ARRAY_JUL5TS);
	}

	/**Original name: WS-DATE-ARRAY-JUL5T<br>*/
	public String getArrayJul5t() {
		return readString(Pos.ARRAY_JUL5T, Len.ARRAY_JUL5T);
	}

	public String getArrayJul5tFormatted() {
		return Functions.padBlanks(getArrayJul5t(), Len.ARRAY_JUL5T);
	}

	public void setArrayAdt(String arrayAdt) {
		writeString(Pos.ARRAY_ADT, arrayAdt, Len.ARRAY_ADT);
	}

	/**Original name: WS-DATE-ARRAY-ADT<br>*/
	public String getArrayAdt() {
		return readString(Pos.ARRAY_ADT, Len.ARRAY_ADT);
	}

	public void setArrayAdday(String arrayAdday) {
		writeString(Pos.ARRAY_ADDAY, arrayAdday, Len.ARRAY_ADDAY);
	}

	/**Original name: WS-DATE-ARRAY-ADDAY<br>*/
	public String getArrayAdday() {
		return readString(Pos.ARRAY_ADDAY, Len.ARRAY_ADDAY);
	}

	public void setArrayAdmonth(String arrayAdmonth) {
		writeString(Pos.ARRAY_ADMONTH, arrayAdmonth, Len.ARRAY_ADMONTH);
	}

	/**Original name: WS-DATE-ARRAY-ADMONTH<br>*/
	public String getArrayAdmonth() {
		return readString(Pos.ARRAY_ADMONTH, Len.ARRAY_ADMONTH);
	}

	public void setArrayAmt(String arrayAmt) {
		writeString(Pos.ARRAY_AMT, arrayAmt, Len.ARRAY_AMT);
	}

	/**Original name: WS-DATE-ARRAY-AMT<br>*/
	public String getArrayAmt() {
		return readString(Pos.ARRAY_AMT, Len.ARRAY_AMT);
	}

	public void setArrayAmmonth(String arrayAmmonth) {
		writeString(Pos.ARRAY_AMMONTH, arrayAmmonth, Len.ARRAY_AMMONTH);
	}

	/**Original name: WS-DATE-ARRAY-AMMONTH<br>*/
	public String getArrayAmmonth() {
		return readString(Pos.ARRAY_AMMONTH, Len.ARRAY_AMMONTH);
	}

	public void setArrayAst(String arrayAst) {
		writeString(Pos.ARRAY_AST, arrayAst, Len.ARRAY_AST);
	}

	/**Original name: WS-DATE-ARRAY-AST<br>*/
	public String getArrayAst() {
		return readString(Pos.ARRAY_AST, Len.ARRAY_AST);
	}

	public void setArrayAsday(String arrayAsday) {
		writeString(Pos.ARRAY_ASDAY, arrayAsday, Len.ARRAY_ASDAY);
	}

	/**Original name: WS-DATE-ARRAY-ASDAY<br>*/
	public String getArrayAsday() {
		return readString(Pos.ARRAY_ASDAY, Len.ARRAY_ASDAY);
	}

	public void setArrayAsmonth(String arrayAsmonth) {
		writeString(Pos.ARRAY_ASMONTH, arrayAsmonth, Len.ARRAY_ASMONTH);
	}

	/**Original name: WS-DATE-ARRAY-ASMONTH<br>*/
	public String getArrayAsmonth() {
		return readString(Pos.ARRAY_ASMONTH, Len.ARRAY_ASMONTH);
	}

	public void setArrayAsmonths(char arrayAsmonths) {
		writeChar(Pos.ARRAY_ASMONTHS, arrayAsmonths);
	}

	/**Original name: WS-DATE-ARRAY-ASMONTHS<br>*/
	public char getArrayAsmonths() {
		return readChar(Pos.ARRAY_ASMONTHS);
	}

	public void setArrayAsyear(String arrayAsyear) {
		writeString(Pos.ARRAY_ASYEAR, arrayAsyear, Len.ARRAY_ASYEAR);
	}

	/**Original name: WS-DATE-ARRAY-ASYEAR<br>*/
	public String getArrayAsyear() {
		return readString(Pos.ARRAY_ASYEAR, Len.ARRAY_ASYEAR);
	}

	public void setArrayYmd4yFormatted(String arrayYmd4y) {
		writeString(Pos.ARRAY_YMD4Y, Trunc.toUnsignedNumeric(arrayYmd4y, Len.ARRAY_YMD4Y), Len.ARRAY_YMD4Y);
	}

	/**Original name: WS-DATE-ARRAY-YMD4Y<br>*/
	public short getArrayYmd4y() {
		return readNumDispUnsignedShort(Pos.ARRAY_YMD4Y, Len.ARRAY_YMD4Y);
	}

	public String getArrayYmd4yFormatted() {
		return readFixedString(Pos.ARRAY_YMD4Y, Len.ARRAY_YMD4Y);
	}

	public void setArrayYmd4ys(char arrayYmd4ys) {
		writeChar(Pos.ARRAY_YMD4YS, arrayYmd4ys);
	}

	/**Original name: WS-DATE-ARRAY-YMD4YS<br>*/
	public char getArrayYmd4ys() {
		return readChar(Pos.ARRAY_YMD4YS);
	}

	public void setArrayYmd4mFormatted(String arrayYmd4m) {
		writeString(Pos.ARRAY_YMD4M, Trunc.toUnsignedNumeric(arrayYmd4m, Len.ARRAY_YMD4M), Len.ARRAY_YMD4M);
	}

	/**Original name: WS-DATE-ARRAY-YMD4M<br>*/
	public short getArrayYmd4m() {
		return readNumDispUnsignedShort(Pos.ARRAY_YMD4M, Len.ARRAY_YMD4M);
	}

	public String getArrayYmd4mFormatted() {
		return readFixedString(Pos.ARRAY_YMD4M, Len.ARRAY_YMD4M);
	}

	public void setArrayYmd4ms(char arrayYmd4ms) {
		writeChar(Pos.ARRAY_YMD4MS, arrayYmd4ms);
	}

	/**Original name: WS-DATE-ARRAY-YMD4MS<br>*/
	public char getArrayYmd4ms() {
		return readChar(Pos.ARRAY_YMD4MS);
	}

	public void setArrayYmd4dFormatted(String arrayYmd4d) {
		writeString(Pos.ARRAY_YMD4D, Trunc.toUnsignedNumeric(arrayYmd4d, Len.ARRAY_YMD4D), Len.ARRAY_YMD4D);
	}

	/**Original name: WS-DATE-ARRAY-YMD4D<br>*/
	public short getArrayYmd4d() {
		return readNumDispUnsignedShort(Pos.ARRAY_YMD4D, Len.ARRAY_YMD4D);
	}

	public String getArrayYmd4dFormatted() {
		return readFixedString(Pos.ARRAY_YMD4D, Len.ARRAY_YMD4D);
	}

	public void setArrayYmd4ds(char arrayYmd4ds) {
		writeChar(Pos.ARRAY_YMD4DS, arrayYmd4ds);
	}

	/**Original name: WS-DATE-ARRAY-YMD4DS<br>*/
	public char getArrayYmd4ds() {
		return readChar(Pos.ARRAY_YMD4DS);
	}

	public void setArrayYmd4t(String arrayYmd4t) {
		writeString(Pos.ARRAY_YMD4T, arrayYmd4t, Len.ARRAY_YMD4T);
	}

	/**Original name: WS-DATE-ARRAY-YMD4T<br>*/
	public String getArrayYmd4t() {
		return readString(Pos.ARRAY_YMD4T, Len.ARRAY_YMD4T);
	}

	public String getArrayYmd4tFormatted() {
		return Functions.padBlanks(getArrayYmd4t(), Len.ARRAY_YMD4T);
	}

	public String getArrayYmd2yFormatted() {
		return readFixedString(Pos.ARRAY_YMD2Y, Len.ARRAY_YMD2Y);
	}

	/**Original name: WS-DATE-ARRAY-YMD2YS<br>*/
	public char getArrayYmd2ys() {
		return readChar(Pos.ARRAY_YMD2YS);
	}

	public String getArrayYmd2mFormatted() {
		return readFixedString(Pos.ARRAY_YMD2M, Len.ARRAY_YMD2M);
	}

	/**Original name: WS-DATE-ARRAY-YMD2MS<br>*/
	public char getArrayYmd2ms() {
		return readChar(Pos.ARRAY_YMD2MS);
	}

	public String getArrayYmd2dFormatted() {
		return readFixedString(Pos.ARRAY_YMD2D, Len.ARRAY_YMD2D);
	}

	/**Original name: WS-DATE-ARRAY-YMD2DS<br>*/
	public char getArrayYmd2ds() {
		return readChar(Pos.ARRAY_YMD2DS);
	}

	/**Original name: WS-DATE-ARRAY-YMD2T<br>*/
	public String getArrayYmd2t() {
		return readString(Pos.ARRAY_YMD2T, Len.ARRAY_YMD2T);
	}

	public String getArrayYmd2tFormatted() {
		return Functions.padBlanks(getArrayYmd2t(), Len.ARRAY_YMD2T);
	}

	public void setArrayTsYearFormatted(String arrayTsYear) {
		writeString(Pos.ARRAY_TS_YEAR, Trunc.toUnsignedNumeric(arrayTsYear, Len.ARRAY_TS_YEAR), Len.ARRAY_TS_YEAR);
	}

	/**Original name: WS-DATE-ARRAY-TS-YEAR<br>*/
	public short getArrayTsYear() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_YEAR, Len.ARRAY_TS_YEAR);
	}

	public String getArrayTsYearFormatted() {
		return readFixedString(Pos.ARRAY_TS_YEAR, Len.ARRAY_TS_YEAR);
	}

	public void setArrayTsMonthFormatted(String arrayTsMonth) {
		writeString(Pos.ARRAY_TS_MONTH, Trunc.toUnsignedNumeric(arrayTsMonth, Len.ARRAY_TS_MONTH), Len.ARRAY_TS_MONTH);
	}

	/**Original name: WS-DATE-ARRAY-TS-MONTH<br>*/
	public short getArrayTsMonth() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_MONTH, Len.ARRAY_TS_MONTH);
	}

	public String getArrayTsMonthFormatted() {
		return readFixedString(Pos.ARRAY_TS_MONTH, Len.ARRAY_TS_MONTH);
	}

	public void setArrayTsDayFormatted(String arrayTsDay) {
		writeString(Pos.ARRAY_TS_DAY, Trunc.toUnsignedNumeric(arrayTsDay, Len.ARRAY_TS_DAY), Len.ARRAY_TS_DAY);
	}

	/**Original name: WS-DATE-ARRAY-TS-DAY<br>*/
	public short getArrayTsDay() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_DAY, Len.ARRAY_TS_DAY);
	}

	public String getArrayTsDayFormatted() {
		return readFixedString(Pos.ARRAY_TS_DAY, Len.ARRAY_TS_DAY);
	}

	public void setArrayTsHourFormatted(String arrayTsHour) {
		writeString(Pos.ARRAY_TS_HOUR, Trunc.toUnsignedNumeric(arrayTsHour, Len.ARRAY_TS_HOUR), Len.ARRAY_TS_HOUR);
	}

	/**Original name: WS-DATE-ARRAY-TS-HOUR<br>*/
	public short getArrayTsHour() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_HOUR, Len.ARRAY_TS_HOUR);
	}

	public String getArrayTsHourFormatted() {
		return readFixedString(Pos.ARRAY_TS_HOUR, Len.ARRAY_TS_HOUR);
	}

	public void setArrayTsMinFormatted(String arrayTsMin) {
		writeString(Pos.ARRAY_TS_MIN, Trunc.toUnsignedNumeric(arrayTsMin, Len.ARRAY_TS_MIN), Len.ARRAY_TS_MIN);
	}

	/**Original name: WS-DATE-ARRAY-TS-MIN<br>*/
	public short getArrayTsMin() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_MIN, Len.ARRAY_TS_MIN);
	}

	public String getArrayTsMinFormatted() {
		return readFixedString(Pos.ARRAY_TS_MIN, Len.ARRAY_TS_MIN);
	}

	public void setArrayTsSecFormatted(String arrayTsSec) {
		writeString(Pos.ARRAY_TS_SEC, Trunc.toUnsignedNumeric(arrayTsSec, Len.ARRAY_TS_SEC), Len.ARRAY_TS_SEC);
	}

	/**Original name: WS-DATE-ARRAY-TS-SEC<br>*/
	public short getArrayTsSec() {
		return readNumDispUnsignedShort(Pos.ARRAY_TS_SEC, Len.ARRAY_TS_SEC);
	}

	public String getArrayTsSecFormatted() {
		return readFixedString(Pos.ARRAY_TS_SEC, Len.ARRAY_TS_SEC);
	}

	public void setArrayTsMsecs(int arrayTsMsecs) {
		writeInt(Pos.ARRAY_TS_MSECS, arrayTsMsecs, Len.Int.ARRAY_TS_MSECS, SignType.NO_SIGN);
	}

	public void setArrayTsMsecsFormatted(String arrayTsMsecs) {
		writeString(Pos.ARRAY_TS_MSECS, Trunc.toUnsignedNumeric(arrayTsMsecs, Len.ARRAY_TS_MSECS), Len.ARRAY_TS_MSECS);
	}

	/**Original name: WS-DATE-ARRAY-TS-MSECS<br>*/
	public int getArrayTsMsecs() {
		return readNumDispUnsignedInt(Pos.ARRAY_TS_MSECS, Len.ARRAY_TS_MSECS);
	}

	public String getArrayTsMsecsFormatted() {
		return readFixedString(Pos.ARRAY_TS_MSECS, Len.ARRAY_TS_MSECS);
	}

	/**Original name: WS-DATE-ARRAY-TI<br>*/
	public byte[] getWsDateArrayTiBytes() {
		byte[] buffer = new byte[Len.WS_DATE_ARRAY_TI];
		return getWsDateArrayTiBytes(buffer, 1);
	}

	public byte[] getWsDateArrayTiBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WS_DATE_ARRAY_TI, Pos.WS_DATE_ARRAY_TI);
		return buffer;
	}

	public void setArrayTiHourFormatted(String arrayTiHour) {
		writeString(Pos.ARRAY_TI_HOUR, Trunc.toUnsignedNumeric(arrayTiHour, Len.ARRAY_TI_HOUR), Len.ARRAY_TI_HOUR);
	}

	/**Original name: WS-DATE-ARRAY-TI-HOUR<br>*/
	public short getArrayTiHour() {
		return readNumDispUnsignedShort(Pos.ARRAY_TI_HOUR, Len.ARRAY_TI_HOUR);
	}

	public void setArrayTiHoursFormatted(String arrayTiHours) {
		writeString(Pos.ARRAY_TI_HOURS, Trunc.toUnsignedNumeric(arrayTiHours, Len.ARRAY_TI_HOURS), Len.ARRAY_TI_HOURS);
	}

	/**Original name: WS-DATE-ARRAY-TI-HOURS<br>*/
	public short getArrayTiHours() {
		return readNumDispUnsignedShort(Pos.ARRAY_TI_HOURS, Len.ARRAY_TI_HOURS);
	}

	public void setArrayTiMinFormatted(String arrayTiMin) {
		writeString(Pos.ARRAY_TI_MIN, Trunc.toUnsignedNumeric(arrayTiMin, Len.ARRAY_TI_MIN), Len.ARRAY_TI_MIN);
	}

	/**Original name: WS-DATE-ARRAY-TI-MIN<br>*/
	public short getArrayTiMin() {
		return readNumDispUnsignedShort(Pos.ARRAY_TI_MIN, Len.ARRAY_TI_MIN);
	}

	public void setArrayTiMinsFormatted(String arrayTiMins) {
		writeString(Pos.ARRAY_TI_MINS, Trunc.toUnsignedNumeric(arrayTiMins, Len.ARRAY_TI_MINS), Len.ARRAY_TI_MINS);
	}

	/**Original name: WS-DATE-ARRAY-TI-MINS<br>*/
	public short getArrayTiMins() {
		return readNumDispUnsignedShort(Pos.ARRAY_TI_MINS, Len.ARRAY_TI_MINS);
	}

	public void setArrayTiSecFormatted(String arrayTiSec) {
		writeString(Pos.ARRAY_TI_SEC, Trunc.toUnsignedNumeric(arrayTiSec, Len.ARRAY_TI_SEC), Len.ARRAY_TI_SEC);
	}

	/**Original name: WS-DATE-ARRAY-TI-SEC<br>*/
	public short getArrayTiSec() {
		return readNumDispUnsignedShort(Pos.ARRAY_TI_SEC, Len.ARRAY_TI_SEC);
	}

	public void setArrayTuHourFormatted(String arrayTuHour) {
		writeString(Pos.ARRAY_TU_HOUR, Trunc.toUnsignedNumeric(arrayTuHour, Len.ARRAY_TU_HOUR), Len.ARRAY_TU_HOUR);
	}

	/**Original name: WS-DATE-ARRAY-TU-HOUR<br>*/
	public short getArrayTuHour() {
		return readNumDispUnsignedShort(Pos.ARRAY_TU_HOUR, Len.ARRAY_TU_HOUR);
	}

	public String getArrayTuHourFormatted() {
		return readFixedString(Pos.ARRAY_TU_HOUR, Len.ARRAY_TU_HOUR);
	}

	public void setArrayTuMinFormatted(String arrayTuMin) {
		writeString(Pos.ARRAY_TU_MIN, Trunc.toUnsignedNumeric(arrayTuMin, Len.ARRAY_TU_MIN), Len.ARRAY_TU_MIN);
	}

	/**Original name: WS-DATE-ARRAY-TU-MIN<br>*/
	public short getArrayTuMin() {
		return readNumDispUnsignedShort(Pos.ARRAY_TU_MIN, Len.ARRAY_TU_MIN);
	}

	public String getArrayTuMinFormatted() {
		return readFixedString(Pos.ARRAY_TU_MIN, Len.ARRAY_TU_MIN);
	}

	public void setArrayTuSecFormatted(String arrayTuSec) {
		writeString(Pos.ARRAY_TU_SEC, Trunc.toUnsignedNumeric(arrayTuSec, Len.ARRAY_TU_SEC), Len.ARRAY_TU_SEC);
	}

	/**Original name: WS-DATE-ARRAY-TU-SEC<br>*/
	public short getArrayTuSec() {
		return readNumDispUnsignedShort(Pos.ARRAY_TU_SEC, Len.ARRAY_TU_SEC);
	}

	public String getArrayTuSecFormatted() {
		return readFixedString(Pos.ARRAY_TU_SEC, Len.ARRAY_TU_SEC);
	}

	public void setArrayMdy4mFormatted(String arrayMdy4m) {
		writeString(Pos.ARRAY_MDY4M, Trunc.toUnsignedNumeric(arrayMdy4m, Len.ARRAY_MDY4M), Len.ARRAY_MDY4M);
	}

	/**Original name: WS-DATE-ARRAY-MDY4M<br>*/
	public short getArrayMdy4m() {
		return readNumDispUnsignedShort(Pos.ARRAY_MDY4M, Len.ARRAY_MDY4M);
	}

	public String getArrayMdy4mFormatted() {
		return readFixedString(Pos.ARRAY_MDY4M, Len.ARRAY_MDY4M);
	}

	public void setArrayMdy4ms(char arrayMdy4ms) {
		writeChar(Pos.ARRAY_MDY4MS, arrayMdy4ms);
	}

	/**Original name: WS-DATE-ARRAY-MDY4MS<br>*/
	public char getArrayMdy4ms() {
		return readChar(Pos.ARRAY_MDY4MS);
	}

	public void setArrayMdy4dFormatted(String arrayMdy4d) {
		writeString(Pos.ARRAY_MDY4D, Trunc.toUnsignedNumeric(arrayMdy4d, Len.ARRAY_MDY4D), Len.ARRAY_MDY4D);
	}

	/**Original name: WS-DATE-ARRAY-MDY4D<br>*/
	public short getArrayMdy4d() {
		return readNumDispUnsignedShort(Pos.ARRAY_MDY4D, Len.ARRAY_MDY4D);
	}

	public String getArrayMdy4dFormatted() {
		return readFixedString(Pos.ARRAY_MDY4D, Len.ARRAY_MDY4D);
	}

	public void setArrayMdy4ds(char arrayMdy4ds) {
		writeChar(Pos.ARRAY_MDY4DS, arrayMdy4ds);
	}

	/**Original name: WS-DATE-ARRAY-MDY4DS<br>*/
	public char getArrayMdy4ds() {
		return readChar(Pos.ARRAY_MDY4DS);
	}

	public void setArrayMdy4yFormatted(String arrayMdy4y) {
		writeString(Pos.ARRAY_MDY4Y, Trunc.toUnsignedNumeric(arrayMdy4y, Len.ARRAY_MDY4Y), Len.ARRAY_MDY4Y);
	}

	/**Original name: WS-DATE-ARRAY-MDY4Y<br>*/
	public short getArrayMdy4y() {
		return readNumDispUnsignedShort(Pos.ARRAY_MDY4Y, Len.ARRAY_MDY4Y);
	}

	public String getArrayMdy4yFormatted() {
		return readFixedString(Pos.ARRAY_MDY4Y, Len.ARRAY_MDY4Y);
	}

	public void setArrayMdy4ys(char arrayMdy4ys) {
		writeChar(Pos.ARRAY_MDY4YS, arrayMdy4ys);
	}

	/**Original name: WS-DATE-ARRAY-MDY4YS<br>*/
	public char getArrayMdy4ys() {
		return readChar(Pos.ARRAY_MDY4YS);
	}

	public void setArrayMdy4t(String arrayMdy4t) {
		writeString(Pos.ARRAY_MDY4T, arrayMdy4t, Len.ARRAY_MDY4T);
	}

	/**Original name: WS-DATE-ARRAY-MDY4T<br>*/
	public String getArrayMdy4t() {
		return readString(Pos.ARRAY_MDY4T, Len.ARRAY_MDY4T);
	}

	public String getArrayMdy4tFormatted() {
		return Functions.padBlanks(getArrayMdy4t(), Len.ARRAY_MDY4T);
	}

	public String getArrayMdy2mFormatted() {
		return readFixedString(Pos.ARRAY_MDY2M, Len.ARRAY_MDY2M);
	}

	/**Original name: WS-DATE-ARRAY-MDY2MS<br>*/
	public char getArrayMdy2ms() {
		return readChar(Pos.ARRAY_MDY2MS);
	}

	public String getArrayMdy2dFormatted() {
		return readFixedString(Pos.ARRAY_MDY2D, Len.ARRAY_MDY2D);
	}

	/**Original name: WS-DATE-ARRAY-MDY2DS<br>*/
	public char getArrayMdy2ds() {
		return readChar(Pos.ARRAY_MDY2DS);
	}

	public String getArrayMdy2yFormatted() {
		return readFixedString(Pos.ARRAY_MDY2Y, Len.ARRAY_MDY2Y);
	}

	/**Original name: WS-DATE-ARRAY-MDY2YS<br>*/
	public char getArrayMdy2ys() {
		return readChar(Pos.ARRAY_MDY2YS);
	}

	/**Original name: WS-DATE-ARRAY-MDY2T<br>*/
	public String getArrayMdy2t() {
		return readString(Pos.ARRAY_MDY2T, Len.ARRAY_MDY2T);
	}

	public String getArrayMdy2tFormatted() {
		return Functions.padBlanks(getArrayMdy2t(), Len.ARRAY_MDY2T);
	}

	public void setArrayDmy4dFormatted(String arrayDmy4d) {
		writeString(Pos.ARRAY_DMY4D, Trunc.toUnsignedNumeric(arrayDmy4d, Len.ARRAY_DMY4D), Len.ARRAY_DMY4D);
	}

	/**Original name: WS-DATE-ARRAY-DMY4D<br>*/
	public short getArrayDmy4d() {
		return readNumDispUnsignedShort(Pos.ARRAY_DMY4D, Len.ARRAY_DMY4D);
	}

	public String getArrayDmy4dFormatted() {
		return readFixedString(Pos.ARRAY_DMY4D, Len.ARRAY_DMY4D);
	}

	public void setArrayDmy4ds(char arrayDmy4ds) {
		writeChar(Pos.ARRAY_DMY4DS, arrayDmy4ds);
	}

	/**Original name: WS-DATE-ARRAY-DMY4DS<br>*/
	public char getArrayDmy4ds() {
		return readChar(Pos.ARRAY_DMY4DS);
	}

	public void setArrayDmy4mFormatted(String arrayDmy4m) {
		writeString(Pos.ARRAY_DMY4M, Trunc.toUnsignedNumeric(arrayDmy4m, Len.ARRAY_DMY4M), Len.ARRAY_DMY4M);
	}

	/**Original name: WS-DATE-ARRAY-DMY4M<br>*/
	public short getArrayDmy4m() {
		return readNumDispUnsignedShort(Pos.ARRAY_DMY4M, Len.ARRAY_DMY4M);
	}

	public String getArrayDmy4mFormatted() {
		return readFixedString(Pos.ARRAY_DMY4M, Len.ARRAY_DMY4M);
	}

	public void setArrayDmy4ms(char arrayDmy4ms) {
		writeChar(Pos.ARRAY_DMY4MS, arrayDmy4ms);
	}

	/**Original name: WS-DATE-ARRAY-DMY4MS<br>*/
	public char getArrayDmy4ms() {
		return readChar(Pos.ARRAY_DMY4MS);
	}

	public void setArrayDmy4yFormatted(String arrayDmy4y) {
		writeString(Pos.ARRAY_DMY4Y, Trunc.toUnsignedNumeric(arrayDmy4y, Len.ARRAY_DMY4Y), Len.ARRAY_DMY4Y);
	}

	/**Original name: WS-DATE-ARRAY-DMY4Y<br>*/
	public short getArrayDmy4y() {
		return readNumDispUnsignedShort(Pos.ARRAY_DMY4Y, Len.ARRAY_DMY4Y);
	}

	public String getArrayDmy4yFormatted() {
		return readFixedString(Pos.ARRAY_DMY4Y, Len.ARRAY_DMY4Y);
	}

	public void setArrayDmy4ys(char arrayDmy4ys) {
		writeChar(Pos.ARRAY_DMY4YS, arrayDmy4ys);
	}

	/**Original name: WS-DATE-ARRAY-DMY4YS<br>*/
	public char getArrayDmy4ys() {
		return readChar(Pos.ARRAY_DMY4YS);
	}

	public void setArrayDmy4t(String arrayDmy4t) {
		writeString(Pos.ARRAY_DMY4T, arrayDmy4t, Len.ARRAY_DMY4T);
	}

	/**Original name: WS-DATE-ARRAY-DMY4T<br>*/
	public String getArrayDmy4t() {
		return readString(Pos.ARRAY_DMY4T, Len.ARRAY_DMY4T);
	}

	public String getArrayDmy4tFormatted() {
		return Functions.padBlanks(getArrayDmy4t(), Len.ARRAY_DMY4T);
	}

	public String getArrayDmy2dFormatted() {
		return readFixedString(Pos.ARRAY_DMY2D, Len.ARRAY_DMY2D);
	}

	/**Original name: WS-DATE-ARRAY-DMY2DS<br>*/
	public char getArrayDmy2ds() {
		return readChar(Pos.ARRAY_DMY2DS);
	}

	public String getArrayDmy2mFormatted() {
		return readFixedString(Pos.ARRAY_DMY2M, Len.ARRAY_DMY2M);
	}

	/**Original name: WS-DATE-ARRAY-DMY2MS<br>*/
	public char getArrayDmy2ms() {
		return readChar(Pos.ARRAY_DMY2MS);
	}

	public String getArrayDmy2yFormatted() {
		return readFixedString(Pos.ARRAY_DMY2Y, Len.ARRAY_DMY2Y);
	}

	/**Original name: WS-DATE-ARRAY-DMY2YS<br>*/
	public char getArrayDmy2ys() {
		return readChar(Pos.ARRAY_DMY2YS);
	}

	/**Original name: WS-DATE-ARRAY-DMY2T<br>*/
	public String getArrayDmy2t() {
		return readString(Pos.ARRAY_DMY2T, Len.ARRAY_DMY2T);
	}

	public String getArrayDmy2tFormatted() {
		return Functions.padBlanks(getArrayDmy2t(), Len.ARRAY_DMY2T);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_DATE_ARRAY = 1;
		public static final int ARRAY_IN = WS_DATE_ARRAY;
		public static final int WS_DATE_ARRAY_RED = 1;
		public static final int WS_DATE_ARRAY_JUL7 = 1;
		public static final int ARRAY_JUL7Y = WS_DATE_ARRAY_JUL7;
		public static final int ARRAY_JUL7D = ARRAY_JUL7Y + Len.ARRAY_JUL7Y;
		public static final int ARRAY_JUL7TS = ARRAY_JUL7D + Len.ARRAY_JUL7D;
		public static final int ARRAY_JUL7T = ARRAY_JUL7TS + Len.ARRAY_JUL7TS;
		public static final int FLR1 = ARRAY_JUL7T + Len.ARRAY_JUL7T;
		public static final int WS_DATE_ARRAY_JUL5 = 1;
		public static final int ARRAY_JUL5Y = WS_DATE_ARRAY_JUL5;
		public static final int ARRAY_JUL5D = ARRAY_JUL5Y + Len.ARRAY_JUL5Y;
		public static final int ARRAY_JUL5TS = ARRAY_JUL5D + Len.ARRAY_JUL5D;
		public static final int ARRAY_JUL5T = ARRAY_JUL5TS + Len.ARRAY_JUL5TS;
		public static final int FLR2 = ARRAY_JUL5T + Len.ARRAY_JUL5T;
		public static final int WS_DATE_ARRAY_AD = 1;
		public static final int ARRAY_ADT = WS_DATE_ARRAY_AD;
		public static final int ARRAY_ADDAY = ARRAY_ADT + Len.ARRAY_ADT;
		public static final int ARRAY_ADDAYS = ARRAY_ADDAY + Len.ARRAY_ADDAY;
		public static final int ARRAY_ADMONTH = ARRAY_ADDAYS + Len.ARRAY_ADDAYS;
		public static final int FLR3 = ARRAY_ADMONTH + Len.ARRAY_ADMONTH;
		public static final int WS_DATE_ARRAY_AM = 1;
		public static final int ARRAY_AMT = WS_DATE_ARRAY_AM;
		public static final int ARRAY_AMMONTH = ARRAY_AMT + Len.ARRAY_AMT;
		public static final int FLR4 = ARRAY_AMMONTH + Len.ARRAY_AMMONTH;
		public static final int WS_DATE_ARRAY_AS = 1;
		public static final int ARRAY_AST = WS_DATE_ARRAY_AS;
		public static final int ARRAY_ASDAY = ARRAY_AST + Len.ARRAY_AST;
		public static final int ARRAY_ASDAYS = ARRAY_ASDAY + Len.ARRAY_ASDAY;
		public static final int ARRAY_ASMONTH = ARRAY_ASDAYS + Len.ARRAY_ASDAYS;
		public static final int ARRAY_ASMONTHS = ARRAY_ASMONTH + Len.ARRAY_ASMONTH;
		public static final int ARRAY_ASMONTHSS = ARRAY_ASMONTHS + Len.ARRAY_ASMONTHS;
		public static final int ARRAY_ASYEAR = ARRAY_ASMONTHSS + Len.ARRAY_ASMONTHSS;
		public static final int FLR5 = ARRAY_ASYEAR + Len.ARRAY_ASYEAR;
		public static final int WS_DATE_ARRAY_YMD4 = 1;
		public static final int ARRAY_YMD4Y = WS_DATE_ARRAY_YMD4;
		public static final int ARRAY_YMD4YS = ARRAY_YMD4Y + Len.ARRAY_YMD4Y;
		public static final int ARRAY_YMD4M = ARRAY_YMD4YS + Len.ARRAY_YMD4YS;
		public static final int ARRAY_YMD4MS = ARRAY_YMD4M + Len.ARRAY_YMD4M;
		public static final int ARRAY_YMD4D = ARRAY_YMD4MS + Len.ARRAY_YMD4MS;
		public static final int ARRAY_YMD4DS = ARRAY_YMD4D + Len.ARRAY_YMD4D;
		public static final int ARRAY_YMD4T = ARRAY_YMD4DS + Len.ARRAY_YMD4DS;
		public static final int FLR6 = ARRAY_YMD4T + Len.ARRAY_YMD4T;
		public static final int WS_DATE_ARRAY_YMD2 = 1;
		public static final int ARRAY_YMD2Y = WS_DATE_ARRAY_YMD2;
		public static final int ARRAY_YMD2YS = ARRAY_YMD2Y + Len.ARRAY_YMD2Y;
		public static final int ARRAY_YMD2M = ARRAY_YMD2YS + Len.ARRAY_YMD2YS;
		public static final int ARRAY_YMD2MS = ARRAY_YMD2M + Len.ARRAY_YMD2M;
		public static final int ARRAY_YMD2D = ARRAY_YMD2MS + Len.ARRAY_YMD2MS;
		public static final int ARRAY_YMD2DS = ARRAY_YMD2D + Len.ARRAY_YMD2D;
		public static final int ARRAY_YMD2T = ARRAY_YMD2DS + Len.ARRAY_YMD2DS;
		public static final int FLR7 = ARRAY_YMD2T + Len.ARRAY_YMD2T;
		public static final int WS_DATE_ARRAY_TS = 1;
		public static final int ARRAY_TS_YEAR = WS_DATE_ARRAY_TS;
		public static final int ARRAY_TS_MONTH = ARRAY_TS_YEAR + Len.ARRAY_TS_YEAR;
		public static final int ARRAY_TS_DAY = ARRAY_TS_MONTH + Len.ARRAY_TS_MONTH;
		public static final int ARRAY_TS_HOUR = ARRAY_TS_DAY + Len.ARRAY_TS_DAY;
		public static final int ARRAY_TS_MIN = ARRAY_TS_HOUR + Len.ARRAY_TS_HOUR;
		public static final int ARRAY_TS_SEC = ARRAY_TS_MIN + Len.ARRAY_TS_MIN;
		public static final int ARRAY_TS_MSECS = ARRAY_TS_SEC + Len.ARRAY_TS_SEC;
		public static final int FLR8 = ARRAY_TS_MSECS + Len.ARRAY_TS_MSECS;
		public static final int WS_DATE_ARRAY_TI = 1;
		public static final int ARRAY_TI_HOUR = WS_DATE_ARRAY_TI;
		public static final int ARRAY_TI_HOURS = ARRAY_TI_HOUR + Len.ARRAY_TI_HOUR;
		public static final int ARRAY_TI_MIN = ARRAY_TI_HOURS + Len.ARRAY_TI_HOURS;
		public static final int ARRAY_TI_MINS = ARRAY_TI_MIN + Len.ARRAY_TI_MIN;
		public static final int ARRAY_TI_SEC = ARRAY_TI_MINS + Len.ARRAY_TI_MINS;
		public static final int FLR9 = ARRAY_TI_SEC + Len.ARRAY_TI_SEC;
		public static final int WS_DATE_ARRAY_TU = 1;
		public static final int ARRAY_TU_HOUR = WS_DATE_ARRAY_TU;
		public static final int ARRAY_TU_MIN = ARRAY_TU_HOUR + Len.ARRAY_TU_HOUR;
		public static final int ARRAY_TU_SEC = ARRAY_TU_MIN + Len.ARRAY_TU_MIN;
		public static final int FLR10 = ARRAY_TU_SEC + Len.ARRAY_TU_SEC;
		public static final int WS_DATE_ARRAY_MDY4 = 1;
		public static final int ARRAY_MDY4M = WS_DATE_ARRAY_MDY4;
		public static final int ARRAY_MDY4MS = ARRAY_MDY4M + Len.ARRAY_MDY4M;
		public static final int ARRAY_MDY4D = ARRAY_MDY4MS + Len.ARRAY_MDY4MS;
		public static final int ARRAY_MDY4DS = ARRAY_MDY4D + Len.ARRAY_MDY4D;
		public static final int ARRAY_MDY4Y = ARRAY_MDY4DS + Len.ARRAY_MDY4DS;
		public static final int ARRAY_MDY4YS = ARRAY_MDY4Y + Len.ARRAY_MDY4Y;
		public static final int ARRAY_MDY4T = ARRAY_MDY4YS + Len.ARRAY_MDY4YS;
		public static final int FLR11 = ARRAY_MDY4T + Len.ARRAY_MDY4T;
		public static final int WS_DATE_ARRAY_MDY2 = 1;
		public static final int ARRAY_MDY2M = WS_DATE_ARRAY_MDY2;
		public static final int ARRAY_MDY2MS = ARRAY_MDY2M + Len.ARRAY_MDY2M;
		public static final int ARRAY_MDY2D = ARRAY_MDY2MS + Len.ARRAY_MDY2MS;
		public static final int ARRAY_MDY2DS = ARRAY_MDY2D + Len.ARRAY_MDY2D;
		public static final int ARRAY_MDY2Y = ARRAY_MDY2DS + Len.ARRAY_MDY2DS;
		public static final int ARRAY_MDY2YS = ARRAY_MDY2Y + Len.ARRAY_MDY2Y;
		public static final int ARRAY_MDY2T = ARRAY_MDY2YS + Len.ARRAY_MDY2YS;
		public static final int FLR12 = ARRAY_MDY2T + Len.ARRAY_MDY2T;
		public static final int WS_DATE_ARRAY_DMY4 = 1;
		public static final int ARRAY_DMY4D = WS_DATE_ARRAY_DMY4;
		public static final int ARRAY_DMY4DS = ARRAY_DMY4D + Len.ARRAY_DMY4D;
		public static final int ARRAY_DMY4M = ARRAY_DMY4DS + Len.ARRAY_DMY4DS;
		public static final int ARRAY_DMY4MS = ARRAY_DMY4M + Len.ARRAY_DMY4M;
		public static final int ARRAY_DMY4Y = ARRAY_DMY4MS + Len.ARRAY_DMY4MS;
		public static final int ARRAY_DMY4YS = ARRAY_DMY4Y + Len.ARRAY_DMY4Y;
		public static final int ARRAY_DMY4T = ARRAY_DMY4YS + Len.ARRAY_DMY4YS;
		public static final int FLR13 = ARRAY_DMY4T + Len.ARRAY_DMY4T;
		public static final int WS_DATE_ARRAY_DMY2 = 1;
		public static final int ARRAY_DMY2D = WS_DATE_ARRAY_DMY2;
		public static final int ARRAY_DMY2DS = ARRAY_DMY2D + Len.ARRAY_DMY2D;
		public static final int ARRAY_DMY2M = ARRAY_DMY2DS + Len.ARRAY_DMY2DS;
		public static final int ARRAY_DMY2MS = ARRAY_DMY2M + Len.ARRAY_DMY2M;
		public static final int ARRAY_DMY2Y = ARRAY_DMY2MS + Len.ARRAY_DMY2MS;
		public static final int ARRAY_DMY2YS = ARRAY_DMY2Y + Len.ARRAY_DMY2Y;
		public static final int ARRAY_DMY2T = ARRAY_DMY2YS + Len.ARRAY_DMY2YS;
		public static final int FLR14 = ARRAY_DMY2T + Len.ARRAY_DMY2T;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int wsDateArrayChar(int idx) {
			return WS_DATE_ARRAY_RED + idx * Len.ARRAY_CHAR;
		}

		public static int wsDateChar(int idx) {
			return wsDateArrayChar(idx);
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CHAR_FLD = 1;
		public static final int ARRAY_CHAR = CHAR_FLD;
		public static final int ARRAY_JUL7Y = 4;
		public static final int ARRAY_JUL7D = 3;
		public static final int ARRAY_JUL7TS = 1;
		public static final int ARRAY_JUL7T = 20;
		public static final int ARRAY_JUL5Y = 2;
		public static final int ARRAY_JUL5D = 3;
		public static final int ARRAY_JUL5TS = 1;
		public static final int ARRAY_JUL5T = 20;
		public static final int ARRAY_ADT = 12;
		public static final int ARRAY_ADDAY = 2;
		public static final int ARRAY_ADDAYS = 1;
		public static final int ARRAY_ADMONTH = 9;
		public static final int ARRAY_AMT = 12;
		public static final int ARRAY_AMMONTH = 9;
		public static final int ARRAY_AST = 12;
		public static final int ARRAY_ASDAY = 2;
		public static final int ARRAY_ASDAYS = 1;
		public static final int ARRAY_ASMONTH = 3;
		public static final int ARRAY_ASMONTHS = 1;
		public static final int ARRAY_ASMONTHSS = 1;
		public static final int ARRAY_ASYEAR = 4;
		public static final int ARRAY_YMD4Y = 4;
		public static final int ARRAY_YMD4YS = 1;
		public static final int ARRAY_YMD4M = 2;
		public static final int ARRAY_YMD4MS = 1;
		public static final int ARRAY_YMD4D = 2;
		public static final int ARRAY_YMD4DS = 1;
		public static final int ARRAY_YMD4T = 20;
		public static final int ARRAY_YMD2Y = 2;
		public static final int ARRAY_YMD2YS = 1;
		public static final int ARRAY_YMD2M = 2;
		public static final int ARRAY_YMD2MS = 1;
		public static final int ARRAY_YMD2D = 2;
		public static final int ARRAY_YMD2DS = 1;
		public static final int ARRAY_YMD2T = 20;
		public static final int ARRAY_TS_YEAR = 4;
		public static final int ARRAY_TS_MONTH = 2;
		public static final int ARRAY_TS_DAY = 2;
		public static final int ARRAY_TS_HOUR = 2;
		public static final int ARRAY_TS_MIN = 2;
		public static final int ARRAY_TS_SEC = 2;
		public static final int ARRAY_TS_MSECS = 6;
		public static final int ARRAY_TI_HOUR = 2;
		public static final int ARRAY_TI_HOURS = 1;
		public static final int ARRAY_TI_MIN = 2;
		public static final int ARRAY_TI_MINS = 1;
		public static final int ARRAY_TI_SEC = 2;
		public static final int ARRAY_TU_HOUR = 2;
		public static final int ARRAY_TU_MIN = 2;
		public static final int ARRAY_TU_SEC = 2;
		public static final int ARRAY_MDY4M = 2;
		public static final int ARRAY_MDY4MS = 1;
		public static final int ARRAY_MDY4D = 2;
		public static final int ARRAY_MDY4DS = 1;
		public static final int ARRAY_MDY4Y = 4;
		public static final int ARRAY_MDY4YS = 1;
		public static final int ARRAY_MDY4T = 20;
		public static final int ARRAY_MDY2M = 2;
		public static final int ARRAY_MDY2MS = 1;
		public static final int ARRAY_MDY2D = 2;
		public static final int ARRAY_MDY2DS = 1;
		public static final int ARRAY_MDY2Y = 2;
		public static final int ARRAY_MDY2YS = 1;
		public static final int ARRAY_MDY2T = 20;
		public static final int ARRAY_DMY4D = 2;
		public static final int ARRAY_DMY4DS = 1;
		public static final int ARRAY_DMY4M = 2;
		public static final int ARRAY_DMY4MS = 1;
		public static final int ARRAY_DMY4Y = 4;
		public static final int ARRAY_DMY4YS = 1;
		public static final int ARRAY_DMY4T = 20;
		public static final int ARRAY_DMY2D = 2;
		public static final int ARRAY_DMY2DS = 1;
		public static final int ARRAY_DMY2M = 2;
		public static final int ARRAY_DMY2MS = 1;
		public static final int ARRAY_DMY2Y = 2;
		public static final int ARRAY_DMY2YS = 1;
		public static final int ARRAY_DMY2T = 20;
		public static final int ARRAY_IN = 50;
		public static final int WS_DATE_ARRAY = ARRAY_IN;
		public static final int FLR9 = 42;
		public static final int WS_DATE_ARRAY_TI = ARRAY_TI_HOUR + ARRAY_TI_HOURS + ARRAY_TI_MIN + ARRAY_TI_MINS + ARRAY_TI_SEC + FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int ARRAY_TS_MSECS = 6;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
