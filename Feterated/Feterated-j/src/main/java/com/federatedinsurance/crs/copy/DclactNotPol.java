/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPol;
import com.federatedinsurance.crs.commons.data.to.IActNotPolDtaExt;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLACT-NOT-POL<br>
 * Variable: DCLACT-NOT-POL from copybook XZH00004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclactNotPol implements IActNotPolDtaExt, IActNotPol {

	//==== PROPERTIES ====
	//Original name: CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: NIN-CLT-ID
	private String ninCltId = DefaultValues.stringVal(Len.NIN_CLT_ID);
	//Original name: NIN-ADR-ID
	private String ninAdrId = DefaultValues.stringVal(Len.NIN_ADR_ID);
	//Original name: WF-STARTED-IND
	private char wfStartedInd = DefaultValues.CHAR_VAL;
	//Original name: POL-BIL-STA-CD
	private char polBilStaCd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	@Override
	public String getPolTypCd() {
		return this.polTypCd;
	}

	@Override
	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	@Override
	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	@Override
	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	@Override
	public String getNotEffDt() {
		return this.notEffDt;
	}

	@Override
	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	@Override
	public String getPolEffDt() {
		return this.polEffDt;
	}

	@Override
	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	@Override
	public String getPolExpDt() {
		return this.polExpDt;
	}

	@Override
	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	@Override
	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	@Override
	public void setNinCltId(String ninCltId) {
		this.ninCltId = Functions.subString(ninCltId, Len.NIN_CLT_ID);
	}

	@Override
	public String getNinCltId() {
		return this.ninCltId;
	}

	@Override
	public void setNinAdrId(String ninAdrId) {
		this.ninAdrId = Functions.subString(ninAdrId, Len.NIN_ADR_ID);
	}

	@Override
	public String getNinAdrId() {
		return this.ninAdrId;
	}

	@Override
	public void setWfStartedInd(char wfStartedInd) {
		this.wfStartedInd = wfStartedInd;
	}

	@Override
	public char getWfStartedInd() {
		return this.wfStartedInd;
	}

	@Override
	public void setPolBilStaCd(char polBilStaCd) {
		this.polBilStaCd = polBilStaCd;
	}

	@Override
	public char getPolBilStaCd() {
		return this.polBilStaCd;
	}

	@Override
	public String getActTypCd() {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public void setActTypCd(String actTypCd) {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public String getCfActNotImpending() {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public void setCfActNotImpending(String cfActNotImpending) {
		throw new FieldNotMappedException("cfActNotImpending");
	}

	@Override
	public String getCfActNotNonpay() {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public void setCfActNotNonpay(String cfActNotNonpay) {
		throw new FieldNotMappedException("cfActNotNonpay");
	}

	@Override
	public String getCfActNotRescind() {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public void setCfActNotRescind(String cfActNotRescind) {
		throw new FieldNotMappedException("cfActNotRescind");
	}

	@Override
	public String getCfAddedByBusinessWorks() {
		throw new FieldNotMappedException("cfAddedByBusinessWorks");
	}

	@Override
	public void setCfAddedByBusinessWorks(String cfAddedByBusinessWorks) {
		throw new FieldNotMappedException("cfAddedByBusinessWorks");
	}

	@Override
	public String getNotEffDtObj() {
		return getNotEffDt();
	}

	@Override
	public void setNotEffDtObj(String notEffDtObj) {
		setNotEffDt(notEffDtObj);
	}

	@Override
	public Character getPolBilStaCdObj() {
		return (getPolBilStaCd());
	}

	@Override
	public void setPolBilStaCdObj(Character polBilStaCdObj) {
		setPolBilStaCd((polBilStaCdObj));
	}

	@Override
	public AfDecimal getPolDueAmtObj() {
		return getPolDueAmt().toString();
	}

	@Override
	public void setPolDueAmtObj(AfDecimal polDueAmtObj) {
		setPolDueAmt(new AfDecimal(polDueAmtObj, 10, 2));
	}

	@Override
	public Character getWfStartedIndObj() {
		return (getWfStartedInd());
	}

	@Override
	public void setWfStartedIndObj(Character wfStartedIndObj) {
		setWfStartedInd((wfStartedIndObj));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NOT_EFF_DT = 10;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int POL_TYP_CD = 3;
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int NIN_CLT_ID = 64;
		public static final int NIN_ADR_ID = 64;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
