/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.DclfedRmtPrtTab;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program TS030299<br>
 * Generated as a class for rule WS.<br>*/
public class Ts030299Data {

	//==== PROPERTIES ====
	//Original name: DCLFED-RMT-PRT-TAB
	private DclfedRmtPrtTab dclfedRmtPrtTab = new DclfedRmtPrtTab();
	//Original name: CF-SC-RECORD-FOUND
	private int cfScRecordFound = 0;
	//Original name: CF-SC-NO-RECORD-FOUND
	private int cfScNoRecordFound = 100;
	//Original name: CF-SC-NOT-CONNECTED
	private int cfScNotConnected = -981;
	//Original name: DSNRLI-PARMS
	private DsnrliParms dsnrliParms = new DsnrliParms();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessages errorAndAdviceMessages = new ErrorAndAdviceMessages();
	//Original name: SA-REPORT-NBR
	private String saReportNbr = DefaultValues.stringVal(Len.SA_REPORT_NBR);
	//Original name: SW-FIRST-TIME-FLAG
	private boolean swFirstTimeFlag = true;

	//==== METHODS ====
	public int getCfScRecordFound() {
		return this.cfScRecordFound;
	}

	public int getCfScNoRecordFound() {
		return this.cfScNoRecordFound;
	}

	public int getCfScNotConnected() {
		return this.cfScNotConnected;
	}

	public void setSaReportNbr(String saReportNbr) {
		this.saReportNbr = Functions.subString(saReportNbr, Len.SA_REPORT_NBR);
	}

	public String getSaReportNbr() {
		return this.saReportNbr;
	}

	public void setSwFirstTimeFlag(boolean swFirstTimeFlag) {
		this.swFirstTimeFlag = swFirstTimeFlag;
	}

	public boolean isSwFirstTimeFlag() {
		return this.swFirstTimeFlag;
	}

	public DclfedRmtPrtTab getDclfedRmtPrtTab() {
		return dclfedRmtPrtTab;
	}

	public DsnrliParms getDsnrliParms() {
		return dsnrliParms;
	}

	public ErrorAndAdviceMessages getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SA_REPORT_NBR = 6;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
