/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: CSC-GENERAL-PARMS<br>
 * Variable: CSC-GENERAL-PARMS from copybook TS020COM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CscGeneralParms {

	//==== PROPERTIES ====
	//Original name: CSC-OPERATION
	private String operation = DefaultValues.stringVal(Len.OPERATION);
	//Original name: CSC-UNIT-OF-WORK
	private String unitOfWork = DefaultValues.stringVal(Len.UNIT_OF_WORK);
	//Original name: CSC-MSG-ID
	private String msgId = DefaultValues.stringVal(Len.MSG_ID);
	//Original name: CSC-SESSION-ID
	private String sessionId = DefaultValues.stringVal(Len.SESSION_ID);
	//Original name: CSC-AUTH-USERID
	private String authUserid = DefaultValues.stringVal(Len.AUTH_USERID);
	//Original name: CSC-REQUEST-MODULE
	private String requestModule = DefaultValues.stringVal(Len.REQUEST_MODULE);
	//Original name: CSC-RESPONSE-MODULE
	private String responseModule = DefaultValues.stringVal(Len.RESPONSE_MODULE);
	//Original name: CSC-REQ-SWITCHES-TSQ
	private String reqSwitchesTsq = DefaultValues.stringVal(Len.REQ_SWITCHES_TSQ);

	//==== METHODS ====
	public void setCscGeneralParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		operation = MarshalByte.readString(buffer, position, Len.OPERATION);
		position += Len.OPERATION;
		unitOfWork = MarshalByte.readString(buffer, position, Len.UNIT_OF_WORK);
		position += Len.UNIT_OF_WORK;
		msgId = MarshalByte.readString(buffer, position, Len.MSG_ID);
		position += Len.MSG_ID;
		sessionId = MarshalByte.readString(buffer, position, Len.SESSION_ID);
		position += Len.SESSION_ID;
		authUserid = MarshalByte.readString(buffer, position, Len.AUTH_USERID);
		position += Len.AUTH_USERID;
		requestModule = MarshalByte.readString(buffer, position, Len.REQUEST_MODULE);
		position += Len.REQUEST_MODULE;
		responseModule = MarshalByte.readString(buffer, position, Len.RESPONSE_MODULE);
		position += Len.RESPONSE_MODULE;
		reqSwitchesTsq = MarshalByte.readString(buffer, position, Len.REQ_SWITCHES_TSQ);
	}

	public byte[] getCscGeneralParmsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, operation, Len.OPERATION);
		position += Len.OPERATION;
		MarshalByte.writeString(buffer, position, unitOfWork, Len.UNIT_OF_WORK);
		position += Len.UNIT_OF_WORK;
		MarshalByte.writeString(buffer, position, msgId, Len.MSG_ID);
		position += Len.MSG_ID;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		position += Len.SESSION_ID;
		MarshalByte.writeString(buffer, position, authUserid, Len.AUTH_USERID);
		position += Len.AUTH_USERID;
		MarshalByte.writeString(buffer, position, requestModule, Len.REQUEST_MODULE);
		position += Len.REQUEST_MODULE;
		MarshalByte.writeString(buffer, position, responseModule, Len.RESPONSE_MODULE);
		position += Len.RESPONSE_MODULE;
		MarshalByte.writeString(buffer, position, reqSwitchesTsq, Len.REQ_SWITCHES_TSQ);
		return buffer;
	}

	public void setOperation(String operation) {
		this.operation = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.operation;
	}

	public void setUnitOfWork(String unitOfWork) {
		this.unitOfWork = Functions.subString(unitOfWork, Len.UNIT_OF_WORK);
	}

	public String getUnitOfWork() {
		return this.unitOfWork;
	}

	public void setMsgId(String msgId) {
		this.msgId = Functions.subString(msgId, Len.MSG_ID);
	}

	public String getMsgId() {
		return this.msgId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = Functions.subString(sessionId, Len.SESSION_ID);
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setAuthUserid(String authUserid) {
		this.authUserid = Functions.subString(authUserid, Len.AUTH_USERID);
	}

	public String getAuthUserid() {
		return this.authUserid;
	}

	public void setRequestModule(String requestModule) {
		this.requestModule = Functions.subString(requestModule, Len.REQUEST_MODULE);
	}

	public String getRequestModule() {
		return this.requestModule;
	}

	public String getCscRequestModuleFormatted() {
		return Functions.padBlanks(getRequestModule(), Len.REQUEST_MODULE);
	}

	public void setResponseModule(String responseModule) {
		this.responseModule = Functions.subString(responseModule, Len.RESPONSE_MODULE);
	}

	public String getResponseModule() {
		return this.responseModule;
	}

	public String getCscResponseModuleFormatted() {
		return Functions.padBlanks(getResponseModule(), Len.RESPONSE_MODULE);
	}

	public void setReqSwitchesTsq(String reqSwitchesTsq) {
		this.reqSwitchesTsq = Functions.subString(reqSwitchesTsq, Len.REQ_SWITCHES_TSQ);
	}

	public String getReqSwitchesTsq() {
		return this.reqSwitchesTsq;
	}

	public String getCscReqSwitchesTsqFormatted() {
		return Functions.padBlanks(getReqSwitchesTsq(), Len.REQ_SWITCHES_TSQ);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION = 32;
		public static final int UNIT_OF_WORK = 32;
		public static final int MSG_ID = 32;
		public static final int SESSION_ID = 32;
		public static final int AUTH_USERID = 32;
		public static final int REQUEST_MODULE = 8;
		public static final int RESPONSE_MODULE = 8;
		public static final int REQ_SWITCHES_TSQ = 16;
		public static final int CSC_GENERAL_PARMS = OPERATION + UNIT_OF_WORK + MSG_ID + SESSION_ID + AUTH_USERID + REQUEST_MODULE + RESPONSE_MODULE
				+ REQ_SWITCHES_TSQ;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
