/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC090CO<br>
 * Variable: XZC090CO from copybook XZC090CO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc090co {

	//==== PROPERTIES ====
	//Original name: XZ009O-TRS-DTL-RSP
	private Xz009oTrsDtlRsp trsDtlRsp = new Xz009oTrsDtlRsp();
	//Original name: XZ009O-ERROR-INDICATOR
	private String errorIndicator = DefaultValues.stringVal(Len.ERROR_INDICATOR);
	//Original name: XZ009O-FAULT-CODE
	private String faultCode = DefaultValues.stringVal(Len.FAULT_CODE);
	//Original name: XZ009O-FAULT-STRING
	private String faultString = DefaultValues.stringVal(Len.FAULT_STRING);
	//Original name: XZ009O-FAULT-ACTOR
	private String faultActor = DefaultValues.stringVal(Len.FAULT_ACTOR);
	//Original name: XZ009O-FAULT-DETAIL
	private String faultDetail = DefaultValues.stringVal(Len.FAULT_DETAIL);
	//Original name: XZ009O-NON-SOAP-FAULT-ERR-TXT
	private String nonSoapFaultErrTxt = DefaultValues.stringVal(Len.NON_SOAP_FAULT_ERR_TXT);

	//==== METHODS ====
	public void setErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		errorIndicator = MarshalByte.readString(buffer, position, Len.ERROR_INDICATOR);
		position += Len.ERROR_INDICATOR;
		faultCode = MarshalByte.readString(buffer, position, Len.FAULT_CODE);
		position += Len.FAULT_CODE;
		faultString = MarshalByte.readString(buffer, position, Len.FAULT_STRING);
		position += Len.FAULT_STRING;
		faultActor = MarshalByte.readString(buffer, position, Len.FAULT_ACTOR);
		position += Len.FAULT_ACTOR;
		faultDetail = MarshalByte.readString(buffer, position, Len.FAULT_DETAIL);
		position += Len.FAULT_DETAIL;
		nonSoapFaultErrTxt = MarshalByte.readString(buffer, position, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public byte[] getErrorInformationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, errorIndicator, Len.ERROR_INDICATOR);
		position += Len.ERROR_INDICATOR;
		MarshalByte.writeString(buffer, position, faultCode, Len.FAULT_CODE);
		position += Len.FAULT_CODE;
		MarshalByte.writeString(buffer, position, faultString, Len.FAULT_STRING);
		position += Len.FAULT_STRING;
		MarshalByte.writeString(buffer, position, faultActor, Len.FAULT_ACTOR);
		position += Len.FAULT_ACTOR;
		MarshalByte.writeString(buffer, position, faultDetail, Len.FAULT_DETAIL);
		position += Len.FAULT_DETAIL;
		MarshalByte.writeString(buffer, position, nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
		return buffer;
	}

	public void setErrorIndicator(String errorIndicator) {
		this.errorIndicator = Functions.subString(errorIndicator, Len.ERROR_INDICATOR);
	}

	public String getErrorIndicator() {
		return this.errorIndicator;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = Functions.subString(faultCode, Len.FAULT_CODE);
	}

	public String getFaultCode() {
		return this.faultCode;
	}

	public void setFaultString(String faultString) {
		this.faultString = Functions.subString(faultString, Len.FAULT_STRING);
	}

	public String getFaultString() {
		return this.faultString;
	}

	public void setFaultActor(String faultActor) {
		this.faultActor = Functions.subString(faultActor, Len.FAULT_ACTOR);
	}

	public String getFaultActor() {
		return this.faultActor;
	}

	public void setFaultDetail(String faultDetail) {
		this.faultDetail = Functions.subString(faultDetail, Len.FAULT_DETAIL);
	}

	public String getFaultDetail() {
		return this.faultDetail;
	}

	public void setNonSoapFaultErrTxt(String nonSoapFaultErrTxt) {
		this.nonSoapFaultErrTxt = Functions.subString(nonSoapFaultErrTxt, Len.NON_SOAP_FAULT_ERR_TXT);
	}

	public String getNonSoapFaultErrTxt() {
		return this.nonSoapFaultErrTxt;
	}

	public Xz009oTrsDtlRsp getTrsDtlRsp() {
		return trsDtlRsp;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_INDICATOR = 30;
		public static final int FAULT_CODE = 30;
		public static final int FAULT_STRING = 256;
		public static final int FAULT_ACTOR = 256;
		public static final int FAULT_DETAIL = 256;
		public static final int NON_SOAP_FAULT_ERR_TXT = 256;
		public static final int ERROR_INFORMATION = ERROR_INDICATOR + FAULT_CODE + FAULT_STRING + FAULT_ACTOR + FAULT_DETAIL + NON_SOAP_FAULT_ERR_TXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
