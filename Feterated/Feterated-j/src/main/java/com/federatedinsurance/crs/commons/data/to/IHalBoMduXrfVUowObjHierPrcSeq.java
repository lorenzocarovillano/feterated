/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [HAL_BO_MDU_XRF_V, HAL_UOW_OBJ_HIER_V, HAL_UOW_PRC_SEQ_V]
 * 
 */
public interface IHalBoMduXrfVUowObjHierPrcSeq extends BaseSqlTo {

	/**
	 * Host Variable WS-CUR1-BOBJ-NM
	 * 
	 */
	String getWsCur1BobjNm();

	void setWsCur1BobjNm(String wsCur1BobjNm);

	/**
	 * Host Variable HBMX-DP-DFL-MDU-NM
	 * 
	 */
	String getHbmxDpDflMduNm();

	void setHbmxDpDflMduNm(String hbmxDpDflMduNm);
};
