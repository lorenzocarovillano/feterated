/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0X0022<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class ConstantFieldsXz0x0022 {

	//==== PROPERTIES ====
	//Original name: CF-BLANK
	private char blank = Types.SPACE_CHAR;
	//Original name: CF-MAIN-DRIVER
	private String mainDriver = "TS020000";
	//Original name: CF-MAX-POL
	private short maxPol = ((short) 150);
	//Original name: CF-UNIT-OF-WORK
	private String unitOfWork = "XZ_SET_TMN_FLG";
	//Original name: CF-REQUEST-MODULE
	private String requestModule = "XZ0Q0022";
	//Original name: CF-RESPONSE-MODULE
	private String responseModule = "XZ0R0022";
	//Original name: CF-PROGRAM-NAME
	private String programName = "XZ0X0022";
	//Original name: CF-COPYBOOK-NAMES
	private CfCopybookNames copybookNames = new CfCopybookNames();

	//==== METHODS ====
	public char getBlank() {
		return this.blank;
	}

	public String getMainDriver() {
		return this.mainDriver;
	}

	public short getMaxPol() {
		return this.maxPol;
	}

	public String getUnitOfWork() {
		return this.unitOfWork;
	}

	public String getRequestModule() {
		return this.requestModule;
	}

	public String getResponseModule() {
		return this.responseModule;
	}

	public String getProgramName() {
		return this.programName;
	}

	public CfCopybookNames getCopybookNames() {
		return copybookNames;
	}
}
