/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.copy.Xz0y8010;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0P90J0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0p90j0Data implements IActNot {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0p90j0 constantFields = new ConstantFieldsXz0p90j0();
	//Original name: ES-01-FATAL-ERROR-MSG
	private Es01FatalErrorMsg es01FatalErrorMsg = new Es01FatalErrorMsg();
	/**Original name: NI-CER-HLD-NOT-IND<br>
	 * <pre>*  NULL-INDICATORS.</pre>*/
	private short niCerHldNotInd = DefaultValues.BIN_SHORT_VAL;
	//Original name: SS-MSG-IDX
	private short ssMsgIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_MSG_IDX_MAX = ((short) 10);
	//Original name: SS-WNG-IDX
	private short ssWngIdx = DefaultValues.BIN_SHORT_VAL;
	public static final short SS_WNG_IDX_MAX = ((short) 10);
	//Original name: SWITCHES
	private SwitchesCawpcorc switches = new SwitchesCawpcorc();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0p90j0 workingStorageArea = new WorkingStorageAreaXz0p90j0();
	//Original name: WS-XZ0Y90J0-ROW
	private WsXz0y90j0Row wsXz0y90j0Row = new WsXz0y90j0Row();
	//Original name: XZ0Y8010
	private Xz0y8010 xz0y8010 = new Xz0y8010();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: WS-PROXY-PROGRAM-AREA
	private WsProxyProgramArea wsProxyProgramArea = new WsProxyProgramArea();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public void setNiCerHldNotInd(short niCerHldNotInd) {
		this.niCerHldNotInd = niCerHldNotInd;
	}

	public short getNiCerHldNotInd() {
		return this.niCerHldNotInd;
	}

	public void setSsMsgIdx(short ssMsgIdx) {
		this.ssMsgIdx = ssMsgIdx;
	}

	public short getSsMsgIdx() {
		return this.ssMsgIdx;
	}

	public boolean isSsMsgIdxMax() {
		return ssMsgIdx == SS_MSG_IDX_MAX;
	}

	public void setSsWngIdx(short ssWngIdx) {
		this.ssWngIdx = ssWngIdx;
	}

	public short getSsWngIdx() {
		return this.ssWngIdx;
	}

	public boolean isSsWngIdxMax() {
		return ssWngIdx == SS_WNG_IDX_MAX;
	}

	public void setWsXz0y8010Formatted(String data) {
		byte[] buffer = new byte[Len.WS_XZ0Y8010];
		MarshalByte.writeString(buffer, 1, data, Len.WS_XZ0Y8010);
		setWsXz0y8010Bytes(buffer, 1);
	}

	public String getWsXz0y8010Formatted() {
		return xz0y8010.getInputOutputParmsFormatted();
	}

	public void setWsXz0y8010Bytes(byte[] buffer, int offset) {
		int position = offset;
		xz0y8010.setInputOutputParmsBytes(buffer, position);
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	@Override
	public String getActNotStaCd() {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		throw new FieldNotMappedException("actNotStaCd");
	}

	@Override
	public String getActNotTypCd() {
		return dclactNot.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNot.setActNotTypCd(actNotTypCd);
	}

	@Override
	public String getActOwnAdrId() {
		throw new FieldNotMappedException("actOwnAdrId");
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		throw new FieldNotMappedException("actOwnAdrId");
	}

	@Override
	public String getActOwnCltId() {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		throw new FieldNotMappedException("actOwnCltId");
	}

	@Override
	public String getActTypCd() {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public void setActTypCd(String actTypCd) {
		throw new FieldNotMappedException("actTypCd");
	}

	@Override
	public String getActTypCdObj() {
		return getActTypCd();
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		setActTypCd(actTypCdObj);
	}

	@Override
	public short getAddCncDay() {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		throw new FieldNotMappedException("addCncDay");
	}

	@Override
	public Short getAddCncDayObj() {
		return (getAddCncDay());
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		setAddCncDay((addCncDayObj));
	}

	@Override
	public char getCerHldNotInd() {
		return dclactNot.getCerHldNotInd();
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		this.dclactNot.setCerHldNotInd(cerHldNotInd);
	}

	@Override
	public Character getCerHldNotIndObj() {
		if (getNiCerHldNotInd() >= 0) {
			return (getCerHldNotInd());
		} else {
			return null;
		}
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		if (cerHldNotIndObj != null) {
			setCerHldNotInd((cerHldNotIndObj));
			setNiCerHldNotInd(((short) 0));
		} else {
			setNiCerHldNotInd(((short) -1));
		}
	}

	public ConstantFieldsXz0p90j0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		throw new FieldNotMappedException("csrActNbr");
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		throw new FieldNotMappedException("csrActNbr");
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	@Override
	public String getEmpId() {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public void setEmpId(String empId) {
		throw new FieldNotMappedException("empId");
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	public Es01FatalErrorMsg getEs01FatalErrorMsg() {
		return es01FatalErrorMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public void setNotDt(String notDt) {
		throw new FieldNotMappedException("notDt");
	}

	@Override
	public String getNotPrcTs() {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		throw new FieldNotMappedException("notPrcTs");
	}

	@Override
	public String getPdcNbr() {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		throw new FieldNotMappedException("pdcNbr");
	}

	@Override
	public String getPdcNbrObj() {
		return getPdcNbr();
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		setPdcNbr(pdcNbrObj);
	}

	@Override
	public String getPdcNm() {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public void setPdcNm(String pdcNm) {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public String getPdcNmObj() {
		return getPdcNm();
	}

	@Override
	public void setPdcNmObj(String pdcNmObj) {
		setPdcNm(pdcNmObj);
	}

	@Override
	public String getReaDes() {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public void setReaDes(String reaDes) {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public String getReaDesObj() {
		return getReaDes();
	}

	@Override
	public void setReaDesObj(String reaDesObj) {
		setReaDes(reaDesObj);
	}

	@Override
	public String getSaNotPrcTsTime() {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public String getSegCd() {
		throw new FieldNotMappedException("segCd");
	}

	@Override
	public void setSegCd(String segCd) {
		throw new FieldNotMappedException("segCd");
	}

	@Override
	public String getSegCdObj() {
		return getSegCd();
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		setSegCd(segCdObj);
	}

	@Override
	public String getStAbb() {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public void setStAbb(String stAbb) {
		throw new FieldNotMappedException("stAbb");
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	@Override
	public String getStaMdfTs() {
		throw new FieldNotMappedException("staMdfTs");
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		throw new FieldNotMappedException("staMdfTs");
	}

	public SwitchesCawpcorc getSwitches() {
		return switches;
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		throw new FieldNotMappedException("totFeeAmt");
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0p90j0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsProxyProgramArea getWsProxyProgramArea() {
		return wsProxyProgramArea;
	}

	public WsXz0y90j0Row getWsXz0y90j0Row() {
		return wsXz0y90j0Row;
	}

	public Xz0y8010 getXz0y8010() {
		return xz0y8010;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_XZ0Y8010 = Xz0y8010.Len.INPUT_OUTPUT_PARMS;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
