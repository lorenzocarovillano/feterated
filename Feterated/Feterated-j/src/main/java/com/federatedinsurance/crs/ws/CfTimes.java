/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-TIMES<br>
 * Variable: CF-TIMES from program XZ0B9060<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class CfTimes {

	//==== PROPERTIES ====
	//Original name: CF-1ST-DSY-PRT-TIME
	private String cf1stDsyPrtTime = "00.00.00";
	//Original name: CF-2ND-DSY-PRT-TIME
	private String cf2ndDsyPrtTime = "07.30.00";
	//Original name: CF-3RD-DSY-PRT-TIME
	private String cf3rdDsyPrtTime = "08.30.00";
	//Original name: CF-4TH-DSY-PRT-TIME
	private String cf4thDsyPrtTime = "09.30.00";
	//Original name: CF-5TH-DSY-PRT-TIME
	private String cf5thDsyPrtTime = "10.30.00";
	//Original name: CF-6TH-DSY-PRT-TIME
	private String cf6thDsyPrtTime = "11.30.00";
	//Original name: CF-7TH-DSY-PRT-TIME
	private String cf7thDsyPrtTime = "12.30.00";

	//==== METHODS ====
	public String getCf1stDsyPrtTime() {
		return this.cf1stDsyPrtTime;
	}

	public String getCf2ndDsyPrtTime() {
		return this.cf2ndDsyPrtTime;
	}

	public String getCf3rdDsyPrtTime() {
		return this.cf3rdDsyPrtTime;
	}

	public String getCf4thDsyPrtTime() {
		return this.cf4thDsyPrtTime;
	}

	public String getCf5thDsyPrtTime() {
		return this.cf5thDsyPrtTime;
	}

	public String getCf6thDsyPrtTime() {
		return this.cf6thDsyPrtTime;
	}

	public String getCf7thDsyPrtTime() {
		return this.cf7thDsyPrtTime;
	}
}
