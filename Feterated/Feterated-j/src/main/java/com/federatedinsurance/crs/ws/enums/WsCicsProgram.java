/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-CICS-PROGRAM<br>
 * Variable: WS-CICS-PROGRAM from program FNC02090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCicsProgram {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.CICS_PROGRAM);
	public static final String WEB_SVC_LKU_PGM = "TS571099";
	public static final String CALLABLE_SVC_PGM = "GIICALS";

	//==== METHODS ====
	public void setCicsProgram(String cicsProgram) {
		this.value = Functions.subString(cicsProgram, Len.CICS_PROGRAM);
	}

	public String getCicsProgram() {
		return this.value;
	}

	public void setWebSvcLkuPgm() {
		value = WEB_SVC_LKU_PGM;
	}

	public void setCallableSvcPgm() {
		value = CALLABLE_SVC_PGM;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_PROGRAM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
