/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: O-TSX132CM-COM-RECORD<br>
 * Variable: O-TSX132CM-COM-RECORD from program TS030099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class OTsx132cmComRecord {

	//==== PROPERTIES ====
	//Original name: TCO-132-DIVISION
	private char division = DefaultValues.CHAR_VAL;
	//Original name: TCO-132-SUBDIVISION
	private char subdivision = DefaultValues.CHAR_VAL;
	//Original name: TCO-132-REPORT-NUMBER
	private String reportNumber = DefaultValues.stringVal(Len.REPORT_NUMBER);
	//Original name: TCO-132-LINES-PER-PAGE
	private String linesPerPage = DefaultValues.stringVal(Len.LINES_PER_PAGE);
	//Original name: TCO-132-CARRIAGE-CONTROL
	private char carriageControl = DefaultValues.CHAR_VAL;
	//Original name: TCO-132-BEFORE-OR-AFTER
	private char beforeOrAfter = DefaultValues.CHAR_VAL;
	//Original name: TCO-132-DATA
	private String data2 = DefaultValues.stringVal(Len.DATA2);

	//==== METHODS ====
	public void setoTsx132cmComRecordBytes(byte[] buffer) {
		setoTsx132cmComRecordBytes(buffer, 1);
	}

	public void setoTsx132cmComRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		setHeadingBytes(buffer, position);
		position += Len.HEADING;
		data2 = MarshalByte.readString(buffer, position, Len.DATA2);
	}

	public byte[] getoTsx132cmComRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		getHeadingBytes(buffer, position);
		position += Len.HEADING;
		MarshalByte.writeString(buffer, position, data2, Len.DATA2);
		return buffer;
	}

	public void setHeadingBytes(byte[] buffer, int offset) {
		int position = offset;
		setOfficeLocationBytes(buffer, position);
		position += Len.OFFICE_LOCATION;
		reportNumber = MarshalByte.readString(buffer, position, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		linesPerPage = MarshalByte.readString(buffer, position, Len.LINES_PER_PAGE);
		position += Len.LINES_PER_PAGE;
		carriageControl = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		beforeOrAfter = MarshalByte.readChar(buffer, position);
	}

	public byte[] getHeadingBytes(byte[] buffer, int offset) {
		int position = offset;
		getOfficeLocationBytes(buffer, position);
		position += Len.OFFICE_LOCATION;
		MarshalByte.writeString(buffer, position, reportNumber, Len.REPORT_NUMBER);
		position += Len.REPORT_NUMBER;
		MarshalByte.writeString(buffer, position, linesPerPage, Len.LINES_PER_PAGE);
		position += Len.LINES_PER_PAGE;
		MarshalByte.writeChar(buffer, position, carriageControl);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, beforeOrAfter);
		return buffer;
	}

	public void setOfficeLocationBytes(byte[] buffer, int offset) {
		int position = offset;
		division = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		subdivision = MarshalByte.readChar(buffer, position);
	}

	public byte[] getOfficeLocationBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, division);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, subdivision);
		return buffer;
	}

	public void setDivision(char division) {
		this.division = division;
	}

	public char getDivision() {
		return this.division;
	}

	public void setSubdivision(char subdivision) {
		this.subdivision = subdivision;
	}

	public char getSubdivision() {
		return this.subdivision;
	}

	public void setReportNumber(String reportNumber) {
		this.reportNumber = Functions.subString(reportNumber, Len.REPORT_NUMBER);
	}

	public String getReportNumber() {
		return this.reportNumber;
	}

	public void setLinesPerPage(String linesPerPage) {
		this.linesPerPage = Functions.subString(linesPerPage, Len.LINES_PER_PAGE);
	}

	public String getLinesPerPage() {
		return this.linesPerPage;
	}

	public void setCarriageControl(char carriageControl) {
		this.carriageControl = carriageControl;
	}

	public char getCarriageControl() {
		return this.carriageControl;
	}

	public void setBeforeOrAfter(char beforeOrAfter) {
		this.beforeOrAfter = beforeOrAfter;
	}

	public char getBeforeOrAfter() {
		return this.beforeOrAfter;
	}

	public void setData2(String data2) {
		this.data2 = Functions.subString(data2, Len.DATA2);
	}

	public String getData2() {
		return this.data2;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DIVISION = 1;
		public static final int SUBDIVISION = 1;
		public static final int OFFICE_LOCATION = DIVISION + SUBDIVISION;
		public static final int REPORT_NUMBER = 6;
		public static final int LINES_PER_PAGE = 2;
		public static final int CARRIAGE_CONTROL = 1;
		public static final int BEFORE_OR_AFTER = 1;
		public static final int HEADING = OFFICE_LOCATION + REPORT_NUMBER + LINES_PER_PAGE + CARRIAGE_CONTROL + BEFORE_OR_AFTER;
		public static final int DATA2 = 132;
		public static final int O_TSX132CM_COM_RECORD = HEADING + DATA2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
