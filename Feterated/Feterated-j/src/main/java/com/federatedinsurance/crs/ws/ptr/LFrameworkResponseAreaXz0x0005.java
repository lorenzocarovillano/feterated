/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0X0005<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0x0005 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0x0005() {
	}

	public LFrameworkResponseAreaXz0x0005(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXzc001ActNotRowFormatted(String data) {
		writeString(Pos.XZC001R_ACT_NOT_ROW, data, Len.XZC001R_ACT_NOT_ROW);
	}

	public void setXzc001rActNotCsumFormatted(String xzc001rActNotCsum) {
		writeString(Pos.XZC001R_ACT_NOT_CSUM, Trunc.toUnsignedNumeric(xzc001rActNotCsum, Len.XZC001R_ACT_NOT_CSUM), Len.XZC001R_ACT_NOT_CSUM);
	}

	/**Original name: XZC001R-ACT-NOT-CSUM<br>*/
	public int getXzc001rActNotCsum() {
		return readNumDispUnsignedInt(Pos.XZC001R_ACT_NOT_CSUM, Len.XZC001R_ACT_NOT_CSUM);
	}

	public String getXzc001rActNotCsumFormatted() {
		return readFixedString(Pos.XZC001R_ACT_NOT_CSUM, Len.XZC001R_ACT_NOT_CSUM);
	}

	public void setXzc001rCsrActNbrKcre(String xzc001rCsrActNbrKcre) {
		writeString(Pos.XZC001R_CSR_ACT_NBR_KCRE, xzc001rCsrActNbrKcre, Len.XZC001R_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC001R-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc001rCsrActNbrKcre() {
		return readString(Pos.XZC001R_CSR_ACT_NBR_KCRE, Len.XZC001R_CSR_ACT_NBR_KCRE);
	}

	public void setXzc001rNotPrcTsKcre(String xzc001rNotPrcTsKcre) {
		writeString(Pos.XZC001R_NOT_PRC_TS_KCRE, xzc001rNotPrcTsKcre, Len.XZC001R_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC001R-NOT-PRC-TS-KCRE<br>*/
	public String getXzc001rNotPrcTsKcre() {
		return readString(Pos.XZC001R_NOT_PRC_TS_KCRE, Len.XZC001R_NOT_PRC_TS_KCRE);
	}

	public void setXzc001rTransProcessDt(String xzc001rTransProcessDt) {
		writeString(Pos.XZC001R_TRANS_PROCESS_DT, xzc001rTransProcessDt, Len.XZC001R_TRANS_PROCESS_DT);
	}

	/**Original name: XZC001R-TRANS-PROCESS-DT<br>*/
	public String getXzc001rTransProcessDt() {
		return readString(Pos.XZC001R_TRANS_PROCESS_DT, Len.XZC001R_TRANS_PROCESS_DT);
	}

	public void setXzc001rCsrActNbr(String xzc001rCsrActNbr) {
		writeString(Pos.XZC001R_CSR_ACT_NBR, xzc001rCsrActNbr, Len.XZC001R_CSR_ACT_NBR);
	}

	/**Original name: XZC001R-CSR-ACT-NBR<br>*/
	public String getXzc001rCsrActNbr() {
		return readString(Pos.XZC001R_CSR_ACT_NBR, Len.XZC001R_CSR_ACT_NBR);
	}

	public String getXzc001CsrActNbrFormatted() {
		return Functions.padBlanks(getXzc001rCsrActNbr(), Len.XZC001R_CSR_ACT_NBR);
	}

	public void setXzc001rNotPrcTs(String xzc001rNotPrcTs) {
		writeString(Pos.XZC001R_NOT_PRC_TS, xzc001rNotPrcTs, Len.XZC001R_NOT_PRC_TS);
	}

	/**Original name: XZC001R-NOT-PRC-TS<br>*/
	public String getXzc001rNotPrcTs() {
		return readString(Pos.XZC001R_NOT_PRC_TS, Len.XZC001R_NOT_PRC_TS);
	}

	public String getXzc001NotPrcTsFormatted() {
		return Functions.padBlanks(getXzc001rNotPrcTs(), Len.XZC001R_NOT_PRC_TS);
	}

	public void setXzc001rCsrActNbrCi(char xzc001rCsrActNbrCi) {
		writeChar(Pos.XZC001R_CSR_ACT_NBR_CI, xzc001rCsrActNbrCi);
	}

	/**Original name: XZC001R-CSR-ACT-NBR-CI<br>*/
	public char getXzc001rCsrActNbrCi() {
		return readChar(Pos.XZC001R_CSR_ACT_NBR_CI);
	}

	public void setXzc001rNotPrcTsCi(char xzc001rNotPrcTsCi) {
		writeChar(Pos.XZC001R_NOT_PRC_TS_CI, xzc001rNotPrcTsCi);
	}

	/**Original name: XZC001R-NOT-PRC-TS-CI<br>*/
	public char getXzc001rNotPrcTsCi() {
		return readChar(Pos.XZC001R_NOT_PRC_TS_CI);
	}

	public void setXzc001rActNotTypCdCi(char xzc001rActNotTypCdCi) {
		writeChar(Pos.XZC001R_ACT_NOT_TYP_CD_CI, xzc001rActNotTypCdCi);
	}

	/**Original name: XZC001R-ACT-NOT-TYP-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getXzc001rActNotTypCdCi() {
		return readChar(Pos.XZC001R_ACT_NOT_TYP_CD_CI);
	}

	public void setXzc001rActNotTypCd(String xzc001rActNotTypCd) {
		writeString(Pos.XZC001R_ACT_NOT_TYP_CD, xzc001rActNotTypCd, Len.XZC001R_ACT_NOT_TYP_CD);
	}

	/**Original name: XZC001R-ACT-NOT-TYP-CD<br>*/
	public String getXzc001rActNotTypCd() {
		return readString(Pos.XZC001R_ACT_NOT_TYP_CD, Len.XZC001R_ACT_NOT_TYP_CD);
	}

	public void setXzc001rNotDtCi(char xzc001rNotDtCi) {
		writeChar(Pos.XZC001R_NOT_DT_CI, xzc001rNotDtCi);
	}

	/**Original name: XZC001R-NOT-DT-CI<br>*/
	public char getXzc001rNotDtCi() {
		return readChar(Pos.XZC001R_NOT_DT_CI);
	}

	public void setXzc001rNotDt(String xzc001rNotDt) {
		writeString(Pos.XZC001R_NOT_DT, xzc001rNotDt, Len.XZC001R_NOT_DT);
	}

	/**Original name: XZC001R-NOT-DT<br>*/
	public String getXzc001rNotDt() {
		return readString(Pos.XZC001R_NOT_DT, Len.XZC001R_NOT_DT);
	}

	public void setXzc001rActOwnCltIdCi(char xzc001rActOwnCltIdCi) {
		writeChar(Pos.XZC001R_ACT_OWN_CLT_ID_CI, xzc001rActOwnCltIdCi);
	}

	/**Original name: XZC001R-ACT-OWN-CLT-ID-CI<br>*/
	public char getXzc001rActOwnCltIdCi() {
		return readChar(Pos.XZC001R_ACT_OWN_CLT_ID_CI);
	}

	public void setXzc001rActOwnCltId(String xzc001rActOwnCltId) {
		writeString(Pos.XZC001R_ACT_OWN_CLT_ID, xzc001rActOwnCltId, Len.XZC001R_ACT_OWN_CLT_ID);
	}

	/**Original name: XZC001R-ACT-OWN-CLT-ID<br>*/
	public String getXzc001rActOwnCltId() {
		return readString(Pos.XZC001R_ACT_OWN_CLT_ID, Len.XZC001R_ACT_OWN_CLT_ID);
	}

	public void setXzc001rActOwnAdrIdCi(char xzc001rActOwnAdrIdCi) {
		writeChar(Pos.XZC001R_ACT_OWN_ADR_ID_CI, xzc001rActOwnAdrIdCi);
	}

	/**Original name: XZC001R-ACT-OWN-ADR-ID-CI<br>*/
	public char getXzc001rActOwnAdrIdCi() {
		return readChar(Pos.XZC001R_ACT_OWN_ADR_ID_CI);
	}

	public void setXzc001rActOwnAdrId(String xzc001rActOwnAdrId) {
		writeString(Pos.XZC001R_ACT_OWN_ADR_ID, xzc001rActOwnAdrId, Len.XZC001R_ACT_OWN_ADR_ID);
	}

	/**Original name: XZC001R-ACT-OWN-ADR-ID<br>*/
	public String getXzc001rActOwnAdrId() {
		return readString(Pos.XZC001R_ACT_OWN_ADR_ID, Len.XZC001R_ACT_OWN_ADR_ID);
	}

	public void setXzc001rEmpIdCi(char xzc001rEmpIdCi) {
		writeChar(Pos.XZC001R_EMP_ID_CI, xzc001rEmpIdCi);
	}

	/**Original name: XZC001R-EMP-ID-CI<br>*/
	public char getXzc001rEmpIdCi() {
		return readChar(Pos.XZC001R_EMP_ID_CI);
	}

	public void setXzc001rEmpIdNi(char xzc001rEmpIdNi) {
		writeChar(Pos.XZC001R_EMP_ID_NI, xzc001rEmpIdNi);
	}

	/**Original name: XZC001R-EMP-ID-NI<br>*/
	public char getXzc001rEmpIdNi() {
		return readChar(Pos.XZC001R_EMP_ID_NI);
	}

	public void setXzc001rEmpId(String xzc001rEmpId) {
		writeString(Pos.XZC001R_EMP_ID, xzc001rEmpId, Len.XZC001R_EMP_ID);
	}

	/**Original name: XZC001R-EMP-ID<br>*/
	public String getXzc001rEmpId() {
		return readString(Pos.XZC001R_EMP_ID, Len.XZC001R_EMP_ID);
	}

	public void setXzc001rStaMdfTsCi(char xzc001rStaMdfTsCi) {
		writeChar(Pos.XZC001R_STA_MDF_TS_CI, xzc001rStaMdfTsCi);
	}

	/**Original name: XZC001R-STA-MDF-TS-CI<br>*/
	public char getXzc001rStaMdfTsCi() {
		return readChar(Pos.XZC001R_STA_MDF_TS_CI);
	}

	public void setXzc001rStaMdfTs(String xzc001rStaMdfTs) {
		writeString(Pos.XZC001R_STA_MDF_TS, xzc001rStaMdfTs, Len.XZC001R_STA_MDF_TS);
	}

	/**Original name: XZC001R-STA-MDF-TS<br>*/
	public String getXzc001rStaMdfTs() {
		return readString(Pos.XZC001R_STA_MDF_TS, Len.XZC001R_STA_MDF_TS);
	}

	public void setXzc001rActNotStaCdCi(char xzc001rActNotStaCdCi) {
		writeChar(Pos.XZC001R_ACT_NOT_STA_CD_CI, xzc001rActNotStaCdCi);
	}

	/**Original name: XZC001R-ACT-NOT-STA-CD-CI<br>*/
	public char getXzc001rActNotStaCdCi() {
		return readChar(Pos.XZC001R_ACT_NOT_STA_CD_CI);
	}

	public void setXzc001rActNotStaCd(String xzc001rActNotStaCd) {
		writeString(Pos.XZC001R_ACT_NOT_STA_CD, xzc001rActNotStaCd, Len.XZC001R_ACT_NOT_STA_CD);
	}

	/**Original name: XZC001R-ACT-NOT-STA-CD<br>*/
	public String getXzc001rActNotStaCd() {
		return readString(Pos.XZC001R_ACT_NOT_STA_CD, Len.XZC001R_ACT_NOT_STA_CD);
	}

	public void setXzc001rPdcNbrCi(char xzc001rPdcNbrCi) {
		writeChar(Pos.XZC001R_PDC_NBR_CI, xzc001rPdcNbrCi);
	}

	/**Original name: XZC001R-PDC-NBR-CI<br>*/
	public char getXzc001rPdcNbrCi() {
		return readChar(Pos.XZC001R_PDC_NBR_CI);
	}

	public void setXzc001rPdcNbrNi(char xzc001rPdcNbrNi) {
		writeChar(Pos.XZC001R_PDC_NBR_NI, xzc001rPdcNbrNi);
	}

	/**Original name: XZC001R-PDC-NBR-NI<br>*/
	public char getXzc001rPdcNbrNi() {
		return readChar(Pos.XZC001R_PDC_NBR_NI);
	}

	public void setXzc001rPdcNbr(String xzc001rPdcNbr) {
		writeString(Pos.XZC001R_PDC_NBR, xzc001rPdcNbr, Len.XZC001R_PDC_NBR);
	}

	/**Original name: XZC001R-PDC-NBR<br>*/
	public String getXzc001rPdcNbr() {
		return readString(Pos.XZC001R_PDC_NBR, Len.XZC001R_PDC_NBR);
	}

	public void setXzc001rPdcNmCi(char xzc001rPdcNmCi) {
		writeChar(Pos.XZC001R_PDC_NM_CI, xzc001rPdcNmCi);
	}

	/**Original name: XZC001R-PDC-NM-CI<br>*/
	public char getXzc001rPdcNmCi() {
		return readChar(Pos.XZC001R_PDC_NM_CI);
	}

	public void setXzc001rPdcNmNi(char xzc001rPdcNmNi) {
		writeChar(Pos.XZC001R_PDC_NM_NI, xzc001rPdcNmNi);
	}

	/**Original name: XZC001R-PDC-NM-NI<br>*/
	public char getXzc001rPdcNmNi() {
		return readChar(Pos.XZC001R_PDC_NM_NI);
	}

	public void setXzc001rPdcNm(String xzc001rPdcNm) {
		writeString(Pos.XZC001R_PDC_NM, xzc001rPdcNm, Len.XZC001R_PDC_NM);
	}

	/**Original name: XZC001R-PDC-NM<br>*/
	public String getXzc001rPdcNm() {
		return readString(Pos.XZC001R_PDC_NM, Len.XZC001R_PDC_NM);
	}

	public void setXzc001rSegCdCi(char xzc001rSegCdCi) {
		writeChar(Pos.XZC001R_SEG_CD_CI, xzc001rSegCdCi);
	}

	/**Original name: XZC001R-SEG-CD-CI<br>*/
	public char getXzc001rSegCdCi() {
		return readChar(Pos.XZC001R_SEG_CD_CI);
	}

	public void setXzc001rSegCdNi(char xzc001rSegCdNi) {
		writeChar(Pos.XZC001R_SEG_CD_NI, xzc001rSegCdNi);
	}

	/**Original name: XZC001R-SEG-CD-NI<br>*/
	public char getXzc001rSegCdNi() {
		return readChar(Pos.XZC001R_SEG_CD_NI);
	}

	public void setXzc001rSegCd(String xzc001rSegCd) {
		writeString(Pos.XZC001R_SEG_CD, xzc001rSegCd, Len.XZC001R_SEG_CD);
	}

	/**Original name: XZC001R-SEG-CD<br>*/
	public String getXzc001rSegCd() {
		return readString(Pos.XZC001R_SEG_CD, Len.XZC001R_SEG_CD);
	}

	public void setXzc001rActTypCdCi(char xzc001rActTypCdCi) {
		writeChar(Pos.XZC001R_ACT_TYP_CD_CI, xzc001rActTypCdCi);
	}

	/**Original name: XZC001R-ACT-TYP-CD-CI<br>*/
	public char getXzc001rActTypCdCi() {
		return readChar(Pos.XZC001R_ACT_TYP_CD_CI);
	}

	public void setXzc001rActTypCdNi(char xzc001rActTypCdNi) {
		writeChar(Pos.XZC001R_ACT_TYP_CD_NI, xzc001rActTypCdNi);
	}

	/**Original name: XZC001R-ACT-TYP-CD-NI<br>*/
	public char getXzc001rActTypCdNi() {
		return readChar(Pos.XZC001R_ACT_TYP_CD_NI);
	}

	public void setXzc001rActTypCd(String xzc001rActTypCd) {
		writeString(Pos.XZC001R_ACT_TYP_CD, xzc001rActTypCd, Len.XZC001R_ACT_TYP_CD);
	}

	/**Original name: XZC001R-ACT-TYP-CD<br>*/
	public String getXzc001rActTypCd() {
		return readString(Pos.XZC001R_ACT_TYP_CD, Len.XZC001R_ACT_TYP_CD);
	}

	public void setXzc001rTotFeeAmtCi(char xzc001rTotFeeAmtCi) {
		writeChar(Pos.XZC001R_TOT_FEE_AMT_CI, xzc001rTotFeeAmtCi);
	}

	/**Original name: XZC001R-TOT-FEE-AMT-CI<br>*/
	public char getXzc001rTotFeeAmtCi() {
		return readChar(Pos.XZC001R_TOT_FEE_AMT_CI);
	}

	public void setXzc001rTotFeeAmtNi(char xzc001rTotFeeAmtNi) {
		writeChar(Pos.XZC001R_TOT_FEE_AMT_NI, xzc001rTotFeeAmtNi);
	}

	/**Original name: XZC001R-TOT-FEE-AMT-NI<br>*/
	public char getXzc001rTotFeeAmtNi() {
		return readChar(Pos.XZC001R_TOT_FEE_AMT_NI);
	}

	public void setXzc001rTotFeeAmtSign(char xzc001rTotFeeAmtSign) {
		writeChar(Pos.XZC001R_TOT_FEE_AMT_SIGN, xzc001rTotFeeAmtSign);
	}

	/**Original name: XZC001R-TOT-FEE-AMT-SIGN<br>*/
	public char getXzc001rTotFeeAmtSign() {
		return readChar(Pos.XZC001R_TOT_FEE_AMT_SIGN);
	}

	public void setXzc001rTotFeeAmt(AfDecimal xzc001rTotFeeAmt) {
		writeDecimal(Pos.XZC001R_TOT_FEE_AMT, xzc001rTotFeeAmt.copy(), SignType.NO_SIGN);
	}

	/**Original name: XZC001R-TOT-FEE-AMT<br>*/
	public AfDecimal getXzc001rTotFeeAmt() {
		return readDecimal(Pos.XZC001R_TOT_FEE_AMT, Len.Int.XZC001R_TOT_FEE_AMT, Len.Fract.XZC001R_TOT_FEE_AMT, SignType.NO_SIGN);
	}

	public void setXzc001rStAbbCi(char xzc001rStAbbCi) {
		writeChar(Pos.XZC001R_ST_ABB_CI, xzc001rStAbbCi);
	}

	/**Original name: XZC001R-ST-ABB-CI<br>*/
	public char getXzc001rStAbbCi() {
		return readChar(Pos.XZC001R_ST_ABB_CI);
	}

	public void setXzc001rStAbbNi(char xzc001rStAbbNi) {
		writeChar(Pos.XZC001R_ST_ABB_NI, xzc001rStAbbNi);
	}

	/**Original name: XZC001R-ST-ABB-NI<br>*/
	public char getXzc001rStAbbNi() {
		return readChar(Pos.XZC001R_ST_ABB_NI);
	}

	public void setXzc001rStAbb(String xzc001rStAbb) {
		writeString(Pos.XZC001R_ST_ABB, xzc001rStAbb, Len.XZC001R_ST_ABB);
	}

	/**Original name: XZC001R-ST-ABB<br>*/
	public String getXzc001rStAbb() {
		return readString(Pos.XZC001R_ST_ABB, Len.XZC001R_ST_ABB);
	}

	public void setXzc001rCerHldNotIndCi(char xzc001rCerHldNotIndCi) {
		writeChar(Pos.XZC001R_CER_HLD_NOT_IND_CI, xzc001rCerHldNotIndCi);
	}

	/**Original name: XZC001R-CER-HLD-NOT-IND-CI<br>*/
	public char getXzc001rCerHldNotIndCi() {
		return readChar(Pos.XZC001R_CER_HLD_NOT_IND_CI);
	}

	public void setXzc001rCerHldNotIndNi(char xzc001rCerHldNotIndNi) {
		writeChar(Pos.XZC001R_CER_HLD_NOT_IND_NI, xzc001rCerHldNotIndNi);
	}

	/**Original name: XZC001R-CER-HLD-NOT-IND-NI<br>*/
	public char getXzc001rCerHldNotIndNi() {
		return readChar(Pos.XZC001R_CER_HLD_NOT_IND_NI);
	}

	public void setXzc001rCerHldNotInd(char xzc001rCerHldNotInd) {
		writeChar(Pos.XZC001R_CER_HLD_NOT_IND, xzc001rCerHldNotInd);
	}

	/**Original name: XZC001R-CER-HLD-NOT-IND<br>*/
	public char getXzc001rCerHldNotInd() {
		return readChar(Pos.XZC001R_CER_HLD_NOT_IND);
	}

	public void setXzc001rAddCncDayCi(char xzc001rAddCncDayCi) {
		writeChar(Pos.XZC001R_ADD_CNC_DAY_CI, xzc001rAddCncDayCi);
	}

	/**Original name: XZC001R-ADD-CNC-DAY-CI<br>*/
	public char getXzc001rAddCncDayCi() {
		return readChar(Pos.XZC001R_ADD_CNC_DAY_CI);
	}

	public void setXzc001rAddCncDayNi(char xzc001rAddCncDayNi) {
		writeChar(Pos.XZC001R_ADD_CNC_DAY_NI, xzc001rAddCncDayNi);
	}

	/**Original name: XZC001R-ADD-CNC-DAY-NI<br>*/
	public char getXzc001rAddCncDayNi() {
		return readChar(Pos.XZC001R_ADD_CNC_DAY_NI);
	}

	public void setXzc001rAddCncDaySign(char xzc001rAddCncDaySign) {
		writeChar(Pos.XZC001R_ADD_CNC_DAY_SIGN, xzc001rAddCncDaySign);
	}

	/**Original name: XZC001R-ADD-CNC-DAY-SIGN<br>*/
	public char getXzc001rAddCncDaySign() {
		return readChar(Pos.XZC001R_ADD_CNC_DAY_SIGN);
	}

	public void setXzc001rAddCncDayFormatted(String xzc001rAddCncDay) {
		writeString(Pos.XZC001R_ADD_CNC_DAY, Trunc.toUnsignedNumeric(xzc001rAddCncDay, Len.XZC001R_ADD_CNC_DAY), Len.XZC001R_ADD_CNC_DAY);
	}

	/**Original name: XZC001R-ADD-CNC-DAY<br>*/
	public int getXzc001rAddCncDay() {
		return readNumDispUnsignedInt(Pos.XZC001R_ADD_CNC_DAY, Len.XZC001R_ADD_CNC_DAY);
	}

	public String getXzc001rAddCncDayFormatted() {
		return readFixedString(Pos.XZC001R_ADD_CNC_DAY, Len.XZC001R_ADD_CNC_DAY);
	}

	public void setXzc001rReaDesCi(char xzc001rReaDesCi) {
		writeChar(Pos.XZC001R_REA_DES_CI, xzc001rReaDesCi);
	}

	/**Original name: XZC001R-REA-DES-CI<br>*/
	public char getXzc001rReaDesCi() {
		return readChar(Pos.XZC001R_REA_DES_CI);
	}

	public void setXzc001rReaDesNi(char xzc001rReaDesNi) {
		writeChar(Pos.XZC001R_REA_DES_NI, xzc001rReaDesNi);
	}

	/**Original name: XZC001R-REA-DES-NI<br>*/
	public char getXzc001rReaDesNi() {
		return readChar(Pos.XZC001R_REA_DES_NI);
	}

	public void setXzc001rReaDes(String xzc001rReaDes) {
		writeString(Pos.XZC001R_REA_DES, xzc001rReaDes, Len.XZC001R_REA_DES);
	}

	/**Original name: XZC001R-REA-DES<br>*/
	public String getXzc001rReaDes() {
		return readString(Pos.XZC001R_REA_DES, Len.XZC001R_REA_DES);
	}

	public void setXzc001rActNotTypDesc(String xzc001rActNotTypDesc) {
		writeString(Pos.XZC001R_ACT_NOT_TYP_DESC, xzc001rActNotTypDesc, Len.XZC001R_ACT_NOT_TYP_DESC);
	}

	/**Original name: XZC001R-ACT-NOT-TYP-DESC<br>*/
	public String getXzc001rActNotTypDesc() {
		return readString(Pos.XZC001R_ACT_NOT_TYP_DESC, Len.XZC001R_ACT_NOT_TYP_DESC);
	}

	public void setXzy810InputOutputParmsFormatted(String data) {
		writeString(Pos.XZY810_INPUT_OUTPUT_PARMS, data, Len.XZY810_INPUT_OUTPUT_PARMS);
	}

	public String getXzy810InputOutputParmsFormatted() {
		return readFixedString(Pos.XZY810_INPUT_OUTPUT_PARMS, Len.XZY810_INPUT_OUTPUT_PARMS);
	}

	public void setXzy810TkNotPrcTs(String xzy810TkNotPrcTs) {
		writeString(Pos.XZY810_TK_NOT_PRC_TS, xzy810TkNotPrcTs, Len.XZY810_TK_NOT_PRC_TS);
	}

	/**Original name: XZY810-TK-NOT-PRC-TS<br>*/
	public String getXzy810TkNotPrcTs() {
		return readString(Pos.XZY810_TK_NOT_PRC_TS, Len.XZY810_TK_NOT_PRC_TS);
	}

	public void setXzy810CsrActNbr(String xzy810CsrActNbr) {
		writeString(Pos.XZY810_CSR_ACT_NBR, xzy810CsrActNbr, Len.XZY810_CSR_ACT_NBR);
	}

	/**Original name: XZY810-CSR-ACT-NBR<br>*/
	public String getXzy810CsrActNbr() {
		return readString(Pos.XZY810_CSR_ACT_NBR, Len.XZY810_CSR_ACT_NBR);
	}

	public void setXzy810Userid(String xzy810Userid) {
		writeString(Pos.XZY810_USERID, xzy810Userid, Len.XZY810_USERID);
	}

	/**Original name: XZY810-USERID<br>*/
	public String getXzy810Userid() {
		return readString(Pos.XZY810_USERID, Len.XZY810_USERID);
	}

	public void setXzy810FrmAtcInd(char xzy810FrmAtcInd) {
		writeChar(Pos.XZY810_FRM_ATC_IND, xzy810FrmAtcInd);
	}

	/**Original name: XZY810-FRM-ATC-IND<br>*/
	public char getXzy810FrmAtcInd() {
		return readChar(Pos.XZY810_FRM_ATC_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0C0001 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZC001R_ACT_NOT_ROW = L_FW_RESP_XZ0C0001;
		public static final int XZC001R_ACT_NOT_FIXED = XZC001R_ACT_NOT_ROW;
		public static final int XZC001R_ACT_NOT_CSUM = XZC001R_ACT_NOT_FIXED;
		public static final int XZC001R_CSR_ACT_NBR_KCRE = XZC001R_ACT_NOT_CSUM + Len.XZC001R_ACT_NOT_CSUM;
		public static final int XZC001R_NOT_PRC_TS_KCRE = XZC001R_CSR_ACT_NBR_KCRE + Len.XZC001R_CSR_ACT_NBR_KCRE;
		public static final int XZC001R_ACT_NOT_DATES = XZC001R_NOT_PRC_TS_KCRE + Len.XZC001R_NOT_PRC_TS_KCRE;
		public static final int XZC001R_TRANS_PROCESS_DT = XZC001R_ACT_NOT_DATES;
		public static final int XZC001R_ACT_NOT_KEY = XZC001R_TRANS_PROCESS_DT + Len.XZC001R_TRANS_PROCESS_DT;
		public static final int XZC001R_CSR_ACT_NBR = XZC001R_ACT_NOT_KEY;
		public static final int XZC001R_NOT_PRC_TS = XZC001R_CSR_ACT_NBR + Len.XZC001R_CSR_ACT_NBR;
		public static final int XZC001R_ACT_NOT_KEY_CI = XZC001R_NOT_PRC_TS + Len.XZC001R_NOT_PRC_TS;
		public static final int XZC001R_CSR_ACT_NBR_CI = XZC001R_ACT_NOT_KEY_CI;
		public static final int XZC001R_NOT_PRC_TS_CI = XZC001R_CSR_ACT_NBR_CI + Len.XZC001R_CSR_ACT_NBR_CI;
		public static final int XZC001R_ACT_NOT_DATA = XZC001R_NOT_PRC_TS_CI + Len.XZC001R_NOT_PRC_TS_CI;
		public static final int XZC001R_ACT_NOT_TYP_CD_CI = XZC001R_ACT_NOT_DATA;
		public static final int XZC001R_ACT_NOT_TYP_CD = XZC001R_ACT_NOT_TYP_CD_CI + Len.XZC001R_ACT_NOT_TYP_CD_CI;
		public static final int XZC001R_NOT_DT_CI = XZC001R_ACT_NOT_TYP_CD + Len.XZC001R_ACT_NOT_TYP_CD;
		public static final int XZC001R_NOT_DT = XZC001R_NOT_DT_CI + Len.XZC001R_NOT_DT_CI;
		public static final int XZC001R_ACT_OWN_CLT_ID_CI = XZC001R_NOT_DT + Len.XZC001R_NOT_DT;
		public static final int XZC001R_ACT_OWN_CLT_ID = XZC001R_ACT_OWN_CLT_ID_CI + Len.XZC001R_ACT_OWN_CLT_ID_CI;
		public static final int XZC001R_ACT_OWN_ADR_ID_CI = XZC001R_ACT_OWN_CLT_ID + Len.XZC001R_ACT_OWN_CLT_ID;
		public static final int XZC001R_ACT_OWN_ADR_ID = XZC001R_ACT_OWN_ADR_ID_CI + Len.XZC001R_ACT_OWN_ADR_ID_CI;
		public static final int XZC001R_EMP_ID_CI = XZC001R_ACT_OWN_ADR_ID + Len.XZC001R_ACT_OWN_ADR_ID;
		public static final int XZC001R_EMP_ID_NI = XZC001R_EMP_ID_CI + Len.XZC001R_EMP_ID_CI;
		public static final int XZC001R_EMP_ID = XZC001R_EMP_ID_NI + Len.XZC001R_EMP_ID_NI;
		public static final int XZC001R_STA_MDF_TS_CI = XZC001R_EMP_ID + Len.XZC001R_EMP_ID;
		public static final int XZC001R_STA_MDF_TS = XZC001R_STA_MDF_TS_CI + Len.XZC001R_STA_MDF_TS_CI;
		public static final int XZC001R_ACT_NOT_STA_CD_CI = XZC001R_STA_MDF_TS + Len.XZC001R_STA_MDF_TS;
		public static final int XZC001R_ACT_NOT_STA_CD = XZC001R_ACT_NOT_STA_CD_CI + Len.XZC001R_ACT_NOT_STA_CD_CI;
		public static final int XZC001R_PDC_NBR_CI = XZC001R_ACT_NOT_STA_CD + Len.XZC001R_ACT_NOT_STA_CD;
		public static final int XZC001R_PDC_NBR_NI = XZC001R_PDC_NBR_CI + Len.XZC001R_PDC_NBR_CI;
		public static final int XZC001R_PDC_NBR = XZC001R_PDC_NBR_NI + Len.XZC001R_PDC_NBR_NI;
		public static final int XZC001R_PDC_NM_CI = XZC001R_PDC_NBR + Len.XZC001R_PDC_NBR;
		public static final int XZC001R_PDC_NM_NI = XZC001R_PDC_NM_CI + Len.XZC001R_PDC_NM_CI;
		public static final int XZC001R_PDC_NM = XZC001R_PDC_NM_NI + Len.XZC001R_PDC_NM_NI;
		public static final int XZC001R_SEG_CD_CI = XZC001R_PDC_NM + Len.XZC001R_PDC_NM;
		public static final int XZC001R_SEG_CD_NI = XZC001R_SEG_CD_CI + Len.XZC001R_SEG_CD_CI;
		public static final int XZC001R_SEG_CD = XZC001R_SEG_CD_NI + Len.XZC001R_SEG_CD_NI;
		public static final int XZC001R_ACT_TYP_CD_CI = XZC001R_SEG_CD + Len.XZC001R_SEG_CD;
		public static final int XZC001R_ACT_TYP_CD_NI = XZC001R_ACT_TYP_CD_CI + Len.XZC001R_ACT_TYP_CD_CI;
		public static final int XZC001R_ACT_TYP_CD = XZC001R_ACT_TYP_CD_NI + Len.XZC001R_ACT_TYP_CD_NI;
		public static final int XZC001R_TOT_FEE_AMT_CI = XZC001R_ACT_TYP_CD + Len.XZC001R_ACT_TYP_CD;
		public static final int XZC001R_TOT_FEE_AMT_NI = XZC001R_TOT_FEE_AMT_CI + Len.XZC001R_TOT_FEE_AMT_CI;
		public static final int XZC001R_TOT_FEE_AMT_SIGN = XZC001R_TOT_FEE_AMT_NI + Len.XZC001R_TOT_FEE_AMT_NI;
		public static final int XZC001R_TOT_FEE_AMT = XZC001R_TOT_FEE_AMT_SIGN + Len.XZC001R_TOT_FEE_AMT_SIGN;
		public static final int XZC001R_ST_ABB_CI = XZC001R_TOT_FEE_AMT + Len.XZC001R_TOT_FEE_AMT;
		public static final int XZC001R_ST_ABB_NI = XZC001R_ST_ABB_CI + Len.XZC001R_ST_ABB_CI;
		public static final int XZC001R_ST_ABB = XZC001R_ST_ABB_NI + Len.XZC001R_ST_ABB_NI;
		public static final int XZC001R_CER_HLD_NOT_IND_CI = XZC001R_ST_ABB + Len.XZC001R_ST_ABB;
		public static final int XZC001R_CER_HLD_NOT_IND_NI = XZC001R_CER_HLD_NOT_IND_CI + Len.XZC001R_CER_HLD_NOT_IND_CI;
		public static final int XZC001R_CER_HLD_NOT_IND = XZC001R_CER_HLD_NOT_IND_NI + Len.XZC001R_CER_HLD_NOT_IND_NI;
		public static final int XZC001R_ADD_CNC_DAY_CI = XZC001R_CER_HLD_NOT_IND + Len.XZC001R_CER_HLD_NOT_IND;
		public static final int XZC001R_ADD_CNC_DAY_NI = XZC001R_ADD_CNC_DAY_CI + Len.XZC001R_ADD_CNC_DAY_CI;
		public static final int XZC001R_ADD_CNC_DAY_SIGN = XZC001R_ADD_CNC_DAY_NI + Len.XZC001R_ADD_CNC_DAY_NI;
		public static final int XZC001R_ADD_CNC_DAY = XZC001R_ADD_CNC_DAY_SIGN + Len.XZC001R_ADD_CNC_DAY_SIGN;
		public static final int XZC001R_REA_DES_CI = XZC001R_ADD_CNC_DAY + Len.XZC001R_ADD_CNC_DAY;
		public static final int XZC001R_REA_DES_NI = XZC001R_REA_DES_CI + Len.XZC001R_REA_DES_CI;
		public static final int XZC001R_REA_DES = XZC001R_REA_DES_NI + Len.XZC001R_REA_DES_NI;
		public static final int XZC001R_EXTENSION_FIELDS = XZC001R_REA_DES + Len.XZC001R_REA_DES;
		public static final int XZC001R_ACT_NOT_TYP_DESC = XZC001R_EXTENSION_FIELDS;
		public static final int XZY810_INPUT_OUTPUT_PARMS = XZC001R_ACT_NOT_TYP_DESC + Len.XZC001R_ACT_NOT_TYP_DESC;
		public static final int XZY810_INPUTS = XZY810_INPUT_OUTPUT_PARMS;
		public static final int XZY810_TECHNICAL_KEY = XZY810_INPUTS;
		public static final int XZY810_TK_NOT_PRC_TS = XZY810_TECHNICAL_KEY;
		public static final int XZY810_CSR_ACT_NBR = XZY810_TK_NOT_PRC_TS + Len.XZY810_TK_NOT_PRC_TS;
		public static final int XZY810_USERID = XZY810_CSR_ACT_NBR + Len.XZY810_CSR_ACT_NBR;
		public static final int FLR1 = XZY810_USERID + Len.XZY810_USERID;
		public static final int XZY810_OUTPUTS = FLR1 + Len.FLR1;
		public static final int XZY810_FRM_ATC_IND = XZY810_OUTPUTS;
		public static final int FLR2 = XZY810_FRM_ATC_IND + Len.XZY810_FRM_ATC_IND;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC001R_ACT_NOT_CSUM = 9;
		public static final int XZC001R_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC001R_NOT_PRC_TS_KCRE = 32;
		public static final int XZC001R_TRANS_PROCESS_DT = 10;
		public static final int XZC001R_CSR_ACT_NBR = 9;
		public static final int XZC001R_NOT_PRC_TS = 26;
		public static final int XZC001R_CSR_ACT_NBR_CI = 1;
		public static final int XZC001R_NOT_PRC_TS_CI = 1;
		public static final int XZC001R_ACT_NOT_TYP_CD_CI = 1;
		public static final int XZC001R_ACT_NOT_TYP_CD = 5;
		public static final int XZC001R_NOT_DT_CI = 1;
		public static final int XZC001R_NOT_DT = 10;
		public static final int XZC001R_ACT_OWN_CLT_ID_CI = 1;
		public static final int XZC001R_ACT_OWN_CLT_ID = 64;
		public static final int XZC001R_ACT_OWN_ADR_ID_CI = 1;
		public static final int XZC001R_ACT_OWN_ADR_ID = 64;
		public static final int XZC001R_EMP_ID_CI = 1;
		public static final int XZC001R_EMP_ID_NI = 1;
		public static final int XZC001R_EMP_ID = 6;
		public static final int XZC001R_STA_MDF_TS_CI = 1;
		public static final int XZC001R_STA_MDF_TS = 26;
		public static final int XZC001R_ACT_NOT_STA_CD_CI = 1;
		public static final int XZC001R_ACT_NOT_STA_CD = 2;
		public static final int XZC001R_PDC_NBR_CI = 1;
		public static final int XZC001R_PDC_NBR_NI = 1;
		public static final int XZC001R_PDC_NBR = 5;
		public static final int XZC001R_PDC_NM_CI = 1;
		public static final int XZC001R_PDC_NM_NI = 1;
		public static final int XZC001R_PDC_NM = 120;
		public static final int XZC001R_SEG_CD_CI = 1;
		public static final int XZC001R_SEG_CD_NI = 1;
		public static final int XZC001R_SEG_CD = 3;
		public static final int XZC001R_ACT_TYP_CD_CI = 1;
		public static final int XZC001R_ACT_TYP_CD_NI = 1;
		public static final int XZC001R_ACT_TYP_CD = 2;
		public static final int XZC001R_TOT_FEE_AMT_CI = 1;
		public static final int XZC001R_TOT_FEE_AMT_NI = 1;
		public static final int XZC001R_TOT_FEE_AMT_SIGN = 1;
		public static final int XZC001R_TOT_FEE_AMT = 10;
		public static final int XZC001R_ST_ABB_CI = 1;
		public static final int XZC001R_ST_ABB_NI = 1;
		public static final int XZC001R_ST_ABB = 2;
		public static final int XZC001R_CER_HLD_NOT_IND_CI = 1;
		public static final int XZC001R_CER_HLD_NOT_IND_NI = 1;
		public static final int XZC001R_CER_HLD_NOT_IND = 1;
		public static final int XZC001R_ADD_CNC_DAY_CI = 1;
		public static final int XZC001R_ADD_CNC_DAY_NI = 1;
		public static final int XZC001R_ADD_CNC_DAY_SIGN = 1;
		public static final int XZC001R_ADD_CNC_DAY = 5;
		public static final int XZC001R_REA_DES_CI = 1;
		public static final int XZC001R_REA_DES_NI = 1;
		public static final int XZC001R_REA_DES = 500;
		public static final int XZC001R_ACT_NOT_TYP_DESC = 35;
		public static final int XZY810_TK_NOT_PRC_TS = 26;
		public static final int XZY810_CSR_ACT_NBR = 9;
		public static final int XZY810_USERID = 8;
		public static final int FLR1 = 100;
		public static final int XZY810_FRM_ATC_IND = 1;
		public static final int XZC001R_ACT_NOT_FIXED = XZC001R_ACT_NOT_CSUM + XZC001R_CSR_ACT_NBR_KCRE + XZC001R_NOT_PRC_TS_KCRE;
		public static final int XZC001R_ACT_NOT_DATES = XZC001R_TRANS_PROCESS_DT;
		public static final int XZC001R_ACT_NOT_KEY = XZC001R_CSR_ACT_NBR + XZC001R_NOT_PRC_TS;
		public static final int XZC001R_ACT_NOT_KEY_CI = XZC001R_CSR_ACT_NBR_CI + XZC001R_NOT_PRC_TS_CI;
		public static final int XZC001R_ACT_NOT_DATA = XZC001R_ACT_NOT_TYP_CD_CI + XZC001R_ACT_NOT_TYP_CD + XZC001R_NOT_DT_CI + XZC001R_NOT_DT
				+ XZC001R_ACT_OWN_CLT_ID_CI + XZC001R_ACT_OWN_CLT_ID + XZC001R_ACT_OWN_ADR_ID_CI + XZC001R_ACT_OWN_ADR_ID + XZC001R_EMP_ID_CI
				+ XZC001R_EMP_ID_NI + XZC001R_EMP_ID + XZC001R_STA_MDF_TS_CI + XZC001R_STA_MDF_TS + XZC001R_ACT_NOT_STA_CD_CI + XZC001R_ACT_NOT_STA_CD
				+ XZC001R_PDC_NBR_CI + XZC001R_PDC_NBR_NI + XZC001R_PDC_NBR + XZC001R_PDC_NM_CI + XZC001R_PDC_NM_NI + XZC001R_PDC_NM
				+ XZC001R_SEG_CD_CI + XZC001R_SEG_CD_NI + XZC001R_SEG_CD + XZC001R_ACT_TYP_CD_CI + XZC001R_ACT_TYP_CD_NI + XZC001R_ACT_TYP_CD
				+ XZC001R_TOT_FEE_AMT_CI + XZC001R_TOT_FEE_AMT_NI + XZC001R_TOT_FEE_AMT_SIGN + XZC001R_TOT_FEE_AMT + XZC001R_ST_ABB_CI
				+ XZC001R_ST_ABB_NI + XZC001R_ST_ABB + XZC001R_CER_HLD_NOT_IND_CI + XZC001R_CER_HLD_NOT_IND_NI + XZC001R_CER_HLD_NOT_IND
				+ XZC001R_ADD_CNC_DAY_CI + XZC001R_ADD_CNC_DAY_NI + XZC001R_ADD_CNC_DAY_SIGN + XZC001R_ADD_CNC_DAY + XZC001R_REA_DES_CI
				+ XZC001R_REA_DES_NI + XZC001R_REA_DES;
		public static final int XZC001R_EXTENSION_FIELDS = XZC001R_ACT_NOT_TYP_DESC;
		public static final int XZC001R_ACT_NOT_ROW = XZC001R_ACT_NOT_FIXED + XZC001R_ACT_NOT_DATES + XZC001R_ACT_NOT_KEY + XZC001R_ACT_NOT_KEY_CI
				+ XZC001R_ACT_NOT_DATA + XZC001R_EXTENSION_FIELDS;
		public static final int L_FW_RESP_XZ0C0001 = XZC001R_ACT_NOT_ROW;
		public static final int XZY810_TECHNICAL_KEY = XZY810_TK_NOT_PRC_TS;
		public static final int XZY810_INPUTS = XZY810_TECHNICAL_KEY + XZY810_CSR_ACT_NBR + XZY810_USERID + FLR1;
		public static final int FLR2 = 400;
		public static final int XZY810_OUTPUTS = XZY810_FRM_ATC_IND + FLR2;
		public static final int XZY810_INPUT_OUTPUT_PARMS = XZY810_INPUTS + XZY810_OUTPUTS;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0C0001 + XZY810_INPUT_OUTPUT_PARMS;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZC001R_TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZC001R_TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
