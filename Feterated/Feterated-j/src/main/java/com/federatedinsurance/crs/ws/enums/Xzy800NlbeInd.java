/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: XZY800-NLBE-IND<br>
 * Variable: XZY800-NLBE-IND from copybook XZ0Y8000<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzy800NlbeInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NLBE_OCC = 'Y';
	public static final char NO_NLBE_OCC = 'N';

	//==== METHODS ====
	public void setNlbeInd(char nlbeInd) {
		this.value = nlbeInd;
	}

	public char getNlbeInd() {
		return this.value;
	}

	public boolean isNlbeOcc() {
		return value == NLBE_OCC;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NLBE_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
