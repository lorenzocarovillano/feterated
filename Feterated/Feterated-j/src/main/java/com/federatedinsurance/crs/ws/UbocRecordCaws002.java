/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: UBOC-RECORD<br>
 * Variable: UBOC-RECORD from copybook HALLUBOC<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class UbocRecordCaws002 extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: UBOC-COMM-INFO
	private UbocCommInfoCaws002 commInfo = new UbocCommInfoCaws002();
	//Original name: UBOC-APP-DATA-BUFFER-LENGTH
	private String appDataBufferLength = DefaultValues.stringVal(Len.APP_DATA_BUFFER_LENGTH);
	//Original name: UBOC-APP-DATA-BUFFER
	private String appDataBuffer = DefaultValues.stringVal(Len.APP_DATA_BUFFER);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.UBOC_RECORD;
	}

	@Override
	public void deserialize(byte[] buf) {
		setUbocRecordBytes(buf);
	}

	public void setUbocRecordBytes(byte[] buffer) {
		setUbocRecordBytes(buffer, 1);
	}

	public byte[] getUbocRecordBytes() {
		byte[] buffer = new byte[Len.UBOC_RECORD];
		return getUbocRecordBytes(buffer, 1);
	}

	public void setUbocRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		commInfo.setCommInfoBytes(buffer, position);
		position += UbocCommInfoCaws002.Len.COMM_INFO;
		setExtraDataBytes(buffer, position);
	}

	public byte[] getUbocRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		commInfo.getCommInfoBytes(buffer, position);
		position += UbocCommInfoCaws002.Len.COMM_INFO;
		getExtraDataBytes(buffer, position);
		return buffer;
	}

	public void setExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		appDataBufferLength = MarshalByte.readFixedString(buffer, position, Len.APP_DATA_BUFFER_LENGTH);
		position += Len.APP_DATA_BUFFER_LENGTH;
		appDataBuffer = MarshalByte.readString(buffer, position, Len.APP_DATA_BUFFER);
	}

	public byte[] getExtraDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, appDataBufferLength, Len.APP_DATA_BUFFER_LENGTH);
		position += Len.APP_DATA_BUFFER_LENGTH;
		MarshalByte.writeString(buffer, position, appDataBuffer, Len.APP_DATA_BUFFER);
		return buffer;
	}

	public void setAppDataBufferLength(short appDataBufferLength) {
		this.appDataBufferLength = NumericDisplay.asString(appDataBufferLength, Len.APP_DATA_BUFFER_LENGTH);
	}

	public short getAppDataBufferLength() {
		return NumericDisplay.asShort(this.appDataBufferLength);
	}

	public void setAppDataBuffer(String appDataBuffer) {
		this.appDataBuffer = Functions.subString(appDataBuffer, Len.APP_DATA_BUFFER);
	}

	public String getAppDataBuffer() {
		return this.appDataBuffer;
	}

	public String getAppDataBufferFormatted() {
		return Functions.padBlanks(getAppDataBuffer(), Len.APP_DATA_BUFFER);
	}

	public UbocCommInfoCaws002 getCommInfo() {
		return commInfo;
	}

	@Override
	public byte[] serialize() {
		return getUbocRecordBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int APP_DATA_BUFFER_LENGTH = 4;
		public static final int APP_DATA_BUFFER = 5000;
		public static final int EXTRA_DATA = APP_DATA_BUFFER_LENGTH + APP_DATA_BUFFER;
		public static final int UBOC_RECORD = UbocCommInfoCaws002.Len.COMM_INFO + EXTRA_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
