/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-09-CONTACT-CLS-MSG<br>
 * Variable: EA-09-CONTACT-CLS-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea09ContactClsMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG
	private String flr1 = "";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-1
	private String flr2 = "- PLEASE";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-2
	private String flr3 = "CONTACT A COBOL";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-3
	private String flr4 = " LANGUAGE";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-4
	private String flr5 = "SPECIALIST TO";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-5
	private String flr6 = "ADDRESS THIS";
	//Original name: FILLER-EA-09-CONTACT-CLS-MSG-6
	private String flr7 = "ISSUE";

	//==== METHODS ====
	public String getEa09ContactClsMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa09ContactClsMsgBytes());
	}

	public byte[] getEa09ContactClsMsgBytes() {
		byte[] buffer = new byte[Len.EA09_CONTACT_CLS_MSG];
		return getEa09ContactClsMsgBytes(buffer, 1);
	}

	public byte[] getEa09ContactClsMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public String getFlr7() {
		return this.flr7;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 10;
		public static final int FLR2 = 9;
		public static final int FLR3 = 15;
		public static final int FLR5 = 14;
		public static final int FLR6 = 13;
		public static final int FLR7 = 5;
		public static final int EA09_CONTACT_CLS_MSG = 2 * FLR1 + FLR2 + FLR3 + FLR5 + FLR6 + FLR7;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
