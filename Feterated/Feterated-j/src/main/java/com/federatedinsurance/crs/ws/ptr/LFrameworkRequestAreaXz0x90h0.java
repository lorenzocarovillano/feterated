/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X90H0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x90h0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_REQ_XZ0Y90H1_MAXOCCURS = 50;

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x90h0() {
	}

	public LFrameworkRequestAreaXz0x90h0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzy9h0qMaxPolRows(short xzy9h0qMaxPolRows) {
		writeBinaryShort(Pos.XZY9H0Q_MAX_POL_ROWS, xzy9h0qMaxPolRows);
	}

	/**Original name: XZY9H0Q-MAX-POL-ROWS<br>*/
	public short getXzy9h0qMaxPolRows() {
		return readBinaryShort(Pos.XZY9H0Q_MAX_POL_ROWS);
	}

	public void setXzy9h0qCsrActNbr(String xzy9h0qCsrActNbr) {
		writeString(Pos.XZY9H0Q_CSR_ACT_NBR, xzy9h0qCsrActNbr, Len.XZY9H0Q_CSR_ACT_NBR);
	}

	/**Original name: XZY9H0Q-CSR-ACT-NBR<br>*/
	public String getXzy9h0qCsrActNbr() {
		return readString(Pos.XZY9H0Q_CSR_ACT_NBR, Len.XZY9H0Q_CSR_ACT_NBR);
	}

	public void setXzy9h0qNotPrcTs(String xzy9h0qNotPrcTs) {
		writeString(Pos.XZY9H0Q_NOT_PRC_TS, xzy9h0qNotPrcTs, Len.XZY9H0Q_NOT_PRC_TS);
	}

	/**Original name: XZY9H0Q-NOT-PRC-TS<br>*/
	public String getXzy9h0qNotPrcTs() {
		return readString(Pos.XZY9H0Q_NOT_PRC_TS, Len.XZY9H0Q_NOT_PRC_TS);
	}

	public void setXzy9h0qUserid(String xzy9h0qUserid) {
		writeString(Pos.XZY9H0Q_USERID, xzy9h0qUserid, Len.XZY9H0Q_USERID);
	}

	/**Original name: XZY9H0Q-USERID<br>*/
	public String getXzy9h0qUserid() {
		return readString(Pos.XZY9H0Q_USERID, Len.XZY9H0Q_USERID);
	}

	public void setXzy9h1qPolNbr(int xzy9h1qPolNbrIdx, String xzy9h1qPolNbr) {
		int position = Pos.xzy9h1qPolNbr(xzy9h1qPolNbrIdx - 1);
		writeString(position, xzy9h1qPolNbr, Len.XZY9H1Q_POL_NBR);
	}

	/**Original name: XZY9H1Q-POL-NBR<br>*/
	public String getXzy9h1qPolNbr(int xzy9h1qPolNbrIdx) {
		int position = Pos.xzy9h1qPolNbr(xzy9h1qPolNbrIdx - 1);
		return readString(position, Len.XZY9H1Q_POL_NBR);
	}

	public void setXzy9h1qPolEffDt(int xzy9h1qPolEffDtIdx, String xzy9h1qPolEffDt) {
		int position = Pos.xzy9h1qPolEffDt(xzy9h1qPolEffDtIdx - 1);
		writeString(position, xzy9h1qPolEffDt, Len.XZY9H1Q_POL_EFF_DT);
	}

	/**Original name: XZY9H1Q-POL-EFF-DT<br>*/
	public String getXzy9h1qPolEffDt(int xzy9h1qPolEffDtIdx) {
		int position = Pos.xzy9h1qPolEffDt(xzy9h1qPolEffDtIdx - 1);
		return readString(position, Len.XZY9H1Q_POL_EFF_DT);
	}

	public void setXzy9h1qPolExpDt(int xzy9h1qPolExpDtIdx, String xzy9h1qPolExpDt) {
		int position = Pos.xzy9h1qPolExpDt(xzy9h1qPolExpDtIdx - 1);
		writeString(position, xzy9h1qPolExpDt, Len.XZY9H1Q_POL_EXP_DT);
	}

	/**Original name: XZY9H1Q-POL-EXP-DT<br>*/
	public String getXzy9h1qPolExpDt(int xzy9h1qPolExpDtIdx) {
		int position = Pos.xzy9h1qPolExpDt(xzy9h1qPolExpDtIdx - 1);
		return readString(position, Len.XZY9H1Q_POL_EXP_DT);
	}

	public void setXzy9h1qNotEffDt(int xzy9h1qNotEffDtIdx, String xzy9h1qNotEffDt) {
		int position = Pos.xzy9h1qNotEffDt(xzy9h1qNotEffDtIdx - 1);
		writeString(position, xzy9h1qNotEffDt, Len.XZY9H1Q_NOT_EFF_DT);
	}

	/**Original name: XZY9H1Q-NOT-EFF-DT<br>*/
	public String getXzy9h1qNotEffDt(int xzy9h1qNotEffDtIdx) {
		int position = Pos.xzy9h1qNotEffDt(xzy9h1qNotEffDtIdx - 1);
		return readString(position, Len.XZY9H1Q_NOT_EFF_DT);
	}

	public void setXzy9h1qPolDueAmt(int xzy9h1qPolDueAmtIdx, AfDecimal xzy9h1qPolDueAmt) {
		int position = Pos.xzy9h1qPolDueAmt(xzy9h1qPolDueAmtIdx - 1);
		writeDecimal(position, xzy9h1qPolDueAmt.copy());
	}

	/**Original name: XZY9H1Q-POL-DUE-AMT<br>*/
	public AfDecimal getXzy9h1qPolDueAmt(int xzy9h1qPolDueAmtIdx) {
		int position = Pos.xzy9h1qPolDueAmt(xzy9h1qPolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.XZY9H1Q_POL_DUE_AMT, Len.Fract.XZY9H1Q_POL_DUE_AMT);
	}

	public void setXzy9h1qPolBilStaCd(int xzy9h1qPolBilStaCdIdx, char xzy9h1qPolBilStaCd) {
		int position = Pos.xzy9h1qPolBilStaCd(xzy9h1qPolBilStaCdIdx - 1);
		writeChar(position, xzy9h1qPolBilStaCd);
	}

	/**Original name: XZY9H1Q-POL-BIL-STA-CD<br>*/
	public char getXzy9h1qPolBilStaCd(int xzy9h1qPolBilStaCdIdx) {
		int position = Pos.xzy9h1qPolBilStaCd(xzy9h1qPolBilStaCdIdx - 1);
		return readChar(position);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0Y90H0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZY9H0Q_PREPARE_INS_POL_ROW = L_FW_REQ_XZ0Y90H0;
		public static final int XZY9H0Q_MAX_POL_ROWS = XZY9H0Q_PREPARE_INS_POL_ROW;
		public static final int XZY9H0Q_CSR_ACT_NBR = XZY9H0Q_MAX_POL_ROWS + Len.XZY9H0Q_MAX_POL_ROWS;
		public static final int XZY9H0Q_NOT_PRC_TS = XZY9H0Q_CSR_ACT_NBR + Len.XZY9H0Q_CSR_ACT_NBR;
		public static final int XZY9H0Q_USERID = XZY9H0Q_NOT_PRC_TS + Len.XZY9H0Q_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwReqXz0y90h1(int idx) {
			return XZY9H0Q_USERID + Len.XZY9H0Q_USERID + idx * Len.L_FW_REQ_XZ0Y90H1;
		}

		public static int xzy9h1qPreparePolicyRow(int idx) {
			return lFwReqXz0y90h1(idx);
		}

		public static int xzy9h1qPolNbr(int idx) {
			return xzy9h1qPreparePolicyRow(idx);
		}

		public static int xzy9h1qPolEffDt(int idx) {
			return xzy9h1qPolNbr(idx) + Len.XZY9H1Q_POL_NBR;
		}

		public static int xzy9h1qPolExpDt(int idx) {
			return xzy9h1qPolEffDt(idx) + Len.XZY9H1Q_POL_EFF_DT;
		}

		public static int xzy9h1qNotEffDt(int idx) {
			return xzy9h1qPolExpDt(idx) + Len.XZY9H1Q_POL_EXP_DT;
		}

		public static int xzy9h1qPolDueAmt(int idx) {
			return xzy9h1qNotEffDt(idx) + Len.XZY9H1Q_NOT_EFF_DT;
		}

		public static int xzy9h1qPolBilStaCd(int idx) {
			return xzy9h1qPolDueAmt(idx) + Len.XZY9H1Q_POL_DUE_AMT;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZY9H0Q_MAX_POL_ROWS = 2;
		public static final int XZY9H0Q_CSR_ACT_NBR = 9;
		public static final int XZY9H0Q_NOT_PRC_TS = 26;
		public static final int XZY9H0Q_USERID = 8;
		public static final int XZY9H1Q_POL_NBR = 25;
		public static final int XZY9H1Q_POL_EFF_DT = 10;
		public static final int XZY9H1Q_POL_EXP_DT = 10;
		public static final int XZY9H1Q_NOT_EFF_DT = 10;
		public static final int XZY9H1Q_POL_DUE_AMT = 10;
		public static final int XZY9H1Q_POL_BIL_STA_CD = 1;
		public static final int XZY9H1Q_PREPARE_POLICY_ROW = XZY9H1Q_POL_NBR + XZY9H1Q_POL_EFF_DT + XZY9H1Q_POL_EXP_DT + XZY9H1Q_NOT_EFF_DT
				+ XZY9H1Q_POL_DUE_AMT + XZY9H1Q_POL_BIL_STA_CD;
		public static final int L_FW_REQ_XZ0Y90H1 = XZY9H1Q_PREPARE_POLICY_ROW;
		public static final int XZY9H0Q_PREPARE_INS_POL_ROW = XZY9H0Q_MAX_POL_ROWS + XZY9H0Q_CSR_ACT_NBR + XZY9H0Q_NOT_PRC_TS + XZY9H0Q_USERID;
		public static final int L_FW_REQ_XZ0Y90H0 = XZY9H0Q_PREPARE_INS_POL_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0Y90H0
				+ LFrameworkRequestAreaXz0x90h0.L_FW_REQ_XZ0Y90H1_MAXOCCURS * L_FW_REQ_XZ0Y90H1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZY9H1Q_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZY9H1Q_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
