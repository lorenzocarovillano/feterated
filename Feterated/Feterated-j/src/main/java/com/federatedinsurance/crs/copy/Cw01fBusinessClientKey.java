/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CW01F-BUSINESS-CLIENT-KEY<br>
 * Variable: CW01F-BUSINESS-CLIENT-KEY from copybook CAWLF001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw01fBusinessClientKey {

	//==== PROPERTIES ====
	//Original name: CW01F-CLIENT-ID-CI
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW01F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-HISTORY-VLD-NBR-SIGN
	private char historyVldNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW01F-HISTORY-VLD-NBR
	private String historyVldNbr = DefaultValues.stringVal(Len.HISTORY_VLD_NBR);
	//Original name: CW01F-CIBC-BUS-SEQ-NBR-CI
	private char cibcBusSeqNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-BUS-SEQ-NBR-SIGN
	private char cibcBusSeqNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-BUS-SEQ-NBR
	private String cibcBusSeqNbr = DefaultValues.stringVal(Len.CIBC_BUS_SEQ_NBR);
	//Original name: CW01F-CIBC-EFF-DT-CI
	private char cibcEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW01F-CIBC-EFF-DT
	private String cibcEffDt = DefaultValues.stringVal(Len.CIBC_EFF_DT);

	//==== METHODS ====
	public void setBusinessClientKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbr = MarshalByte.readFixedString(buffer, position, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		cibcBusSeqNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcBusSeqNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcBusSeqNbr = MarshalByte.readFixedString(buffer, position, Len.CIBC_BUS_SEQ_NBR);
		position += Len.CIBC_BUS_SEQ_NBR;
		cibcEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		cibcEffDt = MarshalByte.readString(buffer, position, Len.CIBC_EFF_DT);
	}

	public byte[] getBusinessClientKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbr, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		MarshalByte.writeChar(buffer, position, cibcBusSeqNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, cibcBusSeqNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcBusSeqNbr, Len.CIBC_BUS_SEQ_NBR);
		position += Len.CIBC_BUS_SEQ_NBR;
		MarshalByte.writeChar(buffer, position, cibcEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, cibcEffDt, Len.CIBC_EFF_DT);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setHistoryVldNbrSign(char historyVldNbrSign) {
		this.historyVldNbrSign = historyVldNbrSign;
	}

	public char getHistoryVldNbrSign() {
		return this.historyVldNbrSign;
	}

	public void setCwcacfHistoryVldNbrFormatted(String cwcacfHistoryVldNbr) {
		this.historyVldNbr = Trunc.toUnsignedNumeric(cwcacfHistoryVldNbr, Len.HISTORY_VLD_NBR);
	}

	public int getCwcacfHistoryVldNbr() {
		return NumericDisplay.asInt(this.historyVldNbr);
	}

	public void setCibcBusSeqNbrCi(char cibcBusSeqNbrCi) {
		this.cibcBusSeqNbrCi = cibcBusSeqNbrCi;
	}

	public char getCibcBusSeqNbrCi() {
		return this.cibcBusSeqNbrCi;
	}

	public void setCibcBusSeqNbrSign(char cibcBusSeqNbrSign) {
		this.cibcBusSeqNbrSign = cibcBusSeqNbrSign;
	}

	public char getCibcBusSeqNbrSign() {
		return this.cibcBusSeqNbrSign;
	}

	public void setCwcacfAdrSeqNbrFormatted(String cwcacfAdrSeqNbr) {
		this.cibcBusSeqNbr = Trunc.toUnsignedNumeric(cwcacfAdrSeqNbr, Len.CIBC_BUS_SEQ_NBR);
	}

	public int getCwcacfAdrSeqNbr() {
		return NumericDisplay.asInt(this.cibcBusSeqNbr);
	}

	public void setCibcEffDtCi(char cibcEffDtCi) {
		this.cibcEffDtCi = cibcEffDtCi;
	}

	public char getCibcEffDtCi() {
		return this.cibcEffDtCi;
	}

	public void setCibcEffDt(String cibcEffDt) {
		this.cibcEffDt = Functions.subString(cibcEffDt, Len.CIBC_EFF_DT);
	}

	public String getCibcEffDt() {
		return this.cibcEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int HISTORY_VLD_NBR = 5;
		public static final int CIBC_BUS_SEQ_NBR = 5;
		public static final int CIBC_EFF_DT = 10;
		public static final int CLIENT_ID_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int HISTORY_VLD_NBR_SIGN = 1;
		public static final int CIBC_BUS_SEQ_NBR_CI = 1;
		public static final int CIBC_BUS_SEQ_NBR_SIGN = 1;
		public static final int CIBC_EFF_DT_CI = 1;
		public static final int BUSINESS_CLIENT_KEY = CLIENT_ID_CI + CLIENT_ID + HISTORY_VLD_NBR_CI + HISTORY_VLD_NBR_SIGN + HISTORY_VLD_NBR
				+ CIBC_BUS_SEQ_NBR_CI + CIBC_BUS_SEQ_NBR_SIGN + CIBC_BUS_SEQ_NBR + CIBC_EFF_DT_CI + CIBC_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
