/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9030<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9030 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9030() {
	}

	public LFrameworkRequestAreaXz0q9030(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setMaxActNotTypRows(short maxActNotTypRows) {
		writeBinaryShort(Pos.MAX_ACT_NOT_TYP_ROWS, maxActNotTypRows);
	}

	/**Original name: XZA930Q-MAX-ACT-NOT-TYP-ROWS<br>*/
	public short getMaxActNotTypRows() {
		return readBinaryShort(Pos.MAX_ACT_NOT_TYP_ROWS);
	}

	public void setActNotTypCd(String actNotTypCd) {
		writeString(Pos.ACT_NOT_TYP_CD, actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	/**Original name: XZA930Q-ACT-NOT-TYP-CD<br>*/
	public String getActNotTypCd() {
		return readString(Pos.ACT_NOT_TYP_CD, Len.ACT_NOT_TYP_CD);
	}

	public void setActNotDes(String actNotDes) {
		writeString(Pos.ACT_NOT_DES, actNotDes, Len.ACT_NOT_DES);
	}

	/**Original name: XZA930Q-ACT-NOT-DES<br>*/
	public String getActNotDes() {
		return readString(Pos.ACT_NOT_DES, Len.ACT_NOT_DES);
	}

	public void setDsyOrdNbr(int dsyOrdNbr) {
		writeInt(Pos.DSY_ORD_NBR, dsyOrdNbr, Len.Int.DSY_ORD_NBR);
	}

	/**Original name: XZA930Q-DSY-ORD-NBR<br>*/
	public int getDsyOrdNbr() {
		return readNumDispInt(Pos.DSY_ORD_NBR, Len.DSY_ORD_NBR);
	}

	public void setDocDes(String docDes) {
		writeString(Pos.DOC_DES, docDes, Len.DOC_DES);
	}

	/**Original name: XZA930Q-DOC-DES<br>*/
	public String getDocDes() {
		return readString(Pos.DOC_DES, Len.DOC_DES);
	}

	public void setActNotPthCd(String actNotPthCd) {
		writeString(Pos.ACT_NOT_PTH_CD, actNotPthCd, Len.ACT_NOT_PTH_CD);
	}

	/**Original name: XZA930Q-ACT-NOT-PTH-CD<br>*/
	public String getActNotPthCd() {
		return readString(Pos.ACT_NOT_PTH_CD, Len.ACT_NOT_PTH_CD);
	}

	public void setActNotPthDes(String actNotPthDes) {
		writeString(Pos.ACT_NOT_PTH_DES, actNotPthDes, Len.ACT_NOT_PTH_DES);
	}

	/**Original name: XZA930Q-ACT-NOT-PTH-DES<br>*/
	public String getActNotPthDes() {
		return readString(Pos.ACT_NOT_PTH_DES, Len.ACT_NOT_PTH_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int ACT_NOT_TYP_INFO = L_FRAMEWORK_REQUEST_AREA;
		public static final int MAX_ACT_NOT_TYP_ROWS = ACT_NOT_TYP_INFO;
		public static final int ACT_NOT_TYP_ROW = MAX_ACT_NOT_TYP_ROWS + Len.MAX_ACT_NOT_TYP_ROWS;
		public static final int ACT_NOT_TYP_CD = ACT_NOT_TYP_ROW;
		public static final int ACT_NOT_DES = ACT_NOT_TYP_CD + Len.ACT_NOT_TYP_CD;
		public static final int DSY_ORD_NBR = ACT_NOT_DES + Len.ACT_NOT_DES;
		public static final int DOC_DES = DSY_ORD_NBR + Len.DSY_ORD_NBR;
		public static final int ACT_NOT_PTH_CD = DOC_DES + Len.DOC_DES;
		public static final int ACT_NOT_PTH_DES = ACT_NOT_PTH_CD + Len.ACT_NOT_PTH_CD;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ACT_NOT_TYP_ROWS = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int ACT_NOT_DES = 35;
		public static final int DSY_ORD_NBR = 5;
		public static final int DOC_DES = 240;
		public static final int ACT_NOT_PTH_CD = 5;
		public static final int ACT_NOT_PTH_DES = 35;
		public static final int ACT_NOT_TYP_ROW = ACT_NOT_TYP_CD + ACT_NOT_DES + DSY_ORD_NBR + DOC_DES + ACT_NOT_PTH_CD + ACT_NOT_PTH_DES;
		public static final int ACT_NOT_TYP_INFO = MAX_ACT_NOT_TYP_ROWS + ACT_NOT_TYP_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = ACT_NOT_TYP_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DSY_ORD_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
