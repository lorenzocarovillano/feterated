/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.WsRescindInd;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B0004<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b0004 {

	//==== PROPERTIES ====
	//Original name: WS-NBR-OF-NOT-DAY
	private short nbrOfNotDay = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B0004";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM
	private String busObjNm = "GET_RESCIND_IND_BPO";
	//Original name: WS-POLICY-CANC-DT
	private String policyCancDt = DefaultValues.stringVal(Len.POLICY_CANC_DT);
	//Original name: WS-RESCIND-IND
	private WsRescindInd rescindInd = new WsRescindInd();

	//==== METHODS ====
	public void setNbrOfNotDay(short nbrOfNotDay) {
		this.nbrOfNotDay = nbrOfNotDay;
	}

	public short getNbrOfNotDay() {
		return this.nbrOfNotDay;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setPolicyCancDt(String policyCancDt) {
		this.policyCancDt = Functions.subString(policyCancDt, Len.POLICY_CANC_DT);
	}

	public String getPolicyCancDt() {
		return this.policyCancDt;
	}

	public WsRescindInd getRescindInd() {
		return rescindInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POLICY_CANC_DT = 10;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
