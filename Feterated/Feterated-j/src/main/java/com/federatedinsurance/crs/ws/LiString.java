/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: LI-STRING<br>
 * Variable: LI-STRING from program HALRLEN1<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LiString extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int LI_STRING_X_MAXOCCURS = 999;
	//Original name: LI-STRING-X
	private char[] liStringX = new char[LI_STRING_X_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public LiString() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.LI_STRING;
	}

	@Override
	public void deserialize(byte[] buf) {
		setLiStringBytes(buf);
	}

	public void init() {
		for (int liStringXIdx = 1; liStringXIdx <= LI_STRING_X_MAXOCCURS; liStringXIdx++) {
			setLiStringX(liStringXIdx, DefaultValues.CHAR_VAL);
		}
	}

	public void setLiStringBytes(byte[] buffer) {
		setLiStringBytes(buffer, 1);
	}

	public byte[] getLiStringBytes() {
		byte[] buffer = new byte[Len.LI_STRING];
		return getLiStringBytes(buffer, 1);
	}

	public void setLiStringBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= LI_STRING_X_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setLiStringX(idx, MarshalByte.readChar(buffer, position));
				position += Types.CHAR_SIZE;
			} else {
				setLiStringX(idx, Types.SPACE_CHAR);
				position += Types.CHAR_SIZE;
			}
		}
	}

	public byte[] getLiStringBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= LI_STRING_X_MAXOCCURS; idx++) {
			MarshalByte.writeChar(buffer, position, getLiStringX(idx));
			position += Types.CHAR_SIZE;
		}
		return buffer;
	}

	public void setLiStringX(int liStringXIdx, char liStringX) {
		this.liStringX[liStringXIdx - 1] = liStringX;
	}

	public char getLiStringX(int liStringXIdx) {
		return this.liStringX[liStringXIdx - 1];
	}

	@Override
	public byte[] serialize() {
		return getLiStringBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int LI_STRING_X = 1;
		public static final int LI_STRING = LiString.LI_STRING_X_MAXOCCURS * LI_STRING_X;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
