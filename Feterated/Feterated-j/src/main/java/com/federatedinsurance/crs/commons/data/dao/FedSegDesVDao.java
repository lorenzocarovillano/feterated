/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFedSegDesV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [FED_SEG_DES_V]
 * 
 */
public class FedSegDesVDao extends BaseSqlDao<IFedSegDesV> {

	public FedSegDesVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFedSegDesV> getToClass() {
		return IFedSegDesV.class;
	}

	public String selectByFedSegCd(String fedSegCd, String dft) {
		return buildQuery("selectByFedSegCd").bind("fedSegCd", fedSegCd).scalarResultString(dft);
	}
}
