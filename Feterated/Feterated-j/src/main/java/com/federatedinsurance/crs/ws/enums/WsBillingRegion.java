/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-BILLING-REGION<br>
 * Variable: WS-BILLING-REGION from program XZ0P90D0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsBillingRegion {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.BILLING_REGION);
	public static final String DEV = "DH";
	public static final String DEVM = "DM";
	public static final String BETA = "BB";
	public static final String BETAG = "BG";
	public static final String EDUC = "E5";
	public static final String PROD = "C5";

	//==== METHODS ====
	public String getBillingRegion() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BILLING_REGION = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
