/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: FILLER-EA-08-SUB-PGM-ERROR-7<br>
 * Variable: FILLER-EA-08-SUB-PGM-ERROR-7 from program FNC02090<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class FillerEa08SubPgmError7 {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FLR8);
	public static final String NO_CRED_FOUND = "NO CREDENTIALS FOUND";
	public static final String NO_URI_FOUND = "NO URI FOR PPDFLOOKUP";
	public static final String SOAP_FAULT = "SOAP FAULT ERROR";

	//==== METHODS ====
	public void setFlr8(String flr8) {
		this.value = Functions.subString(flr8, Len.FLR8);
	}

	public String getFlr8() {
		return this.value;
	}

	public void setNoCredFound() {
		value = NO_CRED_FOUND;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR8 = 21;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
