/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.ISqlCa;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: SQLCA<br>
 * Variable: SQLCA from program HALOUIDG<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class Sqlca extends BytesAllocatingClass implements ISqlCa {

	//==== PROPERTIES ====
	public static final int SQLERRD_MAXOCCURS = 6;
	public static final int ERD_SQL_GOOD = 0;
	public static final int ERD_SQL_NOT_FOUND = 100;
	public static final int ERD_SQL_NOT_FND_UPDDEL = 100;
	private static final int[] ERD_SQL_DATETIME_INVALID = new int[] { -180, -181 };
	public static final int ERD_SQL_INVALID_VAR = -302;
	public static final int ERD_SQL_NULL_VALUE = -305;
	private static final int[] ERD_SQL_UNOPEN_CSR = new int[] { -501, -507 };
	public static final int ERD_SQL_CSR_ALREADY_OPEN = -502;
	public static final int ERD_SQL_FOREIGN_KEY_INVALID = -530;
	public static final int ERD_SQL_DELETE_RESTRICT_EFF = -532;
	public static final int ERD_SQL_DUPLICATE_KEY = -803;
	public static final int ERD_SQL_PROG_NM_NOT_FND = -805;
	public static final int ERD_SQL_DUPLICATE_ROW_RETURN = -811;
	public static final int ERD_SQL_DIFFERENT_TIMESTAMP = -818;
	public static final int ERD_SQL_UNAVAIL_RESOURCE = -904;
	public static final int ERD_SQL_RLLBK_DEADLK_TMOUT = -911;
	public static final int ERD_SQL_DEADLK_TMOUT = -913;
	public static final int ERD_SQL_AUTHOR_FAILURE = -922;
	public static final int ERD_SQL_NO_DB_CONNECTION = -923;

	//==== CONSTRUCTORS ====
	public Sqlca() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.SQLCA;
	}

	@Override
	public void updateSqlcaid(String sqlCaid) {
		writeString(1, sqlCaid, 8);
	}

	@Override
	public void updateSqlcode(int sqlCode) {
		writeBinaryInt(13, sqlCode);
	}

	@Override
	public void updateSqlerrml(short sqlErrml) {
		writeBinaryShort(17, sqlErrml);
	}

	@Override
	public void updateSqlerrmc(String sqlErrmc) {
		writeString(19, sqlErrmc, 70);
	}

	@Override
	public void updateSqlerrp(String sqlErrp) {
		writeString(89, sqlErrp, 8);
	}

	@Override
	public void updateSqlerrd(int index, int value) {
		int position = 1 + 97 + Types.INT_SIZE * (index - 1);
		writeBinaryInt(position, value);
	}

	@Override
	public void updateSqlwarn(int index, char value) {
		int position = 1 + 121 + 1 * (index - 1);
		writeChar(position, value);
	}

	@Override
	public void updateSqlstate(String sqlState) {
		writeString(132, sqlState, 5);
	}

	/**Original name: SQLCODE<br>*/
	public int getSqlcode() {
		int position = 13;
		return readBinaryInt(position);
	}

	/**Original name: SQLERRMC<br>*/
	public String getSqlerrmc() {
		int position = 19;
		return readString(position, Len.SQLERRMC);
	}

	/**Original name: SQLERRD<br>*/
	public int getSqlerrd(int sqlerrdIdx) {
		int position = 97 + (sqlerrdIdx - 1) * 4;
		return readBinaryInt(position);
	}

	/**Original name: ERD-SQL-RED<br>*/
	public int getErdSqlRed() {
		int position = 13;
		return readBinaryInt(position);
	}

	public boolean isErdSqlGood() {
		return getErdSqlRed() == ERD_SQL_GOOD;
	}

	public boolean isErdSqlNotFound() {
		return getErdSqlRed() == ERD_SQL_NOT_FOUND;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SQLCAID = 8;
		public static final int SQLCABC = 4;
		public static final int SQLCODE = 4;
		public static final int SQLERRML = 2;
		public static final int SQLERRMC = 70;
		public static final int SQLERRM = SQLERRML + SQLERRMC;
		public static final int SQLERRP = 8;
		public static final int SQLERRD = 4;
		public static final int SQLWARN0 = 1;
		public static final int SQLWARN1 = 1;
		public static final int SQLWARN2 = 1;
		public static final int SQLWARN3 = 1;
		public static final int SQLWARN4 = 1;
		public static final int SQLWARN5 = 1;
		public static final int SQLWARN6 = 1;
		public static final int SQLWARN7 = 1;
		public static final int SQLWARN = SQLWARN0 + SQLWARN1 + SQLWARN2 + SQLWARN3 + SQLWARN4 + SQLWARN5 + SQLWARN6 + SQLWARN7;
		public static final int SQLWARN8 = 1;
		public static final int SQLWARN9 = 1;
		public static final int SQLWARNA = 1;
		public static final int SQLSTATE = 5;
		public static final int SQLEXT = SQLWARN8 + SQLWARN9 + SQLWARNA + SQLSTATE;
		public static final int SQLCA = SQLCAID + SQLCABC + SQLCODE + SQLERRM + SQLERRP + Sqlca.SQLERRD_MAXOCCURS * SQLERRD + SQLWARN + SQLEXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
