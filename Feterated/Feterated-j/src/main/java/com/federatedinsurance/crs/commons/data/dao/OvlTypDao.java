/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IOvlTyp;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [OVL_TYP]
 * 
 */
public class OvlTypDao extends BaseSqlDao<IOvlTyp> {

	public OvlTypDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IOvlTyp> getToClass() {
		return IOvlTyp.class;
	}

	public IOvlTyp selectRec(String ovlEdlFrmNm, char cfYes, IOvlTyp iOvlTyp) {
		return buildQuery("selectRec").bind("ovlEdlFrmNm", ovlEdlFrmNm).bind("cfYes", String.valueOf(cfYes)).singleResult(iOvlTyp);
	}
}
