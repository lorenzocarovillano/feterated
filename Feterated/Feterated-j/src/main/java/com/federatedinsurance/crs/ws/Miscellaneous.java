/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.redefines.WsLastName;

/**Original name: MISCELLANEOUS<br>
 * Variable: MISCELLANEOUS from program CIWBNSRB<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Miscellaneous {

	//==== PROPERTIES ====
	//Original name: FIRST-BYTE
	private char firstByte = 'Y';
	//Original name: WS-STRING-POINTER
	private short wsStringPointer = ((short) 1);
	//Original name: WS-AND-MID-POINTER
	private short wsAndMidPointer = ((short) 1);
	//Original name: HOLD-PARSE-IDX
	private short holdParseIdx = ((short) 0);
	//Original name: WS-HOLD-NAME-WORK1
	private String wsHoldNameWork1 = "";
	//Original name: WS-HOLD-NAME-WORK2
	private String wsHoldNameWork2 = "";
	//Original name: WS-HOLD-NAME-WORK3
	private String wsHoldNameWork3 = "";
	//Original name: WS-LONG-NAME
	private String wsLongName = "";
	//Original name: WS-SPACE-COUNTER
	private short wsSpaceCounter = ((short) 0);
	//Original name: WS-MID-NAME-COUNT
	private short wsMidNameCount = ((short) 0);
	//Original name: WS-ERROR-COUNT
	private short wsErrorCount = ((short) 0);
	//Original name: WS-ERROR-CODE
	private String wsErrorCode = "";
	//Original name: DEBUG-PARAGRAPH
	private String debugParagraph = "";
	//Original name: WS-BYTE-TEST
	private char wsByteTest = Types.SPACE_CHAR;
	public static final char GOOD_CHAR_1_MIN = 'A';
	public static final char GOOD_CHAR_1_MAX = 'Z';
	public static final char GOOD_CHAR_2_MIN = '0';
	public static final char GOOD_CHAR_2_MAX = '9';
	public static final char GOOD_CHAR_3 = '.';
	public static final char GOOD_CHAR_4 = ',';
	public static final char GOOD_CHAR_5 = '/';
	public static final char GOOD_CHAR_6 = '&';
	public static final char GOOD_CHAR_7 = '-';
	public static final char GOOD_CHAR_8 = ' ';
	public static final char GOOD_CHAR_9 = '#';
	public static final char GOOD_CHAR_10 = Types.QUOTE_CHAR;
	//Original name: WS-LAST-NAME
	private WsLastName wsLastName = new WsLastName();

	//==== METHODS ====
	public void setFirstByte(char firstByte) {
		this.firstByte = firstByte;
	}

	public void setFirstByteFormatted(String firstByte) {
		setFirstByte(Functions.charAt(firstByte, Types.CHAR_SIZE));
	}

	public char getFirstByte() {
		return this.firstByte;
	}

	public void setWsStringPointer(short wsStringPointer) {
		this.wsStringPointer = wsStringPointer;
	}

	public short getWsStringPointer() {
		return this.wsStringPointer;
	}

	public void setWsAndMidPointer(short wsAndMidPointer) {
		this.wsAndMidPointer = wsAndMidPointer;
	}

	public short getWsAndMidPointer() {
		return this.wsAndMidPointer;
	}

	public void setHoldParseIdx(short holdParseIdx) {
		this.holdParseIdx = holdParseIdx;
	}

	public short getHoldParseIdx() {
		return this.holdParseIdx;
	}

	public void setWsHoldNameWork1(String wsHoldNameWork1) {
		this.wsHoldNameWork1 = Functions.subString(wsHoldNameWork1, Len.WS_HOLD_NAME_WORK1);
	}

	public String getWsHoldNameWork1() {
		return this.wsHoldNameWork1;
	}

	public String getWsHoldNameWork1Formatted() {
		return Functions.padBlanks(getWsHoldNameWork1(), Len.WS_HOLD_NAME_WORK1);
	}

	public void setWsHoldNameWork2(String wsHoldNameWork2) {
		this.wsHoldNameWork2 = Functions.subString(wsHoldNameWork2, Len.WS_HOLD_NAME_WORK2);
	}

	public String getWsHoldNameWork2() {
		return this.wsHoldNameWork2;
	}

	public String getWsHoldNameWork2Formatted() {
		return Functions.padBlanks(getWsHoldNameWork2(), Len.WS_HOLD_NAME_WORK2);
	}

	public void setWsHoldNameWork3(String wsHoldNameWork3) {
		this.wsHoldNameWork3 = Functions.subString(wsHoldNameWork3, Len.WS_HOLD_NAME_WORK3);
	}

	public String getWsHoldNameWork3() {
		return this.wsHoldNameWork3;
	}

	public String getWsHoldNameWork3Formatted() {
		return Functions.padBlanks(getWsHoldNameWork3(), Len.WS_HOLD_NAME_WORK3);
	}

	public void setWsLongName(String wsLongName) {
		this.wsLongName = Functions.subString(wsLongName, Len.WS_LONG_NAME);
	}

	public String getWsLongName() {
		return this.wsLongName;
	}

	public String getWsLongNameFormatted() {
		return Functions.padBlanks(getWsLongName(), Len.WS_LONG_NAME);
	}

	public void setWsSpaceCounter(short wsSpaceCounter) {
		this.wsSpaceCounter = wsSpaceCounter;
	}

	public short getWsSpaceCounter() {
		return this.wsSpaceCounter;
	}

	public void setWsMidNameCount(short wsMidNameCount) {
		this.wsMidNameCount = wsMidNameCount;
	}

	public short getWsMidNameCount() {
		return this.wsMidNameCount;
	}

	public void setWsErrorCount(short wsErrorCount) {
		this.wsErrorCount = wsErrorCount;
	}

	public short getWsErrorCount() {
		return this.wsErrorCount;
	}

	public void setWsErrorCode(String wsErrorCode) {
		this.wsErrorCode = Functions.subString(wsErrorCode, Len.WS_ERROR_CODE);
	}

	public String getWsErrorCode() {
		return this.wsErrorCode;
	}

	public void setDebugParagraph(String debugParagraph) {
		this.debugParagraph = Functions.subString(debugParagraph, Len.DEBUG_PARAGRAPH);
	}

	public String getDebugParagraph() {
		return this.debugParagraph;
	}

	public void setWsByteTest(char wsByteTest) {
		this.wsByteTest = wsByteTest;
	}

	public char getWsByteTest() {
		return this.wsByteTest;
	}

	public boolean isGoodChar() {
		char fld = wsByteTest;
		return Conditions.isBetween(fld, GOOD_CHAR_1_MIN, GOOD_CHAR_1_MAX) || Conditions.isBetween(fld, GOOD_CHAR_2_MIN, GOOD_CHAR_2_MAX)
				|| fld == GOOD_CHAR_3 || fld == GOOD_CHAR_4 || fld == GOOD_CHAR_5 || fld == GOOD_CHAR_6 || fld == GOOD_CHAR_7 || fld == GOOD_CHAR_8
				|| fld == GOOD_CHAR_9 || fld == GOOD_CHAR_10;
	}

	public WsLastName getWsLastName() {
		return wsLastName;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SPACE_COUNTER = 1;
		public static final int WS_MID_NAME_COUNT = 1;
		public static final int WS_ERROR_CODE = 3;
		public static final int WS_LONG_NAME = 60;
		public static final int WS_HOLD_NAME_WORK2 = 60;
		public static final int WS_HOLD_NAME_WORK3 = 60;
		public static final int WS_HOLD_NAME_WORK1 = 60;
		public static final int DEBUG_PARAGRAPH = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
