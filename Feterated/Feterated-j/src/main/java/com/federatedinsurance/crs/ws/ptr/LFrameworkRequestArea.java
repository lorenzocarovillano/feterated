/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program MU0X0004<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestArea extends BytesClass {

	//==== PROPERTIES ====
	public static final char CWCACQ_MORE_THAN_ONE_ROW = 'Y';
	public static final char CWCACQ_NOT_MORE_THAN_ONE_ROW = 'N';

	//==== CONSTRUCTORS ====
	public LFrameworkRequestArea() {
	}

	public LFrameworkRequestArea(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public String getCwgikqInquireKeyRowFormatted() {
		return readFixedString(Pos.CWGIKQ_INQUIRE_KEY_ROW, Len.CWGIKQ_INQUIRE_KEY_ROW);
	}

	public void setCwgikqTchObjectKey(String cwgikqTchObjectKey) {
		writeString(Pos.CWGIKQ_TCH_OBJECT_KEY, cwgikqTchObjectKey, Len.CWGIKQ_TCH_OBJECT_KEY);
	}

	/**Original name: CWGIKQ-TCH-OBJECT-KEY<br>*/
	public String getCwgikqTchObjectKey() {
		return readString(Pos.CWGIKQ_TCH_OBJECT_KEY, Len.CWGIKQ_TCH_OBJECT_KEY);
	}

	public void setCwgikqRltTypCd(String cwgikqRltTypCd) {
		writeString(Pos.CWGIKQ_RLT_TYP_CD, cwgikqRltTypCd, Len.CWGIKQ_RLT_TYP_CD);
	}

	/**Original name: CWGIKQ-RLT-TYP-CD<br>*/
	public String getCwgikqRltTypCd() {
		return readString(Pos.CWGIKQ_RLT_TYP_CD, Len.CWGIKQ_RLT_TYP_CD);
	}

	public void setCwgikqObjCd(String cwgikqObjCd) {
		writeString(Pos.CWGIKQ_OBJ_CD, cwgikqObjCd, Len.CWGIKQ_OBJ_CD);
	}

	/**Original name: CWGIKQ-OBJ-CD<br>*/
	public String getCwgikqObjCd() {
		return readString(Pos.CWGIKQ_OBJ_CD, Len.CWGIKQ_OBJ_CD);
	}

	public void setCwgikqShwObjKey(String cwgikqShwObjKey) {
		writeString(Pos.CWGIKQ_SHW_OBJ_KEY, cwgikqShwObjKey, Len.CWGIKQ_SHW_OBJ_KEY);
	}

	/**Original name: CWGIKQ-SHW-OBJ-KEY<br>*/
	public String getCwgikqShwObjKey() {
		return readString(Pos.CWGIKQ_SHW_OBJ_KEY, Len.CWGIKQ_SHW_OBJ_KEY);
	}

	public void setCwgikqObjSysId(String cwgikqObjSysId) {
		writeString(Pos.CWGIKQ_OBJ_SYS_ID, cwgikqObjSysId, Len.CWGIKQ_OBJ_SYS_ID);
	}

	/**Original name: CWGIKQ-OBJ-SYS-ID<br>*/
	public String getCwgikqObjSysId() {
		return readString(Pos.CWGIKQ_OBJ_SYS_ID, Len.CWGIKQ_OBJ_SYS_ID);
	}

	public void setCwgikqPhnNbr(String cwgikqPhnNbr) {
		writeString(Pos.CWGIKQ_PHN_NBR, cwgikqPhnNbr, Len.CWGIKQ_PHN_NBR);
	}

	/**Original name: CWGIKQ-PHN-NBR<br>*/
	public String getCwgikqPhnNbr() {
		return readString(Pos.CWGIKQ_PHN_NBR, Len.CWGIKQ_PHN_NBR);
	}

	public void setCwgikqPhnTypCd(String cwgikqPhnTypCd) {
		writeString(Pos.CWGIKQ_PHN_TYP_CD, cwgikqPhnTypCd, Len.CWGIKQ_PHN_TYP_CD);
	}

	/**Original name: CWGIKQ-PHN-TYP-CD<br>*/
	public String getCwgikqPhnTypCd() {
		return readString(Pos.CWGIKQ_PHN_TYP_CD, Len.CWGIKQ_PHN_TYP_CD);
	}

	public void setCwgikqEmailAdrTxt(String cwgikqEmailAdrTxt) {
		writeString(Pos.CWGIKQ_EMAIL_ADR_TXT, cwgikqEmailAdrTxt, Len.CWGIKQ_EMAIL_ADR_TXT);
	}

	/**Original name: CWGIKQ-EMAIL-ADR-TXT<br>*/
	public String getCwgikqEmailAdrTxt() {
		return readString(Pos.CWGIKQ_EMAIL_ADR_TXT, Len.CWGIKQ_EMAIL_ADR_TXT);
	}

	public void setCwgikqEmailTypeCd(String cwgikqEmailTypeCd) {
		writeString(Pos.CWGIKQ_EMAIL_TYPE_CD, cwgikqEmailTypeCd, Len.CWGIKQ_EMAIL_TYPE_CD);
	}

	/**Original name: CWGIKQ-EMAIL-TYPE-CD<br>*/
	public String getCwgikqEmailTypeCd() {
		return readString(Pos.CWGIKQ_EMAIL_TYPE_CD, Len.CWGIKQ_EMAIL_TYPE_CD);
	}

	public void setCwgikqTaxTypeCd(String cwgikqTaxTypeCd) {
		writeString(Pos.CWGIKQ_TAX_TYPE_CD, cwgikqTaxTypeCd, Len.CWGIKQ_TAX_TYPE_CD);
	}

	/**Original name: CWGIKQ-TAX-TYPE-CD<br>*/
	public String getCwgikqTaxTypeCd() {
		return readString(Pos.CWGIKQ_TAX_TYPE_CD, Len.CWGIKQ_TAX_TYPE_CD);
	}

	public void setCwgikqCitxTaxId(String cwgikqCitxTaxId) {
		writeString(Pos.CWGIKQ_CITX_TAX_ID, cwgikqCitxTaxId, Len.CWGIKQ_CITX_TAX_ID);
	}

	/**Original name: CWGIKQ-CITX-TAX-ID<br>*/
	public String getCwgikqCitxTaxId() {
		return readString(Pos.CWGIKQ_CITX_TAX_ID, Len.CWGIKQ_CITX_TAX_ID);
	}

	public void setCwgikqPolNbr(String cwgikqPolNbr) {
		writeString(Pos.CWGIKQ_POL_NBR, cwgikqPolNbr, Len.CWGIKQ_POL_NBR);
	}

	/**Original name: CWGIKQ-POL-NBR<br>*/
	public String getCwgikqPolNbr() {
		return readString(Pos.CWGIKQ_POL_NBR, Len.CWGIKQ_POL_NBR);
	}

	public void setCwgikqEffDt(String cwgikqEffDt) {
		writeString(Pos.CWGIKQ_EFF_DT, cwgikqEffDt, Len.CWGIKQ_EFF_DT);
	}

	/**Original name: CWGIKQ-EFF-DT<br>*/
	public String getCwgikqEffDt() {
		return readString(Pos.CWGIKQ_EFF_DT, Len.CWGIKQ_EFF_DT);
	}

	public void setCwgikqExpDt(String cwgikqExpDt) {
		writeString(Pos.CWGIKQ_EXP_DT, cwgikqExpDt, Len.CWGIKQ_EXP_DT);
	}

	/**Original name: CWGIKQ-EXP-DT<br>*/
	public String getCwgikqExpDt() {
		return readString(Pos.CWGIKQ_EXP_DT, Len.CWGIKQ_EXP_DT);
	}

	public String getCw02qClientTabRowFormatted() {
		return readFixedString(Pos.CW02Q_CLIENT_TAB_ROW, Len.CW02Q_CLIENT_TAB_ROW);
	}

	public void setCw02qClientTabChkSumFormatted(String cw02qClientTabChkSum) {
		writeString(Pos.CW02Q_CLIENT_TAB_CHK_SUM, Trunc.toUnsignedNumeric(cw02qClientTabChkSum, Len.CW02Q_CLIENT_TAB_CHK_SUM),
				Len.CW02Q_CLIENT_TAB_CHK_SUM);
	}

	/**Original name: CW02Q-CLIENT-TAB-CHK-SUM<br>*/
	public int getCw02qClientTabChkSum() {
		return readNumDispUnsignedInt(Pos.CW02Q_CLIENT_TAB_CHK_SUM, Len.CW02Q_CLIENT_TAB_CHK_SUM);
	}

	public void setCw02qClientIdKcre(String cw02qClientIdKcre) {
		writeString(Pos.CW02Q_CLIENT_ID_KCRE, cw02qClientIdKcre, Len.CW02Q_CLIENT_ID_KCRE);
	}

	/**Original name: CW02Q-CLIENT-ID-KCRE<br>*/
	public String getCw02qClientIdKcre() {
		return readString(Pos.CW02Q_CLIENT_ID_KCRE, Len.CW02Q_CLIENT_ID_KCRE);
	}

	public void setCw02qTransProcessDt(String cw02qTransProcessDt) {
		writeString(Pos.CW02Q_TRANS_PROCESS_DT, cw02qTransProcessDt, Len.CW02Q_TRANS_PROCESS_DT);
	}

	/**Original name: CW02Q-TRANS-PROCESS-DT<br>*/
	public String getCw02qTransProcessDt() {
		return readString(Pos.CW02Q_TRANS_PROCESS_DT, Len.CW02Q_TRANS_PROCESS_DT);
	}

	public void setCw02qClientIdCi(char cw02qClientIdCi) {
		writeChar(Pos.CW02Q_CLIENT_ID_CI, cw02qClientIdCi);
	}

	/**Original name: CW02Q-CLIENT-ID-CI<br>*/
	public char getCw02qClientIdCi() {
		return readChar(Pos.CW02Q_CLIENT_ID_CI);
	}

	public void setCw02qClientId(String cw02qClientId) {
		writeString(Pos.CW02Q_CLIENT_ID, cw02qClientId, Len.CW02Q_CLIENT_ID);
	}

	/**Original name: CW02Q-CLIENT-ID<br>*/
	public String getCw02qClientId() {
		return readString(Pos.CW02Q_CLIENT_ID, Len.CW02Q_CLIENT_ID);
	}

	public void setCw02qHistoryVldNbrCi(char cw02qHistoryVldNbrCi) {
		writeChar(Pos.CW02Q_HISTORY_VLD_NBR_CI, cw02qHistoryVldNbrCi);
	}

	/**Original name: CW02Q-HISTORY-VLD-NBR-CI<br>*/
	public char getCw02qHistoryVldNbrCi() {
		return readChar(Pos.CW02Q_HISTORY_VLD_NBR_CI);
	}

	public void setCw02qHistoryVldNbrSign(char cw02qHistoryVldNbrSign) {
		writeChar(Pos.CW02Q_HISTORY_VLD_NBR_SIGN, cw02qHistoryVldNbrSign);
	}

	/**Original name: CW02Q-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw02qHistoryVldNbrSign() {
		return readChar(Pos.CW02Q_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw02qHistoryVldNbrFormatted(String cw02qHistoryVldNbr) {
		writeString(Pos.CW02Q_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw02qHistoryVldNbr, Len.CW02Q_HISTORY_VLD_NBR), Len.CW02Q_HISTORY_VLD_NBR);
	}

	/**Original name: CW02Q-HISTORY-VLD-NBR<br>*/
	public int getCw02qHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW02Q_HISTORY_VLD_NBR, Len.CW02Q_HISTORY_VLD_NBR);
	}

	public void setCw02qCiclEffDtCi(char cw02qCiclEffDtCi) {
		writeChar(Pos.CW02Q_CICL_EFF_DT_CI, cw02qCiclEffDtCi);
	}

	/**Original name: CW02Q-CICL-EFF-DT-CI<br>*/
	public char getCw02qCiclEffDtCi() {
		return readChar(Pos.CW02Q_CICL_EFF_DT_CI);
	}

	public void setCw02qCiclEffDt(String cw02qCiclEffDt) {
		writeString(Pos.CW02Q_CICL_EFF_DT, cw02qCiclEffDt, Len.CW02Q_CICL_EFF_DT);
	}

	/**Original name: CW02Q-CICL-EFF-DT<br>*/
	public String getCw02qCiclEffDt() {
		return readString(Pos.CW02Q_CICL_EFF_DT, Len.CW02Q_CICL_EFF_DT);
	}

	public void setCw02qCiclPriSubCdCi(char cw02qCiclPriSubCdCi) {
		writeChar(Pos.CW02Q_CICL_PRI_SUB_CD_CI, cw02qCiclPriSubCdCi);
	}

	/**Original name: CW02Q-CICL-PRI-SUB-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw02qCiclPriSubCdCi() {
		return readChar(Pos.CW02Q_CICL_PRI_SUB_CD_CI);
	}

	public void setCw02qCiclPriSubCd(String cw02qCiclPriSubCd) {
		writeString(Pos.CW02Q_CICL_PRI_SUB_CD, cw02qCiclPriSubCd, Len.CW02Q_CICL_PRI_SUB_CD);
	}

	/**Original name: CW02Q-CICL-PRI-SUB-CD<br>*/
	public String getCw02qCiclPriSubCd() {
		return readString(Pos.CW02Q_CICL_PRI_SUB_CD, Len.CW02Q_CICL_PRI_SUB_CD);
	}

	public void setCw02qCiclFstNmCi(char cw02qCiclFstNmCi) {
		writeChar(Pos.CW02Q_CICL_FST_NM_CI, cw02qCiclFstNmCi);
	}

	/**Original name: CW02Q-CICL-FST-NM-CI<br>*/
	public char getCw02qCiclFstNmCi() {
		return readChar(Pos.CW02Q_CICL_FST_NM_CI);
	}

	public void setCw02qCiclFstNm(String cw02qCiclFstNm) {
		writeString(Pos.CW02Q_CICL_FST_NM, cw02qCiclFstNm, Len.CW02Q_CICL_FST_NM);
	}

	/**Original name: CW02Q-CICL-FST-NM<br>*/
	public String getCw02qCiclFstNm() {
		return readString(Pos.CW02Q_CICL_FST_NM, Len.CW02Q_CICL_FST_NM);
	}

	public void setCw02qCiclLstNmCi(char cw02qCiclLstNmCi) {
		writeChar(Pos.CW02Q_CICL_LST_NM_CI, cw02qCiclLstNmCi);
	}

	/**Original name: CW02Q-CICL-LST-NM-CI<br>*/
	public char getCw02qCiclLstNmCi() {
		return readChar(Pos.CW02Q_CICL_LST_NM_CI);
	}

	public void setCw02qCiclLstNm(String cw02qCiclLstNm) {
		writeString(Pos.CW02Q_CICL_LST_NM, cw02qCiclLstNm, Len.CW02Q_CICL_LST_NM);
	}

	/**Original name: CW02Q-CICL-LST-NM<br>*/
	public String getCw02qCiclLstNm() {
		return readString(Pos.CW02Q_CICL_LST_NM, Len.CW02Q_CICL_LST_NM);
	}

	public void setCw02qCiclMdlNmCi(char cw02qCiclMdlNmCi) {
		writeChar(Pos.CW02Q_CICL_MDL_NM_CI, cw02qCiclMdlNmCi);
	}

	/**Original name: CW02Q-CICL-MDL-NM-CI<br>*/
	public char getCw02qCiclMdlNmCi() {
		return readChar(Pos.CW02Q_CICL_MDL_NM_CI);
	}

	public void setCw02qCiclMdlNm(String cw02qCiclMdlNm) {
		writeString(Pos.CW02Q_CICL_MDL_NM, cw02qCiclMdlNm, Len.CW02Q_CICL_MDL_NM);
	}

	/**Original name: CW02Q-CICL-MDL-NM<br>*/
	public String getCw02qCiclMdlNm() {
		return readString(Pos.CW02Q_CICL_MDL_NM, Len.CW02Q_CICL_MDL_NM);
	}

	public void setCw02qNmPfxCi(char cw02qNmPfxCi) {
		writeChar(Pos.CW02Q_NM_PFX_CI, cw02qNmPfxCi);
	}

	/**Original name: CW02Q-NM-PFX-CI<br>*/
	public char getCw02qNmPfxCi() {
		return readChar(Pos.CW02Q_NM_PFX_CI);
	}

	public void setCw02qNmPfx(String cw02qNmPfx) {
		writeString(Pos.CW02Q_NM_PFX, cw02qNmPfx, Len.CW02Q_NM_PFX);
	}

	/**Original name: CW02Q-NM-PFX<br>*/
	public String getCw02qNmPfx() {
		return readString(Pos.CW02Q_NM_PFX, Len.CW02Q_NM_PFX);
	}

	public void setCw02qNmSfxCi(char cw02qNmSfxCi) {
		writeChar(Pos.CW02Q_NM_SFX_CI, cw02qNmSfxCi);
	}

	/**Original name: CW02Q-NM-SFX-CI<br>*/
	public char getCw02qNmSfxCi() {
		return readChar(Pos.CW02Q_NM_SFX_CI);
	}

	public void setCw02qNmSfx(String cw02qNmSfx) {
		writeString(Pos.CW02Q_NM_SFX, cw02qNmSfx, Len.CW02Q_NM_SFX);
	}

	/**Original name: CW02Q-NM-SFX<br>*/
	public String getCw02qNmSfx() {
		return readString(Pos.CW02Q_NM_SFX, Len.CW02Q_NM_SFX);
	}

	public void setCw02qPrimaryProDsnCdCi(char cw02qPrimaryProDsnCdCi) {
		writeChar(Pos.CW02Q_PRIMARY_PRO_DSN_CD_CI, cw02qPrimaryProDsnCdCi);
	}

	/**Original name: CW02Q-PRIMARY-PRO-DSN-CD-CI<br>*/
	public char getCw02qPrimaryProDsnCdCi() {
		return readChar(Pos.CW02Q_PRIMARY_PRO_DSN_CD_CI);
	}

	public void setCw02qPrimaryProDsnCdNi(char cw02qPrimaryProDsnCdNi) {
		writeChar(Pos.CW02Q_PRIMARY_PRO_DSN_CD_NI, cw02qPrimaryProDsnCdNi);
	}

	/**Original name: CW02Q-PRIMARY-PRO-DSN-CD-NI<br>*/
	public char getCw02qPrimaryProDsnCdNi() {
		return readChar(Pos.CW02Q_PRIMARY_PRO_DSN_CD_NI);
	}

	public void setCw02qPrimaryProDsnCd(String cw02qPrimaryProDsnCd) {
		writeString(Pos.CW02Q_PRIMARY_PRO_DSN_CD, cw02qPrimaryProDsnCd, Len.CW02Q_PRIMARY_PRO_DSN_CD);
	}

	/**Original name: CW02Q-PRIMARY-PRO-DSN-CD<br>*/
	public String getCw02qPrimaryProDsnCd() {
		return readString(Pos.CW02Q_PRIMARY_PRO_DSN_CD, Len.CW02Q_PRIMARY_PRO_DSN_CD);
	}

	public void setCw02qLegEntCdCi(char cw02qLegEntCdCi) {
		writeChar(Pos.CW02Q_LEG_ENT_CD_CI, cw02qLegEntCdCi);
	}

	/**Original name: CW02Q-LEG-ENT-CD-CI<br>*/
	public char getCw02qLegEntCdCi() {
		return readChar(Pos.CW02Q_LEG_ENT_CD_CI);
	}

	public void setCw02qLegEntCd(String cw02qLegEntCd) {
		writeString(Pos.CW02Q_LEG_ENT_CD, cw02qLegEntCd, Len.CW02Q_LEG_ENT_CD);
	}

	/**Original name: CW02Q-LEG-ENT-CD<br>*/
	public String getCw02qLegEntCd() {
		return readString(Pos.CW02Q_LEG_ENT_CD, Len.CW02Q_LEG_ENT_CD);
	}

	public void setCw02qCiclSdxCdCi(char cw02qCiclSdxCdCi) {
		writeChar(Pos.CW02Q_CICL_SDX_CD_CI, cw02qCiclSdxCdCi);
	}

	/**Original name: CW02Q-CICL-SDX-CD-CI<br>*/
	public char getCw02qCiclSdxCdCi() {
		return readChar(Pos.CW02Q_CICL_SDX_CD_CI);
	}

	public void setCw02qCiclSdxCd(String cw02qCiclSdxCd) {
		writeString(Pos.CW02Q_CICL_SDX_CD, cw02qCiclSdxCd, Len.CW02Q_CICL_SDX_CD);
	}

	/**Original name: CW02Q-CICL-SDX-CD<br>*/
	public String getCw02qCiclSdxCd() {
		return readString(Pos.CW02Q_CICL_SDX_CD, Len.CW02Q_CICL_SDX_CD);
	}

	public void setCw02qCiclOgnInceptDtCi(char cw02qCiclOgnInceptDtCi) {
		writeChar(Pos.CW02Q_CICL_OGN_INCEPT_DT_CI, cw02qCiclOgnInceptDtCi);
	}

	/**Original name: CW02Q-CICL-OGN-INCEPT-DT-CI<br>*/
	public char getCw02qCiclOgnInceptDtCi() {
		return readChar(Pos.CW02Q_CICL_OGN_INCEPT_DT_CI);
	}

	public void setCw02qCiclOgnInceptDtNi(char cw02qCiclOgnInceptDtNi) {
		writeChar(Pos.CW02Q_CICL_OGN_INCEPT_DT_NI, cw02qCiclOgnInceptDtNi);
	}

	/**Original name: CW02Q-CICL-OGN-INCEPT-DT-NI<br>*/
	public char getCw02qCiclOgnInceptDtNi() {
		return readChar(Pos.CW02Q_CICL_OGN_INCEPT_DT_NI);
	}

	public void setCw02qCiclOgnInceptDt(String cw02qCiclOgnInceptDt) {
		writeString(Pos.CW02Q_CICL_OGN_INCEPT_DT, cw02qCiclOgnInceptDt, Len.CW02Q_CICL_OGN_INCEPT_DT);
	}

	/**Original name: CW02Q-CICL-OGN-INCEPT-DT<br>*/
	public String getCw02qCiclOgnInceptDt() {
		return readString(Pos.CW02Q_CICL_OGN_INCEPT_DT, Len.CW02Q_CICL_OGN_INCEPT_DT);
	}

	public void setCw02qCiclAddNmIndCi(char cw02qCiclAddNmIndCi) {
		writeChar(Pos.CW02Q_CICL_ADD_NM_IND_CI, cw02qCiclAddNmIndCi);
	}

	/**Original name: CW02Q-CICL-ADD-NM-IND-CI<br>*/
	public char getCw02qCiclAddNmIndCi() {
		return readChar(Pos.CW02Q_CICL_ADD_NM_IND_CI);
	}

	public void setCw02qCiclAddNmIndNi(char cw02qCiclAddNmIndNi) {
		writeChar(Pos.CW02Q_CICL_ADD_NM_IND_NI, cw02qCiclAddNmIndNi);
	}

	/**Original name: CW02Q-CICL-ADD-NM-IND-NI<br>*/
	public char getCw02qCiclAddNmIndNi() {
		return readChar(Pos.CW02Q_CICL_ADD_NM_IND_NI);
	}

	public void setCw02qCiclAddNmInd(char cw02qCiclAddNmInd) {
		writeChar(Pos.CW02Q_CICL_ADD_NM_IND, cw02qCiclAddNmInd);
	}

	/**Original name: CW02Q-CICL-ADD-NM-IND<br>*/
	public char getCw02qCiclAddNmInd() {
		return readChar(Pos.CW02Q_CICL_ADD_NM_IND);
	}

	public void setCw02qCiclDobDtCi(char cw02qCiclDobDtCi) {
		writeChar(Pos.CW02Q_CICL_DOB_DT_CI, cw02qCiclDobDtCi);
	}

	/**Original name: CW02Q-CICL-DOB-DT-CI<br>*/
	public char getCw02qCiclDobDtCi() {
		return readChar(Pos.CW02Q_CICL_DOB_DT_CI);
	}

	public void setCw02qCiclDobDt(String cw02qCiclDobDt) {
		writeString(Pos.CW02Q_CICL_DOB_DT, cw02qCiclDobDt, Len.CW02Q_CICL_DOB_DT);
	}

	/**Original name: CW02Q-CICL-DOB-DT<br>*/
	public String getCw02qCiclDobDt() {
		return readString(Pos.CW02Q_CICL_DOB_DT, Len.CW02Q_CICL_DOB_DT);
	}

	public void setCw02qCiclBirStCdCi(char cw02qCiclBirStCdCi) {
		writeChar(Pos.CW02Q_CICL_BIR_ST_CD_CI, cw02qCiclBirStCdCi);
	}

	/**Original name: CW02Q-CICL-BIR-ST-CD-CI<br>*/
	public char getCw02qCiclBirStCdCi() {
		return readChar(Pos.CW02Q_CICL_BIR_ST_CD_CI);
	}

	public void setCw02qCiclBirStCd(String cw02qCiclBirStCd) {
		writeString(Pos.CW02Q_CICL_BIR_ST_CD, cw02qCiclBirStCd, Len.CW02Q_CICL_BIR_ST_CD);
	}

	/**Original name: CW02Q-CICL-BIR-ST-CD<br>*/
	public String getCw02qCiclBirStCd() {
		return readString(Pos.CW02Q_CICL_BIR_ST_CD, Len.CW02Q_CICL_BIR_ST_CD);
	}

	public void setCw02qGenderCdCi(char cw02qGenderCdCi) {
		writeChar(Pos.CW02Q_GENDER_CD_CI, cw02qGenderCdCi);
	}

	/**Original name: CW02Q-GENDER-CD-CI<br>*/
	public char getCw02qGenderCdCi() {
		return readChar(Pos.CW02Q_GENDER_CD_CI);
	}

	public void setCw02qGenderCd(char cw02qGenderCd) {
		writeChar(Pos.CW02Q_GENDER_CD, cw02qGenderCd);
	}

	/**Original name: CW02Q-GENDER-CD<br>*/
	public char getCw02qGenderCd() {
		return readChar(Pos.CW02Q_GENDER_CD);
	}

	public void setCw02qPriLggCdCi(char cw02qPriLggCdCi) {
		writeChar(Pos.CW02Q_PRI_LGG_CD_CI, cw02qPriLggCdCi);
	}

	/**Original name: CW02Q-PRI-LGG-CD-CI<br>*/
	public char getCw02qPriLggCdCi() {
		return readChar(Pos.CW02Q_PRI_LGG_CD_CI);
	}

	public void setCw02qPriLggCd(String cw02qPriLggCd) {
		writeString(Pos.CW02Q_PRI_LGG_CD, cw02qPriLggCd, Len.CW02Q_PRI_LGG_CD);
	}

	/**Original name: CW02Q-PRI-LGG-CD<br>*/
	public String getCw02qPriLggCd() {
		return readString(Pos.CW02Q_PRI_LGG_CD, Len.CW02Q_PRI_LGG_CD);
	}

	public void setCw02qUserIdCi(char cw02qUserIdCi) {
		writeChar(Pos.CW02Q_USER_ID_CI, cw02qUserIdCi);
	}

	/**Original name: CW02Q-USER-ID-CI<br>*/
	public char getCw02qUserIdCi() {
		return readChar(Pos.CW02Q_USER_ID_CI);
	}

	public void setCw02qUserId(String cw02qUserId) {
		writeString(Pos.CW02Q_USER_ID, cw02qUserId, Len.CW02Q_USER_ID);
	}

	/**Original name: CW02Q-USER-ID<br>*/
	public String getCw02qUserId() {
		return readString(Pos.CW02Q_USER_ID, Len.CW02Q_USER_ID);
	}

	public void setCw02qStatusCdCi(char cw02qStatusCdCi) {
		writeChar(Pos.CW02Q_STATUS_CD_CI, cw02qStatusCdCi);
	}

	/**Original name: CW02Q-STATUS-CD-CI<br>*/
	public char getCw02qStatusCdCi() {
		return readChar(Pos.CW02Q_STATUS_CD_CI);
	}

	public void setCw02qStatusCd(char cw02qStatusCd) {
		writeChar(Pos.CW02Q_STATUS_CD, cw02qStatusCd);
	}

	/**Original name: CW02Q-STATUS-CD<br>*/
	public char getCw02qStatusCd() {
		return readChar(Pos.CW02Q_STATUS_CD);
	}

	public void setCw02qTerminalIdCi(char cw02qTerminalIdCi) {
		writeChar(Pos.CW02Q_TERMINAL_ID_CI, cw02qTerminalIdCi);
	}

	/**Original name: CW02Q-TERMINAL-ID-CI<br>*/
	public char getCw02qTerminalIdCi() {
		return readChar(Pos.CW02Q_TERMINAL_ID_CI);
	}

	public void setCw02qTerminalId(String cw02qTerminalId) {
		writeString(Pos.CW02Q_TERMINAL_ID, cw02qTerminalId, Len.CW02Q_TERMINAL_ID);
	}

	/**Original name: CW02Q-TERMINAL-ID<br>*/
	public String getCw02qTerminalId() {
		return readString(Pos.CW02Q_TERMINAL_ID, Len.CW02Q_TERMINAL_ID);
	}

	public void setCw02qCiclExpDtCi(char cw02qCiclExpDtCi) {
		writeChar(Pos.CW02Q_CICL_EXP_DT_CI, cw02qCiclExpDtCi);
	}

	/**Original name: CW02Q-CICL-EXP-DT-CI<br>*/
	public char getCw02qCiclExpDtCi() {
		return readChar(Pos.CW02Q_CICL_EXP_DT_CI);
	}

	public void setCw02qCiclExpDt(String cw02qCiclExpDt) {
		writeString(Pos.CW02Q_CICL_EXP_DT, cw02qCiclExpDt, Len.CW02Q_CICL_EXP_DT);
	}

	/**Original name: CW02Q-CICL-EXP-DT<br>*/
	public String getCw02qCiclExpDt() {
		return readString(Pos.CW02Q_CICL_EXP_DT, Len.CW02Q_CICL_EXP_DT);
	}

	public void setCw02qCiclEffAcyTsCi(char cw02qCiclEffAcyTsCi) {
		writeChar(Pos.CW02Q_CICL_EFF_ACY_TS_CI, cw02qCiclEffAcyTsCi);
	}

	/**Original name: CW02Q-CICL-EFF-ACY-TS-CI<br>*/
	public char getCw02qCiclEffAcyTsCi() {
		return readChar(Pos.CW02Q_CICL_EFF_ACY_TS_CI);
	}

	public void setCw02qCiclEffAcyTs(String cw02qCiclEffAcyTs) {
		writeString(Pos.CW02Q_CICL_EFF_ACY_TS, cw02qCiclEffAcyTs, Len.CW02Q_CICL_EFF_ACY_TS);
	}

	/**Original name: CW02Q-CICL-EFF-ACY-TS<br>*/
	public String getCw02qCiclEffAcyTs() {
		return readString(Pos.CW02Q_CICL_EFF_ACY_TS, Len.CW02Q_CICL_EFF_ACY_TS);
	}

	public void setCw02qCiclExpAcyTsCi(char cw02qCiclExpAcyTsCi) {
		writeChar(Pos.CW02Q_CICL_EXP_ACY_TS_CI, cw02qCiclExpAcyTsCi);
	}

	/**Original name: CW02Q-CICL-EXP-ACY-TS-CI<br>*/
	public char getCw02qCiclExpAcyTsCi() {
		return readChar(Pos.CW02Q_CICL_EXP_ACY_TS_CI);
	}

	public void setCw02qCiclExpAcyTs(String cw02qCiclExpAcyTs) {
		writeString(Pos.CW02Q_CICL_EXP_ACY_TS, cw02qCiclExpAcyTs, Len.CW02Q_CICL_EXP_ACY_TS);
	}

	/**Original name: CW02Q-CICL-EXP-ACY-TS<br>*/
	public String getCw02qCiclExpAcyTs() {
		return readString(Pos.CW02Q_CICL_EXP_ACY_TS, Len.CW02Q_CICL_EXP_ACY_TS);
	}

	public void setCw02qStatutoryTleCdCi(char cw02qStatutoryTleCdCi) {
		writeChar(Pos.CW02Q_STATUTORY_TLE_CD_CI, cw02qStatutoryTleCdCi);
	}

	/**Original name: CW02Q-STATUTORY-TLE-CD-CI<br>*/
	public char getCw02qStatutoryTleCdCi() {
		return readChar(Pos.CW02Q_STATUTORY_TLE_CD_CI);
	}

	public void setCw02qStatutoryTleCd(String cw02qStatutoryTleCd) {
		writeString(Pos.CW02Q_STATUTORY_TLE_CD, cw02qStatutoryTleCd, Len.CW02Q_STATUTORY_TLE_CD);
	}

	/**Original name: CW02Q-STATUTORY-TLE-CD<br>*/
	public String getCw02qStatutoryTleCd() {
		return readString(Pos.CW02Q_STATUTORY_TLE_CD, Len.CW02Q_STATUTORY_TLE_CD);
	}

	public void setCw02qCiclLngNmCi(char cw02qCiclLngNmCi) {
		writeChar(Pos.CW02Q_CICL_LNG_NM_CI, cw02qCiclLngNmCi);
	}

	/**Original name: CW02Q-CICL-LNG-NM-CI<br>*/
	public char getCw02qCiclLngNmCi() {
		return readChar(Pos.CW02Q_CICL_LNG_NM_CI);
	}

	public void setCw02qCiclLngNmNi(char cw02qCiclLngNmNi) {
		writeChar(Pos.CW02Q_CICL_LNG_NM_NI, cw02qCiclLngNmNi);
	}

	/**Original name: CW02Q-CICL-LNG-NM-NI<br>*/
	public char getCw02qCiclLngNmNi() {
		return readChar(Pos.CW02Q_CICL_LNG_NM_NI);
	}

	public void setCw02qCiclLngNm(String cw02qCiclLngNm) {
		writeString(Pos.CW02Q_CICL_LNG_NM, cw02qCiclLngNm, Len.CW02Q_CICL_LNG_NM);
	}

	/**Original name: CW02Q-CICL-LNG-NM<br>*/
	public String getCw02qCiclLngNm() {
		return readString(Pos.CW02Q_CICL_LNG_NM, Len.CW02Q_CICL_LNG_NM);
	}

	public void setCw02qLoLstNmMchCdCi(char cw02qLoLstNmMchCdCi) {
		writeChar(Pos.CW02Q_LO_LST_NM_MCH_CD_CI, cw02qLoLstNmMchCdCi);
	}

	/**Original name: CW02Q-LO-LST-NM-MCH-CD-CI<br>*/
	public char getCw02qLoLstNmMchCdCi() {
		return readChar(Pos.CW02Q_LO_LST_NM_MCH_CD_CI);
	}

	public void setCw02qLoLstNmMchCdNi(char cw02qLoLstNmMchCdNi) {
		writeChar(Pos.CW02Q_LO_LST_NM_MCH_CD_NI, cw02qLoLstNmMchCdNi);
	}

	/**Original name: CW02Q-LO-LST-NM-MCH-CD-NI<br>*/
	public char getCw02qLoLstNmMchCdNi() {
		return readChar(Pos.CW02Q_LO_LST_NM_MCH_CD_NI);
	}

	public void setCw02qLoLstNmMchCd(String cw02qLoLstNmMchCd) {
		writeString(Pos.CW02Q_LO_LST_NM_MCH_CD, cw02qLoLstNmMchCd, Len.CW02Q_LO_LST_NM_MCH_CD);
	}

	/**Original name: CW02Q-LO-LST-NM-MCH-CD<br>*/
	public String getCw02qLoLstNmMchCd() {
		return readString(Pos.CW02Q_LO_LST_NM_MCH_CD, Len.CW02Q_LO_LST_NM_MCH_CD);
	}

	public void setCw02qHiLstNmMchCdCi(char cw02qHiLstNmMchCdCi) {
		writeChar(Pos.CW02Q_HI_LST_NM_MCH_CD_CI, cw02qHiLstNmMchCdCi);
	}

	/**Original name: CW02Q-HI-LST-NM-MCH-CD-CI<br>*/
	public char getCw02qHiLstNmMchCdCi() {
		return readChar(Pos.CW02Q_HI_LST_NM_MCH_CD_CI);
	}

	public void setCw02qHiLstNmMchCdNi(char cw02qHiLstNmMchCdNi) {
		writeChar(Pos.CW02Q_HI_LST_NM_MCH_CD_NI, cw02qHiLstNmMchCdNi);
	}

	/**Original name: CW02Q-HI-LST-NM-MCH-CD-NI<br>*/
	public char getCw02qHiLstNmMchCdNi() {
		return readChar(Pos.CW02Q_HI_LST_NM_MCH_CD_NI);
	}

	public void setCw02qHiLstNmMchCd(String cw02qHiLstNmMchCd) {
		writeString(Pos.CW02Q_HI_LST_NM_MCH_CD, cw02qHiLstNmMchCd, Len.CW02Q_HI_LST_NM_MCH_CD);
	}

	/**Original name: CW02Q-HI-LST-NM-MCH-CD<br>*/
	public String getCw02qHiLstNmMchCd() {
		return readString(Pos.CW02Q_HI_LST_NM_MCH_CD, Len.CW02Q_HI_LST_NM_MCH_CD);
	}

	public void setCw02qLoFstNmMchCdCi(char cw02qLoFstNmMchCdCi) {
		writeChar(Pos.CW02Q_LO_FST_NM_MCH_CD_CI, cw02qLoFstNmMchCdCi);
	}

	/**Original name: CW02Q-LO-FST-NM-MCH-CD-CI<br>*/
	public char getCw02qLoFstNmMchCdCi() {
		return readChar(Pos.CW02Q_LO_FST_NM_MCH_CD_CI);
	}

	public void setCw02qLoFstNmMchCdNi(char cw02qLoFstNmMchCdNi) {
		writeChar(Pos.CW02Q_LO_FST_NM_MCH_CD_NI, cw02qLoFstNmMchCdNi);
	}

	/**Original name: CW02Q-LO-FST-NM-MCH-CD-NI<br>*/
	public char getCw02qLoFstNmMchCdNi() {
		return readChar(Pos.CW02Q_LO_FST_NM_MCH_CD_NI);
	}

	public void setCw02qLoFstNmMchCd(String cw02qLoFstNmMchCd) {
		writeString(Pos.CW02Q_LO_FST_NM_MCH_CD, cw02qLoFstNmMchCd, Len.CW02Q_LO_FST_NM_MCH_CD);
	}

	/**Original name: CW02Q-LO-FST-NM-MCH-CD<br>*/
	public String getCw02qLoFstNmMchCd() {
		return readString(Pos.CW02Q_LO_FST_NM_MCH_CD, Len.CW02Q_LO_FST_NM_MCH_CD);
	}

	public void setCw02qHiFstNmMchCdCi(char cw02qHiFstNmMchCdCi) {
		writeChar(Pos.CW02Q_HI_FST_NM_MCH_CD_CI, cw02qHiFstNmMchCdCi);
	}

	/**Original name: CW02Q-HI-FST-NM-MCH-CD-CI<br>*/
	public char getCw02qHiFstNmMchCdCi() {
		return readChar(Pos.CW02Q_HI_FST_NM_MCH_CD_CI);
	}

	public void setCw02qHiFstNmMchCdNi(char cw02qHiFstNmMchCdNi) {
		writeChar(Pos.CW02Q_HI_FST_NM_MCH_CD_NI, cw02qHiFstNmMchCdNi);
	}

	/**Original name: CW02Q-HI-FST-NM-MCH-CD-NI<br>*/
	public char getCw02qHiFstNmMchCdNi() {
		return readChar(Pos.CW02Q_HI_FST_NM_MCH_CD_NI);
	}

	public void setCw02qHiFstNmMchCd(String cw02qHiFstNmMchCd) {
		writeString(Pos.CW02Q_HI_FST_NM_MCH_CD, cw02qHiFstNmMchCd, Len.CW02Q_HI_FST_NM_MCH_CD);
	}

	/**Original name: CW02Q-HI-FST-NM-MCH-CD<br>*/
	public String getCw02qHiFstNmMchCd() {
		return readString(Pos.CW02Q_HI_FST_NM_MCH_CD, Len.CW02Q_HI_FST_NM_MCH_CD);
	}

	public void setCw02qCiclAcqSrcCdCi(char cw02qCiclAcqSrcCdCi) {
		writeChar(Pos.CW02Q_CICL_ACQ_SRC_CD_CI, cw02qCiclAcqSrcCdCi);
	}

	/**Original name: CW02Q-CICL-ACQ-SRC-CD-CI<br>*/
	public char getCw02qCiclAcqSrcCdCi() {
		return readChar(Pos.CW02Q_CICL_ACQ_SRC_CD_CI);
	}

	public void setCw02qCiclAcqSrcCdNi(char cw02qCiclAcqSrcCdNi) {
		writeChar(Pos.CW02Q_CICL_ACQ_SRC_CD_NI, cw02qCiclAcqSrcCdNi);
	}

	/**Original name: CW02Q-CICL-ACQ-SRC-CD-NI<br>*/
	public char getCw02qCiclAcqSrcCdNi() {
		return readChar(Pos.CW02Q_CICL_ACQ_SRC_CD_NI);
	}

	public void setCw02qCiclAcqSrcCd(String cw02qCiclAcqSrcCd) {
		writeString(Pos.CW02Q_CICL_ACQ_SRC_CD, cw02qCiclAcqSrcCd, Len.CW02Q_CICL_ACQ_SRC_CD);
	}

	/**Original name: CW02Q-CICL-ACQ-SRC-CD<br>*/
	public String getCw02qCiclAcqSrcCd() {
		return readString(Pos.CW02Q_CICL_ACQ_SRC_CD, Len.CW02Q_CICL_ACQ_SRC_CD);
	}

	public void setCw02qLegEntDesc(String cw02qLegEntDesc) {
		writeString(Pos.CW02Q_LEG_ENT_DESC, cw02qLegEntDesc, Len.CW02Q_LEG_ENT_DESC);
	}

	/**Original name: CW02Q-LEG-ENT-DESC<br>*/
	public String getCw02qLegEntDesc() {
		return readString(Pos.CW02Q_LEG_ENT_DESC, Len.CW02Q_LEG_ENT_DESC);
	}

	public void setCwcacqClientAddrCompChksumFormatted(String cwcacqClientAddrCompChksum) {
		writeString(Pos.CWCACQ_CLIENT_ADDR_COMP_CHKSUM, Trunc.toUnsignedNumeric(cwcacqClientAddrCompChksum, Len.CWCACQ_CLIENT_ADDR_COMP_CHKSUM),
				Len.CWCACQ_CLIENT_ADDR_COMP_CHKSUM);
	}

	/**Original name: CWCACQ-CLIENT-ADDR-COMP-CHKSUM<br>*/
	public int getCwcacqClientAddrCompChksum() {
		return readNumDispUnsignedInt(Pos.CWCACQ_CLIENT_ADDR_COMP_CHKSUM, Len.CWCACQ_CLIENT_ADDR_COMP_CHKSUM);
	}

	public void setCwcacqAdrIdKcre(String cwcacqAdrIdKcre) {
		writeString(Pos.CWCACQ_ADR_ID_KCRE, cwcacqAdrIdKcre, Len.CWCACQ_ADR_ID_KCRE);
	}

	/**Original name: CWCACQ-ADR-ID-KCRE<br>*/
	public String getCwcacqAdrIdKcre() {
		return readString(Pos.CWCACQ_ADR_ID_KCRE, Len.CWCACQ_ADR_ID_KCRE);
	}

	public void setCwcacqClientIdKcre(String cwcacqClientIdKcre) {
		writeString(Pos.CWCACQ_CLIENT_ID_KCRE, cwcacqClientIdKcre, Len.CWCACQ_CLIENT_ID_KCRE);
	}

	/**Original name: CWCACQ-CLIENT-ID-KCRE<br>*/
	public String getCwcacqClientIdKcre() {
		return readString(Pos.CWCACQ_CLIENT_ID_KCRE, Len.CWCACQ_CLIENT_ID_KCRE);
	}

	public void setCwcacqAdrSeqNbrKcre(String cwcacqAdrSeqNbrKcre) {
		writeString(Pos.CWCACQ_ADR_SEQ_NBR_KCRE, cwcacqAdrSeqNbrKcre, Len.CWCACQ_ADR_SEQ_NBR_KCRE);
	}

	/**Original name: CWCACQ-ADR-SEQ-NBR-KCRE<br>*/
	public String getCwcacqAdrSeqNbrKcre() {
		return readString(Pos.CWCACQ_ADR_SEQ_NBR_KCRE, Len.CWCACQ_ADR_SEQ_NBR_KCRE);
	}

	public void setCwcacqTchObjectKeyKcre(String cwcacqTchObjectKeyKcre) {
		writeString(Pos.CWCACQ_TCH_OBJECT_KEY_KCRE, cwcacqTchObjectKeyKcre, Len.CWCACQ_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CWCACQ-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCwcacqTchObjectKeyKcre() {
		return readString(Pos.CWCACQ_TCH_OBJECT_KEY_KCRE, Len.CWCACQ_TCH_OBJECT_KEY_KCRE);
	}

	public void setCwcacqAdrIdCi(char cwcacqAdrIdCi) {
		writeChar(Pos.CWCACQ_ADR_ID_CI, cwcacqAdrIdCi);
	}

	/**Original name: CWCACQ-ADR-ID-CI<br>*/
	public char getCwcacqAdrIdCi() {
		return readChar(Pos.CWCACQ_ADR_ID_CI);
	}

	public void setCwcacqAdrId(String cwcacqAdrId) {
		writeString(Pos.CWCACQ_ADR_ID, cwcacqAdrId, Len.CWCACQ_ADR_ID);
	}

	/**Original name: CWCACQ-ADR-ID<br>*/
	public String getCwcacqAdrId() {
		return readString(Pos.CWCACQ_ADR_ID, Len.CWCACQ_ADR_ID);
	}

	public void setCwcacqClientIdCi(char cwcacqClientIdCi) {
		writeChar(Pos.CWCACQ_CLIENT_ID_CI, cwcacqClientIdCi);
	}

	/**Original name: CWCACQ-CLIENT-ID-CI<br>*/
	public char getCwcacqClientIdCi() {
		return readChar(Pos.CWCACQ_CLIENT_ID_CI);
	}

	public void setCwcacqClientId(String cwcacqClientId) {
		writeString(Pos.CWCACQ_CLIENT_ID, cwcacqClientId, Len.CWCACQ_CLIENT_ID);
	}

	/**Original name: CWCACQ-CLIENT-ID<br>*/
	public String getCwcacqClientId() {
		return readString(Pos.CWCACQ_CLIENT_ID, Len.CWCACQ_CLIENT_ID);
	}

	public void setCwcacqHistoryVldNbrCi(char cwcacqHistoryVldNbrCi) {
		writeChar(Pos.CWCACQ_HISTORY_VLD_NBR_CI, cwcacqHistoryVldNbrCi);
	}

	/**Original name: CWCACQ-HISTORY-VLD-NBR-CI<br>*/
	public char getCwcacqHistoryVldNbrCi() {
		return readChar(Pos.CWCACQ_HISTORY_VLD_NBR_CI);
	}

	public void setCwcacqHistoryVldNbrSign(char cwcacqHistoryVldNbrSign) {
		writeChar(Pos.CWCACQ_HISTORY_VLD_NBR_SIGN, cwcacqHistoryVldNbrSign);
	}

	/**Original name: CWCACQ-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCwcacqHistoryVldNbrSign() {
		return readChar(Pos.CWCACQ_HISTORY_VLD_NBR_SIGN);
	}

	public void setCwcacqHistoryVldNbrFormatted(String cwcacqHistoryVldNbr) {
		writeString(Pos.CWCACQ_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cwcacqHistoryVldNbr, Len.CWCACQ_HISTORY_VLD_NBR), Len.CWCACQ_HISTORY_VLD_NBR);
	}

	/**Original name: CWCACQ-HISTORY-VLD-NBR<br>*/
	public int getCwcacqHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CWCACQ_HISTORY_VLD_NBR, Len.CWCACQ_HISTORY_VLD_NBR);
	}

	public void setCwcacqAdrSeqNbrCi(char cwcacqAdrSeqNbrCi) {
		writeChar(Pos.CWCACQ_ADR_SEQ_NBR_CI, cwcacqAdrSeqNbrCi);
	}

	/**Original name: CWCACQ-ADR-SEQ-NBR-CI<br>*/
	public char getCwcacqAdrSeqNbrCi() {
		return readChar(Pos.CWCACQ_ADR_SEQ_NBR_CI);
	}

	public void setCwcacqAdrSeqNbrSign(char cwcacqAdrSeqNbrSign) {
		writeChar(Pos.CWCACQ_ADR_SEQ_NBR_SIGN, cwcacqAdrSeqNbrSign);
	}

	/**Original name: CWCACQ-ADR-SEQ-NBR-SIGN<br>*/
	public char getCwcacqAdrSeqNbrSign() {
		return readChar(Pos.CWCACQ_ADR_SEQ_NBR_SIGN);
	}

	public void setCwcacqAdrSeqNbrFormatted(String cwcacqAdrSeqNbr) {
		writeString(Pos.CWCACQ_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(cwcacqAdrSeqNbr, Len.CWCACQ_ADR_SEQ_NBR), Len.CWCACQ_ADR_SEQ_NBR);
	}

	/**Original name: CWCACQ-ADR-SEQ-NBR<br>*/
	public int getCwcacqAdrSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWCACQ_ADR_SEQ_NBR, Len.CWCACQ_ADR_SEQ_NBR);
	}

	public void setCwcacqCiarEffDtCi(char cwcacqCiarEffDtCi) {
		writeChar(Pos.CWCACQ_CIAR_EFF_DT_CI, cwcacqCiarEffDtCi);
	}

	/**Original name: CWCACQ-CIAR-EFF-DT-CI<br>*/
	public char getCwcacqCiarEffDtCi() {
		return readChar(Pos.CWCACQ_CIAR_EFF_DT_CI);
	}

	public void setCwcacqCiarEffDt(String cwcacqCiarEffDt) {
		writeString(Pos.CWCACQ_CIAR_EFF_DT, cwcacqCiarEffDt, Len.CWCACQ_CIAR_EFF_DT);
	}

	/**Original name: CWCACQ-CIAR-EFF-DT<br>*/
	public String getCwcacqCiarEffDt() {
		return readString(Pos.CWCACQ_CIAR_EFF_DT, Len.CWCACQ_CIAR_EFF_DT);
	}

	public void setCwcacqTransProcessDt(String cwcacqTransProcessDt) {
		writeString(Pos.CWCACQ_TRANS_PROCESS_DT, cwcacqTransProcessDt, Len.CWCACQ_TRANS_PROCESS_DT);
	}

	/**Original name: CWCACQ-TRANS-PROCESS-DT<br>*/
	public String getCwcacqTransProcessDt() {
		return readString(Pos.CWCACQ_TRANS_PROCESS_DT, Len.CWCACQ_TRANS_PROCESS_DT);
	}

	public void setCwcacqUserIdCi(char cwcacqUserIdCi) {
		writeChar(Pos.CWCACQ_USER_ID_CI, cwcacqUserIdCi);
	}

	/**Original name: CWCACQ-USER-ID-CI<br>*/
	public char getCwcacqUserIdCi() {
		return readChar(Pos.CWCACQ_USER_ID_CI);
	}

	public void setCwcacqUserId(String cwcacqUserId) {
		writeString(Pos.CWCACQ_USER_ID, cwcacqUserId, Len.CWCACQ_USER_ID);
	}

	/**Original name: CWCACQ-USER-ID<br>*/
	public String getCwcacqUserId() {
		return readString(Pos.CWCACQ_USER_ID, Len.CWCACQ_USER_ID);
	}

	public void setCwcacqTerminalIdCi(char cwcacqTerminalIdCi) {
		writeChar(Pos.CWCACQ_TERMINAL_ID_CI, cwcacqTerminalIdCi);
	}

	/**Original name: CWCACQ-TERMINAL-ID-CI<br>*/
	public char getCwcacqTerminalIdCi() {
		return readChar(Pos.CWCACQ_TERMINAL_ID_CI);
	}

	public void setCwcacqTerminalId(String cwcacqTerminalId) {
		writeString(Pos.CWCACQ_TERMINAL_ID, cwcacqTerminalId, Len.CWCACQ_TERMINAL_ID);
	}

	/**Original name: CWCACQ-TERMINAL-ID<br>*/
	public String getCwcacqTerminalId() {
		return readString(Pos.CWCACQ_TERMINAL_ID, Len.CWCACQ_TERMINAL_ID);
	}

	public void setCwcacqCicaAdr1Ci(char cwcacqCicaAdr1Ci) {
		writeChar(Pos.CWCACQ_CICA_ADR1_CI, cwcacqCicaAdr1Ci);
	}

	/**Original name: CWCACQ-CICA-ADR-1-CI<br>*/
	public char getCwcacqCicaAdr1Ci() {
		return readChar(Pos.CWCACQ_CICA_ADR1_CI);
	}

	public void setCwcacqCicaAdr1(String cwcacqCicaAdr1) {
		writeString(Pos.CWCACQ_CICA_ADR1, cwcacqCicaAdr1, Len.CWCACQ_CICA_ADR1);
	}

	/**Original name: CWCACQ-CICA-ADR-1<br>*/
	public String getCwcacqCicaAdr1() {
		return readString(Pos.CWCACQ_CICA_ADR1, Len.CWCACQ_CICA_ADR1);
	}

	public void setCwcacqCicaAdr2Ci(char cwcacqCicaAdr2Ci) {
		writeChar(Pos.CWCACQ_CICA_ADR2_CI, cwcacqCicaAdr2Ci);
	}

	/**Original name: CWCACQ-CICA-ADR-2-CI<br>*/
	public char getCwcacqCicaAdr2Ci() {
		return readChar(Pos.CWCACQ_CICA_ADR2_CI);
	}

	public void setCwcacqCicaAdr2Ni(char cwcacqCicaAdr2Ni) {
		writeChar(Pos.CWCACQ_CICA_ADR2_NI, cwcacqCicaAdr2Ni);
	}

	/**Original name: CWCACQ-CICA-ADR-2-NI<br>*/
	public char getCwcacqCicaAdr2Ni() {
		return readChar(Pos.CWCACQ_CICA_ADR2_NI);
	}

	public void setCwcacqCicaAdr2(String cwcacqCicaAdr2) {
		writeString(Pos.CWCACQ_CICA_ADR2, cwcacqCicaAdr2, Len.CWCACQ_CICA_ADR2);
	}

	/**Original name: CWCACQ-CICA-ADR-2<br>*/
	public String getCwcacqCicaAdr2() {
		return readString(Pos.CWCACQ_CICA_ADR2, Len.CWCACQ_CICA_ADR2);
	}

	public void setCwcacqCicaCitNmCi(char cwcacqCicaCitNmCi) {
		writeChar(Pos.CWCACQ_CICA_CIT_NM_CI, cwcacqCicaCitNmCi);
	}

	/**Original name: CWCACQ-CICA-CIT-NM-CI<br>*/
	public char getCwcacqCicaCitNmCi() {
		return readChar(Pos.CWCACQ_CICA_CIT_NM_CI);
	}

	public void setCwcacqCicaCitNm(String cwcacqCicaCitNm) {
		writeString(Pos.CWCACQ_CICA_CIT_NM, cwcacqCicaCitNm, Len.CWCACQ_CICA_CIT_NM);
	}

	/**Original name: CWCACQ-CICA-CIT-NM<br>*/
	public String getCwcacqCicaCitNm() {
		return readString(Pos.CWCACQ_CICA_CIT_NM, Len.CWCACQ_CICA_CIT_NM);
	}

	public void setCwcacqCicaCtyCi(char cwcacqCicaCtyCi) {
		writeChar(Pos.CWCACQ_CICA_CTY_CI, cwcacqCicaCtyCi);
	}

	/**Original name: CWCACQ-CICA-CTY-CI<br>*/
	public char getCwcacqCicaCtyCi() {
		return readChar(Pos.CWCACQ_CICA_CTY_CI);
	}

	public void setCwcacqCicaCtyNi(char cwcacqCicaCtyNi) {
		writeChar(Pos.CWCACQ_CICA_CTY_NI, cwcacqCicaCtyNi);
	}

	/**Original name: CWCACQ-CICA-CTY-NI<br>*/
	public char getCwcacqCicaCtyNi() {
		return readChar(Pos.CWCACQ_CICA_CTY_NI);
	}

	public void setCwcacqCicaCty(String cwcacqCicaCty) {
		writeString(Pos.CWCACQ_CICA_CTY, cwcacqCicaCty, Len.CWCACQ_CICA_CTY);
	}

	/**Original name: CWCACQ-CICA-CTY<br>*/
	public String getCwcacqCicaCty() {
		return readString(Pos.CWCACQ_CICA_CTY, Len.CWCACQ_CICA_CTY);
	}

	public void setCwcacqStCdCi(char cwcacqStCdCi) {
		writeChar(Pos.CWCACQ_ST_CD_CI, cwcacqStCdCi);
	}

	/**Original name: CWCACQ-ST-CD-CI<br>*/
	public char getCwcacqStCdCi() {
		return readChar(Pos.CWCACQ_ST_CD_CI);
	}

	public void setCwcacqStCd(String cwcacqStCd) {
		writeString(Pos.CWCACQ_ST_CD, cwcacqStCd, Len.CWCACQ_ST_CD);
	}

	/**Original name: CWCACQ-ST-CD<br>*/
	public String getCwcacqStCd() {
		return readString(Pos.CWCACQ_ST_CD, Len.CWCACQ_ST_CD);
	}

	public void setCwcacqCicaPstCdCi(char cwcacqCicaPstCdCi) {
		writeChar(Pos.CWCACQ_CICA_PST_CD_CI, cwcacqCicaPstCdCi);
	}

	/**Original name: CWCACQ-CICA-PST-CD-CI<br>*/
	public char getCwcacqCicaPstCdCi() {
		return readChar(Pos.CWCACQ_CICA_PST_CD_CI);
	}

	public void setCwcacqCicaPstCd(String cwcacqCicaPstCd) {
		writeString(Pos.CWCACQ_CICA_PST_CD, cwcacqCicaPstCd, Len.CWCACQ_CICA_PST_CD);
	}

	/**Original name: CWCACQ-CICA-PST-CD<br>*/
	public String getCwcacqCicaPstCd() {
		return readString(Pos.CWCACQ_CICA_PST_CD, Len.CWCACQ_CICA_PST_CD);
	}

	public void setCwcacqCtrCdCi(char cwcacqCtrCdCi) {
		writeChar(Pos.CWCACQ_CTR_CD_CI, cwcacqCtrCdCi);
	}

	/**Original name: CWCACQ-CTR-CD-CI<br>*/
	public char getCwcacqCtrCdCi() {
		return readChar(Pos.CWCACQ_CTR_CD_CI);
	}

	public void setCwcacqCtrCd(String cwcacqCtrCd) {
		writeString(Pos.CWCACQ_CTR_CD, cwcacqCtrCd, Len.CWCACQ_CTR_CD);
	}

	/**Original name: CWCACQ-CTR-CD<br>*/
	public String getCwcacqCtrCd() {
		return readString(Pos.CWCACQ_CTR_CD, Len.CWCACQ_CTR_CD);
	}

	public void setCwcacqCicaAddAdrIndCi(char cwcacqCicaAddAdrIndCi) {
		writeChar(Pos.CWCACQ_CICA_ADD_ADR_IND_CI, cwcacqCicaAddAdrIndCi);
	}

	/**Original name: CWCACQ-CICA-ADD-ADR-IND-CI<br>*/
	public char getCwcacqCicaAddAdrIndCi() {
		return readChar(Pos.CWCACQ_CICA_ADD_ADR_IND_CI);
	}

	public void setCwcacqCicaAddAdrIndNi(char cwcacqCicaAddAdrIndNi) {
		writeChar(Pos.CWCACQ_CICA_ADD_ADR_IND_NI, cwcacqCicaAddAdrIndNi);
	}

	/**Original name: CWCACQ-CICA-ADD-ADR-IND-NI<br>*/
	public char getCwcacqCicaAddAdrIndNi() {
		return readChar(Pos.CWCACQ_CICA_ADD_ADR_IND_NI);
	}

	public void setCwcacqCicaAddAdrInd(char cwcacqCicaAddAdrInd) {
		writeChar(Pos.CWCACQ_CICA_ADD_ADR_IND, cwcacqCicaAddAdrInd);
	}

	/**Original name: CWCACQ-CICA-ADD-ADR-IND<br>*/
	public char getCwcacqCicaAddAdrInd() {
		return readChar(Pos.CWCACQ_CICA_ADD_ADR_IND);
	}

	public void setCwcacqAddrStatusCdCi(char cwcacqAddrStatusCdCi) {
		writeChar(Pos.CWCACQ_ADDR_STATUS_CD_CI, cwcacqAddrStatusCdCi);
	}

	/**Original name: CWCACQ-ADDR-STATUS-CD-CI<br>*/
	public char getCwcacqAddrStatusCdCi() {
		return readChar(Pos.CWCACQ_ADDR_STATUS_CD_CI);
	}

	public void setCwcacqAddrStatusCd(char cwcacqAddrStatusCd) {
		writeChar(Pos.CWCACQ_ADDR_STATUS_CD, cwcacqAddrStatusCd);
	}

	/**Original name: CWCACQ-ADDR-STATUS-CD<br>*/
	public char getCwcacqAddrStatusCd() {
		return readChar(Pos.CWCACQ_ADDR_STATUS_CD);
	}

	public void setCwcacqAddNmIndCi(char cwcacqAddNmIndCi) {
		writeChar(Pos.CWCACQ_ADD_NM_IND_CI, cwcacqAddNmIndCi);
	}

	/**Original name: CWCACQ-ADD-NM-IND-CI<br>*/
	public char getCwcacqAddNmIndCi() {
		return readChar(Pos.CWCACQ_ADD_NM_IND_CI);
	}

	public void setCwcacqAddNmInd(char cwcacqAddNmInd) {
		writeChar(Pos.CWCACQ_ADD_NM_IND, cwcacqAddNmInd);
	}

	/**Original name: CWCACQ-ADD-NM-IND<br>*/
	public char getCwcacqAddNmInd() {
		return readChar(Pos.CWCACQ_ADD_NM_IND);
	}

	public void setCwcacqAdrTypCdCi(char cwcacqAdrTypCdCi) {
		writeChar(Pos.CWCACQ_ADR_TYP_CD_CI, cwcacqAdrTypCdCi);
	}

	/**Original name: CWCACQ-ADR-TYP-CD-CI<br>*/
	public char getCwcacqAdrTypCdCi() {
		return readChar(Pos.CWCACQ_ADR_TYP_CD_CI);
	}

	public void setCwcacqAdrTypCd(String cwcacqAdrTypCd) {
		writeString(Pos.CWCACQ_ADR_TYP_CD, cwcacqAdrTypCd, Len.CWCACQ_ADR_TYP_CD);
	}

	/**Original name: CWCACQ-ADR-TYP-CD<br>*/
	public String getCwcacqAdrTypCd() {
		return readString(Pos.CWCACQ_ADR_TYP_CD, Len.CWCACQ_ADR_TYP_CD);
	}

	public void setCwcacqTchObjectKeyCi(char cwcacqTchObjectKeyCi) {
		writeChar(Pos.CWCACQ_TCH_OBJECT_KEY_CI, cwcacqTchObjectKeyCi);
	}

	/**Original name: CWCACQ-TCH-OBJECT-KEY-CI<br>*/
	public char getCwcacqTchObjectKeyCi() {
		return readChar(Pos.CWCACQ_TCH_OBJECT_KEY_CI);
	}

	public void setCwcacqTchObjectKey(String cwcacqTchObjectKey) {
		writeString(Pos.CWCACQ_TCH_OBJECT_KEY, cwcacqTchObjectKey, Len.CWCACQ_TCH_OBJECT_KEY);
	}

	/**Original name: CWCACQ-TCH-OBJECT-KEY<br>*/
	public String getCwcacqTchObjectKey() {
		return readString(Pos.CWCACQ_TCH_OBJECT_KEY, Len.CWCACQ_TCH_OBJECT_KEY);
	}

	public void setCwcacqCiarExpDtCi(String cwcacqCiarExpDtCi) {
		writeString(Pos.CWCACQ_CIAR_EXP_DT_CI, cwcacqCiarExpDtCi, Len.CWCACQ_CIAR_EXP_DT_CI);
	}

	/**Original name: CWCACQ-CIAR-EXP-DT-CI<br>*/
	public String getCwcacqCiarExpDtCi() {
		return readString(Pos.CWCACQ_CIAR_EXP_DT_CI, Len.CWCACQ_CIAR_EXP_DT_CI);
	}

	public void setCwcacqCiarExpDt(String cwcacqCiarExpDt) {
		writeString(Pos.CWCACQ_CIAR_EXP_DT, cwcacqCiarExpDt, Len.CWCACQ_CIAR_EXP_DT);
	}

	/**Original name: CWCACQ-CIAR-EXP-DT<br>*/
	public String getCwcacqCiarExpDt() {
		return readString(Pos.CWCACQ_CIAR_EXP_DT, Len.CWCACQ_CIAR_EXP_DT);
	}

	public void setCwcacqCiarSerAdr1TxtCi(char cwcacqCiarSerAdr1TxtCi) {
		writeChar(Pos.CWCACQ_CIAR_SER_ADR1_TXT_CI, cwcacqCiarSerAdr1TxtCi);
	}

	/**Original name: CWCACQ-CIAR-SER-ADR-1-TXT-CI<br>*/
	public char getCwcacqCiarSerAdr1TxtCi() {
		return readChar(Pos.CWCACQ_CIAR_SER_ADR1_TXT_CI);
	}

	public void setCwcacqCiarSerAdr1Txt(String cwcacqCiarSerAdr1Txt) {
		writeString(Pos.CWCACQ_CIAR_SER_ADR1_TXT, cwcacqCiarSerAdr1Txt, Len.CWCACQ_CIAR_SER_ADR1_TXT);
	}

	/**Original name: CWCACQ-CIAR-SER-ADR-1-TXT<br>*/
	public String getCwcacqCiarSerAdr1Txt() {
		return readString(Pos.CWCACQ_CIAR_SER_ADR1_TXT, Len.CWCACQ_CIAR_SER_ADR1_TXT);
	}

	public void setCwcacqCiarSerCitNmCi(char cwcacqCiarSerCitNmCi) {
		writeChar(Pos.CWCACQ_CIAR_SER_CIT_NM_CI, cwcacqCiarSerCitNmCi);
	}

	/**Original name: CWCACQ-CIAR-SER-CIT-NM-CI<br>*/
	public char getCwcacqCiarSerCitNmCi() {
		return readChar(Pos.CWCACQ_CIAR_SER_CIT_NM_CI);
	}

	public void setCwcacqCiarSerCitNm(String cwcacqCiarSerCitNm) {
		writeString(Pos.CWCACQ_CIAR_SER_CIT_NM, cwcacqCiarSerCitNm, Len.CWCACQ_CIAR_SER_CIT_NM);
	}

	/**Original name: CWCACQ-CIAR-SER-CIT-NM<br>*/
	public String getCwcacqCiarSerCitNm() {
		return readString(Pos.CWCACQ_CIAR_SER_CIT_NM, Len.CWCACQ_CIAR_SER_CIT_NM);
	}

	public void setCwcacqCiarSerStCdCi(char cwcacqCiarSerStCdCi) {
		writeChar(Pos.CWCACQ_CIAR_SER_ST_CD_CI, cwcacqCiarSerStCdCi);
	}

	/**Original name: CWCACQ-CIAR-SER-ST-CD-CI<br>*/
	public char getCwcacqCiarSerStCdCi() {
		return readChar(Pos.CWCACQ_CIAR_SER_ST_CD_CI);
	}

	public void setCwcacqCiarSerStCd(String cwcacqCiarSerStCd) {
		writeString(Pos.CWCACQ_CIAR_SER_ST_CD, cwcacqCiarSerStCd, Len.CWCACQ_CIAR_SER_ST_CD);
	}

	/**Original name: CWCACQ-CIAR-SER-ST-CD<br>*/
	public String getCwcacqCiarSerStCd() {
		return readString(Pos.CWCACQ_CIAR_SER_ST_CD, Len.CWCACQ_CIAR_SER_ST_CD);
	}

	public void setCwcacqCiarSerPstCdCi(char cwcacqCiarSerPstCdCi) {
		writeChar(Pos.CWCACQ_CIAR_SER_PST_CD_CI, cwcacqCiarSerPstCdCi);
	}

	/**Original name: CWCACQ-CIAR-SER-PST-CD-CI<br>*/
	public char getCwcacqCiarSerPstCdCi() {
		return readChar(Pos.CWCACQ_CIAR_SER_PST_CD_CI);
	}

	public void setCwcacqCiarSerPstCd(String cwcacqCiarSerPstCd) {
		writeString(Pos.CWCACQ_CIAR_SER_PST_CD, cwcacqCiarSerPstCd, Len.CWCACQ_CIAR_SER_PST_CD);
	}

	/**Original name: CWCACQ-CIAR-SER-PST-CD<br>*/
	public String getCwcacqCiarSerPstCd() {
		return readString(Pos.CWCACQ_CIAR_SER_PST_CD, Len.CWCACQ_CIAR_SER_PST_CD);
	}

	public void setCwcacqRelStatusCdCi(char cwcacqRelStatusCdCi) {
		writeChar(Pos.CWCACQ_REL_STATUS_CD_CI, cwcacqRelStatusCdCi);
	}

	/**Original name: CWCACQ-REL-STATUS-CD-CI<br>*/
	public char getCwcacqRelStatusCdCi() {
		return readChar(Pos.CWCACQ_REL_STATUS_CD_CI);
	}

	public void setCwcacqRelStatusCd(char cwcacqRelStatusCd) {
		writeChar(Pos.CWCACQ_REL_STATUS_CD, cwcacqRelStatusCd);
	}

	/**Original name: CWCACQ-REL-STATUS-CD<br>*/
	public char getCwcacqRelStatusCd() {
		return readChar(Pos.CWCACQ_REL_STATUS_CD);
	}

	public void setCwcacqEffAcyTsCi(char cwcacqEffAcyTsCi) {
		writeChar(Pos.CWCACQ_EFF_ACY_TS_CI, cwcacqEffAcyTsCi);
	}

	/**Original name: CWCACQ-EFF-ACY-TS-CI<br>*/
	public char getCwcacqEffAcyTsCi() {
		return readChar(Pos.CWCACQ_EFF_ACY_TS_CI);
	}

	public void setCwcacqEffAcyTs(String cwcacqEffAcyTs) {
		writeString(Pos.CWCACQ_EFF_ACY_TS, cwcacqEffAcyTs, Len.CWCACQ_EFF_ACY_TS);
	}

	/**Original name: CWCACQ-EFF-ACY-TS<br>*/
	public String getCwcacqEffAcyTs() {
		return readString(Pos.CWCACQ_EFF_ACY_TS, Len.CWCACQ_EFF_ACY_TS);
	}

	public void setCwcacqCiarExpAcyTsCi(char cwcacqCiarExpAcyTsCi) {
		writeChar(Pos.CWCACQ_CIAR_EXP_ACY_TS_CI, cwcacqCiarExpAcyTsCi);
	}

	/**Original name: CWCACQ-CIAR-EXP-ACY-TS-CI<br>*/
	public char getCwcacqCiarExpAcyTsCi() {
		return readChar(Pos.CWCACQ_CIAR_EXP_ACY_TS_CI);
	}

	public void setCwcacqCiarExpAcyTs(String cwcacqCiarExpAcyTs) {
		writeString(Pos.CWCACQ_CIAR_EXP_ACY_TS, cwcacqCiarExpAcyTs, Len.CWCACQ_CIAR_EXP_ACY_TS);
	}

	/**Original name: CWCACQ-CIAR-EXP-ACY-TS<br>*/
	public String getCwcacqCiarExpAcyTs() {
		return readString(Pos.CWCACQ_CIAR_EXP_ACY_TS, Len.CWCACQ_CIAR_EXP_ACY_TS);
	}

	public void setCwcacqMoreRowsSw(char cwcacqMoreRowsSw) {
		writeChar(Pos.CWCACQ_MORE_ROWS_SW, cwcacqMoreRowsSw);
	}

	/**Original name: CWCACQ-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	public char getCwcacqMoreRowsSw() {
		return readChar(Pos.CWCACQ_MORE_ROWS_SW);
	}

	public void setCwcacqCicaShwObjKeyCi(char cwcacqCicaShwObjKeyCi) {
		writeChar(Pos.CWCACQ_CICA_SHW_OBJ_KEY_CI, cwcacqCicaShwObjKeyCi);
	}

	/**Original name: CWCACQ-CICA-SHW-OBJ-KEY-CI<br>
	 * <pre>* VARIABLE FOR SHW-OBJ-KEY(SHOWABLE OBJECT KEY)</pre>*/
	public char getCwcacqCicaShwObjKeyCi() {
		return readChar(Pos.CWCACQ_CICA_SHW_OBJ_KEY_CI);
	}

	public void setCwcacqCicaShwObjKey(String cwcacqCicaShwObjKey) {
		writeString(Pos.CWCACQ_CICA_SHW_OBJ_KEY, cwcacqCicaShwObjKey, Len.CWCACQ_CICA_SHW_OBJ_KEY);
	}

	/**Original name: CWCACQ-CICA-SHW-OBJ-KEY<br>*/
	public String getCwcacqCicaShwObjKey() {
		return readString(Pos.CWCACQ_CICA_SHW_OBJ_KEY, Len.CWCACQ_CICA_SHW_OBJ_KEY);
	}

	public void setCwcacqBusObjNm(String cwcacqBusObjNm) {
		writeString(Pos.CWCACQ_BUS_OBJ_NM, cwcacqBusObjNm, Len.CWCACQ_BUS_OBJ_NM);
	}

	/**Original name: CWCACQ-BUS-OBJ-NM<br>*/
	public String getCwcacqBusObjNm() {
		return readString(Pos.CWCACQ_BUS_OBJ_NM, Len.CWCACQ_BUS_OBJ_NM);
	}

	public void setCwcacqStDesc(String cwcacqStDesc) {
		writeString(Pos.CWCACQ_ST_DESC, cwcacqStDesc, Len.CWCACQ_ST_DESC);
	}

	/**Original name: CWCACQ-ST-DESC<br>*/
	public String getCwcacqStDesc() {
		return readString(Pos.CWCACQ_ST_DESC, Len.CWCACQ_ST_DESC);
	}

	public void setCwcacqAdrTypDesc(String cwcacqAdrTypDesc) {
		writeString(Pos.CWCACQ_ADR_TYP_DESC, cwcacqAdrTypDesc, Len.CWCACQ_ADR_TYP_DESC);
	}

	/**Original name: CWCACQ-ADR-TYP-DESC<br>*/
	public String getCwcacqAdrTypDesc() {
		return readString(Pos.CWCACQ_ADR_TYP_DESC, Len.CWCACQ_ADR_TYP_DESC);
	}

	public void setCw06qCltCltRelationCsumFormatted(String cw06qCltCltRelationCsum) {
		writeString(Pos.CW06Q_CLT_CLT_RELATION_CSUM, Trunc.toUnsignedNumeric(cw06qCltCltRelationCsum, Len.CW06Q_CLT_CLT_RELATION_CSUM),
				Len.CW06Q_CLT_CLT_RELATION_CSUM);
	}

	/**Original name: CW06Q-CLT-CLT-RELATION-CSUM<br>*/
	public int getCw06qCltCltRelationCsum() {
		return readNumDispUnsignedInt(Pos.CW06Q_CLT_CLT_RELATION_CSUM, Len.CW06Q_CLT_CLT_RELATION_CSUM);
	}

	public void setCw06qClientIdKcre(String cw06qClientIdKcre) {
		writeString(Pos.CW06Q_CLIENT_ID_KCRE, cw06qClientIdKcre, Len.CW06Q_CLIENT_ID_KCRE);
	}

	/**Original name: CW06Q-CLIENT-ID-KCRE<br>*/
	public String getCw06qClientIdKcre() {
		return readString(Pos.CW06Q_CLIENT_ID_KCRE, Len.CW06Q_CLIENT_ID_KCRE);
	}

	public void setCw06qCltTypCdKcre(String cw06qCltTypCdKcre) {
		writeString(Pos.CW06Q_CLT_TYP_CD_KCRE, cw06qCltTypCdKcre, Len.CW06Q_CLT_TYP_CD_KCRE);
	}

	/**Original name: CW06Q-CLT-TYP-CD-KCRE<br>*/
	public String getCw06qCltTypCdKcre() {
		return readString(Pos.CW06Q_CLT_TYP_CD_KCRE, Len.CW06Q_CLT_TYP_CD_KCRE);
	}

	public void setCw06qHistoryVldNbrKcre(String cw06qHistoryVldNbrKcre) {
		writeString(Pos.CW06Q_HISTORY_VLD_NBR_KCRE, cw06qHistoryVldNbrKcre, Len.CW06Q_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW06Q-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw06qHistoryVldNbrKcre() {
		return readString(Pos.CW06Q_HISTORY_VLD_NBR_KCRE, Len.CW06Q_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw06qCicrXrfIdKcre(String cw06qCicrXrfIdKcre) {
		writeString(Pos.CW06Q_CICR_XRF_ID_KCRE, cw06qCicrXrfIdKcre, Len.CW06Q_CICR_XRF_ID_KCRE);
	}

	/**Original name: CW06Q-CICR-XRF-ID-KCRE<br>*/
	public String getCw06qCicrXrfIdKcre() {
		return readString(Pos.CW06Q_CICR_XRF_ID_KCRE, Len.CW06Q_CICR_XRF_ID_KCRE);
	}

	public void setCw06qXrfTypCdKcre(String cw06qXrfTypCdKcre) {
		writeString(Pos.CW06Q_XRF_TYP_CD_KCRE, cw06qXrfTypCdKcre, Len.CW06Q_XRF_TYP_CD_KCRE);
	}

	/**Original name: CW06Q-XRF-TYP-CD-KCRE<br>*/
	public String getCw06qXrfTypCdKcre() {
		return readString(Pos.CW06Q_XRF_TYP_CD_KCRE, Len.CW06Q_XRF_TYP_CD_KCRE);
	}

	public void setCw06qCicrEffDtKcre(String cw06qCicrEffDtKcre) {
		writeString(Pos.CW06Q_CICR_EFF_DT_KCRE, cw06qCicrEffDtKcre, Len.CW06Q_CICR_EFF_DT_KCRE);
	}

	/**Original name: CW06Q-CICR-EFF-DT-KCRE<br>*/
	public String getCw06qCicrEffDtKcre() {
		return readString(Pos.CW06Q_CICR_EFF_DT_KCRE, Len.CW06Q_CICR_EFF_DT_KCRE);
	}

	public void setCw06qTransProcessDt(String cw06qTransProcessDt) {
		writeString(Pos.CW06Q_TRANS_PROCESS_DT, cw06qTransProcessDt, Len.CW06Q_TRANS_PROCESS_DT);
	}

	/**Original name: CW06Q-TRANS-PROCESS-DT<br>*/
	public String getCw06qTransProcessDt() {
		return readString(Pos.CW06Q_TRANS_PROCESS_DT, Len.CW06Q_TRANS_PROCESS_DT);
	}

	public void setCw06qClientIdCi(char cw06qClientIdCi) {
		writeChar(Pos.CW06Q_CLIENT_ID_CI, cw06qClientIdCi);
	}

	/**Original name: CW06Q-CLIENT-ID-CI<br>*/
	public char getCw06qClientIdCi() {
		return readChar(Pos.CW06Q_CLIENT_ID_CI);
	}

	public void setCw06qClientId(String cw06qClientId) {
		writeString(Pos.CW06Q_CLIENT_ID, cw06qClientId, Len.CW06Q_CLIENT_ID);
	}

	/**Original name: CW06Q-CLIENT-ID<br>*/
	public String getCw06qClientId() {
		return readString(Pos.CW06Q_CLIENT_ID, Len.CW06Q_CLIENT_ID);
	}

	public void setCw06qCltTypCdCi(char cw06qCltTypCdCi) {
		writeChar(Pos.CW06Q_CLT_TYP_CD_CI, cw06qCltTypCdCi);
	}

	/**Original name: CW06Q-CLT-TYP-CD-CI<br>*/
	public char getCw06qCltTypCdCi() {
		return readChar(Pos.CW06Q_CLT_TYP_CD_CI);
	}

	public void setCw06qCltTypCd(String cw06qCltTypCd) {
		writeString(Pos.CW06Q_CLT_TYP_CD, cw06qCltTypCd, Len.CW06Q_CLT_TYP_CD);
	}

	/**Original name: CW06Q-CLT-TYP-CD<br>*/
	public String getCw06qCltTypCd() {
		return readString(Pos.CW06Q_CLT_TYP_CD, Len.CW06Q_CLT_TYP_CD);
	}

	public void setCw06qHistoryVldNbrCi(char cw06qHistoryVldNbrCi) {
		writeChar(Pos.CW06Q_HISTORY_VLD_NBR_CI, cw06qHistoryVldNbrCi);
	}

	/**Original name: CW06Q-HISTORY-VLD-NBR-CI<br>*/
	public char getCw06qHistoryVldNbrCi() {
		return readChar(Pos.CW06Q_HISTORY_VLD_NBR_CI);
	}

	public void setCw06qHistoryVldNbrSignedFormatted(String cw06qHistoryVldNbrSigned) {
		writeString(Pos.CW06Q_HISTORY_VLD_NBR_SIGNED, PicFormatter.display("-9(5)").format(cw06qHistoryVldNbrSigned).toString(),
				Len.CW06Q_HISTORY_VLD_NBR_SIGNED);
	}

	/**Original name: CW06Q-HISTORY-VLD-NBR-SIGNED<br>*/
	public long getCw06qHistoryVldNbrSigned() {
		return PicParser.display("-9(5)").parseLong(readString(Pos.CW06Q_HISTORY_VLD_NBR_SIGNED, Len.CW06Q_HISTORY_VLD_NBR_SIGNED));
	}

	public void setCw06qCicrXrfIdCi(char cw06qCicrXrfIdCi) {
		writeChar(Pos.CW06Q_CICR_XRF_ID_CI, cw06qCicrXrfIdCi);
	}

	/**Original name: CW06Q-CICR-XRF-ID-CI<br>*/
	public char getCw06qCicrXrfIdCi() {
		return readChar(Pos.CW06Q_CICR_XRF_ID_CI);
	}

	public void setCw06qCicrXrfId(String cw06qCicrXrfId) {
		writeString(Pos.CW06Q_CICR_XRF_ID, cw06qCicrXrfId, Len.CW06Q_CICR_XRF_ID);
	}

	/**Original name: CW06Q-CICR-XRF-ID<br>*/
	public String getCw06qCicrXrfId() {
		return readString(Pos.CW06Q_CICR_XRF_ID, Len.CW06Q_CICR_XRF_ID);
	}

	public void setCw06qXrfTypCdCi(char cw06qXrfTypCdCi) {
		writeChar(Pos.CW06Q_XRF_TYP_CD_CI, cw06qXrfTypCdCi);
	}

	/**Original name: CW06Q-XRF-TYP-CD-CI<br>*/
	public char getCw06qXrfTypCdCi() {
		return readChar(Pos.CW06Q_XRF_TYP_CD_CI);
	}

	public void setCw06qXrfTypCd(String cw06qXrfTypCd) {
		writeString(Pos.CW06Q_XRF_TYP_CD, cw06qXrfTypCd, Len.CW06Q_XRF_TYP_CD);
	}

	/**Original name: CW06Q-XRF-TYP-CD<br>*/
	public String getCw06qXrfTypCd() {
		return readString(Pos.CW06Q_XRF_TYP_CD, Len.CW06Q_XRF_TYP_CD);
	}

	public void setCw06qCicrEffDtCi(char cw06qCicrEffDtCi) {
		writeChar(Pos.CW06Q_CICR_EFF_DT_CI, cw06qCicrEffDtCi);
	}

	/**Original name: CW06Q-CICR-EFF-DT-CI<br>*/
	public char getCw06qCicrEffDtCi() {
		return readChar(Pos.CW06Q_CICR_EFF_DT_CI);
	}

	public void setCw06qCicrEffDt(String cw06qCicrEffDt) {
		writeString(Pos.CW06Q_CICR_EFF_DT, cw06qCicrEffDt, Len.CW06Q_CICR_EFF_DT);
	}

	/**Original name: CW06Q-CICR-EFF-DT<br>*/
	public String getCw06qCicrEffDt() {
		return readString(Pos.CW06Q_CICR_EFF_DT, Len.CW06Q_CICR_EFF_DT);
	}

	public void setCw06qCicrExpDtCi(char cw06qCicrExpDtCi) {
		writeChar(Pos.CW06Q_CICR_EXP_DT_CI, cw06qCicrExpDtCi);
	}

	/**Original name: CW06Q-CICR-EXP-DT-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw06qCicrExpDtCi() {
		return readChar(Pos.CW06Q_CICR_EXP_DT_CI);
	}

	public void setCw06qCicrExpDt(String cw06qCicrExpDt) {
		writeString(Pos.CW06Q_CICR_EXP_DT, cw06qCicrExpDt, Len.CW06Q_CICR_EXP_DT);
	}

	/**Original name: CW06Q-CICR-EXP-DT<br>*/
	public String getCw06qCicrExpDt() {
		return readString(Pos.CW06Q_CICR_EXP_DT, Len.CW06Q_CICR_EXP_DT);
	}

	public void setCw06qCicrNbrOprStrsCi(char cw06qCicrNbrOprStrsCi) {
		writeChar(Pos.CW06Q_CICR_NBR_OPR_STRS_CI, cw06qCicrNbrOprStrsCi);
	}

	/**Original name: CW06Q-CICR-NBR-OPR-STRS-CI<br>*/
	public char getCw06qCicrNbrOprStrsCi() {
		return readChar(Pos.CW06Q_CICR_NBR_OPR_STRS_CI);
	}

	public void setCw06qCicrNbrOprStrsNi(char cw06qCicrNbrOprStrsNi) {
		writeChar(Pos.CW06Q_CICR_NBR_OPR_STRS_NI, cw06qCicrNbrOprStrsNi);
	}

	/**Original name: CW06Q-CICR-NBR-OPR-STRS-NI<br>*/
	public char getCw06qCicrNbrOprStrsNi() {
		return readChar(Pos.CW06Q_CICR_NBR_OPR_STRS_NI);
	}

	public void setCw06qCicrNbrOprStrs(String cw06qCicrNbrOprStrs) {
		writeString(Pos.CW06Q_CICR_NBR_OPR_STRS, cw06qCicrNbrOprStrs, Len.CW06Q_CICR_NBR_OPR_STRS);
	}

	/**Original name: CW06Q-CICR-NBR-OPR-STRS<br>*/
	public String getCw06qCicrNbrOprStrs() {
		return readString(Pos.CW06Q_CICR_NBR_OPR_STRS, Len.CW06Q_CICR_NBR_OPR_STRS);
	}

	public void setCw06qUserIdCi(char cw06qUserIdCi) {
		writeChar(Pos.CW06Q_USER_ID_CI, cw06qUserIdCi);
	}

	/**Original name: CW06Q-USER-ID-CI<br>*/
	public char getCw06qUserIdCi() {
		return readChar(Pos.CW06Q_USER_ID_CI);
	}

	public void setCw06qUserId(String cw06qUserId) {
		writeString(Pos.CW06Q_USER_ID, cw06qUserId, Len.CW06Q_USER_ID);
	}

	/**Original name: CW06Q-USER-ID<br>*/
	public String getCw06qUserId() {
		return readString(Pos.CW06Q_USER_ID, Len.CW06Q_USER_ID);
	}

	public void setCw06qStatusCdCi(char cw06qStatusCdCi) {
		writeChar(Pos.CW06Q_STATUS_CD_CI, cw06qStatusCdCi);
	}

	/**Original name: CW06Q-STATUS-CD-CI<br>*/
	public char getCw06qStatusCdCi() {
		return readChar(Pos.CW06Q_STATUS_CD_CI);
	}

	public void setCw06qStatusCd(char cw06qStatusCd) {
		writeChar(Pos.CW06Q_STATUS_CD, cw06qStatusCd);
	}

	/**Original name: CW06Q-STATUS-CD<br>*/
	public char getCw06qStatusCd() {
		return readChar(Pos.CW06Q_STATUS_CD);
	}

	public void setCw06qTerminalIdCi(char cw06qTerminalIdCi) {
		writeChar(Pos.CW06Q_TERMINAL_ID_CI, cw06qTerminalIdCi);
	}

	/**Original name: CW06Q-TERMINAL-ID-CI<br>*/
	public char getCw06qTerminalIdCi() {
		return readChar(Pos.CW06Q_TERMINAL_ID_CI);
	}

	public void setCw06qTerminalId(String cw06qTerminalId) {
		writeString(Pos.CW06Q_TERMINAL_ID, cw06qTerminalId, Len.CW06Q_TERMINAL_ID);
	}

	/**Original name: CW06Q-TERMINAL-ID<br>*/
	public String getCw06qTerminalId() {
		return readString(Pos.CW06Q_TERMINAL_ID, Len.CW06Q_TERMINAL_ID);
	}

	public void setCw06qCicrEffAcyTsCi(char cw06qCicrEffAcyTsCi) {
		writeChar(Pos.CW06Q_CICR_EFF_ACY_TS_CI, cw06qCicrEffAcyTsCi);
	}

	/**Original name: CW06Q-CICR-EFF-ACY-TS-CI<br>*/
	public char getCw06qCicrEffAcyTsCi() {
		return readChar(Pos.CW06Q_CICR_EFF_ACY_TS_CI);
	}

	public void setCw06qCicrEffAcyTs(String cw06qCicrEffAcyTs) {
		writeString(Pos.CW06Q_CICR_EFF_ACY_TS, cw06qCicrEffAcyTs, Len.CW06Q_CICR_EFF_ACY_TS);
	}

	/**Original name: CW06Q-CICR-EFF-ACY-TS<br>*/
	public String getCw06qCicrEffAcyTs() {
		return readString(Pos.CW06Q_CICR_EFF_ACY_TS, Len.CW06Q_CICR_EFF_ACY_TS);
	}

	public void setCw06qCicrExpAcyTsCi(char cw06qCicrExpAcyTsCi) {
		writeChar(Pos.CW06Q_CICR_EXP_ACY_TS_CI, cw06qCicrExpAcyTsCi);
	}

	/**Original name: CW06Q-CICR-EXP-ACY-TS-CI<br>*/
	public char getCw06qCicrExpAcyTsCi() {
		return readChar(Pos.CW06Q_CICR_EXP_ACY_TS_CI);
	}

	public void setCw06qCicrExpAcyTs(String cw06qCicrExpAcyTs) {
		writeString(Pos.CW06Q_CICR_EXP_ACY_TS, cw06qCicrExpAcyTs, Len.CW06Q_CICR_EXP_ACY_TS);
	}

	/**Original name: CW06Q-CICR-EXP-ACY-TS<br>*/
	public String getCw06qCicrExpAcyTs() {
		return readString(Pos.CW06Q_CICR_EXP_ACY_TS, Len.CW06Q_CICR_EXP_ACY_TS);
	}

	public String getCw08qCltObjRelationRowFormatted() {
		return readFixedString(Pos.CW08Q_CLT_OBJ_RELATION_ROW, Len.CW08Q_CLT_OBJ_RELATION_ROW);
	}

	public void setCw08qCltObjRelationCsumFormatted(String cw08qCltObjRelationCsum) {
		writeString(Pos.CW08Q_CLT_OBJ_RELATION_CSUM, Trunc.toUnsignedNumeric(cw08qCltObjRelationCsum, Len.CW08Q_CLT_OBJ_RELATION_CSUM),
				Len.CW08Q_CLT_OBJ_RELATION_CSUM);
	}

	/**Original name: CW08Q-CLT-OBJ-RELATION-CSUM<br>*/
	public int getCw08qCltObjRelationCsum() {
		return readNumDispUnsignedInt(Pos.CW08Q_CLT_OBJ_RELATION_CSUM, Len.CW08Q_CLT_OBJ_RELATION_CSUM);
	}

	public void setCw08qTchObjectKeyKcre(String cw08qTchObjectKeyKcre) {
		writeString(Pos.CW08Q_TCH_OBJECT_KEY_KCRE, cw08qTchObjectKeyKcre, Len.CW08Q_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CW08Q-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCw08qTchObjectKeyKcre() {
		return readString(Pos.CW08Q_TCH_OBJECT_KEY_KCRE, Len.CW08Q_TCH_OBJECT_KEY_KCRE);
	}

	public void setCw08qHistoryVldNbrKcre(String cw08qHistoryVldNbrKcre) {
		writeString(Pos.CW08Q_HISTORY_VLD_NBR_KCRE, cw08qHistoryVldNbrKcre, Len.CW08Q_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CW08Q-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCw08qHistoryVldNbrKcre() {
		return readString(Pos.CW08Q_HISTORY_VLD_NBR_KCRE, Len.CW08Q_HISTORY_VLD_NBR_KCRE);
	}

	public void setCw08qCiorEffDtKcre(String cw08qCiorEffDtKcre) {
		writeString(Pos.CW08Q_CIOR_EFF_DT_KCRE, cw08qCiorEffDtKcre, Len.CW08Q_CIOR_EFF_DT_KCRE);
	}

	/**Original name: CW08Q-CIOR-EFF-DT-KCRE<br>*/
	public String getCw08qCiorEffDtKcre() {
		return readString(Pos.CW08Q_CIOR_EFF_DT_KCRE, Len.CW08Q_CIOR_EFF_DT_KCRE);
	}

	public void setCw08qObjSysIdKcre(String cw08qObjSysIdKcre) {
		writeString(Pos.CW08Q_OBJ_SYS_ID_KCRE, cw08qObjSysIdKcre, Len.CW08Q_OBJ_SYS_ID_KCRE);
	}

	/**Original name: CW08Q-OBJ-SYS-ID-KCRE<br>*/
	public String getCw08qObjSysIdKcre() {
		return readString(Pos.CW08Q_OBJ_SYS_ID_KCRE, Len.CW08Q_OBJ_SYS_ID_KCRE);
	}

	public void setCw08qCiorObjSeqNbrKcre(String cw08qCiorObjSeqNbrKcre) {
		writeString(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_KCRE, cw08qCiorObjSeqNbrKcre, Len.CW08Q_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	/**Original name: CW08Q-CIOR-OBJ-SEQ-NBR-KCRE<br>*/
	public String getCw08qCiorObjSeqNbrKcre() {
		return readString(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_KCRE, Len.CW08Q_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	public void setCw08qClientIdKcre(String cw08qClientIdKcre) {
		writeString(Pos.CW08Q_CLIENT_ID_KCRE, cw08qClientIdKcre, Len.CW08Q_CLIENT_ID_KCRE);
	}

	/**Original name: CW08Q-CLIENT-ID-KCRE<br>*/
	public String getCw08qClientIdKcre() {
		return readString(Pos.CW08Q_CLIENT_ID_KCRE, Len.CW08Q_CLIENT_ID_KCRE);
	}

	public void setCw08qTransProcessDt(String cw08qTransProcessDt) {
		writeString(Pos.CW08Q_TRANS_PROCESS_DT, cw08qTransProcessDt, Len.CW08Q_TRANS_PROCESS_DT);
	}

	/**Original name: CW08Q-TRANS-PROCESS-DT<br>*/
	public String getCw08qTransProcessDt() {
		return readString(Pos.CW08Q_TRANS_PROCESS_DT, Len.CW08Q_TRANS_PROCESS_DT);
	}

	public void setCw08qTchObjectKeyCi(char cw08qTchObjectKeyCi) {
		writeChar(Pos.CW08Q_TCH_OBJECT_KEY_CI, cw08qTchObjectKeyCi);
	}

	/**Original name: CW08Q-TCH-OBJECT-KEY-CI<br>*/
	public char getCw08qTchObjectKeyCi() {
		return readChar(Pos.CW08Q_TCH_OBJECT_KEY_CI);
	}

	public void setCw08qTchObjectKey(String cw08qTchObjectKey) {
		writeString(Pos.CW08Q_TCH_OBJECT_KEY, cw08qTchObjectKey, Len.CW08Q_TCH_OBJECT_KEY);
	}

	/**Original name: CW08Q-TCH-OBJECT-KEY<br>*/
	public String getCw08qTchObjectKey() {
		return readString(Pos.CW08Q_TCH_OBJECT_KEY, Len.CW08Q_TCH_OBJECT_KEY);
	}

	public void setCw08qHistoryVldNbrCi(char cw08qHistoryVldNbrCi) {
		writeChar(Pos.CW08Q_HISTORY_VLD_NBR_CI, cw08qHistoryVldNbrCi);
	}

	/**Original name: CW08Q-HISTORY-VLD-NBR-CI<br>*/
	public char getCw08qHistoryVldNbrCi() {
		return readChar(Pos.CW08Q_HISTORY_VLD_NBR_CI);
	}

	public void setCw08qHistoryVldNbrSign(char cw08qHistoryVldNbrSign) {
		writeChar(Pos.CW08Q_HISTORY_VLD_NBR_SIGN, cw08qHistoryVldNbrSign);
	}

	/**Original name: CW08Q-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCw08qHistoryVldNbrSign() {
		return readChar(Pos.CW08Q_HISTORY_VLD_NBR_SIGN);
	}

	public void setCw08qHistoryVldNbrFormatted(String cw08qHistoryVldNbr) {
		writeString(Pos.CW08Q_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cw08qHistoryVldNbr, Len.CW08Q_HISTORY_VLD_NBR), Len.CW08Q_HISTORY_VLD_NBR);
	}

	/**Original name: CW08Q-HISTORY-VLD-NBR<br>*/
	public int getCw08qHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CW08Q_HISTORY_VLD_NBR, Len.CW08Q_HISTORY_VLD_NBR);
	}

	public void setCw08qCiorEffDtCi(char cw08qCiorEffDtCi) {
		writeChar(Pos.CW08Q_CIOR_EFF_DT_CI, cw08qCiorEffDtCi);
	}

	/**Original name: CW08Q-CIOR-EFF-DT-CI<br>*/
	public char getCw08qCiorEffDtCi() {
		return readChar(Pos.CW08Q_CIOR_EFF_DT_CI);
	}

	public void setCw08qCiorEffDt(String cw08qCiorEffDt) {
		writeString(Pos.CW08Q_CIOR_EFF_DT, cw08qCiorEffDt, Len.CW08Q_CIOR_EFF_DT);
	}

	/**Original name: CW08Q-CIOR-EFF-DT<br>*/
	public String getCw08qCiorEffDt() {
		return readString(Pos.CW08Q_CIOR_EFF_DT, Len.CW08Q_CIOR_EFF_DT);
	}

	public void setCw08qObjSysIdCi(char cw08qObjSysIdCi) {
		writeChar(Pos.CW08Q_OBJ_SYS_ID_CI, cw08qObjSysIdCi);
	}

	/**Original name: CW08Q-OBJ-SYS-ID-CI<br>*/
	public char getCw08qObjSysIdCi() {
		return readChar(Pos.CW08Q_OBJ_SYS_ID_CI);
	}

	public void setCw08qObjSysId(String cw08qObjSysId) {
		writeString(Pos.CW08Q_OBJ_SYS_ID, cw08qObjSysId, Len.CW08Q_OBJ_SYS_ID);
	}

	/**Original name: CW08Q-OBJ-SYS-ID<br>*/
	public String getCw08qObjSysId() {
		return readString(Pos.CW08Q_OBJ_SYS_ID, Len.CW08Q_OBJ_SYS_ID);
	}

	public void setCw08qCiorObjSeqNbrCi(char cw08qCiorObjSeqNbrCi) {
		writeChar(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_CI, cw08qCiorObjSeqNbrCi);
	}

	/**Original name: CW08Q-CIOR-OBJ-SEQ-NBR-CI<br>*/
	public char getCw08qCiorObjSeqNbrCi() {
		return readChar(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_CI);
	}

	public void setCw08qCiorObjSeqNbrSign(char cw08qCiorObjSeqNbrSign) {
		writeChar(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_SIGN, cw08qCiorObjSeqNbrSign);
	}

	/**Original name: CW08Q-CIOR-OBJ-SEQ-NBR-SIGN<br>*/
	public char getCw08qCiorObjSeqNbrSign() {
		return readChar(Pos.CW08Q_CIOR_OBJ_SEQ_NBR_SIGN);
	}

	public void setCw08qCiorObjSeqNbrFormatted(String cw08qCiorObjSeqNbr) {
		writeString(Pos.CW08Q_CIOR_OBJ_SEQ_NBR, Trunc.toUnsignedNumeric(cw08qCiorObjSeqNbr, Len.CW08Q_CIOR_OBJ_SEQ_NBR), Len.CW08Q_CIOR_OBJ_SEQ_NBR);
	}

	/**Original name: CW08Q-CIOR-OBJ-SEQ-NBR<br>*/
	public int getCw08qCiorObjSeqNbr() {
		return readNumDispUnsignedInt(Pos.CW08Q_CIOR_OBJ_SEQ_NBR, Len.CW08Q_CIOR_OBJ_SEQ_NBR);
	}

	public void setCw08qClientIdCi(char cw08qClientIdCi) {
		writeChar(Pos.CW08Q_CLIENT_ID_CI, cw08qClientIdCi);
	}

	/**Original name: CW08Q-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCw08qClientIdCi() {
		return readChar(Pos.CW08Q_CLIENT_ID_CI);
	}

	public void setCw08qClientId(String cw08qClientId) {
		writeString(Pos.CW08Q_CLIENT_ID, cw08qClientId, Len.CW08Q_CLIENT_ID);
	}

	/**Original name: CW08Q-CLIENT-ID<br>*/
	public String getCw08qClientId() {
		return readString(Pos.CW08Q_CLIENT_ID, Len.CW08Q_CLIENT_ID);
	}

	public void setCw08qRltTypCdCi(char cw08qRltTypCdCi) {
		writeChar(Pos.CW08Q_RLT_TYP_CD_CI, cw08qRltTypCdCi);
	}

	/**Original name: CW08Q-RLT-TYP-CD-CI<br>*/
	public char getCw08qRltTypCdCi() {
		return readChar(Pos.CW08Q_RLT_TYP_CD_CI);
	}

	public void setCw08qRltTypCd(String cw08qRltTypCd) {
		writeString(Pos.CW08Q_RLT_TYP_CD, cw08qRltTypCd, Len.CW08Q_RLT_TYP_CD);
	}

	/**Original name: CW08Q-RLT-TYP-CD<br>*/
	public String getCw08qRltTypCd() {
		return readString(Pos.CW08Q_RLT_TYP_CD, Len.CW08Q_RLT_TYP_CD);
	}

	public void setCw08qObjCdCi(char cw08qObjCdCi) {
		writeChar(Pos.CW08Q_OBJ_CD_CI, cw08qObjCdCi);
	}

	/**Original name: CW08Q-OBJ-CD-CI<br>*/
	public char getCw08qObjCdCi() {
		return readChar(Pos.CW08Q_OBJ_CD_CI);
	}

	public void setCw08qObjCd(String cw08qObjCd) {
		writeString(Pos.CW08Q_OBJ_CD, cw08qObjCd, Len.CW08Q_OBJ_CD);
	}

	/**Original name: CW08Q-OBJ-CD<br>*/
	public String getCw08qObjCd() {
		return readString(Pos.CW08Q_OBJ_CD, Len.CW08Q_OBJ_CD);
	}

	public void setCw08qCiorShwObjKeyCi(char cw08qCiorShwObjKeyCi) {
		writeChar(Pos.CW08Q_CIOR_SHW_OBJ_KEY_CI, cw08qCiorShwObjKeyCi);
	}

	/**Original name: CW08Q-CIOR-SHW-OBJ-KEY-CI<br>*/
	public char getCw08qCiorShwObjKeyCi() {
		return readChar(Pos.CW08Q_CIOR_SHW_OBJ_KEY_CI);
	}

	public void setCw08qCiorShwObjKey(String cw08qCiorShwObjKey) {
		writeString(Pos.CW08Q_CIOR_SHW_OBJ_KEY, cw08qCiorShwObjKey, Len.CW08Q_CIOR_SHW_OBJ_KEY);
	}

	/**Original name: CW08Q-CIOR-SHW-OBJ-KEY<br>*/
	public String getCw08qCiorShwObjKey() {
		return readString(Pos.CW08Q_CIOR_SHW_OBJ_KEY, Len.CW08Q_CIOR_SHW_OBJ_KEY);
	}

	public void setCw08qAdrSeqNbrCi(char cw08qAdrSeqNbrCi) {
		writeChar(Pos.CW08Q_ADR_SEQ_NBR_CI, cw08qAdrSeqNbrCi);
	}

	/**Original name: CW08Q-ADR-SEQ-NBR-CI<br>*/
	public char getCw08qAdrSeqNbrCi() {
		return readChar(Pos.CW08Q_ADR_SEQ_NBR_CI);
	}

	public void setCw08qAdrSeqNbrSign(char cw08qAdrSeqNbrSign) {
		writeChar(Pos.CW08Q_ADR_SEQ_NBR_SIGN, cw08qAdrSeqNbrSign);
	}

	/**Original name: CW08Q-ADR-SEQ-NBR-SIGN<br>*/
	public char getCw08qAdrSeqNbrSign() {
		return readChar(Pos.CW08Q_ADR_SEQ_NBR_SIGN);
	}

	public void setCw08qAdrSeqNbrFormatted(String cw08qAdrSeqNbr) {
		writeString(Pos.CW08Q_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(cw08qAdrSeqNbr, Len.CW08Q_ADR_SEQ_NBR), Len.CW08Q_ADR_SEQ_NBR);
	}

	/**Original name: CW08Q-ADR-SEQ-NBR<br>*/
	public int getCw08qAdrSeqNbr() {
		return readNumDispUnsignedInt(Pos.CW08Q_ADR_SEQ_NBR, Len.CW08Q_ADR_SEQ_NBR);
	}

	public void setCw08qUserIdCi(char cw08qUserIdCi) {
		writeChar(Pos.CW08Q_USER_ID_CI, cw08qUserIdCi);
	}

	/**Original name: CW08Q-USER-ID-CI<br>*/
	public char getCw08qUserIdCi() {
		return readChar(Pos.CW08Q_USER_ID_CI);
	}

	public void setCw08qUserId(String cw08qUserId) {
		writeString(Pos.CW08Q_USER_ID, cw08qUserId, Len.CW08Q_USER_ID);
	}

	/**Original name: CW08Q-USER-ID<br>*/
	public String getCw08qUserId() {
		return readString(Pos.CW08Q_USER_ID, Len.CW08Q_USER_ID);
	}

	public void setCw08qStatusCdCi(char cw08qStatusCdCi) {
		writeChar(Pos.CW08Q_STATUS_CD_CI, cw08qStatusCdCi);
	}

	/**Original name: CW08Q-STATUS-CD-CI<br>*/
	public char getCw08qStatusCdCi() {
		return readChar(Pos.CW08Q_STATUS_CD_CI);
	}

	public void setCw08qStatusCd(char cw08qStatusCd) {
		writeChar(Pos.CW08Q_STATUS_CD, cw08qStatusCd);
	}

	/**Original name: CW08Q-STATUS-CD<br>*/
	public char getCw08qStatusCd() {
		return readChar(Pos.CW08Q_STATUS_CD);
	}

	public void setCw08qTerminalIdCi(char cw08qTerminalIdCi) {
		writeChar(Pos.CW08Q_TERMINAL_ID_CI, cw08qTerminalIdCi);
	}

	/**Original name: CW08Q-TERMINAL-ID-CI<br>*/
	public char getCw08qTerminalIdCi() {
		return readChar(Pos.CW08Q_TERMINAL_ID_CI);
	}

	public void setCw08qTerminalId(String cw08qTerminalId) {
		writeString(Pos.CW08Q_TERMINAL_ID, cw08qTerminalId, Len.CW08Q_TERMINAL_ID);
	}

	/**Original name: CW08Q-TERMINAL-ID<br>*/
	public String getCw08qTerminalId() {
		return readString(Pos.CW08Q_TERMINAL_ID, Len.CW08Q_TERMINAL_ID);
	}

	public void setCw08qCiorExpDtCi(char cw08qCiorExpDtCi) {
		writeChar(Pos.CW08Q_CIOR_EXP_DT_CI, cw08qCiorExpDtCi);
	}

	/**Original name: CW08Q-CIOR-EXP-DT-CI<br>*/
	public char getCw08qCiorExpDtCi() {
		return readChar(Pos.CW08Q_CIOR_EXP_DT_CI);
	}

	public void setCw08qCiorExpDt(String cw08qCiorExpDt) {
		writeString(Pos.CW08Q_CIOR_EXP_DT, cw08qCiorExpDt, Len.CW08Q_CIOR_EXP_DT);
	}

	/**Original name: CW08Q-CIOR-EXP-DT<br>*/
	public String getCw08qCiorExpDt() {
		return readString(Pos.CW08Q_CIOR_EXP_DT, Len.CW08Q_CIOR_EXP_DT);
	}

	public void setCw08qCiorEffAcyTsCi(char cw08qCiorEffAcyTsCi) {
		writeChar(Pos.CW08Q_CIOR_EFF_ACY_TS_CI, cw08qCiorEffAcyTsCi);
	}

	/**Original name: CW08Q-CIOR-EFF-ACY-TS-CI<br>*/
	public char getCw08qCiorEffAcyTsCi() {
		return readChar(Pos.CW08Q_CIOR_EFF_ACY_TS_CI);
	}

	public void setCw08qCiorEffAcyTs(String cw08qCiorEffAcyTs) {
		writeString(Pos.CW08Q_CIOR_EFF_ACY_TS, cw08qCiorEffAcyTs, Len.CW08Q_CIOR_EFF_ACY_TS);
	}

	/**Original name: CW08Q-CIOR-EFF-ACY-TS<br>*/
	public String getCw08qCiorEffAcyTs() {
		return readString(Pos.CW08Q_CIOR_EFF_ACY_TS, Len.CW08Q_CIOR_EFF_ACY_TS);
	}

	public void setCw08qCiorExpAcyTsCi(char cw08qCiorExpAcyTsCi) {
		writeChar(Pos.CW08Q_CIOR_EXP_ACY_TS_CI, cw08qCiorExpAcyTsCi);
	}

	/**Original name: CW08Q-CIOR-EXP-ACY-TS-CI<br>*/
	public char getCw08qCiorExpAcyTsCi() {
		return readChar(Pos.CW08Q_CIOR_EXP_ACY_TS_CI);
	}

	public void setCw08qCiorExpAcyTs(String cw08qCiorExpAcyTs) {
		writeString(Pos.CW08Q_CIOR_EXP_ACY_TS, cw08qCiorExpAcyTs, Len.CW08Q_CIOR_EXP_ACY_TS);
	}

	/**Original name: CW08Q-CIOR-EXP-ACY-TS<br>*/
	public String getCw08qCiorExpAcyTs() {
		return readString(Pos.CW08Q_CIOR_EXP_ACY_TS, Len.CW08Q_CIOR_EXP_ACY_TS);
	}

	public void setCw08qAppType(char cw08qAppType) {
		writeChar(Pos.CW08Q_APP_TYPE, cw08qAppType);
	}

	/**Original name: CW08Q-APP-TYPE<br>*/
	public char getCw08qAppType() {
		return readChar(Pos.CW08Q_APP_TYPE);
	}

	public void setCw08qBusObjNm(String cw08qBusObjNm) {
		writeString(Pos.CW08Q_BUS_OBJ_NM, cw08qBusObjNm, Len.CW08Q_BUS_OBJ_NM);
	}

	/**Original name: CW08Q-BUS-OBJ-NM<br>*/
	public String getCw08qBusObjNm() {
		return readString(Pos.CW08Q_BUS_OBJ_NM, Len.CW08Q_BUS_OBJ_NM);
	}

	public void setCw08qObjDesc(String cw08qObjDesc) {
		writeString(Pos.CW08Q_OBJ_DESC, cw08qObjDesc, Len.CW08Q_OBJ_DESC);
	}

	/**Original name: CW08Q-OBJ-DESC<br>*/
	public String getCw08qObjDesc() {
		return readString(Pos.CW08Q_OBJ_DESC, Len.CW08Q_OBJ_DESC);
	}

	public String getCworcCltObjRelationRowFormatted() {
		return readFixedString(Pos.CWORC_CLT_OBJ_RELATION_ROW, Len.CWORC_CLT_OBJ_RELATION_ROW);
	}

	public void setCworcCltObjRelationCsumFormatted(String cworcCltObjRelationCsum) {
		writeString(Pos.CWORC_CLT_OBJ_RELATION_CSUM, Trunc.toUnsignedNumeric(cworcCltObjRelationCsum, Len.CWORC_CLT_OBJ_RELATION_CSUM),
				Len.CWORC_CLT_OBJ_RELATION_CSUM);
	}

	/**Original name: CWORC-CLT-OBJ-RELATION-CSUM<br>*/
	public int getCworcCltObjRelationCsum() {
		return readNumDispUnsignedInt(Pos.CWORC_CLT_OBJ_RELATION_CSUM, Len.CWORC_CLT_OBJ_RELATION_CSUM);
	}

	public void setCworcTchObjectKeyKcre(String cworcTchObjectKeyKcre) {
		writeString(Pos.CWORC_TCH_OBJECT_KEY_KCRE, cworcTchObjectKeyKcre, Len.CWORC_TCH_OBJECT_KEY_KCRE);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY-KCRE<br>*/
	public String getCworcTchObjectKeyKcre() {
		return readString(Pos.CWORC_TCH_OBJECT_KEY_KCRE, Len.CWORC_TCH_OBJECT_KEY_KCRE);
	}

	public void setCworcHistoryVldNbrKcre(String cworcHistoryVldNbrKcre) {
		writeString(Pos.CWORC_HISTORY_VLD_NBR_KCRE, cworcHistoryVldNbrKcre, Len.CWORC_HISTORY_VLD_NBR_KCRE);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-KCRE<br>*/
	public String getCworcHistoryVldNbrKcre() {
		return readString(Pos.CWORC_HISTORY_VLD_NBR_KCRE, Len.CWORC_HISTORY_VLD_NBR_KCRE);
	}

	public void setCworcCiorEffDtKcre(String cworcCiorEffDtKcre) {
		writeString(Pos.CWORC_CIOR_EFF_DT_KCRE, cworcCiorEffDtKcre, Len.CWORC_CIOR_EFF_DT_KCRE);
	}

	/**Original name: CWORC-CIOR-EFF-DT-KCRE<br>*/
	public String getCworcCiorEffDtKcre() {
		return readString(Pos.CWORC_CIOR_EFF_DT_KCRE, Len.CWORC_CIOR_EFF_DT_KCRE);
	}

	public void setCworcObjSysIdKcre(String cworcObjSysIdKcre) {
		writeString(Pos.CWORC_OBJ_SYS_ID_KCRE, cworcObjSysIdKcre, Len.CWORC_OBJ_SYS_ID_KCRE);
	}

	/**Original name: CWORC-OBJ-SYS-ID-KCRE<br>*/
	public String getCworcObjSysIdKcre() {
		return readString(Pos.CWORC_OBJ_SYS_ID_KCRE, Len.CWORC_OBJ_SYS_ID_KCRE);
	}

	public void setCworcCiorObjSeqNbrKcre(String cworcCiorObjSeqNbrKcre) {
		writeString(Pos.CWORC_CIOR_OBJ_SEQ_NBR_KCRE, cworcCiorObjSeqNbrKcre, Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-KCRE<br>*/
	public String getCworcCiorObjSeqNbrKcre() {
		return readString(Pos.CWORC_CIOR_OBJ_SEQ_NBR_KCRE, Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE);
	}

	public void setCworcClientIdKcre(String cworcClientIdKcre) {
		writeString(Pos.CWORC_CLIENT_ID_KCRE, cworcClientIdKcre, Len.CWORC_CLIENT_ID_KCRE);
	}

	/**Original name: CWORC-CLIENT-ID-KCRE<br>*/
	public String getCworcClientIdKcre() {
		return readString(Pos.CWORC_CLIENT_ID_KCRE, Len.CWORC_CLIENT_ID_KCRE);
	}

	public void setCworcTransProcessDt(String cworcTransProcessDt) {
		writeString(Pos.CWORC_TRANS_PROCESS_DT, cworcTransProcessDt, Len.CWORC_TRANS_PROCESS_DT);
	}

	/**Original name: CWORC-TRANS-PROCESS-DT<br>*/
	public String getCworcTransProcessDt() {
		return readString(Pos.CWORC_TRANS_PROCESS_DT, Len.CWORC_TRANS_PROCESS_DT);
	}

	public void setCworcTchObjectKeyCi(char cworcTchObjectKeyCi) {
		writeChar(Pos.CWORC_TCH_OBJECT_KEY_CI, cworcTchObjectKeyCi);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY-CI<br>*/
	public char getCworcTchObjectKeyCi() {
		return readChar(Pos.CWORC_TCH_OBJECT_KEY_CI);
	}

	public void setCworcTchObjectKey(String cworcTchObjectKey) {
		writeString(Pos.CWORC_TCH_OBJECT_KEY, cworcTchObjectKey, Len.CWORC_TCH_OBJECT_KEY);
	}

	/**Original name: CWORC-TCH-OBJECT-KEY<br>*/
	public String getCworcTchObjectKey() {
		return readString(Pos.CWORC_TCH_OBJECT_KEY, Len.CWORC_TCH_OBJECT_KEY);
	}

	public void setCworcHistoryVldNbrCi(char cworcHistoryVldNbrCi) {
		writeChar(Pos.CWORC_HISTORY_VLD_NBR_CI, cworcHistoryVldNbrCi);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-CI<br>*/
	public char getCworcHistoryVldNbrCi() {
		return readChar(Pos.CWORC_HISTORY_VLD_NBR_CI);
	}

	public void setCworcHistoryVldNbrSign(char cworcHistoryVldNbrSign) {
		writeChar(Pos.CWORC_HISTORY_VLD_NBR_SIGN, cworcHistoryVldNbrSign);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR-SIGN<br>*/
	public char getCworcHistoryVldNbrSign() {
		return readChar(Pos.CWORC_HISTORY_VLD_NBR_SIGN);
	}

	public void setCworcHistoryVldNbrFormatted(String cworcHistoryVldNbr) {
		writeString(Pos.CWORC_HISTORY_VLD_NBR, Trunc.toUnsignedNumeric(cworcHistoryVldNbr, Len.CWORC_HISTORY_VLD_NBR), Len.CWORC_HISTORY_VLD_NBR);
	}

	/**Original name: CWORC-HISTORY-VLD-NBR<br>*/
	public int getCworcHistoryVldNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_HISTORY_VLD_NBR, Len.CWORC_HISTORY_VLD_NBR);
	}

	public void setCworcCiorEffDtCi(char cworcCiorEffDtCi) {
		writeChar(Pos.CWORC_CIOR_EFF_DT_CI, cworcCiorEffDtCi);
	}

	/**Original name: CWORC-CIOR-EFF-DT-CI<br>*/
	public char getCworcCiorEffDtCi() {
		return readChar(Pos.CWORC_CIOR_EFF_DT_CI);
	}

	public void setCworcCiorEffDt(String cworcCiorEffDt) {
		writeString(Pos.CWORC_CIOR_EFF_DT, cworcCiorEffDt, Len.CWORC_CIOR_EFF_DT);
	}

	/**Original name: CWORC-CIOR-EFF-DT<br>*/
	public String getCworcCiorEffDt() {
		return readString(Pos.CWORC_CIOR_EFF_DT, Len.CWORC_CIOR_EFF_DT);
	}

	public void setCworcObjSysIdCi(char cworcObjSysIdCi) {
		writeChar(Pos.CWORC_OBJ_SYS_ID_CI, cworcObjSysIdCi);
	}

	/**Original name: CWORC-OBJ-SYS-ID-CI<br>*/
	public char getCworcObjSysIdCi() {
		return readChar(Pos.CWORC_OBJ_SYS_ID_CI);
	}

	public void setCworcObjSysId(String cworcObjSysId) {
		writeString(Pos.CWORC_OBJ_SYS_ID, cworcObjSysId, Len.CWORC_OBJ_SYS_ID);
	}

	/**Original name: CWORC-OBJ-SYS-ID<br>*/
	public String getCworcObjSysId() {
		return readString(Pos.CWORC_OBJ_SYS_ID, Len.CWORC_OBJ_SYS_ID);
	}

	public void setCworcCiorObjSeqNbrCi(char cworcCiorObjSeqNbrCi) {
		writeChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_CI, cworcCiorObjSeqNbrCi);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-CI<br>*/
	public char getCworcCiorObjSeqNbrCi() {
		return readChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_CI);
	}

	public void setCworcCiorObjSeqNbrSign(char cworcCiorObjSeqNbrSign) {
		writeChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_SIGN, cworcCiorObjSeqNbrSign);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR-SIGN<br>*/
	public char getCworcCiorObjSeqNbrSign() {
		return readChar(Pos.CWORC_CIOR_OBJ_SEQ_NBR_SIGN);
	}

	public void setCworcCiorObjSeqNbrFormatted(String cworcCiorObjSeqNbr) {
		writeString(Pos.CWORC_CIOR_OBJ_SEQ_NBR, Trunc.toUnsignedNumeric(cworcCiorObjSeqNbr, Len.CWORC_CIOR_OBJ_SEQ_NBR), Len.CWORC_CIOR_OBJ_SEQ_NBR);
	}

	/**Original name: CWORC-CIOR-OBJ-SEQ-NBR<br>*/
	public int getCworcCiorObjSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_CIOR_OBJ_SEQ_NBR, Len.CWORC_CIOR_OBJ_SEQ_NBR);
	}

	public void setCworcClientIdCi(char cworcClientIdCi) {
		writeChar(Pos.CWORC_CLIENT_ID_CI, cworcClientIdCi);
	}

	/**Original name: CWORC-CLIENT-ID-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	public char getCworcClientIdCi() {
		return readChar(Pos.CWORC_CLIENT_ID_CI);
	}

	public void setCworcClientId(String cworcClientId) {
		writeString(Pos.CWORC_CLIENT_ID, cworcClientId, Len.CWORC_CLIENT_ID);
	}

	/**Original name: CWORC-CLIENT-ID<br>*/
	public String getCworcClientId() {
		return readString(Pos.CWORC_CLIENT_ID, Len.CWORC_CLIENT_ID);
	}

	public void setCworcRltTypCdCi(char cworcRltTypCdCi) {
		writeChar(Pos.CWORC_RLT_TYP_CD_CI, cworcRltTypCdCi);
	}

	/**Original name: CWORC-RLT-TYP-CD-CI<br>*/
	public char getCworcRltTypCdCi() {
		return readChar(Pos.CWORC_RLT_TYP_CD_CI);
	}

	public void setCworcRltTypCd(String cworcRltTypCd) {
		writeString(Pos.CWORC_RLT_TYP_CD, cworcRltTypCd, Len.CWORC_RLT_TYP_CD);
	}

	/**Original name: CWORC-RLT-TYP-CD<br>*/
	public String getCworcRltTypCd() {
		return readString(Pos.CWORC_RLT_TYP_CD, Len.CWORC_RLT_TYP_CD);
	}

	public void setCworcObjCdCi(char cworcObjCdCi) {
		writeChar(Pos.CWORC_OBJ_CD_CI, cworcObjCdCi);
	}

	/**Original name: CWORC-OBJ-CD-CI<br>*/
	public char getCworcObjCdCi() {
		return readChar(Pos.CWORC_OBJ_CD_CI);
	}

	public void setCworcObjCd(String cworcObjCd) {
		writeString(Pos.CWORC_OBJ_CD, cworcObjCd, Len.CWORC_OBJ_CD);
	}

	/**Original name: CWORC-OBJ-CD<br>*/
	public String getCworcObjCd() {
		return readString(Pos.CWORC_OBJ_CD, Len.CWORC_OBJ_CD);
	}

	public void setCworcCiorShwObjKeyCi(char cworcCiorShwObjKeyCi) {
		writeChar(Pos.CWORC_CIOR_SHW_OBJ_KEY_CI, cworcCiorShwObjKeyCi);
	}

	/**Original name: CWORC-CIOR-SHW-OBJ-KEY-CI<br>*/
	public char getCworcCiorShwObjKeyCi() {
		return readChar(Pos.CWORC_CIOR_SHW_OBJ_KEY_CI);
	}

	public void setCworcCiorShwObjKey(String cworcCiorShwObjKey) {
		writeString(Pos.CWORC_CIOR_SHW_OBJ_KEY, cworcCiorShwObjKey, Len.CWORC_CIOR_SHW_OBJ_KEY);
	}

	/**Original name: CWORC-CIOR-SHW-OBJ-KEY<br>*/
	public String getCworcCiorShwObjKey() {
		return readString(Pos.CWORC_CIOR_SHW_OBJ_KEY, Len.CWORC_CIOR_SHW_OBJ_KEY);
	}

	public void setCworcAdrSeqNbrCi(char cworcAdrSeqNbrCi) {
		writeChar(Pos.CWORC_ADR_SEQ_NBR_CI, cworcAdrSeqNbrCi);
	}

	/**Original name: CWORC-ADR-SEQ-NBR-CI<br>*/
	public char getCworcAdrSeqNbrCi() {
		return readChar(Pos.CWORC_ADR_SEQ_NBR_CI);
	}

	public void setCworcAdrSeqNbrSign(char cworcAdrSeqNbrSign) {
		writeChar(Pos.CWORC_ADR_SEQ_NBR_SIGN, cworcAdrSeqNbrSign);
	}

	/**Original name: CWORC-ADR-SEQ-NBR-SIGN<br>*/
	public char getCworcAdrSeqNbrSign() {
		return readChar(Pos.CWORC_ADR_SEQ_NBR_SIGN);
	}

	public void setCworcAdrSeqNbrFormatted(String cworcAdrSeqNbr) {
		writeString(Pos.CWORC_ADR_SEQ_NBR, Trunc.toUnsignedNumeric(cworcAdrSeqNbr, Len.CWORC_ADR_SEQ_NBR), Len.CWORC_ADR_SEQ_NBR);
	}

	/**Original name: CWORC-ADR-SEQ-NBR<br>*/
	public int getCworcAdrSeqNbr() {
		return readNumDispUnsignedInt(Pos.CWORC_ADR_SEQ_NBR, Len.CWORC_ADR_SEQ_NBR);
	}

	public void setCworcUserIdCi(char cworcUserIdCi) {
		writeChar(Pos.CWORC_USER_ID_CI, cworcUserIdCi);
	}

	/**Original name: CWORC-USER-ID-CI<br>*/
	public char getCworcUserIdCi() {
		return readChar(Pos.CWORC_USER_ID_CI);
	}

	public void setCworcUserId(String cworcUserId) {
		writeString(Pos.CWORC_USER_ID, cworcUserId, Len.CWORC_USER_ID);
	}

	/**Original name: CWORC-USER-ID<br>*/
	public String getCworcUserId() {
		return readString(Pos.CWORC_USER_ID, Len.CWORC_USER_ID);
	}

	public void setCworcStatusCdCi(char cworcStatusCdCi) {
		writeChar(Pos.CWORC_STATUS_CD_CI, cworcStatusCdCi);
	}

	/**Original name: CWORC-STATUS-CD-CI<br>*/
	public char getCworcStatusCdCi() {
		return readChar(Pos.CWORC_STATUS_CD_CI);
	}

	public void setCworcStatusCd(char cworcStatusCd) {
		writeChar(Pos.CWORC_STATUS_CD, cworcStatusCd);
	}

	/**Original name: CWORC-STATUS-CD<br>*/
	public char getCworcStatusCd() {
		return readChar(Pos.CWORC_STATUS_CD);
	}

	public void setCworcTerminalIdCi(char cworcTerminalIdCi) {
		writeChar(Pos.CWORC_TERMINAL_ID_CI, cworcTerminalIdCi);
	}

	/**Original name: CWORC-TERMINAL-ID-CI<br>*/
	public char getCworcTerminalIdCi() {
		return readChar(Pos.CWORC_TERMINAL_ID_CI);
	}

	public void setCworcTerminalId(String cworcTerminalId) {
		writeString(Pos.CWORC_TERMINAL_ID, cworcTerminalId, Len.CWORC_TERMINAL_ID);
	}

	/**Original name: CWORC-TERMINAL-ID<br>*/
	public String getCworcTerminalId() {
		return readString(Pos.CWORC_TERMINAL_ID, Len.CWORC_TERMINAL_ID);
	}

	public void setCworcCiorExpDtCi(char cworcCiorExpDtCi) {
		writeChar(Pos.CWORC_CIOR_EXP_DT_CI, cworcCiorExpDtCi);
	}

	/**Original name: CWORC-CIOR-EXP-DT-CI<br>*/
	public char getCworcCiorExpDtCi() {
		return readChar(Pos.CWORC_CIOR_EXP_DT_CI);
	}

	public void setCworcCiorExpDt(String cworcCiorExpDt) {
		writeString(Pos.CWORC_CIOR_EXP_DT, cworcCiorExpDt, Len.CWORC_CIOR_EXP_DT);
	}

	/**Original name: CWORC-CIOR-EXP-DT<br>*/
	public String getCworcCiorExpDt() {
		return readString(Pos.CWORC_CIOR_EXP_DT, Len.CWORC_CIOR_EXP_DT);
	}

	public void setCworcCiorEffAcyTsCi(char cworcCiorEffAcyTsCi) {
		writeChar(Pos.CWORC_CIOR_EFF_ACY_TS_CI, cworcCiorEffAcyTsCi);
	}

	/**Original name: CWORC-CIOR-EFF-ACY-TS-CI<br>*/
	public char getCworcCiorEffAcyTsCi() {
		return readChar(Pos.CWORC_CIOR_EFF_ACY_TS_CI);
	}

	public void setCworcCiorEffAcyTs(String cworcCiorEffAcyTs) {
		writeString(Pos.CWORC_CIOR_EFF_ACY_TS, cworcCiorEffAcyTs, Len.CWORC_CIOR_EFF_ACY_TS);
	}

	/**Original name: CWORC-CIOR-EFF-ACY-TS<br>*/
	public String getCworcCiorEffAcyTs() {
		return readString(Pos.CWORC_CIOR_EFF_ACY_TS, Len.CWORC_CIOR_EFF_ACY_TS);
	}

	public void setCworcCiorExpAcyTsCi(char cworcCiorExpAcyTsCi) {
		writeChar(Pos.CWORC_CIOR_EXP_ACY_TS_CI, cworcCiorExpAcyTsCi);
	}

	/**Original name: CWORC-CIOR-EXP-ACY-TS-CI<br>*/
	public char getCworcCiorExpAcyTsCi() {
		return readChar(Pos.CWORC_CIOR_EXP_ACY_TS_CI);
	}

	public void setCworcCiorExpAcyTs(String cworcCiorExpAcyTs) {
		writeString(Pos.CWORC_CIOR_EXP_ACY_TS, cworcCiorExpAcyTs, Len.CWORC_CIOR_EXP_ACY_TS);
	}

	/**Original name: CWORC-CIOR-EXP-ACY-TS<br>*/
	public String getCworcCiorExpAcyTs() {
		return readString(Pos.CWORC_CIOR_EXP_ACY_TS, Len.CWORC_CIOR_EXP_ACY_TS);
	}

	public void setCworcCiorLegEntCdCi(char cworcCiorLegEntCdCi) {
		writeChar(Pos.CWORC_CIOR_LEG_ENT_CD_CI, cworcCiorLegEntCdCi);
	}

	/**Original name: CWORC-CIOR-LEG-ENT-CD-CI<br>*/
	public char getCworcCiorLegEntCdCi() {
		return readChar(Pos.CWORC_CIOR_LEG_ENT_CD_CI);
	}

	public void setCworcCiorLegEntCd(String cworcCiorLegEntCd) {
		writeString(Pos.CWORC_CIOR_LEG_ENT_CD, cworcCiorLegEntCd, Len.CWORC_CIOR_LEG_ENT_CD);
	}

	/**Original name: CWORC-CIOR-LEG-ENT-CD<br>*/
	public String getCworcCiorLegEntCd() {
		return readString(Pos.CWORC_CIOR_LEG_ENT_CD, Len.CWORC_CIOR_LEG_ENT_CD);
	}

	public void setCworcCiorFstNmCi(char cworcCiorFstNmCi) {
		writeChar(Pos.CWORC_CIOR_FST_NM_CI, cworcCiorFstNmCi);
	}

	/**Original name: CWORC-CIOR-FST-NM-CI<br>*/
	public char getCworcCiorFstNmCi() {
		return readChar(Pos.CWORC_CIOR_FST_NM_CI);
	}

	public void setCworcCiorFstNm(String cworcCiorFstNm) {
		writeString(Pos.CWORC_CIOR_FST_NM, cworcCiorFstNm, Len.CWORC_CIOR_FST_NM);
	}

	/**Original name: CWORC-CIOR-FST-NM<br>*/
	public String getCworcCiorFstNm() {
		return readString(Pos.CWORC_CIOR_FST_NM, Len.CWORC_CIOR_FST_NM);
	}

	public void setCworcCiorLstNmCi(char cworcCiorLstNmCi) {
		writeChar(Pos.CWORC_CIOR_LST_NM_CI, cworcCiorLstNmCi);
	}

	/**Original name: CWORC-CIOR-LST-NM-CI<br>*/
	public char getCworcCiorLstNmCi() {
		return readChar(Pos.CWORC_CIOR_LST_NM_CI);
	}

	public void setCworcCiorLstNm(String cworcCiorLstNm) {
		writeString(Pos.CWORC_CIOR_LST_NM, cworcCiorLstNm, Len.CWORC_CIOR_LST_NM);
	}

	/**Original name: CWORC-CIOR-LST-NM<br>*/
	public String getCworcCiorLstNm() {
		return readString(Pos.CWORC_CIOR_LST_NM, Len.CWORC_CIOR_LST_NM);
	}

	public void setCworcCiorMdlNmCi(char cworcCiorMdlNmCi) {
		writeChar(Pos.CWORC_CIOR_MDL_NM_CI, cworcCiorMdlNmCi);
	}

	/**Original name: CWORC-CIOR-MDL-NM-CI<br>*/
	public char getCworcCiorMdlNmCi() {
		return readChar(Pos.CWORC_CIOR_MDL_NM_CI);
	}

	public void setCworcCiorMdlNm(String cworcCiorMdlNm) {
		writeString(Pos.CWORC_CIOR_MDL_NM, cworcCiorMdlNm, Len.CWORC_CIOR_MDL_NM);
	}

	/**Original name: CWORC-CIOR-MDL-NM<br>*/
	public String getCworcCiorMdlNm() {
		return readString(Pos.CWORC_CIOR_MDL_NM, Len.CWORC_CIOR_MDL_NM);
	}

	public void setCworcCiorNmPfxCi(char cworcCiorNmPfxCi) {
		writeChar(Pos.CWORC_CIOR_NM_PFX_CI, cworcCiorNmPfxCi);
	}

	/**Original name: CWORC-CIOR-NM-PFX-CI<br>*/
	public char getCworcCiorNmPfxCi() {
		return readChar(Pos.CWORC_CIOR_NM_PFX_CI);
	}

	public void setCworcCiorNmPfx(String cworcCiorNmPfx) {
		writeString(Pos.CWORC_CIOR_NM_PFX, cworcCiorNmPfx, Len.CWORC_CIOR_NM_PFX);
	}

	/**Original name: CWORC-CIOR-NM-PFX<br>*/
	public String getCworcCiorNmPfx() {
		return readString(Pos.CWORC_CIOR_NM_PFX, Len.CWORC_CIOR_NM_PFX);
	}

	public void setCworcCiorNmSfxCi(char cworcCiorNmSfxCi) {
		writeChar(Pos.CWORC_CIOR_NM_SFX_CI, cworcCiorNmSfxCi);
	}

	/**Original name: CWORC-CIOR-NM-SFX-CI<br>*/
	public char getCworcCiorNmSfxCi() {
		return readChar(Pos.CWORC_CIOR_NM_SFX_CI);
	}

	public void setCworcCiorNmSfx(String cworcCiorNmSfx) {
		writeString(Pos.CWORC_CIOR_NM_SFX, cworcCiorNmSfx, Len.CWORC_CIOR_NM_SFX);
	}

	/**Original name: CWORC-CIOR-NM-SFX<br>*/
	public String getCworcCiorNmSfx() {
		return readString(Pos.CWORC_CIOR_NM_SFX, Len.CWORC_CIOR_NM_SFX);
	}

	public void setCworcCiorLngNmCi(char cworcCiorLngNmCi) {
		writeChar(Pos.CWORC_CIOR_LNG_NM_CI, cworcCiorLngNmCi);
	}

	/**Original name: CWORC-CIOR-LNG-NM-CI<br>*/
	public char getCworcCiorLngNmCi() {
		return readChar(Pos.CWORC_CIOR_LNG_NM_CI);
	}

	public void setCworcCiorLngNmNi(char cworcCiorLngNmNi) {
		writeChar(Pos.CWORC_CIOR_LNG_NM_NI, cworcCiorLngNmNi);
	}

	/**Original name: CWORC-CIOR-LNG-NM-NI<br>*/
	public char getCworcCiorLngNmNi() {
		return readChar(Pos.CWORC_CIOR_LNG_NM_NI);
	}

	public void setCworcCiorLngNm(String cworcCiorLngNm) {
		writeString(Pos.CWORC_CIOR_LNG_NM, cworcCiorLngNm, Len.CWORC_CIOR_LNG_NM);
	}

	/**Original name: CWORC-CIOR-LNG-NM<br>*/
	public String getCworcCiorLngNm() {
		return readString(Pos.CWORC_CIOR_LNG_NM, Len.CWORC_CIOR_LNG_NM);
	}

	public void setCworcFilterType(String cworcFilterType) {
		writeString(Pos.CWORC_FILTER_TYPE, cworcFilterType, Len.CWORC_FILTER_TYPE);
	}

	/**Original name: CWORC-FILTER-TYPE<br>
	 * <pre>*  FIELDS AS INPUT TO BPO:</pre>*/
	public String getCworcFilterType() {
		return readString(Pos.CWORC_FILTER_TYPE, Len.CWORC_FILTER_TYPE);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_LOC_CLT_LIST = L_FRAMEWORK_REQUEST_AREA;
		public static final int CWGIKQ_INQUIRE_KEY_ROW = L_FW_REQ_LOC_CLT_LIST;
		public static final int CWGIKQ_CLT_OBJ_RELATION = CWGIKQ_INQUIRE_KEY_ROW;
		public static final int CWGIKQ_TCH_OBJECT_KEY = CWGIKQ_CLT_OBJ_RELATION;
		public static final int CWGIKQ_RLT_TYP_CD = CWGIKQ_TCH_OBJECT_KEY + Len.CWGIKQ_TCH_OBJECT_KEY;
		public static final int CWGIKQ_OBJ_CD = CWGIKQ_RLT_TYP_CD + Len.CWGIKQ_RLT_TYP_CD;
		public static final int CWGIKQ_SHW_OBJ_KEY = CWGIKQ_OBJ_CD + Len.CWGIKQ_OBJ_CD;
		public static final int CWGIKQ_OBJ_SYS_ID = CWGIKQ_SHW_OBJ_KEY + Len.CWGIKQ_SHW_OBJ_KEY;
		public static final int CWGIKQ_CLIENT_PHONE = CWGIKQ_OBJ_SYS_ID + Len.CWGIKQ_OBJ_SYS_ID;
		public static final int CWGIKQ_PHN_NBR = CWGIKQ_CLIENT_PHONE;
		public static final int CWGIKQ_PHN_TYP_CD = CWGIKQ_PHN_NBR + Len.CWGIKQ_PHN_NBR;
		public static final int CWGIKQ_CLIENT_EMAIL = CWGIKQ_PHN_TYP_CD + Len.CWGIKQ_PHN_TYP_CD;
		public static final int CWGIKQ_EMAIL_ADR_TXT = CWGIKQ_CLIENT_EMAIL;
		public static final int CWGIKQ_EMAIL_TYPE_CD = CWGIKQ_EMAIL_ADR_TXT + Len.CWGIKQ_EMAIL_ADR_TXT;
		public static final int CWGIKQ_CLIENT_TAX = CWGIKQ_EMAIL_TYPE_CD + Len.CWGIKQ_EMAIL_TYPE_CD;
		public static final int CWGIKQ_TAX_TYPE_CD = CWGIKQ_CLIENT_TAX;
		public static final int CWGIKQ_CITX_TAX_ID = CWGIKQ_TAX_TYPE_CD + Len.CWGIKQ_TAX_TYPE_CD;
		public static final int CWGIKQ_POL_NBR = CWGIKQ_CITX_TAX_ID + Len.CWGIKQ_CITX_TAX_ID;
		public static final int CWGIKQ_EFF_DT = CWGIKQ_POL_NBR + Len.CWGIKQ_POL_NBR;
		public static final int CWGIKQ_EXP_DT = CWGIKQ_EFF_DT + Len.CWGIKQ_EFF_DT;
		public static final int L_FW_REQ_CLIENT_TAB = CWGIKQ_EXP_DT + Len.CWGIKQ_EXP_DT;
		public static final int CW02Q_CLIENT_TAB_ROW = L_FW_REQ_CLIENT_TAB;
		public static final int CW02Q_CLIENT_TAB_FIXED = CW02Q_CLIENT_TAB_ROW;
		public static final int CW02Q_CLIENT_TAB_CHK_SUM = CW02Q_CLIENT_TAB_FIXED;
		public static final int CW02Q_CLIENT_ID_KCRE = CW02Q_CLIENT_TAB_CHK_SUM + Len.CW02Q_CLIENT_TAB_CHK_SUM;
		public static final int CW02Q_CLIENT_TAB_DATES = CW02Q_CLIENT_ID_KCRE + Len.CW02Q_CLIENT_ID_KCRE;
		public static final int CW02Q_TRANS_PROCESS_DT = CW02Q_CLIENT_TAB_DATES;
		public static final int CW02Q_CLIENT_TAB_KEY = CW02Q_TRANS_PROCESS_DT + Len.CW02Q_TRANS_PROCESS_DT;
		public static final int CW02Q_CLIENT_ID_CI = CW02Q_CLIENT_TAB_KEY;
		public static final int CW02Q_CLIENT_ID = CW02Q_CLIENT_ID_CI + Len.CW02Q_CLIENT_ID_CI;
		public static final int CW02Q_HISTORY_VLD_NBR_CI = CW02Q_CLIENT_ID + Len.CW02Q_CLIENT_ID;
		public static final int CW02Q_HISTORY_VLD_NBR_SIGN = CW02Q_HISTORY_VLD_NBR_CI + Len.CW02Q_HISTORY_VLD_NBR_CI;
		public static final int CW02Q_HISTORY_VLD_NBR = CW02Q_HISTORY_VLD_NBR_SIGN + Len.CW02Q_HISTORY_VLD_NBR_SIGN;
		public static final int CW02Q_CICL_EFF_DT_CI = CW02Q_HISTORY_VLD_NBR + Len.CW02Q_HISTORY_VLD_NBR;
		public static final int CW02Q_CICL_EFF_DT = CW02Q_CICL_EFF_DT_CI + Len.CW02Q_CICL_EFF_DT_CI;
		public static final int CW02Q_CLIENT_TAB_DATA = CW02Q_CICL_EFF_DT + Len.CW02Q_CICL_EFF_DT;
		public static final int CW02Q_CICL_PRI_SUB_CD_CI = CW02Q_CLIENT_TAB_DATA;
		public static final int CW02Q_CICL_PRI_SUB_CD = CW02Q_CICL_PRI_SUB_CD_CI + Len.CW02Q_CICL_PRI_SUB_CD_CI;
		public static final int CW02Q_CICL_FST_NM_CI = CW02Q_CICL_PRI_SUB_CD + Len.CW02Q_CICL_PRI_SUB_CD;
		public static final int CW02Q_CICL_FST_NM = CW02Q_CICL_FST_NM_CI + Len.CW02Q_CICL_FST_NM_CI;
		public static final int CW02Q_CICL_LST_NM_CI = CW02Q_CICL_FST_NM + Len.CW02Q_CICL_FST_NM;
		public static final int CW02Q_CICL_LST_NM = CW02Q_CICL_LST_NM_CI + Len.CW02Q_CICL_LST_NM_CI;
		public static final int CW02Q_CICL_MDL_NM_CI = CW02Q_CICL_LST_NM + Len.CW02Q_CICL_LST_NM;
		public static final int CW02Q_CICL_MDL_NM = CW02Q_CICL_MDL_NM_CI + Len.CW02Q_CICL_MDL_NM_CI;
		public static final int CW02Q_NM_PFX_CI = CW02Q_CICL_MDL_NM + Len.CW02Q_CICL_MDL_NM;
		public static final int CW02Q_NM_PFX = CW02Q_NM_PFX_CI + Len.CW02Q_NM_PFX_CI;
		public static final int CW02Q_NM_SFX_CI = CW02Q_NM_PFX + Len.CW02Q_NM_PFX;
		public static final int CW02Q_NM_SFX = CW02Q_NM_SFX_CI + Len.CW02Q_NM_SFX_CI;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD_CI = CW02Q_NM_SFX + Len.CW02Q_NM_SFX;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD_NI = CW02Q_PRIMARY_PRO_DSN_CD_CI + Len.CW02Q_PRIMARY_PRO_DSN_CD_CI;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD = CW02Q_PRIMARY_PRO_DSN_CD_NI + Len.CW02Q_PRIMARY_PRO_DSN_CD_NI;
		public static final int CW02Q_LEG_ENT_CD_CI = CW02Q_PRIMARY_PRO_DSN_CD + Len.CW02Q_PRIMARY_PRO_DSN_CD;
		public static final int CW02Q_LEG_ENT_CD = CW02Q_LEG_ENT_CD_CI + Len.CW02Q_LEG_ENT_CD_CI;
		public static final int CW02Q_CICL_SDX_CD_CI = CW02Q_LEG_ENT_CD + Len.CW02Q_LEG_ENT_CD;
		public static final int CW02Q_CICL_SDX_CD = CW02Q_CICL_SDX_CD_CI + Len.CW02Q_CICL_SDX_CD_CI;
		public static final int CW02Q_CICL_OGN_INCEPT_DT_CI = CW02Q_CICL_SDX_CD + Len.CW02Q_CICL_SDX_CD;
		public static final int CW02Q_CICL_OGN_INCEPT_DT_NI = CW02Q_CICL_OGN_INCEPT_DT_CI + Len.CW02Q_CICL_OGN_INCEPT_DT_CI;
		public static final int CW02Q_CICL_OGN_INCEPT_DT = CW02Q_CICL_OGN_INCEPT_DT_NI + Len.CW02Q_CICL_OGN_INCEPT_DT_NI;
		public static final int CW02Q_CICL_ADD_NM_IND_CI = CW02Q_CICL_OGN_INCEPT_DT + Len.CW02Q_CICL_OGN_INCEPT_DT;
		public static final int CW02Q_CICL_ADD_NM_IND_NI = CW02Q_CICL_ADD_NM_IND_CI + Len.CW02Q_CICL_ADD_NM_IND_CI;
		public static final int CW02Q_CICL_ADD_NM_IND = CW02Q_CICL_ADD_NM_IND_NI + Len.CW02Q_CICL_ADD_NM_IND_NI;
		public static final int CW02Q_CICL_DOB_DT_CI = CW02Q_CICL_ADD_NM_IND + Len.CW02Q_CICL_ADD_NM_IND;
		public static final int CW02Q_CICL_DOB_DT = CW02Q_CICL_DOB_DT_CI + Len.CW02Q_CICL_DOB_DT_CI;
		public static final int CW02Q_CICL_BIR_ST_CD_CI = CW02Q_CICL_DOB_DT + Len.CW02Q_CICL_DOB_DT;
		public static final int CW02Q_CICL_BIR_ST_CD = CW02Q_CICL_BIR_ST_CD_CI + Len.CW02Q_CICL_BIR_ST_CD_CI;
		public static final int CW02Q_GENDER_CD_CI = CW02Q_CICL_BIR_ST_CD + Len.CW02Q_CICL_BIR_ST_CD;
		public static final int CW02Q_GENDER_CD = CW02Q_GENDER_CD_CI + Len.CW02Q_GENDER_CD_CI;
		public static final int CW02Q_PRI_LGG_CD_CI = CW02Q_GENDER_CD + Len.CW02Q_GENDER_CD;
		public static final int CW02Q_PRI_LGG_CD = CW02Q_PRI_LGG_CD_CI + Len.CW02Q_PRI_LGG_CD_CI;
		public static final int CW02Q_USER_ID_CI = CW02Q_PRI_LGG_CD + Len.CW02Q_PRI_LGG_CD;
		public static final int CW02Q_USER_ID = CW02Q_USER_ID_CI + Len.CW02Q_USER_ID_CI;
		public static final int CW02Q_STATUS_CD_CI = CW02Q_USER_ID + Len.CW02Q_USER_ID;
		public static final int CW02Q_STATUS_CD = CW02Q_STATUS_CD_CI + Len.CW02Q_STATUS_CD_CI;
		public static final int CW02Q_TERMINAL_ID_CI = CW02Q_STATUS_CD + Len.CW02Q_STATUS_CD;
		public static final int CW02Q_TERMINAL_ID = CW02Q_TERMINAL_ID_CI + Len.CW02Q_TERMINAL_ID_CI;
		public static final int CW02Q_CICL_EXP_DT_CI = CW02Q_TERMINAL_ID + Len.CW02Q_TERMINAL_ID;
		public static final int CW02Q_CICL_EXP_DT = CW02Q_CICL_EXP_DT_CI + Len.CW02Q_CICL_EXP_DT_CI;
		public static final int CW02Q_CICL_EFF_ACY_TS_CI = CW02Q_CICL_EXP_DT + Len.CW02Q_CICL_EXP_DT;
		public static final int CW02Q_CICL_EFF_ACY_TS = CW02Q_CICL_EFF_ACY_TS_CI + Len.CW02Q_CICL_EFF_ACY_TS_CI;
		public static final int CW02Q_CICL_EXP_ACY_TS_CI = CW02Q_CICL_EFF_ACY_TS + Len.CW02Q_CICL_EFF_ACY_TS;
		public static final int CW02Q_CICL_EXP_ACY_TS = CW02Q_CICL_EXP_ACY_TS_CI + Len.CW02Q_CICL_EXP_ACY_TS_CI;
		public static final int CW02Q_STATUTORY_TLE_CD_CI = CW02Q_CICL_EXP_ACY_TS + Len.CW02Q_CICL_EXP_ACY_TS;
		public static final int CW02Q_STATUTORY_TLE_CD = CW02Q_STATUTORY_TLE_CD_CI + Len.CW02Q_STATUTORY_TLE_CD_CI;
		public static final int CW02Q_CICL_LNG_NM_CI = CW02Q_STATUTORY_TLE_CD + Len.CW02Q_STATUTORY_TLE_CD;
		public static final int CW02Q_CICL_LNG_NM_NI = CW02Q_CICL_LNG_NM_CI + Len.CW02Q_CICL_LNG_NM_CI;
		public static final int CW02Q_CICL_LNG_NM = CW02Q_CICL_LNG_NM_NI + Len.CW02Q_CICL_LNG_NM_NI;
		public static final int CW02Q_LO_LST_NM_MCH_CD_CI = CW02Q_CICL_LNG_NM + Len.CW02Q_CICL_LNG_NM;
		public static final int CW02Q_LO_LST_NM_MCH_CD_NI = CW02Q_LO_LST_NM_MCH_CD_CI + Len.CW02Q_LO_LST_NM_MCH_CD_CI;
		public static final int CW02Q_LO_LST_NM_MCH_CD = CW02Q_LO_LST_NM_MCH_CD_NI + Len.CW02Q_LO_LST_NM_MCH_CD_NI;
		public static final int CW02Q_HI_LST_NM_MCH_CD_CI = CW02Q_LO_LST_NM_MCH_CD + Len.CW02Q_LO_LST_NM_MCH_CD;
		public static final int CW02Q_HI_LST_NM_MCH_CD_NI = CW02Q_HI_LST_NM_MCH_CD_CI + Len.CW02Q_HI_LST_NM_MCH_CD_CI;
		public static final int CW02Q_HI_LST_NM_MCH_CD = CW02Q_HI_LST_NM_MCH_CD_NI + Len.CW02Q_HI_LST_NM_MCH_CD_NI;
		public static final int CW02Q_LO_FST_NM_MCH_CD_CI = CW02Q_HI_LST_NM_MCH_CD + Len.CW02Q_HI_LST_NM_MCH_CD;
		public static final int CW02Q_LO_FST_NM_MCH_CD_NI = CW02Q_LO_FST_NM_MCH_CD_CI + Len.CW02Q_LO_FST_NM_MCH_CD_CI;
		public static final int CW02Q_LO_FST_NM_MCH_CD = CW02Q_LO_FST_NM_MCH_CD_NI + Len.CW02Q_LO_FST_NM_MCH_CD_NI;
		public static final int CW02Q_HI_FST_NM_MCH_CD_CI = CW02Q_LO_FST_NM_MCH_CD + Len.CW02Q_LO_FST_NM_MCH_CD;
		public static final int CW02Q_HI_FST_NM_MCH_CD_NI = CW02Q_HI_FST_NM_MCH_CD_CI + Len.CW02Q_HI_FST_NM_MCH_CD_CI;
		public static final int CW02Q_HI_FST_NM_MCH_CD = CW02Q_HI_FST_NM_MCH_CD_NI + Len.CW02Q_HI_FST_NM_MCH_CD_NI;
		public static final int CW02Q_CICL_ACQ_SRC_CD_CI = CW02Q_HI_FST_NM_MCH_CD + Len.CW02Q_HI_FST_NM_MCH_CD;
		public static final int CW02Q_CICL_ACQ_SRC_CD_NI = CW02Q_CICL_ACQ_SRC_CD_CI + Len.CW02Q_CICL_ACQ_SRC_CD_CI;
		public static final int CW02Q_CICL_ACQ_SRC_CD = CW02Q_CICL_ACQ_SRC_CD_NI + Len.CW02Q_CICL_ACQ_SRC_CD_NI;
		public static final int CW02Q_LEG_ENT_DESC = CW02Q_CICL_ACQ_SRC_CD + Len.CW02Q_CICL_ACQ_SRC_CD;
		public static final int L_FW_REQ_CLT_ADR_RELATION = CW02Q_LEG_ENT_DESC + Len.CW02Q_LEG_ENT_DESC;
		public static final int CWCACQ_CLIENT_ADDR_COMPOSITE = L_FW_REQ_CLT_ADR_RELATION;
		public static final int CWCACQ_CLIENT_ADDR_COMP_FIXED = CWCACQ_CLIENT_ADDR_COMPOSITE;
		public static final int CWCACQ_CLIENT_ADDR_COMP_CHKSUM = CWCACQ_CLIENT_ADDR_COMP_FIXED;
		public static final int CWCACQ_ADR_ID_KCRE = CWCACQ_CLIENT_ADDR_COMP_CHKSUM + Len.CWCACQ_CLIENT_ADDR_COMP_CHKSUM;
		public static final int CWCACQ_CLIENT_ID_KCRE = CWCACQ_ADR_ID_KCRE + Len.CWCACQ_ADR_ID_KCRE;
		public static final int CWCACQ_ADR_SEQ_NBR_KCRE = CWCACQ_CLIENT_ID_KCRE + Len.CWCACQ_CLIENT_ID_KCRE;
		public static final int CWCACQ_TCH_OBJECT_KEY_KCRE = CWCACQ_ADR_SEQ_NBR_KCRE + Len.CWCACQ_ADR_SEQ_NBR_KCRE;
		public static final int CWCACQ_CLIENT_ADDRESS_KEY = CWCACQ_TCH_OBJECT_KEY_KCRE + Len.CWCACQ_TCH_OBJECT_KEY_KCRE;
		public static final int CWCACQ_ADR_ID_CI = CWCACQ_CLIENT_ADDRESS_KEY;
		public static final int CWCACQ_ADR_ID = CWCACQ_ADR_ID_CI + Len.CWCACQ_ADR_ID_CI;
		public static final int CWCACQ_CLT_ADR_RELATION_KEY = CWCACQ_ADR_ID + Len.CWCACQ_ADR_ID;
		public static final int CWCACQ_CLIENT_ID_CI = CWCACQ_CLT_ADR_RELATION_KEY;
		public static final int CWCACQ_CLIENT_ID = CWCACQ_CLIENT_ID_CI + Len.CWCACQ_CLIENT_ID_CI;
		public static final int CWCACQ_HISTORY_VLD_NBR_CI = CWCACQ_CLIENT_ID + Len.CWCACQ_CLIENT_ID;
		public static final int CWCACQ_HISTORY_VLD_NBR_SIGN = CWCACQ_HISTORY_VLD_NBR_CI + Len.CWCACQ_HISTORY_VLD_NBR_CI;
		public static final int CWCACQ_HISTORY_VLD_NBR = CWCACQ_HISTORY_VLD_NBR_SIGN + Len.CWCACQ_HISTORY_VLD_NBR_SIGN;
		public static final int CWCACQ_ADR_SEQ_NBR_CI = CWCACQ_HISTORY_VLD_NBR + Len.CWCACQ_HISTORY_VLD_NBR;
		public static final int CWCACQ_ADR_SEQ_NBR_SIGN = CWCACQ_ADR_SEQ_NBR_CI + Len.CWCACQ_ADR_SEQ_NBR_CI;
		public static final int CWCACQ_ADR_SEQ_NBR = CWCACQ_ADR_SEQ_NBR_SIGN + Len.CWCACQ_ADR_SEQ_NBR_SIGN;
		public static final int CWCACQ_CIAR_EFF_DT_CI = CWCACQ_ADR_SEQ_NBR + Len.CWCACQ_ADR_SEQ_NBR;
		public static final int CWCACQ_CIAR_EFF_DT = CWCACQ_CIAR_EFF_DT_CI + Len.CWCACQ_CIAR_EFF_DT_CI;
		public static final int CWCACQ_CLIENT_ADDR_COMP_DATES = CWCACQ_CIAR_EFF_DT + Len.CWCACQ_CIAR_EFF_DT;
		public static final int CWCACQ_TRANS_PROCESS_DT = CWCACQ_CLIENT_ADDR_COMP_DATES;
		public static final int CWCACQ_CLIENT_ADDR_COMP_DATA = CWCACQ_TRANS_PROCESS_DT + Len.CWCACQ_TRANS_PROCESS_DT;
		public static final int CWCACQ_USER_ID_CI = CWCACQ_CLIENT_ADDR_COMP_DATA;
		public static final int CWCACQ_USER_ID = CWCACQ_USER_ID_CI + Len.CWCACQ_USER_ID_CI;
		public static final int CWCACQ_TERMINAL_ID_CI = CWCACQ_USER_ID + Len.CWCACQ_USER_ID;
		public static final int CWCACQ_TERMINAL_ID = CWCACQ_TERMINAL_ID_CI + Len.CWCACQ_TERMINAL_ID_CI;
		public static final int CWCACQ_CICA_ADR1_CI = CWCACQ_TERMINAL_ID + Len.CWCACQ_TERMINAL_ID;
		public static final int CWCACQ_CICA_ADR1 = CWCACQ_CICA_ADR1_CI + Len.CWCACQ_CICA_ADR1_CI;
		public static final int CWCACQ_CICA_ADR2_CI = CWCACQ_CICA_ADR1 + Len.CWCACQ_CICA_ADR1;
		public static final int CWCACQ_CICA_ADR2_NI = CWCACQ_CICA_ADR2_CI + Len.CWCACQ_CICA_ADR2_CI;
		public static final int CWCACQ_CICA_ADR2 = CWCACQ_CICA_ADR2_NI + Len.CWCACQ_CICA_ADR2_NI;
		public static final int CWCACQ_CICA_CIT_NM_CI = CWCACQ_CICA_ADR2 + Len.CWCACQ_CICA_ADR2;
		public static final int CWCACQ_CICA_CIT_NM = CWCACQ_CICA_CIT_NM_CI + Len.CWCACQ_CICA_CIT_NM_CI;
		public static final int CWCACQ_CICA_CTY_CI = CWCACQ_CICA_CIT_NM + Len.CWCACQ_CICA_CIT_NM;
		public static final int CWCACQ_CICA_CTY_NI = CWCACQ_CICA_CTY_CI + Len.CWCACQ_CICA_CTY_CI;
		public static final int CWCACQ_CICA_CTY = CWCACQ_CICA_CTY_NI + Len.CWCACQ_CICA_CTY_NI;
		public static final int CWCACQ_ST_CD_CI = CWCACQ_CICA_CTY + Len.CWCACQ_CICA_CTY;
		public static final int CWCACQ_ST_CD = CWCACQ_ST_CD_CI + Len.CWCACQ_ST_CD_CI;
		public static final int CWCACQ_CICA_PST_CD_CI = CWCACQ_ST_CD + Len.CWCACQ_ST_CD;
		public static final int CWCACQ_CICA_PST_CD = CWCACQ_CICA_PST_CD_CI + Len.CWCACQ_CICA_PST_CD_CI;
		public static final int CWCACQ_CTR_CD_CI = CWCACQ_CICA_PST_CD + Len.CWCACQ_CICA_PST_CD;
		public static final int CWCACQ_CTR_CD = CWCACQ_CTR_CD_CI + Len.CWCACQ_CTR_CD_CI;
		public static final int CWCACQ_CICA_ADD_ADR_IND_CI = CWCACQ_CTR_CD + Len.CWCACQ_CTR_CD;
		public static final int CWCACQ_CICA_ADD_ADR_IND_NI = CWCACQ_CICA_ADD_ADR_IND_CI + Len.CWCACQ_CICA_ADD_ADR_IND_CI;
		public static final int CWCACQ_CICA_ADD_ADR_IND = CWCACQ_CICA_ADD_ADR_IND_NI + Len.CWCACQ_CICA_ADD_ADR_IND_NI;
		public static final int CWCACQ_ADDR_STATUS_CD_CI = CWCACQ_CICA_ADD_ADR_IND + Len.CWCACQ_CICA_ADD_ADR_IND;
		public static final int CWCACQ_ADDR_STATUS_CD = CWCACQ_ADDR_STATUS_CD_CI + Len.CWCACQ_ADDR_STATUS_CD_CI;
		public static final int CWCACQ_ADD_NM_IND_CI = CWCACQ_ADDR_STATUS_CD + Len.CWCACQ_ADDR_STATUS_CD;
		public static final int CWCACQ_ADD_NM_IND = CWCACQ_ADD_NM_IND_CI + Len.CWCACQ_ADD_NM_IND_CI;
		public static final int CWCACQ_ADR_TYP_CD_CI = CWCACQ_ADD_NM_IND + Len.CWCACQ_ADD_NM_IND;
		public static final int CWCACQ_ADR_TYP_CD = CWCACQ_ADR_TYP_CD_CI + Len.CWCACQ_ADR_TYP_CD_CI;
		public static final int CWCACQ_TCH_OBJECT_KEY_CI = CWCACQ_ADR_TYP_CD + Len.CWCACQ_ADR_TYP_CD;
		public static final int CWCACQ_TCH_OBJECT_KEY = CWCACQ_TCH_OBJECT_KEY_CI + Len.CWCACQ_TCH_OBJECT_KEY_CI;
		public static final int CWCACQ_CIAR_EXP_DT_CI = CWCACQ_TCH_OBJECT_KEY + Len.CWCACQ_TCH_OBJECT_KEY;
		public static final int CWCACQ_CIAR_EXP_DT = CWCACQ_CIAR_EXP_DT_CI + Len.CWCACQ_CIAR_EXP_DT_CI;
		public static final int CWCACQ_CIAR_SER_ADR1_TXT_CI = CWCACQ_CIAR_EXP_DT + Len.CWCACQ_CIAR_EXP_DT;
		public static final int CWCACQ_CIAR_SER_ADR1_TXT = CWCACQ_CIAR_SER_ADR1_TXT_CI + Len.CWCACQ_CIAR_SER_ADR1_TXT_CI;
		public static final int CWCACQ_CIAR_SER_CIT_NM_CI = CWCACQ_CIAR_SER_ADR1_TXT + Len.CWCACQ_CIAR_SER_ADR1_TXT;
		public static final int CWCACQ_CIAR_SER_CIT_NM = CWCACQ_CIAR_SER_CIT_NM_CI + Len.CWCACQ_CIAR_SER_CIT_NM_CI;
		public static final int CWCACQ_CIAR_SER_ST_CD_CI = CWCACQ_CIAR_SER_CIT_NM + Len.CWCACQ_CIAR_SER_CIT_NM;
		public static final int CWCACQ_CIAR_SER_ST_CD = CWCACQ_CIAR_SER_ST_CD_CI + Len.CWCACQ_CIAR_SER_ST_CD_CI;
		public static final int CWCACQ_CIAR_SER_PST_CD_CI = CWCACQ_CIAR_SER_ST_CD + Len.CWCACQ_CIAR_SER_ST_CD;
		public static final int CWCACQ_CIAR_SER_PST_CD = CWCACQ_CIAR_SER_PST_CD_CI + Len.CWCACQ_CIAR_SER_PST_CD_CI;
		public static final int CWCACQ_REL_STATUS_CD_CI = CWCACQ_CIAR_SER_PST_CD + Len.CWCACQ_CIAR_SER_PST_CD;
		public static final int CWCACQ_REL_STATUS_CD = CWCACQ_REL_STATUS_CD_CI + Len.CWCACQ_REL_STATUS_CD_CI;
		public static final int CWCACQ_EFF_ACY_TS_CI = CWCACQ_REL_STATUS_CD + Len.CWCACQ_REL_STATUS_CD;
		public static final int CWCACQ_EFF_ACY_TS = CWCACQ_EFF_ACY_TS_CI + Len.CWCACQ_EFF_ACY_TS_CI;
		public static final int CWCACQ_CIAR_EXP_ACY_TS_CI = CWCACQ_EFF_ACY_TS + Len.CWCACQ_EFF_ACY_TS;
		public static final int CWCACQ_CIAR_EXP_ACY_TS = CWCACQ_CIAR_EXP_ACY_TS_CI + Len.CWCACQ_CIAR_EXP_ACY_TS_CI;
		public static final int CWCACQ_MORE_ROWS_SW = CWCACQ_CIAR_EXP_ACY_TS + Len.CWCACQ_CIAR_EXP_ACY_TS;
		public static final int CWCACQ_CICA_SHW_OBJ_KEY_CI = CWCACQ_MORE_ROWS_SW + Len.CWCACQ_MORE_ROWS_SW;
		public static final int CWCACQ_CICA_SHW_OBJ_KEY = CWCACQ_CICA_SHW_OBJ_KEY_CI + Len.CWCACQ_CICA_SHW_OBJ_KEY_CI;
		public static final int CWCACQ_BUS_OBJ_NM = CWCACQ_CICA_SHW_OBJ_KEY + Len.CWCACQ_CICA_SHW_OBJ_KEY;
		public static final int CWCACQ_ST_DESC = CWCACQ_BUS_OBJ_NM + Len.CWCACQ_BUS_OBJ_NM;
		public static final int CWCACQ_ADR_TYP_DESC = CWCACQ_ST_DESC + Len.CWCACQ_ST_DESC;
		public static final int L_FW_REQ_CLT_CLT_REL = CWCACQ_ADR_TYP_DESC + Len.CWCACQ_ADR_TYP_DESC;
		public static final int CW06Q_CLT_CLT_RELATION_ROW = L_FW_REQ_CLT_CLT_REL;
		public static final int CW06Q_CLT_CLT_RELATION_FIXED = CW06Q_CLT_CLT_RELATION_ROW;
		public static final int CW06Q_CLT_CLT_RELATION_CSUM = CW06Q_CLT_CLT_RELATION_FIXED;
		public static final int CW06Q_CLIENT_ID_KCRE = CW06Q_CLT_CLT_RELATION_CSUM + Len.CW06Q_CLT_CLT_RELATION_CSUM;
		public static final int CW06Q_CLT_TYP_CD_KCRE = CW06Q_CLIENT_ID_KCRE + Len.CW06Q_CLIENT_ID_KCRE;
		public static final int CW06Q_HISTORY_VLD_NBR_KCRE = CW06Q_CLT_TYP_CD_KCRE + Len.CW06Q_CLT_TYP_CD_KCRE;
		public static final int CW06Q_CICR_XRF_ID_KCRE = CW06Q_HISTORY_VLD_NBR_KCRE + Len.CW06Q_HISTORY_VLD_NBR_KCRE;
		public static final int CW06Q_XRF_TYP_CD_KCRE = CW06Q_CICR_XRF_ID_KCRE + Len.CW06Q_CICR_XRF_ID_KCRE;
		public static final int CW06Q_CICR_EFF_DT_KCRE = CW06Q_XRF_TYP_CD_KCRE + Len.CW06Q_XRF_TYP_CD_KCRE;
		public static final int CW06Q_CLT_CLT_RELATION_DATES = CW06Q_CICR_EFF_DT_KCRE + Len.CW06Q_CICR_EFF_DT_KCRE;
		public static final int CW06Q_TRANS_PROCESS_DT = CW06Q_CLT_CLT_RELATION_DATES;
		public static final int CW06Q_CLT_CLT_RELATION_KEY = CW06Q_TRANS_PROCESS_DT + Len.CW06Q_TRANS_PROCESS_DT;
		public static final int CW06Q_CLIENT_ID_CI = CW06Q_CLT_CLT_RELATION_KEY;
		public static final int CW06Q_CLIENT_ID = CW06Q_CLIENT_ID_CI + Len.CW06Q_CLIENT_ID_CI;
		public static final int CW06Q_CLT_TYP_CD_CI = CW06Q_CLIENT_ID + Len.CW06Q_CLIENT_ID;
		public static final int CW06Q_CLT_TYP_CD = CW06Q_CLT_TYP_CD_CI + Len.CW06Q_CLT_TYP_CD_CI;
		public static final int CW06Q_HISTORY_VLD_NBR_CI = CW06Q_CLT_TYP_CD + Len.CW06Q_CLT_TYP_CD;
		public static final int CW06Q_HISTORY_VLD_NBR_SIGNED = CW06Q_HISTORY_VLD_NBR_CI + Len.CW06Q_HISTORY_VLD_NBR_CI;
		public static final int CW06Q_CICR_XRF_ID_CI = CW06Q_HISTORY_VLD_NBR_SIGNED + Len.CW06Q_HISTORY_VLD_NBR_SIGNED;
		public static final int CW06Q_CICR_XRF_ID = CW06Q_CICR_XRF_ID_CI + Len.CW06Q_CICR_XRF_ID_CI;
		public static final int CW06Q_XRF_TYP_CD_CI = CW06Q_CICR_XRF_ID + Len.CW06Q_CICR_XRF_ID;
		public static final int CW06Q_XRF_TYP_CD = CW06Q_XRF_TYP_CD_CI + Len.CW06Q_XRF_TYP_CD_CI;
		public static final int CW06Q_CICR_EFF_DT_CI = CW06Q_XRF_TYP_CD + Len.CW06Q_XRF_TYP_CD;
		public static final int CW06Q_CICR_EFF_DT = CW06Q_CICR_EFF_DT_CI + Len.CW06Q_CICR_EFF_DT_CI;
		public static final int CW06Q_CLT_CLT_RELATION_DATA = CW06Q_CICR_EFF_DT + Len.CW06Q_CICR_EFF_DT;
		public static final int CW06Q_CICR_EXP_DT_CI = CW06Q_CLT_CLT_RELATION_DATA;
		public static final int CW06Q_CICR_EXP_DT = CW06Q_CICR_EXP_DT_CI + Len.CW06Q_CICR_EXP_DT_CI;
		public static final int CW06Q_CICR_NBR_OPR_STRS_CI = CW06Q_CICR_EXP_DT + Len.CW06Q_CICR_EXP_DT;
		public static final int CW06Q_CICR_NBR_OPR_STRS_NI = CW06Q_CICR_NBR_OPR_STRS_CI + Len.CW06Q_CICR_NBR_OPR_STRS_CI;
		public static final int CW06Q_CICR_NBR_OPR_STRS = CW06Q_CICR_NBR_OPR_STRS_NI + Len.CW06Q_CICR_NBR_OPR_STRS_NI;
		public static final int CW06Q_USER_ID_CI = CW06Q_CICR_NBR_OPR_STRS + Len.CW06Q_CICR_NBR_OPR_STRS;
		public static final int CW06Q_USER_ID = CW06Q_USER_ID_CI + Len.CW06Q_USER_ID_CI;
		public static final int CW06Q_STATUS_CD_CI = CW06Q_USER_ID + Len.CW06Q_USER_ID;
		public static final int CW06Q_STATUS_CD = CW06Q_STATUS_CD_CI + Len.CW06Q_STATUS_CD_CI;
		public static final int CW06Q_TERMINAL_ID_CI = CW06Q_STATUS_CD + Len.CW06Q_STATUS_CD;
		public static final int CW06Q_TERMINAL_ID = CW06Q_TERMINAL_ID_CI + Len.CW06Q_TERMINAL_ID_CI;
		public static final int CW06Q_CICR_EFF_ACY_TS_CI = CW06Q_TERMINAL_ID + Len.CW06Q_TERMINAL_ID;
		public static final int CW06Q_CICR_EFF_ACY_TS = CW06Q_CICR_EFF_ACY_TS_CI + Len.CW06Q_CICR_EFF_ACY_TS_CI;
		public static final int CW06Q_CICR_EXP_ACY_TS_CI = CW06Q_CICR_EFF_ACY_TS + Len.CW06Q_CICR_EFF_ACY_TS;
		public static final int CW06Q_CICR_EXP_ACY_TS = CW06Q_CICR_EXP_ACY_TS_CI + Len.CW06Q_CICR_EXP_ACY_TS_CI;
		public static final int L_FW_REQ_CLT_OBJ_REL = CW06Q_CICR_EXP_ACY_TS + Len.CW06Q_CICR_EXP_ACY_TS;
		public static final int CW08Q_CLT_OBJ_RELATION_ROW = L_FW_REQ_CLT_OBJ_REL;
		public static final int CW08Q_CLT_OBJ_RELATION_FIXED = CW08Q_CLT_OBJ_RELATION_ROW;
		public static final int CW08Q_CLT_OBJ_RELATION_CSUM = CW08Q_CLT_OBJ_RELATION_FIXED;
		public static final int CW08Q_TCH_OBJECT_KEY_KCRE = CW08Q_CLT_OBJ_RELATION_CSUM + Len.CW08Q_CLT_OBJ_RELATION_CSUM;
		public static final int CW08Q_HISTORY_VLD_NBR_KCRE = CW08Q_TCH_OBJECT_KEY_KCRE + Len.CW08Q_TCH_OBJECT_KEY_KCRE;
		public static final int CW08Q_CIOR_EFF_DT_KCRE = CW08Q_HISTORY_VLD_NBR_KCRE + Len.CW08Q_HISTORY_VLD_NBR_KCRE;
		public static final int CW08Q_OBJ_SYS_ID_KCRE = CW08Q_CIOR_EFF_DT_KCRE + Len.CW08Q_CIOR_EFF_DT_KCRE;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_KCRE = CW08Q_OBJ_SYS_ID_KCRE + Len.CW08Q_OBJ_SYS_ID_KCRE;
		public static final int CW08Q_CLIENT_ID_KCRE = CW08Q_CIOR_OBJ_SEQ_NBR_KCRE + Len.CW08Q_CIOR_OBJ_SEQ_NBR_KCRE;
		public static final int CW08Q_CLT_OBJ_RELATION_DATES = CW08Q_CLIENT_ID_KCRE + Len.CW08Q_CLIENT_ID_KCRE;
		public static final int CW08Q_TRANS_PROCESS_DT = CW08Q_CLT_OBJ_RELATION_DATES;
		public static final int CW08Q_CLT_OBJ_RELATION_KEY = CW08Q_TRANS_PROCESS_DT + Len.CW08Q_TRANS_PROCESS_DT;
		public static final int CW08Q_TCH_OBJECT_KEY_CI = CW08Q_CLT_OBJ_RELATION_KEY;
		public static final int CW08Q_TCH_OBJECT_KEY = CW08Q_TCH_OBJECT_KEY_CI + Len.CW08Q_TCH_OBJECT_KEY_CI;
		public static final int CW08Q_HISTORY_VLD_NBR_CI = CW08Q_TCH_OBJECT_KEY + Len.CW08Q_TCH_OBJECT_KEY;
		public static final int CW08Q_HISTORY_VLD_NBR_SIGN = CW08Q_HISTORY_VLD_NBR_CI + Len.CW08Q_HISTORY_VLD_NBR_CI;
		public static final int CW08Q_HISTORY_VLD_NBR = CW08Q_HISTORY_VLD_NBR_SIGN + Len.CW08Q_HISTORY_VLD_NBR_SIGN;
		public static final int CW08Q_CIOR_EFF_DT_CI = CW08Q_HISTORY_VLD_NBR + Len.CW08Q_HISTORY_VLD_NBR;
		public static final int CW08Q_CIOR_EFF_DT = CW08Q_CIOR_EFF_DT_CI + Len.CW08Q_CIOR_EFF_DT_CI;
		public static final int CW08Q_OBJ_SYS_ID_CI = CW08Q_CIOR_EFF_DT + Len.CW08Q_CIOR_EFF_DT;
		public static final int CW08Q_OBJ_SYS_ID = CW08Q_OBJ_SYS_ID_CI + Len.CW08Q_OBJ_SYS_ID_CI;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_CI = CW08Q_OBJ_SYS_ID + Len.CW08Q_OBJ_SYS_ID;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_SIGN = CW08Q_CIOR_OBJ_SEQ_NBR_CI + Len.CW08Q_CIOR_OBJ_SEQ_NBR_CI;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR = CW08Q_CIOR_OBJ_SEQ_NBR_SIGN + Len.CW08Q_CIOR_OBJ_SEQ_NBR_SIGN;
		public static final int CW08Q_CLT_OBJ_RELATION_DATA = CW08Q_CIOR_OBJ_SEQ_NBR + Len.CW08Q_CIOR_OBJ_SEQ_NBR;
		public static final int CW08Q_CLIENT_ID_CI = CW08Q_CLT_OBJ_RELATION_DATA;
		public static final int CW08Q_CLIENT_ID = CW08Q_CLIENT_ID_CI + Len.CW08Q_CLIENT_ID_CI;
		public static final int CW08Q_RLT_TYP_CD_CI = CW08Q_CLIENT_ID + Len.CW08Q_CLIENT_ID;
		public static final int CW08Q_RLT_TYP_CD = CW08Q_RLT_TYP_CD_CI + Len.CW08Q_RLT_TYP_CD_CI;
		public static final int CW08Q_OBJ_CD_CI = CW08Q_RLT_TYP_CD + Len.CW08Q_RLT_TYP_CD;
		public static final int CW08Q_OBJ_CD = CW08Q_OBJ_CD_CI + Len.CW08Q_OBJ_CD_CI;
		public static final int CW08Q_CIOR_SHW_OBJ_KEY_CI = CW08Q_OBJ_CD + Len.CW08Q_OBJ_CD;
		public static final int CW08Q_CIOR_SHW_OBJ_KEY = CW08Q_CIOR_SHW_OBJ_KEY_CI + Len.CW08Q_CIOR_SHW_OBJ_KEY_CI;
		public static final int CW08Q_ADR_SEQ_NBR_CI = CW08Q_CIOR_SHW_OBJ_KEY + Len.CW08Q_CIOR_SHW_OBJ_KEY;
		public static final int CW08Q_ADR_SEQ_NBR_SIGN = CW08Q_ADR_SEQ_NBR_CI + Len.CW08Q_ADR_SEQ_NBR_CI;
		public static final int CW08Q_ADR_SEQ_NBR = CW08Q_ADR_SEQ_NBR_SIGN + Len.CW08Q_ADR_SEQ_NBR_SIGN;
		public static final int CW08Q_USER_ID_CI = CW08Q_ADR_SEQ_NBR + Len.CW08Q_ADR_SEQ_NBR;
		public static final int CW08Q_USER_ID = CW08Q_USER_ID_CI + Len.CW08Q_USER_ID_CI;
		public static final int CW08Q_STATUS_CD_CI = CW08Q_USER_ID + Len.CW08Q_USER_ID;
		public static final int CW08Q_STATUS_CD = CW08Q_STATUS_CD_CI + Len.CW08Q_STATUS_CD_CI;
		public static final int CW08Q_TERMINAL_ID_CI = CW08Q_STATUS_CD + Len.CW08Q_STATUS_CD;
		public static final int CW08Q_TERMINAL_ID = CW08Q_TERMINAL_ID_CI + Len.CW08Q_TERMINAL_ID_CI;
		public static final int CW08Q_CIOR_EXP_DT_CI = CW08Q_TERMINAL_ID + Len.CW08Q_TERMINAL_ID;
		public static final int CW08Q_CIOR_EXP_DT = CW08Q_CIOR_EXP_DT_CI + Len.CW08Q_CIOR_EXP_DT_CI;
		public static final int CW08Q_CIOR_EFF_ACY_TS_CI = CW08Q_CIOR_EXP_DT + Len.CW08Q_CIOR_EXP_DT;
		public static final int CW08Q_CIOR_EFF_ACY_TS = CW08Q_CIOR_EFF_ACY_TS_CI + Len.CW08Q_CIOR_EFF_ACY_TS_CI;
		public static final int CW08Q_CIOR_EXP_ACY_TS_CI = CW08Q_CIOR_EFF_ACY_TS + Len.CW08Q_CIOR_EFF_ACY_TS;
		public static final int CW08Q_CIOR_EXP_ACY_TS = CW08Q_CIOR_EXP_ACY_TS_CI + Len.CW08Q_CIOR_EXP_ACY_TS_CI;
		public static final int CW08Q_APP_TYPE = CW08Q_CIOR_EXP_ACY_TS + Len.CW08Q_CIOR_EXP_ACY_TS;
		public static final int CW08Q_BUS_OBJ_NM = CW08Q_APP_TYPE + Len.CW08Q_APP_TYPE;
		public static final int CW08Q_OBJ_DESC = CW08Q_BUS_OBJ_NM + Len.CW08Q_BUS_OBJ_NM;
		public static final int L_FW_REQ_REL_CLT_OBJ_REL = CW08Q_OBJ_DESC + Len.CW08Q_OBJ_DESC;
		public static final int CWORC_CLT_OBJ_RELATION_ROW = L_FW_REQ_REL_CLT_OBJ_REL;
		public static final int CWORC_CLT_OBJ_RELATION_FIXED = CWORC_CLT_OBJ_RELATION_ROW;
		public static final int CWORC_CLT_OBJ_RELATION_CSUM = CWORC_CLT_OBJ_RELATION_FIXED;
		public static final int CWORC_TCH_OBJECT_KEY_KCRE = CWORC_CLT_OBJ_RELATION_CSUM + Len.CWORC_CLT_OBJ_RELATION_CSUM;
		public static final int CWORC_HISTORY_VLD_NBR_KCRE = CWORC_TCH_OBJECT_KEY_KCRE + Len.CWORC_TCH_OBJECT_KEY_KCRE;
		public static final int CWORC_CIOR_EFF_DT_KCRE = CWORC_HISTORY_VLD_NBR_KCRE + Len.CWORC_HISTORY_VLD_NBR_KCRE;
		public static final int CWORC_OBJ_SYS_ID_KCRE = CWORC_CIOR_EFF_DT_KCRE + Len.CWORC_CIOR_EFF_DT_KCRE;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_KCRE = CWORC_OBJ_SYS_ID_KCRE + Len.CWORC_OBJ_SYS_ID_KCRE;
		public static final int CWORC_CLIENT_ID_KCRE = CWORC_CIOR_OBJ_SEQ_NBR_KCRE + Len.CWORC_CIOR_OBJ_SEQ_NBR_KCRE;
		public static final int CWORC_CLT_OBJ_RELATION_DATES = CWORC_CLIENT_ID_KCRE + Len.CWORC_CLIENT_ID_KCRE;
		public static final int CWORC_TRANS_PROCESS_DT = CWORC_CLT_OBJ_RELATION_DATES;
		public static final int CWORC_CLT_OBJ_RELATION_KEY = CWORC_TRANS_PROCESS_DT + Len.CWORC_TRANS_PROCESS_DT;
		public static final int CWORC_TCH_OBJECT_KEY_CI = CWORC_CLT_OBJ_RELATION_KEY;
		public static final int CWORC_TCH_OBJECT_KEY = CWORC_TCH_OBJECT_KEY_CI + Len.CWORC_TCH_OBJECT_KEY_CI;
		public static final int CWORC_HISTORY_VLD_NBR_CI = CWORC_TCH_OBJECT_KEY + Len.CWORC_TCH_OBJECT_KEY;
		public static final int CWORC_HISTORY_VLD_NBR_SIGN = CWORC_HISTORY_VLD_NBR_CI + Len.CWORC_HISTORY_VLD_NBR_CI;
		public static final int CWORC_HISTORY_VLD_NBR = CWORC_HISTORY_VLD_NBR_SIGN + Len.CWORC_HISTORY_VLD_NBR_SIGN;
		public static final int CWORC_CIOR_EFF_DT_CI = CWORC_HISTORY_VLD_NBR + Len.CWORC_HISTORY_VLD_NBR;
		public static final int CWORC_CIOR_EFF_DT = CWORC_CIOR_EFF_DT_CI + Len.CWORC_CIOR_EFF_DT_CI;
		public static final int CWORC_OBJ_SYS_ID_CI = CWORC_CIOR_EFF_DT + Len.CWORC_CIOR_EFF_DT;
		public static final int CWORC_OBJ_SYS_ID = CWORC_OBJ_SYS_ID_CI + Len.CWORC_OBJ_SYS_ID_CI;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_CI = CWORC_OBJ_SYS_ID + Len.CWORC_OBJ_SYS_ID;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_SIGN = CWORC_CIOR_OBJ_SEQ_NBR_CI + Len.CWORC_CIOR_OBJ_SEQ_NBR_CI;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR = CWORC_CIOR_OBJ_SEQ_NBR_SIGN + Len.CWORC_CIOR_OBJ_SEQ_NBR_SIGN;
		public static final int CWORC_CLT_OBJ_RELATION_DATA = CWORC_CIOR_OBJ_SEQ_NBR + Len.CWORC_CIOR_OBJ_SEQ_NBR;
		public static final int CWORC_CLIENT_ID_CI = CWORC_CLT_OBJ_RELATION_DATA;
		public static final int CWORC_CLIENT_ID = CWORC_CLIENT_ID_CI + Len.CWORC_CLIENT_ID_CI;
		public static final int CWORC_RLT_TYP_CD_CI = CWORC_CLIENT_ID + Len.CWORC_CLIENT_ID;
		public static final int CWORC_RLT_TYP_CD = CWORC_RLT_TYP_CD_CI + Len.CWORC_RLT_TYP_CD_CI;
		public static final int CWORC_OBJ_CD_CI = CWORC_RLT_TYP_CD + Len.CWORC_RLT_TYP_CD;
		public static final int CWORC_OBJ_CD = CWORC_OBJ_CD_CI + Len.CWORC_OBJ_CD_CI;
		public static final int CWORC_CIOR_SHW_OBJ_KEY_CI = CWORC_OBJ_CD + Len.CWORC_OBJ_CD;
		public static final int CWORC_CIOR_SHW_OBJ_KEY = CWORC_CIOR_SHW_OBJ_KEY_CI + Len.CWORC_CIOR_SHW_OBJ_KEY_CI;
		public static final int CWORC_ADR_SEQ_NBR_CI = CWORC_CIOR_SHW_OBJ_KEY + Len.CWORC_CIOR_SHW_OBJ_KEY;
		public static final int CWORC_ADR_SEQ_NBR_SIGN = CWORC_ADR_SEQ_NBR_CI + Len.CWORC_ADR_SEQ_NBR_CI;
		public static final int CWORC_ADR_SEQ_NBR = CWORC_ADR_SEQ_NBR_SIGN + Len.CWORC_ADR_SEQ_NBR_SIGN;
		public static final int CWORC_USER_ID_CI = CWORC_ADR_SEQ_NBR + Len.CWORC_ADR_SEQ_NBR;
		public static final int CWORC_USER_ID = CWORC_USER_ID_CI + Len.CWORC_USER_ID_CI;
		public static final int CWORC_STATUS_CD_CI = CWORC_USER_ID + Len.CWORC_USER_ID;
		public static final int CWORC_STATUS_CD = CWORC_STATUS_CD_CI + Len.CWORC_STATUS_CD_CI;
		public static final int CWORC_TERMINAL_ID_CI = CWORC_STATUS_CD + Len.CWORC_STATUS_CD;
		public static final int CWORC_TERMINAL_ID = CWORC_TERMINAL_ID_CI + Len.CWORC_TERMINAL_ID_CI;
		public static final int CWORC_CIOR_EXP_DT_CI = CWORC_TERMINAL_ID + Len.CWORC_TERMINAL_ID;
		public static final int CWORC_CIOR_EXP_DT = CWORC_CIOR_EXP_DT_CI + Len.CWORC_CIOR_EXP_DT_CI;
		public static final int CWORC_CIOR_EFF_ACY_TS_CI = CWORC_CIOR_EXP_DT + Len.CWORC_CIOR_EXP_DT;
		public static final int CWORC_CIOR_EFF_ACY_TS = CWORC_CIOR_EFF_ACY_TS_CI + Len.CWORC_CIOR_EFF_ACY_TS_CI;
		public static final int CWORC_CIOR_EXP_ACY_TS_CI = CWORC_CIOR_EFF_ACY_TS + Len.CWORC_CIOR_EFF_ACY_TS;
		public static final int CWORC_CIOR_EXP_ACY_TS = CWORC_CIOR_EXP_ACY_TS_CI + Len.CWORC_CIOR_EXP_ACY_TS_CI;
		public static final int CWORC_CIOR_LEG_ENT_CD_CI = CWORC_CIOR_EXP_ACY_TS + Len.CWORC_CIOR_EXP_ACY_TS;
		public static final int CWORC_CIOR_LEG_ENT_CD = CWORC_CIOR_LEG_ENT_CD_CI + Len.CWORC_CIOR_LEG_ENT_CD_CI;
		public static final int CWORC_CIOR_FST_NM_CI = CWORC_CIOR_LEG_ENT_CD + Len.CWORC_CIOR_LEG_ENT_CD;
		public static final int CWORC_CIOR_FST_NM = CWORC_CIOR_FST_NM_CI + Len.CWORC_CIOR_FST_NM_CI;
		public static final int CWORC_CIOR_LST_NM_CI = CWORC_CIOR_FST_NM + Len.CWORC_CIOR_FST_NM;
		public static final int CWORC_CIOR_LST_NM = CWORC_CIOR_LST_NM_CI + Len.CWORC_CIOR_LST_NM_CI;
		public static final int CWORC_CIOR_MDL_NM_CI = CWORC_CIOR_LST_NM + Len.CWORC_CIOR_LST_NM;
		public static final int CWORC_CIOR_MDL_NM = CWORC_CIOR_MDL_NM_CI + Len.CWORC_CIOR_MDL_NM_CI;
		public static final int CWORC_CIOR_NM_PFX_CI = CWORC_CIOR_MDL_NM + Len.CWORC_CIOR_MDL_NM;
		public static final int CWORC_CIOR_NM_PFX = CWORC_CIOR_NM_PFX_CI + Len.CWORC_CIOR_NM_PFX_CI;
		public static final int CWORC_CIOR_NM_SFX_CI = CWORC_CIOR_NM_PFX + Len.CWORC_CIOR_NM_PFX;
		public static final int CWORC_CIOR_NM_SFX = CWORC_CIOR_NM_SFX_CI + Len.CWORC_CIOR_NM_SFX_CI;
		public static final int CWORC_CIOR_LNG_NM_CI = CWORC_CIOR_NM_SFX + Len.CWORC_CIOR_NM_SFX;
		public static final int CWORC_CIOR_LNG_NM_NI = CWORC_CIOR_LNG_NM_CI + Len.CWORC_CIOR_LNG_NM_CI;
		public static final int CWORC_CIOR_LNG_NM = CWORC_CIOR_LNG_NM_NI + Len.CWORC_CIOR_LNG_NM_NI;
		public static final int CWORC_CLT_OBJ_INPUT_DATA = CWORC_CIOR_LNG_NM + Len.CWORC_CIOR_LNG_NM;
		public static final int CWORC_FILTER_TYPE = CWORC_CLT_OBJ_INPUT_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int CWGIKQ_TCH_OBJECT_KEY = 20;
		public static final int CWGIKQ_RLT_TYP_CD = 4;
		public static final int CWGIKQ_OBJ_CD = 4;
		public static final int CWGIKQ_SHW_OBJ_KEY = 30;
		public static final int CWGIKQ_OBJ_SYS_ID = 4;
		public static final int CWGIKQ_PHN_NBR = 21;
		public static final int CWGIKQ_PHN_TYP_CD = 2;
		public static final int CWGIKQ_EMAIL_ADR_TXT = 255;
		public static final int CWGIKQ_EMAIL_TYPE_CD = 3;
		public static final int CWGIKQ_TAX_TYPE_CD = 3;
		public static final int CWGIKQ_CITX_TAX_ID = 15;
		public static final int CWGIKQ_POL_NBR = 25;
		public static final int CWGIKQ_EFF_DT = 10;
		public static final int CWGIKQ_EXP_DT = 10;
		public static final int CW02Q_CLIENT_TAB_CHK_SUM = 9;
		public static final int CW02Q_CLIENT_ID_KCRE = 32;
		public static final int CW02Q_TRANS_PROCESS_DT = 10;
		public static final int CW02Q_CLIENT_ID_CI = 1;
		public static final int CW02Q_CLIENT_ID = 20;
		public static final int CW02Q_HISTORY_VLD_NBR_CI = 1;
		public static final int CW02Q_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW02Q_HISTORY_VLD_NBR = 5;
		public static final int CW02Q_CICL_EFF_DT_CI = 1;
		public static final int CW02Q_CICL_EFF_DT = 10;
		public static final int CW02Q_CICL_PRI_SUB_CD_CI = 1;
		public static final int CW02Q_CICL_PRI_SUB_CD = 4;
		public static final int CW02Q_CICL_FST_NM_CI = 1;
		public static final int CW02Q_CICL_FST_NM = 30;
		public static final int CW02Q_CICL_LST_NM_CI = 1;
		public static final int CW02Q_CICL_LST_NM = 60;
		public static final int CW02Q_CICL_MDL_NM_CI = 1;
		public static final int CW02Q_CICL_MDL_NM = 30;
		public static final int CW02Q_NM_PFX_CI = 1;
		public static final int CW02Q_NM_PFX = 4;
		public static final int CW02Q_NM_SFX_CI = 1;
		public static final int CW02Q_NM_SFX = 4;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD_CI = 1;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD_NI = 1;
		public static final int CW02Q_PRIMARY_PRO_DSN_CD = 4;
		public static final int CW02Q_LEG_ENT_CD_CI = 1;
		public static final int CW02Q_LEG_ENT_CD = 2;
		public static final int CW02Q_CICL_SDX_CD_CI = 1;
		public static final int CW02Q_CICL_SDX_CD = 8;
		public static final int CW02Q_CICL_OGN_INCEPT_DT_CI = 1;
		public static final int CW02Q_CICL_OGN_INCEPT_DT_NI = 1;
		public static final int CW02Q_CICL_OGN_INCEPT_DT = 10;
		public static final int CW02Q_CICL_ADD_NM_IND_CI = 1;
		public static final int CW02Q_CICL_ADD_NM_IND_NI = 1;
		public static final int CW02Q_CICL_ADD_NM_IND = 1;
		public static final int CW02Q_CICL_DOB_DT_CI = 1;
		public static final int CW02Q_CICL_DOB_DT = 10;
		public static final int CW02Q_CICL_BIR_ST_CD_CI = 1;
		public static final int CW02Q_CICL_BIR_ST_CD = 3;
		public static final int CW02Q_GENDER_CD_CI = 1;
		public static final int CW02Q_GENDER_CD = 1;
		public static final int CW02Q_PRI_LGG_CD_CI = 1;
		public static final int CW02Q_PRI_LGG_CD = 3;
		public static final int CW02Q_USER_ID_CI = 1;
		public static final int CW02Q_USER_ID = 8;
		public static final int CW02Q_STATUS_CD_CI = 1;
		public static final int CW02Q_STATUS_CD = 1;
		public static final int CW02Q_TERMINAL_ID_CI = 1;
		public static final int CW02Q_TERMINAL_ID = 8;
		public static final int CW02Q_CICL_EXP_DT_CI = 1;
		public static final int CW02Q_CICL_EXP_DT = 10;
		public static final int CW02Q_CICL_EFF_ACY_TS_CI = 1;
		public static final int CW02Q_CICL_EFF_ACY_TS = 26;
		public static final int CW02Q_CICL_EXP_ACY_TS_CI = 1;
		public static final int CW02Q_CICL_EXP_ACY_TS = 26;
		public static final int CW02Q_STATUTORY_TLE_CD_CI = 1;
		public static final int CW02Q_STATUTORY_TLE_CD = 3;
		public static final int CW02Q_CICL_LNG_NM_CI = 1;
		public static final int CW02Q_CICL_LNG_NM_NI = 1;
		public static final int CW02Q_CICL_LNG_NM = 132;
		public static final int CW02Q_LO_LST_NM_MCH_CD_CI = 1;
		public static final int CW02Q_LO_LST_NM_MCH_CD_NI = 1;
		public static final int CW02Q_LO_LST_NM_MCH_CD = 35;
		public static final int CW02Q_HI_LST_NM_MCH_CD_CI = 1;
		public static final int CW02Q_HI_LST_NM_MCH_CD_NI = 1;
		public static final int CW02Q_HI_LST_NM_MCH_CD = 35;
		public static final int CW02Q_LO_FST_NM_MCH_CD_CI = 1;
		public static final int CW02Q_LO_FST_NM_MCH_CD_NI = 1;
		public static final int CW02Q_LO_FST_NM_MCH_CD = 35;
		public static final int CW02Q_HI_FST_NM_MCH_CD_CI = 1;
		public static final int CW02Q_HI_FST_NM_MCH_CD_NI = 1;
		public static final int CW02Q_HI_FST_NM_MCH_CD = 35;
		public static final int CW02Q_CICL_ACQ_SRC_CD_CI = 1;
		public static final int CW02Q_CICL_ACQ_SRC_CD_NI = 1;
		public static final int CW02Q_CICL_ACQ_SRC_CD = 3;
		public static final int CW02Q_LEG_ENT_DESC = 40;
		public static final int CWCACQ_CLIENT_ADDR_COMP_CHKSUM = 9;
		public static final int CWCACQ_ADR_ID_KCRE = 32;
		public static final int CWCACQ_CLIENT_ID_KCRE = 32;
		public static final int CWCACQ_ADR_SEQ_NBR_KCRE = 32;
		public static final int CWCACQ_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CWCACQ_ADR_ID_CI = 1;
		public static final int CWCACQ_ADR_ID = 20;
		public static final int CWCACQ_CLIENT_ID_CI = 1;
		public static final int CWCACQ_CLIENT_ID = 20;
		public static final int CWCACQ_HISTORY_VLD_NBR_CI = 1;
		public static final int CWCACQ_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CWCACQ_HISTORY_VLD_NBR = 5;
		public static final int CWCACQ_ADR_SEQ_NBR_CI = 1;
		public static final int CWCACQ_ADR_SEQ_NBR_SIGN = 1;
		public static final int CWCACQ_ADR_SEQ_NBR = 5;
		public static final int CWCACQ_CIAR_EFF_DT_CI = 1;
		public static final int CWCACQ_CIAR_EFF_DT = 10;
		public static final int CWCACQ_TRANS_PROCESS_DT = 10;
		public static final int CWCACQ_USER_ID_CI = 1;
		public static final int CWCACQ_USER_ID = 8;
		public static final int CWCACQ_TERMINAL_ID_CI = 1;
		public static final int CWCACQ_TERMINAL_ID = 8;
		public static final int CWCACQ_CICA_ADR1_CI = 1;
		public static final int CWCACQ_CICA_ADR1 = 45;
		public static final int CWCACQ_CICA_ADR2_CI = 1;
		public static final int CWCACQ_CICA_ADR2_NI = 1;
		public static final int CWCACQ_CICA_ADR2 = 45;
		public static final int CWCACQ_CICA_CIT_NM_CI = 1;
		public static final int CWCACQ_CICA_CIT_NM = 30;
		public static final int CWCACQ_CICA_CTY_CI = 1;
		public static final int CWCACQ_CICA_CTY_NI = 1;
		public static final int CWCACQ_CICA_CTY = 30;
		public static final int CWCACQ_ST_CD_CI = 1;
		public static final int CWCACQ_ST_CD = 3;
		public static final int CWCACQ_CICA_PST_CD_CI = 1;
		public static final int CWCACQ_CICA_PST_CD = 13;
		public static final int CWCACQ_CTR_CD_CI = 1;
		public static final int CWCACQ_CTR_CD = 4;
		public static final int CWCACQ_CICA_ADD_ADR_IND_CI = 1;
		public static final int CWCACQ_CICA_ADD_ADR_IND_NI = 1;
		public static final int CWCACQ_CICA_ADD_ADR_IND = 1;
		public static final int CWCACQ_ADDR_STATUS_CD_CI = 1;
		public static final int CWCACQ_ADDR_STATUS_CD = 1;
		public static final int CWCACQ_ADD_NM_IND_CI = 1;
		public static final int CWCACQ_ADD_NM_IND = 1;
		public static final int CWCACQ_ADR_TYP_CD_CI = 1;
		public static final int CWCACQ_ADR_TYP_CD = 4;
		public static final int CWCACQ_TCH_OBJECT_KEY_CI = 1;
		public static final int CWCACQ_TCH_OBJECT_KEY = 20;
		public static final int CWCACQ_CIAR_EXP_DT_CI = 10;
		public static final int CWCACQ_CIAR_EXP_DT = 10;
		public static final int CWCACQ_CIAR_SER_ADR1_TXT_CI = 1;
		public static final int CWCACQ_CIAR_SER_ADR1_TXT = 8;
		public static final int CWCACQ_CIAR_SER_CIT_NM_CI = 1;
		public static final int CWCACQ_CIAR_SER_CIT_NM = 5;
		public static final int CWCACQ_CIAR_SER_ST_CD_CI = 1;
		public static final int CWCACQ_CIAR_SER_ST_CD = 3;
		public static final int CWCACQ_CIAR_SER_PST_CD_CI = 1;
		public static final int CWCACQ_CIAR_SER_PST_CD = 6;
		public static final int CWCACQ_REL_STATUS_CD_CI = 1;
		public static final int CWCACQ_REL_STATUS_CD = 1;
		public static final int CWCACQ_EFF_ACY_TS_CI = 1;
		public static final int CWCACQ_EFF_ACY_TS = 26;
		public static final int CWCACQ_CIAR_EXP_ACY_TS_CI = 1;
		public static final int CWCACQ_CIAR_EXP_ACY_TS = 26;
		public static final int CWCACQ_MORE_ROWS_SW = 1;
		public static final int CWCACQ_CICA_SHW_OBJ_KEY_CI = 1;
		public static final int CWCACQ_CICA_SHW_OBJ_KEY = 30;
		public static final int CWCACQ_BUS_OBJ_NM = 32;
		public static final int CWCACQ_ST_DESC = 40;
		public static final int CWCACQ_ADR_TYP_DESC = 40;
		public static final int CW06Q_CLT_CLT_RELATION_CSUM = 9;
		public static final int CW06Q_CLIENT_ID_KCRE = 32;
		public static final int CW06Q_CLT_TYP_CD_KCRE = 32;
		public static final int CW06Q_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW06Q_CICR_XRF_ID_KCRE = 32;
		public static final int CW06Q_XRF_TYP_CD_KCRE = 32;
		public static final int CW06Q_CICR_EFF_DT_KCRE = 32;
		public static final int CW06Q_TRANS_PROCESS_DT = 10;
		public static final int CW06Q_CLIENT_ID_CI = 1;
		public static final int CW06Q_CLIENT_ID = 20;
		public static final int CW06Q_CLT_TYP_CD_CI = 1;
		public static final int CW06Q_CLT_TYP_CD = 4;
		public static final int CW06Q_HISTORY_VLD_NBR_CI = 1;
		public static final int CW06Q_HISTORY_VLD_NBR_SIGNED = 6;
		public static final int CW06Q_CICR_XRF_ID_CI = 1;
		public static final int CW06Q_CICR_XRF_ID = 20;
		public static final int CW06Q_XRF_TYP_CD_CI = 1;
		public static final int CW06Q_XRF_TYP_CD = 4;
		public static final int CW06Q_CICR_EFF_DT_CI = 1;
		public static final int CW06Q_CICR_EFF_DT = 10;
		public static final int CW06Q_CICR_EXP_DT_CI = 1;
		public static final int CW06Q_CICR_EXP_DT = 10;
		public static final int CW06Q_CICR_NBR_OPR_STRS_CI = 1;
		public static final int CW06Q_CICR_NBR_OPR_STRS_NI = 1;
		public static final int CW06Q_CICR_NBR_OPR_STRS = 3;
		public static final int CW06Q_USER_ID_CI = 1;
		public static final int CW06Q_USER_ID = 8;
		public static final int CW06Q_STATUS_CD_CI = 1;
		public static final int CW06Q_STATUS_CD = 1;
		public static final int CW06Q_TERMINAL_ID_CI = 1;
		public static final int CW06Q_TERMINAL_ID = 8;
		public static final int CW06Q_CICR_EFF_ACY_TS_CI = 1;
		public static final int CW06Q_CICR_EFF_ACY_TS = 26;
		public static final int CW06Q_CICR_EXP_ACY_TS_CI = 1;
		public static final int CW06Q_CICR_EXP_ACY_TS = 26;
		public static final int CW08Q_CLT_OBJ_RELATION_CSUM = 9;
		public static final int CW08Q_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CW08Q_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CW08Q_CIOR_EFF_DT_KCRE = 32;
		public static final int CW08Q_OBJ_SYS_ID_KCRE = 32;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_KCRE = 32;
		public static final int CW08Q_CLIENT_ID_KCRE = 32;
		public static final int CW08Q_TRANS_PROCESS_DT = 10;
		public static final int CW08Q_TCH_OBJECT_KEY_CI = 1;
		public static final int CW08Q_TCH_OBJECT_KEY = 20;
		public static final int CW08Q_HISTORY_VLD_NBR_CI = 1;
		public static final int CW08Q_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CW08Q_HISTORY_VLD_NBR = 5;
		public static final int CW08Q_CIOR_EFF_DT_CI = 1;
		public static final int CW08Q_CIOR_EFF_DT = 10;
		public static final int CW08Q_OBJ_SYS_ID_CI = 1;
		public static final int CW08Q_OBJ_SYS_ID = 4;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_CI = 1;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR_SIGN = 1;
		public static final int CW08Q_CIOR_OBJ_SEQ_NBR = 5;
		public static final int CW08Q_CLIENT_ID_CI = 1;
		public static final int CW08Q_CLIENT_ID = 20;
		public static final int CW08Q_RLT_TYP_CD_CI = 1;
		public static final int CW08Q_RLT_TYP_CD = 4;
		public static final int CW08Q_OBJ_CD_CI = 1;
		public static final int CW08Q_OBJ_CD = 4;
		public static final int CW08Q_CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int CW08Q_CIOR_SHW_OBJ_KEY = 30;
		public static final int CW08Q_ADR_SEQ_NBR_CI = 1;
		public static final int CW08Q_ADR_SEQ_NBR_SIGN = 1;
		public static final int CW08Q_ADR_SEQ_NBR = 5;
		public static final int CW08Q_USER_ID_CI = 1;
		public static final int CW08Q_USER_ID = 8;
		public static final int CW08Q_STATUS_CD_CI = 1;
		public static final int CW08Q_STATUS_CD = 1;
		public static final int CW08Q_TERMINAL_ID_CI = 1;
		public static final int CW08Q_TERMINAL_ID = 8;
		public static final int CW08Q_CIOR_EXP_DT_CI = 1;
		public static final int CW08Q_CIOR_EXP_DT = 10;
		public static final int CW08Q_CIOR_EFF_ACY_TS_CI = 1;
		public static final int CW08Q_CIOR_EFF_ACY_TS = 26;
		public static final int CW08Q_CIOR_EXP_ACY_TS_CI = 1;
		public static final int CW08Q_CIOR_EXP_ACY_TS = 26;
		public static final int CW08Q_APP_TYPE = 1;
		public static final int CW08Q_BUS_OBJ_NM = 32;
		public static final int CW08Q_OBJ_DESC = 40;
		public static final int CWORC_CLT_OBJ_RELATION_CSUM = 9;
		public static final int CWORC_TCH_OBJECT_KEY_KCRE = 32;
		public static final int CWORC_HISTORY_VLD_NBR_KCRE = 32;
		public static final int CWORC_CIOR_EFF_DT_KCRE = 32;
		public static final int CWORC_OBJ_SYS_ID_KCRE = 32;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_KCRE = 32;
		public static final int CWORC_CLIENT_ID_KCRE = 32;
		public static final int CWORC_TRANS_PROCESS_DT = 10;
		public static final int CWORC_TCH_OBJECT_KEY_CI = 1;
		public static final int CWORC_TCH_OBJECT_KEY = 20;
		public static final int CWORC_HISTORY_VLD_NBR_CI = 1;
		public static final int CWORC_HISTORY_VLD_NBR_SIGN = 1;
		public static final int CWORC_HISTORY_VLD_NBR = 5;
		public static final int CWORC_CIOR_EFF_DT_CI = 1;
		public static final int CWORC_CIOR_EFF_DT = 10;
		public static final int CWORC_OBJ_SYS_ID_CI = 1;
		public static final int CWORC_OBJ_SYS_ID = 4;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_CI = 1;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR_SIGN = 1;
		public static final int CWORC_CIOR_OBJ_SEQ_NBR = 5;
		public static final int CWORC_CLIENT_ID_CI = 1;
		public static final int CWORC_CLIENT_ID = 20;
		public static final int CWORC_RLT_TYP_CD_CI = 1;
		public static final int CWORC_RLT_TYP_CD = 4;
		public static final int CWORC_OBJ_CD_CI = 1;
		public static final int CWORC_OBJ_CD = 4;
		public static final int CWORC_CIOR_SHW_OBJ_KEY_CI = 1;
		public static final int CWORC_CIOR_SHW_OBJ_KEY = 30;
		public static final int CWORC_ADR_SEQ_NBR_CI = 1;
		public static final int CWORC_ADR_SEQ_NBR_SIGN = 1;
		public static final int CWORC_ADR_SEQ_NBR = 5;
		public static final int CWORC_USER_ID_CI = 1;
		public static final int CWORC_USER_ID = 8;
		public static final int CWORC_STATUS_CD_CI = 1;
		public static final int CWORC_STATUS_CD = 1;
		public static final int CWORC_TERMINAL_ID_CI = 1;
		public static final int CWORC_TERMINAL_ID = 8;
		public static final int CWORC_CIOR_EXP_DT_CI = 1;
		public static final int CWORC_CIOR_EXP_DT = 10;
		public static final int CWORC_CIOR_EFF_ACY_TS_CI = 1;
		public static final int CWORC_CIOR_EFF_ACY_TS = 26;
		public static final int CWORC_CIOR_EXP_ACY_TS_CI = 1;
		public static final int CWORC_CIOR_EXP_ACY_TS = 26;
		public static final int CWORC_CIOR_LEG_ENT_CD_CI = 1;
		public static final int CWORC_CIOR_LEG_ENT_CD = 2;
		public static final int CWORC_CIOR_FST_NM_CI = 1;
		public static final int CWORC_CIOR_FST_NM = 30;
		public static final int CWORC_CIOR_LST_NM_CI = 1;
		public static final int CWORC_CIOR_LST_NM = 60;
		public static final int CWORC_CIOR_MDL_NM_CI = 1;
		public static final int CWORC_CIOR_MDL_NM = 30;
		public static final int CWORC_CIOR_NM_PFX_CI = 1;
		public static final int CWORC_CIOR_NM_PFX = 4;
		public static final int CWORC_CIOR_NM_SFX_CI = 1;
		public static final int CWORC_CIOR_NM_SFX = 4;
		public static final int CWORC_CIOR_LNG_NM_CI = 1;
		public static final int CWORC_CIOR_LNG_NM_NI = 1;
		public static final int CWORC_CIOR_LNG_NM = 132;
		public static final int CWGIKQ_CLT_OBJ_RELATION = CWGIKQ_TCH_OBJECT_KEY + CWGIKQ_RLT_TYP_CD + CWGIKQ_OBJ_CD + CWGIKQ_SHW_OBJ_KEY
				+ CWGIKQ_OBJ_SYS_ID;
		public static final int CWGIKQ_CLIENT_PHONE = CWGIKQ_PHN_NBR + CWGIKQ_PHN_TYP_CD;
		public static final int CWGIKQ_CLIENT_EMAIL = CWGIKQ_EMAIL_ADR_TXT + CWGIKQ_EMAIL_TYPE_CD;
		public static final int CWGIKQ_CLIENT_TAX = CWGIKQ_TAX_TYPE_CD + CWGIKQ_CITX_TAX_ID;
		public static final int CWGIKQ_INQUIRE_KEY_ROW = CWGIKQ_CLT_OBJ_RELATION + CWGIKQ_CLIENT_PHONE + CWGIKQ_CLIENT_EMAIL + CWGIKQ_CLIENT_TAX
				+ CWGIKQ_POL_NBR + CWGIKQ_EFF_DT + CWGIKQ_EXP_DT;
		public static final int L_FW_REQ_LOC_CLT_LIST = CWGIKQ_INQUIRE_KEY_ROW;
		public static final int CW02Q_CLIENT_TAB_FIXED = CW02Q_CLIENT_TAB_CHK_SUM + CW02Q_CLIENT_ID_KCRE;
		public static final int CW02Q_CLIENT_TAB_DATES = CW02Q_TRANS_PROCESS_DT;
		public static final int CW02Q_CLIENT_TAB_KEY = CW02Q_CLIENT_ID_CI + CW02Q_CLIENT_ID + CW02Q_HISTORY_VLD_NBR_CI + CW02Q_HISTORY_VLD_NBR_SIGN
				+ CW02Q_HISTORY_VLD_NBR + CW02Q_CICL_EFF_DT_CI + CW02Q_CICL_EFF_DT;
		public static final int CW02Q_CLIENT_TAB_DATA = CW02Q_CICL_PRI_SUB_CD_CI + CW02Q_CICL_PRI_SUB_CD + CW02Q_CICL_FST_NM_CI + CW02Q_CICL_FST_NM
				+ CW02Q_CICL_LST_NM_CI + CW02Q_CICL_LST_NM + CW02Q_CICL_MDL_NM_CI + CW02Q_CICL_MDL_NM + CW02Q_NM_PFX_CI + CW02Q_NM_PFX
				+ CW02Q_NM_SFX_CI + CW02Q_NM_SFX + CW02Q_PRIMARY_PRO_DSN_CD_CI + CW02Q_PRIMARY_PRO_DSN_CD_NI + CW02Q_PRIMARY_PRO_DSN_CD
				+ CW02Q_LEG_ENT_CD_CI + CW02Q_LEG_ENT_CD + CW02Q_CICL_SDX_CD_CI + CW02Q_CICL_SDX_CD + CW02Q_CICL_OGN_INCEPT_DT_CI
				+ CW02Q_CICL_OGN_INCEPT_DT_NI + CW02Q_CICL_OGN_INCEPT_DT + CW02Q_CICL_ADD_NM_IND_CI + CW02Q_CICL_ADD_NM_IND_NI + CW02Q_CICL_ADD_NM_IND
				+ CW02Q_CICL_DOB_DT_CI + CW02Q_CICL_DOB_DT + CW02Q_CICL_BIR_ST_CD_CI + CW02Q_CICL_BIR_ST_CD + CW02Q_GENDER_CD_CI + CW02Q_GENDER_CD
				+ CW02Q_PRI_LGG_CD_CI + CW02Q_PRI_LGG_CD + CW02Q_USER_ID_CI + CW02Q_USER_ID + CW02Q_STATUS_CD_CI + CW02Q_STATUS_CD
				+ CW02Q_TERMINAL_ID_CI + CW02Q_TERMINAL_ID + CW02Q_CICL_EXP_DT_CI + CW02Q_CICL_EXP_DT + CW02Q_CICL_EFF_ACY_TS_CI
				+ CW02Q_CICL_EFF_ACY_TS + CW02Q_CICL_EXP_ACY_TS_CI + CW02Q_CICL_EXP_ACY_TS + CW02Q_STATUTORY_TLE_CD_CI + CW02Q_STATUTORY_TLE_CD
				+ CW02Q_CICL_LNG_NM_CI + CW02Q_CICL_LNG_NM_NI + CW02Q_CICL_LNG_NM + CW02Q_LO_LST_NM_MCH_CD_CI + CW02Q_LO_LST_NM_MCH_CD_NI
				+ CW02Q_LO_LST_NM_MCH_CD + CW02Q_HI_LST_NM_MCH_CD_CI + CW02Q_HI_LST_NM_MCH_CD_NI + CW02Q_HI_LST_NM_MCH_CD + CW02Q_LO_FST_NM_MCH_CD_CI
				+ CW02Q_LO_FST_NM_MCH_CD_NI + CW02Q_LO_FST_NM_MCH_CD + CW02Q_HI_FST_NM_MCH_CD_CI + CW02Q_HI_FST_NM_MCH_CD_NI + CW02Q_HI_FST_NM_MCH_CD
				+ CW02Q_CICL_ACQ_SRC_CD_CI + CW02Q_CICL_ACQ_SRC_CD_NI + CW02Q_CICL_ACQ_SRC_CD + CW02Q_LEG_ENT_DESC;
		public static final int CW02Q_CLIENT_TAB_ROW = CW02Q_CLIENT_TAB_FIXED + CW02Q_CLIENT_TAB_DATES + CW02Q_CLIENT_TAB_KEY + CW02Q_CLIENT_TAB_DATA;
		public static final int L_FW_REQ_CLIENT_TAB = CW02Q_CLIENT_TAB_ROW;
		public static final int CWCACQ_CLIENT_ADDR_COMP_FIXED = CWCACQ_CLIENT_ADDR_COMP_CHKSUM + CWCACQ_ADR_ID_KCRE + CWCACQ_CLIENT_ID_KCRE
				+ CWCACQ_ADR_SEQ_NBR_KCRE + CWCACQ_TCH_OBJECT_KEY_KCRE;
		public static final int CWCACQ_CLIENT_ADDRESS_KEY = CWCACQ_ADR_ID_CI + CWCACQ_ADR_ID;
		public static final int CWCACQ_CLT_ADR_RELATION_KEY = CWCACQ_CLIENT_ID_CI + CWCACQ_CLIENT_ID + CWCACQ_HISTORY_VLD_NBR_CI
				+ CWCACQ_HISTORY_VLD_NBR_SIGN + CWCACQ_HISTORY_VLD_NBR + CWCACQ_ADR_SEQ_NBR_CI + CWCACQ_ADR_SEQ_NBR_SIGN + CWCACQ_ADR_SEQ_NBR
				+ CWCACQ_CIAR_EFF_DT_CI + CWCACQ_CIAR_EFF_DT;
		public static final int CWCACQ_CLIENT_ADDR_COMP_DATES = CWCACQ_TRANS_PROCESS_DT;
		public static final int CWCACQ_CLIENT_ADDR_COMP_DATA = CWCACQ_USER_ID_CI + CWCACQ_USER_ID + CWCACQ_TERMINAL_ID_CI + CWCACQ_TERMINAL_ID
				+ CWCACQ_CICA_ADR1_CI + CWCACQ_CICA_ADR1 + CWCACQ_CICA_ADR2_CI + CWCACQ_CICA_ADR2_NI + CWCACQ_CICA_ADR2 + CWCACQ_CICA_CIT_NM_CI
				+ CWCACQ_CICA_CIT_NM + CWCACQ_CICA_CTY_CI + CWCACQ_CICA_CTY_NI + CWCACQ_CICA_CTY + CWCACQ_ST_CD_CI + CWCACQ_ST_CD
				+ CWCACQ_CICA_PST_CD_CI + CWCACQ_CICA_PST_CD + CWCACQ_CTR_CD_CI + CWCACQ_CTR_CD + CWCACQ_CICA_ADD_ADR_IND_CI
				+ CWCACQ_CICA_ADD_ADR_IND_NI + CWCACQ_CICA_ADD_ADR_IND + CWCACQ_ADDR_STATUS_CD_CI + CWCACQ_ADDR_STATUS_CD + CWCACQ_ADD_NM_IND_CI
				+ CWCACQ_ADD_NM_IND + CWCACQ_ADR_TYP_CD_CI + CWCACQ_ADR_TYP_CD + CWCACQ_TCH_OBJECT_KEY_CI + CWCACQ_TCH_OBJECT_KEY
				+ CWCACQ_CIAR_EXP_DT_CI + CWCACQ_CIAR_EXP_DT + CWCACQ_CIAR_SER_ADR1_TXT_CI + CWCACQ_CIAR_SER_ADR1_TXT + CWCACQ_CIAR_SER_CIT_NM_CI
				+ CWCACQ_CIAR_SER_CIT_NM + CWCACQ_CIAR_SER_ST_CD_CI + CWCACQ_CIAR_SER_ST_CD + CWCACQ_CIAR_SER_PST_CD_CI + CWCACQ_CIAR_SER_PST_CD
				+ CWCACQ_REL_STATUS_CD_CI + CWCACQ_REL_STATUS_CD + CWCACQ_EFF_ACY_TS_CI + CWCACQ_EFF_ACY_TS + CWCACQ_CIAR_EXP_ACY_TS_CI
				+ CWCACQ_CIAR_EXP_ACY_TS + CWCACQ_MORE_ROWS_SW + CWCACQ_CICA_SHW_OBJ_KEY_CI + CWCACQ_CICA_SHW_OBJ_KEY + CWCACQ_BUS_OBJ_NM
				+ CWCACQ_ST_DESC + CWCACQ_ADR_TYP_DESC;
		public static final int CWCACQ_CLIENT_ADDR_COMPOSITE = CWCACQ_CLIENT_ADDR_COMP_FIXED + CWCACQ_CLIENT_ADDRESS_KEY + CWCACQ_CLT_ADR_RELATION_KEY
				+ CWCACQ_CLIENT_ADDR_COMP_DATES + CWCACQ_CLIENT_ADDR_COMP_DATA;
		public static final int L_FW_REQ_CLT_ADR_RELATION = CWCACQ_CLIENT_ADDR_COMPOSITE;
		public static final int CW06Q_CLT_CLT_RELATION_FIXED = CW06Q_CLT_CLT_RELATION_CSUM + CW06Q_CLIENT_ID_KCRE + CW06Q_CLT_TYP_CD_KCRE
				+ CW06Q_HISTORY_VLD_NBR_KCRE + CW06Q_CICR_XRF_ID_KCRE + CW06Q_XRF_TYP_CD_KCRE + CW06Q_CICR_EFF_DT_KCRE;
		public static final int CW06Q_CLT_CLT_RELATION_DATES = CW06Q_TRANS_PROCESS_DT;
		public static final int CW06Q_CLT_CLT_RELATION_KEY = CW06Q_CLIENT_ID_CI + CW06Q_CLIENT_ID + CW06Q_CLT_TYP_CD_CI + CW06Q_CLT_TYP_CD
				+ CW06Q_HISTORY_VLD_NBR_CI + CW06Q_HISTORY_VLD_NBR_SIGNED + CW06Q_CICR_XRF_ID_CI + CW06Q_CICR_XRF_ID + CW06Q_XRF_TYP_CD_CI
				+ CW06Q_XRF_TYP_CD + CW06Q_CICR_EFF_DT_CI + CW06Q_CICR_EFF_DT;
		public static final int CW06Q_CLT_CLT_RELATION_DATA = CW06Q_CICR_EXP_DT_CI + CW06Q_CICR_EXP_DT + CW06Q_CICR_NBR_OPR_STRS_CI
				+ CW06Q_CICR_NBR_OPR_STRS_NI + CW06Q_CICR_NBR_OPR_STRS + CW06Q_USER_ID_CI + CW06Q_USER_ID + CW06Q_STATUS_CD_CI + CW06Q_STATUS_CD
				+ CW06Q_TERMINAL_ID_CI + CW06Q_TERMINAL_ID + CW06Q_CICR_EFF_ACY_TS_CI + CW06Q_CICR_EFF_ACY_TS + CW06Q_CICR_EXP_ACY_TS_CI
				+ CW06Q_CICR_EXP_ACY_TS;
		public static final int CW06Q_CLT_CLT_RELATION_ROW = CW06Q_CLT_CLT_RELATION_FIXED + CW06Q_CLT_CLT_RELATION_DATES + CW06Q_CLT_CLT_RELATION_KEY
				+ CW06Q_CLT_CLT_RELATION_DATA;
		public static final int L_FW_REQ_CLT_CLT_REL = CW06Q_CLT_CLT_RELATION_ROW;
		public static final int CW08Q_CLT_OBJ_RELATION_FIXED = CW08Q_CLT_OBJ_RELATION_CSUM + CW08Q_TCH_OBJECT_KEY_KCRE + CW08Q_HISTORY_VLD_NBR_KCRE
				+ CW08Q_CIOR_EFF_DT_KCRE + CW08Q_OBJ_SYS_ID_KCRE + CW08Q_CIOR_OBJ_SEQ_NBR_KCRE + CW08Q_CLIENT_ID_KCRE;
		public static final int CW08Q_CLT_OBJ_RELATION_DATES = CW08Q_TRANS_PROCESS_DT;
		public static final int CW08Q_CLT_OBJ_RELATION_KEY = CW08Q_TCH_OBJECT_KEY_CI + CW08Q_TCH_OBJECT_KEY + CW08Q_HISTORY_VLD_NBR_CI
				+ CW08Q_HISTORY_VLD_NBR_SIGN + CW08Q_HISTORY_VLD_NBR + CW08Q_CIOR_EFF_DT_CI + CW08Q_CIOR_EFF_DT + CW08Q_OBJ_SYS_ID_CI
				+ CW08Q_OBJ_SYS_ID + CW08Q_CIOR_OBJ_SEQ_NBR_CI + CW08Q_CIOR_OBJ_SEQ_NBR_SIGN + CW08Q_CIOR_OBJ_SEQ_NBR;
		public static final int CW08Q_CLT_OBJ_RELATION_DATA = CW08Q_CLIENT_ID_CI + CW08Q_CLIENT_ID + CW08Q_RLT_TYP_CD_CI + CW08Q_RLT_TYP_CD
				+ CW08Q_OBJ_CD_CI + CW08Q_OBJ_CD + CW08Q_CIOR_SHW_OBJ_KEY_CI + CW08Q_CIOR_SHW_OBJ_KEY + CW08Q_ADR_SEQ_NBR_CI + CW08Q_ADR_SEQ_NBR_SIGN
				+ CW08Q_ADR_SEQ_NBR + CW08Q_USER_ID_CI + CW08Q_USER_ID + CW08Q_STATUS_CD_CI + CW08Q_STATUS_CD + CW08Q_TERMINAL_ID_CI
				+ CW08Q_TERMINAL_ID + CW08Q_CIOR_EXP_DT_CI + CW08Q_CIOR_EXP_DT + CW08Q_CIOR_EFF_ACY_TS_CI + CW08Q_CIOR_EFF_ACY_TS
				+ CW08Q_CIOR_EXP_ACY_TS_CI + CW08Q_CIOR_EXP_ACY_TS + CW08Q_APP_TYPE + CW08Q_BUS_OBJ_NM + CW08Q_OBJ_DESC;
		public static final int CW08Q_CLT_OBJ_RELATION_ROW = CW08Q_CLT_OBJ_RELATION_FIXED + CW08Q_CLT_OBJ_RELATION_DATES + CW08Q_CLT_OBJ_RELATION_KEY
				+ CW08Q_CLT_OBJ_RELATION_DATA;
		public static final int L_FW_REQ_CLT_OBJ_REL = CW08Q_CLT_OBJ_RELATION_ROW;
		public static final int CWORC_CLT_OBJ_RELATION_FIXED = CWORC_CLT_OBJ_RELATION_CSUM + CWORC_TCH_OBJECT_KEY_KCRE + CWORC_HISTORY_VLD_NBR_KCRE
				+ CWORC_CIOR_EFF_DT_KCRE + CWORC_OBJ_SYS_ID_KCRE + CWORC_CIOR_OBJ_SEQ_NBR_KCRE + CWORC_CLIENT_ID_KCRE;
		public static final int CWORC_CLT_OBJ_RELATION_DATES = CWORC_TRANS_PROCESS_DT;
		public static final int CWORC_CLT_OBJ_RELATION_KEY = CWORC_TCH_OBJECT_KEY_CI + CWORC_TCH_OBJECT_KEY + CWORC_HISTORY_VLD_NBR_CI
				+ CWORC_HISTORY_VLD_NBR_SIGN + CWORC_HISTORY_VLD_NBR + CWORC_CIOR_EFF_DT_CI + CWORC_CIOR_EFF_DT + CWORC_OBJ_SYS_ID_CI
				+ CWORC_OBJ_SYS_ID + CWORC_CIOR_OBJ_SEQ_NBR_CI + CWORC_CIOR_OBJ_SEQ_NBR_SIGN + CWORC_CIOR_OBJ_SEQ_NBR;
		public static final int CWORC_CLT_OBJ_RELATION_DATA = CWORC_CLIENT_ID_CI + CWORC_CLIENT_ID + CWORC_RLT_TYP_CD_CI + CWORC_RLT_TYP_CD
				+ CWORC_OBJ_CD_CI + CWORC_OBJ_CD + CWORC_CIOR_SHW_OBJ_KEY_CI + CWORC_CIOR_SHW_OBJ_KEY + CWORC_ADR_SEQ_NBR_CI + CWORC_ADR_SEQ_NBR_SIGN
				+ CWORC_ADR_SEQ_NBR + CWORC_USER_ID_CI + CWORC_USER_ID + CWORC_STATUS_CD_CI + CWORC_STATUS_CD + CWORC_TERMINAL_ID_CI
				+ CWORC_TERMINAL_ID + CWORC_CIOR_EXP_DT_CI + CWORC_CIOR_EXP_DT + CWORC_CIOR_EFF_ACY_TS_CI + CWORC_CIOR_EFF_ACY_TS
				+ CWORC_CIOR_EXP_ACY_TS_CI + CWORC_CIOR_EXP_ACY_TS + CWORC_CIOR_LEG_ENT_CD_CI + CWORC_CIOR_LEG_ENT_CD + CWORC_CIOR_FST_NM_CI
				+ CWORC_CIOR_FST_NM + CWORC_CIOR_LST_NM_CI + CWORC_CIOR_LST_NM + CWORC_CIOR_MDL_NM_CI + CWORC_CIOR_MDL_NM + CWORC_CIOR_NM_PFX_CI
				+ CWORC_CIOR_NM_PFX + CWORC_CIOR_NM_SFX_CI + CWORC_CIOR_NM_SFX + CWORC_CIOR_LNG_NM_CI + CWORC_CIOR_LNG_NM_NI + CWORC_CIOR_LNG_NM;
		public static final int CWORC_FILTER_TYPE = 2;
		public static final int CWORC_CLT_OBJ_INPUT_DATA = CWORC_FILTER_TYPE;
		public static final int CWORC_CLT_OBJ_RELATION_ROW = CWORC_CLT_OBJ_RELATION_FIXED + CWORC_CLT_OBJ_RELATION_DATES + CWORC_CLT_OBJ_RELATION_KEY
				+ CWORC_CLT_OBJ_RELATION_DATA + CWORC_CLT_OBJ_INPUT_DATA;
		public static final int L_FW_REQ_REL_CLT_OBJ_REL = CWORC_CLT_OBJ_RELATION_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_LOC_CLT_LIST + L_FW_REQ_CLIENT_TAB + L_FW_REQ_CLT_ADR_RELATION
				+ L_FW_REQ_CLT_CLT_REL + L_FW_REQ_CLT_OBJ_REL + L_FW_REQ_REL_CLT_OBJ_REL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int CW06Q_HISTORY_VLD_NBR_SIGNED = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
