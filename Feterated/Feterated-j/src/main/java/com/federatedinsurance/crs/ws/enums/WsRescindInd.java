/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-RESCIND-IND<br>
 * Variable: WS-RESCIND-IND from program XZ0B0004<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsRescindInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char YES = 'Y';
	public static final char NO = 'N';

	//==== METHODS ====
	public void setRescindInd(char rescindInd) {
		this.value = rescindInd;
	}

	public char getRescindInd() {
		return this.value;
	}

	public void setYes() {
		value = YES;
	}

	public void setNo() {
		value = NO;
	}
}
