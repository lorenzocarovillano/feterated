/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.federatedinsurance.crs.ws.DfhcommareaTs020000;
import com.federatedinsurance.crs.ws.Mu0x0004Data;
import com.federatedinsurance.crs.ws.SaveAreaMu0x0004;
import com.federatedinsurance.crs.ws.enums.WsOperationName;
import com.federatedinsurance.crs.ws.ptr.DfhcommareaMu0x0004;
import com.federatedinsurance.crs.ws.ptr.LContractLocation;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestArea;
import com.federatedinsurance.crs.ws.ptr.LFrameworkResponseArea;
import com.federatedinsurance.crs.ws.ptr.LServiceContractArea;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: MU0X0004<br>
 * <pre>AUTHOR.       FEDERATED INSURANCE.
 * DATE-WRITTEN. 06 MAR 2007.
 * ***************************************************************
 *   PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
 *                     UOW        : GET_CLIENT_DETAIL            *
 *                                : GET_CLIENT_DETAIL_NO_SWAP    *
 *                     OPERATIONS : GetInsuredDetail             *
 *                                : GetInsuredNmAdrPhnTobDetail  *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
 *                        PROGRAM OR FROM AN IVORY WRAPPER       *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         AND SHARED MEMORY                     *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         AND SHARED MEMORY                     *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    07JUL05   E404LJL   INITIAL TEMPLATE VERSION        *
 *  TL000113 01JUL09   E404DMA   USE NEW PROXY COMMON LOGIC      *
 *                               COPYBOOK                        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *    YJ249  15NOV07   E404EJD   INITIAL PROGRAM                 *
 *  TO07614  15DEC08   E404TWR   ADDING PERSONAL LINES ACCOUNT   *
 *                               ADDING OBJECT CODE AND          *
 *                               DESCRIPTION FOR EACH ACCOUNT    *
 *  PP01625  04/08/09  E404CPM   FPU CHANGES TO GENERIC UW FIELD *
 * IM000554-02 5/5/09  E404CPM   CREATE CAMP OPERATION FOR       *
 *                               PERFORMANCE                     *
 * TO07609-01 05/06/09 E404CPM   ADDED NEW FIELDS FOR SIM PROJECT*
 *                               SE-SIC ARRAY, NBR of EMPS, and  *
 *                               FED_ACCOUNT_INFO ST,CTY,TOWN    *
 *  PP02017  11/06/09  E404GCL   Fixed FEIN problem.             *
 *  PP02017  12/10/09  E404GCL   Removed PP02017.                *
 * TO07602-18 02/22/10 E404EJD   Added cowonwer info             *
 *  PP02384  06/04/10  E404CPM   Fix to not overlay phone numbers*
 *  PP02516  08/26/10  E404DLP   Fix to not overlay fax number   *
 *   YJ2546  01/27/11  E404CPM   FIXED ACCOUNT INFO TO RETURN    *
 *                               BETTER.                         *
 *  PP03519  05/15/12  E404AKW   CHANGED TO GET UNDERWRITING DATA*
 *                               FROM NEW BPO.                   *
 * ***************************************************************
 * ** EJD END ***</pre>*/
public class Mu0x0004 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Mu0x0004Data ws = new Mu0x0004Data();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaMu0x0004 dfhcommarea = new DfhcommareaMu0x0004(null);
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* "FRAMEWORK" REQUEST COPYBOOK FOR THIS OPERATION</pre>*/
	private LFrameworkRequestArea lFrameworkRequestArea = new LFrameworkRequestArea(null);
	/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
	 * <pre>* "FRAMEWORK" RESPONSE COPYBOOK(S) FOR THIS OPERATION
	 * * INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
	 * * THE BUSINESS FRAMEWORK.</pre>*/
	private LFrameworkResponseArea lFrameworkResponseArea = new LFrameworkResponseArea(null);
	/**Original name: L-SERVICE-CONTRACT-AREA<br>
	 * <pre>* "SERVICE" CONTRACT FOR THIS OPERATION</pre>*/
	private LServiceContractArea lServiceContractArea = new LServiceContractArea(null);
	/**Original name: L-CONTRACT-LOCATION<br>
	 * <pre>***************************************************************
	 *  TSC21PRO - PROXY COMMON LOGIC                                *
	 * ***************************************************************
	 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR COMMON LOGIC      *
	 *         COPYBOOK VERSIONING.                                  *
	 *         APPLICATION CODING.                                   *
	 *                                                               *
	 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
	 *                                                               *
	 *    WR #    DATE     EMP ID              DESCRIPTION           *
	 *  -------- --------- -------   ------------------------------- *
	 *  TS129    06MAR06   E404LJL   INITIAL VERSION                 *
	 *  TL000113 30JUN09   E404DMA   EXPAND FUNCTIONALITY SUCH THAT  *
	 *                               DATA CAN BE PASSED VIA CHANNEL  *
	 *                               & CONTAINERS, COMMAREA ONLY, OR *
	 *                               REFERENCE STORAGE (POINTERS).   *
	 * ***************************************************************</pre>*/
	private LContractLocation lContractLocation = new LContractLocation(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaMu0x0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea.assignBc(dfhcommarea);
		mainline();
		exit();
		return 0;
	}

	public static Mu0x0004 getInstance() {
		return (Programs.getInstance(Mu0x0004.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CALL-FRAMEWORK
		//              THRU 3000-EXIT.
		callFramework();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
	 *  DETERMINE METHOD OF CAPTURING INPUT CONTRACTS TO GET         *
	 *  ADDRESSABILITY TO THEM.                                      *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO WS-PROGRAM-NAME.
		ws.getWsMiscWorkFlds().setWsProgramName(ws.getConstantFields().getProgramName());
		// COB_CODE: PERFORM 2500-CAPTURE-INPUT-CONTRACTS
		//              THRU 2500-EXIT.
		captureInputContracts();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: INITIALIZE DSD-ERROR-HANDLING-PARMS
		//                      PPC-ERROR-HANDLING-PARMS.
		initDsdErrorHandlingParms();
		initPpcErrorHandlingParms();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-FW-MEMORY
		//              THRU 2100-EXIT.
		allocateFwMemory();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-USERID
		//              THRU 2200-EXIT.
		getUserid();
	}

	/**Original name: 2100-ALLOCATE-FW-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE FRAMEWORK REQUEST AND RESPONSE AREAS            *
	 *  FOR THIS OPERATION.                                          *
	 * ***************************************************************</pre>*/
	private void allocateFwMemory() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputDataSize(LFrameworkRequestArea.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getMainDriverData().getCommunicationShellCommon().getMaInputDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-POINTER.
		lFrameworkRequestArea = ((pointerManager
				.resolve(ws.getMainDriverData().getCommunicationShellCommon().getMaInputPointer(), LFrameworkRequestArea.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-REQUEST-AREA.
		initLFrameworkRequestArea();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-DATA-SIZE.
		ws.getMainDriverData().getCommunicationShellCommon().setMaOutputDataSize(LFrameworkResponseArea.Len.L_FRAMEWORK_RESPONSE_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//              SET(MA-OUTPUT-POINTER)
		//              FLENGTH(MA-OUTPUT-DATA-SIZE)
		//              INITIMG(CF-BLANK)
		//              RESP(WS-RESPONSE-CODE)
		//              RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getMainDriverData().getCommunicationShellCommon().setMaOutputPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getMainDriverData().getCommunicationShellCommon().getMaOutputDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-POINTER.
		lFrameworkResponseArea = ((pointerManager
				.resolve(ws.getMainDriverData().getCommunicationShellCommon().getMaOutputPointer(), LFrameworkResponseArea.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-RESPONSE-AREA.
		initLFrameworkResponseArea();
	}

	/**Original name: 2200-GET-USERID<br>
	 * <pre>***************************************************************
	 *  GET THE USERID FOR THIS TRANSACTION FROM CICS AND POPULATE   *
	 *  THE FRAMEWORK LEVEL USERID FIELD.  THIS CAN BE OVERRIDEN BY  *
	 *  A USER-SUPPLIED USERID IF NECESSARY.                         *
	 * ***************************************************************</pre>*/
	private void getUserid() {
		// COB_CODE: INITIALIZE CSC-AUTH-USERID.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setAuthUserid("");
		// COB_CODE: EXEC CICS ASSIGN
		//               USERID(CSC-AUTH-USERID)
		//           END-EXEC.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(execContext.getUserId());
		execContext.clearStatus();
	}

	/**Original name: 2500-CAPTURE-INPUT-CONTRACTS<br>
	 * <pre>***************************************************************
	 *  DETERMINE IF THE DATA WAS PASSED USING CHANNELS AND          *
	 *  CONTAINERS, COMMAREA AND REFERENCE STORAGE, OR COMMARE ONLY. *
	 *  BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
	 *  CONTRACT TO BE USED.                                         *
	 * ***************************************************************
	 *     IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
	 *      THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
	 *      LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
	 *      THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
	 *      COMMON COPYBOOK.</pre>*/
	private void captureInputContracts() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EIBCALEN = +0
		//                      THRU 2510-EXIT
		//               WHEN EIBCALEN = LENGTH OF PROXY-PROGRAM-COMMON
		//                             + LENGTH OF L-SERVICE-CONTRACT-AREA
		//                      THRU 2520-EXIT
		//               WHEN OTHER
		//                      THRU 2530-EXIT
		//           END-EVALUATE.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 2510-CAPTURE-FROM-CHANNEL
			//              THRU 2510-EXIT
			captureFromChannel();
		} else if (execContext.getCommAreaLen() == DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON + LServiceContractArea.Len.L_SERVICE_CONTRACT_AREA) {
			// COB_CODE: PERFORM 2520-CAPTURE-FROM-COMMAREA
			//              THRU 2520-EXIT
			captureFromCommarea();
		} else {
			// COB_CODE: PERFORM 2530-CAPTURE-FROM-REF-STORAGE
			//              THRU 2530-EXIT
			captureFromRefStorage();
		}
	}

	/**Original name: 2510-CAPTURE-FROM-CHANNEL<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
	 *  CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
	 *  CONTRACT.                                                    *
	 * ***************************************************************</pre>*/
	private void captureFromChannel() {
		// COB_CODE: PERFORM 2511-ALLOCATE-PROXY-CNT
		//              THRU 2511-EXIT.
		allocateProxyCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2512-GET-PROXY-COMMON-CNT
		//              THRU 2512-EXIT.
		getProxyCommonCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
		//              THRU 2513-EXIT.
		allocateServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		//    HAVE THE DEVELOPER SPECIFY THE LOCATION AND LENGTHS OF THE
		//     SERVICE CONTRACT INPUT AND OUTPUT AREAS.
		// COB_CODE: PERFORM 2514-CAPTURE-SVC-CTT-ATB
		//              THRU 2514-EXIT.
		captureSvcCttAtb();
		// COB_CODE: PERFORM 2515-GET-SERVICE-CONTRACT
		//              THRU 2515-EXIT.
		getServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
	}

	/**Original name: 2511-ALLOCATE-PROXY-CNT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
	 * ***************************************************************</pre>*/
	private void allocateProxyCnt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF DFHCOMMAREA  TO MA-INPUT-DATA-SIZE.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputDataSize(DfhcommareaMu0x0004.Len.DFHCOMMAREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getMainDriverData().getCommunicationShellCommon().getMaInputDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		//!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
		//!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2511-ALLOCATE-PROXY-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2511-ALLOCATE-PROXY-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF DFHCOMMAREA  TO MA-INPUT-POINTER.
		dfhcommarea = ((pointerManager.resolve(ws.getMainDriverData().getCommunicationShellCommon().getMaInputPointer(),
				DfhcommareaMu0x0004.class)));
		// COB_CODE: INITIALIZE DFHCOMMAREA.
		initDfhcommarea();
		//    WITH THE PROXY COMMON CONTRACT ALLOCATED, SPECIFY BOTH THE
		//     SIZE AND LOCATION OF THE INPUT AND OUTPUT AREAS.
		// COB_CODE: SET WS-PC-INPUT-PTR         TO ADDRESS OF PPC-INPUT-PARMS.
		ws.getWsMiscWorkFlds().getWsProxyContractAtb().setInputPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: SET WS-PC-OUTPUT-PTR        TO ADDRESS OF PPC-OUTPUT-PARMS.
		ws.getWsMiscWorkFlds().getWsProxyContractAtb().setOutputPtr(pointerManager.addressOf(dfhcommarea, DfhcommareaMu0x0004.Pos.PPC_OUTPUT_PARMS));
		// COB_CODE: COMPUTE WS-PC-INPUT-LEN = LENGTH OF PPC-INPUT-PARMS.
		ws.getWsMiscWorkFlds().getWsProxyContractAtb().setInputLen(DfhcommareaMu0x0004.Len.PPC_INPUT_PARMS);
		// COB_CODE: COMPUTE WS-PC-OUTPUT-LEN = LENGTH OF PPC-OUTPUT-PARMS.
		ws.getWsMiscWorkFlds().getWsProxyContractAtb().setOutputLen(DfhcommareaMu0x0004.Len.PPC_OUTPUT_PARMS);
	}

	/**Original name: 2512-GET-PROXY-COMMON-CNT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
	 * ***************************************************************</pre>*/
	private void getProxyCommonCnt() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getWsProxyContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-PC-INPUT-LEN))
		//               FLENGTH    (WS-PC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getWsProxyContractAtb().getInputLen());
		Channel.read(execContext, "", ws.getConstantFields().getCopybookNames().getProxyCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2512-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2512-GET-PROXY-COMMON-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2512-GET-PROXY-COMMON-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2512-EXIT
			return;
		}
	}

	/**Original name: 2513-ALLOCATE-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
	 * ***************************************************************</pre>*/
	private void allocateServiceContract() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputDataSize(LServiceContractArea.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getMainDriverData().getCommunicationShellCommon().getMaInputDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2513-ALLOCATE-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2513-ALLOCATE-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2513-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-POINTER.
		lServiceContractArea = ((pointerManager
				.resolve(ws.getMainDriverData().getCommunicationShellCommon().getMaInputPointer(), LServiceContractArea.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT-AREA.
		initLServiceContractArea();
	}

	/**Original name: 2514-CAPTURE-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  CAPTURE BOTH THE SIZE AND LOCATION OF THE INPUT AND OUTPUT   *
	 *  AREAS OF THE SERVICE CONTRACT AS SPECIFIED BY THE DEVELOPER  *
	 * ***************************************************************</pre>*/
	private void captureSvcCttAtb() {
		// COB_CODE: PERFORM 12514-SET-SVC-CTT-ATB
		//              THRU 12514-EXIT.
		setSvcCttAtb();
	}

	/**Original name: 2515-GET-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
	 * ***************************************************************</pre>*/
	private void getServiceContract() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getWsServiceContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-SC-INPUT-LEN))
		//               FLENGTH    (WS-SC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getWsServiceContractAtb().getInputLen());
		Channel.read(execContext, "", ws.getConstantFields().getCopybookNames().getServiceCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2515-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2515-GET-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2515-GET-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2515-EXIT
			return;
		}
	}

	/**Original name: 2520-CAPTURE-FROM-COMMAREA<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING THE COMMAREA ONLY.  SINCE THE          *
	 *  ENVIRONMENT WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY,   *
	 *  SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT.              *
	 * ***************************************************************
	 *   SINCE THE SERVICE CONTRACT IS APPENDED TO THE PROXY CONTRACT,
	 *    SET THE LOCATION OF THE SERVICE CONTRACT TO BE IMMEDIATELY
	 *    AFTER THE PROXY CONTRACT.</pre>*/
	private void captureFromCommarea() {
		// COB_CODE: SET WS-CA-PTR               TO
		//                                        ADDRESS OF PROXY-PROGRAM-COMMON.
		ws.getWsMiscWorkFlds().getWsCaPtr().setWsCaPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: ADD LENGTH OF PROXY-PROGRAM-COMMON
		//                                       TO WS-CA-PTR-VAL.
		ws.getWsMiscWorkFlds().getWsCaPtr()
				.setWsCaPtrVal(DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON + ws.getWsMiscWorkFlds().getWsCaPtr().getWsCaPtrVal());
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO WS-CA-PTR.
		lServiceContractArea = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getWsCaPtr().getWsCaPtr(),
				LServiceContractArea.class)));
	}

	/**Original name: 2530-CAPTURE-FROM-REF-STORAGE<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
	 *  WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
	 *  ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
	 * ***************************************************************</pre>*/
	private void captureFromRefStorage() {
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContractArea = ((pointerManager.resolve(dfhcommarea.getPpcServiceDataPointer(), LServiceContractArea.class)));
	}

	/**Original name: 3000-CALL-FRAMEWORK<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
	 *  DRIVER.                                                      *
	 * ***************************************************************</pre>*/
	private void callFramework() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-MAIN-DRIVER)
		//               COMMAREA   (MAIN-DRIVER-DATA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("MU0X0004", execContext).commarea(ws.getMainDriverData()).length(DfhcommareaTs020000.Len.DFHCOMMAREA)
				.link(ws.getConstantFields().getMainDriver(), new Ts020000());
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3000-CALL-FRAMEWORK'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3000-CALL-FRAMEWORK", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF NOT DSD-NO-ERROR-CODE
		//                                       TO PPC-ERROR-HANDLING-PARMS
		//           END-IF.
		if (!ws.getMainDriverData().getDriverSpecificData().getErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: MOVE DSD-ERROR-HANDLING-PARMS
			//                                   TO PPC-ERROR-HANDLING-PARMS
			dfhcommarea.setPpcErrorHandlingParmsBytes(ws.getMainDriverData().getDriverSpecificData().getDsdErrorHandlingParmsBytes());
		}
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getPpcOperation());
		// COB_CODE: MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(ws.getConstantFields().getUnitOfWork());
		// COB_CODE: MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setRequestModule(ws.getConstantFields().getRequestModule());
		// COB_CODE: MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setResponseModule(ws.getConstantFields().getResponseModule());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		ws.getMainDriverData().getDriverSpecificData().getBypassSyncpointMdrvInd()
				.setBypassSyncpointMdrvInd(dfhcommarea.getPpcBypassSyncpointMdrvInd());
		// COB_CODE: PERFORM 13100-SET-UP-INPUT
		//              THRU 13100-EXIT.
		setUpInput1();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput() {
		// COB_CODE: PERFORM 13200-SET-UP-OUTPUT
		//              THRU 13200-EXIT.
		setUpOutput1();
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 3210-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 3210-OUTPUT-SVC-CONTRACT-CTA
			//              THRU 3210-EXIT
			outputSvcContractCta();
		}
	}

	/**Original name: 3210-OUTPUT-SVC-CONTRACT-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE SERVICE CONTRACT WILL NOT BE UPDATED FURTHER FROM  *
	 *  THIS POINT, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputSvcContractCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getWsServiceContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-SC-OUTPUT-LEN))
		//               FLENGTH    (WS-SC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getWsServiceContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", ws.getConstantFields().getCopybookNames().getServiceCbkOupNmFormatted(), tsOutputData);
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3210-OUTPUT-SVC-CONTRACT-CTA'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3210-OUTPUT-SVC-CONTRACT-CTA", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-REQUEST-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkRequestArea));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-1'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-1", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-RESPONSE-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkResponseArea));
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-2'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-2", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IF USING CHANNELS AND CONTAINERS, DEALLOCATE THE SERVICE
		//     CONTRACT AND THE PROXY CONTRACT.  OTHERWISE, THAT WILL BE
		//     THE RESPONSIBILITY OF THE CALLER AND/OR THE CICS ENVIRONMENT
		// COB_CODE: IF EIBCALEN = +0
		//               CONTINUE
		//           ELSE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF L-SERVICE-CONTRACT-AREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(lServiceContractArea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(L-SERVICE-CONTRACT-AREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContractArea));
			ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
			ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		}
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-3'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-3", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
		//              THRU 8100-EXIT.
		outputProxyCommonCta();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(DFHCOMMAREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(dfhcommarea));
			ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
			ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		}
		//!   RECORD AN ERROR IF ONE OCCURS AND THE MEMORY IS STILL
		//!    ALLOCATED.  IF IT IS NOT ALLOCATED, THEN WE WOULD RUN INTO
		//!    ADDRESSING ISSUES AND MOST LIKELY ABEND.  EITHER WAY THOUGH,
		//!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-4'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-4", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	/**Original name: 8100-OUTPUT-PROXY-COMMON-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
	 *  FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************
	 *     IT IS POSSIBLE TO GET INTO THIS PARAGRAPH WHEN USING A METHOD
	 *      OTHER THAN CHANNELS.  BECAUSE OF THIS, WE WILL MAKE SURE WE
	 *      ARE UTILIZING A CHANNEL BEFORE PLACING THE COPYBOOK INTO A
	 *      CONTAINER.</pre>*/
	private void outputProxyCommonCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT EIBCALEN = +0
		//               GO TO 8100-EXIT
		//           END-IF.
		if (!(execContext.getCommAreaLen() == 0)) {
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getWsProxyContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-PC-OUTPUT-LEN))
		//               FLENGTH    (WS-PC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getWsProxyContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", ws.getConstantFields().getCopybookNames().getProxyCbkOupNmFormatted(), tsOutputData);
		ws.getWsMiscWorkFlds().setWsResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setWsResponseCode2(execContext.getResp2());
		//!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
		//!   BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibrespDisplay(ws.getWsMiscWorkFlds().getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			ws.getWsMiscWorkFlds().setWsEibresp2Display(ws.getWsMiscWorkFlds().getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8100-OUTPUT-PROXY-COMMON'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", ws.getWsMiscWorkFlds().getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8100-OUTPUT-PROXY-COMMON", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							ws.getWsMiscWorkFlds().getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							ws.getWsMiscWorkFlds().getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
	}

	/**Original name: 12514-SET-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
	 * ***************************************************************</pre>*/
	private void setSvcCttAtb() {
		// COB_CODE: SET WS-SC-INPUT-PTR         TO ADDRESS OF
		//                                             MUT004-SERVICE-INPUTS.
		ws.getWsMiscWorkFlds().getWsServiceContractAtb().setInputPtr(pointerManager.addressOf(lServiceContractArea));
		// COB_CODE: SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
		//                                             MUT004-SERVICE-OUTPUTS.
		ws.getWsMiscWorkFlds().getWsServiceContractAtb()
				.setOutputPtr(pointerManager.addressOf(lServiceContractArea, LServiceContractArea.Pos.MUT004_SERVICE_OUTPUTS));
		// COB_CODE: MOVE LENGTH OF MUT004-SERVICE-INPUTS
		//                                       TO WS-SC-INPUT-LEN.
		ws.getWsMiscWorkFlds().getWsServiceContractAtb().setInputLen(LServiceContractArea.Len.MUT004_SERVICE_INPUTS);
		// COB_CODE: MOVE LENGTH OF MUT004-SERVICE-OUTPUTS
		//                                       TO WS-SC-OUTPUT-LEN.
		ws.getWsMiscWorkFlds().getWsServiceContractAtb().setOutputLen(LServiceContractArea.Len.MUT004_SERVICE_OUTPUTS);
	}

	/**Original name: 13100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput1() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION
		//                                          WS-OPERATION-NAME.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getPpcOperation());
		ws.getWsMiscWorkFlds().getWsOperationName().setWsOperationName(dfhcommarea.getPpcOperation());
		//*   MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		//**  IF SEARCHING WITH AN ACCOUNT NUMBER AND THE ACCOUNT NUMBER
		//**  IS PERSONAL LINES (STARTS WITH A 7 OR A 9) THEN WE WILL
		//**  NOT WANT TO SWAP TO THE OWNER CLIENT ID LATER IN THE UOW.
		//**  TO AVOID THIS, WE ARE CALLING A DIFFERENT UOW.
		// COB_CODE: IF (MUT04I-ACT-NBR (1:1) = '7'
		//             OR
		//              MUT04I-TK-CIOR-SHW-OBJ-KEY (1:1) = '7'
		//             OR
		//              MUT04I-ACT-NBR (1:1) = '9'
		//             OR
		//              MUT04I-TK-CIOR-SHW-OBJ-KEY (1:1) = '9')
		//             AND
		//              MUT04I-TK-CLT-ID = SPACES
		//                                       TO CSC-UNIT-OF-WORK
		//           ELSE
		//               MOVE CF-UNIT-OF-WORK    TO CSC-UNIT-OF-WORK
		//           END-IF.
		if ((Conditions.eq(lServiceContractArea.getMut04iActNbrFormatted().substring((1) - 1, 1), "7")
				|| Conditions.eq(lServiceContractArea.getMut04iTkCiorShwObjKeyFormatted().substring((1) - 1, 1), "7")
				|| Conditions.eq(lServiceContractArea.getMut04iActNbrFormatted().substring((1) - 1, 1), "9")
				|| Conditions.eq(lServiceContractArea.getMut04iTkCiorShwObjKeyFormatted().substring((1) - 1, 1), "9"))
				&& Characters.EQ_SPACE.test(lServiceContractArea.getMut04iTkCltId())) {
			// COB_CODE: MOVE CF-2ND-UNIT-OF-WORK
			//                                   TO CSC-UNIT-OF-WORK
			ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(ws.getConstantFields().getCf2ndUnitOfWork());
		} else {
			// COB_CODE: MOVE CF-UNIT-OF-WORK    TO CSC-UNIT-OF-WORK
			ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(ws.getConstantFields().getUnitOfWork());
		}
		// COB_CODE: MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setRequestModule(ws.getConstantFields().getRequestModule());
		// COB_CODE: MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setResponseModule(ws.getConstantFields().getResponseModule());
		// COB_CODE: MOVE MUT04I-USR-ID          TO CSC-AUTH-USERID.
		ws.getMainDriverData().getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(lServiceContractArea.getMut04iUsrId());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		ws.getMainDriverData().getDriverSpecificData().getBypassSyncpointMdrvInd()
				.setBypassSyncpointMdrvInd(dfhcommarea.getPpcBypassSyncpointMdrvInd());
		// FRAMEWORK DATA FIELDS
		// COB_CODE: IF (MUT04I-TK-CLT-ID NOT EQUAL SPACES)
		//                                          CW08Q-CLIENT-ID
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04iTkCltId())) {
			// COB_CODE: MOVE MUT04I-TK-CLT-ID   TO CW02Q-CLIENT-ID
			//                                      CW08Q-CLIENT-ID
			lFrameworkRequestArea.setCw02qClientId(lServiceContractArea.getMut04iTkCltId());
			lFrameworkRequestArea.setCw08qClientId(lServiceContractArea.getMut04iTkCltId());
		}
		// COB_CODE: IF (MUT04I-TK-CIOR-SHW-OBJ-KEY NOT EQUAL SPACES)
		//                                            OF L-FRAMEWORK-REQUEST-AREA
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04iTkCiorShwObjKey())) {
			// COB_CODE: MOVE MUT04I-TK-CIOR-SHW-OBJ-KEY
			//                                   TO CWGIKQ-SHW-OBJ-KEY
			//                                      CW08Q-CIOR-SHW-OBJ-KEY
			//                                      CWORC-CIOR-SHW-OBJ-KEY
			//                                        OF L-FRAMEWORK-REQUEST-AREA
			lFrameworkRequestArea.setCwgikqShwObjKey(lServiceContractArea.getMut04iTkCiorShwObjKey());
			lFrameworkRequestArea.setCw08qCiorShwObjKey(lServiceContractArea.getMut04iTkCiorShwObjKey());
			lFrameworkRequestArea.setCworcCiorShwObjKey(lServiceContractArea.getMut04iTkCiorShwObjKey());
		}
		// COB_CODE: IF (MUT04I-TK-ADR-SEQ-NBR NUMERIC
		//             AND
		//              MUT04I-TK-ADR-SEQ-NBR NOT EQUAL 0)
		//                                       TO CWCACQ-ADR-SEQ-NBR
		//           END-IF.
		if (Functions.isNumber(lServiceContractArea.getMut04iTkAdrSeqNbr()) && lServiceContractArea.getMut04iTkAdrSeqNbr() != 0) {
			// COB_CODE: MOVE CF-POSITIVE        TO CWCACQ-ADR-SEQ-NBR-SIGN
			lFrameworkRequestArea.setCwcacqAdrSeqNbrSign(ws.getConstantFields().getPositive());
			// COB_CODE: MOVE MUT04I-TK-ADR-SEQ-NBR
			//                                   TO CWCACQ-ADR-SEQ-NBR
			lFrameworkRequestArea.setCwcacqAdrSeqNbrFormatted(Trunc.removeSign(lServiceContractArea.getMut04iTkAdrSeqNbrFormatted()));
		}
		// COB_CODE: IF MUT04I-ACT-NBR(4:1) NOT = CF-DASH
		//             AND
		//              MUT04I-ACT-NBR(4:1) NOT = SPACES
		//                                            OF L-FRAMEWORK-REQUEST-AREA
		//           ELSE
		//           EXIT.
		if (!Conditions.eq(lServiceContractArea.getMut04iActNbrFormatted().substring((4) - 1, 4), String.valueOf(ws.getConstantFields().getDash()))
				&& !Conditions.eq(lServiceContractArea.getMut04iActNbrFormatted().substring((4) - 1, 4), "")) {
			// COB_CODE: MOVE MUT04I-ACT-NBR(1:3)
			//                                   TO SA-FA-FIRST-3
			ws.getSaveArea().getFormattedActNbr().setFirst3(lServiceContractArea.getMut04iActNbrFormatted().substring((1) - 1, 3));
			// COB_CODE: MOVE MUT04I-ACT-NBR(4:3)
			//                                   TO SA-FA-SECOND-3
			ws.getSaveArea().getFormattedActNbr().setSecond3(lServiceContractArea.getMut04iActNbrFormatted().substring((4) - 1, 6));
			// COB_CODE: MOVE MUT04I-ACT-NBR(7:1)
			//                                   TO SA-FA-LAST-1
			ws.getSaveArea().getFormattedActNbr().setLast1Formatted(lServiceContractArea.getMut04iActNbrFormatted().substring((7) - 1, 7));
			// COB_CODE: MOVE SA-FORMATTED-ACT-NBR
			//                                   TO CWGIKQ-SHW-OBJ-KEY
			//                                      CW08Q-CIOR-SHW-OBJ-KEY
			//                                      CWORC-CIOR-SHW-OBJ-KEY
			//                                        OF L-FRAMEWORK-REQUEST-AREA
			lFrameworkRequestArea.setCwgikqShwObjKey(ws.getSaveArea().getFormattedActNbr().getFormattedActNbrFormatted());
			lFrameworkRequestArea.setCw08qCiorShwObjKey(ws.getSaveArea().getFormattedActNbr().getFormattedActNbrFormatted());
			lFrameworkRequestArea.setCworcCiorShwObjKey(ws.getSaveArea().getFormattedActNbr().getFormattedActNbrFormatted());
		} else {
			// COB_CODE: IF (MUT04I-ACT-NBR NOT = SPACES)
			//                                        OF L-FRAMEWORK-REQUEST-AREA
			//           END-IF
			if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04iActNbr())) {
				// COB_CODE: MOVE MUT04I-ACT-NBR TO CWGIKQ-SHW-OBJ-KEY
				//                                  CW08Q-CIOR-SHW-OBJ-KEY
				//                                  CWORC-CIOR-SHW-OBJ-KEY
				//                                    OF L-FRAMEWORK-REQUEST-AREA
				lFrameworkRequestArea.setCwgikqShwObjKey(lServiceContractArea.getMut04iActNbr());
				lFrameworkRequestArea.setCw08qCiorShwObjKey(lServiceContractArea.getMut04iActNbr());
				lFrameworkRequestArea.setCworcCiorShwObjKey(lServiceContractArea.getMut04iActNbr());
			}
			// COB_CODE: EXIT.
			//exit
		}
		// COB_CODE: IF MUT04I-FEIN(3:1) NOT = CF-DASH
		//             AND
		//              MUT04I-FEIN NOT = SPACES
		//           EXIT.
		if (!Conditions.eq(lServiceContractArea.getMut04iFeinFormatted().substring((3) - 1, 3), String.valueOf(ws.getConstantFields().getDash()))
				&& !Characters.EQ_SPACE.test(lServiceContractArea.getMut04iFein())) {
			// COB_CODE: MOVE MUT04I-FEIN(1:2)   TO SA-FF-FIRST-2
			ws.getSaveArea().getFormattedFein().setFirst2(lServiceContractArea.getMut04iFeinFormatted().substring((1) - 1, 2));
			// COB_CODE: MOVE MUT04I-FEIN(4:7)   TO SA-FF-LAST-7
			ws.getSaveArea().getFormattedFein().setLast7(lServiceContractArea.getMut04iFeinFormatted().substring((4) - 1, 10));
			// COB_CODE: MOVE SA-FORMATTED-FEIN  TO CWGIKQ-CITX-TAX-ID
			lFrameworkRequestArea.setCwgikqCitxTaxId(ws.getSaveArea().getFormattedFein().getFormattedFeinFormatted());
			// COB_CODE: MOVE CF-TT-FEIN         TO CWGIKQ-TAX-TYPE-CD
			lFrameworkRequestArea.setCwgikqTaxTypeCd(ws.getConstantFields().getTtFein());
			// COB_CODE: EXIT.
			//exit
		}
		// COB_CODE: IF MUT04I-SSN(4:1) NOT = CF-DASH
		//             AND
		//              MUT04I-SSN NOT = SPACES
		//           EXIT.
		if (!Conditions.eq(lServiceContractArea.getMut04iSsnFormatted().substring((4) - 1, 4), String.valueOf(ws.getConstantFields().getDash()))
				&& !Characters.EQ_SPACE.test(lServiceContractArea.getMut04iSsn())) {
			// COB_CODE: MOVE MUT04I-SSN(1:3)    TO SA-FS-FIRST-3
			ws.getSaveArea().getFormattedSsn().setFirst3(lServiceContractArea.getMut04iSsnFormatted().substring((1) - 1, 3));
			// COB_CODE: MOVE MUT04I-SSN(4:2)    TO SA-FS-SECOND-2
			ws.getSaveArea().getFormattedSsn().setSecond2(lServiceContractArea.getMut04iSsnFormatted().substring((4) - 1, 5));
			// COB_CODE: MOVE MUT04I-SSN(6:4)    TO SA-FS-LAST-4
			ws.getSaveArea().getFormattedSsn().setLast4(lServiceContractArea.getMut04iSsnFormatted().substring((6) - 1, 9));
			// COB_CODE: MOVE SA-FORMATTED-SSN   TO CWGIKQ-CITX-TAX-ID
			lFrameworkRequestArea.setCwgikqCitxTaxId(ws.getSaveArea().getFormattedSsn().getFormattedSsnFormatted());
			// COB_CODE: MOVE CF-TT-SSN          TO CWGIKQ-TAX-TYPE-CD
			lFrameworkRequestArea.setCwgikqTaxTypeCd(ws.getConstantFields().getTtSsn());
			// COB_CODE: EXIT.
			//exit
		}
		//**  SET THE FILTER ON THE RELATED CLIENT OBJECT RELATION ROW
		//**  SO THAT WE ARE LOOKING FOR COOWNER DATA.
		// COB_CODE: MOVE CF-COOWNER-LOOKUP      TO CWORC-FILTER-TYPE
		//                                            OF L-FRAMEWORK-REQUEST-AREA.
		lFrameworkRequestArea.setCworcFilterType(ws.getConstantFields().getCoownerLookup());
	}

	/**Original name: 13200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput1() {
		// COB_CODE: INITIALIZE MUT004-SERVICE-OUTPUTS.
		initMut004ServiceOutputs();
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-GET-INSURED-DETAIL
		//                      THRU 9D00-EXIT
		//               WHEN WS-GET-INSD-NAPT-DETAIL
		//                      THRU 9400-EXIT
		//           END-EVALUATE.
		switch (ws.getWsMiscWorkFlds().getWsOperationName().getWsOperationName()) {

		case WsOperationName.GET_INSURED_DETAIL:// COB_CODE: PERFORM 9100-SET-CLT-NAME-ADDR
			//              THRU 9100-EXIT
			rng9100SetCltNameAddr();
			// COB_CODE: PERFORM 9200-SET-CLT-PHONE
			//              THRU 9200-EXIT
			rng9200SetCltPhone();
			// COB_CODE: PERFORM 9300-SET-CLT-TAX-INFO
			//              THRU 9300-EXIT
			setCltTaxInfo();
			// COB_CODE: PERFORM 9400-SET-CLT-DEMO-INFO
			//              THRU 9400-EXIT
			rng9400SetCltDemoInfo();
			// COB_CODE: PERFORM 9500-SET-CLT-ACCT-INFO
			//              THRU 9500-EXIT
			rng9500SetCltAcctInfo();
			// COB_CODE: PERFORM 9600-SET-CLT-CST-INFO
			//              THRU 9600-EXIT
			setCltCstInfo();
			// COB_CODE: IF CW45R-SMR-NM = SPACES
			//               END-IF
			//           ELSE
			//                  THRU 9700-EXIT
			//           END-IF
			if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw45rSmrNm())) {
				// COB_CODE: IF CW46R-SMR-NM = SPACES
				//               CONTINUE
				//           ELSE
				//                  THRU 9800-EXIT
				//           END-IF
				if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw46rSmrNm())) {
					// COB_CODE: CONTINUE
					//continue
				} else {
					// COB_CODE: PERFORM 9800-SET-CLT-AGC-TER-INFO
					//              THRU 9800-EXIT
					setCltAgcTerInfo();
				}
			} else {
				// COB_CODE: PERFORM 9700-SET-CLT-MUT-TER-INFO
				//              THRU 9700-EXIT
				setCltMutTerInfo();
			}
			// COB_CODE: PERFORM 9900-SET-CLT-UW-INFO
			//              THRU 9900-EXIT
			rng9900SetCltUwInfo();
			// COB_CODE: PERFORM 9B00-SET-FED-ACT-INFO
			//              THRU 9B00-EXIT
			b00SetFedActInfo();
			// COB_CODE: PERFORM 9C00-SET-BUSINESS-CLT
			//              THRU 9C00-EXIT
			c00SetBusinessClt();
			// COB_CODE: PERFORM 9D00-SET-COWNER-INFO
			//              THRU 9D00-EXIT
			d00SetCownerInfo();
			break;

		case WsOperationName.GET_INSD_NAPT_DETAIL:// COB_CODE: PERFORM 9100-SET-CLT-NAME-ADDR
			//              THRU 9100-EXIT
			rng9100SetCltNameAddr();
			// COB_CODE: PERFORM 9200-SET-CLT-PHONE
			//              THRU 9200-EXIT
			rng9200SetCltPhone();
			// COB_CODE: PERFORM 9400-SET-CLT-DEMO-INFO
			//              THRU 9400-EXIT
			rng9400SetCltDemoInfo();
			break;

		default:
			break;
		}
	}

	/**Original name: 9100-SET-CLT-NAME-ADDR<br>*/
	private void setCltNameAddr() {
		// COB_CODE: MOVE 1                      TO SS-ADR-IDX.
		ws.getSubscripts().setAdrIdx(((short) 1));
		// COB_CODE: MOVE CF-NO                  TO MUT04O-CN-IDV-NM-IND.
		lServiceContractArea.setMut04oCnIdvNmInd(ws.getConstantFields().getNo());
		// COB_CODE: MOVE CW02R-CLIENT-ID        TO MUT04O-TK-CLT-ID.
		lServiceContractArea.setMut04oTkCltId(lFrameworkResponseArea.getCw02rClientId());
		// COB_CODE: IF (CW02R-LEG-ENT-CD = CF-INDIVIDUAL)
		//               MOVE SA-SRT-NAME        TO MUT04O-CN-SR-NM
		//           END-IF.
		if (Conditions.eq(lFrameworkResponseArea.getCw02rLegEntCd(), ws.getConstantFields().getIndividual())) {
			// COB_CODE: MOVE CW02R-CICL-FST-NM  TO MUT04O-CN-FST-NM
			//                                      SA-FST-NM
			lServiceContractArea.setMut04oCnFstNm(lFrameworkResponseArea.getCw02rCiclFstNm());
			ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw02rCiclFstNm());
			// COB_CODE: MOVE CW02R-CICL-MDL-NM  TO MUT04O-CN-MDL-NM
			//                                      SA-MDL-NM
			lServiceContractArea.setMut04oCnMdlNm(lFrameworkResponseArea.getCw02rCiclMdlNm());
			ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw02rCiclMdlNm());
			// COB_CODE: MOVE CW02R-CICL-LST-NM  TO MUT04O-CN-LST-NM
			//                                      SA-LST-NM
			lServiceContractArea.setMut04oCnLstNm(lFrameworkResponseArea.getCw02rCiclLstNm());
			ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw02rCiclLstNm());
			// COB_CODE: MOVE CW02R-NM-PFX       TO MUT04O-CN-PFX
			lServiceContractArea.setMut04oCnPfx(lFrameworkResponseArea.getCw02rNmPfx());
			// COB_CODE: MOVE CW02R-NM-SFX       TO MUT04O-CN-SFX
			//                                      SA-SFX
			lServiceContractArea.setMut04oCnSfx(lFrameworkResponseArea.getCw02rNmSfx());
			ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw02rNmSfx());
			// COB_CODE: MOVE CF-YES             TO MUT04O-CN-IDV-NM-IND
			lServiceContractArea.setMut04oCnIdvNmInd(ws.getConstantFields().getYes());
			// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
			//              THRU 9A00-EXIT
			a00FormatDsyName();
			// COB_CODE: MOVE SA-DISPLAY-NAME    TO MUT04O-CN-DSY-NM
			lServiceContractArea.setMut04oCnDsyNm(ws.getSaveArea().getDisplayName());
			// COB_CODE: PERFORM 9140-FILL-IND-SRT-NAME
			//              THRU 9140-EXIT
			fillIndSrtName();
			// COB_CODE: MOVE SA-SRT-NAME        TO MUT04O-CN-SR-NM
			lServiceContractArea.setMut04oCnSrNm(ws.getSaveArea().getSrtName());
		}
		// COB_CODE: IF MUT04O-CN-IDV-NM-IND = CF-NO
		//               END-IF
		//           END-IF.
		if (lServiceContractArea.getMut04oCnIdvNmInd() == ws.getConstantFields().getNo()) {
			// COB_CODE: MOVE CW02R-CICL-LST-NM  TO SA-FULL-COMPANY-NAME-1
			ws.getSaveArea().setFullCompanyName1(lFrameworkResponseArea.getCw02rCiclLstNm());
			// COB_CODE: MOVE CWCACR-CICA-ADR-1(SS-ADR-IDX)
			//                                   TO SA-FULL-COMPANY-NAME-2
			ws.getSaveArea().setFullCompanyName2(lFrameworkResponseArea.getCwcacrCicaAdr1(ws.getSubscripts().getAdrIdx()));
			// COB_CODE: IF SW-NAME-OVERFLOW-NOT-FOUND
			//                  THRU 9130-EXIT
			//           END-IF
			if (!ws.getSwitches().isNameOverflowFoundFlag()) {
				// COB_CODE: PERFORM 9130-CHECK-NAME-OVERFLOW
				//              THRU 9130-EXIT
				checkNameOverflow();
			}
		}
	}

	/**Original name: 9100-A<br>*/
	private String a() {
		// COB_CODE: IF CWCACR-CICA-ADR-1(SS-ADR-IDX) = SPACES
		//               GO TO 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCwcacrCicaAdr1(ws.getSubscripts().getAdrIdx()))) {
			// COB_CODE: GO TO 9100-EXIT
			return "";
		}
		// COB_CODE: EVALUATE CWCACR-ADR-TYP-CD(SS-ADR-IDX)
		//               WHEN CF-AC-BSM
		//                      THRU 9110-EXIT
		//               WHEN CF-AC-BSL
		//                      THRU 9120-EXIT
		//           END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getCwcacrAdrTypCd(ws.getSubscripts().getAdrIdx()), ws.getConstantFields().getAcBsm())) {
			// COB_CODE: PERFORM 9110-FILL-BSM-ADDRESS
			//              THRU 9110-EXIT
			fillBsmAddress();
		} else if (Conditions.eq(lFrameworkResponseArea.getCwcacrAdrTypCd(ws.getSubscripts().getAdrIdx()), ws.getConstantFields().getAcBsl())) {
			// COB_CODE: PERFORM 9120-FILL-BSL-ADDRESS
			//              THRU 9120-EXIT
			fillBslAddress();
		}
		// COB_CODE: ADD 1                       TO SS-ADR-IDX.
		ws.getSubscripts().setAdrIdx(Trunc.toShort(1 + ws.getSubscripts().getAdrIdx(), 4));
		// COB_CODE: IF SS-ADR-IDX-MAX
		//               GO TO 9100-EXIT
		//           END-IF.
		if (ws.getSubscripts().isAdrIdxMax()) {
			// COB_CODE: GO TO 9100-EXIT
			return "";
		}
		// COB_CODE: GO TO 9100-A.
		return "9100-A";
	}

	/**Original name: 9110-FILL-BSM-ADDRESS<br>*/
	private void fillBsmAddress() {
		// COB_CODE: IF (CWCACR-CICA-ADR-2-NI(SS-ADR-IDX) = CF-COL-IS-NULL)
		//               MOVE SPACES             TO MUT04O-BSM-LIN-2-ADR
		//           ELSE
		//               END-IF
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrCicaAdr2Ni(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getColIsNull()) {
			// COB_CODE: MOVE SPACES             TO MUT04O-BSM-LIN-2-ADR
			lServiceContractArea.setMut04oBsmLin2Adr("");
		} else if (lFrameworkResponseArea.getCwcacrAddNmInd(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getYes()) {
			// COB_CODE: IF CWCACR-ADD-NM-IND(SS-ADR-IDX) = CF-YES
			//               MOVE SA-ADDRESS-2   TO MUT04O-BSM-LIN-2-ADR
			//           ELSE
			//                                   TO MUT04O-BSM-LIN-2-ADR
			//           END-IF
			// COB_CODE: MOVE SA-ADDRESS-2   TO MUT04O-BSM-LIN-2-ADR
			lServiceContractArea.setMut04oBsmLin2Adr(ws.getSaveArea().getAddress2());
		} else {
			// COB_CODE: MOVE CWCACR-CICA-ADR-2(SS-ADR-IDX)
			//                               TO MUT04O-BSM-LIN-2-ADR
			lServiceContractArea.setMut04oBsmLin2Adr(lFrameworkResponseArea.getCwcacrCicaAdr2(ws.getSubscripts().getAdrIdx()));
		}
		// COB_CODE: MOVE CWCACR-ADR-SEQ-NBR(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-ADR-SEQ-NBR.
		lServiceContractArea.setMut04oBsmAdrSeqNbrFormatted(lFrameworkResponseArea.getCwcacrAdrSeqNbrFormatted(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: IF CWCACR-ADD-NM-IND(SS-ADR-IDX) = CF-YES
		//               MOVE SA-ADDRESS-1       TO MUT04O-BSM-LIN-1-ADR
		//           ELSE
		//                                       TO MUT04O-BSM-LIN-1-ADR
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrAddNmInd(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getYes()) {
			// COB_CODE: MOVE SA-ADDRESS-1       TO MUT04O-BSM-LIN-1-ADR
			lServiceContractArea.setMut04oBsmLin1Adr(ws.getSaveArea().getAddress1());
		} else {
			// COB_CODE: MOVE CWCACR-CICA-ADR-1(SS-ADR-IDX)
			//                                   TO MUT04O-BSM-LIN-1-ADR
			lServiceContractArea.setMut04oBsmLin1Adr(lFrameworkResponseArea.getCwcacrCicaAdr1(ws.getSubscripts().getAdrIdx()));
		}
		// COB_CODE: MOVE CWCACR-CICA-CIT-NM(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-CIT.
		lServiceContractArea.setMut04oBsmCit(lFrameworkResponseArea.getCwcacrCicaCitNm(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-ST-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-ST-ABB.
		lServiceContractArea.setMut04oBsmStAbb(lFrameworkResponseArea.getCwcacrStCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-CICA-PST-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-PST-CD.
		lServiceContractArea.setMut04oBsmPstCd(lFrameworkResponseArea.getCwcacrCicaPstCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-CTR-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-CTR-CD.
		lServiceContractArea.setMut04oBsmCtrCd(lFrameworkResponseArea.getCwcacrCtrCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-ADR-ID(SS-ADR-IDX)
		//                                       TO MUT04O-BSM-ADR-ID.
		lServiceContractArea.setMut04oBsmAdrId(lFrameworkResponseArea.getCwcacrAdrId(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: IF (CWCACR-CICA-CTY-NI(SS-ADR-IDX) = CF-COL-IS-NULL)
		//               MOVE SPACES             TO MUT04O-BSM-CTY
		//           ELSE
		//                                       TO MUT04O-BSM-CTY
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrCicaCtyNi(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getColIsNull()) {
			// COB_CODE: MOVE SPACES             TO MUT04O-BSM-CTY
			lServiceContractArea.setMut04oBsmCty("");
		} else {
			// COB_CODE: MOVE CWCACR-CICA-CTY(SS-ADR-IDX)
			//                                   TO MUT04O-BSM-CTY
			lServiceContractArea.setMut04oBsmCty(lFrameworkResponseArea.getCwcacrCicaCty(ws.getSubscripts().getAdrIdx()));
		}
	}

	/**Original name: 9120-FILL-BSL-ADDRESS<br>*/
	private void fillBslAddress() {
		// COB_CODE: IF (CWCACR-CICA-ADR-2-NI(SS-ADR-IDX) = CF-COL-IS-NULL)
		//               MOVE SPACES             TO MUT04O-BSL-LIN-2-ADR
		//           ELSE
		//               END-IF
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrCicaAdr2Ni(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getColIsNull()) {
			// COB_CODE: MOVE SPACES             TO MUT04O-BSL-LIN-2-ADR
			lServiceContractArea.setMut04oBslLin2Adr("");
		} else if (lFrameworkResponseArea.getCwcacrAddNmInd(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getYes()) {
			// COB_CODE: IF CWCACR-ADD-NM-IND(SS-ADR-IDX) = CF-YES
			//               MOVE SA-ADDRESS-2   TO MUT04O-BSL-LIN-2-ADR
			//           ELSE
			//                                   TO MUT04O-BSL-LIN-2-ADR
			//           END-IF
			// COB_CODE: MOVE SA-ADDRESS-2   TO MUT04O-BSL-LIN-2-ADR
			lServiceContractArea.setMut04oBslLin2Adr(ws.getSaveArea().getAddress2());
		} else {
			// COB_CODE: MOVE CWCACR-CICA-ADR-2(SS-ADR-IDX)
			//                               TO MUT04O-BSL-LIN-2-ADR
			lServiceContractArea.setMut04oBslLin2Adr(lFrameworkResponseArea.getCwcacrCicaAdr2(ws.getSubscripts().getAdrIdx()));
		}
		// COB_CODE: MOVE CWCACR-ADR-SEQ-NBR(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-ADR-SEQ-NBR.
		lServiceContractArea.setMut04oBslAdrSeqNbrFormatted(lFrameworkResponseArea.getCwcacrAdrSeqNbrFormatted(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: IF CWCACR-ADD-NM-IND(SS-ADR-IDX) = CF-YES
		//               MOVE SA-ADDRESS-1       TO MUT04O-BSL-LIN-1-ADR
		//           ELSE
		//                                       TO MUT04O-BSL-LIN-1-ADR
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrAddNmInd(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getYes()) {
			// COB_CODE: MOVE SA-ADDRESS-1       TO MUT04O-BSL-LIN-1-ADR
			lServiceContractArea.setMut04oBslLin1Adr(ws.getSaveArea().getAddress1());
		} else {
			// COB_CODE: MOVE CWCACR-CICA-ADR-1(SS-ADR-IDX)
			//                                   TO MUT04O-BSL-LIN-1-ADR
			lServiceContractArea.setMut04oBslLin1Adr(lFrameworkResponseArea.getCwcacrCicaAdr1(ws.getSubscripts().getAdrIdx()));
		}
		// COB_CODE: MOVE CWCACR-CICA-CIT-NM(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-CIT.
		lServiceContractArea.setMut04oBslCit(lFrameworkResponseArea.getCwcacrCicaCitNm(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-ST-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-ST-ABB.
		lServiceContractArea.setMut04oBslStAbb(lFrameworkResponseArea.getCwcacrStCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-CICA-PST-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-PST-CD.
		lServiceContractArea.setMut04oBslPstCd(lFrameworkResponseArea.getCwcacrCicaPstCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-CTR-CD(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-CTR-CD.
		lServiceContractArea.setMut04oBslCtrCd(lFrameworkResponseArea.getCwcacrCtrCd(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: MOVE CWCACR-ADR-ID(SS-ADR-IDX)
		//                                       TO MUT04O-BSL-ADR-ID.
		lServiceContractArea.setMut04oBslAdrId(lFrameworkResponseArea.getCwcacrAdrId(ws.getSubscripts().getAdrIdx()));
		// COB_CODE: IF (CWCACR-CICA-CTY-NI(SS-ADR-IDX) = CF-COL-IS-NULL)
		//               MOVE SPACES             TO MUT04O-BSL-CTY
		//           ELSE
		//                                       TO MUT04O-BSL-CTY
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrCicaCtyNi(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getColIsNull()) {
			// COB_CODE: MOVE SPACES             TO MUT04O-BSL-CTY
			lServiceContractArea.setMut04oBslCty("");
		} else {
			// COB_CODE: MOVE CWCACR-CICA-CTY(SS-ADR-IDX)
			//                                   TO MUT04O-BSL-CTY
			lServiceContractArea.setMut04oBslCty(lFrameworkResponseArea.getCwcacrCicaCty(ws.getSubscripts().getAdrIdx()));
		}
	}

	/**Original name: 9130-CHECK-NAME-OVERFLOW<br>
	 * <pre> FOLLOW BUSINESS RULES FOR NAME OVERFLOW INTO ADDRESS LINE 1.</pre>*/
	private void checkNameOverflow() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF CWCACR-ADD-NM-IND(SS-ADR-IDX) = CF-YES
		//                                       TO TRUE
		//           ELSE
		//                                          MUT04O-CN-SR-NM
		//           END-IF.
		if (lFrameworkResponseArea.getCwcacrAddNmInd(ws.getSubscripts().getAdrIdx()) == ws.getConstantFields().getYes()) {
			// COB_CODE: STRING SA-FULL-COMPANY-NAME-1
			//                                   DELIMITED BY CF-NAME-DELIMITER
			//                  SPACE            DELIMITED BY SIZE
			//                  SA-FULL-COMPANY-NAME-2
			//                                   DELIMITED BY CF-NAME-DELIMITER
			//               INTO MUT04O-CN-DSY-NM
			//           END-STRING
			concatUtil = ConcatUtil.buildString(LServiceContractArea.Len.MUT04O_CN_DSY_NM,
					Functions.substringBefore(ws.getSaveArea().getFullCompanyName1Formatted(), ws.getConstantFields().getNameDelimiterFormatted()),
					Types.SPACE_STRING,
					Functions.substringBefore(ws.getSaveArea().getFullCompanyName2Formatted(), ws.getConstantFields().getNameDelimiterFormatted()));
			lServiceContractArea.setMut04oCnDsyNm(concatUtil.replaceInString(lServiceContractArea.getMut04oCnDsyNmFormatted()));
			// COB_CODE: MOVE MUT04O-CN-DSY-NM   TO MUT04O-CN-SR-NM
			lServiceContractArea.setMut04oCnSrNm(lServiceContractArea.getMut04oCnDsyNm());
			// COB_CODE: IF CWCACR-CICA-ADR-2(SS-ADR-IDX) = SPACES
			//               MOVE SPACES         TO SA-ADDRESS-1
			//           ELSE
			//               MOVE SPACES         TO SA-ADDRESS-2
			//           END-IF
			if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCwcacrCicaAdr2(ws.getSubscripts().getAdrIdx()))) {
				// COB_CODE: MOVE SPACES         TO SA-ADDRESS-1
				ws.getSaveArea().setAddress1("");
			} else {
				// COB_CODE: MOVE CWCACR-CICA-ADR-2(SS-ADR-IDX)
				//                               TO SA-ADDRESS-1
				ws.getSaveArea().setAddress1(lFrameworkResponseArea.getCwcacrCicaAdr2(ws.getSubscripts().getAdrIdx()));
				// COB_CODE: MOVE SPACES         TO SA-ADDRESS-2
				ws.getSaveArea().setAddress2("");
			}
			// COB_CODE: SET SW-NAME-OVERFLOW-FOUND
			//                                   TO TRUE
			ws.getSwitches().setNameOverflowFoundFlag(true);
		} else {
			// COB_CODE: MOVE SA-FULL-COMPANY-NAME-1
			//                                   TO MUT04O-CN-DSY-NM
			//                                      MUT04O-CN-SR-NM
			lServiceContractArea.setMut04oCnDsyNm(ws.getSaveArea().getFullCompanyName1());
			lServiceContractArea.setMut04oCnSrNm(ws.getSaveArea().getFullCompanyName1());
		}
	}

	/**Original name: 9140-FILL-IND-SRT-NAME<br>*/
	private void fillIndSrtName() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE SA-SRT-NAME.
		ws.getSaveArea().setSrtName("");
		// COB_CODE: IF SA-LST-NM = SPACES
		//               MOVE SPACES             TO SA-SD-LST-SPR
		//           ELSE
		//               MOVE CF-SORT-DELIMITER  TO SA-SD-LST-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getLstNm())) {
			// COB_CODE: MOVE SPACES             TO SA-SD-LST-SPR
			ws.getSaveArea().getSrtDelimiters().setLstSpr("");
		} else {
			// COB_CODE: MOVE CF-SORT-DELIMITER  TO SA-SD-LST-SPR
			ws.getSaveArea().getSrtDelimiters().setLstSpr(ws.getConstantFields().getSortDelimiter());
		}
		// COB_CODE: IF SA-FST-NM = SPACES
		//               MOVE SPACES             TO SA-SD-FST-SPR
		//           ELSE
		//               MOVE CF-SORT-DELIMITER  TO SA-SD-FST-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getFstNm())) {
			// COB_CODE: MOVE SPACES             TO SA-SD-FST-SPR
			ws.getSaveArea().getSrtDelimiters().setFstSpr("");
		} else {
			// COB_CODE: MOVE CF-SORT-DELIMITER  TO SA-SD-FST-SPR
			ws.getSaveArea().getSrtDelimiters().setFstSpr(ws.getConstantFields().getSortDelimiter());
		}
		// COB_CODE: IF SA-MDL-NM = SPACES
		//               MOVE SPACES             TO SA-SD-MDL-SPR
		//           ELSE
		//               MOVE CF-SORT-DELIMITER  TO SA-SD-MDL-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getMdlNm())) {
			// COB_CODE: MOVE SPACES             TO SA-SD-MDL-SPR
			ws.getSaveArea().getSrtDelimiters().setMdlSpr("");
		} else {
			// COB_CODE: MOVE CF-SORT-DELIMITER  TO SA-SD-MDL-SPR
			ws.getSaveArea().getSrtDelimiters().setMdlSpr(ws.getConstantFields().getSortDelimiter());
		}
		// COB_CODE: IF SA-SFX = SPACES
		//               END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getSfx())) {
			// COB_CODE: MOVE SPACES             TO SA-SD-MDL-SPR
			ws.getSaveArea().getSrtDelimiters().setMdlSpr("");
			// COB_CODE: IF SA-MDL-NM = SPACES
			//               END-IF
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getSaveArea().getMdlNm())) {
				// COB_CODE: MOVE SPACES         TO SA-SD-FST-SPR
				ws.getSaveArea().getSrtDelimiters().setFstSpr("");
				// COB_CODE: IF SA-FST-NM = SPACES
				//               MOVE SPACES     TO SA-SD-LST-SPR
				//           END-IF
				if (Characters.EQ_SPACE.test(ws.getSaveArea().getFstNm())) {
					// COB_CODE: MOVE SPACES     TO SA-SD-LST-SPR
					ws.getSaveArea().getSrtDelimiters().setLstSpr("");
				}
			}
		}
		//        INTO MUT04O-CN-SR-NM
		// COB_CODE:      STRING SA-LST-NM
		//                       SA-SD-LST-SPR
		//                       SA-FST-NM
		//                       SA-SD-FST-SPR
		//                       SA-MDL-NM
		//                       SA-SD-MDL-SPR
		//                       SA-SFX               DELIMITED BY CF-NAME-DELIMITER
		//           *        INTO MUT04O-CN-SR-NM
		//                    INTO SA-SRT-NAME
		//                END-STRING.
		concatUtil = ConcatUtil.buildString(SaveAreaMu0x0004.Len.SRT_NAME,
				new String[] { Functions.substringBefore(ws.getSaveArea().getLstNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getSrtDelimiters().getLstSprFormatted(),
								ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getFstNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getSrtDelimiters().getFstSprFormatted(),
								ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getMdlNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getSrtDelimiters().getMdlSprFormatted(),
								ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getSfxFormatted(), ws.getConstantFields().getNameDelimiterFormatted()) });
		ws.getSaveArea().setSrtName(concatUtil.replaceInString(ws.getSaveArea().getSrtNameFormatted()));
	}

	/**Original name: 9200-SET-CLT-PHONE<br>*/
	private void setCltPhone() {
		// COB_CODE: MOVE +0                     TO SS-PHN-IDX.
		ws.getSubscripts().setPhnIdx(((short) 0));
		// COB_CODE: SET SW-PRIMARY-FAX-NBR-NOT-FOUND
		//                                       TO TRUE.
		ws.getSwitches().setPrimaryFaxNbrFlag(false);
	}

	/**Original name: 9200-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-PHN-IDX.
		ws.getSubscripts().setPhnIdx(Trunc.toShort(1 + ws.getSubscripts().getPhnIdx(), 4));
		// COB_CODE: IF CW04R-CIPH-PHN-NBR(SS-PHN-IDX) = SPACES
		//               GO TO 9200-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw04rCiphPhnNbr(ws.getSubscripts().getPhnIdx()))) {
			// COB_CODE: GO TO 9200-EXIT
			return "";
		}
		// COB_CODE: EVALUATE CW04R-PHN-TYP-CD(SS-PHN-IDX)
		//               WHEN CF-PN-BUS
		//                   END-IF
		//               WHEN CF-PN-FAX-1
		//               WHEN CF-PN-FAX-2
		//                      THRU 9220-EXIT
		//               WHEN CF-PN-CTC
		//                   END-IF
		//           END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getCw04rPhnTypCd(ws.getSubscripts().getPhnIdx()),
				ws.getConstantFields().getPhoneNbrCode().getBus())) {
			// COB_CODE: IF MUT04O-BUS-PHN-NBR-FMT = SPACES
			//                  THRU 9210-EXIT
			//           END-IF
			if (Characters.EQ_SPACE.test(lServiceContractArea.getMut04oBusPhnNbrFmt())) {
				// COB_CODE: PERFORM 9210-FILL-BUSINESS-NBR
				//              THRU 9210-EXIT
				fillBusinessNbr();
			}
		} else if (Conditions.eq(lFrameworkResponseArea.getCw04rPhnTypCd(ws.getSubscripts().getPhnIdx()),
				ws.getConstantFields().getPhoneNbrCode().getFax1())
				|| Conditions.eq(lFrameworkResponseArea.getCw04rPhnTypCd(ws.getSubscripts().getPhnIdx()),
						ws.getConstantFields().getPhoneNbrCode().getFax2())) {
			// COB_CODE: PERFORM 9220-FILL-FAX-NBR
			//              THRU 9220-EXIT
			fillFaxNbr();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw04rPhnTypCd(ws.getSubscripts().getPhnIdx()),
				ws.getConstantFields().getPhoneNbrCode().getCtc())) {
			// COB_CODE: IF MUT04O-CTC-PHN-NBR-FMT = SPACES
			//                  THRU 9230-EXIT
			//           END-IF
			if (Characters.EQ_SPACE.test(lServiceContractArea.getMut04oCtcPhnNbrFmt())) {
				// COB_CODE: PERFORM 9230-FILL-CTC-NBR
				//              THRU 9230-EXIT
				fillCtcNbr();
			}
		}
		// COB_CODE: IF SS-PHN-IDX-MAX
		//               GO TO 9200-EXIT
		//           END-IF.
		if (ws.getSubscripts().isPhnIdxMax()) {
			// COB_CODE: GO TO 9200-EXIT
			return "";
		}
		// COB_CODE: GO TO 9200-A.
		return "9200-A";
	}

	/**Original name: 9210-FILL-BUSINESS-NBR<br>*/
	private void fillBusinessNbr() {
		// COB_CODE: MOVE CW04R-CIPH-PHN-NBR(SS-PHN-IDX)
		//                                       TO MUT04O-BUS-PHN-NBR-FMT.
		lServiceContractArea.setMut04oBusPhnNbrFmt(lFrameworkResponseArea.getCw04rCiphPhnNbr(ws.getSubscripts().getPhnIdx()));
		// COB_CODE: MOVE MUT04O-BUS-PHN-NBR-FMT(2:3)
		//                                       TO MUT04O-BUS-PHN-ACD.
		lServiceContractArea.setMut04oBusPhnAcd(lServiceContractArea.getMut04oBusPhnNbrFmtFormatted().substring((2) - 1, 4));
		// COB_CODE: MOVE MUT04O-BUS-PHN-NBR-FMT(6:3)
		//                                       TO MUT04O-BUS-PHN-PFX-NBR.
		lServiceContractArea.setMut04oBusPhnPfxNbr(lServiceContractArea.getMut04oBusPhnNbrFmtFormatted().substring((6) - 1, 8));
		// COB_CODE: MOVE MUT04O-BUS-PHN-NBR-FMT(10:4)
		//                                       TO MUT04O-BUS-PHN-LIN-NBR.
		lServiceContractArea.setMut04oBusPhnLinNbr(lServiceContractArea.getMut04oBusPhnNbrFmtFormatted().substring((10) - 1, 13));
		// COB_CODE: MOVE MUT04O-BUS-PHN-NBR-FMT(17:5)
		//                                       TO MUT04O-BUS-PHN-EXT-NBR.
		lServiceContractArea.setMut04oBusPhnExtNbr(lServiceContractArea.getMut04oBusPhnNbrFmtFormatted().substring((17) - 1, 21));
	}

	/**Original name: 9220-FILL-FAX-NBR<br>*/
	private void fillFaxNbr() {
		// COB_CODE: IF SW-PRIMARY-FAX-NBR-FOUND
		//               GO TO 9220-EXIT
		//           END-IF.
		if (ws.getSwitches().isPrimaryFaxNbrFlag()) {
			// COB_CODE: GO TO 9220-EXIT
			return;
		}
		// COB_CODE:      IF CW04R-PHN-TYP-CD(SS-PHN-IDX) = CF-PN-FAX-1
		//                                            TO TRUE
		//           *    ELSE
		//           *        IF SW-PRIMARY-FAX-NBR-FOUND
		//           *            GO TO 9220-EXIT
		//           *        END-IF
		//                END-IF.
		if (Conditions.eq(lFrameworkResponseArea.getCw04rPhnTypCd(ws.getSubscripts().getPhnIdx()),
				ws.getConstantFields().getPhoneNbrCode().getFax1())) {
			// COB_CODE: SET SW-PRIMARY-FAX-NBR-FOUND
			//                                   TO TRUE
			ws.getSwitches().setPrimaryFaxNbrFlag(true);
			//    ELSE
			//        IF SW-PRIMARY-FAX-NBR-FOUND
			//            GO TO 9220-EXIT
			//        END-IF
		}
		// COB_CODE: MOVE CW04R-CIPH-PHN-NBR(SS-PHN-IDX)
		//                                       TO MUT04O-FAX-PHN-NBR-FMT.
		lServiceContractArea.setMut04oFaxPhnNbrFmt(lFrameworkResponseArea.getCw04rCiphPhnNbr(ws.getSubscripts().getPhnIdx()));
		// COB_CODE: MOVE MUT04O-FAX-PHN-NBR-FMT(2:3)
		//                                       TO MUT04O-FAX-PHN-ACD.
		lServiceContractArea.setMut04oFaxPhnAcd(lServiceContractArea.getMut04oFaxPhnNbrFmtFormatted().substring((2) - 1, 4));
		// COB_CODE: MOVE MUT04O-FAX-PHN-NBR-FMT(6:3)
		//                                       TO MUT04O-FAX-PHN-PFX-NBR.
		lServiceContractArea.setMut04oFaxPhnPfxNbr(lServiceContractArea.getMut04oFaxPhnNbrFmtFormatted().substring((6) - 1, 8));
		// COB_CODE: MOVE MUT04O-FAX-PHN-NBR-FMT(10:4)
		//                                       TO MUT04O-FAX-PHN-LIN-NBR.
		lServiceContractArea.setMut04oFaxPhnLinNbr(lServiceContractArea.getMut04oFaxPhnNbrFmtFormatted().substring((10) - 1, 13));
		// COB_CODE: MOVE MUT04O-FAX-PHN-NBR-FMT(17:5)
		//                                       TO MUT04O-FAX-PHN-EXT-NBR.
		lServiceContractArea.setMut04oFaxPhnExtNbr(lServiceContractArea.getMut04oFaxPhnNbrFmtFormatted().substring((17) - 1, 21));
	}

	/**Original name: 9230-FILL-CTC-NBR<br>
	 * <pre>** EJD START ***</pre>*/
	private void fillCtcNbr() {
		// COB_CODE: MOVE CW04R-CIPH-PHN-NBR(SS-PHN-IDX)
		//                                       TO MUT04O-CTC-PHN-NBR-FMT.
		lServiceContractArea.setMut04oCtcPhnNbrFmt(lFrameworkResponseArea.getCw04rCiphPhnNbr(ws.getSubscripts().getPhnIdx()));
		// COB_CODE: MOVE MUT04O-CTC-PHN-NBR-FMT(2:3)
		//                                       TO MUT04O-CTC-PHN-ACD.
		lServiceContractArea.setMut04oCtcPhnAcd(lServiceContractArea.getMut04oCtcPhnNbrFmtFormatted().substring((2) - 1, 4));
		// COB_CODE: MOVE MUT04O-CTC-PHN-NBR-FMT(6:3)
		//                                       TO MUT04O-CTC-PHN-PFX-NBR.
		lServiceContractArea.setMut04oCtcPhnPfxNbr(lServiceContractArea.getMut04oCtcPhnNbrFmtFormatted().substring((6) - 1, 8));
		// COB_CODE: MOVE MUT04O-CTC-PHN-NBR-FMT(10:4)
		//                                       TO MUT04O-CTC-PHN-LIN-NBR.
		lServiceContractArea.setMut04oCtcPhnLinNbr(lServiceContractArea.getMut04oCtcPhnNbrFmtFormatted().substring((10) - 1, 13));
		// COB_CODE: MOVE MUT04O-CTC-PHN-NBR-FMT(17:5)
		//                                       TO MUT04O-CTC-PHN-EXT-NBR.
		lServiceContractArea.setMut04oCtcPhnExtNbr(lServiceContractArea.getMut04oCtcPhnNbrFmtFormatted().substring((17) - 1, 21));
	}

	/**Original name: 9300-SET-CLT-TAX-INFO<br>
	 * <pre>** EJD END ***</pre>*/
	private void setCltTaxInfo() {
		// COB_CODE: EVALUATE CW27R-TAX-TYPE-CD
		//               WHEN CF-TT-FEIN
		//                      THRU 9310-EXIT
		//               WHEN CF-TT-SSN
		//                      THRU 9320-EXIT
		//           END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getCw27rTaxTypeCd(), ws.getConstantFields().getTtFein())) {
			// COB_CODE: PERFORM 9310-FILL-FEIN-NBR
			//              THRU 9310-EXIT
			fillFeinNbr();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw27rTaxTypeCd(), ws.getConstantFields().getTtSsn())) {
			// COB_CODE: PERFORM 9320-FILL-SSN-NBR
			//              THRU 9320-EXIT
			fillSsnNbr();
		}
	}

	/**Original name: 9310-FILL-FEIN-NBR<br>*/
	private void fillFeinNbr() {
		// COB_CODE: MOVE CW27R-CITX-TAX-ID      TO MUT04O-FEIN-FMT.
		lServiceContractArea.setMut04oFeinFmt(lFrameworkResponseArea.getCw27rCitxTaxId());
		// COB_CODE: MOVE MUT04O-FEIN-FMT(1:2)   TO MUT04O-FEIN.
		lServiceContractArea.setMut04oFein(lServiceContractArea.getMut04oFeinFmtFormatted().substring((1) - 1, 2));
		// COB_CODE: MOVE MUT04O-FEIN-FMT(4:7)   TO MUT04O-FEIN(3:).
		lServiceContractArea.setMut04oFein(Functions.setSubstring(lServiceContractArea.getMut04oFein(),
				lServiceContractArea.getMut04oFeinFmtFormatted().substring((4) - 1, 10), 3));
	}

	/**Original name: 9320-FILL-SSN-NBR<br>*/
	private void fillSsnNbr() {
		// COB_CODE: MOVE CW27R-CITX-TAX-ID      TO MUT04O-SSN-FMT.
		lServiceContractArea.setMut04oSsnFmt(lFrameworkResponseArea.getCw27rCitxTaxId());
		// COB_CODE: MOVE MUT04O-SSN-FMT(1:3)    TO MUT04O-SSN.
		lServiceContractArea.setMut04oSsn(lServiceContractArea.getMut04oSsnFmtFormatted().substring((1) - 1, 3));
		// COB_CODE: MOVE MUT04O-SSN-FMT(5:2)    TO MUT04O-SSN(4:).
		lServiceContractArea.setMut04oSsn(Functions.setSubstring(lServiceContractArea.getMut04oSsn(),
				lServiceContractArea.getMut04oSsnFmtFormatted().substring((5) - 1, 6), 4));
		// COB_CODE: MOVE MUT04O-SSN-FMT(8:4)    TO MUT04O-SSN(6:).
		lServiceContractArea.setMut04oSsn(Functions.setSubstring(lServiceContractArea.getMut04oSsn(),
				lServiceContractArea.getMut04oSsnFmtFormatted().substring((8) - 1, 11), 6));
	}

	/**Original name: 9400-SET-CLT-DEMO-INFO<br>*/
	private void setCltDemoInfo() {
		// COB_CODE: MOVE CW02R-LEG-ENT-CD       TO MUT04O-LEG-ETY-CD.
		lServiceContractArea.setMut04oLegEtyCd(lFrameworkResponseArea.getCw02rLegEntCd());
		// COB_CODE: MOVE CW02R-LEG-ENT-DESC     TO MUT04O-LEG-ETY-DES.
		lServiceContractArea.setMut04oLegEtyDes(lFrameworkResponseArea.getCw02rLegEntDesc());
		// COB_CODE: MOVE CW49R-SEG-CD           TO MUT04O-SEG-CD.
		lServiceContractArea.setMut04oSegCd(lFrameworkResponseArea.getCw49rSegCd());
		// COB_CODE: MOVE CW49R-SEG-DES          TO MUT04O-SEG-DES.
		lServiceContractArea.setMut04oSegDes(lFrameworkResponseArea.getCw49rSegDes());
		// COB_CODE: MOVE +0                     TO SS-PRI-CD-IDX.
		ws.getSubscripts().setPriCdIdx(((short) 0));
		// COB_CODE: MOVE +0                     TO SS-SE-SIC-IDX.
		ws.getSubscripts().setSeSicIdx(((short) 0));
	}

	/**Original name: 9400-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-PRI-CD-IDX.
		ws.getSubscripts().setPriCdIdx(Trunc.toShort(1 + ws.getSubscripts().getPriCdIdx(), 4));
		// COB_CODE: IF CW48R-TOB-SIC-TYP-CD(SS-PRI-CD-IDX) = SPACES
		//               GO TO 9400-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw48rTobSicTypCd(ws.getSubscripts().getPriCdIdx()))) {
			// COB_CODE: GO TO 9400-EXIT
			return "";
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN (CW48R-TOB-SIC-TYP-CD(SS-PRI-CD-IDX) = CF-PC-TOB)
		//                                       TO MUT04O-TOB-DES
		//               WHEN (CW48R-TOB-SIC-TYP-CD(SS-PRI-CD-IDX) = CF-PC-SIC)
		//                   END-IF
		//           END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getCw48rTobSicTypCd(ws.getSubscripts().getPriCdIdx()), ws.getConstantFields().getPcTob())) {
			// COB_CODE: MOVE CW48R-TOB-CD(SS-PRI-CD-IDX)
			//                               TO MUT04O-TOB-CD
			lServiceContractArea.setMut04oTobCd(lFrameworkResponseArea.getCw48rTobCd(ws.getSubscripts().getPriCdIdx()));
			// COB_CODE: MOVE CW48R-TOB-CD-DESC(SS-PRI-CD-IDX)
			//                               TO MUT04O-TOB-DES
			lServiceContractArea.setMut04oTobDes(lFrameworkResponseArea.getCw48rTobCdDesc(ws.getSubscripts().getPriCdIdx()));
		} else if (Conditions.eq(lFrameworkResponseArea.getCw48rTobSicTypCd(ws.getSubscripts().getPriCdIdx()), ws.getConstantFields().getPcSic())) {
			// COB_CODE: IF CW48R-TOB-PTY-CD(SS-PRI-CD-IDX) = CF-PRIMARY
			//                               TO MUT04O-SIC-DES
			//           ELSE
			//               END-IF
			//           END-IF
			if (Conditions.eq(lFrameworkResponseArea.getCw48rTobPtyCd(ws.getSubscripts().getPriCdIdx()), ws.getConstantFields().getPrimary())) {
				// COB_CODE: MOVE CW48R-TOB-CD(SS-PRI-CD-IDX)
				//                           TO MUT04O-SIC-CD
				lServiceContractArea.setMut04oSicCd(lFrameworkResponseArea.getCw48rTobCd(ws.getSubscripts().getPriCdIdx()));
				// COB_CODE: MOVE CW48R-TOB-CD-DESC(SS-PRI-CD-IDX)
				//                           TO MUT04O-SIC-DES
				lServiceContractArea.setMut04oSicDes(lFrameworkResponseArea.getCw48rTobCdDesc(ws.getSubscripts().getPriCdIdx()));
			} else if (ws.getSubscripts().isSeSicIdxMax()) {
				// COB_CODE: IF SS-SE-SIC-IDX-MAX
				//               CONTINUE
				//           ELSE
				//                                       (SS-SE-SIC-IDX)
				//           END-IF
				// COB_CODE: CONTINUE
				//continue
			} else {
				// COB_CODE: ADD +1      TO SS-SE-SIC-IDX
				ws.getSubscripts().setSeSicIdx(Trunc.toShort(1 + ws.getSubscripts().getSeSicIdx(), 4));
				// COB_CODE: MOVE CW48R-TOB-CD(SS-PRI-CD-IDX)
				//                       TO MUT04O-SE-SIC-CD
				//                                   (SS-SE-SIC-IDX)
				lServiceContractArea.setMut04oSeSicCd(ws.getSubscripts().getSeSicIdx(),
						lFrameworkResponseArea.getCw48rTobCd(ws.getSubscripts().getPriCdIdx()));
				// COB_CODE: MOVE CW48R-TOB-CD-DESC(SS-PRI-CD-IDX)
				//                       TO MUT04O-SE-SIC-DES
				//                                   (SS-SE-SIC-IDX)
				lServiceContractArea.setMut04oSeSicDes(ws.getSubscripts().getSeSicIdx(),
						lFrameworkResponseArea.getCw48rTobCdDesc(ws.getSubscripts().getPriCdIdx()));
			}
		}
		// COB_CODE: IF SS-PRI-CD-IDX-MAX
		//               GO TO 9400-EXIT
		//           END-IF.
		if (ws.getSubscripts().isPriCdIdxMax()) {
			// COB_CODE: GO TO 9400-EXIT
			return "";
		}
		// COB_CODE: GO TO 9400-A.
		return "9400-A";
	}

	/**Original name: 9500-SET-CLT-ACCT-INFO<br>*/
	private void setCltAcctInfo() {
		// COB_CODE: MOVE +0                     TO SS-ACT-IDX.
		ws.getSubscripts().setActIdx(((short) 0));
		// COB_CODE: MOVE CF-NO                  TO MUT04O-PC-PSPT-ACT-IND.
		lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getNo());
	}

	/**Original name: 9500-A<br>*/
	private String a3() {
		// COB_CODE: ADD +1                      TO SS-ACT-IDX.
		ws.getSubscripts().setActIdx(Trunc.toShort(1 + ws.getSubscripts().getActIdx(), 4));
		// COB_CODE: IF CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX) = SPACES
		//               GO TO 9500-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()))) {
			// COB_CODE: GO TO 9500-EXIT
			return "";
		}
		// COB_CODE: EVALUATE CW08R-OBJ-CD(SS-ACT-IDX)
		//               WHEN CF-AN-PROSPECT-ACCOUNT
		//                      THRU 9510-EXIT
		//               WHEN CF-AN-PANDC-ACCOUNT
		//                      THRU 9520-EXIT
		//               WHEN CF-AN-SELF-INSURED-ACT
		//                      THRU 9530-EXIT
		//               WHEN CF-AN-SPLIT-BILLED-ACT
		//                      THRU 9540-EXIT
		//               WHEN CF-AN-PRS-LINES-ACT
		//                      THRU 9550-EXIT
		//           END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()),
				ws.getConstantFields().getActNbrCode().getProspectAccount())) {
			// COB_CODE: PERFORM 9510-FILL-PROSPECT-ACT
			//              THRU 9510-EXIT
			fillProspectAct();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()),
				ws.getConstantFields().getActNbrCode().getPandcAccount())) {
			// COB_CODE: PERFORM 9520-FILL-PANDC-ACT
			//              THRU 9520-EXIT
			fillPandcAct();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()),
				ws.getConstantFields().getActNbrCode().getSelfInsuredAct())) {
			// COB_CODE: PERFORM 9530-FILL-SELF-INSURED-ACT
			//              THRU 9530-EXIT
			fillSelfInsuredAct();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()),
				ws.getConstantFields().getActNbrCode().getSplitBilledAct())) {
			// COB_CODE: PERFORM 9540-FILL-SPLIT-BILLED-ACT
			//              THRU 9540-EXIT
			fillSplitBilledAct();
		} else if (Conditions.eq(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()),
				ws.getConstantFields().getActNbrCode().getPrsLinesAct())) {
			// COB_CODE: PERFORM 9550-FILL-PRS-LINES-ACT
			//              THRU 9550-EXIT
			fillPrsLinesAct();
		}
		// COB_CODE: IF SS-ACT-IDX-MAX
		//               GO TO 9500-EXIT
		//           END-IF.
		if (ws.getSubscripts().isActIdxMax()) {
			// COB_CODE: GO TO 9500-EXIT
			return "";
		}
		// COB_CODE: GO TO 9500-A.
		return "9500-A";
	}

	/**Original name: 9510-FILL-PROSPECT-ACT<br>*/
	private void fillProspectAct() {
		// COB_CODE: IF MUT04O-PSPT-ACT-NBR NOT = SPACES
		//               GO TO 9510-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04oPsptActNbr())) {
			// COB_CODE: GO TO 9510-EXIT
			return;
		}
		// COB_CODE: IF MUT04O-PC-ACT-NBR = SPACES
		//               MOVE CF-YES             TO MUT04O-PC-PSPT-ACT-IND
		//           END-IF.
		if (Characters.EQ_SPACE.test(lServiceContractArea.getMut04oPcActNbr())) {
			// COB_CODE: MOVE CF-YES             TO MUT04O-PC-PSPT-ACT-IND
			lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getYes());
		}
		// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
		//                                       TO MUT04O-PSPT-ACT-NBR-FMT
		//                                          SA-FORMATTED-ACT-NBR.
		lServiceContractArea.setMut04oPsptActNbrFmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
		ws.getSaveArea().getFormattedActNbr()
				.setFormattedActNbrFormatted(lFrameworkResponseArea.getCw08rCiorShwObjKeyFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
		//                                       TO MUT04O-ACT-NM-ID.
		lServiceContractArea.setMut04oActNmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: PERFORM 9590-UNFORMAT-ACT-NBR
		//              THRU 9590-EXIT.
		unformatActNbr();
		// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR TO MUT04O-PSPT-ACT-NBR.
		lServiceContractArea.setMut04oPsptActNbr(ws.getSaveArea().getUnformattedActNbr());
		// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
		//                                       TO MUT04O-OBJ-CD.
		lServiceContractArea.setMut04oObjCd(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
		//                                       TO MUT04O-OBJ-DES.
		lServiceContractArea.setMut04oObjDes(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
	}

	/**Original name: 9520-FILL-PANDC-ACT<br>*/
	private void fillPandcAct() {
		// COB_CODE: MOVE CF-NO                  TO MUT04O-PC-PSPT-ACT-IND.
		lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getNo());
		// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
		//                                       TO SA-FORMATTED-ACT-NBR.
		ws.getSaveArea().getFormattedActNbr()
				.setFormattedActNbrFormatted(lFrameworkResponseArea.getCw08rCiorShwObjKeyFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: PERFORM 9590-UNFORMAT-ACT-NBR
		//              THRU 9590-EXIT.
		unformatActNbr();
		// COB_CODE: IF MUT04O-PC-ACT-NBR = SPACES
		//                                       TO MUT04O-OBJ-DES
		//           ELSE
		//               END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(lServiceContractArea.getMut04oPcActNbr())) {
			// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
			//                                   TO MUT04O-PC-ACT-NBR-FMT
			lServiceContractArea.setMut04oPcActNbrFmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR
			//                                   TO MUT04O-PC-ACT-NBR
			lServiceContractArea.setMut04oPcActNbr(ws.getSaveArea().getUnformattedActNbr());
			// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
			//                                   TO MUT04O-ACT-NM-ID
			lServiceContractArea.setMut04oActNmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
			//                                   TO MUT04O-OBJ-CD
			lServiceContractArea.setMut04oObjCd(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
			//                                   TO MUT04O-OBJ-DES
			lServiceContractArea.setMut04oObjDes(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
		} else if (Characters.EQ_SPACE.test(lServiceContractArea.getMut04oPcActNbr2())) {
			// COB_CODE: IF MUT04O-PC-ACT-NBR-2 = SPACES
			//                                   TO MUT04O-OBJ-DES-2
			//           END-IF
			// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
			//                               TO MUT04O-PC-ACT-NBR-2-FMT
			lServiceContractArea.setMut04oPcActNbr2Fmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR
			//                               TO MUT04O-PC-ACT-NBR-2
			lServiceContractArea.setMut04oPcActNbr2(ws.getSaveArea().getUnformattedActNbr());
			// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
			//                               TO MUT04O-ACT-2-NM-ID
			lServiceContractArea.setMut04oAct2NmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
			//                               TO MUT04O-OBJ-CD-2
			lServiceContractArea.setMut04oObjCd2(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
			// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
			//                               TO MUT04O-OBJ-DES-2
			lServiceContractArea.setMut04oObjDes2(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
		}
	}

	/**Original name: 9530-FILL-SELF-INSURED-ACT<br>*/
	private void fillSelfInsuredAct() {
		// COB_CODE: IF MUT04O-SIN-WC-ACT-NBR NOT = SPACES
		//               GO TO 9530-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04oSinWcActNbr())) {
			// COB_CODE: GO TO 9530-EXIT
			return;
		}
		// COB_CODE: MOVE CF-NO                  TO MUT04O-PC-PSPT-ACT-IND.
		lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getNo());
		// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
		//                                       TO MUT04O-SIN-WC-ACT-NBR-FMT
		//                                          SA-FORMATTED-ACT-NBR.
		lServiceContractArea.setMut04oSinWcActNbrFmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
		ws.getSaveArea().getFormattedActNbr()
				.setFormattedActNbrFormatted(lFrameworkResponseArea.getCw08rCiorShwObjKeyFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
		//                                       TO MUT04O-SIN-WC-ACT-NM-ID.
		lServiceContractArea.setMut04oSinWcActNmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: PERFORM 9590-UNFORMAT-ACT-NBR
		//              THRU 9590-EXIT.
		unformatActNbr();
		// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR TO MUT04O-SIN-WC-ACT-NBR.
		lServiceContractArea.setMut04oSinWcActNbr(ws.getSaveArea().getUnformattedActNbr());
		// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
		//                                       TO MUT04O-SIN-WC-OBJ-CD.
		lServiceContractArea.setMut04oSinWcObjCd(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
		//                                       TO MUT04O-SIN-WC-OBJ-DES.
		lServiceContractArea.setMut04oSinWcObjDes(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
	}

	/**Original name: 9540-FILL-SPLIT-BILLED-ACT<br>*/
	private void fillSplitBilledAct() {
		// COB_CODE: IF MUT04O-SPL-BILL-ACT-NBR NOT = SPACES
		//               GO TO 9540-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04oSplBillActNbr())) {
			// COB_CODE: GO TO 9540-EXIT
			return;
		}
		// COB_CODE: MOVE CF-NO                  TO MUT04O-PC-PSPT-ACT-IND.
		lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getNo());
		// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
		//                                       TO MUT04O-SPL-BILL-ACT-NBR-FMT
		//                                          SA-FORMATTED-ACT-NBR.
		lServiceContractArea.setMut04oSplBillActNbrFmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
		ws.getSaveArea().getFormattedActNbr()
				.setFormattedActNbrFormatted(lFrameworkResponseArea.getCw08rCiorShwObjKeyFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
		//                                       TO MUT04O-SPL-BILL-ACT-NM-ID.
		lServiceContractArea.setMut04oSplBillActNmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: PERFORM 9590-UNFORMAT-ACT-NBR
		//              THRU 9590-EXIT.
		unformatActNbr();
		// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR TO MUT04O-SPL-BILL-ACT-NBR.
		lServiceContractArea.setMut04oSplBillActNbr(ws.getSaveArea().getUnformattedActNbr());
		// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
		//                                       TO MUT04O-SPL-BILL-OBJ-CD.
		lServiceContractArea.setMut04oSplBillObjCd(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
		//                                       TO MUT04O-SPL-BILL-OBJ-DES.
		lServiceContractArea.setMut04oSplBillObjDes(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
	}

	/**Original name: 9550-FILL-PRS-LINES-ACT<br>*/
	private void fillPrsLinesAct() {
		// COB_CODE: IF MUT04O-PRS-LIN-ACT-NBR NOT = SPACES
		//               GO TO 9550-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getMut04oPrsLinActNbr())) {
			// COB_CODE: GO TO 9550-EXIT
			return;
		}
		// COB_CODE: MOVE CF-NO                  TO MUT04O-PC-PSPT-ACT-IND.
		lServiceContractArea.setMut04oPcPsptActInd(ws.getConstantFields().getNo());
		// COB_CODE: MOVE CW08R-CIOR-SHW-OBJ-KEY(SS-ACT-IDX)
		//                                       TO MUT04O-PRS-LIN-ACT-NBR-FMT
		//                                          SA-FORMATTED-ACT-NBR.
		lServiceContractArea.setMut04oPrsLinActNbrFmt(lFrameworkResponseArea.getCw08rCiorShwObjKey(ws.getSubscripts().getActIdx()));
		ws.getSaveArea().getFormattedActNbr()
				.setFormattedActNbrFormatted(lFrameworkResponseArea.getCw08rCiorShwObjKeyFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-CIOR-OBJ-SEQ-NBR(SS-ACT-IDX)
		//                                       TO MUT04O-PRS-LIN-ACT-NM-ID.
		lServiceContractArea.setMut04oPrsLinActNmIdFormatted(lFrameworkResponseArea.getCw08rCiorObjSeqNbrFormatted(ws.getSubscripts().getActIdx()));
		// COB_CODE: PERFORM 9590-UNFORMAT-ACT-NBR
		//              THRU 9590-EXIT.
		unformatActNbr();
		// COB_CODE: MOVE SA-UNFORMATTED-ACT-NBR TO MUT04O-PRS-LIN-ACT-NBR.
		lServiceContractArea.setMut04oPrsLinActNbr(ws.getSaveArea().getUnformattedActNbr());
		// COB_CODE: MOVE CW08R-OBJ-CD(SS-ACT-IDX)
		//                                       TO MUT04O-PRS-LIN-OBJ-CD.
		lServiceContractArea.setMut04oPrsLinObjCd(lFrameworkResponseArea.getCw08rObjCd(ws.getSubscripts().getActIdx()));
		// COB_CODE: MOVE CW08R-OBJ-DESC(SS-ACT-IDX)
		//                                       TO MUT04O-PRS-LIN-OBJ-DES.
		lServiceContractArea.setMut04oPrsLinObjDes(lFrameworkResponseArea.getCw08rObjDesc(ws.getSubscripts().getActIdx()));
	}

	/**Original name: 9590-UNFORMAT-ACT-NBR<br>*/
	private void unformatActNbr() {
		ConcatUtil concatUtil = null;
		// COB_CODE: STRING SA-FA-FIRST-3
		//                  SA-FA-SECOND-3
		//                  SA-FA-LAST-1         DELIMITED BY SIZE
		//               INTO SA-UNFORMATTED-ACT-NBR
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(SaveAreaMu0x0004.Len.UNFORMATTED_ACT_NBR, ws.getSaveArea().getFormattedActNbr().getFirst3Formatted(),
				ws.getSaveArea().getFormattedActNbr().getSecond3Formatted(), String.valueOf(ws.getSaveArea().getFormattedActNbr().getLast1()));
		ws.getSaveArea().setUnformattedActNbr(concatUtil.replaceInString(ws.getSaveArea().getUnformattedActNbrFormatted()));
	}

	/**Original name: 9600-SET-CLT-CST-INFO<br>*/
	private void setCltCstInfo() {
		// COB_CODE: MOVE CW11R-CIRF-REF-ID      TO MUT04O-CST-NBR.
		lServiceContractArea.setMut04oCstNbr(lFrameworkResponseArea.getCw11rCirfRefId());
	}

	/**Original name: 9700-SET-CLT-MUT-TER-INFO<br>*/
	private void setCltMutTerInfo() {
		// COB_CODE: MOVE CW45R-SMR-CLT-ID       TO MUT04O-MKT-CLT-ID.
		lServiceContractArea.setMut04oMktCltId(lFrameworkResponseArea.getCw45rSmrCltId());
		// COB_CODE: MOVE CW45R-TER-NBR          TO MUT04O-MKT-TER-NBR
		lServiceContractArea.setMut04oMktTerNbr(lFrameworkResponseArea.getCw45rTerNbr());
		// COB_CODE: MOVE CW45R-SMR-CLT-ID       TO MUT04O-MT-SMR-CLT-ID.
		lServiceContractArea.setMut04oMtSmrCltId(lFrameworkResponseArea.getCw45rSmrCltId());
		// COB_CODE: MOVE CW45R-SMR-TER-NBR      TO MUT04O-MT-SMR-TER-NBR.
		lServiceContractArea.setMut04oMtSmrTerNbr(lFrameworkResponseArea.getCw45rSmrTerNbr());
		// COB_CODE: MOVE CW45R-SMR-FST-NM       TO SA-FST-NM
		//                                          MUT04O-MT-SMR-FST-NM.
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rSmrFstNm());
		lServiceContractArea.setMut04oMtSmrFstNm(lFrameworkResponseArea.getCw45rSmrFstNm());
		// COB_CODE: MOVE CW45R-SMR-MDL-NM       TO SA-MDL-NM
		//                                          MUT04O-MT-SMR-MDL-NM.
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rSmrMdlNm());
		lServiceContractArea.setMut04oMtSmrMdlNm(lFrameworkResponseArea.getCw45rSmrMdlNm());
		// COB_CODE: MOVE CW45R-SMR-LST-NM       TO SA-LST-NM
		//                                          MUT04O-MT-SMR-LST-NM.
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rSmrLstNm());
		lServiceContractArea.setMut04oMtSmrLstNm(lFrameworkResponseArea.getCw45rSmrLstNm());
		// COB_CODE: MOVE CW45R-SMR-NM-PFX       TO MUT04O-MT-SMR-PFX.
		lServiceContractArea.setMut04oMtSmrPfx(lFrameworkResponseArea.getCw45rSmrNmPfx());
		// COB_CODE: MOVE CW45R-SMR-NM-SFX       TO SA-SFX
		//                                          MUT04O-MT-SMR-SFX.
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rSmrNmSfx());
		lServiceContractArea.setMut04oMtSmrSfx(lFrameworkResponseArea.getCw45rSmrNmSfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MKT-SVC-DSY-NM
		//                                          MUT04O-MT-SMR-DSY-NM.
		lServiceContractArea.setMut04oMktSvcDsyNm(ws.getSaveArea().getDisplayName());
		lServiceContractArea.setMut04oMtSmrDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CF-MA-MUT-CD           TO MUT04O-MKT-TYPE-CD.
		lServiceContractArea.setMut04oMktTypeCd(ws.getConstantFields().getMaMutCd());
		// COB_CODE: MOVE CF-MA-MUT-CD-DES       TO MUT04O-MKT-TYPE-DES.
		lServiceContractArea.setMut04oMktTypeDes(ws.getConstantFields().getMaMutCdDes());
		// COB_CODE: MOVE CW45R-MR-CLT-ID        TO MUT04O-MT-MKT-CLT-ID.
		lServiceContractArea.setMut04oMtMktCltId(lFrameworkResponseArea.getCw45rMrCltId());
		// COB_CODE: MOVE CW45R-MR-TER-NBR       TO MUT04O-MT-MKT-TER-NBR.
		lServiceContractArea.setMut04oMtMktTerNbr(lFrameworkResponseArea.getCw45rMrTerNbr());
		// COB_CODE: MOVE CW45R-MR-FST-NM        TO MUT04O-MT-MKT-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oMtMktFstNm(lFrameworkResponseArea.getCw45rMrFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rMrFstNm());
		// COB_CODE: MOVE CW45R-MR-MDL-NM        TO MUT04O-MT-MKT-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oMtMktMdlNm(lFrameworkResponseArea.getCw45rMrMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rMrMdlNm());
		// COB_CODE: MOVE CW45R-MR-LST-NM        TO MUT04O-MT-MKT-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oMtMktLstNm(lFrameworkResponseArea.getCw45rMrLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rMrLstNm());
		// COB_CODE: MOVE CW45R-MR-NM-SFX        TO MUT04O-MT-MKT-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oMtMktSfx(lFrameworkResponseArea.getCw45rMrNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rMrNmSfx());
		// COB_CODE: MOVE CW45R-MR-NM-PFX        TO MUT04O-MT-MKT-PFX.
		lServiceContractArea.setMut04oMtMktPfx(lFrameworkResponseArea.getCw45rMrNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MT-MKT-DSY-NM.
		lServiceContractArea.setMut04oMtMktDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW45R-CSR-CLT-ID       TO MUT04O-MT-CSR-CLT-ID.
		lServiceContractArea.setMut04oMtCsrCltId(lFrameworkResponseArea.getCw45rCsrCltId());
		// COB_CODE: MOVE CW45R-CSR-TER-NBR      TO MUT04O-MT-CSR-TER-NBR.
		lServiceContractArea.setMut04oMtCsrTerNbr(lFrameworkResponseArea.getCw45rCsrTerNbr());
		// COB_CODE: MOVE CW45R-CSR-FST-NM       TO MUT04O-MT-CSR-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oMtCsrFstNm(lFrameworkResponseArea.getCw45rCsrFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rCsrFstNm());
		// COB_CODE: MOVE CW45R-CSR-MDL-NM       TO MUT04O-MT-CSR-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oMtCsrMdlNm(lFrameworkResponseArea.getCw45rCsrMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rCsrMdlNm());
		// COB_CODE: MOVE CW45R-CSR-LST-NM       TO MUT04O-MT-CSR-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oMtCsrLstNm(lFrameworkResponseArea.getCw45rCsrLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rCsrLstNm());
		// COB_CODE: MOVE CW45R-CSR-NM-SFX       TO MUT04O-MT-CSR-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oMtCsrSfx(lFrameworkResponseArea.getCw45rCsrNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rCsrNmSfx());
		// COB_CODE: MOVE CW45R-CSR-NM-PFX       TO MUT04O-MT-CSR-PFX.
		lServiceContractArea.setMut04oMtCsrPfx(lFrameworkResponseArea.getCw45rCsrNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MT-CSR-DSY-NM.
		lServiceContractArea.setMut04oMtCsrDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW45R-DMM-CLT-ID       TO MUT04O-MT-DMM-CLT-ID.
		lServiceContractArea.setMut04oMtDmmCltId(lFrameworkResponseArea.getCw45rDmmCltId());
		// COB_CODE: MOVE CW45R-DMM-TER-NBR      TO MUT04O-MT-DMM-TER-NBR.
		lServiceContractArea.setMut04oMtDmmTerNbr(lFrameworkResponseArea.getCw45rDmmTerNbr());
		// COB_CODE: MOVE CW45R-DMM-FST-NM       TO MUT04O-MT-DMM-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oMtDmmFstNm(lFrameworkResponseArea.getCw45rDmmFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rDmmFstNm());
		// COB_CODE: MOVE CW45R-DMM-MDL-NM       TO MUT04O-MT-DMM-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oMtDmmMdlNm(lFrameworkResponseArea.getCw45rDmmMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rDmmMdlNm());
		// COB_CODE: MOVE CW45R-DMM-LST-NM       TO MUT04O-MT-DMM-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oMtDmmLstNm(lFrameworkResponseArea.getCw45rDmmLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rDmmLstNm());
		// COB_CODE: MOVE CW45R-DMM-NM-SFX       TO MUT04O-MT-DMM-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oMtDmmSfx(lFrameworkResponseArea.getCw45rDmmNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rDmmNmSfx());
		// COB_CODE: MOVE CW45R-DMM-NM-PFX       TO MUT04O-MT-DMM-PFX.
		lServiceContractArea.setMut04oMtDmmPfx(lFrameworkResponseArea.getCw45rDmmNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MT-DMM-DSY-NM.
		lServiceContractArea.setMut04oMtDmmDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW45R-RMM-CLT-ID       TO MUT04O-MT-RMM-CLT-ID.
		lServiceContractArea.setMut04oMtRmmCltId(lFrameworkResponseArea.getCw45rRmmCltId());
		// COB_CODE: MOVE CW45R-RMM-TER-NBR      TO MUT04O-MT-RMM-TER-NBR.
		lServiceContractArea.setMut04oMtRmmTerNbr(lFrameworkResponseArea.getCw45rRmmTerNbr());
		// COB_CODE: MOVE CW45R-RMM-FST-NM       TO MUT04O-MT-RMM-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oMtRmmFstNm(lFrameworkResponseArea.getCw45rRmmFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rRmmFstNm());
		// COB_CODE: MOVE CW45R-RMM-MDL-NM       TO MUT04O-MT-RMM-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oMtRmmMdlNm(lFrameworkResponseArea.getCw45rRmmMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rRmmMdlNm());
		// COB_CODE: MOVE CW45R-RMM-LST-NM       TO MUT04O-MT-RMM-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oMtRmmLstNm(lFrameworkResponseArea.getCw45rRmmLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rRmmLstNm());
		// COB_CODE: MOVE CW45R-RMM-NM-SFX       TO MUT04O-MT-RMM-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oMtRmmSfx(lFrameworkResponseArea.getCw45rRmmNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rRmmNmSfx());
		// COB_CODE: MOVE CW45R-RMM-NM-PFX       TO MUT04O-MT-RMM-PFX.
		lServiceContractArea.setMut04oMtRmmPfx(lFrameworkResponseArea.getCw45rRmmNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MT-RMM-DSY-NM.
		lServiceContractArea.setMut04oMtRmmDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW45R-DFO-CLT-ID       TO MUT04O-MT-DFO-CLT-ID.
		lServiceContractArea.setMut04oMtDfoCltId(lFrameworkResponseArea.getCw45rDfoCltId());
		// COB_CODE: MOVE CW45R-DFO-TER-NBR      TO MUT04O-MT-DFO-TER-NBR.
		lServiceContractArea.setMut04oMtDfoTerNbr(lFrameworkResponseArea.getCw45rDfoTerNbr());
		// COB_CODE: MOVE CW45R-DFO-FST-NM       TO MUT04O-MT-DFO-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oMtDfoFstNm(lFrameworkResponseArea.getCw45rDfoFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw45rDfoFstNm());
		// COB_CODE: MOVE CW45R-DFO-MDL-NM       TO MUT04O-MT-DFO-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oMtDfoMdlNm(lFrameworkResponseArea.getCw45rDfoMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw45rDfoMdlNm());
		// COB_CODE: MOVE CW45R-DFO-LST-NM       TO MUT04O-MT-DFO-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oMtDfoLstNm(lFrameworkResponseArea.getCw45rDfoLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw45rDfoLstNm());
		// COB_CODE: MOVE CW45R-DFO-NM-SFX       TO MUT04O-MT-DFO-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oMtDfoSfx(lFrameworkResponseArea.getCw45rDfoNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw45rDfoNmSfx());
		// COB_CODE: MOVE CW45R-DFO-NM-PFX       TO MUT04O-MT-DFO-PFX.
		lServiceContractArea.setMut04oMtDfoPfx(lFrameworkResponseArea.getCw45rDfoNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-MT-DFO-DSY-NM.
		lServiceContractArea.setMut04oMtDfoDsyNm(ws.getSaveArea().getDisplayName());
	}

	/**Original name: 9800-SET-CLT-AGC-TER-INFO<br>*/
	private void setCltAgcTerInfo() {
		// COB_CODE: MOVE CW46R-SMR-CLT-ID       TO MUT04O-MKT-CLT-ID.
		lServiceContractArea.setMut04oMktCltId(lFrameworkResponseArea.getCw46rSmrCltId());
		// COB_CODE: MOVE CW46R-TER-NBR          TO MUT04O-MKT-TER-NBR
		lServiceContractArea.setMut04oMktTerNbr(lFrameworkResponseArea.getCw46rTerNbr());
		// COB_CODE: MOVE CW46R-SMR-NM           TO MUT04O-MKT-SVC-DSY-NM.
		lServiceContractArea.setMut04oMktSvcDsyNm(lFrameworkResponseArea.getCw46rSmrNm());
		// COB_CODE: MOVE CF-MA-AGC-CD           TO MUT04O-MKT-TYPE-CD.
		lServiceContractArea.setMut04oMktTypeCd(ws.getConstantFields().getMaAgcCd());
		// COB_CODE: MOVE CF-MA-AGC-CD-DES       TO MUT04O-MKT-TYPE-DES.
		lServiceContractArea.setMut04oMktTypeDes(ws.getConstantFields().getMaAgcCdDes());
		// COB_CODE: MOVE CW46R-BRN-CLT-ID       TO MUT04O-AT-BRN-CLT-ID.
		lServiceContractArea.setMut04oAtBrnCltId(lFrameworkResponseArea.getCw46rBrnCltId());
		// COB_CODE: MOVE CW46R-BRN-TER-NBR      TO MUT04O-AT-BRN-TER-NBR.
		lServiceContractArea.setMut04oAtBrnTerNbr(lFrameworkResponseArea.getCw46rBrnTerNbr());
		// COB_CODE: MOVE CW46R-BRN-NM           TO MUT04O-AT-BRN-NM.
		lServiceContractArea.setMut04oAtBrnNm(lFrameworkResponseArea.getCw46rBrnNm());
		// COB_CODE: MOVE CW46R-AGC-CLT-ID       TO MUT04O-AT-AGC-CLT-ID.
		lServiceContractArea.setMut04oAtAgcCltId(lFrameworkResponseArea.getCw46rAgcCltId());
		// COB_CODE: MOVE CW46R-AGC-TER-NBR      TO MUT04O-AT-AGC-TER-NBR.
		lServiceContractArea.setMut04oAtAgcTerNbr(lFrameworkResponseArea.getCw46rAgcTerNbr());
		// COB_CODE: MOVE CW46R-AGC-NM           TO MUT04O-AT-AGC-NM.
		lServiceContractArea.setMut04oAtAgcNm(lFrameworkResponseArea.getCw46rAgcNm());
		// COB_CODE: MOVE CW46R-SMR-CLT-ID       TO MUT04O-AT-SMR-CLT-ID.
		lServiceContractArea.setMut04oAtSmrCltId(lFrameworkResponseArea.getCw46rSmrCltId());
		// COB_CODE: MOVE CW46R-SMR-TER-NBR      TO MUT04O-AT-SMR-TER-NBR.
		lServiceContractArea.setMut04oAtSmrTerNbr(lFrameworkResponseArea.getCw46rSmrTerNbr());
		// COB_CODE: MOVE CW46R-SMR-NM           TO MUT04O-AT-SMR-NM.
		lServiceContractArea.setMut04oAtSmrNm(lFrameworkResponseArea.getCw46rSmrNm());
		// COB_CODE: MOVE CW46R-AMM-CLT-ID       TO MUT04O-AT-AMM-CLT-ID.
		lServiceContractArea.setMut04oAtAmmCltId(lFrameworkResponseArea.getCw46rAmmCltId());
		// COB_CODE: MOVE CW46R-AMM-TER-NBR      TO MUT04O-AT-AMM-TER-NBR.
		lServiceContractArea.setMut04oAtAmmTerNbr(lFrameworkResponseArea.getCw46rAmmTerNbr());
		// COB_CODE: MOVE CW46R-AMM-FST-NM       TO MUT04O-AT-AMM-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oAtAmmFstNm(lFrameworkResponseArea.getCw46rAmmFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw46rAmmFstNm());
		// COB_CODE: MOVE CW46R-AMM-MDL-NM       TO MUT04O-AT-AMM-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oAtAmmMdlNm(lFrameworkResponseArea.getCw46rAmmMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw46rAmmMdlNm());
		// COB_CODE: MOVE CW46R-AMM-LST-NM       TO MUT04O-AT-AMM-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oAtAmmLstNm(lFrameworkResponseArea.getCw46rAmmLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw46rAmmLstNm());
		// COB_CODE: MOVE CW46R-AMM-NM-SFX       TO MUT04O-AT-AMM-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oAtAmmSfx(lFrameworkResponseArea.getCw46rAmmNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw46rAmmNmSfx());
		// COB_CODE: MOVE CW46R-AMM-NM-PFX       TO MUT04O-AT-AMM-PFX.
		lServiceContractArea.setMut04oAtAmmPfx(lFrameworkResponseArea.getCw46rAmmNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-AT-AMM-DSY-NM.
		lServiceContractArea.setMut04oAtAmmDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW46R-AFM-CLT-ID       TO MUT04O-AT-AFM-CLT-ID.
		lServiceContractArea.setMut04oAtAfmCltId(lFrameworkResponseArea.getCw46rAfmCltId());
		// COB_CODE: MOVE CW46R-AFM-TER-NBR      TO MUT04O-AT-AFM-TER-NBR.
		lServiceContractArea.setMut04oAtAfmTerNbr(lFrameworkResponseArea.getCw46rAfmTerNbr());
		// COB_CODE: MOVE CW46R-AFM-FST-NM       TO MUT04O-AT-AFM-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oAtAfmFstNm(lFrameworkResponseArea.getCw46rAfmFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw46rAfmFstNm());
		// COB_CODE: MOVE CW46R-AFM-MDL-NM       TO MUT04O-AT-AFM-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oAtAfmMdlNm(lFrameworkResponseArea.getCw46rAfmMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw46rAfmMdlNm());
		// COB_CODE: MOVE CW46R-AFM-LST-NM       TO MUT04O-AT-AFM-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oAtAfmLstNm(lFrameworkResponseArea.getCw46rAfmLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw46rAfmLstNm());
		// COB_CODE: MOVE CW46R-AFM-NM-SFX       TO MUT04O-AT-AFM-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oAtAfmSfx(lFrameworkResponseArea.getCw46rAfmNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw46rAfmNmSfx());
		// COB_CODE: MOVE CW46R-AFM-NM-PFX       TO MUT04O-AT-AFM-PFX.
		lServiceContractArea.setMut04oAtAfmPfx(lFrameworkResponseArea.getCw46rAfmNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-AT-AFM-DSY-NM.
		lServiceContractArea.setMut04oAtAfmDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE CW46R-AVP-CLT-ID       TO MUT04O-AT-AVP-CLT-ID.
		lServiceContractArea.setMut04oAtAvpCltId(lFrameworkResponseArea.getCw46rAvpCltId());
		// COB_CODE: MOVE CW46R-AVP-TER-NBR      TO MUT04O-AT-AVP-TER-NBR.
		lServiceContractArea.setMut04oAtAvpTerNbr(lFrameworkResponseArea.getCw46rAvpTerNbr());
		// COB_CODE: MOVE CW46R-AVP-FST-NM       TO MUT04O-AT-AVP-FST-NM
		//                                          SA-FST-NM.
		lServiceContractArea.setMut04oAtAvpFstNm(lFrameworkResponseArea.getCw46rAvpFstNm());
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCw46rAvpFstNm());
		// COB_CODE: MOVE CW46R-AVP-MDL-NM       TO MUT04O-AT-AVP-MDL-NM
		//                                          SA-MDL-NM.
		lServiceContractArea.setMut04oAtAvpMdlNm(lFrameworkResponseArea.getCw46rAvpMdlNm());
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCw46rAvpMdlNm());
		// COB_CODE: MOVE CW46R-AVP-LST-NM       TO MUT04O-AT-AVP-LST-NM
		//                                          SA-LST-NM.
		lServiceContractArea.setMut04oAtAvpLstNm(lFrameworkResponseArea.getCw46rAvpLstNm());
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCw46rAvpLstNm());
		// COB_CODE: MOVE CW46R-AVP-NM-SFX       TO MUT04O-AT-AVP-SFX
		//                                          SA-SFX.
		lServiceContractArea.setMut04oAtAvpSfx(lFrameworkResponseArea.getCw46rAvpNmSfx());
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getCw46rAvpNmSfx());
		// COB_CODE: MOVE CW46R-AVP-NM-PFX       TO MUT04O-AT-AVP-PFX.
		lServiceContractArea.setMut04oAtAvpPfx(lFrameworkResponseArea.getCw46rAvpNmPfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-AT-AVP-DSY-NM.
		lServiceContractArea.setMut04oAtAvpDsyNm(ws.getSaveArea().getDisplayName());
	}

	/**Original name: 9900-SET-CLT-UW-INFO<br>*/
	private void setCltUwInfo() {
		// COB_CODE: MOVE +0                     TO SS-CLT-RLT-IDX.
		ws.getSubscripts().setCltRltIdx(((short) 0));
	}

	/**Original name: 9900-A<br>*/
	private String a4() {
		// COB_CODE: ADD +1                      TO SS-CLT-RLT-IDX.
		ws.getSubscripts().setCltRltIdx(Trunc.toShort(1 + ws.getSubscripts().getCltRltIdx(), 4));
		// COB_CODE: IF SS-CLT-RLT-IDX-MAX
		//               GO TO 9900-EXIT
		//           END-IF.
		if (ws.getSubscripts().isCltRltIdxMax()) {
			// COB_CODE: GO TO 9900-EXIT
			return "";
		}
		// COB_CODE: IF CW06R-XRF-TYP-CD(SS-CLT-RLT-IDX) = SPACES
		//               GO TO 9900-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCw06rXrfTypCd(ws.getSubscripts().getCltRltIdx()))) {
			// COB_CODE: GO TO 9900-EXIT
			return "";
		}
		//**  IF WE ARE NOT LOOKING AT A CLT_CLT_RELATION ROW THAT IS AN
		//**  UNDERWRITING RELATIONSHIP WE WILL SKIP IT.
		// COB_CODE: IF CW06R-XRF-TYP-CD(SS-CLT-RLT-IDX) NOT = CF-MC-UNDERWRITER
		//             AND
		//              CW06R-XRF-TYP-CD(SS-CLT-RLT-IDX) NOT = CF-MC-RISK-ANALYST
		//             AND
		//              CW06R-XRF-TYP-CD(SS-CLT-RLT-IDX)
		//                                        NOT = CF-MC-FLD-PRODUCTION-UW
		//             AND
		//              CW06R-XRF-TYP-CD(SS-CLT-RLT-IDX)
		//                                         NOT = CF-MC-LP-RISK-ANALYST
		//               GO TO 9900-A
		//           END-IF.
		if (!Conditions.eq(lFrameworkResponseArea.getCw06rXrfTypCd(ws.getSubscripts().getCltRltIdx()),
				ws.getConstantFields().getMarketingCode().getUnderwriter())
				&& !Conditions.eq(lFrameworkResponseArea.getCw06rXrfTypCd(ws.getSubscripts().getCltRltIdx()),
						ws.getConstantFields().getMarketingCode().getRiskAnalyst())
				&& !Conditions.eq(lFrameworkResponseArea.getCw06rXrfTypCd(ws.getSubscripts().getCltRltIdx()),
						ws.getConstantFields().getMarketingCode().getFldProductionUw())
				&& !Conditions.eq(lFrameworkResponseArea.getCw06rXrfTypCd(ws.getSubscripts().getCltRltIdx()),
						ws.getConstantFields().getMarketingCode().getLpRiskAnalyst())) {
			// COB_CODE: GO TO 9900-A
			return "9900-A";
		}
		// COB_CODE: IF MUA02-CICR-XRF-ID NOT = CW06R-CICR-XRF-ID(SS-CLT-RLT-IDX)
		//               GO TO 9900-A
		//           END-IF.
		if (!Conditions.eq(lFrameworkResponseArea.getMua02CicrXrfId(), lFrameworkResponseArea.getCw06rCicrXrfId(ws.getSubscripts().getCltRltIdx()))) {
			// COB_CODE: GO TO 9900-A
			return "9900-A";
		}
		// SINCE AN ACCOUNT CAN ONLY HAVE AN UNDERWRITER OR FPU ASSIGNED
		// TO IT, THIS CAN BE SIMPLIFIED.
		// COB_CODE: PERFORM 9905-FILL-UW-INFO
		//              THRU 9905-EXIT.
		fillUwInfo();
		return "";
	}

	/**Original name: 9905-FILL-UW-INFO<br>*/
	private void fillUwInfo() {
		// COB_CODE: MOVE MUA02-CICR-FST-NM      TO SA-FST-NM.
		ws.getSaveArea().setFstNm(lFrameworkResponseArea.getMua02CicrFstNm());
		// COB_CODE: MOVE MUA02-CICR-MDL-NM      TO SA-MDL-NM.
		ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getMua02CicrMdlNm());
		// COB_CODE: MOVE MUA02-CICR-LST-NM      TO SA-LST-NM
		ws.getSaveArea().setLstNm(lFrameworkResponseArea.getMua02CicrLstNm());
		// COB_CODE: MOVE MUA02-NM-SFX           TO SA-SFX.
		ws.getSaveArea().setSfx(lFrameworkResponseArea.getMua02NmSfx());
		// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
		//              THRU 9A00-EXIT.
		a00FormatDsyName();
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-SVC-UW-DSY-NM.
		lServiceContractArea.setMut04oSvcUwDsyNm(ws.getSaveArea().getDisplayName());
		// COB_CODE: MOVE MUA02-XRF-TYP-CD       TO MUT04O-SVC-UW-TYPE-CD
		lServiceContractArea.setMut04oSvcUwTypeCd(lFrameworkResponseArea.getMua02XrfTypCd());
		// COB_CODE: MOVE MUA02-CLIENT-DES       TO MUT04O-SVC-UW-TYPE-DES.
		lServiceContractArea.setMut04oSvcUwTypeDes(lFrameworkResponseArea.getMua02ClientDes());
		// COB_CODE:      EVALUATE MUA02-XRF-TYP-CD
		//                    WHEN CF-MC-UNDERWRITER
		//                           THRU 9910-EXIT
		//                    WHEN CF-MC-FLD-PRODUCTION-UW
		//                           THRU 9915-EXIT
		//                    WHEN OTHER
		//           * These are the only underwriting positions stored in client
		//           * anymore, so everything else will be left as spaces.
		//                        CONTINUE
		//                END-EVALUATE.
		if (Conditions.eq(lFrameworkResponseArea.getMua02XrfTypCd(), ws.getConstantFields().getMarketingCode().getUnderwriter())) {
			// COB_CODE: PERFORM 9910-FILL-UND-FIELDS
			//              THRU 9910-EXIT
			fillUndFields();
		} else if (Conditions.eq(lFrameworkResponseArea.getMua02XrfTypCd(), ws.getConstantFields().getMarketingCode().getFldProductionUw())) {
			// COB_CODE: PERFORM 9915-FILL-FPU-FIELDS
			//              THRU 9915-EXIT
			fillFpuFields();
		} else {
			// These are the only underwriting positions stored in client
			// anymore, so everything else will be left as spaces.
			// COB_CODE: CONTINUE
			//continue
		}
	}

	/**Original name: 9910-FILL-UND-FIELDS<br>*/
	private void fillUndFields() {
		// COB_CODE: MOVE MUA02-CICR-XRF-ID      TO MUT04O-UW-CLT-ID.
		lServiceContractArea.setMut04oUwCltId(lFrameworkResponseArea.getMua02CicrXrfId());
		// COB_CODE: MOVE MUA02-CICR-FST-NM      TO MUT04O-UW-FST-NM.
		lServiceContractArea.setMut04oUwFstNm(lFrameworkResponseArea.getMua02CicrFstNm());
		// COB_CODE: MOVE MUA02-CICR-MDL-NM      TO MUT04O-UW-MDL-NM.
		lServiceContractArea.setMut04oUwMdlNm(lFrameworkResponseArea.getMua02CicrMdlNm());
		// COB_CODE: MOVE MUA02-CICR-LST-NM      TO MUT04O-UW-LST-NM.
		lServiceContractArea.setMut04oUwLstNm(lFrameworkResponseArea.getMua02CicrLstNm());
		// COB_CODE: MOVE MUA02-NM-SFX           TO MUT04O-UW-SFX.
		lServiceContractArea.setMut04oUwSfx(lFrameworkResponseArea.getMua02NmSfx());
		// COB_CODE: MOVE MUA02-NM-PFX           TO MUT04O-UW-PFX.
		lServiceContractArea.setMut04oUwPfx(lFrameworkResponseArea.getMua02NmPfx());
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-UW-DSY-NM.
		lServiceContractArea.setMut04oUwDsyNm(ws.getSaveArea().getDisplayName());
	}

	/**Original name: 9915-FILL-FPU-FIELDS<br>*/
	private void fillFpuFields() {
		// COB_CODE: MOVE MUA02-CICR-XRF-ID      TO MUT04O-FPU-CLT-ID.
		lServiceContractArea.setMut04oFpuCltId(lFrameworkResponseArea.getMua02CicrXrfId());
		// COB_CODE: MOVE MUA02-CICR-FST-NM      TO MUT04O-FPU-FST-NM.
		lServiceContractArea.setMut04oFpuFstNm(lFrameworkResponseArea.getMua02CicrFstNm());
		// COB_CODE: MOVE MUA02-CICR-MDL-NM      TO MUT04O-FPU-MDL-NM.
		lServiceContractArea.setMut04oFpuMdlNm(lFrameworkResponseArea.getMua02CicrMdlNm());
		// COB_CODE: MOVE MUA02-CICR-LST-NM      TO MUT04O-FPU-LST-NM.
		lServiceContractArea.setMut04oFpuLstNm(lFrameworkResponseArea.getMua02CicrLstNm());
		// COB_CODE: MOVE MUA02-NM-SFX           TO MUT04O-FPU-SFX.
		lServiceContractArea.setMut04oFpuSfx(lFrameworkResponseArea.getMua02NmSfx());
		// COB_CODE: MOVE MUA02-NM-PFX           TO MUT04O-FPU-PFX.
		lServiceContractArea.setMut04oFpuPfx(lFrameworkResponseArea.getMua02NmPfx());
		// COB_CODE: MOVE SA-DISPLAY-NAME        TO MUT04O-FPU-DSY-NM.
		lServiceContractArea.setMut04oFpuDsyNm(ws.getSaveArea().getDisplayName());
	}

	/**Original name: 9A00-FORMAT-DSY-NAME<br>*/
	private void a00FormatDsyName() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE SA-DISPLAY-NAME.
		ws.getSaveArea().setDisplayName("");
		// COB_CODE: IF SA-FST-NM = SPACES
		//               MOVE CF-DSP-DELIMITER   TO SA-DD-FST-SPR
		//           ELSE
		//                                       TO SA-DD-FST-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getFstNm())) {
			// COB_CODE: MOVE CF-DSP-DELIMITER   TO SA-DD-FST-SPR
			ws.getSaveArea().getDspDelimiters().setFstSpr(String.valueOf(ws.getConstantFields().getDspDelimiter()));
		} else {
			// COB_CODE: MOVE CF-SPACE-DSP-DELIMITER
			//                                   TO SA-DD-FST-SPR
			ws.getSaveArea().getDspDelimiters().setFstSpr(ws.getConstantFields().getSpaceDspDelimiter());
		}
		// COB_CODE: IF SA-MDL-NM = SPACES
		//               MOVE CF-DSP-DELIMITER   TO SA-DD-MDL-SPR
		//           ELSE
		//                                       TO SA-DD-MDL-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getMdlNm())) {
			// COB_CODE: MOVE CF-DSP-DELIMITER   TO SA-DD-MDL-SPR
			ws.getSaveArea().getDspDelimiters().setMdlSpr(String.valueOf(ws.getConstantFields().getDspDelimiter()));
		} else {
			// COB_CODE: MOVE CF-SPACE-DSP-DELIMITER
			//                                   TO SA-DD-MDL-SPR
			ws.getSaveArea().getDspDelimiters().setMdlSpr(ws.getConstantFields().getSpaceDspDelimiter());
		}
		// COB_CODE: IF SA-LST-NM = SPACES
		//               MOVE CF-DSP-DELIMITER   TO SA-DD-LST-SPR
		//           ELSE
		//                                       TO SA-DD-LST-SPR
		//           END-IF
		if (Characters.EQ_SPACE.test(ws.getSaveArea().getLstNm())) {
			// COB_CODE: MOVE CF-DSP-DELIMITER   TO SA-DD-LST-SPR
			ws.getSaveArea().getDspDelimiters().setLstSpr(String.valueOf(ws.getConstantFields().getDspDelimiter()));
		} else {
			// COB_CODE: MOVE CF-SPACE-DSP-DELIMITER
			//                                   TO SA-DD-LST-SPR
			ws.getSaveArea().getDspDelimiters().setLstSpr(ws.getConstantFields().getSpaceDspDelimiter());
		}
		// COB_CODE: STRING SA-FST-NM            DELIMITED BY CF-NAME-DELIMITER
		//                  SA-DD-FST-SPR        DELIMITED BY CF-DSP-DELIMITER
		//                  SA-MDL-NM            DELIMITED BY CF-NAME-DELIMITER
		//                  SA-DD-MDL-SPR        DELIMITED BY CF-DSP-DELIMITER
		//                  SA-LST-NM            DELIMITED BY CF-NAME-DELIMITER
		//                  SA-DD-LST-SPR        DELIMITED BY CF-DSP-DELIMITER
		//                  SA-SFX               DELIMITED BY CF-NAME-DELIMITER
		//               INTO SA-DISPLAY-NAME
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(SaveAreaMu0x0004.Len.DISPLAY_NAME,
				new String[] { Functions.substringBefore(ws.getSaveArea().getFstNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getDspDelimiters().getFstSprFormatted(), ws.getConstantFields().getDspDelimiter()),
						Functions.substringBefore(ws.getSaveArea().getMdlNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getDspDelimiters().getMdlSprFormatted(), ws.getConstantFields().getDspDelimiter()),
						Functions.substringBefore(ws.getSaveArea().getLstNmFormatted(), ws.getConstantFields().getNameDelimiterFormatted()),
						Functions.substringBefore(ws.getSaveArea().getDspDelimiters().getLstSprFormatted(), ws.getConstantFields().getDspDelimiter()),
						Functions.substringBefore(ws.getSaveArea().getSfxFormatted(), ws.getConstantFields().getNameDelimiterFormatted()) });
		ws.getSaveArea().setDisplayName(concatUtil.replaceInString(ws.getSaveArea().getDisplayNameFormatted()));
	}

	/**Original name: 9B00-SET-FED-ACT-INFO<br>*/
	private void b00SetFedActInfo() {
		// COB_CODE: MOVE CW40R-FED-ST-CD        TO MUT04O-AL-STATE-CD.
		lServiceContractArea.setMut04oAlStateCd(lFrameworkResponseArea.getCw40rFedStCd());
		// COB_CODE: MOVE CW40R-FED-COUNTY-CD    TO MUT04O-AL-COUNTY-CD.
		lServiceContractArea.setMut04oAlCountyCd(lFrameworkResponseArea.getCw40rFedCountyCd());
		// COB_CODE: MOVE CW40R-FED-TOWN-CD      TO MUT04O-AL-TOWN-CD.
		lServiceContractArea.setMut04oAlTownCd(lFrameworkResponseArea.getCw40rFedTownCd());
		// COB_CODE: MOVE CW40R-OFC-LOC-CD       TO MUT04O-OFC-LOC-CD.
		lServiceContractArea.setMut04oOfcLocCdFormatted(lFrameworkResponseArea.getCw40rOfcLocCdFormatted());
	}

	/**Original name: 9C00-SET-BUSINESS-CLT<br>*/
	private void c00SetBusinessClt() {
		// COB_CODE: MOVE CW01R-CIBC-NBR-EMP     TO MUT04O-NBR-EMPLOYEES.
		lServiceContractArea.setMut04oNbrEmployeesFormatted(Trunc.addPlusSign(lFrameworkResponseArea.getCw01rCibcNbrEmpFormatted()));
	}

	/**Original name: 9D00-SET-COWNER-INFO<br>
	 * <pre>** EJD START ***</pre>*/
	private void d00SetCownerInfo() {
		// COB_CODE: IF CWORC-CLIENT-ID OF L-FRAMEWORK-RESPONSE-AREA = SPACES
		//               GO TO 9D00-EXIT
		//           END-IF
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getCworcClientId())) {
			// COB_CODE: GO TO 9D00-EXIT
			return;
		}
		// COB_CODE: MOVE CWORC-CLIENT-ID OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MUT04O-CI-CLIENT-ID.
		lServiceContractArea.setMut04oCiClientId(lFrameworkResponseArea.getCworcClientId());
		// COB_CODE: MOVE CF-NO                  TO MUT04O-CI-IDV-NM-IND.
		lServiceContractArea.setMut04oCiIdvNmInd(ws.getConstantFields().getNo());
		// COB_CODE: IF (CWORC-CIOR-LEG-ENT-CD OF L-FRAMEWORK-RESPONSE-AREA
		//                                     = CF-INDIVIDUAL)
		//               MOVE SA-SRT-NAME        TO MUT04O-CI-SR-NM
		//           END-IF.
		if (Conditions.eq(lFrameworkResponseArea.getCworcCiorLegEntCd(), ws.getConstantFields().getIndividual())) {
			// COB_CODE: MOVE CWORC-CIOR-FST-NM OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-FST-NM
			//                                      SA-FST-NM
			lServiceContractArea.setMut04oCiFstNm(lFrameworkResponseArea.getCworcCiorFstNm());
			ws.getSaveArea().setFstNm(lFrameworkResponseArea.getCworcCiorFstNm());
			// COB_CODE: MOVE CWORC-CIOR-MDL-NM OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-MDL-NM
			//                                      SA-MDL-NM
			lServiceContractArea.setMut04oCiMdlNm(lFrameworkResponseArea.getCworcCiorMdlNm());
			ws.getSaveArea().setMdlNm(lFrameworkResponseArea.getCworcCiorMdlNm());
			// COB_CODE: MOVE CWORC-CIOR-LST-NM OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-LST-NM
			//                                      SA-LST-NM
			lServiceContractArea.setMut04oCiLstNm(lFrameworkResponseArea.getCworcCiorLstNm());
			ws.getSaveArea().setLstNm(lFrameworkResponseArea.getCworcCiorLstNm());
			// COB_CODE: MOVE CWORC-CIOR-NM-PFX OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-PFX
			lServiceContractArea.setMut04oCiPfx(lFrameworkResponseArea.getCworcCiorNmPfx());
			// COB_CODE: MOVE CWORC-CIOR-NM-SFX OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-SFX
			//                                      SA-SFX
			lServiceContractArea.setMut04oCiSfx(lFrameworkResponseArea.getCworcCiorNmSfx());
			ws.getSaveArea().setSfx(lFrameworkResponseArea.getCworcCiorNmSfx());
			// COB_CODE: MOVE CF-YES             TO MUT04O-CI-IDV-NM-IND
			lServiceContractArea.setMut04oCiIdvNmInd(ws.getConstantFields().getYes());
			// COB_CODE: PERFORM 9A00-FORMAT-DSY-NAME
			//              THRU 9A00-EXIT
			a00FormatDsyName();
			// COB_CODE: MOVE SA-DISPLAY-NAME    TO MUT04O-CI-DSY-NM
			lServiceContractArea.setMut04oCiDsyNm(ws.getSaveArea().getDisplayName());
			// COB_CODE: PERFORM 9140-FILL-IND-SRT-NAME
			//              THRU 9140-EXIT
			fillIndSrtName();
			// COB_CODE: MOVE SA-SRT-NAME        TO MUT04O-CI-SR-NM
			lServiceContractArea.setMut04oCiSrNm(ws.getSaveArea().getSrtName());
		}
		// COB_CODE: IF MUT04O-CI-IDV-NM-IND = CF-NO
		//                                       TO MUT04O-CI-SR-NM
		//           END-IF.
		if (lServiceContractArea.getMut04oCiIdvNmInd() == ws.getConstantFields().getNo()) {
			// COB_CODE: MOVE CWORC-CIOR-LST-NM OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-DSY-NM
			lServiceContractArea.setMut04oCiDsyNm(lFrameworkResponseArea.getCworcCiorLstNm());
			// COB_CODE: MOVE CWORC-CIOR-LST-NM OF L-FRAMEWORK-RESPONSE-AREA
			//                                   TO MUT04O-CI-SR-NM
			lServiceContractArea.setMut04oCiSrNm(lFrameworkResponseArea.getCworcCiorLstNm());
		}
	}

	/**Original name: RNG_9100-SET-CLT-NAME-ADDR-_-9100-EXIT<br>*/
	private void rng9100SetCltNameAddr() {
		String retcode = "";
		boolean goto9100A = false;
		setCltNameAddr();
		do {
			goto9100A = false;
			retcode = a();
		} while (retcode.equals("9100-A"));
	}

	/**Original name: RNG_9200-SET-CLT-PHONE-_-9200-EXIT<br>*/
	private void rng9200SetCltPhone() {
		String retcode = "";
		boolean goto9200A = false;
		setCltPhone();
		do {
			goto9200A = false;
			retcode = a1();
		} while (retcode.equals("9200-A"));
	}

	/**Original name: RNG_9400-SET-CLT-DEMO-INFO-_-9400-EXIT<br>*/
	private void rng9400SetCltDemoInfo() {
		String retcode = "";
		boolean goto9400A = false;
		setCltDemoInfo();
		do {
			goto9400A = false;
			retcode = a2();
		} while (retcode.equals("9400-A"));
	}

	/**Original name: RNG_9500-SET-CLT-ACCT-INFO-_-9500-EXIT<br>*/
	private void rng9500SetCltAcctInfo() {
		String retcode = "";
		boolean goto9500A = false;
		setCltAcctInfo();
		do {
			goto9500A = false;
			retcode = a3();
		} while (retcode.equals("9500-A"));
	}

	/**Original name: RNG_9900-SET-CLT-UW-INFO-_-9900-EXIT<br>*/
	private void rng9900SetCltUwInfo() {
		String retcode = "";
		boolean goto9900A = false;
		setCltUwInfo();
		do {
			goto9900A = false;
			retcode = a4();
		} while (retcode.equals("9900-A"));
	}

	public void initDsdErrorHandlingParms() {
		ws.getMainDriverData().getDriverSpecificData().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getMainDriverData().getDriverSpecificData().setFatalErrorMessage("");
		ws.getMainDriverData().getDriverSpecificData().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getMainDriverData().getDriverSpecificData().getNonLoggableErrors(idx0).setDsdNonLogErrMsg("");
		}
		ws.getMainDriverData().getDriverSpecificData().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.WARNINGS_MAXOCCURS; idx0++) {
			ws.getMainDriverData().getDriverSpecificData().getWarnings(idx0).setDsdWarnMsg("");
		}
	}

	public void initPpcErrorHandlingParms() {
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initMemoryAllocationParms() {
		ws.getMainDriverData().getCommunicationShellCommon().setMaInputDataSize(0);
		ws.getMainDriverData().getCommunicationShellCommon().setMaOutputDataSize(0);
		ws.getMainDriverData().getCommunicationShellCommon().getMaReturnCode().setMaReturnCodeFormatted("0000");
		ws.getMainDriverData().getCommunicationShellCommon().setMaErrorMessage("");
	}

	public void initLFrameworkRequestArea() {
		lFrameworkRequestArea.setCwgikqTchObjectKey("");
		lFrameworkRequestArea.setCwgikqRltTypCd("");
		lFrameworkRequestArea.setCwgikqObjCd("");
		lFrameworkRequestArea.setCwgikqShwObjKey("");
		lFrameworkRequestArea.setCwgikqObjSysId("");
		lFrameworkRequestArea.setCwgikqPhnNbr("");
		lFrameworkRequestArea.setCwgikqPhnTypCd("");
		lFrameworkRequestArea.setCwgikqEmailAdrTxt("");
		lFrameworkRequestArea.setCwgikqEmailTypeCd("");
		lFrameworkRequestArea.setCwgikqTaxTypeCd("");
		lFrameworkRequestArea.setCwgikqCitxTaxId("");
		lFrameworkRequestArea.setCwgikqPolNbr("");
		lFrameworkRequestArea.setCwgikqEffDt("");
		lFrameworkRequestArea.setCwgikqExpDt("");
		lFrameworkRequestArea.setCw02qClientTabChkSumFormatted("000000000");
		lFrameworkRequestArea.setCw02qClientIdKcre("");
		lFrameworkRequestArea.setCw02qTransProcessDt("");
		lFrameworkRequestArea.setCw02qClientIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qClientId("");
		lFrameworkRequestArea.setCw02qHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHistoryVldNbrFormatted("00000");
		lFrameworkRequestArea.setCw02qCiclEffDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclEffDt("");
		lFrameworkRequestArea.setCw02qCiclPriSubCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclPriSubCd("");
		lFrameworkRequestArea.setCw02qCiclFstNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclFstNm("");
		lFrameworkRequestArea.setCw02qCiclLstNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclLstNm("");
		lFrameworkRequestArea.setCw02qCiclMdlNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclMdlNm("");
		lFrameworkRequestArea.setCw02qNmPfxCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qNmPfx("");
		lFrameworkRequestArea.setCw02qNmSfxCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qNmSfx("");
		lFrameworkRequestArea.setCw02qPrimaryProDsnCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qPrimaryProDsnCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qPrimaryProDsnCd("");
		lFrameworkRequestArea.setCw02qLegEntCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qLegEntCd("");
		lFrameworkRequestArea.setCw02qCiclSdxCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclSdxCd("");
		lFrameworkRequestArea.setCw02qCiclOgnInceptDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclOgnInceptDtNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclOgnInceptDt("");
		lFrameworkRequestArea.setCw02qCiclAddNmIndCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclAddNmIndNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclAddNmInd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclDobDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclDobDt("");
		lFrameworkRequestArea.setCw02qCiclBirStCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclBirStCd("");
		lFrameworkRequestArea.setCw02qGenderCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qGenderCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qPriLggCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qPriLggCd("");
		lFrameworkRequestArea.setCw02qUserIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qUserId("");
		lFrameworkRequestArea.setCw02qStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qTerminalId("");
		lFrameworkRequestArea.setCw02qCiclExpDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclExpDt("");
		lFrameworkRequestArea.setCw02qCiclEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclEffAcyTs("");
		lFrameworkRequestArea.setCw02qCiclExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclExpAcyTs("");
		lFrameworkRequestArea.setCw02qStatutoryTleCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qStatutoryTleCd("");
		lFrameworkRequestArea.setCw02qCiclLngNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclLngNmNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclLngNm("");
		lFrameworkRequestArea.setCw02qLoLstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qLoLstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qLoLstNmMchCd("");
		lFrameworkRequestArea.setCw02qHiLstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHiLstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHiLstNmMchCd("");
		lFrameworkRequestArea.setCw02qLoFstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qLoFstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qLoFstNmMchCd("");
		lFrameworkRequestArea.setCw02qHiFstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHiFstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qHiFstNmMchCd("");
		lFrameworkRequestArea.setCw02qCiclAcqSrcCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclAcqSrcCdNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw02qCiclAcqSrcCd("");
		lFrameworkRequestArea.setCw02qLegEntDesc("");
		lFrameworkRequestArea.setCwcacqClientAddrCompChksumFormatted("000000000");
		lFrameworkRequestArea.setCwcacqAdrIdKcre("");
		lFrameworkRequestArea.setCwcacqClientIdKcre("");
		lFrameworkRequestArea.setCwcacqAdrSeqNbrKcre("");
		lFrameworkRequestArea.setCwcacqTchObjectKeyKcre("");
		lFrameworkRequestArea.setCwcacqAdrIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAdrId("");
		lFrameworkRequestArea.setCwcacqClientIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqClientId("");
		lFrameworkRequestArea.setCwcacqHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqHistoryVldNbrFormatted("00000");
		lFrameworkRequestArea.setCwcacqAdrSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAdrSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAdrSeqNbrFormatted("00000");
		lFrameworkRequestArea.setCwcacqCiarEffDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarEffDt("");
		lFrameworkRequestArea.setCwcacqTransProcessDt("");
		lFrameworkRequestArea.setCwcacqUserIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqUserId("");
		lFrameworkRequestArea.setCwcacqTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqTerminalId("");
		lFrameworkRequestArea.setCwcacqCicaAdr1Ci(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaAdr1("");
		lFrameworkRequestArea.setCwcacqCicaAdr2Ci(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaAdr2Ni(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaAdr2("");
		lFrameworkRequestArea.setCwcacqCicaCitNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaCitNm("");
		lFrameworkRequestArea.setCwcacqCicaCtyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaCtyNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaCty("");
		lFrameworkRequestArea.setCwcacqStCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqStCd("");
		lFrameworkRequestArea.setCwcacqCicaPstCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaPstCd("");
		lFrameworkRequestArea.setCwcacqCtrCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCtrCd("");
		lFrameworkRequestArea.setCwcacqCicaAddAdrIndCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaAddAdrIndNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaAddAdrInd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAddrStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAddrStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAddNmIndCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAddNmInd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAdrTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqAdrTypCd("");
		lFrameworkRequestArea.setCwcacqTchObjectKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqTchObjectKey("");
		lFrameworkRequestArea.setCwcacqCiarExpDtCi("");
		lFrameworkRequestArea.setCwcacqCiarExpDt("");
		lFrameworkRequestArea.setCwcacqCiarSerAdr1TxtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarSerAdr1Txt("");
		lFrameworkRequestArea.setCwcacqCiarSerCitNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarSerCitNm("");
		lFrameworkRequestArea.setCwcacqCiarSerStCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarSerStCd("");
		lFrameworkRequestArea.setCwcacqCiarSerPstCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarSerPstCd("");
		lFrameworkRequestArea.setCwcacqRelStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqRelStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqEffAcyTs("");
		lFrameworkRequestArea.setCwcacqCiarExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCiarExpAcyTs("");
		lFrameworkRequestArea.setCwcacqMoreRowsSw(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaShwObjKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCwcacqCicaShwObjKey("");
		lFrameworkRequestArea.setCwcacqBusObjNm("");
		lFrameworkRequestArea.setCwcacqStDesc("");
		lFrameworkRequestArea.setCwcacqAdrTypDesc("");
		lFrameworkRequestArea.setCw06qCltCltRelationCsumFormatted("000000000");
		lFrameworkRequestArea.setCw06qClientIdKcre("");
		lFrameworkRequestArea.setCw06qCltTypCdKcre("");
		lFrameworkRequestArea.setCw06qHistoryVldNbrKcre("");
		lFrameworkRequestArea.setCw06qCicrXrfIdKcre("");
		lFrameworkRequestArea.setCw06qXrfTypCdKcre("");
		lFrameworkRequestArea.setCw06qCicrEffDtKcre("");
		lFrameworkRequestArea.setCw06qTransProcessDt("");
		lFrameworkRequestArea.setCw06qClientIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qClientId("");
		lFrameworkRequestArea.setCw06qCltTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCltTypCd("");
		lFrameworkRequestArea.setCw06qHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qHistoryVldNbrSignedFormatted("000000");
		lFrameworkRequestArea.setCw06qCicrXrfIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrXrfId("");
		lFrameworkRequestArea.setCw06qXrfTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qXrfTypCd("");
		lFrameworkRequestArea.setCw06qCicrEffDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrEffDt("");
		lFrameworkRequestArea.setCw06qCicrExpDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrExpDt("");
		lFrameworkRequestArea.setCw06qCicrNbrOprStrsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrNbrOprStrsNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrNbrOprStrs("");
		lFrameworkRequestArea.setCw06qUserIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qUserId("");
		lFrameworkRequestArea.setCw06qStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qTerminalId("");
		lFrameworkRequestArea.setCw06qCicrEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrEffAcyTs("");
		lFrameworkRequestArea.setCw06qCicrExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw06qCicrExpAcyTs("");
		lFrameworkRequestArea.setCw08qCltObjRelationCsumFormatted("000000000");
		lFrameworkRequestArea.setCw08qTchObjectKeyKcre("");
		lFrameworkRequestArea.setCw08qHistoryVldNbrKcre("");
		lFrameworkRequestArea.setCw08qCiorEffDtKcre("");
		lFrameworkRequestArea.setCw08qObjSysIdKcre("");
		lFrameworkRequestArea.setCw08qCiorObjSeqNbrKcre("");
		lFrameworkRequestArea.setCw08qClientIdKcre("");
		lFrameworkRequestArea.setCw08qTransProcessDt("");
		lFrameworkRequestArea.setCw08qTchObjectKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qTchObjectKey("");
		lFrameworkRequestArea.setCw08qHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qHistoryVldNbrFormatted("00000");
		lFrameworkRequestArea.setCw08qCiorEffDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorEffDt("");
		lFrameworkRequestArea.setCw08qObjSysIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qObjSysId("");
		lFrameworkRequestArea.setCw08qCiorObjSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorObjSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorObjSeqNbrFormatted("00000");
		lFrameworkRequestArea.setCw08qClientIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qClientId("");
		lFrameworkRequestArea.setCw08qRltTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qRltTypCd("");
		lFrameworkRequestArea.setCw08qObjCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qObjCd("");
		lFrameworkRequestArea.setCw08qCiorShwObjKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorShwObjKey("");
		lFrameworkRequestArea.setCw08qAdrSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qAdrSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qAdrSeqNbrFormatted("00000");
		lFrameworkRequestArea.setCw08qUserIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qUserId("");
		lFrameworkRequestArea.setCw08qStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qTerminalId("");
		lFrameworkRequestArea.setCw08qCiorExpDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorExpDt("");
		lFrameworkRequestArea.setCw08qCiorEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorEffAcyTs("");
		lFrameworkRequestArea.setCw08qCiorExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qCiorExpAcyTs("");
		lFrameworkRequestArea.setCw08qAppType(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCw08qBusObjNm("");
		lFrameworkRequestArea.setCw08qObjDesc("");
		lFrameworkRequestArea.setCworcCltObjRelationCsumFormatted("000000000");
		lFrameworkRequestArea.setCworcTchObjectKeyKcre("");
		lFrameworkRequestArea.setCworcHistoryVldNbrKcre("");
		lFrameworkRequestArea.setCworcCiorEffDtKcre("");
		lFrameworkRequestArea.setCworcObjSysIdKcre("");
		lFrameworkRequestArea.setCworcCiorObjSeqNbrKcre("");
		lFrameworkRequestArea.setCworcClientIdKcre("");
		lFrameworkRequestArea.setCworcTransProcessDt("");
		lFrameworkRequestArea.setCworcTchObjectKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcTchObjectKey("");
		lFrameworkRequestArea.setCworcHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcHistoryVldNbrFormatted("00000");
		lFrameworkRequestArea.setCworcCiorEffDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorEffDt("");
		lFrameworkRequestArea.setCworcObjSysIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcObjSysId("");
		lFrameworkRequestArea.setCworcCiorObjSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorObjSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorObjSeqNbrFormatted("00000");
		lFrameworkRequestArea.setCworcClientIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcClientId("");
		lFrameworkRequestArea.setCworcRltTypCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcRltTypCd("");
		lFrameworkRequestArea.setCworcObjCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcObjCd("");
		lFrameworkRequestArea.setCworcCiorShwObjKeyCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorShwObjKey("");
		lFrameworkRequestArea.setCworcAdrSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcAdrSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcAdrSeqNbrFormatted("00000");
		lFrameworkRequestArea.setCworcUserIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcUserId("");
		lFrameworkRequestArea.setCworcStatusCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcStatusCd(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcTerminalId("");
		lFrameworkRequestArea.setCworcCiorExpDtCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorExpDt("");
		lFrameworkRequestArea.setCworcCiorEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorEffAcyTs("");
		lFrameworkRequestArea.setCworcCiorExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorExpAcyTs("");
		lFrameworkRequestArea.setCworcCiorLegEntCdCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorLegEntCd("");
		lFrameworkRequestArea.setCworcCiorFstNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorFstNm("");
		lFrameworkRequestArea.setCworcCiorLstNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorLstNm("");
		lFrameworkRequestArea.setCworcCiorMdlNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorMdlNm("");
		lFrameworkRequestArea.setCworcCiorNmPfxCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorNmPfx("");
		lFrameworkRequestArea.setCworcCiorNmSfxCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorNmSfx("");
		lFrameworkRequestArea.setCworcCiorLngNmCi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorLngNmNi(Types.SPACE_CHAR);
		lFrameworkRequestArea.setCworcCiorLngNm("");
		lFrameworkRequestArea.setCworcFilterType("");
	}

	public void initLFrameworkResponseArea() {
		lFrameworkResponseArea.setCw02rClientTabChkSumFormatted("000000000");
		lFrameworkResponseArea.setCw02rClientIdKcre("");
		lFrameworkResponseArea.setCw02rTransProcessDt("");
		lFrameworkResponseArea.setCw02rClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rClientId("");
		lFrameworkResponseArea.setCw02rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCw02rCiclEffDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclEffDt("");
		lFrameworkResponseArea.setCw02rCiclPriSubCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclPriSubCd("");
		lFrameworkResponseArea.setCw02rCiclFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclFstNm("");
		lFrameworkResponseArea.setCw02rCiclLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclLstNm("");
		lFrameworkResponseArea.setCw02rCiclMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclMdlNm("");
		lFrameworkResponseArea.setCw02rNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rNmPfx("");
		lFrameworkResponseArea.setCw02rNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rNmSfx("");
		lFrameworkResponseArea.setCw02rPrimaryProDsnCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rPrimaryProDsnCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rPrimaryProDsnCd("");
		lFrameworkResponseArea.setCw02rLegEntCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rLegEntCd("");
		lFrameworkResponseArea.setCw02rCiclSdxCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclSdxCd("");
		lFrameworkResponseArea.setCw02rCiclOgnInceptDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclOgnInceptDtNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclOgnInceptDt("");
		lFrameworkResponseArea.setCw02rCiclAddNmIndCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclAddNmIndNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclAddNmInd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclDobDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclDobDt("");
		lFrameworkResponseArea.setCw02rCiclBirStCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclBirStCd("");
		lFrameworkResponseArea.setCw02rGenderCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rGenderCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rPriLggCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rPriLggCd("");
		lFrameworkResponseArea.setCw02rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rUserId("");
		lFrameworkResponseArea.setCw02rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rTerminalId("");
		lFrameworkResponseArea.setCw02rCiclExpDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclExpDt("");
		lFrameworkResponseArea.setCw02rCiclEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclEffAcyTs("");
		lFrameworkResponseArea.setCw02rCiclExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclExpAcyTs("");
		lFrameworkResponseArea.setCw02rStatutoryTleCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rStatutoryTleCd("");
		lFrameworkResponseArea.setCw02rCiclLngNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclLngNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclLngNm("");
		lFrameworkResponseArea.setCw02rLoLstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rLoLstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rLoLstNmMchCd("");
		lFrameworkResponseArea.setCw02rHiLstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHiLstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHiLstNmMchCd("");
		lFrameworkResponseArea.setCw02rLoFstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rLoFstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rLoFstNmMchCd("");
		lFrameworkResponseArea.setCw02rHiFstNmMchCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHiFstNmMchCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rHiFstNmMchCd("");
		lFrameworkResponseArea.setCw02rCiclAcqSrcCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclAcqSrcCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw02rCiclAcqSrcCd("");
		lFrameworkResponseArea.setCw02rLegEntDesc("");
		for (int idx0 = 1; idx0 <= LFrameworkResponseArea.L_FW_RESP_CLT_ADR_COMPOSITE_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setCwcacrClientAddrCompChksumFormatted(idx0, "000000000");
			lFrameworkResponseArea.setCwcacrAdrIdKcre(idx0, "");
			lFrameworkResponseArea.setCwcacrClientIdKcre(idx0, "");
			lFrameworkResponseArea.setCwcacrAdrSeqNbrKcre(idx0, "");
			lFrameworkResponseArea.setCwcacrTchObjectKeyKcre(idx0, "");
			lFrameworkResponseArea.setCwcacrAdrIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAdrId(idx0, "");
			lFrameworkResponseArea.setCwcacrClientIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrClientId(idx0, "");
			lFrameworkResponseArea.setCwcacrHistoryVldNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrHistoryVldNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrHistoryVldNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCwcacrAdrSeqNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAdrSeqNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAdrSeqNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCwcacrCiarEffDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarEffDt(idx0, "");
			lFrameworkResponseArea.setCwcacrTransProcessDt(idx0, "");
			lFrameworkResponseArea.setCwcacrUserIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrUserId(idx0, "");
			lFrameworkResponseArea.setCwcacrTerminalIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrTerminalId(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaAdr1Ci(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaAdr1(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaAdr2Ci(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaAdr2Ni(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaAdr2(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaCitNmCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaCitNm(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaCtyCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaCtyNi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaCty(idx0, "");
			lFrameworkResponseArea.setCwcacrStCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrStCd(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaPstCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaPstCd(idx0, "");
			lFrameworkResponseArea.setCwcacrCtrCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCtrCd(idx0, "");
			lFrameworkResponseArea.setCwcacrCicaAddAdrIndCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaAddAdrIndNi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaAddAdrInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAddrStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAddrStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAddNmIndCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAddNmInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAdrTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrAdrTypCd(idx0, "");
			lFrameworkResponseArea.setCwcacrTchObjectKeyCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrTchObjectKey(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarExpDtCi(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarExpDt(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarSerAdr1TxtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarSerAdr1Txt(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarSerCitNmCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarSerCitNm(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarSerStCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarSerStCd(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarSerPstCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarSerPstCd(idx0, "");
			lFrameworkResponseArea.setCwcacrRelStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrRelStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrEffAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrEffAcyTs(idx0, "");
			lFrameworkResponseArea.setCwcacrCiarExpAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCiarExpAcyTs(idx0, "");
			lFrameworkResponseArea.setCwcacrMoreRowsSw(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaShwObjKeyCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCwcacrCicaShwObjKey(idx0, "");
			lFrameworkResponseArea.setCwcacrBusObjNm(idx0, "");
			lFrameworkResponseArea.setCwcacrStDesc(idx0, "");
			lFrameworkResponseArea.setCwcacrAdrTypDesc(idx0, "");
		}
		lFrameworkResponseArea.setCwemrClientEmailCsumFormatted("000000000");
		lFrameworkResponseArea.setCwemrClientIdKcre("");
		lFrameworkResponseArea.setCwemrCiemSeqNbrKcre("");
		lFrameworkResponseArea.setCwemrHistoryVldNbrKcre("");
		lFrameworkResponseArea.setCwemrEffectiveDtKcre("");
		lFrameworkResponseArea.setCwemrTransProcessDt("");
		lFrameworkResponseArea.setCwemrClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrClientId("");
		lFrameworkResponseArea.setCwemrCiemSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrCiemSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrCiemSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCwemrHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCwemrEffectiveDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrEffectiveDt("");
		lFrameworkResponseArea.setCwemrEmailTypeCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrEmailTypeCd("");
		lFrameworkResponseArea.setCwemrUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrUserId("");
		lFrameworkResponseArea.setCwemrStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrTerminalId("");
		lFrameworkResponseArea.setCwemrExpirationDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrExpirationDt("");
		lFrameworkResponseArea.setCwemrEffectiveAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrEffectiveAcyTs("");
		lFrameworkResponseArea.setCwemrExpirationAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrExpirationAcyTsNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrExpirationAcyTs("");
		lFrameworkResponseArea.setCwemrCiemEmailAdrTxtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrCiemEmailAdrTxt("");
		lFrameworkResponseArea.setCwemrMoreRowsSw(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCwemrEmailTypeCdDesc("");
		for (int idx0 = 1; idx0 <= LFrameworkResponseArea.L_FW_RESP_CLIENT_PHONE_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setCw04rClientPhoneChkSumFormatted(idx0, "000000000");
			lFrameworkResponseArea.setCw04rClientIdKcre(idx0, "");
			lFrameworkResponseArea.setCw04rCiphPhnSeqNbrKcre(idx0, "");
			lFrameworkResponseArea.setCw04rTransProcessDt(idx0, "");
			lFrameworkResponseArea.setCw04rClientIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rClientId(idx0, "");
			lFrameworkResponseArea.setCw04rCiphPhnSeqNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphPhnSeqNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphPhnSeqNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw04rHistoryVldNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rHistoryVldNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rHistoryVldNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw04rCiphEffDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphEffDt(idx0, "");
			lFrameworkResponseArea.setCw04rCiphPhnNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphPhnNbr(idx0, "");
			lFrameworkResponseArea.setCw04rPhnTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rPhnTypCd(idx0, "");
			lFrameworkResponseArea.setCw04rCiphXrfIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphXrfIdNi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphXrfId(idx0, "");
			lFrameworkResponseArea.setCw04rUserIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rUserId(idx0, "");
			lFrameworkResponseArea.setCw04rStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rTerminalIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rTerminalId(idx0, "");
			lFrameworkResponseArea.setCw04rCiphExpDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphExpDt(idx0, "");
			lFrameworkResponseArea.setCw04rCiphEffAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphEffAcyTs(idx0, "");
			lFrameworkResponseArea.setCw04rCiphExpAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rCiphExpAcyTs(idx0, "");
			lFrameworkResponseArea.setCw04rMoreRowsSw(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw04rPhnTypDesc(idx0, "");
		}
		for (int idx0 = 1; idx0 <= LFrameworkResponseArea.L_FW_RESP_CLT_OBJ_RELATION_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setCw08rCltObjRelationCsumFormatted(idx0, "000000000");
			lFrameworkResponseArea.setCw08rTchObjectKeyKcre(idx0, "");
			lFrameworkResponseArea.setCw08rHistoryVldNbrKcre(idx0, "");
			lFrameworkResponseArea.setCw08rCiorEffDtKcre(idx0, "");
			lFrameworkResponseArea.setCw08rObjSysIdKcre(idx0, "");
			lFrameworkResponseArea.setCw08rCiorObjSeqNbrKcre(idx0, "");
			lFrameworkResponseArea.setCw08rClientIdKcre(idx0, "");
			lFrameworkResponseArea.setCw08rTransProcessDt(idx0, "");
			lFrameworkResponseArea.setCw08rTchObjectKeyCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rTchObjectKey(idx0, "");
			lFrameworkResponseArea.setCw08rHistoryVldNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rHistoryVldNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rHistoryVldNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw08rCiorEffDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorEffDt(idx0, "");
			lFrameworkResponseArea.setCw08rObjSysIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rObjSysId(idx0, "");
			lFrameworkResponseArea.setCw08rCiorObjSeqNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorObjSeqNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorObjSeqNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw08rClientIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rClientId(idx0, "");
			lFrameworkResponseArea.setCw08rRltTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rRltTypCd(idx0, "");
			lFrameworkResponseArea.setCw08rObjCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rObjCd(idx0, "");
			lFrameworkResponseArea.setCw08rCiorShwObjKeyCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorShwObjKey(idx0, "");
			lFrameworkResponseArea.setCw08rAdrSeqNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rAdrSeqNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rAdrSeqNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw08rUserIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rUserId(idx0, "");
			lFrameworkResponseArea.setCw08rStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rTerminalIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rTerminalId(idx0, "");
			lFrameworkResponseArea.setCw08rCiorExpDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorExpDt(idx0, "");
			lFrameworkResponseArea.setCw08rCiorEffAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorEffAcyTs(idx0, "");
			lFrameworkResponseArea.setCw08rCiorExpAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rCiorExpAcyTs(idx0, "");
			lFrameworkResponseArea.setCw08rAppType(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw08rBusObjNm(idx0, "");
			lFrameworkResponseArea.setCw08rObjDesc(idx0, "");
		}
		lFrameworkResponseArea.setCw11rCltRefRelationCsumFormatted("000000000");
		lFrameworkResponseArea.setCw11rClientIdKcre("");
		lFrameworkResponseArea.setCw11rCirfRefSeqNbrKcre("");
		lFrameworkResponseArea.setCw11rHistoryVldNbrKcre("");
		lFrameworkResponseArea.setCw11rCirfEffDtKcre("");
		lFrameworkResponseArea.setCw11rTransProcessDt("");
		lFrameworkResponseArea.setCw11rClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rClientId("");
		lFrameworkResponseArea.setCw11rCirfRefSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfRefSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfRefSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCw11rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCw11rCirfEffDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfEffDt("");
		lFrameworkResponseArea.setCw11rCirfRefIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfRefId("");
		lFrameworkResponseArea.setCw11rRefTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rRefTypCd("");
		lFrameworkResponseArea.setCw11rCirfExpDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfExpDt("");
		lFrameworkResponseArea.setCw11rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rUserId("");
		lFrameworkResponseArea.setCw11rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rTerminalId("");
		lFrameworkResponseArea.setCw11rCirfEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfEffAcyTs("");
		lFrameworkResponseArea.setCw11rCirfExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw11rCirfExpAcyTs("");
		lFrameworkResponseArea.setCw11rMoreRowsSw(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rClientTaxChkSumFormatted("000000000");
		lFrameworkResponseArea.setCw27rClientIdKcre("");
		lFrameworkResponseArea.setCw27rCitxTaxSeqNbrKcre("");
		lFrameworkResponseArea.setCw27rTransProcessDt("");
		lFrameworkResponseArea.setCw27rClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rClientId("");
		lFrameworkResponseArea.setCw27rCitxTaxSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCw27rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCw27rEffectiveDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rEffectiveDt("");
		lFrameworkResponseArea.setCw27rCitxTaxIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxId("");
		lFrameworkResponseArea.setCw27rTaxTypeCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rTaxTypeCd("");
		lFrameworkResponseArea.setCw27rCitxTaxStCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxStCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxStCd("");
		lFrameworkResponseArea.setCw27rCitxTaxCtrCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxCtrCdNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rCitxTaxCtrCd("");
		lFrameworkResponseArea.setCw27rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rUserId("");
		lFrameworkResponseArea.setCw27rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rTerminalId("");
		lFrameworkResponseArea.setCw27rExpirationDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rExpirationDt("");
		lFrameworkResponseArea.setCw27rEffectiveAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rEffectiveAcyTs("");
		lFrameworkResponseArea.setCw27rExpirationAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rExpirationAcyTsNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rExpirationAcyTs("");
		lFrameworkResponseArea.setCw27rMoreRowsSw(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw27rTaxTypeDesc("");
		lFrameworkResponseArea.setCw45rCltMutTerStcCsumFormatted("000000000");
		lFrameworkResponseArea.setCw45rTerCltIdKcre("");
		lFrameworkResponseArea.setCw45rTerLevelCdKcre("");
		lFrameworkResponseArea.setCw45rTerNbrKcre("");
		lFrameworkResponseArea.setCw45rTransProcessDt("");
		lFrameworkResponseArea.setCw45rTerCltId("");
		lFrameworkResponseArea.setCw45rTerLevelCd("");
		lFrameworkResponseArea.setCw45rTerNbr("");
		lFrameworkResponseArea.setCw45rTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rTerLevelCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrTerNbr("");
		lFrameworkResponseArea.setCw45rMrTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrTerCltId("");
		lFrameworkResponseArea.setCw45rMrNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrNmPfx("");
		lFrameworkResponseArea.setCw45rMrFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrFstNm("");
		lFrameworkResponseArea.setCw45rMrMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrMdlNm("");
		lFrameworkResponseArea.setCw45rMrLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrLstNm("");
		lFrameworkResponseArea.setCw45rMrNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrNm("");
		lFrameworkResponseArea.setCw45rMrNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrNmSfx("");
		lFrameworkResponseArea.setCw45rMrCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rMrCltId("");
		lFrameworkResponseArea.setCw45rCsrTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrTerNbr("");
		lFrameworkResponseArea.setCw45rCsrTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrTerCltId("");
		lFrameworkResponseArea.setCw45rCsrNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrNmPfx("");
		lFrameworkResponseArea.setCw45rCsrFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrFstNm("");
		lFrameworkResponseArea.setCw45rCsrMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrMdlNm("");
		lFrameworkResponseArea.setCw45rCsrLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrLstNm("");
		lFrameworkResponseArea.setCw45rCsrNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrNm("");
		lFrameworkResponseArea.setCw45rCsrNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrNmSfx("");
		lFrameworkResponseArea.setCw45rCsrCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rCsrCltId("");
		lFrameworkResponseArea.setCw45rSmrTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrTerNbr("");
		lFrameworkResponseArea.setCw45rSmrTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrTerCltId("");
		lFrameworkResponseArea.setCw45rSmrNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrNmPfx("");
		lFrameworkResponseArea.setCw45rSmrFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrFstNm("");
		lFrameworkResponseArea.setCw45rSmrMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrMdlNm("");
		lFrameworkResponseArea.setCw45rSmrLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrLstNm("");
		lFrameworkResponseArea.setCw45rSmrNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrNm("");
		lFrameworkResponseArea.setCw45rSmrNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrNmSfx("");
		lFrameworkResponseArea.setCw45rSmrCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rSmrCltId("");
		lFrameworkResponseArea.setCw45rDmmTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmTerNbr("");
		lFrameworkResponseArea.setCw45rDmmTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmTerCltId("");
		lFrameworkResponseArea.setCw45rDmmNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmNmPfx("");
		lFrameworkResponseArea.setCw45rDmmFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmFstNm("");
		lFrameworkResponseArea.setCw45rDmmMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmMdlNm("");
		lFrameworkResponseArea.setCw45rDmmLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmLstNm("");
		lFrameworkResponseArea.setCw45rDmmNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmNm("");
		lFrameworkResponseArea.setCw45rDmmNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmNmSfx("");
		lFrameworkResponseArea.setCw45rDmmCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDmmCltId("");
		lFrameworkResponseArea.setCw45rRmmTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmTerNbr("");
		lFrameworkResponseArea.setCw45rRmmTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmTerCltId("");
		lFrameworkResponseArea.setCw45rRmmNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmNmPfx("");
		lFrameworkResponseArea.setCw45rRmmFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmFstNm("");
		lFrameworkResponseArea.setCw45rRmmMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmMdlNm("");
		lFrameworkResponseArea.setCw45rRmmLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmLstNm("");
		lFrameworkResponseArea.setCw45rRmmNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmNm("");
		lFrameworkResponseArea.setCw45rRmmNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmNmSfx("");
		lFrameworkResponseArea.setCw45rRmmCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rRmmCltId("");
		lFrameworkResponseArea.setCw45rDfoTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoTerNbr("");
		lFrameworkResponseArea.setCw45rDfoTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoTerCltId("");
		lFrameworkResponseArea.setCw45rDfoNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoNmPfx("");
		lFrameworkResponseArea.setCw45rDfoFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoFstNm("");
		lFrameworkResponseArea.setCw45rDfoMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoMdlNm("");
		lFrameworkResponseArea.setCw45rDfoLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoLstNm("");
		lFrameworkResponseArea.setCw45rDfoNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoNm("");
		lFrameworkResponseArea.setCw45rDfoNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoNmSfx("");
		lFrameworkResponseArea.setCw45rDfoCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw45rDfoCltId("");
		lFrameworkResponseArea.setCw46rCltAgcTerStcCsumFormatted("000000000");
		lFrameworkResponseArea.setCw46rTerCltIdKcre("");
		lFrameworkResponseArea.setCw46rTerLevelCdKcre("");
		lFrameworkResponseArea.setCw46rTransProcessDt("");
		lFrameworkResponseArea.setCw46rTerCltId("");
		lFrameworkResponseArea.setCw46rTerLevelCd("");
		lFrameworkResponseArea.setCw46rTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rTerLevelCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rTerNbr("");
		lFrameworkResponseArea.setCw46rBrnTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rBrnTerNbr("");
		lFrameworkResponseArea.setCw46rBrnTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rBrnTerCltId("");
		lFrameworkResponseArea.setCw46rBrnNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rBrnNm("");
		lFrameworkResponseArea.setCw46rBrnCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rBrnCltId("");
		lFrameworkResponseArea.setCw46rAgcTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAgcTerNbr("");
		lFrameworkResponseArea.setCw46rAgcTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAgcTerCltId("");
		lFrameworkResponseArea.setCw46rAgcNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAgcNm("");
		lFrameworkResponseArea.setCw46rAgcCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAgcCltId("");
		lFrameworkResponseArea.setCw46rSmrTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rSmrTerNbr("");
		lFrameworkResponseArea.setCw46rSmrTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rSmrTerCltId("");
		lFrameworkResponseArea.setCw46rSmrNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rSmrNm("");
		lFrameworkResponseArea.setCw46rSmrCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rSmrCltId("");
		lFrameworkResponseArea.setCw46rAmmTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmTerNbr("");
		lFrameworkResponseArea.setCw46rAmmTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmTerCltId("");
		lFrameworkResponseArea.setCw46rAmmNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmNmPfx("");
		lFrameworkResponseArea.setCw46rAmmFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmFstNm("");
		lFrameworkResponseArea.setCw46rAmmMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmMdlNm("");
		lFrameworkResponseArea.setCw46rAmmLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmLstNm("");
		lFrameworkResponseArea.setCw46rAmmNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmNm("");
		lFrameworkResponseArea.setCw46rAmmNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmNmSfx("");
		lFrameworkResponseArea.setCw46rAmmCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAmmCltId("");
		lFrameworkResponseArea.setCw46rAfmTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmTerNbr("");
		lFrameworkResponseArea.setCw46rAfmTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmTerCltId("");
		lFrameworkResponseArea.setCw46rAfmNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmNmPfx("");
		lFrameworkResponseArea.setCw46rAfmFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmFstNm("");
		lFrameworkResponseArea.setCw46rAfmMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmMdlNm("");
		lFrameworkResponseArea.setCw46rAfmLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmLstNm("");
		lFrameworkResponseArea.setCw46rAfmNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmNm("");
		lFrameworkResponseArea.setCw46rAfmNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmNmSfx("");
		lFrameworkResponseArea.setCw46rAfmCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAfmCltId("");
		lFrameworkResponseArea.setCw46rAvpTerNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpTerNbr("");
		lFrameworkResponseArea.setCw46rAvpTerCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpTerCltId("");
		lFrameworkResponseArea.setCw46rAvpNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpNmPfx("");
		lFrameworkResponseArea.setCw46rAvpFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpFstNm("");
		lFrameworkResponseArea.setCw46rAvpMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpMdlNm("");
		lFrameworkResponseArea.setCw46rAvpLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpLstNm("");
		lFrameworkResponseArea.setCw46rAvpNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpNm("");
		lFrameworkResponseArea.setCw46rAvpNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpNmSfx("");
		lFrameworkResponseArea.setCw46rAvpCltIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw46rAvpCltId("");
		lFrameworkResponseArea.setMua02CltCltRelationCsumFormatted("000000000");
		lFrameworkResponseArea.setMua02ClientIdKcre("");
		lFrameworkResponseArea.setMua02CltTypCdKcre("");
		lFrameworkResponseArea.setMua02HistoryVldNbrKcre("");
		lFrameworkResponseArea.setMua02CicrXrfIdKcre("");
		lFrameworkResponseArea.setMua02XrfTypCdKcre("");
		lFrameworkResponseArea.setMua02CicrEffDtKcre("");
		lFrameworkResponseArea.setMua02TransProcessDt("");
		lFrameworkResponseArea.setMua02ClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02ClientId("");
		lFrameworkResponseArea.setMua02CltTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CltTypCd("");
		lFrameworkResponseArea.setMua02HistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02HistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02HistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setMua02CicrXrfIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrXrfId("");
		lFrameworkResponseArea.setMua02XrfTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02XrfTypCd("");
		lFrameworkResponseArea.setMua02CicrEffDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrEffDt("");
		lFrameworkResponseArea.setMua02CicrExpDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrExpDt("");
		lFrameworkResponseArea.setMua02CicrNbrOprStrsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrNbrOprStrsNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrNbrOprStrs("");
		lFrameworkResponseArea.setMua02UserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02UserId("");
		lFrameworkResponseArea.setMua02StatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02StatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02TerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02TerminalId("");
		lFrameworkResponseArea.setMua02CicrEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrEffAcyTs("");
		lFrameworkResponseArea.setMua02CicrExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrExpAcyTs("");
		lFrameworkResponseArea.setMua02CicrLegEntCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrLegEntCd("");
		lFrameworkResponseArea.setMua02CicrFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrFstNm("");
		lFrameworkResponseArea.setMua02CicrLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrLstNm("");
		lFrameworkResponseArea.setMua02CicrMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrMdlNm("");
		lFrameworkResponseArea.setMua02NmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02NmPfx("");
		lFrameworkResponseArea.setMua02NmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02NmSfx("");
		lFrameworkResponseArea.setMua02CicrLngNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrLngNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02CicrLngNm("");
		lFrameworkResponseArea.setMua02CicrRelChgSw(Types.SPACE_CHAR);
		lFrameworkResponseArea.setMua02ClientDes("");
		for (int idx0 = 1; idx0 <= LFrameworkResponseArea.L_FW_RESP_FED_BUSINESS_TYP_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setCw48rFedBusinessTypCsumFormatted(idx0, "000000000");
			lFrameworkResponseArea.setCw48rClientIdKcre(idx0, "");
			lFrameworkResponseArea.setCw48rTobCdKcre(idx0, "");
			lFrameworkResponseArea.setCw48rHistoryVldNbrKcre(idx0, "");
			lFrameworkResponseArea.setCw48rEffectiveDtKcre(idx0, "");
			lFrameworkResponseArea.setCw48rTransProcessDt(idx0, "");
			lFrameworkResponseArea.setCw48rClientId(idx0, "");
			lFrameworkResponseArea.setCw48rTobCd(idx0, "");
			lFrameworkResponseArea.setCw48rHistoryVldNbrSign(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rHistoryVldNbrFormatted(idx0, "00000");
			lFrameworkResponseArea.setCw48rEffectiveDt(idx0, "");
			lFrameworkResponseArea.setCw48rClientIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTobCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rHistoryVldNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rEffectiveDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTobSicTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTobSicTypCd(idx0, "");
			lFrameworkResponseArea.setCw48rTobPtyCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTobPtyCd(idx0, "");
			lFrameworkResponseArea.setCw48rUserIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rUserId(idx0, "");
			lFrameworkResponseArea.setCw48rStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTerminalIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rTerminalId(idx0, "");
			lFrameworkResponseArea.setCw48rExpirationDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rExpirationDt(idx0, "");
			lFrameworkResponseArea.setCw48rEffectiveAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rEffectiveAcyTs(idx0, "");
			lFrameworkResponseArea.setCw48rExpirationAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rExpirationAcyTsNi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw48rExpirationAcyTs(idx0, "");
			lFrameworkResponseArea.setCw48rTobCdDesc(idx0, "");
		}
		lFrameworkResponseArea.setCw49rFedSegInfoCsumFormatted("000000000");
		lFrameworkResponseArea.setCw49rClientIdKcre("");
		lFrameworkResponseArea.setCw49rHistoryVldNbrKcre("");
		lFrameworkResponseArea.setCw49rEffectiveDtKcre("");
		lFrameworkResponseArea.setCw49rTransProcessDt("");
		lFrameworkResponseArea.setCw49rClientId("");
		lFrameworkResponseArea.setCw49rHistoryVldNbrSignedFormatted("000000");
		lFrameworkResponseArea.setCw49rEffectiveDt("");
		lFrameworkResponseArea.setCw49rClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rEffectiveDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rSegCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rSegCd("");
		lFrameworkResponseArea.setCw49rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rUserId("");
		lFrameworkResponseArea.setCw49rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rTerminalId("");
		lFrameworkResponseArea.setCw49rExpirationDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rExpirationDt("");
		lFrameworkResponseArea.setCw49rEffectiveAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rEffectiveAcyTs("");
		lFrameworkResponseArea.setCw49rExpirationAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rExpirationAcyTsNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw49rExpirationAcyTs("");
		lFrameworkResponseArea.setCw49rSegDes("");
		lFrameworkResponseArea.setCw40rFedAccountInfoCsumFormatted("000000000");
		lFrameworkResponseArea.setCw40rCiaiActTchKeyKcre("");
		lFrameworkResponseArea.setCw40rHistoryVldNbrKcre("");
		lFrameworkResponseArea.setCw40rEffectiveDtKcre("");
		lFrameworkResponseArea.setCw40rTransProcessDt("");
		lFrameworkResponseArea.setCw40rCiaiActTchKey("");
		lFrameworkResponseArea.setCw40rHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCw40rEffectiveDt("");
		lFrameworkResponseArea.setCw40rCiaiActTchKeyCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rEffectiveDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rOfcLocCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rOfcLocCd("");
		lFrameworkResponseArea.setCw40rCiaiAuthnticLossCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rCiaiAuthnticLoss(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rFedStCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rFedStCd("");
		lFrameworkResponseArea.setCw40rFedCountyCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rFedCountyCd("");
		lFrameworkResponseArea.setCw40rFedTownCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rFedTownCd("");
		lFrameworkResponseArea.setCw40rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rUserId("");
		lFrameworkResponseArea.setCw40rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rTerminalId("");
		lFrameworkResponseArea.setCw40rExpirationDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rExpirationDt("");
		lFrameworkResponseArea.setCw40rEffectiveAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rEffectiveAcyTs("");
		lFrameworkResponseArea.setCw40rExpirationAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rExpirationAcyTsNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw40rExpirationAcyTs("");
		lFrameworkResponseArea.setCw01rBusinessClientChkSumFormatted("000000000");
		lFrameworkResponseArea.setCw01rClientIdKcre("");
		lFrameworkResponseArea.setCw01rCibcBusSeqNbrKcre("");
		lFrameworkResponseArea.setCw01rTransProcessDt("");
		lFrameworkResponseArea.setCw01rClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rClientId("");
		lFrameworkResponseArea.setCw01rHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCw01rCibcBusSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcBusSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcBusSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCw01rCibcEffDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcEffDt("");
		lFrameworkResponseArea.setCw01rCibcExpDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcExpDt("");
		lFrameworkResponseArea.setCw01rGrsRevCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rGrsRevCd("");
		lFrameworkResponseArea.setCw01rIdyTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rIdyTypCd("");
		lFrameworkResponseArea.setCw01rCibcNbrEmpCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcNbrEmpNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcNbrEmpSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcNbrEmpFormatted("0000000000");
		lFrameworkResponseArea.setCw01rCibcStrDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcStrDtNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcStrDt("");
		lFrameworkResponseArea.setCw01rUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rUserId("");
		lFrameworkResponseArea.setCw01rStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rTerminalId("");
		lFrameworkResponseArea.setCw01rCibcEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcEffAcyTs("");
		lFrameworkResponseArea.setCw01rCibcExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCw01rCibcExpAcyTs("");
		lFrameworkResponseArea.setCworcCltObjRelationCsumFormatted("000000000");
		lFrameworkResponseArea.setCworcTchObjectKeyKcre("");
		lFrameworkResponseArea.setCworcHistoryVldNbrKcre("");
		lFrameworkResponseArea.setCworcCiorEffDtKcre("");
		lFrameworkResponseArea.setCworcObjSysIdKcre("");
		lFrameworkResponseArea.setCworcCiorObjSeqNbrKcre("");
		lFrameworkResponseArea.setCworcClientIdKcre("");
		lFrameworkResponseArea.setCworcTransProcessDt("");
		lFrameworkResponseArea.setCworcTchObjectKeyCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcTchObjectKey("");
		lFrameworkResponseArea.setCworcHistoryVldNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcHistoryVldNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcHistoryVldNbrFormatted("00000");
		lFrameworkResponseArea.setCworcCiorEffDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorEffDt("");
		lFrameworkResponseArea.setCworcObjSysIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcObjSysId("");
		lFrameworkResponseArea.setCworcCiorObjSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorObjSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorObjSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCworcClientIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcClientId("");
		lFrameworkResponseArea.setCworcRltTypCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcRltTypCd("");
		lFrameworkResponseArea.setCworcObjCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcObjCd("");
		lFrameworkResponseArea.setCworcCiorShwObjKeyCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorShwObjKey("");
		lFrameworkResponseArea.setCworcAdrSeqNbrCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcAdrSeqNbrSign(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcAdrSeqNbrFormatted("00000");
		lFrameworkResponseArea.setCworcUserIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcUserId("");
		lFrameworkResponseArea.setCworcStatusCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcStatusCd(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcTerminalIdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcTerminalId("");
		lFrameworkResponseArea.setCworcCiorExpDtCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorExpDt("");
		lFrameworkResponseArea.setCworcCiorEffAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorEffAcyTs("");
		lFrameworkResponseArea.setCworcCiorExpAcyTsCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorExpAcyTs("");
		lFrameworkResponseArea.setCworcCiorLegEntCdCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorLegEntCd("");
		lFrameworkResponseArea.setCworcCiorFstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorFstNm("");
		lFrameworkResponseArea.setCworcCiorLstNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorLstNm("");
		lFrameworkResponseArea.setCworcCiorMdlNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorMdlNm("");
		lFrameworkResponseArea.setCworcCiorNmPfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorNmPfx("");
		lFrameworkResponseArea.setCworcCiorNmSfxCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorNmSfx("");
		lFrameworkResponseArea.setCworcCiorLngNmCi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorLngNmNi(Types.SPACE_CHAR);
		lFrameworkResponseArea.setCworcCiorLngNm("");
		lFrameworkResponseArea.setCworcFilterType("");
		for (int idx0 = 1; idx0 <= LFrameworkResponseArea.L_FW_RESP_CLT_CLT_REL_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setCw06rCltCltRelationCsumFormatted(idx0, "000000000");
			lFrameworkResponseArea.setCw06rClientIdKcre(idx0, "");
			lFrameworkResponseArea.setCw06rCltTypCdKcre(idx0, "");
			lFrameworkResponseArea.setCw06rHistoryVldNbrKcre(idx0, "");
			lFrameworkResponseArea.setCw06rCicrXrfIdKcre(idx0, "");
			lFrameworkResponseArea.setCw06rXrfTypCdKcre(idx0, "");
			lFrameworkResponseArea.setCw06rCicrEffDtKcre(idx0, "");
			lFrameworkResponseArea.setCw06rTransProcessDt(idx0, "");
			lFrameworkResponseArea.setCw06rClientIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rClientId(idx0, "");
			lFrameworkResponseArea.setCw06rCltTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCltTypCd(idx0, "");
			lFrameworkResponseArea.setCw06rHistoryVldNbrCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rHistoryVldNbrSignedFormatted(idx0, "000000");
			lFrameworkResponseArea.setCw06rCicrXrfIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrXrfId(idx0, "");
			lFrameworkResponseArea.setCw06rXrfTypCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rXrfTypCd(idx0, "");
			lFrameworkResponseArea.setCw06rCicrEffDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrEffDt(idx0, "");
			lFrameworkResponseArea.setCw06rCicrExpDtCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrExpDt(idx0, "");
			lFrameworkResponseArea.setCw06rCicrNbrOprStrsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrNbrOprStrsNi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrNbrOprStrs(idx0, "");
			lFrameworkResponseArea.setCw06rUserIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rUserId(idx0, "");
			lFrameworkResponseArea.setCw06rStatusCdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rTerminalIdCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rTerminalId(idx0, "");
			lFrameworkResponseArea.setCw06rCicrEffAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrEffAcyTs(idx0, "");
			lFrameworkResponseArea.setCw06rCicrExpAcyTsCi(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setCw06rCicrExpAcyTs(idx0, "");
		}
	}

	public void initDfhcommarea() {
		dfhcommarea.setPpcServiceDataSize(0);
		dfhcommarea.setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		dfhcommarea.setPpcOperation("");
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initLServiceContractArea() {
		lServiceContractArea.setMut04iTkCltId("");
		lServiceContractArea.setMut04iTkCiorShwObjKey("");
		lServiceContractArea.setMut04iTkAdrSeqNbr(0);
		lServiceContractArea.setMut04iUsrId("");
		lServiceContractArea.setMut04iActNbr("");
		lServiceContractArea.setMut04iTerNbr("");
		lServiceContractArea.setMut04iAsOfDt("");
		lServiceContractArea.setMut04iFein("");
		lServiceContractArea.setMut04iSsn("");
		lServiceContractArea.setMut04iPhnAcd("");
		lServiceContractArea.setMut04iPhnPfxNbr("");
		lServiceContractArea.setMut04iPhnLinNbr("");
		lServiceContractArea.setMut04oTkCltId("");
		lServiceContractArea.setMut04oCnIdvNmInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oCnDsyNm("");
		lServiceContractArea.setMut04oCnSrNm("");
		lServiceContractArea.setMut04oCnFstNm("");
		lServiceContractArea.setMut04oCnMdlNm("");
		lServiceContractArea.setMut04oCnLstNm("");
		lServiceContractArea.setMut04oCnSfx("");
		lServiceContractArea.setMut04oCnPfx("");
		lServiceContractArea.setMut04oCiClientId("");
		lServiceContractArea.setMut04oCiIdvNmInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oCiDsyNm("");
		lServiceContractArea.setMut04oCiSrNm("");
		lServiceContractArea.setMut04oCiFstNm("");
		lServiceContractArea.setMut04oCiMdlNm("");
		lServiceContractArea.setMut04oCiLstNm("");
		lServiceContractArea.setMut04oCiSfx("");
		lServiceContractArea.setMut04oCiPfx("");
		lServiceContractArea.setMut04oBsmAdrSeqNbr(0);
		lServiceContractArea.setMut04oBsmAdrId("");
		lServiceContractArea.setMut04oBsmLin1Adr("");
		lServiceContractArea.setMut04oBsmLin2Adr("");
		lServiceContractArea.setMut04oBsmCit("");
		lServiceContractArea.setMut04oBsmStAbb("");
		lServiceContractArea.setMut04oBsmPstCd("");
		lServiceContractArea.setMut04oBsmCty("");
		lServiceContractArea.setMut04oBsmCtrCd("");
		lServiceContractArea.setMut04oBslAdrSeqNbr(0);
		lServiceContractArea.setMut04oBslAdrId("");
		lServiceContractArea.setMut04oBslLin1Adr("");
		lServiceContractArea.setMut04oBslLin2Adr("");
		lServiceContractArea.setMut04oBslCit("");
		lServiceContractArea.setMut04oBslStAbb("");
		lServiceContractArea.setMut04oBslPstCd("");
		lServiceContractArea.setMut04oBslCty("");
		lServiceContractArea.setMut04oBslCtrCd("");
		lServiceContractArea.setMut04oBusPhnAcd("");
		lServiceContractArea.setMut04oBusPhnPfxNbr("");
		lServiceContractArea.setMut04oBusPhnLinNbr("");
		lServiceContractArea.setMut04oBusPhnExtNbr("");
		lServiceContractArea.setMut04oBusPhnNbrFmt("");
		lServiceContractArea.setMut04oFaxPhnAcd("");
		lServiceContractArea.setMut04oFaxPhnPfxNbr("");
		lServiceContractArea.setMut04oFaxPhnLinNbr("");
		lServiceContractArea.setMut04oFaxPhnExtNbr("");
		lServiceContractArea.setMut04oFaxPhnNbrFmt("");
		lServiceContractArea.setMut04oCtcPhnAcd("");
		lServiceContractArea.setMut04oCtcPhnPfxNbr("");
		lServiceContractArea.setMut04oCtcPhnLinNbr("");
		lServiceContractArea.setMut04oCtcPhnExtNbr("");
		lServiceContractArea.setMut04oCtcPhnNbrFmt("");
		lServiceContractArea.setMut04oFein("");
		lServiceContractArea.setMut04oSsn("");
		lServiceContractArea.setMut04oFeinFmt("");
		lServiceContractArea.setMut04oSsnFmt("");
		lServiceContractArea.setMut04oLegEtyCd("");
		lServiceContractArea.setMut04oLegEtyDes("");
		lServiceContractArea.setMut04oTobCd("");
		lServiceContractArea.setMut04oTobDes("");
		lServiceContractArea.setMut04oSicCd("");
		lServiceContractArea.setMut04oSicDes("");
		lServiceContractArea.setMut04oSegCd("");
		lServiceContractArea.setMut04oSegDes("");
		lServiceContractArea.setMut04oPcPsptActInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oPcActNbr("");
		lServiceContractArea.setMut04oPcActNbr2("");
		lServiceContractArea.setMut04oPcActNbrFmt("");
		lServiceContractArea.setMut04oPcActNbr2Fmt("");
		lServiceContractArea.setMut04oPsptActNbr("");
		lServiceContractArea.setMut04oPsptActNbrFmt("");
		lServiceContractArea.setMut04oActNmId(0);
		lServiceContractArea.setMut04oAct2NmId(0);
		lServiceContractArea.setMut04oObjCd("");
		lServiceContractArea.setMut04oObjCd2("");
		lServiceContractArea.setMut04oObjDes("");
		lServiceContractArea.setMut04oObjDes2("");
		lServiceContractArea.setMut04oSinWcActNbr("");
		lServiceContractArea.setMut04oSinWcActNbrFmt("");
		lServiceContractArea.setMut04oSinWcActNmId(0);
		lServiceContractArea.setMut04oSinWcObjCd("");
		lServiceContractArea.setMut04oSinWcObjDes("");
		lServiceContractArea.setMut04oSplBillActNbr("");
		lServiceContractArea.setMut04oSplBillActNbrFmt("");
		lServiceContractArea.setMut04oSplBillActNmId(0);
		lServiceContractArea.setMut04oSplBillObjCd("");
		lServiceContractArea.setMut04oSplBillObjDes("");
		lServiceContractArea.setMut04oPrsLinActNbr("");
		lServiceContractArea.setMut04oPrsLinActNbrFmt("");
		lServiceContractArea.setMut04oPrsLinActNmId(0);
		lServiceContractArea.setMut04oPrsLinObjCd("");
		lServiceContractArea.setMut04oPrsLinObjDes("");
		lServiceContractArea.setMut04oCstNbr("");
		lServiceContractArea.setMut04oMktCltId("");
		lServiceContractArea.setMut04oMktTerNbr("");
		lServiceContractArea.setMut04oMktSvcDsyNm("");
		lServiceContractArea.setMut04oMktTypeCd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oMktTypeDes("");
		lServiceContractArea.setMut04oMtMktCltId("");
		lServiceContractArea.setMut04oMtMktTerNbr("");
		lServiceContractArea.setMut04oMtMktDsyNm("");
		lServiceContractArea.setMut04oMtMktFstNm("");
		lServiceContractArea.setMut04oMtMktMdlNm("");
		lServiceContractArea.setMut04oMtMktLstNm("");
		lServiceContractArea.setMut04oMtMktSfx("");
		lServiceContractArea.setMut04oMtMktPfx("");
		lServiceContractArea.setMut04oMtCsrCltId("");
		lServiceContractArea.setMut04oMtCsrTerNbr("");
		lServiceContractArea.setMut04oMtCsrDsyNm("");
		lServiceContractArea.setMut04oMtCsrFstNm("");
		lServiceContractArea.setMut04oMtCsrMdlNm("");
		lServiceContractArea.setMut04oMtCsrLstNm("");
		lServiceContractArea.setMut04oMtCsrSfx("");
		lServiceContractArea.setMut04oMtCsrPfx("");
		lServiceContractArea.setMut04oMtSmrCltId("");
		lServiceContractArea.setMut04oMtSmrTerNbr("");
		lServiceContractArea.setMut04oMtSmrDsyNm("");
		lServiceContractArea.setMut04oMtSmrFstNm("");
		lServiceContractArea.setMut04oMtSmrMdlNm("");
		lServiceContractArea.setMut04oMtSmrLstNm("");
		lServiceContractArea.setMut04oMtSmrSfx("");
		lServiceContractArea.setMut04oMtSmrPfx("");
		lServiceContractArea.setMut04oMtDmmCltId("");
		lServiceContractArea.setMut04oMtDmmTerNbr("");
		lServiceContractArea.setMut04oMtDmmDsyNm("");
		lServiceContractArea.setMut04oMtDmmFstNm("");
		lServiceContractArea.setMut04oMtDmmMdlNm("");
		lServiceContractArea.setMut04oMtDmmLstNm("");
		lServiceContractArea.setMut04oMtDmmSfx("");
		lServiceContractArea.setMut04oMtDmmPfx("");
		lServiceContractArea.setMut04oMtRmmCltId("");
		lServiceContractArea.setMut04oMtRmmTerNbr("");
		lServiceContractArea.setMut04oMtRmmDsyNm("");
		lServiceContractArea.setMut04oMtRmmFstNm("");
		lServiceContractArea.setMut04oMtRmmMdlNm("");
		lServiceContractArea.setMut04oMtRmmLstNm("");
		lServiceContractArea.setMut04oMtRmmSfx("");
		lServiceContractArea.setMut04oMtRmmPfx("");
		lServiceContractArea.setMut04oMtDfoCltId("");
		lServiceContractArea.setMut04oMtDfoTerNbr("");
		lServiceContractArea.setMut04oMtDfoDsyNm("");
		lServiceContractArea.setMut04oMtDfoFstNm("");
		lServiceContractArea.setMut04oMtDfoMdlNm("");
		lServiceContractArea.setMut04oMtDfoLstNm("");
		lServiceContractArea.setMut04oMtDfoSfx("");
		lServiceContractArea.setMut04oMtDfoPfx("");
		lServiceContractArea.setMut04oAtBrnCltId("");
		lServiceContractArea.setMut04oAtBrnTerNbr("");
		lServiceContractArea.setMut04oAtBrnNm("");
		lServiceContractArea.setMut04oAtAgcCltId("");
		lServiceContractArea.setMut04oAtAgcTerNbr("");
		lServiceContractArea.setMut04oAtAgcNm("");
		lServiceContractArea.setMut04oAtSmrCltId("");
		lServiceContractArea.setMut04oAtSmrTerNbr("");
		lServiceContractArea.setMut04oAtSmrNm("");
		lServiceContractArea.setMut04oAtAmmCltId("");
		lServiceContractArea.setMut04oAtAmmTerNbr("");
		lServiceContractArea.setMut04oAtAmmDsyNm("");
		lServiceContractArea.setMut04oAtAmmFstNm("");
		lServiceContractArea.setMut04oAtAmmMdlNm("");
		lServiceContractArea.setMut04oAtAmmLstNm("");
		lServiceContractArea.setMut04oAtAmmSfx("");
		lServiceContractArea.setMut04oAtAmmPfx("");
		lServiceContractArea.setMut04oAtAfmCltId("");
		lServiceContractArea.setMut04oAtAfmTerNbr("");
		lServiceContractArea.setMut04oAtAfmDsyNm("");
		lServiceContractArea.setMut04oAtAfmFstNm("");
		lServiceContractArea.setMut04oAtAfmMdlNm("");
		lServiceContractArea.setMut04oAtAfmLstNm("");
		lServiceContractArea.setMut04oAtAfmSfx("");
		lServiceContractArea.setMut04oAtAfmPfx("");
		lServiceContractArea.setMut04oAtAvpCltId("");
		lServiceContractArea.setMut04oAtAvpTerNbr("");
		lServiceContractArea.setMut04oAtAvpDsyNm("");
		lServiceContractArea.setMut04oAtAvpFstNm("");
		lServiceContractArea.setMut04oAtAvpMdlNm("");
		lServiceContractArea.setMut04oAtAvpLstNm("");
		lServiceContractArea.setMut04oAtAvpSfx("");
		lServiceContractArea.setMut04oAtAvpPfx("");
		lServiceContractArea.setMut04oSvcUwDsyNm("");
		lServiceContractArea.setMut04oSvcUwTypeCd("");
		lServiceContractArea.setMut04oSvcUwTypeDes("");
		lServiceContractArea.setMut04oUwCltId("");
		lServiceContractArea.setMut04oUwDsyNm("");
		lServiceContractArea.setMut04oUwFstNm("");
		lServiceContractArea.setMut04oUwMdlNm("");
		lServiceContractArea.setMut04oUwLstNm("");
		lServiceContractArea.setMut04oUwSfx("");
		lServiceContractArea.setMut04oUwPfx("");
		lServiceContractArea.setMut04oRskAlsCltId("");
		lServiceContractArea.setMut04oRskAlsDsyNm("");
		lServiceContractArea.setMut04oRskAlsFstNm("");
		lServiceContractArea.setMut04oRskAlsMdlNm("");
		lServiceContractArea.setMut04oRskAlsLstNm("");
		lServiceContractArea.setMut04oRskAlsSfx("");
		lServiceContractArea.setMut04oRskAlsPfx("");
		lServiceContractArea.setMut04oDumCltId("");
		lServiceContractArea.setMut04oDumDsyNm("");
		lServiceContractArea.setMut04oDumTer("");
		lServiceContractArea.setMut04oDumFstNm("");
		lServiceContractArea.setMut04oDumMdlNm("");
		lServiceContractArea.setMut04oDumLstNm("");
		lServiceContractArea.setMut04oDumSfx("");
		lServiceContractArea.setMut04oDumPfx("");
		lServiceContractArea.setMut04oRumCltId("");
		lServiceContractArea.setMut04oRumDsyNm("");
		lServiceContractArea.setMut04oRumTer("");
		lServiceContractArea.setMut04oRumFstNm("");
		lServiceContractArea.setMut04oRumMdlNm("");
		lServiceContractArea.setMut04oRumLstNm("");
		lServiceContractArea.setMut04oRumSfx("");
		lServiceContractArea.setMut04oRumPfx("");
		lServiceContractArea.setMut04oFpuCltId("");
		lServiceContractArea.setMut04oFpuDsyNm("");
		lServiceContractArea.setMut04oFpuFstNm("");
		lServiceContractArea.setMut04oFpuMdlNm("");
		lServiceContractArea.setMut04oFpuLstNm("");
		lServiceContractArea.setMut04oFpuSfx("");
		lServiceContractArea.setMut04oFpuPfx("");
		lServiceContractArea.setMut04oFpuDumCltId("");
		lServiceContractArea.setMut04oFpuDumDsyNm("");
		lServiceContractArea.setMut04oFpuDumTer("");
		lServiceContractArea.setMut04oFpuDumFstNm("");
		lServiceContractArea.setMut04oFpuDumMdlNm("");
		lServiceContractArea.setMut04oFpuDumLstNm("");
		lServiceContractArea.setMut04oFpuDumSfx("");
		lServiceContractArea.setMut04oFpuDumPfx("");
		lServiceContractArea.setMut04oFpuRumCltId("");
		lServiceContractArea.setMut04oFpuRumDsyNm("");
		lServiceContractArea.setMut04oFpuRumTer("");
		lServiceContractArea.setMut04oFpuRumFstNm("");
		lServiceContractArea.setMut04oFpuRumMdlNm("");
		lServiceContractArea.setMut04oFpuRumLstNm("");
		lServiceContractArea.setMut04oFpuRumSfx("");
		lServiceContractArea.setMut04oFpuRumPfx("");
		lServiceContractArea.setMut04oLpRskAlsCltId("");
		lServiceContractArea.setMut04oLpRskAlsDsyNm("");
		lServiceContractArea.setMut04oLpRskAlsFstNm("");
		lServiceContractArea.setMut04oLpRskAlsMdlNm("");
		lServiceContractArea.setMut04oLpRskAlsLstNm("");
		lServiceContractArea.setMut04oLpRskAlsSfx("");
		lServiceContractArea.setMut04oLpRskAlsPfx("");
		lServiceContractArea.setMut04oLpraDumCltId("");
		lServiceContractArea.setMut04oLpraDumDsyNm("");
		lServiceContractArea.setMut04oLpraDumTer("");
		lServiceContractArea.setMut04oLpraDumFstNm("");
		lServiceContractArea.setMut04oLpraDumMdlNm("");
		lServiceContractArea.setMut04oLpraDumLstNm("");
		lServiceContractArea.setMut04oLpraDumSfx("");
		lServiceContractArea.setMut04oLpraDumPfx("");
		lServiceContractArea.setMut04oLpraRumCltId("");
		lServiceContractArea.setMut04oLpraRumDsyNm("");
		lServiceContractArea.setMut04oLpraRumTer("");
		lServiceContractArea.setMut04oLpraRumFstNm("");
		lServiceContractArea.setMut04oLpraRumMdlNm("");
		lServiceContractArea.setMut04oLpraRumLstNm("");
		lServiceContractArea.setMut04oLpraRumSfx("");
		lServiceContractArea.setMut04oLpraRumPfx("");
		lServiceContractArea.setMut04oAlStateCd("");
		lServiceContractArea.setMut04oAlCountyCd("");
		lServiceContractArea.setMut04oAlTownCd("");
		lServiceContractArea.setMut04oNbrEmployees(0);
		for (int idx0 = 1; idx0 <= LServiceContractArea.MUT04O_SE_SIC_MAXOCCURS; idx0++) {
			lServiceContractArea.setMut04oSeSicCd(idx0, "");
			lServiceContractArea.setMut04oSeSicDes(idx0, "");
		}
		lServiceContractArea.setMut04oOlDivision(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oOlSubDivision(Types.SPACE_CHAR);
	}

	public void initMut004ServiceOutputs() {
		lServiceContractArea.setMut04oTkCltId("");
		lServiceContractArea.setMut04oCnIdvNmInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oCnDsyNm("");
		lServiceContractArea.setMut04oCnSrNm("");
		lServiceContractArea.setMut04oCnFstNm("");
		lServiceContractArea.setMut04oCnMdlNm("");
		lServiceContractArea.setMut04oCnLstNm("");
		lServiceContractArea.setMut04oCnSfx("");
		lServiceContractArea.setMut04oCnPfx("");
		lServiceContractArea.setMut04oCiClientId("");
		lServiceContractArea.setMut04oCiIdvNmInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oCiDsyNm("");
		lServiceContractArea.setMut04oCiSrNm("");
		lServiceContractArea.setMut04oCiFstNm("");
		lServiceContractArea.setMut04oCiMdlNm("");
		lServiceContractArea.setMut04oCiLstNm("");
		lServiceContractArea.setMut04oCiSfx("");
		lServiceContractArea.setMut04oCiPfx("");
		lServiceContractArea.setMut04oBsmAdrSeqNbr(0);
		lServiceContractArea.setMut04oBsmAdrId("");
		lServiceContractArea.setMut04oBsmLin1Adr("");
		lServiceContractArea.setMut04oBsmLin2Adr("");
		lServiceContractArea.setMut04oBsmCit("");
		lServiceContractArea.setMut04oBsmStAbb("");
		lServiceContractArea.setMut04oBsmPstCd("");
		lServiceContractArea.setMut04oBsmCty("");
		lServiceContractArea.setMut04oBsmCtrCd("");
		lServiceContractArea.setMut04oBslAdrSeqNbr(0);
		lServiceContractArea.setMut04oBslAdrId("");
		lServiceContractArea.setMut04oBslLin1Adr("");
		lServiceContractArea.setMut04oBslLin2Adr("");
		lServiceContractArea.setMut04oBslCit("");
		lServiceContractArea.setMut04oBslStAbb("");
		lServiceContractArea.setMut04oBslPstCd("");
		lServiceContractArea.setMut04oBslCty("");
		lServiceContractArea.setMut04oBslCtrCd("");
		lServiceContractArea.setMut04oBusPhnAcd("");
		lServiceContractArea.setMut04oBusPhnPfxNbr("");
		lServiceContractArea.setMut04oBusPhnLinNbr("");
		lServiceContractArea.setMut04oBusPhnExtNbr("");
		lServiceContractArea.setMut04oBusPhnNbrFmt("");
		lServiceContractArea.setMut04oFaxPhnAcd("");
		lServiceContractArea.setMut04oFaxPhnPfxNbr("");
		lServiceContractArea.setMut04oFaxPhnLinNbr("");
		lServiceContractArea.setMut04oFaxPhnExtNbr("");
		lServiceContractArea.setMut04oFaxPhnNbrFmt("");
		lServiceContractArea.setMut04oCtcPhnAcd("");
		lServiceContractArea.setMut04oCtcPhnPfxNbr("");
		lServiceContractArea.setMut04oCtcPhnLinNbr("");
		lServiceContractArea.setMut04oCtcPhnExtNbr("");
		lServiceContractArea.setMut04oCtcPhnNbrFmt("");
		lServiceContractArea.setMut04oFein("");
		lServiceContractArea.setMut04oSsn("");
		lServiceContractArea.setMut04oFeinFmt("");
		lServiceContractArea.setMut04oSsnFmt("");
		lServiceContractArea.setMut04oLegEtyCd("");
		lServiceContractArea.setMut04oLegEtyDes("");
		lServiceContractArea.setMut04oTobCd("");
		lServiceContractArea.setMut04oTobDes("");
		lServiceContractArea.setMut04oSicCd("");
		lServiceContractArea.setMut04oSicDes("");
		lServiceContractArea.setMut04oSegCd("");
		lServiceContractArea.setMut04oSegDes("");
		lServiceContractArea.setMut04oPcPsptActInd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oPcActNbr("");
		lServiceContractArea.setMut04oPcActNbr2("");
		lServiceContractArea.setMut04oPcActNbrFmt("");
		lServiceContractArea.setMut04oPcActNbr2Fmt("");
		lServiceContractArea.setMut04oPsptActNbr("");
		lServiceContractArea.setMut04oPsptActNbrFmt("");
		lServiceContractArea.setMut04oActNmId(0);
		lServiceContractArea.setMut04oAct2NmId(0);
		lServiceContractArea.setMut04oObjCd("");
		lServiceContractArea.setMut04oObjCd2("");
		lServiceContractArea.setMut04oObjDes("");
		lServiceContractArea.setMut04oObjDes2("");
		lServiceContractArea.setMut04oSinWcActNbr("");
		lServiceContractArea.setMut04oSinWcActNbrFmt("");
		lServiceContractArea.setMut04oSinWcActNmId(0);
		lServiceContractArea.setMut04oSinWcObjCd("");
		lServiceContractArea.setMut04oSinWcObjDes("");
		lServiceContractArea.setMut04oSplBillActNbr("");
		lServiceContractArea.setMut04oSplBillActNbrFmt("");
		lServiceContractArea.setMut04oSplBillActNmId(0);
		lServiceContractArea.setMut04oSplBillObjCd("");
		lServiceContractArea.setMut04oSplBillObjDes("");
		lServiceContractArea.setMut04oPrsLinActNbr("");
		lServiceContractArea.setMut04oPrsLinActNbrFmt("");
		lServiceContractArea.setMut04oPrsLinActNmId(0);
		lServiceContractArea.setMut04oPrsLinObjCd("");
		lServiceContractArea.setMut04oPrsLinObjDes("");
		lServiceContractArea.setMut04oCstNbr("");
		lServiceContractArea.setMut04oMktCltId("");
		lServiceContractArea.setMut04oMktTerNbr("");
		lServiceContractArea.setMut04oMktSvcDsyNm("");
		lServiceContractArea.setMut04oMktTypeCd(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oMktTypeDes("");
		lServiceContractArea.setMut04oMtMktCltId("");
		lServiceContractArea.setMut04oMtMktTerNbr("");
		lServiceContractArea.setMut04oMtMktDsyNm("");
		lServiceContractArea.setMut04oMtMktFstNm("");
		lServiceContractArea.setMut04oMtMktMdlNm("");
		lServiceContractArea.setMut04oMtMktLstNm("");
		lServiceContractArea.setMut04oMtMktSfx("");
		lServiceContractArea.setMut04oMtMktPfx("");
		lServiceContractArea.setMut04oMtCsrCltId("");
		lServiceContractArea.setMut04oMtCsrTerNbr("");
		lServiceContractArea.setMut04oMtCsrDsyNm("");
		lServiceContractArea.setMut04oMtCsrFstNm("");
		lServiceContractArea.setMut04oMtCsrMdlNm("");
		lServiceContractArea.setMut04oMtCsrLstNm("");
		lServiceContractArea.setMut04oMtCsrSfx("");
		lServiceContractArea.setMut04oMtCsrPfx("");
		lServiceContractArea.setMut04oMtSmrCltId("");
		lServiceContractArea.setMut04oMtSmrTerNbr("");
		lServiceContractArea.setMut04oMtSmrDsyNm("");
		lServiceContractArea.setMut04oMtSmrFstNm("");
		lServiceContractArea.setMut04oMtSmrMdlNm("");
		lServiceContractArea.setMut04oMtSmrLstNm("");
		lServiceContractArea.setMut04oMtSmrSfx("");
		lServiceContractArea.setMut04oMtSmrPfx("");
		lServiceContractArea.setMut04oMtDmmCltId("");
		lServiceContractArea.setMut04oMtDmmTerNbr("");
		lServiceContractArea.setMut04oMtDmmDsyNm("");
		lServiceContractArea.setMut04oMtDmmFstNm("");
		lServiceContractArea.setMut04oMtDmmMdlNm("");
		lServiceContractArea.setMut04oMtDmmLstNm("");
		lServiceContractArea.setMut04oMtDmmSfx("");
		lServiceContractArea.setMut04oMtDmmPfx("");
		lServiceContractArea.setMut04oMtRmmCltId("");
		lServiceContractArea.setMut04oMtRmmTerNbr("");
		lServiceContractArea.setMut04oMtRmmDsyNm("");
		lServiceContractArea.setMut04oMtRmmFstNm("");
		lServiceContractArea.setMut04oMtRmmMdlNm("");
		lServiceContractArea.setMut04oMtRmmLstNm("");
		lServiceContractArea.setMut04oMtRmmSfx("");
		lServiceContractArea.setMut04oMtRmmPfx("");
		lServiceContractArea.setMut04oMtDfoCltId("");
		lServiceContractArea.setMut04oMtDfoTerNbr("");
		lServiceContractArea.setMut04oMtDfoDsyNm("");
		lServiceContractArea.setMut04oMtDfoFstNm("");
		lServiceContractArea.setMut04oMtDfoMdlNm("");
		lServiceContractArea.setMut04oMtDfoLstNm("");
		lServiceContractArea.setMut04oMtDfoSfx("");
		lServiceContractArea.setMut04oMtDfoPfx("");
		lServiceContractArea.setMut04oAtBrnCltId("");
		lServiceContractArea.setMut04oAtBrnTerNbr("");
		lServiceContractArea.setMut04oAtBrnNm("");
		lServiceContractArea.setMut04oAtAgcCltId("");
		lServiceContractArea.setMut04oAtAgcTerNbr("");
		lServiceContractArea.setMut04oAtAgcNm("");
		lServiceContractArea.setMut04oAtSmrCltId("");
		lServiceContractArea.setMut04oAtSmrTerNbr("");
		lServiceContractArea.setMut04oAtSmrNm("");
		lServiceContractArea.setMut04oAtAmmCltId("");
		lServiceContractArea.setMut04oAtAmmTerNbr("");
		lServiceContractArea.setMut04oAtAmmDsyNm("");
		lServiceContractArea.setMut04oAtAmmFstNm("");
		lServiceContractArea.setMut04oAtAmmMdlNm("");
		lServiceContractArea.setMut04oAtAmmLstNm("");
		lServiceContractArea.setMut04oAtAmmSfx("");
		lServiceContractArea.setMut04oAtAmmPfx("");
		lServiceContractArea.setMut04oAtAfmCltId("");
		lServiceContractArea.setMut04oAtAfmTerNbr("");
		lServiceContractArea.setMut04oAtAfmDsyNm("");
		lServiceContractArea.setMut04oAtAfmFstNm("");
		lServiceContractArea.setMut04oAtAfmMdlNm("");
		lServiceContractArea.setMut04oAtAfmLstNm("");
		lServiceContractArea.setMut04oAtAfmSfx("");
		lServiceContractArea.setMut04oAtAfmPfx("");
		lServiceContractArea.setMut04oAtAvpCltId("");
		lServiceContractArea.setMut04oAtAvpTerNbr("");
		lServiceContractArea.setMut04oAtAvpDsyNm("");
		lServiceContractArea.setMut04oAtAvpFstNm("");
		lServiceContractArea.setMut04oAtAvpMdlNm("");
		lServiceContractArea.setMut04oAtAvpLstNm("");
		lServiceContractArea.setMut04oAtAvpSfx("");
		lServiceContractArea.setMut04oAtAvpPfx("");
		lServiceContractArea.setMut04oSvcUwDsyNm("");
		lServiceContractArea.setMut04oSvcUwTypeCd("");
		lServiceContractArea.setMut04oSvcUwTypeDes("");
		lServiceContractArea.setMut04oUwCltId("");
		lServiceContractArea.setMut04oUwDsyNm("");
		lServiceContractArea.setMut04oUwFstNm("");
		lServiceContractArea.setMut04oUwMdlNm("");
		lServiceContractArea.setMut04oUwLstNm("");
		lServiceContractArea.setMut04oUwSfx("");
		lServiceContractArea.setMut04oUwPfx("");
		lServiceContractArea.setMut04oRskAlsCltId("");
		lServiceContractArea.setMut04oRskAlsDsyNm("");
		lServiceContractArea.setMut04oRskAlsFstNm("");
		lServiceContractArea.setMut04oRskAlsMdlNm("");
		lServiceContractArea.setMut04oRskAlsLstNm("");
		lServiceContractArea.setMut04oRskAlsSfx("");
		lServiceContractArea.setMut04oRskAlsPfx("");
		lServiceContractArea.setMut04oDumCltId("");
		lServiceContractArea.setMut04oDumDsyNm("");
		lServiceContractArea.setMut04oDumTer("");
		lServiceContractArea.setMut04oDumFstNm("");
		lServiceContractArea.setMut04oDumMdlNm("");
		lServiceContractArea.setMut04oDumLstNm("");
		lServiceContractArea.setMut04oDumSfx("");
		lServiceContractArea.setMut04oDumPfx("");
		lServiceContractArea.setMut04oRumCltId("");
		lServiceContractArea.setMut04oRumDsyNm("");
		lServiceContractArea.setMut04oRumTer("");
		lServiceContractArea.setMut04oRumFstNm("");
		lServiceContractArea.setMut04oRumMdlNm("");
		lServiceContractArea.setMut04oRumLstNm("");
		lServiceContractArea.setMut04oRumSfx("");
		lServiceContractArea.setMut04oRumPfx("");
		lServiceContractArea.setMut04oFpuCltId("");
		lServiceContractArea.setMut04oFpuDsyNm("");
		lServiceContractArea.setMut04oFpuFstNm("");
		lServiceContractArea.setMut04oFpuMdlNm("");
		lServiceContractArea.setMut04oFpuLstNm("");
		lServiceContractArea.setMut04oFpuSfx("");
		lServiceContractArea.setMut04oFpuPfx("");
		lServiceContractArea.setMut04oFpuDumCltId("");
		lServiceContractArea.setMut04oFpuDumDsyNm("");
		lServiceContractArea.setMut04oFpuDumTer("");
		lServiceContractArea.setMut04oFpuDumFstNm("");
		lServiceContractArea.setMut04oFpuDumMdlNm("");
		lServiceContractArea.setMut04oFpuDumLstNm("");
		lServiceContractArea.setMut04oFpuDumSfx("");
		lServiceContractArea.setMut04oFpuDumPfx("");
		lServiceContractArea.setMut04oFpuRumCltId("");
		lServiceContractArea.setMut04oFpuRumDsyNm("");
		lServiceContractArea.setMut04oFpuRumTer("");
		lServiceContractArea.setMut04oFpuRumFstNm("");
		lServiceContractArea.setMut04oFpuRumMdlNm("");
		lServiceContractArea.setMut04oFpuRumLstNm("");
		lServiceContractArea.setMut04oFpuRumSfx("");
		lServiceContractArea.setMut04oFpuRumPfx("");
		lServiceContractArea.setMut04oLpRskAlsCltId("");
		lServiceContractArea.setMut04oLpRskAlsDsyNm("");
		lServiceContractArea.setMut04oLpRskAlsFstNm("");
		lServiceContractArea.setMut04oLpRskAlsMdlNm("");
		lServiceContractArea.setMut04oLpRskAlsLstNm("");
		lServiceContractArea.setMut04oLpRskAlsSfx("");
		lServiceContractArea.setMut04oLpRskAlsPfx("");
		lServiceContractArea.setMut04oLpraDumCltId("");
		lServiceContractArea.setMut04oLpraDumDsyNm("");
		lServiceContractArea.setMut04oLpraDumTer("");
		lServiceContractArea.setMut04oLpraDumFstNm("");
		lServiceContractArea.setMut04oLpraDumMdlNm("");
		lServiceContractArea.setMut04oLpraDumLstNm("");
		lServiceContractArea.setMut04oLpraDumSfx("");
		lServiceContractArea.setMut04oLpraDumPfx("");
		lServiceContractArea.setMut04oLpraRumCltId("");
		lServiceContractArea.setMut04oLpraRumDsyNm("");
		lServiceContractArea.setMut04oLpraRumTer("");
		lServiceContractArea.setMut04oLpraRumFstNm("");
		lServiceContractArea.setMut04oLpraRumMdlNm("");
		lServiceContractArea.setMut04oLpraRumLstNm("");
		lServiceContractArea.setMut04oLpraRumSfx("");
		lServiceContractArea.setMut04oLpraRumPfx("");
		lServiceContractArea.setMut04oAlStateCd("");
		lServiceContractArea.setMut04oAlCountyCd("");
		lServiceContractArea.setMut04oAlTownCd("");
		lServiceContractArea.setMut04oNbrEmployees(0);
		for (int idx0 = 1; idx0 <= LServiceContractArea.MUT04O_SE_SIC_MAXOCCURS; idx0++) {
			lServiceContractArea.setMut04oSeSicCd(idx0, "");
			lServiceContractArea.setMut04oSeSicDes(idx0, "");
		}
		lServiceContractArea.setMut04oOlDivision(Types.SPACE_CHAR);
		lServiceContractArea.setMut04oOlSubDivision(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
