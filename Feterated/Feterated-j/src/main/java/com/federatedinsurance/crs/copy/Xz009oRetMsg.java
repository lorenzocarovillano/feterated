/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;

/**Original name: XZ009O-RET-MSG<br>
 * Variable: XZ009O-RET-MSG from copybook XZC090CO<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xz009oRetMsg {

	//==== PROPERTIES ====
	public static final int ERR_MSG_MAXOCCURS = 10;
	public static final int WNG_MSG_MAXOCCURS = 10;
	//Original name: XZ009O-RET-CD
	private Xz08coRetCd retCd = new Xz08coRetCd();
	//Original name: XZ009O-ERR-MSG
	private String[] errMsg = new String[ERR_MSG_MAXOCCURS];
	//Original name: XZ009O-WNG-MSG
	private String[] wngMsg = new String[WNG_MSG_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xz009oRetMsg() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int errMsgIdx = 1; errMsgIdx <= ERR_MSG_MAXOCCURS; errMsgIdx++) {
			setXzc09oErrMsg(errMsgIdx, DefaultValues.stringVal(Len.XZC09O_ERR_MSG));
		}
		for (int wngMsgIdx = 1; wngMsgIdx <= WNG_MSG_MAXOCCURS; wngMsgIdx++) {
			setXzc09oWngMsg(wngMsgIdx, DefaultValues.stringVal(Len.XZC09O_WNG_MSG));
		}
	}

	public void setRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		retCd.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		setXzc09oErrLisBytes(buffer, position);
		position += Len.XZC09O_ERR_LIS;
		setXzc09oWngLisBytes(buffer, position);
	}

	public byte[] getRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeShort(buffer, position, retCd.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		getXzc09oErrLisBytes(buffer, position);
		position += Len.XZC09O_ERR_LIS;
		getXzc09oWngLisBytes(buffer, position);
		return buffer;
	}

	public void setXzc09oErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setXzc09oErrMsg(idx, MarshalByte.readString(buffer, position, Len.XZC09O_ERR_MSG));
				position += Len.XZC09O_ERR_MSG;
			} else {
				setXzc09oErrMsg(idx, "");
				position += Len.XZC09O_ERR_MSG;
			}
		}
	}

	public byte[] getXzc09oErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= ERR_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getXzc09oErrMsg(idx), Len.XZC09O_ERR_MSG);
			position += Len.XZC09O_ERR_MSG;
		}
		return buffer;
	}

	public void setXzc09oErrMsg(int xzc09oErrMsgIdx, String xzc09oErrMsg) {
		this.errMsg[xzc09oErrMsgIdx - 1] = Functions.subString(xzc09oErrMsg, Len.XZC09O_ERR_MSG);
	}

	public String getXzc09oErrMsg(int xzc09oErrMsgIdx) {
		return this.errMsg[xzc09oErrMsgIdx - 1];
	}

	public void setXzc09oWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setXzc09oWngMsg(idx, MarshalByte.readString(buffer, position, Len.XZC09O_WNG_MSG));
				position += Len.XZC09O_WNG_MSG;
			} else {
				setXzc09oWngMsg(idx, "");
				position += Len.XZC09O_WNG_MSG;
			}
		}
	}

	public byte[] getXzc09oWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= WNG_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getXzc09oWngMsg(idx), Len.XZC09O_WNG_MSG);
			position += Len.XZC09O_WNG_MSG;
		}
		return buffer;
	}

	public void setXzc09oWngMsg(int xzc09oWngMsgIdx, String xzc09oWngMsg) {
		this.wngMsg[xzc09oWngMsgIdx - 1] = Functions.subString(xzc09oWngMsg, Len.XZC09O_WNG_MSG);
	}

	public String getXzc09oWngMsg(int xzc09oWngMsgIdx) {
		return this.wngMsg[xzc09oWngMsgIdx - 1];
	}

	public Xz08coRetCd getRetCd() {
		return retCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC09O_ERR_MSG = 500;
		public static final int XZC09O_ERR_LIS = Xz009oRetMsg.ERR_MSG_MAXOCCURS * XZC09O_ERR_MSG;
		public static final int XZC09O_WNG_MSG = 500;
		public static final int XZC09O_WNG_LIS = Xz009oRetMsg.WNG_MSG_MAXOCCURS * XZC09O_WNG_MSG;
		public static final int RET_MSG = Xz08coRetCd.Len.XZ08CO_RET_CD + XZC09O_ERR_LIS + XZC09O_WNG_LIS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
