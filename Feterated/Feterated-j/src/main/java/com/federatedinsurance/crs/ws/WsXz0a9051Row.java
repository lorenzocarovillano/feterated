/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Xza951TechnicalKey;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A9051-ROW<br>
 * Variable: WS-XZ0A9051-ROW from program XZ0B9050<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a9051Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA951-TECHNICAL-KEY
	private Xza951TechnicalKey technicalKey = new Xza951TechnicalKey();
	//Original name: XZA951-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZA951-POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: XZA951-POL-TYP-DES
	private String polTypDes = DefaultValues.stringVal(Len.POL_TYP_DES);
	//Original name: XZA951-POL-PRI-RSK-ST-ABB
	private String polPriRskStAbb = DefaultValues.stringVal(Len.POL_PRI_RSK_ST_ABB);
	//Original name: XZA951-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);
	//Original name: XZA951-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: XZA951-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: XZA951-POL-DUE-AMT
	private AfDecimal polDueAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZA951-MASTER-COMPANY-NBR
	private String masterCompanyNbr = DefaultValues.stringVal(Len.MASTER_COMPANY_NBR);
	//Original name: XZA951-MASTER-COMPANY-DES
	private String masterCompanyDes = DefaultValues.stringVal(Len.MASTER_COMPANY_DES);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A9051_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a9051RowBytes(buf);
	}

	public String getWsXz0a9051RowFormatted() {
		return getPolicyListDetailFormatted();
	}

	public void setWsXz0a9051RowBytes(byte[] buffer) {
		setWsXz0a9051RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a9051RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A9051_ROW];
		return getWsXz0a9051RowBytes(buffer, 1);
	}

	public void setWsXz0a9051RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setPolicyListDetailBytes(buffer, position);
	}

	public byte[] getWsXz0a9051RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getPolicyListDetailBytes(buffer, position);
		return buffer;
	}

	public String getPolicyListDetailFormatted() {
		return MarshalByteExt.bufferToStr(getPolicyListDetailBytes());
	}

	/**Original name: XZA951-POLICY-LIST-DETAIL<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0A0951 - XZ_GET_POL_LIST_BPO                                 *
	 *             XZ_GET_POL_LIST UOW                                 *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 11/19/2008  E404GCL   NEW                              *
	 *  PP03824 04/02/2013  E404DNF   ADDED MASTER COMPANY NBR         *
	 *  20163.20 07/13/2018 E404DMW   UPDATED ADDRESS AND CLIENT IDS   *
	 *                                FROM 20 TO 64                    *
	 * *****************************************************************</pre>*/
	public byte[] getPolicyListDetailBytes() {
		byte[] buffer = new byte[Len.POLICY_LIST_DETAIL];
		return getPolicyListDetailBytes(buffer, 1);
	}

	public void setPolicyListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		technicalKey.setTechnicalKeyBytes(buffer, position);
		position += Xza951TechnicalKey.Len.TECHNICAL_KEY;
		polNbr = MarshalByte.readString(buffer, position, Len.POL_NBR);
		position += Len.POL_NBR;
		setPolicyTypeBytes(buffer, position);
		position += Len.POLICY_TYPE;
		polPriRskStAbb = MarshalByte.readString(buffer, position, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		notEffDt = MarshalByte.readString(buffer, position, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		polEffDt = MarshalByte.readString(buffer, position, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		polExpDt = MarshalByte.readString(buffer, position, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		polDueAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.POL_DUE_AMT, Len.Fract.POL_DUE_AMT));
		position += Len.POL_DUE_AMT;
		setMasterCompanyBytes(buffer, position);
	}

	public byte[] getPolicyListDetailBytes(byte[] buffer, int offset) {
		int position = offset;
		technicalKey.getTechnicalKeyBytes(buffer, position);
		position += Xza951TechnicalKey.Len.TECHNICAL_KEY;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		getPolicyTypeBytes(buffer, position);
		position += Len.POLICY_TYPE;
		MarshalByte.writeString(buffer, position, polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
		position += Len.POL_PRI_RSK_ST_ABB;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		position += Len.NOT_EFF_DT;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeDecimal(buffer, position, polDueAmt.copy());
		position += Len.POL_DUE_AMT;
		getMasterCompanyBytes(buffer, position);
		return buffer;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolicyTypeBytes(byte[] buffer, int offset) {
		int position = offset;
		polTypCd = MarshalByte.readString(buffer, position, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		polTypDes = MarshalByte.readString(buffer, position, Len.POL_TYP_DES);
	}

	public byte[] getPolicyTypeBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polTypCd, Len.POL_TYP_CD);
		position += Len.POL_TYP_CD;
		MarshalByte.writeString(buffer, position, polTypDes, Len.POL_TYP_DES);
		return buffer;
	}

	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	public String getPolTypCd() {
		return this.polTypCd;
	}

	public void setPolTypDes(String polTypDes) {
		this.polTypDes = Functions.subString(polTypDes, Len.POL_TYP_DES);
	}

	public String getPolTypDes() {
		return this.polTypDes;
	}

	public void setPolPriRskStAbb(String polPriRskStAbb) {
		this.polPriRskStAbb = Functions.subString(polPriRskStAbb, Len.POL_PRI_RSK_ST_ABB);
	}

	public String getPolPriRskStAbb() {
		return this.polPriRskStAbb;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setPolDueAmt(AfDecimal polDueAmt) {
		this.polDueAmt.assign(polDueAmt);
	}

	public AfDecimal getPolDueAmt() {
		return this.polDueAmt.copy();
	}

	public void setMasterCompanyBytes(byte[] buffer, int offset) {
		int position = offset;
		masterCompanyNbr = MarshalByte.readString(buffer, position, Len.MASTER_COMPANY_NBR);
		position += Len.MASTER_COMPANY_NBR;
		masterCompanyDes = MarshalByte.readString(buffer, position, Len.MASTER_COMPANY_DES);
	}

	public byte[] getMasterCompanyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, masterCompanyNbr, Len.MASTER_COMPANY_NBR);
		position += Len.MASTER_COMPANY_NBR;
		MarshalByte.writeString(buffer, position, masterCompanyDes, Len.MASTER_COMPANY_DES);
		return buffer;
	}

	public void setMasterCompanyNbr(String masterCompanyNbr) {
		this.masterCompanyNbr = Functions.subString(masterCompanyNbr, Len.MASTER_COMPANY_NBR);
	}

	public String getMasterCompanyNbr() {
		return this.masterCompanyNbr;
	}

	public void setMasterCompanyDes(String masterCompanyDes) {
		this.masterCompanyDes = Functions.subString(masterCompanyDes, Len.MASTER_COMPANY_DES);
	}

	public String getMasterCompanyDes() {
		return this.masterCompanyDes;
	}

	public Xza951TechnicalKey getTechnicalKey() {
		return technicalKey;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a9051RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_TYP_CD = 3;
		public static final int POL_TYP_DES = 30;
		public static final int POLICY_TYPE = POL_TYP_CD + POL_TYP_DES;
		public static final int POL_PRI_RSK_ST_ABB = 2;
		public static final int NOT_EFF_DT = 10;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int POL_DUE_AMT = 10;
		public static final int MASTER_COMPANY_NBR = 2;
		public static final int MASTER_COMPANY_DES = 40;
		public static final int MASTER_COMPANY = MASTER_COMPANY_NBR + MASTER_COMPANY_DES;
		public static final int POLICY_LIST_DETAIL = Xza951TechnicalKey.Len.TECHNICAL_KEY + POL_NBR + POLICY_TYPE + POL_PRI_RSK_ST_ABB + NOT_EFF_DT
				+ POL_EFF_DT + POL_EXP_DT + POL_DUE_AMT + MASTER_COMPANY;
		public static final int WS_XZ0A9051_ROW = POLICY_LIST_DETAIL;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
