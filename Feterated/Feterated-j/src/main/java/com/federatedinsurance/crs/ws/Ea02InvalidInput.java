/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-02-INVALID-INPUT<br>
 * Variable: EA-02-INVALID-INPUT from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea02InvalidInput {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-02-INVALID-INPUT
	private String flr1 = "INVALID INPUT";
	//Original name: FILLER-EA-02-INVALID-INPUT-1
	private String flr2 = "INVALID POLICY";
	//Original name: FILLER-EA-02-INVALID-INPUT-2
	private String flr3 = "NUMBER:";
	//Original name: EA-02-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-02-INVALID-INPUT-3
	private String flr4 = " INVALID EXP DT";
	//Original name: FILLER-EA-02-INVALID-INPUT-4
	private String flr5 = ":";
	//Original name: EA-02-EXP-DT
	private String expDt = DefaultValues.stringVal(Len.EXP_DT);

	//==== METHODS ====
	public String getEa02InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa02InvalidInputBytes());
	}

	public byte[] getEa02InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA02_INVALID_INPUT];
		return getEa02InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa02InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, expDt, Len.EXP_DT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setExpDt(String expDt) {
		this.expDt = Functions.subString(expDt, Len.EXP_DT);
	}

	public String getExpDt() {
		return this.expDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int EXP_DT = 10;
		public static final int FLR1 = 14;
		public static final int FLR2 = 15;
		public static final int FLR3 = 8;
		public static final int FLR5 = 2;
		public static final int EA02_INVALID_INPUT = POL_NBR + EXP_DT + FLR1 + 2 * FLR2 + FLR3 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
