/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Fnc02iMatchStringArray;
import com.federatedinsurance.crs.ws.occurs.Fnc02oMatchStringArray;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DFHCOMMAREA<br>
 * Variable: DFHCOMMAREA from program FNC02090<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DfhcommareaFnc02090 extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int I_MATCH_STRING_ARRAY_MAXOCCURS = 4;
	public static final int O_MATCH_STRING_ARRAY_MAXOCCURS = 4;
	//Original name: FNC02I-USERID
	private String iUserid = DefaultValues.stringVal(Len.I_USERID);
	//Original name: FNC02I-MATCH-STRING-ARRAY
	private Fnc02iMatchStringArray[] iMatchStringArray = new Fnc02iMatchStringArray[I_MATCH_STRING_ARRAY_MAXOCCURS];
	//Original name: FILLER-FNC020-PROGRAM-INPUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);
	//Original name: FNC02O-MATCH-STRING-ARRAY
	private Fnc02oMatchStringArray[] oMatchStringArray = new Fnc02oMatchStringArray[O_MATCH_STRING_ARRAY_MAXOCCURS];
	//Original name: FNC02O-ERROR-CODE
	private Xz08coRetCd oErrorCode = new Xz08coRetCd();
	//Original name: FNC02O-ERROR-MESSAGE
	private String oErrorMessage = DefaultValues.stringVal(Len.O_ERROR_MESSAGE);
	//Original name: FILLER-FNC020-PROGRAM-OUTPUT
	private String flr2 = DefaultValues.stringVal(Len.FLR1);

	//==== CONSTRUCTORS ====
	public DfhcommareaFnc02090() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DFHCOMMAREA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDfhcommareaBytes(buf);
	}

	public void init() {
		for (int iMatchStringArrayIdx = 1; iMatchStringArrayIdx <= I_MATCH_STRING_ARRAY_MAXOCCURS; iMatchStringArrayIdx++) {
			iMatchStringArray[iMatchStringArrayIdx - 1] = new Fnc02iMatchStringArray();
		}
		for (int oMatchStringArrayIdx = 1; oMatchStringArrayIdx <= O_MATCH_STRING_ARRAY_MAXOCCURS; oMatchStringArrayIdx++) {
			oMatchStringArray[oMatchStringArrayIdx - 1] = new Fnc02oMatchStringArray();
		}
	}

	public void setDfhcommareaBytes(byte[] buffer) {
		setDfhcommareaBytes(buffer, 1);
	}

	public byte[] getDfhcommareaBytes() {
		byte[] buffer = new byte[Len.DFHCOMMAREA];
		return getDfhcommareaBytes(buffer, 1);
	}

	public void setDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		setFnc020ProgramInputBytes(buffer, position);
		position += Len.FNC020_PROGRAM_INPUT;
		setFnc020ProgramOutputBytes(buffer, position);
	}

	public byte[] getDfhcommareaBytes(byte[] buffer, int offset) {
		int position = offset;
		getFnc020ProgramInputBytes(buffer, position);
		position += Len.FNC020_PROGRAM_INPUT;
		getFnc020ProgramOutputBytes(buffer, position);
		return buffer;
	}

	public void setFnc020ProgramInputBytes(byte[] buffer, int offset) {
		int position = offset;
		iUserid = MarshalByte.readString(buffer, position, Len.I_USERID);
		position += Len.I_USERID;
		for (int idx = 1; idx <= I_MATCH_STRING_ARRAY_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				iMatchStringArray[idx - 1].setiMatchStringArrayBytes(buffer, position);
				position += Fnc02iMatchStringArray.Len.I_MATCH_STRING_ARRAY;
			} else {
				iMatchStringArray[idx - 1].initiMatchStringArraySpaces();
				position += Fnc02iMatchStringArray.Len.I_MATCH_STRING_ARRAY;
			}
		}
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getFnc020ProgramInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, iUserid, Len.I_USERID);
		position += Len.I_USERID;
		for (int idx = 1; idx <= I_MATCH_STRING_ARRAY_MAXOCCURS; idx++) {
			iMatchStringArray[idx - 1].getiMatchStringArrayBytes(buffer, position);
			position += Fnc02iMatchStringArray.Len.I_MATCH_STRING_ARRAY;
		}
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setiUserid(String iUserid) {
		this.iUserid = Functions.subString(iUserid, Len.I_USERID);
	}

	public String getiUserid() {
		return this.iUserid;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setFnc020ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= O_MATCH_STRING_ARRAY_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				oMatchStringArray[idx - 1].setoMatchStringArrayBytes(buffer, position);
				position += Fnc02oMatchStringArray.Len.O_MATCH_STRING_ARRAY;
			} else {
				oMatchStringArray[idx - 1].initoMatchStringArraySpaces();
				position += Fnc02oMatchStringArray.Len.O_MATCH_STRING_ARRAY;
			}
		}
		oErrorCode.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		oErrorMessage = MarshalByte.readString(buffer, position, Len.O_ERROR_MESSAGE);
		position += Len.O_ERROR_MESSAGE;
		flr2 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getFnc020ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= O_MATCH_STRING_ARRAY_MAXOCCURS; idx++) {
			oMatchStringArray[idx - 1].getoMatchStringArrayBytes(buffer, position);
			position += Fnc02oMatchStringArray.Len.O_MATCH_STRING_ARRAY;
		}
		MarshalByte.writeShort(buffer, position, oErrorCode.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		MarshalByte.writeString(buffer, position, oErrorMessage, Len.O_ERROR_MESSAGE);
		position += Len.O_ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR1);
		return buffer;
	}

	public void setoErrorMessage(String oErrorMessage) {
		this.oErrorMessage = Functions.subString(oErrorMessage, Len.O_ERROR_MESSAGE);
	}

	public String getoErrorMessage() {
		return this.oErrorMessage;
	}

	public void setFlr2(String flr2) {
		this.flr2 = Functions.subString(flr2, Len.FLR1);
	}

	public String getFlr2() {
		return this.flr2;
	}

	public Fnc02iMatchStringArray getiMatchStringArray(int idx) {
		return iMatchStringArray[idx - 1];
	}

	public Xz08coRetCd getoErrorCode() {
		return oErrorCode;
	}

	public Fnc02oMatchStringArray getoMatchStringArray(int idx) {
		return oMatchStringArray[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getDfhcommareaBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int I_USERID = 8;
		public static final int FLR1 = 500;
		public static final int FNC020_PROGRAM_INPUT = I_USERID
				+ DfhcommareaFnc02090.I_MATCH_STRING_ARRAY_MAXOCCURS * Fnc02iMatchStringArray.Len.I_MATCH_STRING_ARRAY + FLR1;
		public static final int O_ERROR_MESSAGE = 500;
		public static final int FNC020_PROGRAM_OUTPUT = DfhcommareaFnc02090.O_MATCH_STRING_ARRAY_MAXOCCURS
				* Fnc02oMatchStringArray.Len.O_MATCH_STRING_ARRAY + Xz08coRetCd.Len.XZ08CO_RET_CD + O_ERROR_MESSAGE + FLR1;
		public static final int DFHCOMMAREA = FNC020_PROGRAM_INPUT + FNC020_PROGRAM_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
