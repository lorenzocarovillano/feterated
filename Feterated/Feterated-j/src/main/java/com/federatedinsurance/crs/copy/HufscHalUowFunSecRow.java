/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: HUFSC-HAL-UOW-FUN-SEC-ROW<br>
 * Variable: HUFSC-HAL-UOW-FUN-SEC-ROW from copybook HALLCUFS<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class HufscHalUowFunSecRow {

	//==== PROPERTIES ====
	//Original name: HUFSC-HAL-UOW-FUN-SEC-CSUM
	private String halUowFunSecCsum = DefaultValues.stringVal(Len.HAL_UOW_FUN_SEC_CSUM);
	//Original name: HUFSC-UOW-NM-KCRE
	private String uowNmKcre = DefaultValues.stringVal(Len.UOW_NM_KCRE);
	//Original name: HUFSC-SEC-GRP-NM-KCRE
	private String secGrpNmKcre = DefaultValues.stringVal(Len.SEC_GRP_NM_KCRE);
	//Original name: HUFSC-CONTEXT-KCRE
	private String contextKcre = DefaultValues.stringVal(Len.CONTEXT_KCRE);
	//Original name: HUFSC-SEC-ASC-TYP-KCRE
	private String secAscTypKcre = DefaultValues.stringVal(Len.SEC_ASC_TYP_KCRE);
	//Original name: HUFSC-FUN-SEQ-NBR-KCRE
	private String funSeqNbrKcre = DefaultValues.stringVal(Len.FUN_SEQ_NBR_KCRE);
	//Original name: HUFSC-HAL-UOW-FUN-SEC-KEY
	private HufscHalUowFunSecKey halUowFunSecKey = new HufscHalUowFunSecKey();
	//Original name: HUFSC-HAL-UOW-FUN-SEC-DATA
	private HufscHalUowFunSecData halUowFunSecData = new HufscHalUowFunSecData();

	//==== METHODS ====
	public String getHufscHalUowFunSecRowFormatted() {
		return MarshalByteExt.bufferToStr(getHufscHalUowFunSecRowBytes());
	}

	public byte[] getHufscHalUowFunSecRowBytes() {
		byte[] buffer = new byte[Len.HUFSC_HAL_UOW_FUN_SEC_ROW];
		return getHufscHalUowFunSecRowBytes(buffer, 1);
	}

	public byte[] getHufscHalUowFunSecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getHalUowFunSecFixedBytes(buffer, position);
		position += Len.HAL_UOW_FUN_SEC_FIXED;
		halUowFunSecKey.getHalUowFunSecKeyBytes(buffer, position);
		position += HufscHalUowFunSecKey.Len.HAL_UOW_FUN_SEC_KEY;
		halUowFunSecData.getHalUowFunSecDataBytes(buffer, position);
		return buffer;
	}

	public void initHufscHalUowFunSecRowSpaces() {
		initHalUowFunSecFixedSpaces();
		halUowFunSecKey.initHalUowFunSecKeySpaces();
		halUowFunSecData.initHalUowFunSecDataSpaces();
	}

	public byte[] getHalUowFunSecFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, halUowFunSecCsum, Len.HAL_UOW_FUN_SEC_CSUM);
		position += Len.HAL_UOW_FUN_SEC_CSUM;
		MarshalByte.writeString(buffer, position, uowNmKcre, Len.UOW_NM_KCRE);
		position += Len.UOW_NM_KCRE;
		MarshalByte.writeString(buffer, position, secGrpNmKcre, Len.SEC_GRP_NM_KCRE);
		position += Len.SEC_GRP_NM_KCRE;
		MarshalByte.writeString(buffer, position, contextKcre, Len.CONTEXT_KCRE);
		position += Len.CONTEXT_KCRE;
		MarshalByte.writeString(buffer, position, secAscTypKcre, Len.SEC_ASC_TYP_KCRE);
		position += Len.SEC_ASC_TYP_KCRE;
		MarshalByte.writeString(buffer, position, funSeqNbrKcre, Len.FUN_SEQ_NBR_KCRE);
		return buffer;
	}

	public void initHalUowFunSecFixedSpaces() {
		halUowFunSecCsum = "";
		uowNmKcre = "";
		secGrpNmKcre = "";
		contextKcre = "";
		secAscTypKcre = "";
		funSeqNbrKcre = "";
	}

	public String getUowNmKcre() {
		return this.uowNmKcre;
	}

	public String getSecGrpNmKcre() {
		return this.secGrpNmKcre;
	}

	public String getContextKcre() {
		return this.contextKcre;
	}

	public String getSecAscTypKcre() {
		return this.secAscTypKcre;
	}

	public String getFunSeqNbrKcre() {
		return this.funSeqNbrKcre;
	}

	public HufscHalUowFunSecData getHalUowFunSecData() {
		return halUowFunSecData;
	}

	public HufscHalUowFunSecKey getHalUowFunSecKey() {
		return halUowFunSecKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int HAL_UOW_FUN_SEC_CSUM = 9;
		public static final int UOW_NM_KCRE = 32;
		public static final int SEC_GRP_NM_KCRE = 32;
		public static final int CONTEXT_KCRE = 32;
		public static final int SEC_ASC_TYP_KCRE = 32;
		public static final int FUN_SEQ_NBR_KCRE = 32;
		public static final int HAL_UOW_FUN_SEC_FIXED = HAL_UOW_FUN_SEC_CSUM + UOW_NM_KCRE + SEC_GRP_NM_KCRE + CONTEXT_KCRE + SEC_ASC_TYP_KCRE
				+ FUN_SEQ_NBR_KCRE;
		public static final int HUFSC_HAL_UOW_FUN_SEC_ROW = HAL_UOW_FUN_SEC_FIXED + HufscHalUowFunSecKey.Len.HAL_UOW_FUN_SEC_KEY
				+ HufscHalUowFunSecData.Len.HAL_UOW_FUN_SEC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
