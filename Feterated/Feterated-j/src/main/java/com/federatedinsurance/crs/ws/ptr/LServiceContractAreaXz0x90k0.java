/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90K0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90k0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90k0() {
	}

	public LServiceContractAreaXz0x90k0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT9KI-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT9KI-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	public void setoTkNotPrcTs(String oTkNotPrcTs) {
		writeString(Pos.O_TK_NOT_PRC_TS, oTkNotPrcTs, Len.O_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9KO-TK-NOT-PRC-TS<br>*/
	public String getoTkNotPrcTs() {
		return readString(Pos.O_TK_NOT_PRC_TS, Len.O_TK_NOT_PRC_TS);
	}

	public void setoCsrActNbr(String oCsrActNbr) {
		writeString(Pos.O_CSR_ACT_NBR, oCsrActNbr, Len.O_CSR_ACT_NBR);
	}

	/**Original name: XZT9KO-CSR-ACT-NBR<br>*/
	public String getoCsrActNbr() {
		return readString(Pos.O_CSR_ACT_NBR, Len.O_CSR_ACT_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT9K0_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_CSR_ACT_NBR = XZT9K0_SERVICE_INPUTS;
		public static final int I_USERID = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int XZT9K0_SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int O_TECHNICAL_KEY = XZT9K0_SERVICE_OUTPUTS;
		public static final int O_TK_NOT_PRC_TS = O_TECHNICAL_KEY;
		public static final int O_CSR_ACT_NBR = O_TK_NOT_PRC_TS + Len.O_TK_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_USERID = 8;
		public static final int O_TK_NOT_PRC_TS = 26;
		public static final int XZT9K0_SERVICE_INPUTS = I_CSR_ACT_NBR + I_USERID;
		public static final int O_TECHNICAL_KEY = O_TK_NOT_PRC_TS;
		public static final int O_CSR_ACT_NBR = 9;
		public static final int XZT9K0_SERVICE_OUTPUTS = O_TECHNICAL_KEY + O_CSR_ACT_NBR;
		public static final int L_SERVICE_CONTRACT_AREA = XZT9K0_SERVICE_INPUTS + XZT9K0_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
