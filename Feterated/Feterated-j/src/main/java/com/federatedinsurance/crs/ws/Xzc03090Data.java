/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.copy.Fwppcstk;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz03ci1o;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZC03090<br>
 * Generated as a class for rule WS.<br>*/
public class Xzc03090Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXzc03090 constantFields = new ConstantFieldsXzc03090();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXzc03090 errorAndAdviceMessages = new ErrorAndAdviceMessagesXzc03090();
	//Original name: SUBSCRIPTS
	private SubscriptsXzc03090 subscripts = new SubscriptsXzc03090();
	//Original name: URI-LKU-LINKAGE
	private Ts571cb1 ts571cb1 = new Ts571cb1();
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXzc03090 workingStorageArea = new WorkingStorageAreaXzc03090();
	//Original name: IVORYH
	private Ivoryh ivoryh = new Ivoryh();
	//Original name: FWPPCSTK
	private Fwppcstk fwppcstk = new Fwppcstk();
	//Original name: XZ003I-ACT-NBR
	private String xz003iActNbr = DefaultValues.stringVal(Len.XZ003I_ACT_NBR);
	//Original name: XZ03CI1O
	private Xz03ci1o xz03ci1o = new Xz03ci1o();

	//==== METHODS ====
	/**Original name: CALLABLE-INPUTS<br>*/
	public byte[] getCallableInputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_INPUTS];
		return getCallableInputsBytes(buffer, 1);
	}

	public byte[] getCallableInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		fwppcstk.getTechnicalKeysBytes(buffer, position);
		position += Fwppcstk.Len.TECHNICAL_KEYS;
		getXz003iGetCerPolLisBytes(buffer, position);
		return buffer;
	}

	public void initCallableInputsLowValues() {
		fwppcstk.initFwppcstkLowValues();
		initXz003iGetCerPolLisLowValues();
	}

	public byte[] getXz003iGetCerPolLisBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xz003iActNbr, Len.XZ003I_ACT_NBR);
		return buffer;
	}

	public void initXz003iGetCerPolLisLowValues() {
		xz003iActNbr = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ003I_ACT_NBR);
	}

	public void setXz003iActNbr(String xz003iActNbr) {
		this.xz003iActNbr = Functions.subString(xz003iActNbr, Len.XZ003I_ACT_NBR);
	}

	public String getXz003iActNbr() {
		return this.xz003iActNbr;
	}

	public void setCallableOutputsBytes(byte[] buffer) {
		setCallableOutputsBytes(buffer, 1);
	}

	/**Original name: CALLABLE-OUTPUTS<br>*/
	public byte[] getCallableOutputsBytes() {
		byte[] buffer = new byte[Len.CALLABLE_OUTPUTS];
		return getCallableOutputsBytes(buffer, 1);
	}

	public void setCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz03ci1o.setGetCerPolLisBytes(buffer, position);
	}

	public byte[] getCallableOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		xz03ci1o.getGetCerPolLisBytes(buffer, position);
		return buffer;
	}

	public void initCallableOutputsLowValues() {
		xz03ci1o.initXz03ci1oLowValues();
	}

	public ConstantFieldsXzc03090 getConstantFields() {
		return constantFields;
	}

	public ErrorAndAdviceMessagesXzc03090 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public Fwppcstk getFwppcstk() {
		return fwppcstk;
	}

	public Ivoryh getIvoryh() {
		return ivoryh;
	}

	public SubscriptsXzc03090 getSubscripts() {
		return subscripts;
	}

	public Ts571cb1 getTs571cb1() {
		return ts571cb1;
	}

	public WorkingStorageAreaXzc03090 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public Xz03ci1o getXz03ci1o() {
		return xz03ci1o;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ003I_ACT_NBR = 9;
		public static final int XZ003I_GET_CER_POL_LIS = XZ003I_ACT_NBR;
		public static final int CALLABLE_INPUTS = Fwppcstk.Len.TECHNICAL_KEYS + XZ003I_GET_CER_POL_LIS;
		public static final int CALLABLE_OUTPUTS = Xz03ci1o.Len.GET_CER_POL_LIS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
