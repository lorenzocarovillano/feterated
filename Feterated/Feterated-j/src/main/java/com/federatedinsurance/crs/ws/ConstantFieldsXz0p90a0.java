/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90A0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90a0 {

	//==== PROPERTIES ====
	//Original name: CF-ADD-INS-REC-TYP-CD
	private String addInsRecTypCd = "ADINS";
	//Original name: CF-MANUAL-IND
	private char manualInd = 'N';
	//Original name: CF-SP-ADD-ACT-NOT-REC-SVC
	private String spAddActNotRecSvc = "XZ0X0012";
	//Original name: CF-SP-GET-TTY-LIST-SVC
	private String spGetTtyListSvc = "XZ0X9080";
	//Original name: CF-SP-GET-TTY-CERT-LIST-SVC
	private String spGetTtyCertListSvc = "XZ0X9081";

	//==== METHODS ====
	public String getAddInsRecTypCd() {
		return this.addInsRecTypCd;
	}

	public char getManualInd() {
		return this.manualInd;
	}

	public String getSpAddActNotRecSvc() {
		return this.spAddActNotRecSvc;
	}

	public String getSpGetTtyListSvc() {
		return this.spGetTtyListSvc;
	}

	public String getSpGetTtyCertListSvc() {
		return this.spGetTtyCertListSvc;
	}
}
