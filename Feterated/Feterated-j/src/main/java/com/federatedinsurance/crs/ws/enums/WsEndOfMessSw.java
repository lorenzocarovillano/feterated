/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-END-OF-MESS-SW<br>
 * Variable: WS-END-OF-MESS-SW from program HALRPLAC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsEndOfMessSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char END_OF_MESS = 'Y';
	public static final char NOT_END_OF_MESS = 'N';

	//==== METHODS ====
	public void setEndOfMessSw(char endOfMessSw) {
		this.value = endOfMessSw;
	}

	public char getEndOfMessSw() {
		return this.value;
	}

	public boolean isEndOfMess() {
		return value == END_OF_MESS;
	}

	public void setEndOfMess() {
		value = END_OF_MESS;
	}

	public void setNotEndOfMess() {
		value = NOT_END_OF_MESS;
	}
}
