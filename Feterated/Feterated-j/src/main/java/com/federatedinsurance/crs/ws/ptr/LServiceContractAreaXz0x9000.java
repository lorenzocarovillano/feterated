/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X9000<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x9000 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x9000() {
	}

	public LServiceContractAreaXz0x9000(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setiCsrActNbr(String iCsrActNbr) {
		writeString(Pos.I_CSR_ACT_NBR, iCsrActNbr, Len.I_CSR_ACT_NBR);
	}

	/**Original name: XZT90I-CSR-ACT-NBR<br>*/
	public String getiCsrActNbr() {
		return readString(Pos.I_CSR_ACT_NBR, Len.I_CSR_ACT_NBR);
	}

	public void setiNotPrcTs(String iNotPrcTs) {
		writeString(Pos.I_NOT_PRC_TS, iNotPrcTs, Len.I_NOT_PRC_TS);
	}

	/**Original name: XZT90I-NOT-PRC-TS<br>*/
	public String getiNotPrcTs() {
		return readString(Pos.I_NOT_PRC_TS, Len.I_NOT_PRC_TS);
	}

	public void setiActNotTypCd(String iActNotTypCd) {
		writeString(Pos.I_ACT_NOT_TYP_CD, iActNotTypCd, Len.I_ACT_NOT_TYP_CD);
	}

	/**Original name: XZT90I-ACT-NOT-TYP-CD<br>*/
	public String getiActNotTypCd() {
		return readString(Pos.I_ACT_NOT_TYP_CD, Len.I_ACT_NOT_TYP_CD);
	}

	public void setiUserid(String iUserid) {
		writeString(Pos.I_USERID, iUserid, Len.I_USERID);
	}

	/**Original name: XZT90I-USERID<br>*/
	public String getiUserid() {
		return readString(Pos.I_USERID, Len.I_USERID);
	}

	public String getiUseridFormatted() {
		return Functions.padBlanks(getiUserid(), Len.I_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT900_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int I_CSR_ACT_NBR = XZT900_SERVICE_INPUTS;
		public static final int I_NOT_PRC_TS = I_CSR_ACT_NBR + Len.I_CSR_ACT_NBR;
		public static final int I_ACT_NOT_TYP_CD = I_NOT_PRC_TS + Len.I_NOT_PRC_TS;
		public static final int I_USERID = I_ACT_NOT_TYP_CD + Len.I_ACT_NOT_TYP_CD;
		public static final int XZT900_SERVICE_OUTPUTS = I_USERID + Len.I_USERID;
		public static final int FLR1 = XZT900_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int I_CSR_ACT_NBR = 9;
		public static final int I_NOT_PRC_TS = 26;
		public static final int I_ACT_NOT_TYP_CD = 5;
		public static final int I_USERID = 8;
		public static final int XZT900_SERVICE_INPUTS = I_CSR_ACT_NBR + I_NOT_PRC_TS + I_ACT_NOT_TYP_CD + I_USERID;
		public static final int FLR1 = 1;
		public static final int XZT900_SERVICE_OUTPUTS = FLR1;
		public static final int L_SERVICE_CONTRACT_AREA = XZT900_SERVICE_INPUTS + XZT900_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
