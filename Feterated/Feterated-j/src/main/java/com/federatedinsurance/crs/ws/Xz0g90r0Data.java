/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicUsage;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0G90R0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0g90r0Data {

	//==== PROPERTIES ====
	//Original name: CF-INITIALIZE-GETMAIN
	private char cfInitializeGetmain = Types.SPACE_CHAR;
	//Original name: CF-SERVICE-PROXY-PGM
	private String cfServiceProxyPgm = "XZ0X90R0";
	//Original name: EA-01-CICS-LINK-ERROR
	private Ea01CicsLinkErrorXz0g90r0 ea01CicsLinkError = new Ea01CicsLinkErrorXz0g90r0();
	//Original name: EA-02-GETMAIN-FREEMAIN-ERROR
	private Ea02GetmainFreemainErrorXz0g90r0 ea02GetmainFreemainError = new Ea02GetmainFreemainErrorXz0g90r0();
	//Original name: WS-RESPONSE-CODE
	private int wsResponseCode = DefaultValues.BIN_INT_VAL;
	//Original name: WS-RESPONSE-CODE2
	private int wsResponseCode2 = DefaultValues.BIN_INT_VAL;
	//Original name: SERVICE-PROXY-CONTRACT
	private WsProxyProgramArea serviceProxyContract = new WsProxyProgramArea();

	//==== METHODS ====
	public char getCfInitializeGetmain() {
		return this.cfInitializeGetmain;
	}

	public String getCfServiceProxyPgm() {
		return this.cfServiceProxyPgm;
	}

	public void setWsResponseCode(int wsResponseCode) {
		this.wsResponseCode = wsResponseCode;
	}

	public int getWsResponseCode() {
		return this.wsResponseCode;
	}

	public void setWsResponseCode2(int wsResponseCode2) {
		this.wsResponseCode2 = wsResponseCode2;
	}

	public int getWsResponseCode2() {
		return this.wsResponseCode2;
	}

	public String getWsResponseCode2Formatted() {
		return PicFormatter.display(new PicParams("S9(8)").setUsage(PicUsage.BINARY)).format(getWsResponseCode2()).toString();
	}

	public Ea01CicsLinkErrorXz0g90r0 getEa01CicsLinkError() {
		return ea01CicsLinkError;
	}

	public Ea02GetmainFreemainErrorXz0g90r0 getEa02GetmainFreemainError() {
		return ea02GetmainFreemainError;
	}

	public WsProxyProgramArea getServiceProxyContract() {
		return serviceProxyContract;
	}
}
