/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.Xz0z0005;
import com.federatedinsurance.crs.copy.Xz0z0006;
import com.federatedinsurance.crs.copy.Xzt005ServiceOutputs;
import com.federatedinsurance.crs.copy.Xzt006ServiceInputs;
import com.federatedinsurance.crs.copy.Xzt006ServiceOutputs;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ003000<br>
 * Generated as a class for rule WS.<br>*/
public class Xz003000Data {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz003000 constantFields = new ConstantFieldsXz003000();
	//Original name: CICS-PIPE-INTERFACE-CONTRACT
	private CicsPipeInterfaceContract cicsPipeInterfaceContract = new CicsPipeInterfaceContract();
	//Original name: DATA-TO-PASS
	private DataToPass dataToPass = new DataToPass();
	//Original name: ERROR-AND-ADVICE-MESSAGES
	private ErrorAndAdviceMessagesXz003000 errorAndAdviceMessages = new ErrorAndAdviceMessagesXz003000();
	//Original name: PRINT-LINE
	private PrintLine printLine = new PrintLine();
	//Original name: FILLER-RP-PRTR
	private String flr1 = "    PRTR";
	//Original name: SAVE-AREA
	private SaveAreaXz003000 saveArea = new SaveAreaXz003000();
	//Original name: SW-ROW-FOUND-FLAG
	private boolean swRowFoundFlag = true;
	//Original name: XZ0Z0005
	private Xz0z0005 xz0z0005 = new Xz0z0005();
	//Original name: XZ0Z0006
	private Xz0z0006 xz0z0006 = new Xz0z0006();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();

	//==== METHODS ====
	/**Original name: RP-PRTR<br>*/
	public byte[] getRpPrtrBytes() {
		byte[] buffer = new byte[Len.RP_PRTR];
		return getRpPrtrBytes(buffer, 1);
	}

	public byte[] getRpPrtrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setSwRowFoundFlag(boolean swRowFoundFlag) {
		this.swRowFoundFlag = swRowFoundFlag;
	}

	public boolean isSwRowFoundFlag() {
		return this.swRowFoundFlag;
	}

	public void setGetActNotContractAreaBytes(byte[] buffer) {
		setGetActNotContractAreaBytes(buffer, 1);
	}

	/**Original name: GET-ACT-NOT-CONTRACT-AREA<br>*/
	public byte[] getGetActNotContractAreaBytes() {
		byte[] buffer = new byte[Len.GET_ACT_NOT_CONTRACT_AREA];
		return getGetActNotContractAreaBytes(buffer, 1);
	}

	public void setGetActNotContractAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0z0005.setXzt005ServiceInputsBytes(buffer, position);
		position += Xz0z0005.Len.XZT005_SERVICE_INPUTS;
		xz0z0005.getXzt005ServiceOutputs().setXzt005ServiceOutputsBytes(buffer, position);
		position += Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS;
		xz0z0005.setXzt005ServiceParametersBytes(buffer, position);
		position += Xz0z0005.Len.XZT005_SERVICE_PARAMETERS;
		xz0z0005.setXzt005ServiceErrorInfoBytes(buffer, position);
	}

	public byte[] getGetActNotContractAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0z0005.getXzt005ServiceInputsBytes(buffer, position);
		position += Xz0z0005.Len.XZT005_SERVICE_INPUTS;
		xz0z0005.getXzt005ServiceOutputs().getXzt005ServiceOutputsBytes(buffer, position);
		position += Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS;
		xz0z0005.getXzt005ServiceParametersBytes(buffer, position);
		position += Xz0z0005.Len.XZT005_SERVICE_PARAMETERS;
		xz0z0005.getXzt005ServiceErrorInfoBytes(buffer, position);
		return buffer;
	}

	public void setMtnActNotContractAreaBytes(byte[] buffer) {
		setMtnActNotContractAreaBytes(buffer, 1);
	}

	/**Original name: MTN-ACT-NOT-CONTRACT-AREA<br>*/
	public byte[] getMtnActNotContractAreaBytes() {
		byte[] buffer = new byte[Len.MTN_ACT_NOT_CONTRACT_AREA];
		return getMtnActNotContractAreaBytes(buffer, 1);
	}

	public void setMtnActNotContractAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0z0006.getServiceInputs().setServiceInputsBytes(buffer, position);
		position += Xzt006ServiceInputs.Len.SERVICE_INPUTS;
		xz0z0006.getServiceOutputs().setServiceOutputsBytes(buffer, position);
		position += Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS;
		xz0z0006.setServiceParametersBytes(buffer, position);
		position += Xz0z0006.Len.SERVICE_PARAMETERS;
		xz0z0006.setServiceErrorInfoBytes(buffer, position);
	}

	public byte[] getMtnActNotContractAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0z0006.getServiceInputs().getServiceInputsBytes(buffer, position);
		position += Xzt006ServiceInputs.Len.SERVICE_INPUTS;
		xz0z0006.getServiceOutputs().getServiceOutputsBytes(buffer, position);
		position += Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS;
		xz0z0006.getServiceParametersBytes(buffer, position);
		position += Xz0z0006.Len.SERVICE_PARAMETERS;
		xz0z0006.getServiceErrorInfoBytes(buffer, position);
		return buffer;
	}

	public CicsPipeInterfaceContract getCicsPipeInterfaceContract() {
		return cicsPipeInterfaceContract;
	}

	public ConstantFieldsXz003000 getConstantFields() {
		return constantFields;
	}

	public DataToPass getDataToPass() {
		return dataToPass;
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public ErrorAndAdviceMessagesXz003000 getErrorAndAdviceMessages() {
		return errorAndAdviceMessages;
	}

	public PrintLine getPrintLine() {
		return printLine;
	}

	public SaveAreaXz003000 getSaveArea() {
		return saveArea;
	}

	public Xz0z0005 getXz0z0005() {
		return xz0z0005;
	}

	public Xz0z0006 getXz0z0006() {
		return xz0z0006;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 12;
		public static final int RP_PRTR = FLR1;
		public static final int GET_ACT_NOT_CONTRACT_AREA = Xz0z0005.Len.XZT005_SERVICE_INPUTS + Xzt005ServiceOutputs.Len.XZT005_SERVICE_OUTPUTS
				+ Xz0z0005.Len.XZT005_SERVICE_PARAMETERS + Xz0z0005.Len.XZT005_SERVICE_ERROR_INFO;
		public static final int MTN_ACT_NOT_CONTRACT_AREA = Xzt006ServiceInputs.Len.SERVICE_INPUTS + Xzt006ServiceOutputs.Len.SERVICE_OUTPUTS
				+ Xz0z0006.Len.SERVICE_PARAMETERS + Xz0z0006.Len.SERVICE_ERROR_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
