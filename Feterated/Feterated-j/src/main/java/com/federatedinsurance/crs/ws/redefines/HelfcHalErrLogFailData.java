/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;
import com.bphx.ctu.af.util.Trunc;

/**Original name: HELFC-HAL-ERR-LOG-FAIL-DATA<br>
 * Variable: HELFC-HAL-ERR-LOG-FAIL-DATA from program HALOESTO<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class HelfcHalErrLogFailData extends BytesAllocatingClass {

	//==== CONSTRUCTORS ====
	public HelfcHalErrLogFailData() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.HELFC_HAL_ERR_LOG_FAIL_DATA;
	}

	public void setHelfcHalErrLogFailDataBytes(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.HELFC_HAL_ERR_LOG_FAIL_DATA, Pos.HELFC_HAL_ERR_LOG_FAIL_DATA);
	}

	public byte[] getHelfcHalErrLogFailDataBytes(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.HELFC_HAL_ERR_LOG_FAIL_DATA, Pos.HELFC_HAL_ERR_LOG_FAIL_DATA);
		return buffer;
	}

	public void initHelfcHalErrLogFailDataSpaces() {
		fill(Pos.HELFC_HAL_ERR_LOG_FAIL_DATA, Len.HELFC_HAL_ERR_LOG_FAIL_DATA, Types.SPACE_CHAR);
	}

	public void setHelfcHelfFailAppNm(String helfcHelfFailAppNm) {
		writeString(Pos.HELFC_HELF_FAIL_APP_NM, helfcHelfFailAppNm, Len.HELFC_HELF_FAIL_APP_NM);
	}

	/**Original name: HELFC-HELF-FAIL-APP-NM<br>*/
	public String getHelfcHelfFailAppNm() {
		return readString(Pos.HELFC_HELF_FAIL_APP_NM, Len.HELFC_HELF_FAIL_APP_NM);
	}

	public void setHelfcHelfFailPlf(String helfcHelfFailPlf) {
		writeString(Pos.HELFC_HELF_FAIL_PLF, helfcHelfFailPlf, Len.HELFC_HELF_FAIL_PLF);
	}

	/**Original name: HELFC-HELF-FAIL-PLF<br>*/
	public String getHelfcHelfFailPlf() {
		return readString(Pos.HELFC_HELF_FAIL_PLF, Len.HELFC_HELF_FAIL_PLF);
	}

	public void setHelfcHelfFailTypeCd(char helfcHelfFailTypeCd) {
		writeChar(Pos.HELFC_HELF_FAIL_TYPE_CD, helfcHelfFailTypeCd);
	}

	/**Original name: HELFC-HELF-FAIL-TYPE-CD<br>*/
	public char getHelfcHelfFailTypeCd() {
		return readChar(Pos.HELFC_HELF_FAIL_TYPE_CD);
	}

	public void setHelfcFailAcyCd(String helfcFailAcyCd) {
		writeString(Pos.HELFC_FAIL_ACY_CD, helfcFailAcyCd, Len.HELFC_FAIL_ACY_CD);
	}

	/**Original name: HELFC-FAIL-ACY-CD<br>*/
	public String getHelfcFailAcyCd() {
		return readString(Pos.HELFC_FAIL_ACY_CD, Len.HELFC_FAIL_ACY_CD);
	}

	public void setHelfcHelfFailPgmNm(String helfcHelfFailPgmNm) {
		writeString(Pos.HELFC_HELF_FAIL_PGM_NM, helfcHelfFailPgmNm, Len.HELFC_HELF_FAIL_PGM_NM);
	}

	/**Original name: HELFC-HELF-FAIL-PGM-NM<br>*/
	public String getHelfcHelfFailPgmNm() {
		return readString(Pos.HELFC_HELF_FAIL_PGM_NM, Len.HELFC_HELF_FAIL_PGM_NM);
	}

	public void setHelfcHelfFailParaNm(String helfcHelfFailParaNm) {
		writeString(Pos.HELFC_HELF_FAIL_PARA_NM, helfcHelfFailParaNm, Len.HELFC_HELF_FAIL_PARA_NM);
	}

	/**Original name: HELFC-HELF-FAIL-PARA-NM<br>*/
	public String getHelfcHelfFailParaNm() {
		return readString(Pos.HELFC_HELF_FAIL_PARA_NM, Len.HELFC_HELF_FAIL_PARA_NM);
	}

	public void setHelfcHelfErrAddTxt(String helfcHelfErrAddTxt) {
		writeString(Pos.HELFC_HELF_ERR_ADD_TXT, helfcHelfErrAddTxt, Len.HELFC_HELF_ERR_ADD_TXT);
	}

	/**Original name: HELFC-HELF-ERR-ADD-TXT<br>*/
	public String getHelfcHelfErrAddTxt() {
		return readString(Pos.HELFC_HELF_ERR_ADD_TXT, Len.HELFC_HELF_ERR_ADD_TXT);
	}

	public void setHelfcSqlCodeNi(char helfcSqlCodeNi) {
		writeChar(Pos.HELFC_SQL_CODE_NI, helfcSqlCodeNi);
	}

	/**Original name: HELFC-SQL-CODE-NI<br>*/
	public char getHelfcSqlCodeNi() {
		return readChar(Pos.HELFC_SQL_CODE_NI);
	}

	public void setHelfcSqlCodeSign(char helfcSqlCodeSign) {
		writeChar(Pos.HELFC_SQL_CODE_SIGN, helfcSqlCodeSign);
	}

	/**Original name: HELFC-SQL-CODE-SIGN<br>*/
	public char getHelfcSqlCodeSign() {
		return readChar(Pos.HELFC_SQL_CODE_SIGN);
	}

	public void setHelfcSqlCodeFormatted(String helfcSqlCode) {
		writeString(Pos.HELFC_SQL_CODE, Trunc.toUnsignedNumeric(helfcSqlCode, Len.HELFC_SQL_CODE), Len.HELFC_SQL_CODE);
	}

	/**Original name: HELFC-SQL-CODE<br>*/
	public long getHelfcSqlCode() {
		return readNumDispUnsignedLong(Pos.HELFC_SQL_CODE, Len.HELFC_SQL_CODE);
	}

	public void setHelfcHelfCicsrspCdSign(char helfcHelfCicsrspCdSign) {
		writeChar(Pos.HELFC_HELF_CICSRSP_CD_SIGN, helfcHelfCicsrspCdSign);
	}

	/**Original name: HELFC-HELF-CICSRSP-CD-SIGN<br>*/
	public char getHelfcHelfCicsrspCdSign() {
		return readChar(Pos.HELFC_HELF_CICSRSP_CD_SIGN);
	}

	public void setHelfcHelfCicsrspCdFormatted(String helfcHelfCicsrspCd) {
		writeString(Pos.HELFC_HELF_CICSRSP_CD, Trunc.toUnsignedNumeric(helfcHelfCicsrspCd, Len.HELFC_HELF_CICSRSP_CD), Len.HELFC_HELF_CICSRSP_CD);
	}

	/**Original name: HELFC-HELF-CICSRSP-CD<br>*/
	public long getHelfcHelfCicsrspCd() {
		return readNumDispUnsignedLong(Pos.HELFC_HELF_CICSRSP_CD, Len.HELFC_HELF_CICSRSP_CD);
	}

	public void setHelfcHelfErrCmtTxt(String helfcHelfErrCmtTxt) {
		writeString(Pos.HELFC_HELF_ERR_CMT_TXT, helfcHelfErrCmtTxt, Len.HELFC_HELF_ERR_CMT_TXT);
	}

	/**Original name: HELFC-HELF-ERR-CMT-TXT<br>*/
	public String getHelfcHelfErrCmtTxt() {
		return readString(Pos.HELFC_HELF_ERR_CMT_TXT, Len.HELFC_HELF_ERR_CMT_TXT);
	}

	public void setHelfcHelfFailUowNm(String helfcHelfFailUowNm) {
		writeString(Pos.HELFC_HELF_FAIL_UOW_NM, helfcHelfFailUowNm, Len.HELFC_HELF_FAIL_UOW_NM);
	}

	/**Original name: HELFC-HELF-FAIL-UOW-NM<br>*/
	public String getHelfcHelfFailUowNm() {
		return readString(Pos.HELFC_HELF_FAIL_UOW_NM, Len.HELFC_HELF_FAIL_UOW_NM);
	}

	public void setHelfcHelfFailLocNm(String helfcHelfFailLocNm) {
		writeString(Pos.HELFC_HELF_FAIL_LOC_NM, helfcHelfFailLocNm, Len.HELFC_HELF_FAIL_LOC_NM);
	}

	/**Original name: HELFC-HELF-FAIL-LOC-NM<br>*/
	public String getHelfcHelfFailLocNm() {
		return readString(Pos.HELFC_HELF_FAIL_LOC_NM, Len.HELFC_HELF_FAIL_LOC_NM);
	}

	public void setHelfcHelfSqlerrmcTxt(String helfcHelfSqlerrmcTxt) {
		writeString(Pos.HELFC_HELF_SQLERRMC_TXT, helfcHelfSqlerrmcTxt, Len.HELFC_HELF_SQLERRMC_TXT);
	}

	/**Original name: HELFC-HELF-SQLERRMC-TXT<br>*/
	public String getHelfcHelfSqlerrmcTxt() {
		return readString(Pos.HELFC_HELF_SQLERRMC_TXT, Len.HELFC_HELF_SQLERRMC_TXT);
	}

	public void setHelfcHelfCicsrsp2CdSign(char helfcHelfCicsrsp2CdSign) {
		writeChar(Pos.HELFC_HELF_CICSRSP2_CD_SIGN, helfcHelfCicsrsp2CdSign);
	}

	/**Original name: HELFC-HELF-CICSRSP2-CD-SIGN<br>*/
	public char getHelfcHelfCicsrsp2CdSign() {
		return readChar(Pos.HELFC_HELF_CICSRSP2_CD_SIGN);
	}

	public void setHelfcHelfCicsrsp2CdFormatted(String helfcHelfCicsrsp2Cd) {
		writeString(Pos.HELFC_HELF_CICSRSP2_CD, Trunc.toUnsignedNumeric(helfcHelfCicsrsp2Cd, Len.HELFC_HELF_CICSRSP2_CD), Len.HELFC_HELF_CICSRSP2_CD);
	}

	/**Original name: HELFC-HELF-CICSRSP2-CD<br>*/
	public long getHelfcHelfCicsrsp2Cd() {
		return readNumDispUnsignedLong(Pos.HELFC_HELF_CICSRSP2_CD, Len.HELFC_HELF_CICSRSP2_CD);
	}

	public void setHelfcUserid(String helfcUserid) {
		writeString(Pos.HELFC_USERID, helfcUserid, Len.HELFC_USERID);
	}

	/**Original name: HELFC-USERID<br>*/
	public String getHelfcUserid() {
		return readString(Pos.HELFC_USERID, Len.HELFC_USERID);
	}

	public void setHelfcSecSysIdSign(char helfcSecSysIdSign) {
		writeChar(Pos.HELFC_SEC_SYS_ID_SIGN, helfcSecSysIdSign);
	}

	/**Original name: HELFC-SEC-SYS-ID-SIGN<br>*/
	public char getHelfcSecSysIdSign() {
		return readChar(Pos.HELFC_SEC_SYS_ID_SIGN);
	}

	public void setHelfcSecSysIdFormatted(String helfcSecSysId) {
		writeString(Pos.HELFC_SEC_SYS_ID, Trunc.toUnsignedNumeric(helfcSecSysId, Len.HELFC_SEC_SYS_ID), Len.HELFC_SEC_SYS_ID);
	}

	/**Original name: HELFC-SEC-SYS-ID<br>*/
	public long getHelfcSecSysId() {
		return readNumDispUnsignedLong(Pos.HELFC_SEC_SYS_ID, Len.HELFC_SEC_SYS_ID);
	}

	public void setHelfcHelfFailKeyTxt(String helfcHelfFailKeyTxt) {
		writeString(Pos.HELFC_HELF_FAIL_KEY_TXT, helfcHelfFailKeyTxt, Len.HELFC_HELF_FAIL_KEY_TXT);
	}

	/**Original name: HELFC-HELF-FAIL-KEY-TXT<br>*/
	public String getHelfcHelfFailKeyTxt() {
		return readString(Pos.HELFC_HELF_FAIL_KEY_TXT, Len.HELFC_HELF_FAIL_KEY_TXT);
	}

	public void setHelfcHelfAcyTxt(String helfcHelfAcyTxt) {
		writeString(Pos.HELFC_HELF_ACY_TXT, helfcHelfAcyTxt, Len.HELFC_HELF_ACY_TXT);
	}

	/**Original name: HELFC-HELF-ACY-TXT<br>*/
	public String getHelfcHelfAcyTxt() {
		return readString(Pos.HELFC_HELF_ACY_TXT, Len.HELFC_HELF_ACY_TXT);
	}

	public void setHelfcHelfPriorityCdSign(char helfcHelfPriorityCdSign) {
		writeChar(Pos.HELFC_HELF_PRIORITY_CD_SIGN, helfcHelfPriorityCdSign);
	}

	/**Original name: HELFC-HELF-PRIORITY-CD-SIGN<br>*/
	public char getHelfcHelfPriorityCdSign() {
		return readChar(Pos.HELFC_HELF_PRIORITY_CD_SIGN);
	}

	public void setHelfcHelfPriorityCdFormatted(String helfcHelfPriorityCd) {
		writeString(Pos.HELFC_HELF_PRIORITY_CD, Trunc.toUnsignedNumeric(helfcHelfPriorityCd, Len.HELFC_HELF_PRIORITY_CD), Len.HELFC_HELF_PRIORITY_CD);
	}

	/**Original name: HELFC-HELF-PRIORITY-CD<br>*/
	public int getHelfcHelfPriorityCd() {
		return readNumDispUnsignedInt(Pos.HELFC_HELF_PRIORITY_CD, Len.HELFC_HELF_PRIORITY_CD);
	}

	public String getHelfcHelfPriorityCdFormatted() {
		return readFixedString(Pos.HELFC_HELF_PRIORITY_CD, Len.HELFC_HELF_PRIORITY_CD);
	}

	public void setHelfcHelfErrRfrNbr(String helfcHelfErrRfrNbr) {
		writeString(Pos.HELFC_HELF_ERR_RFR_NBR, helfcHelfErrRfrNbr, Len.HELFC_HELF_ERR_RFR_NBR);
	}

	/**Original name: HELFC-HELF-ERR-RFR-NBR<br>*/
	public String getHelfcHelfErrRfrNbr() {
		return readString(Pos.HELFC_HELF_ERR_RFR_NBR, Len.HELFC_HELF_ERR_RFR_NBR);
	}

	public void setHelfcHelfErrTxt(String helfcHelfErrTxt) {
		writeString(Pos.HELFC_HELF_ERR_TXT, helfcHelfErrTxt, Len.HELFC_HELF_ERR_TXT);
	}

	/**Original name: HELFC-HELF-ERR-TXT<br>*/
	public String getHelfcHelfErrTxt() {
		return readString(Pos.HELFC_HELF_ERR_TXT, Len.HELFC_HELF_ERR_TXT);
	}

	public void setWsHalErrLogFailAltkeyFromBuffer(byte[] buffer, int offset) {
		setBytes(buffer, offset, Len.WS_HAL_ERR_LOG_FAIL_ALTKEY, Pos.WS_HAL_ERR_LOG_FAIL_ALTKEY);
	}

	public void setWsHalErrLogFailAltkeyFromBuffer(byte[] buffer) {
		setWsHalErrLogFailAltkeyFromBuffer(buffer, 1);
	}

	public byte[] getWsHalErrLogFailAltkeyAsBuffer(byte[] buffer, int offset) {
		getBytes(buffer, offset, Len.WS_HAL_ERR_LOG_FAIL_ALTKEY, Pos.WS_HAL_ERR_LOG_FAIL_ALTKEY);
		return buffer;
	}

	public byte[] getWsHalErrLogFailAltkeyAsBuffer() {
		byte[] buffer = new byte[Len.WS_HAL_ERR_LOG_FAIL_ALTKEY];
		return getWsHalErrLogFailAltkeyAsBuffer(buffer, 1);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int HELFC_HAL_ERR_LOG_FAIL_DATA = 1;
		public static final int HELFC_HELF_FAIL_APP_NM_CI = HELFC_HAL_ERR_LOG_FAIL_DATA;
		public static final int HELFC_HELF_FAIL_APP_NM = HELFC_HELF_FAIL_APP_NM_CI + Len.HELFC_HELF_FAIL_APP_NM_CI;
		public static final int HELFC_HELF_FAIL_PLF_CI = HELFC_HELF_FAIL_APP_NM + Len.HELFC_HELF_FAIL_APP_NM;
		public static final int HELFC_HELF_FAIL_PLF = HELFC_HELF_FAIL_PLF_CI + Len.HELFC_HELF_FAIL_PLF_CI;
		public static final int HELFC_HELF_FAIL_TYPE_CD_CI = HELFC_HELF_FAIL_PLF + Len.HELFC_HELF_FAIL_PLF;
		public static final int HELFC_HELF_FAIL_TYPE_CD = HELFC_HELF_FAIL_TYPE_CD_CI + Len.HELFC_HELF_FAIL_TYPE_CD_CI;
		public static final int HELFC_FAIL_ACY_CD_CI = HELFC_HELF_FAIL_TYPE_CD + Len.HELFC_HELF_FAIL_TYPE_CD;
		public static final int HELFC_FAIL_ACY_CD = HELFC_FAIL_ACY_CD_CI + Len.HELFC_FAIL_ACY_CD_CI;
		public static final int HELFC_HELF_FAIL_PGM_NM_CI = HELFC_FAIL_ACY_CD + Len.HELFC_FAIL_ACY_CD;
		public static final int HELFC_HELF_FAIL_PGM_NM = HELFC_HELF_FAIL_PGM_NM_CI + Len.HELFC_HELF_FAIL_PGM_NM_CI;
		public static final int HELFC_HELF_FAIL_PARA_NM_CI = HELFC_HELF_FAIL_PGM_NM + Len.HELFC_HELF_FAIL_PGM_NM;
		public static final int HELFC_HELF_FAIL_PARA_NM = HELFC_HELF_FAIL_PARA_NM_CI + Len.HELFC_HELF_FAIL_PARA_NM_CI;
		public static final int HELFC_HELF_ERR_ADD_TXT_CI = HELFC_HELF_FAIL_PARA_NM + Len.HELFC_HELF_FAIL_PARA_NM;
		public static final int HELFC_HELF_ERR_ADD_TXT = HELFC_HELF_ERR_ADD_TXT_CI + Len.HELFC_HELF_ERR_ADD_TXT_CI;
		public static final int HELFC_SQL_CODE_CI = HELFC_HELF_ERR_ADD_TXT + Len.HELFC_HELF_ERR_ADD_TXT;
		public static final int HELFC_SQL_CODE_NI = HELFC_SQL_CODE_CI + Len.HELFC_SQL_CODE_CI;
		public static final int HELFC_SQL_CODE_SIGN = HELFC_SQL_CODE_NI + Len.HELFC_SQL_CODE_NI;
		public static final int HELFC_SQL_CODE = HELFC_SQL_CODE_SIGN + Len.HELFC_SQL_CODE_SIGN;
		public static final int HELFC_HELF_CICSRSP_CD_CI = HELFC_SQL_CODE + Len.HELFC_SQL_CODE;
		public static final int HELFC_HELF_CICSRSP_CD_SIGN = HELFC_HELF_CICSRSP_CD_CI + Len.HELFC_HELF_CICSRSP_CD_CI;
		public static final int HELFC_HELF_CICSRSP_CD = HELFC_HELF_CICSRSP_CD_SIGN + Len.HELFC_HELF_CICSRSP_CD_SIGN;
		public static final int HELFC_HELF_ERR_CMT_TXT_CI = HELFC_HELF_CICSRSP_CD + Len.HELFC_HELF_CICSRSP_CD;
		public static final int HELFC_HELF_ERR_CMT_TXT = HELFC_HELF_ERR_CMT_TXT_CI + Len.HELFC_HELF_ERR_CMT_TXT_CI;
		public static final int HELFC_HELF_FAIL_UOW_NM_CI = HELFC_HELF_ERR_CMT_TXT + Len.HELFC_HELF_ERR_CMT_TXT;
		public static final int HELFC_HELF_FAIL_UOW_NM = HELFC_HELF_FAIL_UOW_NM_CI + Len.HELFC_HELF_FAIL_UOW_NM_CI;
		public static final int HELFC_HELF_FAIL_LOC_NM_CI = HELFC_HELF_FAIL_UOW_NM + Len.HELFC_HELF_FAIL_UOW_NM;
		public static final int HELFC_HELF_FAIL_LOC_NM = HELFC_HELF_FAIL_LOC_NM_CI + Len.HELFC_HELF_FAIL_LOC_NM_CI;
		public static final int HELFC_HELF_SQLERRMC_TXT_CI = HELFC_HELF_FAIL_LOC_NM + Len.HELFC_HELF_FAIL_LOC_NM;
		public static final int HELFC_HELF_SQLERRMC_TXT = HELFC_HELF_SQLERRMC_TXT_CI + Len.HELFC_HELF_SQLERRMC_TXT_CI;
		public static final int HELFC_HELF_CICSRSP2_CD_CI = HELFC_HELF_SQLERRMC_TXT + Len.HELFC_HELF_SQLERRMC_TXT;
		public static final int HELFC_HELF_CICSRSP2_CD_SIGN = HELFC_HELF_CICSRSP2_CD_CI + Len.HELFC_HELF_CICSRSP2_CD_CI;
		public static final int HELFC_HELF_CICSRSP2_CD = HELFC_HELF_CICSRSP2_CD_SIGN + Len.HELFC_HELF_CICSRSP2_CD_SIGN;
		public static final int HELFC_USERID_CI = HELFC_HELF_CICSRSP2_CD + Len.HELFC_HELF_CICSRSP2_CD;
		public static final int HELFC_USERID = HELFC_USERID_CI + Len.HELFC_USERID_CI;
		public static final int HELFC_SEC_SYS_ID_CI = HELFC_USERID + Len.HELFC_USERID;
		public static final int HELFC_SEC_SYS_ID_SIGN = HELFC_SEC_SYS_ID_CI + Len.HELFC_SEC_SYS_ID_CI;
		public static final int HELFC_SEC_SYS_ID = HELFC_SEC_SYS_ID_SIGN + Len.HELFC_SEC_SYS_ID_SIGN;
		public static final int HELFC_HELF_FAIL_KEY_TXT_CI = HELFC_SEC_SYS_ID + Len.HELFC_SEC_SYS_ID;
		public static final int HELFC_HELF_FAIL_KEY_TXT = HELFC_HELF_FAIL_KEY_TXT_CI + Len.HELFC_HELF_FAIL_KEY_TXT_CI;
		public static final int HELFC_HELF_ACY_TXT_CI = HELFC_HELF_FAIL_KEY_TXT + Len.HELFC_HELF_FAIL_KEY_TXT;
		public static final int HELFC_HELF_ACY_TXT = HELFC_HELF_ACY_TXT_CI + Len.HELFC_HELF_ACY_TXT_CI;
		public static final int HELFC_HELF_PRIORITY_CD_CI = HELFC_HELF_ACY_TXT + Len.HELFC_HELF_ACY_TXT;
		public static final int HELFC_HELF_PRIORITY_CD_SIGN = HELFC_HELF_PRIORITY_CD_CI + Len.HELFC_HELF_PRIORITY_CD_CI;
		public static final int HELFC_HELF_PRIORITY_CD = HELFC_HELF_PRIORITY_CD_SIGN + Len.HELFC_HELF_PRIORITY_CD_SIGN;
		public static final int HELFC_HELF_ERR_RFR_NBR_CI = HELFC_HELF_PRIORITY_CD + Len.HELFC_HELF_PRIORITY_CD;
		public static final int HELFC_HELF_ERR_RFR_NBR = HELFC_HELF_ERR_RFR_NBR_CI + Len.HELFC_HELF_ERR_RFR_NBR_CI;
		public static final int HELFC_HELF_ERR_TXT_CI = HELFC_HELF_ERR_RFR_NBR + Len.HELFC_HELF_ERR_RFR_NBR;
		public static final int HELFC_HELF_ERR_TXT = HELFC_HELF_ERR_TXT_CI + Len.HELFC_HELF_ERR_TXT_CI;
		public static final int WS_HAL_ERR_LOG_FAIL_ALT_DAT = 1;
		public static final int WS_HAL_ERR_LOG_FAIL_ALTKEY = WS_HAL_ERR_LOG_FAIL_ALT_DAT;
		public static final int FLR1 = WS_HAL_ERR_LOG_FAIL_ALTKEY + Len.WS_HAL_ERR_LOG_FAIL_ALTKEY;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int HELFC_HELF_FAIL_APP_NM_CI = 1;
		public static final int HELFC_HELF_FAIL_APP_NM = 10;
		public static final int HELFC_HELF_FAIL_PLF_CI = 1;
		public static final int HELFC_HELF_FAIL_PLF = 10;
		public static final int HELFC_HELF_FAIL_TYPE_CD_CI = 1;
		public static final int HELFC_HELF_FAIL_TYPE_CD = 1;
		public static final int HELFC_FAIL_ACY_CD_CI = 1;
		public static final int HELFC_FAIL_ACY_CD = 8;
		public static final int HELFC_HELF_FAIL_PGM_NM_CI = 1;
		public static final int HELFC_HELF_FAIL_PGM_NM = 32;
		public static final int HELFC_HELF_FAIL_PARA_NM_CI = 1;
		public static final int HELFC_HELF_FAIL_PARA_NM = 30;
		public static final int HELFC_HELF_ERR_ADD_TXT_CI = 1;
		public static final int HELFC_HELF_ERR_ADD_TXT = 32;
		public static final int HELFC_SQL_CODE_CI = 1;
		public static final int HELFC_SQL_CODE_NI = 1;
		public static final int HELFC_SQL_CODE_SIGN = 1;
		public static final int HELFC_SQL_CODE = 10;
		public static final int HELFC_HELF_CICSRSP_CD_CI = 1;
		public static final int HELFC_HELF_CICSRSP_CD_SIGN = 1;
		public static final int HELFC_HELF_CICSRSP_CD = 10;
		public static final int HELFC_HELF_ERR_CMT_TXT_CI = 1;
		public static final int HELFC_HELF_ERR_CMT_TXT = 50;
		public static final int HELFC_HELF_FAIL_UOW_NM_CI = 1;
		public static final int HELFC_HELF_FAIL_UOW_NM = 32;
		public static final int HELFC_HELF_FAIL_LOC_NM_CI = 1;
		public static final int HELFC_HELF_FAIL_LOC_NM = 32;
		public static final int HELFC_HELF_SQLERRMC_TXT_CI = 1;
		public static final int HELFC_HELF_SQLERRMC_TXT = 70;
		public static final int HELFC_HELF_CICSRSP2_CD_CI = 1;
		public static final int HELFC_HELF_CICSRSP2_CD_SIGN = 1;
		public static final int HELFC_HELF_CICSRSP2_CD = 10;
		public static final int HELFC_USERID_CI = 1;
		public static final int HELFC_USERID = 32;
		public static final int HELFC_SEC_SYS_ID_CI = 1;
		public static final int HELFC_SEC_SYS_ID_SIGN = 1;
		public static final int HELFC_SEC_SYS_ID = 10;
		public static final int HELFC_HELF_FAIL_KEY_TXT_CI = 1;
		public static final int HELFC_HELF_FAIL_KEY_TXT = 200;
		public static final int HELFC_HELF_ACY_TXT_CI = 1;
		public static final int HELFC_HELF_ACY_TXT = 250;
		public static final int HELFC_HELF_PRIORITY_CD_CI = 1;
		public static final int HELFC_HELF_PRIORITY_CD_SIGN = 1;
		public static final int HELFC_HELF_PRIORITY_CD = 5;
		public static final int HELFC_HELF_ERR_RFR_NBR_CI = 1;
		public static final int HELFC_HELF_ERR_RFR_NBR = 10;
		public static final int HELFC_HELF_ERR_TXT_CI = 1;
		public static final int WS_HAL_ERR_LOG_FAIL_ALTKEY = 239;
		public static final int HELFC_HELF_ERR_TXT = 100;
		public static final int HELFC_HAL_ERR_LOG_FAIL_DATA = HELFC_HELF_FAIL_APP_NM_CI + HELFC_HELF_FAIL_APP_NM + HELFC_HELF_FAIL_PLF_CI
				+ HELFC_HELF_FAIL_PLF + HELFC_HELF_FAIL_TYPE_CD_CI + HELFC_HELF_FAIL_TYPE_CD + HELFC_FAIL_ACY_CD_CI + HELFC_FAIL_ACY_CD
				+ HELFC_HELF_FAIL_PGM_NM_CI + HELFC_HELF_FAIL_PGM_NM + HELFC_HELF_FAIL_PARA_NM_CI + HELFC_HELF_FAIL_PARA_NM
				+ HELFC_HELF_ERR_ADD_TXT_CI + HELFC_HELF_ERR_ADD_TXT + HELFC_SQL_CODE_CI + HELFC_SQL_CODE_NI + HELFC_SQL_CODE_SIGN + HELFC_SQL_CODE
				+ HELFC_HELF_CICSRSP_CD_CI + HELFC_HELF_CICSRSP_CD_SIGN + HELFC_HELF_CICSRSP_CD + HELFC_HELF_ERR_CMT_TXT_CI + HELFC_HELF_ERR_CMT_TXT
				+ HELFC_HELF_FAIL_UOW_NM_CI + HELFC_HELF_FAIL_UOW_NM + HELFC_HELF_FAIL_LOC_NM_CI + HELFC_HELF_FAIL_LOC_NM + HELFC_HELF_SQLERRMC_TXT_CI
				+ HELFC_HELF_SQLERRMC_TXT + HELFC_HELF_CICSRSP2_CD_CI + HELFC_HELF_CICSRSP2_CD_SIGN + HELFC_HELF_CICSRSP2_CD + HELFC_USERID_CI
				+ HELFC_USERID + HELFC_SEC_SYS_ID_CI + HELFC_SEC_SYS_ID_SIGN + HELFC_SEC_SYS_ID + HELFC_HELF_FAIL_KEY_TXT_CI + HELFC_HELF_FAIL_KEY_TXT
				+ HELFC_HELF_ACY_TXT_CI + HELFC_HELF_ACY_TXT + HELFC_HELF_PRIORITY_CD_CI + HELFC_HELF_PRIORITY_CD_SIGN + HELFC_HELF_PRIORITY_CD
				+ HELFC_HELF_ERR_RFR_NBR_CI + HELFC_HELF_ERR_RFR_NBR + HELFC_HELF_ERR_TXT_CI + HELFC_HELF_ERR_TXT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
