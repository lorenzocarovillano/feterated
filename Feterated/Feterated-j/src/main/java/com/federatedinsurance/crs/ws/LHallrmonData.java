/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HalrmonFunction;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: L-HALLRMON-DATA<br>
 * Variable: L-HALLRMON-DATA from program HALRMON<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class LHallrmonData extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRMON-FUNCTION
	private HalrmonFunction function = new HalrmonFunction();
	//Original name: HALRMON-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);
	//Original name: HALRMON-BUS-MOD-NM
	private String busModNm = DefaultValues.stringVal(Len.BUS_MOD_NM);
	//Original name: HALRMON-INFO-LABEL
	private String infoLabel = DefaultValues.stringVal(Len.INFO_LABEL);
	//Original name: HALRMON-INFO-LENGTH
	private short infoLength = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_HALLRMON_DATA;
	}

	@Override
	public void deserialize(byte[] buf) {
		setlHallrmonDataBytes(buf);
	}

	public void setlHallrmonDataBytes(byte[] buffer) {
		setlHallrmonDataBytes(buffer, 1);
	}

	public byte[] getlHallrmonDataBytes() {
		byte[] buffer = new byte[Len.L_HALLRMON_DATA];
		return getlHallrmonDataBytes(buffer, 1);
	}

	public void setlHallrmonDataBytes(byte[] buffer, int offset) {
		int position = offset;
		setInputLinkageBytes(buffer, position);
	}

	public byte[] getlHallrmonDataBytes(byte[] buffer, int offset) {
		int position = offset;
		getInputLinkageBytes(buffer, position);
		return buffer;
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		busModNm = MarshalByte.readString(buffer, position, Len.BUS_MOD_NM);
		position += Len.BUS_MOD_NM;
		infoLabel = MarshalByte.readString(buffer, position, Len.INFO_LABEL);
		position += Len.INFO_LABEL;
		infoLength = MarshalByte.readBinaryShort(buffer, position);
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, function.getFunction());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		position += Len.BUS_OBJ_NM;
		MarshalByte.writeString(buffer, position, busModNm, Len.BUS_MOD_NM);
		position += Len.BUS_MOD_NM;
		MarshalByte.writeString(buffer, position, infoLabel, Len.INFO_LABEL);
		position += Len.INFO_LABEL;
		MarshalByte.writeBinaryShort(buffer, position, infoLength);
		return buffer;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public void setBusModNm(String busModNm) {
		this.busModNm = Functions.subString(busModNm, Len.BUS_MOD_NM);
	}

	public String getBusModNm() {
		return this.busModNm;
	}

	public void setInfoLabel(String infoLabel) {
		this.infoLabel = Functions.subString(infoLabel, Len.INFO_LABEL);
	}

	public String getInfoLabel() {
		return this.infoLabel;
	}

	public void setInfoLength(short infoLength) {
		this.infoLength = infoLength;
	}

	public short getInfoLength() {
		return this.infoLength;
	}

	@Override
	public byte[] serialize() {
		return getlHallrmonDataBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BUS_OBJ_NM = 32;
		public static final int BUS_MOD_NM = 32;
		public static final int INFO_LABEL = 32;
		public static final int INFO_LENGTH = 2;
		public static final int INPUT_LINKAGE = HalrmonFunction.Len.FUNCTION + BUS_OBJ_NM + BUS_MOD_NM + INFO_LABEL + INFO_LENGTH;
		public static final int L_HALLRMON_DATA = INPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
