/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: FNC02O-OUTPUT-MATCH-CODES<br>
 * Variable: FNC02O-OUTPUT-MATCH-CODES from copybook FNC020C1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Fnc02oOutputMatchCodes {

	//==== PROPERTIES ====
	//Original name: FNC02O-MCH-CD-50
	private String cd50 = DefaultValues.stringVal(Len.CD50);
	//Original name: FNC02O-MCH-CD-55
	private String cd55 = DefaultValues.stringVal(Len.CD55);
	//Original name: FNC02O-MCH-CD-60
	private String cd60 = DefaultValues.stringVal(Len.CD60);
	//Original name: FNC02O-MCH-CD-65
	private String cd65 = DefaultValues.stringVal(Len.CD65);
	//Original name: FNC02O-MCH-CD-70
	private String cd70 = DefaultValues.stringVal(Len.CD70);
	//Original name: FNC02O-MCH-CD-75
	private String cd75 = DefaultValues.stringVal(Len.CD75);
	//Original name: FNC02O-MCH-CD-80
	private String cd80 = DefaultValues.stringVal(Len.CD80);
	//Original name: FNC02O-MCH-CD-85
	private String cd85 = DefaultValues.stringVal(Len.CD85);
	//Original name: FNC02O-MCH-CD-90
	private String cd90 = DefaultValues.stringVal(Len.CD90);
	//Original name: FNC02O-MCH-CD-95
	private String cd95 = DefaultValues.stringVal(Len.CD95);

	//==== METHODS ====
	public void setMatchCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		cd50 = MarshalByte.readString(buffer, position, Len.CD50);
		position += Len.CD50;
		cd55 = MarshalByte.readString(buffer, position, Len.CD55);
		position += Len.CD55;
		cd60 = MarshalByte.readString(buffer, position, Len.CD60);
		position += Len.CD60;
		cd65 = MarshalByte.readString(buffer, position, Len.CD65);
		position += Len.CD65;
		cd70 = MarshalByte.readString(buffer, position, Len.CD70);
		position += Len.CD70;
		cd75 = MarshalByte.readString(buffer, position, Len.CD75);
		position += Len.CD75;
		cd80 = MarshalByte.readString(buffer, position, Len.CD80);
		position += Len.CD80;
		cd85 = MarshalByte.readString(buffer, position, Len.CD85);
		position += Len.CD85;
		cd90 = MarshalByte.readString(buffer, position, Len.CD90);
		position += Len.CD90;
		cd95 = MarshalByte.readString(buffer, position, Len.CD95);
	}

	public byte[] getMatchCodesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cd50, Len.CD50);
		position += Len.CD50;
		MarshalByte.writeString(buffer, position, cd55, Len.CD55);
		position += Len.CD55;
		MarshalByte.writeString(buffer, position, cd60, Len.CD60);
		position += Len.CD60;
		MarshalByte.writeString(buffer, position, cd65, Len.CD65);
		position += Len.CD65;
		MarshalByte.writeString(buffer, position, cd70, Len.CD70);
		position += Len.CD70;
		MarshalByte.writeString(buffer, position, cd75, Len.CD75);
		position += Len.CD75;
		MarshalByte.writeString(buffer, position, cd80, Len.CD80);
		position += Len.CD80;
		MarshalByte.writeString(buffer, position, cd85, Len.CD85);
		position += Len.CD85;
		MarshalByte.writeString(buffer, position, cd90, Len.CD90);
		position += Len.CD90;
		MarshalByte.writeString(buffer, position, cd95, Len.CD95);
		return buffer;
	}

	public void initMatchCodesSpaces() {
		cd50 = "";
		cd55 = "";
		cd60 = "";
		cd65 = "";
		cd70 = "";
		cd75 = "";
		cd80 = "";
		cd85 = "";
		cd90 = "";
		cd95 = "";
	}

	public void setCd50(String cd50) {
		this.cd50 = Functions.subString(cd50, Len.CD50);
	}

	public String getCd50() {
		return this.cd50;
	}

	public void setCd55(String cd55) {
		this.cd55 = Functions.subString(cd55, Len.CD55);
	}

	public String getCd55() {
		return this.cd55;
	}

	public void setCd60(String cd60) {
		this.cd60 = Functions.subString(cd60, Len.CD60);
	}

	public String getCd60() {
		return this.cd60;
	}

	public void setCd65(String cd65) {
		this.cd65 = Functions.subString(cd65, Len.CD65);
	}

	public String getCd65() {
		return this.cd65;
	}

	public void setCd70(String cd70) {
		this.cd70 = Functions.subString(cd70, Len.CD70);
	}

	public String getCd70() {
		return this.cd70;
	}

	public void setCd75(String cd75) {
		this.cd75 = Functions.subString(cd75, Len.CD75);
	}

	public String getCd75() {
		return this.cd75;
	}

	public void setCd80(String cd80) {
		this.cd80 = Functions.subString(cd80, Len.CD80);
	}

	public String getCd80() {
		return this.cd80;
	}

	public void setCd85(String cd85) {
		this.cd85 = Functions.subString(cd85, Len.CD85);
	}

	public String getCd85() {
		return this.cd85;
	}

	public void setCd90(String cd90) {
		this.cd90 = Functions.subString(cd90, Len.CD90);
	}

	public String getCd90() {
		return this.cd90;
	}

	public void setCd95(String cd95) {
		this.cd95 = Functions.subString(cd95, Len.CD95);
	}

	public String getCd95() {
		return this.cd95;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CD50 = 35;
		public static final int CD55 = 35;
		public static final int CD60 = 35;
		public static final int CD65 = 35;
		public static final int CD70 = 35;
		public static final int CD75 = 35;
		public static final int CD80 = 35;
		public static final int CD85 = 35;
		public static final int CD90 = 35;
		public static final int CD95 = 35;
		public static final int MATCH_CODES = CD50 + CD55 + CD60 + CD65 + CD70 + CD75 + CD80 + CD85 + CD90 + CD95;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
