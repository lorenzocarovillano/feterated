/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TpSession;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.Xz0g90m1Data;
import com.federatedinsurance.crs.ws.enums.DsdErrorReturnCode;
import com.federatedinsurance.crs.ws.ptr.ConsumerContract;
import com.federatedinsurance.crs.ws.ptr.DfhcommareaMu0x0004;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90m0;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0G90M1<br>
 * <pre>AUTHOR.         KRISTI SCHETTLER.
 * DATE-WRITTEN.   11 MAY 2009.
 *  ************************************************************
 *  PROGRAM TITLE - UPDATE NOTIFICATION LIST STATUS
 *                  COBOL BUSINESS FRAMEWORK SERVICE INTERFACE
 *                  OPERATION : UpdateNotificationListStatus
 *     PURPOSE: This program is a service interface to the
 *              'UpdateNotificationStatus' service. It will accept
 *              a list of notifications from the consumer, and for
 *              each notification, it will call the
 *              'UpdateNotificationStatus' service to update that
 *              notification's status with the status provided.
 *     NOTES & ASSUMPTIONS :
 *     *********************
 *      - INPUT/OUTPUT = LINKAGE SECTION
 *      - NOTE THAT THIS INTERFACE PROGRAM ACCEPTS A CONTRACT THAT
 *        IS LARGER THAN THE TRADITIONAL 32K LIMIT. THAT IS
 *        BECAUSE THIS INTERFACE SHOULD ONLY BE CALLED FROM AN
 *        ONLINE ENVIRONMENT, NOT A BATCH COBOL ENVIRONMENT.
 *     MAINTENANCE HISTORY
 *     *******************
 *    WR #      DATE   EMP ID   DESCRIPTION
 *  *******   ******** *******  *****************************
 *  TO07614   11MAY09     KXS   INITIAL PROGRAM
 *  TL000143  18FEB10     DMA   UPDATED TO DYNAMIC INPUT LOGIC</pre>*/
public class Xz0g90m1 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Xz0g90m1Data ws = new Xz0g90m1Data();
	private ExecContext execContext = null;
	/**Original name: DFHCOMMAREA<br>
	 * <pre>**  CONSUMER CONTRACT AREA</pre>*/
	private DfhcommareaMu0x0004 dfhcommarea = new DfhcommareaMu0x0004(null);
	//Original name: CONSUMER-CONTRACT
	private ConsumerContract consumerContract = new ConsumerContract(null);
	/**Original name: L-SERVICE-CONTRACT<br>
	 * <pre>**  SERVICE CONTRACT AREA FOR THE UPDATE NOTIFICATION SERVICE</pre>*/
	private LServiceContractAreaXz0x90m0 lServiceContract = new LServiceContractAreaXz0x90m0(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaMu0x0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea.assignBc(dfhcommarea);
		mainline();
		programExit();
		return 0;
	}

	public static Xz0g90m1 getInstance() {
		return (Programs.getInstance(Xz0g90m1.class));
	}

	/**Original name: 1000-MAINLINE<br>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-NO-ERROR-CODE OF DFHCOMMAREA
		//             OR
		//              PPC-WARNING-CODE OF DFHCOMMAREA
		//                  THRU 3000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcNoErrorCode() || dfhcommarea.isPpcWarningCode()) {
			// COB_CODE: PERFORM 3000-PROCESS-LIST
			//              THRU 3000-EXIT
			rng3000ProcessList();
		}
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS.  ALLOCATE      *
	 *  SERVICE INPUT AND SERVICE OUTPUT AREAS.  GET ADDRESSABILITY  *
	 *  VIA THE POINTER PASSED IN.                                   *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE SERVICE-PROXY-CONTRACT.
		initServiceProxyContract();
		// COB_CODE: PERFORM 2500-CAPTURE-INPUT-CONTRACTS
		//              THRU 2500-EXIT.
		captureInputContracts();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: SET PPC-NO-ERROR-CODE OF DFHCOMMAREA
		//                                       TO TRUE.
		dfhcommarea.setPpcNoErrorCode();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE SERVICE PROXY PROGRAM.            *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-SERVICE-MEMORY
		//              THRU 2100-EXIT.
		allocateServiceMemory();
	}

	/**Original name: 2100-ALLOCATE-SERVICE-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE ADDRESS FOR SENDING AND RECEIVING SERVICE INPUT *
	 *  AND OUTPUT DATA.                                             *
	 * ***************************************************************</pre>*/
	private void allocateServiceMemory() {
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT
		//                                       TO PPC-SERVICE-DATA-SIZE
		//                                       OF SERVICE-PROXY-CONTRACT.
		ws.getServiceProxyContract().setPpcServiceDataSize(LServiceContractAreaXz0x90m0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET      (PPC-SERVICE-DATA-POINTER
		//                         OF SERVICE-PROXY-CONTRACT)
		//               FLENGTH  (PPC-SERVICE-DATA-SIZE
		//                         OF SERVICE-PROXY-CONTRACT)
		//               INITIMG  (CF-INITIALIZE-GETMAIN)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getServiceProxyContract().setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getServiceProxyContract().getPpcServiceDataSize(), ws.getConstantFields().getInitializeGetmain()));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
			//               EA-02-PN-2100
			//               EA-02-GETMAIN       TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getParagraphNbr().setPn2100();
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getFlr4().setGetmain();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-02-RESP
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().setResp(TruncAbs.toInt(ws.getWsMiscWorkFlds().getResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().setResp2(ws.getWsMiscWorkFlds().getResponseCode2Formatted());
			// COB_CODE: MOVE EA-02-GETMAIN-FREEMAIN-ERROR
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getEa02GetmainFreemainErrorFormatted());
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		//    SERVICE PROXY IS EXPECTING ADDRESS OF INPUT AND OUTPUT
		//    IN SERVICE DATA POINTER.
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF SERVICE-PROXY-CONTRACT.
		lServiceContract = ((pointerManager.resolve(ws.getServiceProxyContract().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90m0.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT.
		initLServiceContract();
	}

	/**Original name: 2500-CAPTURE-INPUT-CONTRACTS<br>
	 * <pre>***************************************************************
	 *  DETERMINE IF THE DATA WAS PASSED USING EITHER CHANNELS AND   *
	 *  CONTAINERS OR COMMAREA AND REFERENCE STORAGE.                *
	 *  BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
	 *  CONTRACT TO BE USED.                                         *
	 * ***************************************************************
	 *     IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
	 *      THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
	 *      LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
	 *      THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
	 *      COMMON COPYBOOK.</pre>*/
	private void captureInputContracts() {
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 2510-EXIT
		//           ELSE
		//                  THRU 2520-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 2510-CAPTURE-FROM-CHANNEL
			//              THRU 2510-EXIT
			captureFromChannel();
		} else {
			// COB_CODE: PERFORM 2520-CAPTURE-FROM-REF-STORAGE
			//              THRU 2520-EXIT
			captureFromRefStorage();
		}
	}

	/**Original name: 2510-CAPTURE-FROM-CHANNEL<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
	 *  CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
	 *  CONTRACT.                                                    *
	 * ***************************************************************</pre>*/
	private void captureFromChannel() {
		// COB_CODE: PERFORM 2511-ALLOCATE-PROXY-CNT
		//              THRU 2511-EXIT.
		allocateProxyCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2512-GET-PROXY-COMMON-CNT
		//              THRU 2512-EXIT.
		getProxyCommonCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
		//              THRU 2513-EXIT.
		allocateServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2514-GET-SERVICE-CONTRACT
		//              THRU 2514-EXIT.
		getServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
	}

	/**Original name: 2511-ALLOCATE-PROXY-CNT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
	 * ***************************************************************</pre>*/
	private void allocateProxyCnt() {
		// COB_CODE: MOVE LENGTH OF DFHCOMMAREA  TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(DfhcommareaMu0x0004.Len.DFHCOMMAREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET      (WS-POINTER)
		//               FLENGTH  (WS-DATA-SIZE)
		//               INITIMG  (CF-BLANK)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsMiscWorkFlds().setPointer(
				cicsStorageManager.getmainNonshared(execContext, ws.getWsMiscWorkFlds().getDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		//!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
		//!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '2511-ALLOCATE-PROXY-CNT'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("2511-ALLOCATE-PROXY-CNT");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 2511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF DFHCOMMAREA  TO WS-POINTER.
		dfhcommarea = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getPointer(), DfhcommareaMu0x0004.class)));
		// COB_CODE: INITIALIZE DFHCOMMAREA.
		initDfhcommarea();
	}

	/**Original name: 2512-GET-PROXY-COMMON-CNT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
	 * ***************************************************************</pre>*/
	private void getProxyCommonCnt() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE LENGTH OF PPC-INPUT-PARMS
		//                                       OF DFHCOMMAREA
		//                                       TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(DfhcommareaMu0x0004.Len.PPC_INPUT_PARMS);
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
		//               INTO       (PPC-INPUT-PARMS OF DFHCOMMAREA)
		//               FLENGTH    (WS-DATA-SIZE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getDataSize());
		Channel.read(execContext, "", ws.getConstantFields().getCnProxyCbkInpNmFormatted(), tsOutputData);
		dfhcommarea.setPpcInputParmsBytes(tsOutputData.getData());
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2512-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '2512-GET-PROXY-COMMON-CNT'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("2512-GET-PROXY-COMMON-CNT");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 2512-EXIT
			return;
		}
	}

	/**Original name: 2513-ALLOCATE-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
	 * ***************************************************************</pre>*/
	private void allocateServiceContract() {
		// COB_CODE: MOVE LENGTH OF CONSUMER-CONTRACT
		//                                       TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(ConsumerContract.Len.CONSUMER_CONTRACT);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET      (WS-POINTER)
		//               FLENGTH  (WS-DATA-SIZE)
		//               INITIMG  (CF-BLANK)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getWsMiscWorkFlds().setPointer(
				cicsStorageManager.getmainNonshared(execContext, ws.getWsMiscWorkFlds().getDataSize(), ws.getConstantFields().getBlank()));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '2513-ALLOCATE-SERVICE-CONTRACT'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("2513-ALLOCATE-SERVICE-CONTRACT");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 2513-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF CONSUMER-CONTRACT
		//                                       TO WS-POINTER.
		consumerContract = ((pointerManager.resolve(ws.getWsMiscWorkFlds().getPointer(), ConsumerContract.class)));
		// COB_CODE: INITIALIZE CONSUMER-CONTRACT.
		initConsumerContract();
	}

	/**Original name: 2514-GET-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
	 * ***************************************************************</pre>*/
	private void getServiceContract() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE LENGTH OF XZT90M1-SERVICE-INPUTS
		//                                       TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(ConsumerContract.Len.XZT90M1_SERVICE_INPUTS);
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
		//               INTO       (XZT90M1-SERVICE-INPUTS)
		//               FLENGTH    (WS-DATA-SIZE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getDataSize());
		Channel.read(execContext, "", ws.getConstantFields().getCnServiceCbkInpNmFormatted(), tsOutputData);
		consumerContract.setXzt90m1ServiceInputsBytes(tsOutputData.getData());
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2514-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '2514-GET-SERVICE-CONTRACT'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("2514-GET-SERVICE-CONTRACT");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 2514-EXIT
			return;
		}
	}

	/**Original name: 2520-CAPTURE-FROM-REF-STORAGE<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
	 *  WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
	 *  ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
	 * ***************************************************************
	 *     ADDRESS OF INPUT AND OUTPUT IS IN SERVICE DATA POINTER</pre>*/
	private void captureFromRefStorage() {
		// COB_CODE: SET ADDRESS OF CONSUMER-CONTRACT
		//                                       TO PPC-SERVICE-DATA-POINTER
		//                                       OF DFHCOMMAREA.
		consumerContract = ((pointerManager.resolve(dfhcommarea.getPpcServiceDataPointer(), ConsumerContract.class)));
	}

	/**Original name: 3000-PROCESS-LIST<br>
	 * <pre>***************************************************************
	 *  CALL THE SERVICE FOR EACH LIST MEMBER THAT WAS PASSED IN.    *
	 * ***************************************************************
	 *  THE FOLLOWING FIELDS ARE COMMON AND ONLY NEED TO BE SET ONCE.</pre>*/
	private void processList() {
		// COB_CODE: MOVE CF-UPD-NOT-STA         TO PPC-OPERATION
		//                                       OF SERVICE-PROXY-CONTRACT.
		ws.getServiceProxyContract().setPpcOperation(ws.getConstantFields().getUpdNotSta());
		// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV OF SERVICE-PROXY-CONTRACT
		//                                       TO TRUE.
		ws.getServiceProxyContract().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		// COB_CODE: MOVE XZT9M1I-TK-ACT-NOT-STA-CD
		//                                       TO XZT9MI-TK-ACT-NOT-STA-CD.
		lServiceContract.setiTkActNotStaCd(consumerContract.getM1iTkActNotStaCd());
		// COB_CODE: MOVE XZT9M1I-USERID         TO XZT9MI-USERID.
		lServiceContract.setiUserid(consumerContract.getM1iUserid());
		// CALCULATE LIST MAX
		// COB_CODE: COMPUTE WS-LIST-MAX = LENGTH OF XZT9M1I-UPDATE-NOT-LIST-TBL
		//                               / LENGTH OF XZT9M1I-UPDATE-NOT-LIST.
		ws.getWsMiscWorkFlds().setListMax(
				(new AfDecimal(((((double) ConsumerContract.Len.M1I_UPDATE_NOT_LIST_TBL)) / ConsumerContract.Len.M1I_UPDATE_NOT_LIST), 9, 0))
						.toShort());
		// COB_CODE: MOVE +1                     TO SS-UL.
		ws.getSubscripts().setUl(((short) 1));
	}

	/**Original name: 3000-A<br>*/
	private String a() {
		// COB_CODE: IF SS-UL > WS-LIST-MAX
		//             OR
		//              XZT9M1I-CSR-ACT-NBR (SS-UL) = SPACES
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSubscripts().getUl() > ws.getWsMiscWorkFlds().getListMax()
				|| Characters.EQ_SPACE.test(consumerContract.getM1iCsrActNbr(ws.getSubscripts().getUl()))) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3100-CALL-SERVICE
		//              THRU 3100-EXIT.
		callService();
		// COB_CODE: IF PPC-NO-ERROR-CODE OF DFHCOMMAREA
		//             OR
		//              PPC-WARNING-CODE OF DFHCOMMAREA
		//               GO TO 3000-A
		//           END-IF.
		if (dfhcommarea.isPpcNoErrorCode() || dfhcommarea.isPpcWarningCode()) {
			// COB_CODE: ADD +1                  TO SS-UL
			ws.getSubscripts().setUl(Trunc.toShort(1 + ws.getSubscripts().getUl(), 4));
			// COB_CODE: GO TO 3000-A
			return "3000-A";
		}
		return "";
	}

	/**Original name: 3100-CALL-SERVICE<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP AND MAKE THE CALL TO THE SERVICE PROXY PROGRAM*
	 * ***************************************************************</pre>*/
	private void callService() {
		// COB_CODE: PERFORM 3110-SET-UP-INPUT
		//              THRU 3110-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM   (CF-SERVICE-PROXY-PGM)
		//               COMMAREA  (SERVICE-PROXY-CONTRACT)
		//               RESP      (WS-RESPONSE-CODE)
		//               RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0G90M1", execContext).commarea(ws.getServiceProxyContract()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getConstantFields().getServiceProxyPgm(), new Xz0x90m0());
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
			//               EA-01-PN-3100       TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			ws.getErrorAndAdviceMessages().getEa01CicsLinkError().setPn3100();
			// COB_CODE: MOVE CF-SERVICE-PROXY-PGM
			//                                   TO EA-01-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa01CicsLinkError().setLinkPgmName(ws.getConstantFields().getServiceProxyPgm());
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-01-RESP
			ws.getErrorAndAdviceMessages().getEa01CicsLinkError().setResp(TruncAbs.toInt(ws.getWsMiscWorkFlds().getResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-01-RESP2
			ws.getErrorAndAdviceMessages().getEa01CicsLinkError().setResp2(ws.getWsMiscWorkFlds().getResponseCode2Formatted());
			// COB_CODE: MOVE EA-01-CICS-LINK-ERROR
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa01CicsLinkError().getEa01CicsLinkErrorFormatted());
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
		// COB_CODE: IF PPC-NO-ERROR-CODE OF SERVICE-PROXY-CONTRACT
		//               CONTINUE
		//           ELSE
		//                  THRU 3120-EXIT
		//           END-IF.
		if (ws.getServiceProxyContract().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 3120-CAPTURE-SVC-ERROR-INF
			//              THRU 3120-EXIT
			captureSvcErrorInf();
		}
	}

	/**Original name: 3110-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE INPUT AREAS WITH VALUES FROM CONSUMER       *
	 *  CONTRACT                                                     *
	 * ***************************************************************
	 *   SERVICE INPUT PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE XZT9M1I-TK-NOT-PRC-TS (SS-UL)
		//                                       TO XZT9MI-TK-NOT-PRC-TS.
		lServiceContract.setiTkNotPrcTs(consumerContract.getM1iTkNotPrcTs(ws.getSubscripts().getUl()));
		// COB_CODE: MOVE XZT9M1I-CSR-ACT-NBR (SS-UL)
		//                                       TO XZT9MI-CSR-ACT-NBR.
		lServiceContract.setiCsrActNbr(consumerContract.getM1iCsrActNbr(ws.getSubscripts().getUl()));
		// COB_CODE: INITIALIZE PPC-ERROR-HANDLING-PARMS
		//                   OF SERVICE-PROXY-CONTRACT.
		initPpcErrorHandlingParms();
	}

	/**Original name: 3120-CAPTURE-SVC-ERROR-INF<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE ERROR AREA WITH VALUES FROM THE SERVICE     *
	 *  PROXY CONTRACT.                                              *
	 * ***************************************************************</pre>*/
	private void captureSvcErrorInf() {
		// COB_CODE: MOVE PPC-ERROR-RETURN-CODE OF SERVICE-PROXY-CONTRACT
		//                                       TO PPC-ERROR-RETURN-CODE
		//                                       OF DFHCOMMAREA.
		dfhcommarea.setPpcErrorReturnCodeFormatted(ws.getServiceProxyContract().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted());
		//   CAPTURE THE HIGHEST LEVEL ERROR
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-CODE
		//                 OF SERVICE-PROXY-CONTRACT
		//                      THRU 3121-EXIT
		//               WHEN PPC-NLBE-CODE
		//                 OF SERVICE-PROXY-CONTRACT
		//                      THRU 3122-EXIT
		//               WHEN PPC-WARNING-CODE
		//                 OF SERVICE-PROXY-CONTRACT
		//                      THRU 3123-EXIT
		//           END-EVALUATE.
		switch (ws.getServiceProxyContract().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted()) {

		case DsdErrorReturnCode.FATAL_ERROR_CODE:// COB_CODE: PERFORM 3121-SET-FATAL-ERROR
			//              THRU 3121-EXIT
			setFatalError();
			break;

		case DsdErrorReturnCode.NLBE_CODE:// COB_CODE: PERFORM 3122-SET-NLBE
			//              THRU 3122-EXIT
			rng3122SetNlbe();
			break;

		case DsdErrorReturnCode.WARNING_CODE:// COB_CODE: PERFORM 3123-SET-WARNING
			//              THRU 3123-EXIT
			rng3123SetWarning();
			break;

		default:
			break;
		}
	}

	/**Original name: 3121-SET-FATAL-ERROR<br>
	 * <pre>*****************************************************************
	 *   SET FOR FATAL ERROR                                           *
	 * *****************************************************************</pre>*/
	private void setFatalError() {
		// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
		//             OF SERVICE-PROXY-CONTRACT TO PPC-FATAL-ERROR-MESSAGE
		//                                       OF DFHCOMMAREA.
		dfhcommarea.setPpcFatalErrorMessage(ws.getServiceProxyContract().getPpcFatalErrorMessage());
	}

	/**Original name: 3122-SET-NLBE<br>
	 * <pre>*****************************************************************
	 *   SET FOR NLBE                                                  *
	 * *****************************************************************</pre>*/
	private void setNlbe() {
		// COB_CODE: MOVE +0                     TO SS-SP-MSG-IDX.
		ws.getSubscripts().setSpMsgIdx(((short) 0));
	}

	/**Original name: 3122-A<br>*/
	private String a1() {
		// COB_CODE: code not available
		ws.setIntRegister1(((short) 1));
		ws.getSubscripts().setSpMsgIdx(Trunc.toShort(ws.getIntRegister1() + ws.getSubscripts().getSpMsgIdx(), 4));
		ws.getSubscripts().setDfMsgIdx(Trunc.toShort(ws.getIntRegister1() + ws.getSubscripts().getDfMsgIdx(), 4));
		// COB_CODE: IF PPC-NON-LOG-ERR-MSG
		//              OF SERVICE-PROXY-CONTRACT (SS-SP-MSG-IDX) = SPACES
		//               GO TO 3122-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getServiceProxyContract().getPpcNonLoggableErrors(ws.getSubscripts().getSpMsgIdx()).getPpcNonLogErrMsg())) {
			// COB_CODE: GO TO 3122-EXIT
			return "";
		}
		// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG
		//             OF SERVICE-PROXY-CONTRACT (SS-SP-MSG-IDX)
		//                                       TO PPC-NON-LOG-ERR-MSG
		//                                       OF DFHCOMMAREA (SS-DF-MSG-IDX).
		dfhcommarea.setPpcNonLogErrMsg(ws.getSubscripts().getDfMsgIdx(),
				ws.getServiceProxyContract().getPpcNonLoggableErrors(ws.getSubscripts().getSpMsgIdx()).getPpcNonLogErrMsg());
		// COB_CODE: ADD +1                      TO PPC-NON-LOGGABLE-ERROR-CNT
		//                                       OF DFHCOMMAREA.
		dfhcommarea.setPpcNonLoggableErrorCnt(Trunc.toShort(1 + dfhcommarea.getPpcNonLoggableErrorCnt(), 4));
		// COB_CODE: IF SS-SP-MSG-IDX-MAX
		//             OR
		//              SS-DF-MSG-IDX-MAX
		//               GO TO 3122-EXIT
		//           END-IF.
		if (ws.getSubscripts().isSpMsgIdxMax() || ws.getSubscripts().isDfMsgIdxMax()) {
			// COB_CODE: GO TO 3122-EXIT
			return "";
		}
		// COB_CODE: GO TO 3122-A.
		return "3122-A";
	}

	/**Original name: 3123-SET-WARNING<br>
	 * <pre>*****************************************************************
	 *   SET FOR WARNING                                               *
	 * *****************************************************************
	 *     IF WARNING ARRAY IS ALREADY FULL, GO TO EXIT</pre>*/
	private String setWarning() {
		// COB_CODE: IF SS-DF-WNG-IDX-MAX
		//               GO TO 3123-EXIT
		//           END-IF.
		if (ws.getSubscripts().isDfWngIdxMax()) {
			// COB_CODE: GO TO 3123-EXIT
			return "3123-EXIT";
		}
		// COB_CODE: MOVE +0                     TO SS-SP-WNG-IDX.
		ws.getSubscripts().setSpWngIdx(((short) 0));
		return "";
	}

	/**Original name: 3123-A<br>*/
	private String a2() {
		// COB_CODE: code not available
		ws.setIntRegister1(((short) 1));
		ws.getSubscripts().setSpWngIdx(Trunc.toShort(ws.getIntRegister1() + ws.getSubscripts().getSpWngIdx(), 4));
		ws.getSubscripts().setDfWngIdx(Trunc.toShort(ws.getIntRegister1() + ws.getSubscripts().getDfWngIdx(), 4));
		// COB_CODE: IF PPC-WARN-MSG
		//              OF SERVICE-PROXY-CONTRACT (SS-SP-WNG-IDX) = SPACES
		//               GO TO 3123-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getServiceProxyContract().getPpcWarnings(ws.getSubscripts().getSpWngIdx()).getPpcWarnMsg())) {
			// COB_CODE: GO TO 3123-EXIT
			return "";
		}
		// COB_CODE: MOVE PPC-WARN-MSG
		//             OF SERVICE-PROXY-CONTRACT (SS-SP-WNG-IDX)
		//                                       TO PPC-WARN-MSG
		//                                       OF DFHCOMMAREA (SS-DF-WNG-IDX).
		dfhcommarea.setPpcWarnMsg(ws.getSubscripts().getDfWngIdx(),
				ws.getServiceProxyContract().getPpcWarnings(ws.getSubscripts().getSpWngIdx()).getPpcWarnMsg());
		// COB_CODE: ADD +1                      TO PPC-WARNING-CNT
		//                                       OF DFHCOMMAREA.
		dfhcommarea.setPpcWarningCnt(Trunc.toShort(1 + dfhcommarea.getPpcWarningCnt(), 4));
		// COB_CODE: IF SS-SP-WNG-IDX-MAX
		//             OR
		//              SS-DF-WNG-IDX-MAX
		//               GO TO 3123-EXIT
		//           END-IF.
		if (ws.getSubscripts().isSpWngIdxMax() || ws.getSubscripts().isDfWngIdxMax()) {
			// COB_CODE: GO TO 3123-EXIT
			return "";
		}
		// COB_CODE: GO TO 3123-A.
		return "3123-A";
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS.                                       *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 8200-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 8200-RETURN-SVC-CONTRACT
			//              THRU 8200-EXIT
			returnSvcContract();
		}
		// COB_CODE: PERFORM 8100-RELEASE-SERVICE-MEMORY
		//              THRU 8100-EXIT.
		releaseServiceMemory();
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 8300-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 8300-RETURN-PROXY-COMMON
			//              THRU 8300-EXIT
			returnProxyCommon();
		}
	}

	/**Original name: 8100-RELEASE-SERVICE-MEMORY<br>
	 * <pre>***************************************************************
	 *  DEALLOCATE THE SERVICE MEMORY ALLOCATED EARLIER              *
	 * ***************************************************************
	 *     FREE MEMORY</pre>*/
	private void releaseServiceMemory() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA   (L-SERVICE-CONTRACT)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContract));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                                       OF DFHCOMMAREA
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
			//               EA-02-PN-8100
			//               EA-02-FREEMAIN      TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getParagraphNbr().setPn8100();
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getFlr4().setFreemain();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-02-RESP
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().setResp(TruncAbs.toInt(ws.getWsMiscWorkFlds().getResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
			ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().setResp2(ws.getWsMiscWorkFlds().getResponseCode2Formatted());
			// COB_CODE: MOVE EA-02-GETMAIN-FREEMAIN-ERROR
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa02GetmainFreemainError().getEa02GetmainFreemainErrorFormatted());
		}
		//    CHECK IF WE NEED TO COMMIT/ROLLBACK CHANGES
		// COB_CODE: IF PPC-BYPASS-SYNCPOINT-IN-MDRV OF DFHCOMMAREA
		//               GO TO 8100-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcBypassSyncpointInMdrv()) {
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
		// COB_CODE: IF PPC-NO-ERROR-CODE OF DFHCOMMAREA
		//             OR
		//              PPC-WARNING-CODE OF DFHCOMMAREA
		//               END-EXEC
		//           ELSE
		//               END-EXEC
		//           END-IF.
		if (dfhcommarea.isPpcNoErrorCode() || dfhcommarea.isPpcWarningCode()) {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT
			//           END-EXEC
			TpSession.current().syncpoint(true);
		} else {
			// COB_CODE: EXEC CICS
			//               SYNCPOINT ROLLBACK
			//           END-EXEC
			TpSession.current().syncpoint(false);
		}
	}

	/**Original name: 8200-RETURN-SVC-CONTRACT<br>
	 * <pre>***************************************************************
	 *  OUTPUT THE SERVICE CONTRACT CONTAINER AND RELEASE THE MEMORY *
	 *  ALLOCATED BY THIS PROGRAM.                                   *
	 * ***************************************************************</pre>*/
	private void returnSvcContract() {
		// COB_CODE: PERFORM 8210-OUTPUT-SVC-CONTRACT-CTA
		//              THRU 8210-EXIT.
		outputSvcContractCta();
		// COB_CODE: PERFORM 8220-RELEASE-SVC-CONTRACT-MEM
		//              THRU 8220-EXIT.
		releaseSvcContractMem();
	}

	/**Original name: 8210-OUTPUT-SVC-CONTRACT-CTA<br>
	 * <pre>***************************************************************
	 *  PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW             *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputSvcContractCta() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE LENGTH OF XZT90M1-SERVICE-OUTPUTS
		//                                       TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(ConsumerContract.Len.XZT90M1_SERVICE_OUTPUTS);
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
		//               FROM       (CONSUMER-CONTRACT)
		//               FLENGTH    (WS-DATA-SIZE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getDataSize());
		tsOutputData.setData(consumerContract.getConsumerContractBytes());
		Channel.write(execContext, "", ws.getConstantFields().getCnServiceCbkOupNmFormatted(), tsOutputData);
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		//  IF A FATAL ERROR PREVIOUSLY OCCURRED, DO NOT OVERRIDE IT WITH
		//    THIS ONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              NOT PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 8210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL && !dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '8210-OUTPUT-SVC-CONTRACT-CTA'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("8210-OUTPUT-SVC-CONTRACT-CTA");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 8210-EXIT
			return;
		}
	}

	/**Original name: 8220-RELEASE-SVC-CONTRACT-MEM<br>
	 * <pre>***************************************************************
	 *  RELEASE THE MEMORY ALLOCATED BY THIS PROGRAM                 *
	 * ***************************************************************</pre>*/
	private void releaseSvcContractMem() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA  (CONSUMER-CONTRACT)
		//               RESP  (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(consumerContract));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		//  IF A FATAL ERROR PREVIOUSLY OCCURRED, DO NOT OVERRIDE IT WITH
		//    THIS ONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              NOT PPC-FATAL-ERROR-CODE OF DFHCOMMAREA
		//               GO TO 8220-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL && !dfhcommarea.isPpcFatalErrorCode()) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '8220-RELEASE-SVC-CONTRACT-MEM'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("8220-RELEASE-SVC-CONTRACT-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 8220-EXIT
			return;
		}
	}

	/**Original name: 8300-RETURN-PROXY-COMMON<br>
	 * <pre>***************************************************************
	 *  OUTPUT THE PROXY CONTRACT CONTAINER AND RELEASE THE MEMORY   *
	 *  ALLOCATED BY THIS PROGRAM.                                   *
	 * ***************************************************************</pre>*/
	private void returnProxyCommon() {
		// COB_CODE: PERFORM 8310-OUTPUT-PROXY-COMMON-CTA
		//              THRU 8310-EXIT.
		outputProxyCommonCta();
		// COB_CODE: PERFORM 8320-RELEASE-PROXY-COMMON-MEM
		//              THRU 8320-EXIT.
		releaseProxyCommonMem();
	}

	/**Original name: 8310-OUTPUT-PROXY-COMMON-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
	 *  FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputProxyCommonCta() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE LENGTH OF PPC-OUTPUT-PARMS
		//                                       OF DFHCOMMAREA
		//                                       TO WS-DATA-SIZE.
		ws.getWsMiscWorkFlds().setDataSize(DfhcommareaMu0x0004.Len.PPC_OUTPUT_PARMS);
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
		//               FROM       (PPC-OUTPUT-PARMS OF DFHCOMMAREA)
		//               FLENGTH    (WS-DATA-SIZE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(ws.getWsMiscWorkFlds().getDataSize());
		tsOutputData.setData(dfhcommarea.getPpcOutputParmsBytes());
		Channel.write(execContext, "", ws.getConstantFields().getCnProxyCbkOupNmFormatted(), tsOutputData);
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		//!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
		//!   BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8310-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '8310-OUTPUT-PROXY-COMMON'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("8310-OUTPUT-PROXY-COMMON");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 8310-EXIT
			return;
		}
	}

	/**Original name: 8320-RELEASE-PROXY-COMMON-MEM<br>
	 * <pre>***************************************************************
	 *  RELEASE THE MEMORY ALLOCATED BY THIS PROGRAM                 *
	 * ***************************************************************</pre>*/
	private void releaseProxyCommonMem() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA   (DFHCOMMAREA)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(dfhcommarea));
		ws.getWsMiscWorkFlds().setResponseCode(execContext.getResp());
		ws.getWsMiscWorkFlds().setResponseCode2(execContext.getResp2());
		//!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8320-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsMiscWorkFlds().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   OF DFHCOMMAREA
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE '8320-RELEASE-PROXY-COMMON-MEM'
			//                                   TO EA-03-FAILED-PARAGRAPH
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setFailedParagraph("8320-RELEASE-PROXY-COMMON-MEM");
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-03-EIBRESP-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibrespDisplay(ws.getWsMiscWorkFlds().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-03-EIBRESP2-DISPLAY
			ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().setEibresp2Display(ws.getWsMiscWorkFlds().getResponseCode2());
			// COB_CODE: MOVE EA-03-FATAL-ERROR-MSG
			//                                   TO PPC-FATAL-ERROR-MESSAGE
			//                                   OF DFHCOMMAREA
			dfhcommarea.setPpcFatalErrorMessage(ws.getErrorAndAdviceMessages().getEa03FatalErrorMsg().getEa03FatalErrorMsgFormatted());
			// COB_CODE: GO TO 8320-EXIT
			return;
		}
	}

	/**Original name: RNG_3000-PROCESS-LIST-_-3000-EXIT<br>*/
	private void rng3000ProcessList() {
		String retcode = "";
		boolean goto3000A = false;
		processList();
		do {
			goto3000A = false;
			retcode = a();
		} while (retcode.equals("3000-A"));
	}

	/**Original name: RNG_3122-SET-NLBE-_-3122-EXIT<br>*/
	private void rng3122SetNlbe() {
		String retcode = "";
		boolean goto3122A = false;
		setNlbe();
		do {
			goto3122A = false;
			retcode = a1();
		} while (retcode.equals("3122-A"));
	}

	/**Original name: RNG_3123-SET-WARNING-_-3123-EXIT<br>*/
	private void rng3123SetWarning() {
		String retcode = "";
		boolean goto3123A = false;
		boolean goto3123Exit = false;
		retcode = setWarning();
		if (!retcode.equals("3123-EXIT")) {
			do {
				goto3123A = false;
				retcode = a2();
			} while (retcode.equals("3123-A"));
		}
		goto3123Exit = false;
	}

	public void initServiceProxyContract() {
		ws.getServiceProxyContract().setPpcServiceDataSize(0);
		ws.getServiceProxyContract().getPpcBypassSyncpointMdrvInd().setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		ws.getServiceProxyContract().setPpcOperation("");
		ws.getServiceProxyContract().getPpcErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getServiceProxyContract().setPpcFatalErrorMessage("");
		ws.getServiceProxyContract().setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcNonLoggableErrors(idx0).setPpcNonLogErrMsg("");
		}
		ws.getServiceProxyContract().setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_WARNINGS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcWarnings(idx0).setPpcWarnMsg("");
		}
	}

	public void initLServiceContract() {
		lServiceContract.setiTkNotPrcTs("");
		lServiceContract.setiTkActNotStaCd("");
		lServiceContract.setiCsrActNbr("");
		lServiceContract.setiUserid("");
	}

	public void initDfhcommarea() {
		dfhcommarea.setPpcServiceDataSize(0);
		dfhcommarea.setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		dfhcommarea.setPpcOperation("");
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initConsumerContract() {
		consumerContract.setM1iTkActNotStaCd("");
		consumerContract.setM1iUserid("");
		for (int idx0 = 1; idx0 <= ConsumerContract.M1I_UPDATE_NOT_LIST_MAXOCCURS; idx0++) {
			consumerContract.setM1iTkNotPrcTs(idx0, "");
			consumerContract.setM1iCsrActNbr(idx0, "");
		}
	}

	public void initPpcErrorHandlingParms() {
		ws.getServiceProxyContract().getPpcErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getServiceProxyContract().setPpcFatalErrorMessage("");
		ws.getServiceProxyContract().setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcNonLoggableErrors(idx0).setPpcNonLogErrMsg("");
		}
		ws.getServiceProxyContract().setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_WARNINGS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcWarnings(idx0).setPpcWarnMsg("");
		}
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
