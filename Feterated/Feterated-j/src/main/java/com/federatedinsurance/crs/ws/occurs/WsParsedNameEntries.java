/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-PARSED-NAME-ENTRIES<br>
 * Variables: WS-PARSED-NAME-ENTRIES from program CIWBNSRB<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsParsedNameEntries {

	//==== PROPERTIES ====
	//Original name: WS-PARSED-NAME
	private String name = DefaultValues.stringVal(Len.NAME);
	//Original name: WS-PARSED-TYPE-CODE
	private char typeCode = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void initWsParsedNameEntriesSpaces() {
		name = "";
		typeCode = Types.SPACE_CHAR;
	}

	public void setName(String name) {
		this.name = Functions.subString(name, Len.NAME);
	}

	public String getName() {
		return this.name;
	}

	public String getNameFormatted() {
		return Functions.padBlanks(getName(), Len.NAME);
	}

	public void setTypeCode(char typeCode) {
		this.typeCode = typeCode;
	}

	public void setTypeCodeFormatted(String typeCode) {
		setTypeCode(Functions.charAt(typeCode, Types.CHAR_SIZE));
	}

	public char getTypeCode() {
		return this.typeCode;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NAME = 60;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
