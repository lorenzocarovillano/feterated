/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Xz003oPolCovInf;
import com.federatedinsurance.crs.ws.occurs.Xzc03oExtMsgStaLis;

/**Original name: XZC030-PROGRAM-OUTPUT<br>
 * Variable: XZC030-PROGRAM-OUTPUT from copybook XZC030C1<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc030ProgramOutput {

	//==== PROPERTIES ====
	public static final int EXT_MSG_STA_LIS_MAXOCCURS = 50;
	public static final int POL_COV_INF_MAXOCCURS = 50;
	//Original name: XZC03O-POL-COV-INF
	private Xz003oPolCovInf[] polCovInf = new Xz003oPolCovInf[POL_COV_INF_MAXOCCURS];
	//Original name: XZC03O-MSG-STA-CD
	private String msgStaCd = DefaultValues.stringVal(Len.MSG_STA_CD);
	//Original name: XZC03O-MSG-ERR-CD
	private String msgErrCd = DefaultValues.stringVal(Len.MSG_ERR_CD);
	//Original name: XZC03O-MSG-STA-DES
	private String msgStaDes = DefaultValues.stringVal(Len.MSG_STA_DES);
	//Original name: XZC03O-EXT-MSG-STA-LIS
	private Xzc03oExtMsgStaLis[] extMsgStaLis = new Xzc03oExtMsgStaLis[EXT_MSG_STA_LIS_MAXOCCURS];
	//Original name: XZC03O-ERROR-CODE
	private Xz08coRetCd errorCode = new Xz08coRetCd();
	//Original name: XZC03O-ERROR-MESSAGE
	private String errorMessage = DefaultValues.stringVal(Len.ERROR_MESSAGE);
	//Original name: FILLER-XZC030-PROGRAM-OUTPUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== CONSTRUCTORS ====
	public Xzc030ProgramOutput() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int polCovInfIdx = 1; polCovInfIdx <= POL_COV_INF_MAXOCCURS; polCovInfIdx++) {
			polCovInf[polCovInfIdx - 1] = new Xz003oPolCovInf();
		}
		for (int extMsgStaLisIdx = 1; extMsgStaLisIdx <= EXT_MSG_STA_LIS_MAXOCCURS; extMsgStaLisIdx++) {
			extMsgStaLis[extMsgStaLisIdx - 1] = new Xzc03oExtMsgStaLis();
		}
	}

	public void setXzc030ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		setServiceOutputBytes(buffer, position);
		position += Len.SERVICE_OUTPUT;
		errorCode.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		errorMessage = MarshalByte.readString(buffer, position, Len.ERROR_MESSAGE);
		position += Len.ERROR_MESSAGE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzc030ProgramOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		getServiceOutputBytes(buffer, position);
		position += Len.SERVICE_OUTPUT;
		MarshalByte.writeShort(buffer, position, errorCode.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		MarshalByte.writeString(buffer, position, errorMessage, Len.ERROR_MESSAGE);
		position += Len.ERROR_MESSAGE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setServiceOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_COV_INF_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				polCovInf[idx - 1].setPolCovInfBytes(buffer, position);
				position += Xz003oPolCovInf.Len.POL_COV_INF;
			} else {
				polCovInf[idx - 1].initPolCovInfSpaces();
				position += Xz003oPolCovInf.Len.POL_COV_INF;
			}
		}
		setMsgInfBytes(buffer, position);
	}

	public byte[] getServiceOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= POL_COV_INF_MAXOCCURS; idx++) {
			polCovInf[idx - 1].getPolCovInfBytes(buffer, position);
			position += Xz003oPolCovInf.Len.POL_COV_INF;
		}
		getMsgInfBytes(buffer, position);
		return buffer;
	}

	public void setMsgInfBytes(byte[] buffer, int offset) {
		int position = offset;
		msgStaCd = MarshalByte.readString(buffer, position, Len.MSG_STA_CD);
		position += Len.MSG_STA_CD;
		msgErrCd = MarshalByte.readString(buffer, position, Len.MSG_ERR_CD);
		position += Len.MSG_ERR_CD;
		msgStaDes = MarshalByte.readString(buffer, position, Len.MSG_STA_DES);
		position += Len.MSG_STA_DES;
		for (int idx = 1; idx <= EXT_MSG_STA_LIS_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				extMsgStaLis[idx - 1].setExtMsgStaLisBytes(buffer, position);
				position += Xzc03oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
			} else {
				extMsgStaLis[idx - 1].initExtMsgStaLisSpaces();
				position += Xzc03oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
			}
		}
	}

	public byte[] getMsgInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, msgStaCd, Len.MSG_STA_CD);
		position += Len.MSG_STA_CD;
		MarshalByte.writeString(buffer, position, msgErrCd, Len.MSG_ERR_CD);
		position += Len.MSG_ERR_CD;
		MarshalByte.writeString(buffer, position, msgStaDes, Len.MSG_STA_DES);
		position += Len.MSG_STA_DES;
		for (int idx = 1; idx <= EXT_MSG_STA_LIS_MAXOCCURS; idx++) {
			extMsgStaLis[idx - 1].getExtMsgStaLisBytes(buffer, position);
			position += Xzc03oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
		}
		return buffer;
	}

	public void setMsgStaCd(String msgStaCd) {
		this.msgStaCd = Functions.subString(msgStaCd, Len.MSG_STA_CD);
	}

	public String getMsgStaCd() {
		return this.msgStaCd;
	}

	public String getXzc03oMsgStaCdFormatted() {
		return Functions.padBlanks(getMsgStaCd(), Len.MSG_STA_CD);
	}

	public void setMsgErrCd(String msgErrCd) {
		this.msgErrCd = Functions.subString(msgErrCd, Len.MSG_ERR_CD);
	}

	public String getMsgErrCd() {
		return this.msgErrCd;
	}

	public String getXzc03oMsgErrCdFormatted() {
		return Functions.padBlanks(getMsgErrCd(), Len.MSG_ERR_CD);
	}

	public void setMsgStaDes(String msgStaDes) {
		this.msgStaDes = Functions.subString(msgStaDes, Len.MSG_STA_DES);
	}

	public String getMsgStaDes() {
		return this.msgStaDes;
	}

	public String getXzc03oMsgStaDesFormatted() {
		return Functions.padBlanks(getMsgStaDes(), Len.MSG_STA_DES);
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = Functions.subString(errorMessage, Len.ERROR_MESSAGE);
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public Xz08coRetCd getErrorCode() {
		return errorCode;
	}

	public Xzc03oExtMsgStaLis getExtMsgStaLis(int idx) {
		return extMsgStaLis[idx - 1];
	}

	public Xz003oPolCovInf getPolCovInf(int idx) {
		return polCovInf[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSG_STA_CD = 25;
		public static final int MSG_ERR_CD = 30;
		public static final int MSG_STA_DES = 255;
		public static final int MSG_INF = MSG_STA_CD + MSG_ERR_CD + MSG_STA_DES
				+ Xzc030ProgramOutput.EXT_MSG_STA_LIS_MAXOCCURS * Xzc03oExtMsgStaLis.Len.EXT_MSG_STA_LIS;
		public static final int SERVICE_OUTPUT = Xzc030ProgramOutput.POL_COV_INF_MAXOCCURS * Xz003oPolCovInf.Len.POL_COV_INF + MSG_INF;
		public static final int ERROR_MESSAGE = 500;
		public static final int FLR1 = 500;
		public static final int XZC030_PROGRAM_OUTPUT = SERVICE_OUTPUT + Xz08coRetCd.Len.XZ08CO_RET_CD + ERROR_MESSAGE + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
