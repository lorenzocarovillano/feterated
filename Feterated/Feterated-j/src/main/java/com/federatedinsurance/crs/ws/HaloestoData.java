/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.copy.EdtlcHalErrLogDtlRow;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.HemccHalErrLogMcmRow;
import com.federatedinsurance.crs.copy.HemdcHalErrLogMdrvRow;
import com.federatedinsurance.crs.copy.UrqmCommon;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOESTO<br>
 * Generated as a class for rule WS.<br>*/
public class HaloestoData {

	//==== PROPERTIES ====
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-WORK-AREAS
	private WsWorkAreasHaloesto wsWorkAreas = new WsWorkAreasHaloesto();
	//Original name: W-WORKFIELDS
	private WWorkfieldsHaloesto wWorkfields = new WWorkfieldsHaloesto();
	//Original name: URQM-COMMON
	private UrqmCommon urqmCommon = new UrqmCommon();
	//Original name: EDTLC-HAL-ERR-LOG-DTL-ROW
	private EdtlcHalErrLogDtlRow edtlcHalErrLogDtlRow = new EdtlcHalErrLogDtlRow();
	//Original name: HELFC-HAL-ERR-LOG-FAIL-ROW
	private HelfcHalErrLogFailRow helfcHalErrLogFailRow = new HelfcHalErrLogFailRow();
	//Original name: HEMCC-HAL-ERR-LOG-MCM-ROW
	private HemccHalErrLogMcmRow hemccHalErrLogMcmRow = new HemccHalErrLogMcmRow();
	//Original name: HEMDC-HAL-ERR-LOG-MDRV-ROW
	private HemdcHalErrLogMdrvRow hemdcHalErrLogMdrvRow = new HemdcHalErrLogMdrvRow();
	//Original name: WS-ESTO-LINK
	private WsEstoInfo wsEstoLink = new WsEstoInfo();

	//==== METHODS ====
	public void setWsVsamRecordBytes(byte[] buffer) {
		setWsVsamRecordBytes(buffer, 1);
	}

	/**Original name: WS-VSAM-RECORD<br>
	 * <pre>---*
	 *  VSAM RECORD
	 * ---*</pre>*/
	public byte[] getWsVsamRecordBytes() {
		byte[] buffer = new byte[Len.WS_VSAM_RECORD];
		return getWsVsamRecordBytes(buffer, 1);
	}

	public void setWsVsamRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		urqmCommon.setUrqmCommonBytes(buffer, position);
	}

	public byte[] getWsVsamRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		urqmCommon.getUrqmCommonBytes(buffer, position);
		return buffer;
	}

	public String getWsDetailVsamDataFormatted() {
		return edtlcHalErrLogDtlRow.getEdtlcHalErrLogDtlRowFormatted();
	}

	public void setWsCelfRecordFormatted(String data) {
		byte[] buffer = new byte[Len.WS_CELF_RECORD];
		MarshalByte.writeString(buffer, 1, data, Len.WS_CELF_RECORD);
		setWsCelfRecordBytes(buffer, 1);
	}

	public String getWsCelfRecordFormatted() {
		return helfcHalErrLogFailRow.getHelfcHalErrLogFailRowFormatted();
	}

	public void setWsCelfRecordBytes(byte[] buffer, int offset) {
		int position = offset;
		helfcHalErrLogFailRow.setHelfcHalErrLogFailRowBytes(buffer, position);
	}

	public void initWsCelfRecordSpaces() {
		helfcHalErrLogFailRow.initHelfcHalErrLogFailRowSpaces();
	}

	public String getWsCemcRecordFormatted() {
		return hemccHalErrLogMcmRow.getHemccHalErrLogMcmRowFormatted();
	}

	public void initWsCemcRecordSpaces() {
		hemccHalErrLogMcmRow.initHemccHalErrLogMcmRowSpaces();
	}

	public String getWsCemdRecordFormatted() {
		return hemdcHalErrLogMdrvRow.getHemdcHalErrLogMdrvRowFormatted();
	}

	public void initWsCemdRecordSpaces() {
		hemdcHalErrLogMdrvRow.initHemdcHalErrLogMdrvRowSpaces();
	}

	public EdtlcHalErrLogDtlRow getEdtlcHalErrLogDtlRow() {
		return edtlcHalErrLogDtlRow;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public HelfcHalErrLogFailRow getHelfcHalErrLogFailRow() {
		return helfcHalErrLogFailRow;
	}

	public HemccHalErrLogMcmRow getHemccHalErrLogMcmRow() {
		return hemccHalErrLogMcmRow;
	}

	public HemdcHalErrLogMdrvRow getHemdcHalErrLogMdrvRow() {
		return hemdcHalErrLogMdrvRow;
	}

	public UrqmCommon getUrqmCommon() {
		return urqmCommon;
	}

	public WsEstoInfo getWsEstoLink() {
		return wsEstoLink;
	}

	public WsWorkAreasHaloesto getWsWorkAreas() {
		return wsWorkAreas;
	}

	public WWorkfieldsHaloesto getwWorkfields() {
		return wWorkfields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_CELF_RECORD = HelfcHalErrLogFailRow.Len.HELFC_HAL_ERR_LOG_FAIL_ROW;
		public static final int WS_VSAM_RECORD = UrqmCommon.Len.URQM_COMMON;
		public static final int WS_CEMD_RECORD = HemdcHalErrLogMdrvRow.Len.HEMDC_HAL_ERR_LOG_MDRV_ROW;
		public static final int WS_CEMC_RECORD = HemccHalErrLogMcmRow.Len.HEMCC_HAL_ERR_LOG_MCM_ROW;
		public static final int WS_DETAIL_VSAM_DATA = EdtlcHalErrLogDtlRow.Len.EDTLC_HAL_ERR_LOG_DTL_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
