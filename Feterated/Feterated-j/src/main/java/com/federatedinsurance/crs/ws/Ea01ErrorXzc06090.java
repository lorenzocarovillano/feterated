/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-ERROR<br>
 * Variable: EA-01-ERROR from program XZC06090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea01ErrorXzc06090 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-ERROR
	private String flr1 = "PGM = XZC06090";
	//Original name: FILLER-EA-01-ERROR-1
	private String flr2 = "PARA =";
	//Original name: EA-01-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-01-ERROR-2
	private String flr3 = "";
	//Original name: EA-01-ERROR-MESSAGE
	private String errorMessage = DefaultValues.stringVal(Len.ERROR_MESSAGE);

	//==== METHODS ====
	public String getEa01ErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa01ErrorBytes());
	}

	public byte[] getEa01ErrorBytes() {
		byte[] buffer = new byte[Len.EA01_ERROR];
		return getEa01ErrorBytes(buffer, 1);
	}

	public byte[] getEa01ErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, errorMessage, Len.ERROR_MESSAGE);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = Functions.subString(errorMessage, Len.ERROR_MESSAGE);
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PARAGRAPH_NBR = 7;
		public static final int ERROR_MESSAGE = 469;
		public static final int FLR1 = 15;
		public static final int FLR2 = 7;
		public static final int FLR3 = 2;
		public static final int EA01_ERROR = PARAGRAPH_NBR + ERROR_MESSAGE + FLR1 + FLR2 + FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
