/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-20-CICS-LINK-ERROR<br>
 * Variable: EA-20-CICS-LINK-ERROR from program XZC01090<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea20CicsLinkError {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-20-CICS-LINK-ERROR
	private String flr1 = "CICS LINK";
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-1
	private String flr2 = "ERROR.";
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-2
	private String flr3 = "PGM =";
	//Original name: EA-20-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-3
	private String flr4 = ", PARA =";
	//Original name: EA-20-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-4
	private String flr5 = ", CICS MODULE";
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-5
	private String flr6 = "=";
	//Original name: EA-20-CICS-MODULE
	private String cicsModule = DefaultValues.stringVal(Len.CICS_MODULE);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-6
	private String flr7 = ", RESP CD =";
	//Original name: EA-20-RESPONSE-CODE
	private String responseCode = DefaultValues.stringVal(Len.RESPONSE_CODE);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-7
	private String flr8 = ", RESP CD 2 =";
	//Original name: EA-20-RESPONSE-CODE2
	private String responseCode2 = DefaultValues.stringVal(Len.RESPONSE_CODE2);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-8
	private String flr9 = " FOR POL NBR =";
	//Original name: EA-20-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-9
	private String flr10 = ", POL EFF DT =";
	//Original name: EA-20-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: FILLER-EA-20-CICS-LINK-ERROR-10
	private String flr11 = ", POL EXP DT =";
	//Original name: EA-20-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);

	//==== METHODS ====
	public String getEa20CicsLinkErrorFormatted() {
		return MarshalByteExt.bufferToStr(getEa20CicsLinkErrorBytes());
	}

	public byte[] getEa20CicsLinkErrorBytes() {
		byte[] buffer = new byte[Len.EA20_CICS_LINK_ERROR];
		return getEa20CicsLinkErrorBytes(buffer, 1);
	}

	public byte[] getEa20CicsLinkErrorBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, cicsModule, Len.CICS_MODULE);
		position += Len.CICS_MODULE;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR7);
		position += Len.FLR7;
		MarshalByte.writeString(buffer, position, responseCode, Len.RESPONSE_CODE);
		position += Len.RESPONSE_CODE;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, responseCode2, Len.RESPONSE_CODE2);
		position += Len.RESPONSE_CODE2;
		MarshalByte.writeString(buffer, position, flr9, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, flr10, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, flr11, Len.FLR9);
		position += Len.FLR9;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setCicsModule(String cicsModule) {
		this.cicsModule = Functions.subString(cicsModule, Len.CICS_MODULE);
	}

	public String getCicsModule() {
		return this.cicsModule;
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setResponseCode(long responseCode) {
		this.responseCode = PicFormatter.display("-Z(8)9").format(responseCode).toString();
	}

	public long getResponseCode() {
		return PicParser.display("-Z(8)9").parseLong(this.responseCode);
	}

	public String getFlr8() {
		return this.flr8;
	}

	public void setResponseCode2(long responseCode2) {
		this.responseCode2 = PicFormatter.display("-Z(8)9").format(responseCode2).toString();
	}

	public long getResponseCode2() {
		return PicParser.display("-Z(8)9").parseLong(this.responseCode2);
	}

	public String getFlr9() {
		return this.flr9;
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public String getFlr10() {
		return this.flr10;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public String getFlr11() {
		return this.flr11;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int CICS_MODULE = 8;
		public static final int RESPONSE_CODE = 10;
		public static final int RESPONSE_CODE2 = 10;
		public static final int POL_NBR = 9;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int FLR1 = 10;
		public static final int FLR2 = 7;
		public static final int FLR3 = 6;
		public static final int FLR4 = 9;
		public static final int FLR5 = 14;
		public static final int FLR6 = 2;
		public static final int FLR7 = 12;
		public static final int FLR9 = 15;
		public static final int EA20_CICS_LINK_ERROR = PROGRAM_NAME + PARAGRAPH_NBR + CICS_MODULE + RESPONSE_CODE + RESPONSE_CODE2 + POL_NBR
				+ POL_EFF_DT + POL_EXP_DT + FLR1 + FLR2 + FLR3 + FLR4 + 2 * FLR5 + FLR6 + FLR7 + 3 * FLR9;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
