/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACT_NOT_FRM]
 * 
 */
public interface IActNotFrm extends BaseSqlTo {

	/**
	 * Host Variable FRM-SEQ-NBR
	 * 
	 */
	short getSeqNbr();

	void setSeqNbr(short seqNbr);

	/**
	 * Host Variable FRM-NBR
	 * 
	 */
	String getNbr();

	void setNbr(String nbr);

	/**
	 * Host Variable FRM-EDT-DT
	 * 
	 */
	String getEdtDt();

	void setEdtDt(String edtDt);

	/**
	 * Host Variable XZH006-CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable XZH006-NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable XZH006-FRM-SEQ-NBR
	 * 
	 */
	short getFrmSeqNbr();

	void setFrmSeqNbr(short frmSeqNbr);

	/**
	 * Host Variable XZH006-FRM-NBR
	 * 
	 */
	String getFrmNbr();

	void setFrmNbr(String frmNbr);

	/**
	 * Host Variable XZH006-FRM-EDT-DT
	 * 
	 */
	String getFrmEdtDt();

	void setFrmEdtDt(String frmEdtDt);
};
