/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotPolTypDtaExt;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, ACT_NOT_POL, ACT_NOT_TYP, POL_DTA_EXT]
 * 
 */
public class ActNotPolTypDtaExtDao extends BaseSqlDao<IActNotPolTypDtaExt> {

	public ActNotPolTypDtaExtDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotPolTypDtaExt> getToClass() {
		return IActNotPolTypDtaExt.class;
	}

	public IActNotPolTypDtaExt selectRec(String csrActNbr, String polNbr, String polEffDt, String polExpDt, IActNotPolTypDtaExt iActNotPolTypDtaExt) {
		return buildQuery("selectRec").bind("csrActNbr", csrActNbr).bind("polNbr", polNbr).bind("polEffDt", polEffDt).bind("polExpDt", polExpDt)
				.singleResult(iActNotPolTypDtaExt);
	}
}
