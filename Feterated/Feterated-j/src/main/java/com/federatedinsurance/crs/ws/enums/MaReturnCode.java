/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: MA-RETURN-CODE<br>
 * Variable: MA-RETURN-CODE from copybook TS020COM<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MaReturnCode {

	//==== PROPERTIES ====
	public String value = DefaultValues.stringVal(Len.MA_RETURN_CODE);
	public static final String FATAL_ERROR_CODE = "0300";
	public static final String NO_ERROR_CODE = "0000";

	//==== METHODS ====
	public void setMaReturnCode(short maReturnCode) {
		this.value = NumericDisplay.asString(maReturnCode, Len.MA_RETURN_CODE);
	}

	public void setMaReturnCodeFormatted(String maReturnCode) {
		this.value = Trunc.toUnsignedNumeric(maReturnCode, Len.MA_RETURN_CODE);
	}

	public short getMaReturnCode() {
		return NumericDisplay.asShort(this.value);
	}

	public void setMaFatalErrorCode() {
		setMaReturnCodeFormatted(FATAL_ERROR_CODE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MA_RETURN_CODE = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
