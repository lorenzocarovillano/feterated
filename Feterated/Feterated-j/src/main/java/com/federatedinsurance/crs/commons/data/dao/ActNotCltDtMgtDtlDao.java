/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IActNotCltDtMgtDtl;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for tables [ACT_NOT, CLT_ACT_DT_MGT_DTL]
 * 
 */
public class ActNotCltDtMgtDtlDao extends BaseSqlDao<IActNotCltDtMgtDtl> {

	private Cursor actNotPurgeCsr;
	private final IRowMapper<IActNotCltDtMgtDtl> fetchActNotPurgeCsrRm = buildNamedRowMapper(IActNotCltDtMgtDtl.class, "csrActNbr", "notPrcTs");

	public ActNotCltDtMgtDtlDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IActNotCltDtMgtDtl> getToClass() {
		return IActNotCltDtMgtDtl.class;
	}

	public DbAccessStatus openActNotPurgeCsr(String saCopyPgeDt) {
		actNotPurgeCsr = buildQuery("openActNotPurgeCsr").bind("saCopyPgeDt", saCopyPgeDt).withHoldCursorsOverCommit().open();
		return dbStatus;
	}

	public IActNotCltDtMgtDtl fetchActNotPurgeCsr(IActNotCltDtMgtDtl iActNotCltDtMgtDtl) {
		return fetch(actNotPurgeCsr, iActNotCltDtMgtDtl, fetchActNotPurgeCsrRm);
	}

	public DbAccessStatus closeActNotPurgeCsr() {
		return closeCursor(actNotPurgeCsr);
	}
}
