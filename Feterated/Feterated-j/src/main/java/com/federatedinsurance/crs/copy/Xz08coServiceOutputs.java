/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xz08c0ErrInd;
import com.federatedinsurance.crs.ws.enums.Xz08coRetCd;
import com.federatedinsurance.crs.ws.occurs.Xz08coAddlItsInfo;

/**Original name: XZ08CO-SERVICE-OUTPUTS<br>
 * Variable: XZ08CO-SERVICE-OUTPUTS from copybook XZ08CI1O<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz08coServiceOutputs {

	//==== PROPERTIES ====
	public static final int XZ08CO_ADDL_ITS_INFO_MAXOCCURS = 250;
	public static final int XZ08CO_ERR_MSG_MAXOCCURS = 10;
	public static final int XZ08CO_WNG_MSG_MAXOCCURS = 10;
	//Original name: XZ08CO-RET-CD
	private Xz08coRetCd xz08coRetCd = new Xz08coRetCd();
	//Original name: XZ08CO-ERR-MSG
	private String[] xz08coErrMsg = new String[XZ08CO_ERR_MSG_MAXOCCURS];
	//Original name: XZ08CO-WNG-MSG
	private String[] xz08coWngMsg = new String[XZ08CO_WNG_MSG_MAXOCCURS];
	//Original name: XZ08CO-ADDL-ITS-INFO
	private Xz08coAddlItsInfo[] xz08coAddlItsInfo = new Xz08coAddlItsInfo[XZ08CO_ADDL_ITS_INFO_MAXOCCURS];
	//Original name: XZ08C0-ERR-IND
	private Xz08c0ErrInd xz08c0ErrInd = new Xz08c0ErrInd();
	//Original name: XZ08C0-FAULT-CD
	private String xz08c0FaultCd = DefaultValues.stringVal(Len.XZ08C0_FAULT_CD);
	//Original name: XZ08C0-FAULT-STRING
	private String xz08c0FaultString = DefaultValues.stringVal(Len.XZ08C0_FAULT_STRING);
	//Original name: XZ08C0-FAULT-ACTOR
	private String xz08c0FaultActor = DefaultValues.stringVal(Len.XZ08C0_FAULT_ACTOR);
	//Original name: XZ08C0-FAULT-DETAIL
	private String xz08c0FaultDetail = DefaultValues.stringVal(Len.XZ08C0_FAULT_DETAIL);
	//Original name: XZ08C0-NON-SOAP-ERR-TXT
	private String xz08c0NonSoapErrTxt = DefaultValues.stringVal(Len.XZ08C0_NON_SOAP_ERR_TXT);

	//==== CONSTRUCTORS ====
	public Xz08coServiceOutputs() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int xz08coErrMsgIdx = 1; xz08coErrMsgIdx <= XZ08CO_ERR_MSG_MAXOCCURS; xz08coErrMsgIdx++) {
			setXz08coErrMsg(xz08coErrMsgIdx, DefaultValues.stringVal(Len.XZ08CO_ERR_MSG));
		}
		for (int xz08coWngMsgIdx = 1; xz08coWngMsgIdx <= XZ08CO_WNG_MSG_MAXOCCURS; xz08coWngMsgIdx++) {
			setXz08coWngMsg(xz08coWngMsgIdx, DefaultValues.stringVal(Len.XZ08CO_WNG_MSG));
		}
		for (int xz08coAddlItsInfoIdx = 1; xz08coAddlItsInfoIdx <= XZ08CO_ADDL_ITS_INFO_MAXOCCURS; xz08coAddlItsInfoIdx++) {
			xz08coAddlItsInfo[xz08coAddlItsInfoIdx - 1] = new Xz08coAddlItsInfo();
		}
	}

	public void setXz08coServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setXz08coRetMsgBytes(buffer, position);
		position += Len.XZ08CO_RET_MSG;
		for (int idx = 1; idx <= XZ08CO_ADDL_ITS_INFO_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				xz08coAddlItsInfo[idx - 1].setXz08coAddlItsInfoBytes(buffer, position);
				position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			} else {
				xz08coAddlItsInfo[idx - 1].initXz08coAddlItsInfoSpaces();
				position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
			}
		}
		setXz08c0ErrInfBytes(buffer, position);
	}

	public byte[] getXz08coServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getXz08coRetMsgBytes(buffer, position);
		position += Len.XZ08CO_RET_MSG;
		for (int idx = 1; idx <= XZ08CO_ADDL_ITS_INFO_MAXOCCURS; idx++) {
			xz08coAddlItsInfo[idx - 1].getXz08coAddlItsInfoBytes(buffer, position);
			position += Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO;
		}
		getXz08c0ErrInfBytes(buffer, position);
		return buffer;
	}

	public void initXz08coServiceOutputsLowValues() {
		initXz08coRetMsgLowValues();
		for (int idx = 1; idx <= XZ08CO_ADDL_ITS_INFO_MAXOCCURS; idx++) {
			xz08coAddlItsInfo[idx - 1].initXz08coAddlItsInfoLowValues();
		}
		initXz08c0ErrInfLowValues();
	}

	public void setXz08coRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		xz08coRetCd.setRetCd(MarshalByte.readShort(buffer, position, Xz08coRetCd.Len.XZ08CO_RET_CD));
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		setXz08coErrLisBytes(buffer, position);
		position += Len.XZ08CO_ERR_LIS;
		setXz08coWngLisBytes(buffer, position);
	}

	public byte[] getXz08coRetMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeShort(buffer, position, xz08coRetCd.getRetCd(), Xz08coRetCd.Len.XZ08CO_RET_CD);
		position += Xz08coRetCd.Len.XZ08CO_RET_CD;
		getXz08coErrLisBytes(buffer, position);
		position += Len.XZ08CO_ERR_LIS;
		getXz08coWngLisBytes(buffer, position);
		return buffer;
	}

	public void initXz08coRetMsgLowValues() {
		xz08coRetCd.setRetCd(Types.LOW_SHORT_VAL);
		initXz08coErrLisLowValues();
		initXz08coWngLisLowValues();
	}

	public void setXz08coErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ08CO_ERR_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setXz08coErrMsg(idx, MarshalByte.readString(buffer, position, Len.XZ08CO_ERR_MSG));
				position += Len.XZ08CO_ERR_MSG;
			} else {
				setXz08coErrMsg(idx, "");
				position += Len.XZ08CO_ERR_MSG;
			}
		}
	}

	public byte[] getXz08coErrLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ08CO_ERR_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getXz08coErrMsg(idx), Len.XZ08CO_ERR_MSG);
			position += Len.XZ08CO_ERR_MSG;
		}
		return buffer;
	}

	public void initXz08coErrLisLowValues() {
		for (int idx = 1; idx <= XZ08CO_ERR_MSG_MAXOCCURS; idx++) {
			xz08coErrMsg[idx - 1] = (LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08CO_ERR_MSG));
		}
	}

	public void setXz08coErrMsg(int xz08coErrMsgIdx, String xz08coErrMsg) {
		this.xz08coErrMsg[xz08coErrMsgIdx - 1] = Functions.subString(xz08coErrMsg, Len.XZ08CO_ERR_MSG);
	}

	public String getXz08coErrMsg(int xz08coErrMsgIdx) {
		return this.xz08coErrMsg[xz08coErrMsgIdx - 1];
	}

	public void setXz08coWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ08CO_WNG_MSG_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				setXz08coWngMsg(idx, MarshalByte.readString(buffer, position, Len.XZ08CO_WNG_MSG));
				position += Len.XZ08CO_WNG_MSG;
			} else {
				setXz08coWngMsg(idx, "");
				position += Len.XZ08CO_WNG_MSG;
			}
		}
	}

	public byte[] getXz08coWngLisBytes(byte[] buffer, int offset) {
		int position = offset;
		for (int idx = 1; idx <= XZ08CO_WNG_MSG_MAXOCCURS; idx++) {
			MarshalByte.writeString(buffer, position, getXz08coWngMsg(idx), Len.XZ08CO_WNG_MSG);
			position += Len.XZ08CO_WNG_MSG;
		}
		return buffer;
	}

	public void initXz08coWngLisLowValues() {
		for (int idx = 1; idx <= XZ08CO_WNG_MSG_MAXOCCURS; idx++) {
			xz08coWngMsg[idx - 1] = (LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08CO_WNG_MSG));
		}
	}

	public void setXz08coWngMsg(int xz08coWngMsgIdx, String xz08coWngMsg) {
		this.xz08coWngMsg[xz08coWngMsgIdx - 1] = Functions.subString(xz08coWngMsg, Len.XZ08CO_WNG_MSG);
	}

	public String getXz08coWngMsg(int xz08coWngMsgIdx) {
		return this.xz08coWngMsg[xz08coWngMsgIdx - 1];
	}

	public void setXz08c0ErrInfBytes(byte[] buffer, int offset) {
		int position = offset;
		xz08c0ErrInd.setXz08c0ErrInd(MarshalByte.readString(buffer, position, Xz08c0ErrInd.Len.XZ08C0_ERR_IND));
		position += Xz08c0ErrInd.Len.XZ08C0_ERR_IND;
		xz08c0FaultCd = MarshalByte.readString(buffer, position, Len.XZ08C0_FAULT_CD);
		position += Len.XZ08C0_FAULT_CD;
		xz08c0FaultString = MarshalByte.readString(buffer, position, Len.XZ08C0_FAULT_STRING);
		position += Len.XZ08C0_FAULT_STRING;
		xz08c0FaultActor = MarshalByte.readString(buffer, position, Len.XZ08C0_FAULT_ACTOR);
		position += Len.XZ08C0_FAULT_ACTOR;
		xz08c0FaultDetail = MarshalByte.readString(buffer, position, Len.XZ08C0_FAULT_DETAIL);
		position += Len.XZ08C0_FAULT_DETAIL;
		xz08c0NonSoapErrTxt = MarshalByte.readString(buffer, position, Len.XZ08C0_NON_SOAP_ERR_TXT);
	}

	public byte[] getXz08c0ErrInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xz08c0ErrInd.getXz08c0ErrInd(), Xz08c0ErrInd.Len.XZ08C0_ERR_IND);
		position += Xz08c0ErrInd.Len.XZ08C0_ERR_IND;
		MarshalByte.writeString(buffer, position, xz08c0FaultCd, Len.XZ08C0_FAULT_CD);
		position += Len.XZ08C0_FAULT_CD;
		MarshalByte.writeString(buffer, position, xz08c0FaultString, Len.XZ08C0_FAULT_STRING);
		position += Len.XZ08C0_FAULT_STRING;
		MarshalByte.writeString(buffer, position, xz08c0FaultActor, Len.XZ08C0_FAULT_ACTOR);
		position += Len.XZ08C0_FAULT_ACTOR;
		MarshalByte.writeString(buffer, position, xz08c0FaultDetail, Len.XZ08C0_FAULT_DETAIL);
		position += Len.XZ08C0_FAULT_DETAIL;
		MarshalByte.writeString(buffer, position, xz08c0NonSoapErrTxt, Len.XZ08C0_NON_SOAP_ERR_TXT);
		return buffer;
	}

	public void initXz08c0ErrInfLowValues() {
		xz08c0ErrInd.setXz08c0ErrInd(LiteralGenerator.create(Types.LOW_CHAR_VAL, Xz08c0ErrInd.Len.XZ08C0_ERR_IND));
		xz08c0FaultCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08C0_FAULT_CD);
		xz08c0FaultString = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08C0_FAULT_STRING);
		xz08c0FaultActor = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08C0_FAULT_ACTOR);
		xz08c0FaultDetail = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08C0_FAULT_DETAIL);
		xz08c0NonSoapErrTxt = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZ08C0_NON_SOAP_ERR_TXT);
	}

	public void setXz08c0FaultCd(String xz08c0FaultCd) {
		this.xz08c0FaultCd = Functions.subString(xz08c0FaultCd, Len.XZ08C0_FAULT_CD);
	}

	public String getXz08c0FaultCd() {
		return this.xz08c0FaultCd;
	}

	public void setXz08c0FaultString(String xz08c0FaultString) {
		this.xz08c0FaultString = Functions.subString(xz08c0FaultString, Len.XZ08C0_FAULT_STRING);
	}

	public String getXz08c0FaultString() {
		return this.xz08c0FaultString;
	}

	public void setXz08c0FaultActor(String xz08c0FaultActor) {
		this.xz08c0FaultActor = Functions.subString(xz08c0FaultActor, Len.XZ08C0_FAULT_ACTOR);
	}

	public String getXz08c0FaultActor() {
		return this.xz08c0FaultActor;
	}

	public void setXz08c0FaultDetail(String xz08c0FaultDetail) {
		this.xz08c0FaultDetail = Functions.subString(xz08c0FaultDetail, Len.XZ08C0_FAULT_DETAIL);
	}

	public String getXz08c0FaultDetail() {
		return this.xz08c0FaultDetail;
	}

	public void setXz08c0NonSoapErrTxt(String xz08c0NonSoapErrTxt) {
		this.xz08c0NonSoapErrTxt = Functions.subString(xz08c0NonSoapErrTxt, Len.XZ08C0_NON_SOAP_ERR_TXT);
	}

	public String getXz08c0NonSoapErrTxt() {
		return this.xz08c0NonSoapErrTxt;
	}

	public Xz08c0ErrInd getXz08c0ErrInd() {
		return xz08c0ErrInd;
	}

	public Xz08coAddlItsInfo getXz08coAddlItsInfo(int idx) {
		return xz08coAddlItsInfo[idx - 1];
	}

	public Xz08coRetCd getXz08coRetCd() {
		return xz08coRetCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZ08CO_ERR_MSG = 500;
		public static final int XZ08CO_WNG_MSG = 500;
		public static final int XZ08C0_FAULT_CD = 30;
		public static final int XZ08C0_FAULT_STRING = 256;
		public static final int XZ08C0_FAULT_ACTOR = 256;
		public static final int XZ08C0_FAULT_DETAIL = 256;
		public static final int XZ08C0_NON_SOAP_ERR_TXT = 256;
		public static final int XZ08CO_ERR_LIS = Xz08coServiceOutputs.XZ08CO_ERR_MSG_MAXOCCURS * XZ08CO_ERR_MSG;
		public static final int XZ08CO_WNG_LIS = Xz08coServiceOutputs.XZ08CO_WNG_MSG_MAXOCCURS * XZ08CO_WNG_MSG;
		public static final int XZ08CO_RET_MSG = Xz08coRetCd.Len.XZ08CO_RET_CD + XZ08CO_ERR_LIS + XZ08CO_WNG_LIS;
		public static final int XZ08C0_ERR_INF = Xz08c0ErrInd.Len.XZ08C0_ERR_IND + XZ08C0_FAULT_CD + XZ08C0_FAULT_STRING + XZ08C0_FAULT_ACTOR
				+ XZ08C0_FAULT_DETAIL + XZ08C0_NON_SOAP_ERR_TXT;
		public static final int XZ08CO_SERVICE_OUTPUTS = XZ08CO_RET_MSG
				+ Xz08coServiceOutputs.XZ08CO_ADDL_ITS_INFO_MAXOCCURS * Xz08coAddlItsInfo.Len.XZ08CO_ADDL_ITS_INFO + XZ08C0_ERR_INF;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
