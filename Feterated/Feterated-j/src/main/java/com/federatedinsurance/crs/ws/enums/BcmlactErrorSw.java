/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: BCMLACT-ERROR-SW<br>
 * Variable: BCMLACT-ERROR-SW from copybook BXCMLACT<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class BcmlactErrorSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char YES = 'Y';
	private static final char[] NO = new char[] { 'N', ' ' };

	//==== METHODS ====
	public void setErrorSw(char errorSw) {
		this.value = errorSw;
	}

	public char getErrorSw() {
		return this.value;
	}

	public boolean isYes() {
		return value == YES;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ERROR_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
