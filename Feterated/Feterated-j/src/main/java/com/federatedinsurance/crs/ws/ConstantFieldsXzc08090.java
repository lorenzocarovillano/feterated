/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZC08090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXzc08090 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-ERR
	private short maxErr = ((short) 10);
	//Original name: CF-MAX-ADDL-ITS
	private short maxAddlIts = ((short) 250);
	//Original name: CF-SOAP-SERVICE
	private String soapService = "CRSPolicyInformationCallable";
	/**Original name: CF-SOAP-OPERATION<br>
	 * <pre>    The actual soap operation:
	 *     getCncNoticeEligibleAddlInterestList is over 32 chars. The
	 *     soap operation below was changed to be under 32. This must
	 *     match the the operation in the Callable Service Operation
	 *     in IVORY.</pre>*/
	private String soapOperation = "getCncNotElgAddlInterestList";
	//Original name: CF-CALLABLE-SVC-PROGRAM
	private String callableSvcProgram = "GIICALS";
	//Original name: CF-TRACE-STOR-VALUE
	private String traceStorValue = "GWAO";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNames paragraphNames = new CfParagraphNames();
	//Original name: CF-WEB-SVC-ID
	private String webSvcId = "XZBKRPOLINF";
	//Original name: CF-WEB-SVC-LKU-PGM
	private String webSvcLkuPgm = "TS571099";
	//Original name: CF-LV-ZLINUX-SRV
	private String lvZlinuxSrv = "03";
	//Original name: CF-CONTAINER-INFO
	private CfContainerInfo containerInfo = new CfContainerInfo();

	//==== METHODS ====
	public short getMaxErr() {
		return this.maxErr;
	}

	public short getMaxAddlIts() {
		return this.maxAddlIts;
	}

	public String getSoapService() {
		return this.soapService;
	}

	public String getSoapOperation() {
		return this.soapOperation;
	}

	public String getCallableSvcProgram() {
		return this.callableSvcProgram;
	}

	public String getTraceStorValue() {
		return this.traceStorValue;
	}

	public String getWebSvcId() {
		return this.webSvcId;
	}

	public String getWebSvcLkuPgm() {
		return this.webSvcLkuPgm;
	}

	public String getLvZlinuxSrv() {
		return this.lvZlinuxSrv;
	}

	public CfContainerInfo getContainerInfo() {
		return containerInfo;
	}

	public CfParagraphNames getParagraphNames() {
		return paragraphNames;
	}
}
