/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.ICltObjRelationV;

/**Original name: CW08H-CLT-OBJ-RELATION-ROW<br>
 * Variable: CW08H-CLT-OBJ-RELATION-ROW from copybook CAWLH008<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Cw08hCltObjRelationRow implements ICltObjRelationV {

	//==== PROPERTIES ====
	//Original name: CW08H-TCH-OBJECT-KEY
	private String tchObjectKey = DefaultValues.stringVal(Len.TCH_OBJECT_KEY);
	//Original name: CW08H-HISTORY-VLD-NBR
	private short historyVldNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW08H-CIOR-EFF-DT
	private String ciorEffDt = DefaultValues.stringVal(Len.CIOR_EFF_DT);
	//Original name: CW08H-OBJ-SYS-ID
	private String objSysId = DefaultValues.stringVal(Len.OBJ_SYS_ID);
	//Original name: CW08H-CIOR-OBJ-SEQ-NBR
	private short ciorObjSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW08H-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW08H-RLT-TYP-CD
	private String rltTypCd = DefaultValues.stringVal(Len.RLT_TYP_CD);
	//Original name: CW08H-OBJ-CD
	private String objCd = DefaultValues.stringVal(Len.OBJ_CD);
	//Original name: CW08H-CIOR-SHW-OBJ-KEY
	private String ciorShwObjKey = DefaultValues.stringVal(Len.CIOR_SHW_OBJ_KEY);
	//Original name: CW08H-ADR-SEQ-NBR
	private short adrSeqNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW08H-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW08H-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW08H-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW08H-CIOR-EXP-DT
	private String ciorExpDt = DefaultValues.stringVal(Len.CIOR_EXP_DT);
	//Original name: CW08H-CIOR-EFF-ACY-TS
	private String ciorEffAcyTs = DefaultValues.stringVal(Len.CIOR_EFF_ACY_TS);
	//Original name: CW08H-CIOR-EXP-ACY-TS
	private String ciorExpAcyTs = DefaultValues.stringVal(Len.CIOR_EXP_ACY_TS);

	//==== METHODS ====
	@Override
	public void setTchObjectKey(String tchObjectKey) {
		this.tchObjectKey = Functions.subString(tchObjectKey, Len.TCH_OBJECT_KEY);
	}

	@Override
	public String getTchObjectKey() {
		return this.tchObjectKey;
	}

	@Override
	public void setHistoryVldNbr(short historyVldNbr) {
		this.historyVldNbr = historyVldNbr;
	}

	@Override
	public short getHistoryVldNbr() {
		return this.historyVldNbr;
	}

	@Override
	public void setCiorEffDt(String ciorEffDt) {
		this.ciorEffDt = Functions.subString(ciorEffDt, Len.CIOR_EFF_DT);
	}

	@Override
	public String getCiorEffDt() {
		return this.ciorEffDt;
	}

	@Override
	public void setObjSysId(String objSysId) {
		this.objSysId = Functions.subString(objSysId, Len.OBJ_SYS_ID);
	}

	@Override
	public String getObjSysId() {
		return this.objSysId;
	}

	@Override
	public void setCiorObjSeqNbr(short ciorObjSeqNbr) {
		this.ciorObjSeqNbr = ciorObjSeqNbr;
	}

	@Override
	public short getCiorObjSeqNbr() {
		return this.ciorObjSeqNbr;
	}

	@Override
	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	@Override
	public String getClientId() {
		return this.clientId;
	}

	@Override
	public void setRltTypCd(String rltTypCd) {
		this.rltTypCd = Functions.subString(rltTypCd, Len.RLT_TYP_CD);
	}

	@Override
	public String getRltTypCd() {
		return this.rltTypCd;
	}

	@Override
	public void setObjCd(String objCd) {
		this.objCd = Functions.subString(objCd, Len.OBJ_CD);
	}

	@Override
	public String getObjCd() {
		return this.objCd;
	}

	@Override
	public void setCiorShwObjKey(String ciorShwObjKey) {
		this.ciorShwObjKey = Functions.subString(ciorShwObjKey, Len.CIOR_SHW_OBJ_KEY);
	}

	@Override
	public String getCiorShwObjKey() {
		return this.ciorShwObjKey;
	}

	public String getCiorShwObjKeyFormatted() {
		return Functions.padBlanks(getCiorShwObjKey(), Len.CIOR_SHW_OBJ_KEY);
	}

	@Override
	public void setAdrSeqNbr(short adrSeqNbr) {
		this.adrSeqNbr = adrSeqNbr;
	}

	@Override
	public short getAdrSeqNbr() {
		return this.adrSeqNbr;
	}

	@Override
	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	@Override
	public String getUserId() {
		return this.userId;
	}

	@Override
	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	@Override
	public char getStatusCd() {
		return this.statusCd;
	}

	@Override
	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	@Override
	public String getTerminalId() {
		return this.terminalId;
	}

	@Override
	public void setCiorExpDt(String ciorExpDt) {
		this.ciorExpDt = Functions.subString(ciorExpDt, Len.CIOR_EXP_DT);
	}

	@Override
	public String getCiorExpDt() {
		return this.ciorExpDt;
	}

	@Override
	public void setCiorEffAcyTs(String ciorEffAcyTs) {
		this.ciorEffAcyTs = Functions.subString(ciorEffAcyTs, Len.CIOR_EFF_ACY_TS);
	}

	@Override
	public String getCiorEffAcyTs() {
		return this.ciorEffAcyTs;
	}

	@Override
	public void setCiorExpAcyTs(String ciorExpAcyTs) {
		this.ciorExpAcyTs = Functions.subString(ciorExpAcyTs, Len.CIOR_EXP_ACY_TS);
	}

	@Override
	public String getCiorExpAcyTs() {
		return this.ciorExpAcyTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_OBJECT_KEY = 20;
		public static final int CIOR_EFF_DT = 10;
		public static final int OBJ_SYS_ID = 4;
		public static final int CLIENT_ID = 20;
		public static final int RLT_TYP_CD = 4;
		public static final int OBJ_CD = 4;
		public static final int CIOR_SHW_OBJ_KEY = 30;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIOR_EXP_DT = 10;
		public static final int CIOR_EFF_ACY_TS = 26;
		public static final int CIOR_EXP_ACY_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
