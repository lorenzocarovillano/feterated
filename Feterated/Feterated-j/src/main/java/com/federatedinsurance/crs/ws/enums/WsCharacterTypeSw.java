/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-CHARACTER-TYPE-SW<br>
 * Variable: WS-CHARACTER-TYPE-SW from program CAWPCORC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsCharacterTypeSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char VALID_CHAR_FOUND = 'V';
	public static final char SPACE_FOUND = 'S';

	//==== METHODS ====
	public void setWsCharacterTypeSw(char wsCharacterTypeSw) {
		this.value = wsCharacterTypeSw;
	}

	public char getWsCharacterTypeSw() {
		return this.value;
	}

	public boolean isValidCharFound() {
		return value == VALID_CHAR_FOUND;
	}

	public void setValidCharFound() {
		value = VALID_CHAR_FOUND;
	}

	public void setSpaceFound() {
		value = SPACE_FOUND;
	}
}
