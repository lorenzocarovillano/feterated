/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.lang.ICopyable;

/**Original name: TT-KEY-FIELDS<br>
 * Variable: TT-KEY-FIELDS from program XZ0B9080<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class TtKeyFields implements ICopyable<TtKeyFields> {

	//==== PROPERTIES ====
	//Original name: TT-REC-TYP-CD
	private String recTypCd = DefaultValues.stringVal(Len.REC_TYP_CD);
	//Original name: TT-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: TT-ADR-ID
	private String adrId = DefaultValues.stringVal(Len.ADR_ID);

	//==== CONSTRUCTORS ====
	public TtKeyFields() {
	}

	public TtKeyFields(TtKeyFields keyFields) {
		this();
		this.recTypCd = keyFields.recTypCd;
		this.clientId = keyFields.clientId;
		this.adrId = keyFields.adrId;
	}

	//==== METHODS ====
	public byte[] getKeyFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, recTypCd, Len.REC_TYP_CD);
		position += Len.REC_TYP_CD;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeString(buffer, position, adrId, Len.ADR_ID);
		return buffer;
	}

	public void initKeyFieldsHighValues() {
		recTypCd = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.REC_TYP_CD);
		clientId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.CLIENT_ID);
		adrId = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.ADR_ID);
	}

	public void setRecTypCd(String recTypCd) {
		this.recTypCd = Functions.subString(recTypCd, Len.REC_TYP_CD);
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setAdrId(String adrId) {
		this.adrId = Functions.subString(adrId, Len.ADR_ID);
	}

	public String getAdrId() {
		return this.adrId;
	}

	@Override
	public TtKeyFields copy() {
		return new TtKeyFields(this);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int REC_TYP_CD = 5;
		public static final int CLIENT_ID = 20;
		public static final int ADR_ID = 20;
		public static final int KEY_FIELDS = REC_TYP_CD + CLIENT_ID + ADR_ID;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
