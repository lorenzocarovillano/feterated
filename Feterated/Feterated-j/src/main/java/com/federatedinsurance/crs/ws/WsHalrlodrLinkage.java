/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.HalrlodrFunction;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-HALRLODR-LINKAGE<br>
 * Variable: WS-HALRLODR-LINKAGE from program HALRLODR<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsHalrlodrLinkage extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: HALRLODR-FUNCTION
	private HalrlodrFunction function = new HalrlodrFunction();
	//Original name: HALRLODR-TCH-KEY
	private String tchKey = DefaultValues.stringVal(Len.TCH_KEY);
	//Original name: HALRLODR-APP-ID
	private String appId = DefaultValues.stringVal(Len.APP_ID);
	//Original name: HALRLODR-TABLE-NM
	private String tableNm = DefaultValues.stringVal(Len.TABLE_NM);
	//Original name: HALRLODR-BUS-OBJ-NM
	private String busObjNm = DefaultValues.stringVal(Len.BUS_OBJ_NM);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_HALRLODR_LINKAGE;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsHalrlodrLinkageBytes(buf);
	}

	public String getHalrlodrLockDriverStorageFormatted() {
		return getInputLinkageFormatted();
	}

	public void setWsHalrlodrLinkageBytes(byte[] buffer) {
		setWsHalrlodrLinkageBytes(buffer, 1);
	}

	public byte[] getWsHalrlodrLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALRLODR_LINKAGE];
		return getWsHalrlodrLinkageBytes(buffer, 1);
	}

	public void setWsHalrlodrLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		setInputLinkageBytes(buffer, position);
	}

	public byte[] getWsHalrlodrLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		getInputLinkageBytes(buffer, position);
		return buffer;
	}

	public String getInputLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getInputLinkageBytes());
	}

	/**Original name: HALRLODR-INPUT-LINKAGE<br>
	 * <pre>*************************************************************
	 *  HALLLODR                                                   *
	 * *************************************************************
	 *  HALRLODR (LOCK DRIVER) LINKAGE                             *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#     DATE      PROG#      DESCRIPTION                   *
	 *  ------- --------- ---------- ------------------------------*
	 *  NEW     06DEC2000 XXXX       NEW                           *
	 *  17238   29OCT2001 18448      INFRA 2.3 LOCKING ENHANCEMENTS*
	 *                               REMOVED APP-SPECIFIC 88'S.    *
	 *  34504   28AUG2003 18448      DO NOT ALLOW VIRTUAL ROW LEVEL*
	 *                               LOCKING ON INSERT REQUEST. AS *
	 *                               WITH INSERT&RETURN, MAKE SURE *
	 *                               THAT NO PESSI LOCKS EXIST AND *
	 *                               IF OPTIS EXIST THEN ZAP THEM. *
	 *                               HOWEVER, DO NOT ESTABLISH A   *
	 *                               LOCK.  IF A LOCK IS ALREADY IN*
	 *                               PLACE, THOUGH, UPDATE THE TIME*
	 * *************************************************************</pre>*/
	public byte[] getInputLinkageBytes() {
		byte[] buffer = new byte[Len.INPUT_LINKAGE];
		return getInputLinkageBytes(buffer, 1);
	}

	public void setInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		function.setFunction(MarshalByte.readString(buffer, position, HalrlodrFunction.Len.FUNCTION));
		position += HalrlodrFunction.Len.FUNCTION;
		tchKey = MarshalByte.readString(buffer, position, Len.TCH_KEY);
		position += Len.TCH_KEY;
		appId = MarshalByte.readString(buffer, position, Len.APP_ID);
		position += Len.APP_ID;
		tableNm = MarshalByte.readString(buffer, position, Len.TABLE_NM);
		position += Len.TABLE_NM;
		busObjNm = MarshalByte.readString(buffer, position, Len.BUS_OBJ_NM);
	}

	public byte[] getInputLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, function.getFunction(), HalrlodrFunction.Len.FUNCTION);
		position += HalrlodrFunction.Len.FUNCTION;
		MarshalByte.writeString(buffer, position, tchKey, Len.TCH_KEY);
		position += Len.TCH_KEY;
		MarshalByte.writeString(buffer, position, appId, Len.APP_ID);
		position += Len.APP_ID;
		MarshalByte.writeString(buffer, position, tableNm, Len.TABLE_NM);
		position += Len.TABLE_NM;
		MarshalByte.writeString(buffer, position, busObjNm, Len.BUS_OBJ_NM);
		return buffer;
	}

	public void setTchKey(String tchKey) {
		this.tchKey = Functions.subString(tchKey, Len.TCH_KEY);
	}

	public String getTchKey() {
		return this.tchKey;
	}

	public void setAppId(String appId) {
		this.appId = Functions.subString(appId, Len.APP_ID);
	}

	public String getAppId() {
		return this.appId;
	}

	public String getAppIdFormatted() {
		return Functions.padBlanks(getAppId(), Len.APP_ID);
	}

	public void setTableNm(String tableNm) {
		this.tableNm = Functions.subString(tableNm, Len.TABLE_NM);
	}

	public String getTableNm() {
		return this.tableNm;
	}

	public void setBusObjNm(String busObjNm) {
		this.busObjNm = Functions.subString(busObjNm, Len.BUS_OBJ_NM);
	}

	public String getBusObjNm() {
		return this.busObjNm;
	}

	public String getBusObjNmFormatted() {
		return Functions.padBlanks(getBusObjNm(), Len.BUS_OBJ_NM);
	}

	public HalrlodrFunction getFunction() {
		return function;
	}

	@Override
	public byte[] serialize() {
		return getWsHalrlodrLinkageBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TCH_KEY = 126;
		public static final int APP_ID = 10;
		public static final int TABLE_NM = 18;
		public static final int BUS_OBJ_NM = 32;
		public static final int INPUT_LINKAGE = HalrlodrFunction.Len.FUNCTION + TCH_KEY + APP_ID + TABLE_NM + BUS_OBJ_NM;
		public static final int WS_HALRLODR_LINKAGE = INPUT_LINKAGE;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
