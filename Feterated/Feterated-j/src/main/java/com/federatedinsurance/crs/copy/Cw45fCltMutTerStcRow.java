/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW45F-CLT-MUT-TER-STC-ROW<br>
 * Variable: CW45F-CLT-MUT-TER-STC-ROW from copybook CAWLF045<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw45fCltMutTerStcRow {

	//==== PROPERTIES ====
	//Original name: CW45F-CLT-MUT-TER-STC-CSUM
	private String cltMutTerStcCsum = DefaultValues.stringVal(Len.CLT_MUT_TER_STC_CSUM);
	//Original name: CW45F-TER-CLT-ID-KCRE
	private String terCltIdKcre = DefaultValues.stringVal(Len.TER_CLT_ID_KCRE);
	//Original name: CW45F-TER-LEVEL-CD-KCRE
	private String terLevelCdKcre = DefaultValues.stringVal(Len.TER_LEVEL_CD_KCRE);
	//Original name: CW45F-TER-NBR-KCRE
	private String terNbrKcre = DefaultValues.stringVal(Len.TER_NBR_KCRE);
	//Original name: CW45F-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW45F-TER-CLT-ID
	private String terCltId = DefaultValues.stringVal(Len.TER_CLT_ID);
	//Original name: CW45F-TER-LEVEL-CD
	private String terLevelCd = DefaultValues.stringVal(Len.TER_LEVEL_CD);
	//Original name: CW45F-TER-NBR
	private String terNbr = DefaultValues.stringVal(Len.TER_NBR);
	//Original name: CW45F-TER-CLT-ID-CI
	private char terCltIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-TER-LEVEL-CD-CI
	private char terLevelCdCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-TER-NBR-CI
	private char terNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW45F-CLT-MUT-TER-STC-DATA
	private Cw45fCltMutTerStcData cltMutTerStcData = new Cw45fCltMutTerStcData();

	//==== METHODS ====
	public void setCw45fCltMutTerStcRowFormatted(String data) {
		byte[] buffer = new byte[Len.CW45F_CLT_MUT_TER_STC_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CW45F_CLT_MUT_TER_STC_ROW);
		setCw45fCltMutTerStcRowBytes(buffer, 1);
	}

	public String getCw45fCltMutTerStcRowFormatted() {
		return MarshalByteExt.bufferToStr(getCw45fCltMutTerStcRowBytes());
	}

	public byte[] getCw45fCltMutTerStcRowBytes() {
		byte[] buffer = new byte[Len.CW45F_CLT_MUT_TER_STC_ROW];
		return getCw45fCltMutTerStcRowBytes(buffer, 1);
	}

	public void setCw45fCltMutTerStcRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setCltMutTerStcFixedBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_FIXED;
		setCltMutTerStcDatesBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_DATES;
		setCltMutTerStcKeyBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_KEY;
		setCltMutTerStcKeyCiBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_KEY_CI;
		cltMutTerStcData.setCltMutTerStcDataBytes(buffer, position);
	}

	public byte[] getCw45fCltMutTerStcRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getCltMutTerStcFixedBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_FIXED;
		getCltMutTerStcDatesBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_DATES;
		getCltMutTerStcKeyBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_KEY;
		getCltMutTerStcKeyCiBytes(buffer, position);
		position += Len.CLT_MUT_TER_STC_KEY_CI;
		cltMutTerStcData.getCltMutTerStcDataBytes(buffer, position);
		return buffer;
	}

	public void setCltMutTerStcFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		cltMutTerStcCsum = MarshalByte.readFixedString(buffer, position, Len.CLT_MUT_TER_STC_CSUM);
		position += Len.CLT_MUT_TER_STC_CSUM;
		terCltIdKcre = MarshalByte.readString(buffer, position, Len.TER_CLT_ID_KCRE);
		position += Len.TER_CLT_ID_KCRE;
		terLevelCdKcre = MarshalByte.readString(buffer, position, Len.TER_LEVEL_CD_KCRE);
		position += Len.TER_LEVEL_CD_KCRE;
		terNbrKcre = MarshalByte.readString(buffer, position, Len.TER_NBR_KCRE);
	}

	public byte[] getCltMutTerStcFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, cltMutTerStcCsum, Len.CLT_MUT_TER_STC_CSUM);
		position += Len.CLT_MUT_TER_STC_CSUM;
		MarshalByte.writeString(buffer, position, terCltIdKcre, Len.TER_CLT_ID_KCRE);
		position += Len.TER_CLT_ID_KCRE;
		MarshalByte.writeString(buffer, position, terLevelCdKcre, Len.TER_LEVEL_CD_KCRE);
		position += Len.TER_LEVEL_CD_KCRE;
		MarshalByte.writeString(buffer, position, terNbrKcre, Len.TER_NBR_KCRE);
		return buffer;
	}

	public void setTerCltIdKcre(String terCltIdKcre) {
		this.terCltIdKcre = Functions.subString(terCltIdKcre, Len.TER_CLT_ID_KCRE);
	}

	public String getTerCltIdKcre() {
		return this.terCltIdKcre;
	}

	public void setTerLevelCdKcre(String terLevelCdKcre) {
		this.terLevelCdKcre = Functions.subString(terLevelCdKcre, Len.TER_LEVEL_CD_KCRE);
	}

	public String getTerLevelCdKcre() {
		return this.terLevelCdKcre;
	}

	public void setTerNbrKcre(String terNbrKcre) {
		this.terNbrKcre = Functions.subString(terNbrKcre, Len.TER_NBR_KCRE);
	}

	public String getTerNbrKcre() {
		return this.terNbrKcre;
	}

	public void setCltMutTerStcDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getCltMutTerStcDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public void setCltMutTerStcKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		terCltId = MarshalByte.readString(buffer, position, Len.TER_CLT_ID);
		position += Len.TER_CLT_ID;
		terLevelCd = MarshalByte.readString(buffer, position, Len.TER_LEVEL_CD);
		position += Len.TER_LEVEL_CD;
		terNbr = MarshalByte.readString(buffer, position, Len.TER_NBR);
	}

	public byte[] getCltMutTerStcKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, terCltId, Len.TER_CLT_ID);
		position += Len.TER_CLT_ID;
		MarshalByte.writeString(buffer, position, terLevelCd, Len.TER_LEVEL_CD);
		position += Len.TER_LEVEL_CD;
		MarshalByte.writeString(buffer, position, terNbr, Len.TER_NBR);
		return buffer;
	}

	public void setTerCltId(String terCltId) {
		this.terCltId = Functions.subString(terCltId, Len.TER_CLT_ID);
	}

	public String getTerCltId() {
		return this.terCltId;
	}

	public void setTerLevelCd(String terLevelCd) {
		this.terLevelCd = Functions.subString(terLevelCd, Len.TER_LEVEL_CD);
	}

	public String getTerLevelCd() {
		return this.terLevelCd;
	}

	public void setTerNbr(String terNbr) {
		this.terNbr = Functions.subString(terNbr, Len.TER_NBR);
	}

	public String getTerNbr() {
		return this.terNbr;
	}

	public void setCltMutTerStcKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		terCltIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terLevelCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getCltMutTerStcKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, terCltIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terLevelCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terNbrCi);
		return buffer;
	}

	public void setTerCltIdCi(char terCltIdCi) {
		this.terCltIdCi = terCltIdCi;
	}

	public char getTerCltIdCi() {
		return this.terCltIdCi;
	}

	public void setTerLevelCdCi(char terLevelCdCi) {
		this.terLevelCdCi = terLevelCdCi;
	}

	public char getTerLevelCdCi() {
		return this.terLevelCdCi;
	}

	public void setTerNbrCi(char terNbrCi) {
		this.terNbrCi = terNbrCi;
	}

	public char getTerNbrCi() {
		return this.terNbrCi;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLT_MUT_TER_STC_CSUM = 9;
		public static final int TER_CLT_ID_KCRE = 32;
		public static final int TER_LEVEL_CD_KCRE = 32;
		public static final int TER_NBR_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int TER_CLT_ID = 20;
		public static final int TER_LEVEL_CD = 4;
		public static final int TER_NBR = 4;
		public static final int CLT_MUT_TER_STC_FIXED = CLT_MUT_TER_STC_CSUM + TER_CLT_ID_KCRE + TER_LEVEL_CD_KCRE + TER_NBR_KCRE;
		public static final int CLT_MUT_TER_STC_DATES = TRANS_PROCESS_DT;
		public static final int CLT_MUT_TER_STC_KEY = TER_CLT_ID + TER_LEVEL_CD + TER_NBR;
		public static final int TER_CLT_ID_CI = 1;
		public static final int TER_LEVEL_CD_CI = 1;
		public static final int TER_NBR_CI = 1;
		public static final int CLT_MUT_TER_STC_KEY_CI = TER_CLT_ID_CI + TER_LEVEL_CD_CI + TER_NBR_CI;
		public static final int CW45F_CLT_MUT_TER_STC_ROW = CLT_MUT_TER_STC_FIXED + CLT_MUT_TER_STC_DATES + CLT_MUT_TER_STC_KEY
				+ CLT_MUT_TER_STC_KEY_CI + Cw45fCltMutTerStcData.Len.CLT_MUT_TER_STC_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
