/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.data.NumericDisplaySigned;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0P90A0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0p90a0 {

	//==== PROPERTIES ====
	//Original name: WS-MAX-TTY-CERT-ROWS
	private short maxTtyCertRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-MAX-TTY-ROWS
	private short maxTtyRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: WS-EIBRESP-CD
	private short eibrespCd = DefaultValues.SHORT_VAL;
	//Original name: WS-EIBRESP2-CD
	private short eibresp2Cd = DefaultValues.SHORT_VAL;
	//Original name: WS-ERROR-CHECK-INFO
	private WsErrorCheckInfo errorCheckInfo = new WsErrorCheckInfo();
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0P90A0";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-TTY-LIST
	private String busObjNmTtyList = "XZ_PREPARE_THIRD_PARTY_LIST";
	//Original name: WS-OPERATIONS-CALLED
	private WsOperationsCalledXz0p90a0 operationsCalled = new WsOperationsCalledXz0p90a0();

	//==== METHODS ====
	public void setMaxTtyCertRows(short maxTtyCertRows) {
		this.maxTtyCertRows = maxTtyCertRows;
	}

	public short getMaxTtyCertRows() {
		return this.maxTtyCertRows;
	}

	public void setMaxTtyRows(short maxTtyRows) {
		this.maxTtyRows = maxTtyRows;
	}

	public short getMaxTtyRows() {
		return this.maxTtyRows;
	}

	public void setEibrespCd(short eibrespCd) {
		this.eibrespCd = eibrespCd;
	}

	public short getEibrespCd() {
		return this.eibrespCd;
	}

	public String getEibrespCdFormatted() {
		return NumericDisplaySigned.asString(getEibrespCd(), Len.EIBRESP_CD);
	}

	public String getEibrespCdAsString() {
		return getEibrespCdFormatted();
	}

	public void setEibresp2Cd(short eibresp2Cd) {
		this.eibresp2Cd = eibresp2Cd;
	}

	public short getEibresp2Cd() {
		return this.eibresp2Cd;
	}

	public String getEibresp2CdFormatted() {
		return NumericDisplaySigned.asString(getEibresp2Cd(), Len.EIBRESP2_CD);
	}

	public String getEibresp2CdAsString() {
		return getEibresp2CdFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmTtyList() {
		return this.busObjNmTtyList;
	}

	public WsErrorCheckInfo getErrorCheckInfo() {
		return errorCheckInfo;
	}

	public WsOperationsCalledXz0p90a0 getOperationsCalled() {
		return operationsCalled;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int EIBRESP_CD = 4;
		public static final int EIBRESP2_CD = 4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
