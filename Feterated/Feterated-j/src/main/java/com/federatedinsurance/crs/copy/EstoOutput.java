/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;
import com.federatedinsurance.crs.ws.enums.EstoErrFloodInd;
import com.federatedinsurance.crs.ws.enums.EstoErrStoreDetailCd;
import com.federatedinsurance.crs.ws.enums.EstoErrStoreReturnCd;

/**Original name: ESTO-OUTPUT<br>
 * Variable: ESTO-OUTPUT from copybook HALLESTO<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class EstoOutput {

	//==== PROPERTIES ====
	//Original name: ESTO-ERR-STORE-RETURN-CD
	private EstoErrStoreReturnCd storeReturnCd = new EstoErrStoreReturnCd();
	//Original name: ESTO-ERR-STORE-DETAIL-CD
	private EstoErrStoreDetailCd storeDetailCd = new EstoErrStoreDetailCd();
	//Original name: ESTO-ERR-RESP-CD
	private String respCd = DefaultValues.stringVal(Len.RESP_CD);
	//Original name: ESTO-ERR-RESP2-CD
	private String resp2Cd = DefaultValues.stringVal(Len.RESP2_CD);
	//Original name: ESTO-ERR-SQLCODE
	private String sqlcode = DefaultValues.stringVal(Len.SQLCODE);
	//Original name: ESTO-ERR-SQLERRMC
	private String sqlerrmc = DefaultValues.stringVal(Len.SQLERRMC);
	//Original name: ESTO-ERR-FLOOD-IND
	private EstoErrFloodInd floodInd = new EstoErrFloodInd();
	//Original name: FILLER-ESTO-OUTPUT
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setEstoOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		storeReturnCd.value = MarshalByte.readFixedString(buffer, position, EstoErrStoreReturnCd.Len.STORE_RETURN_CD);
		position += EstoErrStoreReturnCd.Len.STORE_RETURN_CD;
		storeDetailCd.value = MarshalByte.readFixedString(buffer, position, EstoErrStoreDetailCd.Len.STORE_DETAIL_CD);
		position += EstoErrStoreDetailCd.Len.STORE_DETAIL_CD;
		respCd = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.RESP_CD), Len.RESP_CD);
		position += Len.RESP_CD;
		resp2Cd = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.RESP2_CD), Len.RESP2_CD);
		position += Len.RESP2_CD;
		sqlcode = Functions.padBlanks(MarshalByte.readString(buffer, position, Len.SQLCODE), Len.SQLCODE);
		position += Len.SQLCODE;
		sqlerrmc = MarshalByte.readString(buffer, position, Len.SQLERRMC);
		position += Len.SQLERRMC;
		floodInd.setFloodInd(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getEstoOutputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, storeReturnCd.value, EstoErrStoreReturnCd.Len.STORE_RETURN_CD);
		position += EstoErrStoreReturnCd.Len.STORE_RETURN_CD;
		MarshalByte.writeString(buffer, position, storeDetailCd.value, EstoErrStoreDetailCd.Len.STORE_DETAIL_CD);
		position += EstoErrStoreDetailCd.Len.STORE_DETAIL_CD;
		MarshalByte.writeString(buffer, position, respCd, Len.RESP_CD);
		position += Len.RESP_CD;
		MarshalByte.writeString(buffer, position, resp2Cd, Len.RESP2_CD);
		position += Len.RESP2_CD;
		MarshalByte.writeString(buffer, position, sqlcode, Len.SQLCODE);
		position += Len.SQLCODE;
		MarshalByte.writeString(buffer, position, sqlerrmc, Len.SQLERRMC);
		position += Len.SQLERRMC;
		MarshalByte.writeChar(buffer, position, floodInd.getFloodInd());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setEstoErrRespCd(long estoErrRespCd) {
		this.respCd = PicFormatter.display("-Z(8)9").format(estoErrRespCd).toString();
	}

	public void setRespCdFormatted(String respCd) {
		this.respCd = PicFormatter.display("-Z(8)9").format(respCd).toString();
	}

	public long getRespCd() {
		return PicParser.display("-Z(8)9").parseLong(this.respCd);
	}

	public void setEstoErrResp2Cd(long estoErrResp2Cd) {
		this.resp2Cd = PicFormatter.display("-Z(8)9").format(estoErrResp2Cd).toString();
	}

	public void setResp2CdFormatted(String resp2Cd) {
		this.resp2Cd = PicFormatter.display("-Z(8)9").format(resp2Cd).toString();
	}

	public long getResp2Cd() {
		return PicParser.display("-Z(8)9").parseLong(this.resp2Cd);
	}

	public void setSqlcode(long sqlcode) {
		this.sqlcode = PicFormatter.display("-Z(8)9").format(sqlcode).toString();
	}

	public void setSqlcodeFormatted(String sqlcode) {
		this.sqlcode = PicFormatter.display("-Z(8)9").format(sqlcode).toString();
	}

	public long getSqlcode() {
		return PicParser.display("-Z(8)9").parseLong(this.sqlcode);
	}

	public void setSqlerrmc(String sqlerrmc) {
		this.sqlerrmc = Functions.subString(sqlerrmc, Len.SQLERRMC);
	}

	public String getSqlerrmc() {
		return this.sqlerrmc;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public EstoErrFloodInd getFloodInd() {
		return floodInd;
	}

	public EstoErrStoreDetailCd getStoreDetailCd() {
		return storeDetailCd;
	}

	public EstoErrStoreReturnCd getStoreReturnCd() {
		return storeReturnCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int RESP_CD = 10;
		public static final int RESP2_CD = 10;
		public static final int SQLCODE = 10;
		public static final int SQLERRMC = 70;
		public static final int FLR1 = 50;
		public static final int ESTO_OUTPUT = EstoErrStoreReturnCd.Len.STORE_RETURN_CD + EstoErrStoreDetailCd.Len.STORE_DETAIL_CD + RESP_CD + RESP2_CD
				+ SQLCODE + SQLERRMC + EstoErrFloodInd.Len.FLOOD_IND + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
