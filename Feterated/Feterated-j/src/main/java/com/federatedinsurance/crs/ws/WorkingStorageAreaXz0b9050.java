/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B9050<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b9050 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount = ((short) 0);
	public static final short NOTHING_FOUND = ((short) 0);
	//Original name: WS-POL-ADR-COUNT
	private short polAdrCount = ((short) 0);
	public static final short NO_POL_ADR_POLS_FOUND = ((short) 0);
	//Original name: WS-POL-OWN-COUNT
	private short polOwnCount = ((short) 0);
	public static final short NO_POL_OWN_POLS_FOUND = ((short) 0);
	//Original name: WS-MAX-ROWS
	private String maxRows = DefaultValues.stringVal(Len.MAX_ROWS);
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B9050";
	//Original name: WS-APPLICATION-NM
	private String applicationNm = "CRS";
	//Original name: WS-BUS-OBJ-NM-HEADER
	private String busObjNmHeader = "XZ_GET_POL_LIST_HEADER";
	//Original name: WS-BUS-OBJ-NM-DETAIL
	private String busObjNmDetail = "XZ_GET_POL_LIST_DETAIL";
	//Original name: WS-TOT-FEE-AMT-POL-NBR
	private String totFeeAmtPolNbr = DefaultValues.stringVal(Len.TOT_FEE_AMT_POL_NBR);
	//Original name: WS-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);

	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount = rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount == NOTHING_FOUND;
	}

	public void setNothingFound() {
		rowCount = NOTHING_FOUND;
	}

	public void setPolAdrCount(short polAdrCount) {
		this.polAdrCount = polAdrCount;
	}

	public short getPolAdrCount() {
		return this.polAdrCount;
	}

	public boolean isNoPolAdrPolsFound() {
		return polAdrCount == NO_POL_ADR_POLS_FOUND;
	}

	public void setPolOwnCount(short polOwnCount) {
		this.polOwnCount = polOwnCount;
	}

	public short getPolOwnCount() {
		return this.polOwnCount;
	}

	public void setMaxRows(long maxRows) {
		this.maxRows = PicFormatter.display("Z(3)9").format(maxRows).toString();
	}

	public long getMaxRows() {
		return PicParser.display("Z(3)9").parseLong(this.maxRows);
	}

	public String getMaxRowsFormatted() {
		return this.maxRows;
	}

	public String getMaxRowsAsString() {
		return getMaxRowsFormatted();
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getApplicationNm() {
		return this.applicationNm;
	}

	public String getBusObjNmHeader() {
		return this.busObjNmHeader;
	}

	public String getBusObjNmDetail() {
		return this.busObjNmDetail;
	}

	public void setTotFeeAmtPolNbr(String totFeeAmtPolNbr) {
		this.totFeeAmtPolNbr = Functions.subString(totFeeAmtPolNbr, Len.TOT_FEE_AMT_POL_NBR);
	}

	public String getTotFeeAmtPolNbr() {
		return this.totFeeAmtPolNbr;
	}

	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_ROWS = 4;
		public static final int SEARCH_POL_TYP_CD = 3;
		public static final int TOT_FEE_AMT_POL_NBR = 25;
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
