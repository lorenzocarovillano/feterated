/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-DATA-CHANGED-SW<br>
 * Variable: WS-DATA-CHANGED-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsDataChangedSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CHANGED = 'Y';
	public static final char NOT_CHANGED = 'N';

	//==== METHODS ====
	public void setDataChangedSw(char dataChangedSw) {
		this.value = dataChangedSw;
	}

	public char getDataChangedSw() {
		return this.value;
	}

	public void setWsDataChanged() {
		value = CHANGED;
	}

	public boolean isWsDataNotChanged() {
		return value == NOT_CHANGED;
	}

	public void setWsDataNotChanged() {
		value = NOT_CHANGED;
	}
}
