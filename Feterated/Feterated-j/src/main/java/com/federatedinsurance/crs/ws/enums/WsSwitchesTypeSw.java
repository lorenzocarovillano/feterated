/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-SWITCHES-TYPE-SW<br>
 * Variable: WS-SWITCHES-TYPE-SW from program CAWS002<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsSwitchesTypeSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char MASTER = 'M';
	public static final char OBJECT_FLD = 'O';

	//==== METHODS ====
	public void setSwitchesTypeSw(char switchesTypeSw) {
		this.value = switchesTypeSw;
	}

	public char getSwitchesTypeSw() {
		return this.value;
	}

	public void setMaster() {
		value = MASTER;
	}

	public boolean isObjectFld() {
		return value == OBJECT_FLD;
	}

	public void setObjectFld() {
		value = OBJECT_FLD;
	}
}
