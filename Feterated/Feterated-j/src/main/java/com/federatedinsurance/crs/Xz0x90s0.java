/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.DriverSpecificData;
import com.federatedinsurance.crs.ws.ConstantFieldsXz0x90s0;
import com.federatedinsurance.crs.ws.DfhcommareaTs020000;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXz0x90s0;
import com.federatedinsurance.crs.ws.ptr.DfhcommareaMu0x0004;
import com.federatedinsurance.crs.ws.ptr.LContractLocation;
import com.federatedinsurance.crs.ws.ptr.LFrameworkRequestAreaXz0x90s0;
import com.federatedinsurance.crs.ws.ptr.LFrameworkResponseAreaXz0x90s0;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x90s0;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0X90S0<br>
 * <pre>AUTHOR.       ANTHONY LINDSAY
 * DATE-WRITTEN. 15 MAR 2010.
 * ***************************************************************
 *   PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
 *                     UOW        : XZ_ACY_PND_CNC_TMN_POL_LIST  *
 *                     OPERATIONS : GetAcyPndCncTmnPolListByAct  *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
 *              EXECUTION OF FRAMEWORK MAIN DRIVER               *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
 *                        PROGRAM OR FROM AN IVORY WRAPPER       *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         AND SHARED MEMORY                     *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                         AND SHARED MEMORY                     *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
 *  YJ249    27APR07   E404NEM   STDS CHGS                       *
 *  TL000113 01JUL09   E404DMA   USE NEW PROXY COMMON LOGIC      *
 *                               COPYBOOK                        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #     DATE       EMP ID             DESCRIPTION         *
 *  --------  ----------  -------  ------------------------------*
 * TO0760222  03/15/2010  E404ABL  INITIAL PROGRAM               *
 *                                                               *
 * ***************************************************************</pre>*/
public class Xz0x90s0 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0x90s0 constantFields = new ConstantFieldsXz0x90s0();
	/**Original name: SS-POL<br>
	 * <pre> POLICY INDEX</pre>*/
	private short ssPol = ((short) 1);
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0x90s0 workingStorageArea = new WorkingStorageAreaXz0x90s0();
	//Original name: MAIN-DRIVER-DATA
	private DfhcommareaTs020000 mainDriverData = new DfhcommareaTs020000();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaMu0x0004 dfhcommarea = new DfhcommareaMu0x0004(null);
	/**Original name: L-SERVICE-CONTRACT-AREA<br>
	 * <pre>* SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
	 * * BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK</pre>*/
	private LServiceContractAreaXz0x90s0 lServiceContractArea = new LServiceContractAreaXz0x90s0(null);
	/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
	 * <pre>* "FRAMEWORK" REQUEST AREA FOR THIS OPERATION</pre>*/
	private LFrameworkRequestAreaXz0x90s0 lFrameworkRequestArea = new LFrameworkRequestAreaXz0x90s0(null);
	/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
	 * <pre>* "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION</pre>*/
	private LFrameworkResponseAreaXz0x90s0 lFrameworkResponseArea = new LFrameworkResponseAreaXz0x90s0(null);
	/**Original name: L-CONTRACT-LOCATION<br>
	 * <pre>***************************************************************
	 *  TSC21PRO - PROXY COMMON LOGIC                                *
	 * ***************************************************************
	 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR COMMON LOGIC      *
	 *         COPYBOOK VERSIONING.                                  *
	 *         APPLICATION CODING.                                   *
	 *                                                               *
	 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
	 *                                                               *
	 *    WR #    DATE     EMP ID              DESCRIPTION           *
	 *  -------- --------- -------   ------------------------------- *
	 *  TS129    06MAR06   E404LJL   INITIAL VERSION                 *
	 *  TL000113 30JUN09   E404DMA   EXPAND FUNCTIONALITY SUCH THAT  *
	 *                               DATA CAN BE PASSED VIA CHANNEL  *
	 *                               & CONTAINERS, COMMAREA ONLY, OR *
	 *                               REFERENCE STORAGE (POINTERS).   *
	 * ***************************************************************</pre>*/
	private LContractLocation lContractLocation = new LContractLocation(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaMu0x0004 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea.assignBc(dfhcommarea);
		mainline();
		exit();
		return 0;
	}

	public static Xz0x90s0 getInstance() {
		return (Programs.getInstance(Xz0x90s0.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-CALL-FRAMEWORK
		//              THRU 3000-EXIT.
		callFramework();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
	 *  DETERMINE METHOD OF CAPTURING INPUT CONTRACTS TO GET         *
	 *  ADDRESSABILITY TO THEM.                                      *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: MOVE CF-PROGRAM-NAME        TO WS-PROGRAM-NAME.
		workingStorageArea.setWsProgramName(constantFields.getProgramName());
		// COB_CODE: PERFORM 2500-CAPTURE-INPUT-CONTRACTS
		//              THRU 2500-EXIT.
		captureInputContracts();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: INITIALIZE DSD-ERROR-HANDLING-PARMS
		//                      PPC-ERROR-HANDLING-PARMS.
		initDsdErrorHandlingParms();
		initPpcErrorHandlingParms();
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-FW-MEMORY
		//              THRU 2100-EXIT.
		allocateFwMemory();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-GET-USERID
		//              THRU 2200-EXIT.
		getUserid();
	}

	/**Original name: 2100-ALLOCATE-FW-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE FRAMEWORK REQUEST AND RESPONSE AREAS            *
	 *  FOR THIS OPERATION.                                          *
	 * ***************************************************************</pre>*/
	private void allocateFwMemory() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LFrameworkRequestAreaXz0x90s0.Len.L_FRAMEWORK_REQUEST_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
		//                                       TO MA-INPUT-POINTER.
		lFrameworkRequestArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LFrameworkRequestAreaXz0x90s0.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-REQUEST-AREA.
		initLFrameworkRequestArea();
		// COB_CODE: MOVE LENGTH OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(LFrameworkResponseAreaXz0x90s0.Len.L_FRAMEWORK_RESPONSE_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//              SET(MA-OUTPUT-POINTER)
		//              FLENGTH(MA-OUTPUT-DATA-SIZE)
		//              INITIMG(CF-BLANK)
		//              RESP(WS-RESPONSE-CODE)
		//              RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaOutputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaOutputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2100-ALLOCATE-FW-MEMORY'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2100-ALLOCATE-FW-MEMORY", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA
		//                                       TO MA-OUTPUT-POINTER.
		lFrameworkResponseArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaOutputPointer(), LFrameworkResponseAreaXz0x90s0.class)));
		// COB_CODE: INITIALIZE L-FRAMEWORK-RESPONSE-AREA.
		initLFrameworkResponseArea();
	}

	/**Original name: 2200-GET-USERID<br>
	 * <pre>***************************************************************
	 *  GET THE USERID FOR THIS TRANSACTION FROM CICS AND POPULATE   *
	 *  THE FRAMEWORK LEVEL USERID FIELD.  THIS CAN BE OVERRIDEN BY  *
	 *  A USER-SUPPLIED USERID IF NECESSARY.                         *
	 * ***************************************************************</pre>*/
	private void getUserid() {
		// COB_CODE: INITIALIZE CSC-AUTH-USERID.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid("");
		// COB_CODE: EXEC CICS ASSIGN
		//               USERID(CSC-AUTH-USERID)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(execContext.getUserId());
		execContext.clearStatus();
	}

	/**Original name: 2500-CAPTURE-INPUT-CONTRACTS<br>
	 * <pre>***************************************************************
	 *  DETERMINE IF THE DATA WAS PASSED USING CHANNELS AND          *
	 *  CONTAINERS, COMMAREA AND REFERENCE STORAGE, OR COMMARE ONLY. *
	 *  BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
	 *  CONTRACT TO BE USED.                                         *
	 * ***************************************************************
	 *     IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
	 *      THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
	 *      LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
	 *      THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
	 *      COMMON COPYBOOK.</pre>*/
	private void captureInputContracts() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EIBCALEN = +0
		//                      THRU 2510-EXIT
		//               WHEN EIBCALEN = LENGTH OF PROXY-PROGRAM-COMMON
		//                             + LENGTH OF L-SERVICE-CONTRACT-AREA
		//                      THRU 2520-EXIT
		//               WHEN OTHER
		//                      THRU 2530-EXIT
		//           END-EVALUATE.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 2510-CAPTURE-FROM-CHANNEL
			//              THRU 2510-EXIT
			captureFromChannel();
		} else if (execContext.getCommAreaLen() == DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON
				+ LServiceContractAreaXz0x90s0.Len.L_SERVICE_CONTRACT_AREA) {
			// COB_CODE: PERFORM 2520-CAPTURE-FROM-COMMAREA
			//              THRU 2520-EXIT
			captureFromCommarea();
		} else {
			// COB_CODE: PERFORM 2530-CAPTURE-FROM-REF-STORAGE
			//              THRU 2530-EXIT
			captureFromRefStorage();
		}
	}

	/**Original name: 2510-CAPTURE-FROM-CHANNEL<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
	 *  CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
	 *  CONTRACT.                                                    *
	 * ***************************************************************</pre>*/
	private void captureFromChannel() {
		// COB_CODE: PERFORM 2511-ALLOCATE-PROXY-CNT
		//              THRU 2511-EXIT.
		allocateProxyCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2512-GET-PROXY-COMMON-CNT
		//              THRU 2512-EXIT.
		getProxyCommonCnt();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		// COB_CODE: PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
		//              THRU 2513-EXIT.
		allocateServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
		//    HAVE THE DEVELOPER SPECIFY THE LOCATION AND LENGTHS OF THE
		//     SERVICE CONTRACT INPUT AND OUTPUT AREAS.
		// COB_CODE: PERFORM 2514-CAPTURE-SVC-CTT-ATB
		//              THRU 2514-EXIT.
		captureSvcCttAtb();
		// COB_CODE: PERFORM 2515-GET-SERVICE-CONTRACT
		//              THRU 2515-EXIT.
		getServiceContract();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 2510-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 2510-EXIT
			return;
		}
	}

	/**Original name: 2511-ALLOCATE-PROXY-CNT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
	 * ***************************************************************</pre>*/
	private void allocateProxyCnt() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF DFHCOMMAREA  TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(DfhcommareaMu0x0004.Len.DFHCOMMAREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
		//!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2511-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2511-ALLOCATE-PROXY-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2511-ALLOCATE-PROXY-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2511-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF DFHCOMMAREA  TO MA-INPUT-POINTER.
		dfhcommarea = ((pointerManager.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(),
				DfhcommareaMu0x0004.class)));
		// COB_CODE: INITIALIZE DFHCOMMAREA.
		initDfhcommarea();
		//    WITH THE PROXY COMMON CONTRACT ALLOCATED, SPECIFY BOTH THE
		//     SIZE AND LOCATION OF THE INPUT AND OUTPUT AREAS.
		// COB_CODE: SET WS-PC-INPUT-PTR         TO ADDRESS OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: SET WS-PC-OUTPUT-PTR        TO ADDRESS OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputPtr(pointerManager.addressOf(dfhcommarea, DfhcommareaMu0x0004.Pos.PPC_OUTPUT_PARMS));
		// COB_CODE: COMPUTE WS-PC-INPUT-LEN = LENGTH OF PPC-INPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setInputLen(DfhcommareaMu0x0004.Len.PPC_INPUT_PARMS);
		// COB_CODE: COMPUTE WS-PC-OUTPUT-LEN = LENGTH OF PPC-OUTPUT-PARMS.
		workingStorageArea.getWsProxyContractAtb().setOutputLen(DfhcommareaMu0x0004.Len.PPC_OUTPUT_PARMS);
	}

	/**Original name: 2512-GET-PROXY-COMMON-CNT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
	 * ***************************************************************</pre>*/
	private void getProxyCommonCnt() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-PC-INPUT-LEN))
		//               FLENGTH    (WS-PC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getProxyCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2512-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2512-GET-PROXY-COMMON-CNT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2512-GET-PROXY-COMMON-CNT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2512-EXIT
			return;
		}
	}

	/**Original name: 2513-ALLOCATE-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
	 * ***************************************************************</pre>*/
	private void allocateServiceContract() {
		ConcatUtil concatUtil = null;
		// COB_CODE: INITIALIZE MEMORY-ALLOCATION-PARMS.
		initMemoryAllocationParms();
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-DATA-SIZE.
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(LServiceContractAreaXz0x90s0.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET(MA-INPUT-POINTER)
		//               FLENGTH(MA-INPUT-DATA-SIZE)
		//               INITIMG(CF-BLANK)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		mainDriverData.getCommunicationShellCommon().setMaInputPointer(cicsStorageManager.getmainNonshared(execContext,
				mainDriverData.getCommunicationShellCommon().getMaInputDataSize(), constantFields.getBlank()));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2513-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2513-ALLOCATE-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2513-ALLOCATE-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2513-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO MA-INPUT-POINTER.
		lServiceContractArea = ((pointerManager
				.resolve(mainDriverData.getCommunicationShellCommon().getMaInputPointer(), LServiceContractAreaXz0x90s0.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT-AREA.
		initLServiceContractArea();
	}

	/**Original name: 2514-CAPTURE-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  CAPTURE BOTH THE SIZE AND LOCATION OF THE INPUT AND OUTPUT   *
	 *  AREAS OF THE SERVICE CONTRACT AS SPECIFIED BY THE DEVELOPER  *
	 * ***************************************************************</pre>*/
	private void captureSvcCttAtb() {
		// COB_CODE: PERFORM 12514-SET-SVC-CTT-ATB
		//              THRU 12514-EXIT.
		setSvcCttAtb();
	}

	/**Original name: 2515-GET-SERVICE-CONTRACT<br>
	 * <pre>***************************************************************
	 *  CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
	 * ***************************************************************</pre>*/
	private void getServiceContract() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-INPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getInputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS GET
		//               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
		//               INTO       (L-CONTRACT-LOCATION (1: WS-SC-INPUT-LEN))
		//               FLENGTH    (WS-SC-INPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getInputLen());
		Channel.read(execContext, "", constantFields.getCopybookNames().getServiceCbkInpNmFormatted(), tsOutputData);
		lContractLocation.setlContractLocationFromBuffer(tsOutputData.getData());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2515-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '2515-GET-SERVICE-CONTRACT'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"2515-GET-SERVICE-CONTRACT", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 2515-EXIT
			return;
		}
	}

	/**Original name: 2520-CAPTURE-FROM-COMMAREA<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING THE COMMAREA ONLY.  SINCE THE          *
	 *  ENVIRONMENT WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY,   *
	 *  SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT.              *
	 * ***************************************************************
	 *   SINCE THE SERVICE CONTRACT IS APPENDED TO THE PROXY CONTRACT,
	 *    SET THE LOCATION OF THE SERVICE CONTRACT TO BE IMMEDIATELY
	 *    AFTER THE PROXY CONTRACT.</pre>*/
	private void captureFromCommarea() {
		// COB_CODE: SET WS-CA-PTR               TO
		//                                        ADDRESS OF PROXY-PROGRAM-COMMON.
		workingStorageArea.getWsCaPtr().setWsCaPtr(pointerManager.addressOf(dfhcommarea));
		// COB_CODE: ADD LENGTH OF PROXY-PROGRAM-COMMON
		//                                       TO WS-CA-PTR-VAL.
		workingStorageArea.getWsCaPtr().setWsCaPtrVal(DfhcommareaMu0x0004.Len.PROXY_PROGRAM_COMMON + workingStorageArea.getWsCaPtr().getWsCaPtrVal());
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO WS-CA-PTR.
		lServiceContractArea = ((pointerManager.resolve(workingStorageArea.getWsCaPtr().getWsCaPtr(),
				LServiceContractAreaXz0x90s0.class)));
	}

	/**Original name: 2530-CAPTURE-FROM-REF-STORAGE<br>
	 * <pre>***************************************************************
	 *  DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
	 *  WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
	 *  ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
	 * ***************************************************************</pre>*/
	private void captureFromRefStorage() {
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT-AREA
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContractArea = ((pointerManager.resolve(dfhcommarea.getPpcServiceDataPointer(),
				LServiceContractAreaXz0x90s0.class)));
	}

	/**Original name: 3000-CALL-FRAMEWORK<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
	 *  DRIVER.                                                      *
	 * ***************************************************************</pre>*/
	private void callFramework() {
		ConcatUtil concatUtil = null;
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM    (CF-MAIN-DRIVER)
		//               COMMAREA   (MAIN-DRIVER-DATA)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0X90S0", execContext).commarea(mainDriverData).length(DfhcommareaTs020000.Len.DFHCOMMAREA)
				.link(constantFields.getMainDriver(), new Ts020000());
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3000-CALL-FRAMEWORK'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3000-CALL-FRAMEWORK", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF NOT DSD-NO-ERROR-CODE
		//                                       TO PPC-ERROR-HANDLING-PARMS
		//           END-IF.
		if (!mainDriverData.getDriverSpecificData().getErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: MOVE DSD-ERROR-HANDLING-PARMS
			//                                   TO PPC-ERROR-HANDLING-PARMS
			dfhcommarea.setPpcErrorHandlingParmsBytes(mainDriverData.getDriverSpecificData().getDsdErrorHandlingParmsBytes());
		}
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  FRAMEWORK PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE PPC-OPERATION          TO CSC-OPERATION.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setOperation(dfhcommarea.getPpcOperation());
		// COB_CODE: MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setUnitOfWork(constantFields.getUnitOfWork());
		// COB_CODE: MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setRequestModule(constantFields.getRequestModule());
		// COB_CODE: MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
		mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setResponseModule(constantFields.getResponseModule());
		// COB_CODE: MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
		//                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.
		mainDriverData.getDriverSpecificData().getBypassSyncpointMdrvInd().setBypassSyncpointMdrvInd(dfhcommarea.getPpcBypassSyncpointMdrvInd());
		// COB_CODE: PERFORM 13100-SET-UP-INPUT
		//              THRU 13100-EXIT.
		setUpInput1();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private void setUpOutput() {
		// COB_CODE: PERFORM 13200-SET-UP-OUTPUT
		//              THRU 13200-EXIT.
		rng13200SetUpOutput();
		// COB_CODE: IF EIBCALEN = +0
		//                  THRU 3210-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: PERFORM 3210-OUTPUT-SVC-CONTRACT-CTA
			//              THRU 3210-EXIT
			outputSvcContractCta();
		}
	}

	/**Original name: 3210-OUTPUT-SVC-CONTRACT-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE SERVICE CONTRACT WILL NOT BE UPDATED FURTHER FROM  *
	 *  THIS POINT, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************</pre>*/
	private void outputSvcContractCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-SC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsServiceContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-SC-OUTPUT-LEN))
		//               FLENGTH    (WS-SC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsServiceContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getServiceCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3210-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '3210-OUTPUT-SVC-CONTRACT-CTA'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"3210-OUTPUT-SVC-CONTRACT-CTA", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 3210-EXIT
			return;
		}
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-REQUEST-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkRequestArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-1'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-1", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA(L-FRAMEWORK-RESPONSE-AREA)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lFrameworkResponseArea));
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-2'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-2", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IF USING CHANNELS AND CONTAINERS, DEALLOCATE THE SERVICE
		//     CONTRACT AND THE PROXY CONTRACT.  OTHERWISE, THAT WILL BE
		//     THE RESPONSIBILITY OF THE CALLER AND/OR THE CICS ENVIRONMENT
		// COB_CODE: IF EIBCALEN = +0
		//               CONTINUE
		//           ELSE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF L-SERVICE-CONTRACT-AREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(lServiceContractArea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(L-SERVICE-CONTRACT-AREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContractArea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-3'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-3", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
			//              THRU 8100-EXIT
			outputProxyCommonCta();
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		// COB_CODE: PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
		//              THRU 8100-EXIT.
		outputProxyCommonCta();
		// COB_CODE: IF PPC-FATAL-ERROR-CODE
		//             OR
		//              PPC-NLBE-CODE
		//               GO TO 8000-EXIT
		//           END-IF.
		if (dfhcommarea.isPpcFatalErrorCode() || dfhcommarea.isPpcNlbeCode()) {
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
		//    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
		//     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
		// COB_CODE: IF ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               END-EXEC
		//           END-IF.
		if (!pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: EXEC CICS FREEMAIN
			//               DATA(DFHCOMMAREA)
			//               RESP(WS-RESPONSE-CODE)
			//               RESP2(WS-RESPONSE-CODE2)
			//           END-EXEC
			cicsStorageManager.freemain(execContext, pointerManager.addressOf(dfhcommarea));
			workingStorageArea.setWsResponseCode(execContext.getResp());
			workingStorageArea.setWsResponseCode2(execContext.getResp2());
		}
		//!   RECORD AN ERROR IF ONE OCCURS AND THE MEMORY IS STILL
		//!    ALLOCATED.  IF IT IS NOT ALLOCATED, THEN WE WOULD RUN INTO
		//!    ADDRESSING ISSUES AND MOST LIKELY ABEND.  EITHER WAY THOUGH,
		//!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//             AND
		//              ADDRESS OF DFHCOMMAREA NOT = NULLS
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL
				&& !pointerManager.isNullPointer(pointerManager.addressOf(dfhcommarea))) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8000-FREE-FW-MEMORY-4'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8000-FREE-FW-MEMORY-4", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	/**Original name: 8100-OUTPUT-PROXY-COMMON-CTA<br>
	 * <pre>***************************************************************
	 *  SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
	 *  FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
	 *  CONTAINER TO SEND BACK TO THE CALLER.                        *
	 * ***************************************************************
	 *     IT IS POSSIBLE TO GET INTO THIS PARAGRAPH WHEN USING A METHOD
	 *      OTHER THAN CHANNELS.  BECAUSE OF THIS, WE WILL MAKE SURE WE
	 *      ARE UTILIZING A CHANNEL BEFORE PLACING THE COPYBOOK INTO A
	 *      CONTAINER.</pre>*/
	private void outputProxyCommonCta() {
		TpOutputData tsOutputData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT EIBCALEN = +0
		//               GO TO 8100-EXIT
		//           END-IF.
		if (!(execContext.getCommAreaLen() == 0)) {
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
		// COB_CODE: SET ADDRESS OF L-CONTRACT-LOCATION
		//                                       TO WS-PC-OUTPUT-PTR.
		lContractLocation = ((pointerManager.resolve(workingStorageArea.getWsProxyContractAtb().getOutputPtr(),
				LContractLocation.class)));
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
		//               FROM       (L-CONTRACT-LOCATION (1: WS-PC-OUTPUT-LEN))
		//               FLENGTH    (WS-PC-OUTPUT-LEN)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(workingStorageArea.getWsProxyContractAtb().getOutputLen());
		tsOutputData.setData(lContractLocation.getlContractLocationAsBuffer());
		Channel.write(execContext, "", constantFields.getCopybookNames().getProxyCbkOupNmFormatted(), tsOutputData);
		workingStorageArea.setWsResponseCode(execContext.getResp());
		workingStorageArea.setWsResponseCode2(execContext.getResp2());
		//!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
		//!   BACK TO THE CONSUMER.
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(workingStorageArea.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET PPC-FATAL-ERROR-CODE
			//                                   TO TRUE
			dfhcommarea.setPpcFatalErrorCode();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
			workingStorageArea.setWsEibrespDisplay(workingStorageArea.getWsResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
			workingStorageArea.setWsEibresp2Display(workingStorageArea.getWsResponseCode2());
			// COB_CODE: STRING 'Failed module is '
			//                  WS-PROGRAM-NAME
			//                  '.  Failed paragraph is '
			//                  '8100-OUTPUT-PROXY-COMMON'
			//                  '.  Failed module SqlCode is 0'
			//                  '.  Failed module EIBRESP Code is '
			//                  WS-EIBRESP-DISPLAY
			//                  '.  Failed module EIBRESP2 Code is '
			//                  WS-EIBRESP2-DISPLAY
			//                  '.'  DELIMITED BY SIZE
			//                  INTO PPC-FATAL-ERROR-MESSAGE
			//           END-STRING
			concatUtil = ConcatUtil.buildString(DfhcommareaMu0x0004.Len.PPC_FATAL_ERROR_MESSAGE,
					new String[] { "Failed module is ", workingStorageArea.getWsProgramNameFormatted(), ".  Failed paragraph is ",
							"8100-OUTPUT-PROXY-COMMON", ".  Failed module SqlCode is 0", ".  Failed module EIBRESP Code is ",
							workingStorageArea.getWsEibrespDisplayAsString(), ".  Failed module EIBRESP2 Code is ",
							workingStorageArea.getWsEibresp2DisplayAsString(), "." });
			dfhcommarea.setPpcFatalErrorMessage(concatUtil.replaceInString(dfhcommarea.getPpcFatalErrorMessageFormatted()));
			// COB_CODE: GO TO 8100-EXIT
			return;
		}
	}

	/**Original name: 12514-SET-SVC-CTT-ATB<br>
	 * <pre>***************************************************************
	 *  SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
	 * ***************************************************************</pre>*/
	private void setSvcCttAtb() {
		// COB_CODE: SET WS-SC-INPUT-PTR         TO ADDRESS OF
		//                                             XZT9S0-SERVICE-INPUTS.
		workingStorageArea.getWsServiceContractAtb().setInputPtr(pointerManager.addressOf(lServiceContractArea));
		// COB_CODE: SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
		//                                             XZT9S0-SERVICE-OUTPUTS.
		workingStorageArea.getWsServiceContractAtb()
				.setOutputPtr(pointerManager.addressOf(lServiceContractArea, LServiceContractAreaXz0x90s0.Pos.XZT9S0_SERVICE_OUTPUTS));
		// COB_CODE: MOVE LENGTH OF XZT9S0-SERVICE-INPUTS
		//                                       TO WS-SC-INPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setInputLen(LServiceContractAreaXz0x90s0.Len.XZT9S0_SERVICE_INPUTS);
		// COB_CODE: MOVE LENGTH OF XZT9S0-SERVICE-OUTPUTS
		//                                       TO WS-SC-OUTPUT-LEN.
		workingStorageArea.getWsServiceContractAtb().setOutputLen(LServiceContractAreaXz0x90s0.Len.XZT9S0_SERVICE_OUTPUTS);
	}

	/**Original name: 13100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
	 *  INPUT AREA                                                   *
	 * ***************************************************************
	 *  CALCULATE MAX NUMBER OF OUTPUTS.</pre>*/
	private void setUpInput1() {
		// COB_CODE: COMPUTE WS-MAX-REC-ROWS = LENGTH OF XZT9S0-SERVICE-OUTPUTS
		//                                   / LENGTH OF XZT9SO-POLICY-LIST.
		workingStorageArea.setWsMaxRecRows((new AfDecimal(
				((((double) LServiceContractAreaXz0x90s0.Len.XZT9S0_SERVICE_OUTPUTS)) / LServiceContractAreaXz0x90s0.Len.O_POLICY_LIST), 9, 0))
						.toShort());
		// COB_CODE: MOVE WS-MAX-REC-ROWS        TO XZA9S0Q-MAX-ROWS-RETURNED.
		lFrameworkRequestArea.setXza9s0qMaxRowsReturned(workingStorageArea.getWsMaxRecRows());
		// MOVE INPUTS.
		// COB_CODE: IF XZT9SI-USERID NOT = SPACES
		//             AND
		//              XZT9SI-USERID NOT = LOW-VALUES
		//                                          XZA9S0Q-USERID
		//           END-IF.
		if (!Characters.EQ_SPACE.test(lServiceContractArea.getiUserid()) && !Characters.EQ_LOW.test(lServiceContractArea.getiUseridFormatted())) {
			// COB_CODE: MOVE XZT9SI-USERID      TO CSC-AUTH-USERID
			//                                      XZA9S0Q-USERID
			mainDriverData.getCommunicationShellCommon().getCscGeneralParms().setAuthUserid(lServiceContractArea.getiUserid());
			lFrameworkRequestArea.setXza9s0qUserid(lServiceContractArea.getiUserid());
		}
		// COB_CODE: MOVE XZT9SI-ACT-NBR         TO XZA9S0Q-ACT-NBR.
		lFrameworkRequestArea.setXza9s0qActNbr(lServiceContractArea.getiActNbr());
		// COB_CODE: MOVE PPC-OPERATION          TO XZA9S0Q-PRIMARY-FUNCTION.
		lFrameworkRequestArea.setXza9s0qPrimaryFunction(dfhcommarea.getPpcOperation());
	}

	/**Original name: 13200-A<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
	 *  RESPONSE AREA.                                               *
	 * ***************************************************************</pre>*/
	private String a() {
		// COB_CODE: IF XZA9S1R-POLICY-ID(SS-POL) = SPACES
		//               GO TO 13200-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(lFrameworkResponseArea.getXza9s1rPolicyId(ssPol))) {
			// COB_CODE: GO TO 13200-EXIT
			return "";
		}
		// COB_CODE: MOVE XZA9S1R-POLICY-ID(SS-POL)
		//                                       TO XZT9SO-TK-POL-ID(SS-POL).
		lServiceContractArea.setoTkPolId(ssPol, lFrameworkResponseArea.getXza9s1rPolicyId(ssPol));
		// COB_CODE: MOVE XZA9S1R-RLT-TYP-CD(SS-POL)
		//                                       TO XZT9SO-TK-RLT-TYP-CD(SS-POL).
		lServiceContractArea.setoTkRltTypCd(ssPol, lFrameworkResponseArea.getXza9s1rRltTypCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-POL-NBR(SS-POL)
		//                                       TO XZT9SO-POL-NBR(SS-POL).
		lServiceContractArea.setoPolNbr(ssPol, lFrameworkResponseArea.getXza9s1rPolNbr(ssPol));
		// COB_CODE: MOVE XZA9S1R-OGN-EFF-DT(SS-POL)
		//                                       TO XZT9SO-POL-EFF-DT(SS-POL).
		lServiceContractArea.setoPolEffDt(ssPol, lFrameworkResponseArea.getXza9s1rOgnEffDt(ssPol));
		// COB_CODE: MOVE XZA9S1R-PLN-EXP-DT(SS-POL)
		//                                       TO XZT9SO-POL-EXP-DT(SS-POL).
		lServiceContractArea.setoPolExpDt(ssPol, lFrameworkResponseArea.getXza9s1rPlnExpDt(ssPol));
		// COB_CODE: MOVE XZA9S1R-QUOTE-SEQUENCE-NBR(SS-POL)
		//                                       TO XZT9SO-QTE-NBR(SS-POL).
		lServiceContractArea.setoQteNbr(ssPol, lFrameworkResponseArea.getXza9s1rQuoteSequenceNbr(ssPol));
		// COB_CODE: MOVE XZA9S1R-ACT-NBR(SS-POL)
		//                                       TO XZT9SO-ACT-NBR(SS-POL).
		lServiceContractArea.setoActNbr(ssPol, lFrameworkResponseArea.getXza9s1rActNbr(ssPol));
		// COB_CODE: MOVE XZA9S1R-ACT-NBR-FMT(SS-POL)
		//                                       TO XZT9SO-ACT-NBR-FMT(SS-POL).
		lServiceContractArea.setoActNbrFmt(ssPol, lFrameworkResponseArea.getXza9s1rActNbrFmt(ssPol));
		// COB_CODE: MOVE XZA9S1R-PRI-RSK-ST-ABB(SS-POL)
		//                                       TO XZT9SO-PRI-RSK-ST-ABB(SS-POL).
		lServiceContractArea.setoPriRskStAbb(ssPol, lFrameworkResponseArea.getXza9s1rPriRskStAbb(ssPol));
		// COB_CODE: MOVE XZA9S1R-PRI-RSK-ST-DES(SS-POL)
		//                                       TO XZT9SO-PRI-RSK-ST-DES(SS-POL).
		lServiceContractArea.setoPriRskStDes(ssPol, lFrameworkResponseArea.getXza9s1rPriRskStDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-LOB-CD(SS-POL) TO XZT9SO-LOB-CD(SS-POL).
		lServiceContractArea.setoLobCd(ssPol, lFrameworkResponseArea.getXza9s1rLobCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-LOB-DES(SS-POL)
		//                                       TO XZT9SO-LOB-DES(SS-POL).
		lServiceContractArea.setoLobDes(ssPol, lFrameworkResponseArea.getXza9s1rLobDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-TOB-CD(SS-POL) TO XZT9SO-TOB-CD(SS-POL).
		lServiceContractArea.setoTobCd(ssPol, lFrameworkResponseArea.getXza9s1rTobCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-TOB-DES(SS-POL)
		//                                       TO XZT9SO-TOB-DES(SS-POL).
		lServiceContractArea.setoTobDes(ssPol, lFrameworkResponseArea.getXza9s1rTobDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-SEG-CD(SS-POL) TO XZT9SO-SEG-CD(SS-POL).
		lServiceContractArea.setoSegCd(ssPol, lFrameworkResponseArea.getXza9s1rSegCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-SEG-DES(SS-POL)
		//                                       TO XZT9SO-SEG-DES(SS-POL).
		lServiceContractArea.setoSegDes(ssPol, lFrameworkResponseArea.getXza9s1rSegDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-LGE-ACT-IND(SS-POL)
		//                                       TO XZT9SO-LGE-ACT-IND(SS-POL).
		lServiceContractArea.setoLgeActInd(ssPol, lFrameworkResponseArea.getXza9s1rLgeActInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-MNL-POL-IND(SS-POL)
		//                                       TO XZT9SO-MNL-POL-IND(SS-POL).
		lServiceContractArea.setoMnlPolInd(ssPol, lFrameworkResponseArea.getXza9s1rMnlPolInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-STA-CD(SS-POL)
		//                                       TO XZT9SO-CUR-STA-CD(SS-POL).
		lServiceContractArea.setoCurStaCd(ssPol, lFrameworkResponseArea.getXza9s1rCurStaCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-STA-DES(SS-POL)
		//                                       TO XZT9SO-CUR-STA-DES(SS-POL).
		lServiceContractArea.setoCurStaDes(ssPol, lFrameworkResponseArea.getXza9s1rCurStaDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-TRS-TYP-CD(SS-POL)
		//                                       TO XZT9SO-CUR-TRS-TYP-CD(SS-POL).
		lServiceContractArea.setoCurTrsTypCd(ssPol, lFrameworkResponseArea.getXza9s1rCurTrsTypCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-TRS-TYP-DES(SS-POL)
		//                                       TO
		//                                       XZT9SO-CUR-TRS-TYP-DES(SS-POL).
		lServiceContractArea.setoCurTrsTypDes(ssPol, lFrameworkResponseArea.getXza9s1rCurTrsTypDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-PND-TRS-IND(SS-POL)
		//                                       TO
		//                                       XZT9SO-CUR-PND-TRS-IND(SS-POL).
		lServiceContractArea.setoCurPndTrsInd(ssPol, lFrameworkResponseArea.getXza9s1rCurPndTrsInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-OGN-CD(SS-POL)
		//                                       TO XZT9SO-CUR-OGN-CD(SS-POL).
		lServiceContractArea.setoCurOgnCd(ssPol, lFrameworkResponseArea.getXza9s1rCurOgnCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-CUR-OGN-DES(SS-POL)
		//                                       TO XZT9SO-CUR-OGN-DES(SS-POL).
		lServiceContractArea.setoCurOgnDes(ssPol, lFrameworkResponseArea.getXza9s1rCurOgnDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-COVERAGE-PARTS(SS-POL)
		//                                       TO XZT9SO-COVERAGE-PARTS(SS-POL).
		lServiceContractArea.setoCoverageParts(ssPol, lFrameworkResponseArea.getXza9s1rCoverageParts(ssPol));
		// COB_CODE: MOVE XZA9S1R-BOND-IND(SS-POL)
		//                                       TO XZT9SO-BOND-IND(SS-POL).
		lServiceContractArea.setoBondInd(ssPol, lFrameworkResponseArea.getXza9s1rBondInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-BRANCH-NBR(SS-POL)
		//                                       TO XZT9SO-PB-BRANCH-NBR(SS-POL).
		lServiceContractArea.setoPbBranchNbr(ssPol, lFrameworkResponseArea.getXza9s1rBranchNbr(ssPol));
		// COB_CODE: MOVE XZA9S1R-BRANCH-DESC(SS-POL)
		//                                       TO XZT9SO-PB-BRANCH-DESC(SS-POL).
		lServiceContractArea.setoPbBranchDesc(ssPol, lFrameworkResponseArea.getXza9s1rBranchDesc(ssPol));
		// COB_CODE: MOVE XZA9S1R-WRT-PREM-AMT(SS-POL)
		//                                       TO XZT9SO-WRT-PREM-AMT(SS-POL).
		lServiceContractArea.setoWrtPremAmt(ssPol, lFrameworkResponseArea.getXza9s1rWrtPremAmt(ssPol));
		// COB_CODE: MOVE XZA9S1R-POL-ACT-IND(SS-POL)
		//                                       TO XZT9SO-POL-ACT-IND(SS-POL).
		lServiceContractArea.setoPolActInd(ssPol, lFrameworkResponseArea.getXza9s1rPolActInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-PND-CNC-IND(SS-POL)
		//                                       TO XZT9SO-PND-CNC-IND(SS-POL).
		lServiceContractArea.setoPndCncInd(ssPol, lFrameworkResponseArea.getXza9s1rPndCncInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-SCH-CNC-DT(SS-POL)
		//                                       TO XZT9SO-SCH-CNC-DT(SS-POL).
		lServiceContractArea.setoSchCncDt(ssPol, lFrameworkResponseArea.getXza9s1rSchCncDt(ssPol));
		// COB_CODE: MOVE XZA9S1R-NOT-TYP-CD(SS-POL)
		//                                       TO XZT9SO-NOT-TYP-CD(SS-POL).
		lServiceContractArea.setoNotTypCd(ssPol, lFrameworkResponseArea.getXza9s1rNotTypCd(ssPol));
		// COB_CODE: MOVE XZA9S1R-NOT-TYP-DES(SS-POL)
		//                                       TO XZT9SO-NOT-TYP-DES(SS-POL).
		lServiceContractArea.setoNotTypDes(ssPol, lFrameworkResponseArea.getXza9s1rNotTypDes(ssPol));
		// COB_CODE: MOVE XZA9S1R-TMN-IND(SS-POL)
		//                                       TO XZT9SO-TMN-IND(SS-POL).
		lServiceContractArea.setoTmnInd(ssPol, lFrameworkResponseArea.getXza9s1rTmnInd(ssPol));
		// COB_CODE: MOVE XZA9S1R-NOT-EMP-ID(SS-POL)
		//                                       TO XZT9SO-NOT-EMP-ID(SS-POL).
		lServiceContractArea.setoNotEmpId(ssPol, lFrameworkResponseArea.getXza9s1rNotEmpId(ssPol));
		// COB_CODE: MOVE XZA9S1R-NOT-EMP-NM(SS-POL)
		//                                       TO XZT9SO-NOT-EMP-NM(SS-POL).
		lServiceContractArea.setoNotEmpNm(ssPol, lFrameworkResponseArea.getXza9s1rNotEmpNm(ssPol));
		// COB_CODE: MOVE XZA9S1R-NOT-PRC-DT(SS-POL)
		//                                       TO XZT9SO-NOT-PRC-DT(SS-POL).
		lServiceContractArea.setoNotPrcDt(ssPol, lFrameworkResponseArea.getXza9s1rNotPrcDt(ssPol));
		// COB_CODE: MOVE XZA9S1R-TMN-EMP-ID(SS-POL)
		//                                       TO XZT9SO-TMN-EMP-ID(SS-POL).
		lServiceContractArea.setoTmnEmpId(ssPol, lFrameworkResponseArea.getXza9s1rTmnEmpId(ssPol));
		// COB_CODE: MOVE XZA9S1R-TMN-EMP-NM(SS-POL)
		//                                       TO XZT9SO-TMN-EMP-NM(SS-POL).
		lServiceContractArea.setoTmnEmpNm(ssPol, lFrameworkResponseArea.getXza9s1rTmnEmpNm(ssPol));
		// COB_CODE: MOVE XZA9S1R-TMN-PRC-DT(SS-POL)
		//                                       TO XZT9SO-TMN-PRC-DT(SS-POL).
		lServiceContractArea.setoTmnPrcDt(ssPol, lFrameworkResponseArea.getXza9s1rTmnPrcDt(ssPol));
		// COB_CODE: IF SS-POL = WS-MAX-REC-ROWS
		//               GO TO 13200-EXIT
		//           END-IF.
		if (ssPol == workingStorageArea.getWsMaxRecRows()) {
			// COB_CODE: GO TO 13200-EXIT
			return "";
		}
		// COB_CODE: ADD +1                      TO SS-POL.
		ssPol = Trunc.toShort(1 + ssPol, 4);
		// COB_CODE: GO TO 13200-A.
		return "13200-A";
	}

	/**Original name: RNG_13200-SET-UP-OUTPUT-_-13200-EXIT<br>*/
	private void rng13200SetUpOutput() {
		String retcode = "";
		boolean goto13200A = false;
		do {
			goto13200A = false;
			retcode = a();
		} while (retcode.equals("13200-A"));
	}

	public void initDsdErrorHandlingParms() {
		mainDriverData.getDriverSpecificData().getErrorReturnCode().setErrorReturnCodeFormatted("0000");
		mainDriverData.getDriverSpecificData().setFatalErrorMessage("");
		mainDriverData.getDriverSpecificData().setNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getNonLoggableErrors(idx0).setDsdNonLogErrMsg("");
		}
		mainDriverData.getDriverSpecificData().setWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DriverSpecificData.WARNINGS_MAXOCCURS; idx0++) {
			mainDriverData.getDriverSpecificData().getWarnings(idx0).setDsdWarnMsg("");
		}
	}

	public void initPpcErrorHandlingParms() {
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initMemoryAllocationParms() {
		mainDriverData.getCommunicationShellCommon().setMaInputDataSize(0);
		mainDriverData.getCommunicationShellCommon().setMaOutputDataSize(0);
		mainDriverData.getCommunicationShellCommon().getMaReturnCode().setMaReturnCodeFormatted("0000");
		mainDriverData.getCommunicationShellCommon().setMaErrorMessage("");
	}

	public void initLFrameworkRequestArea() {
		lFrameworkRequestArea.setXza9s0qMaxRowsReturned(((short) 0));
		lFrameworkRequestArea.setXza9s0qPrimaryFunction("");
		lFrameworkRequestArea.setXza9s0qActNbr("");
		lFrameworkRequestArea.setXza9s0qUserid("");
	}

	public void initLFrameworkResponseArea() {
		for (int idx0 = 1; idx0 <= LFrameworkResponseAreaXz0x90s0.L_FW_RESP_ACY_PND_CNC_TMN_POL_MAXOCCURS; idx0++) {
			lFrameworkResponseArea.setXza9s1rPolicyId(idx0, "");
			lFrameworkResponseArea.setXza9s1rPolNbr(idx0, "");
			lFrameworkResponseArea.setXza9s1rOgnEffDt(idx0, "");
			lFrameworkResponseArea.setXza9s1rPlnExpDt(idx0, "");
			lFrameworkResponseArea.setXza9s1rQuoteSequenceNbr(idx0, 0);
			lFrameworkResponseArea.setXza9s1rActNbr(idx0, "");
			lFrameworkResponseArea.setXza9s1rActNbrFmt(idx0, "");
			lFrameworkResponseArea.setXza9s1rPriRskStAbb(idx0, "");
			lFrameworkResponseArea.setXza9s1rPriRskStDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rLobCd(idx0, "");
			lFrameworkResponseArea.setXza9s1rLobDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rTobCd(idx0, "");
			lFrameworkResponseArea.setXza9s1rTobDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rSegCd(idx0, "");
			lFrameworkResponseArea.setXza9s1rSegDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rLgeActInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rStatusCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurPndTrsInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurStaCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurStaDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rCurTrsTypCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurTrsTypDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rCoverageParts(idx0, "");
			lFrameworkResponseArea.setXza9s1rRltTypCd(idx0, "");
			lFrameworkResponseArea.setXza9s1rMnlPolInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurOgnCd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rCurOgnDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rBondInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rBranchNbr(idx0, "");
			lFrameworkResponseArea.setXza9s1rBranchDesc(idx0, "");
			lFrameworkResponseArea.setXza9s1rWrtPremAmt(idx0, 0);
			lFrameworkResponseArea.setXza9s1rPolActInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rPndCncInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rSchCncDt(idx0, "");
			lFrameworkResponseArea.setXza9s1rNotTypCd(idx0, "");
			lFrameworkResponseArea.setXza9s1rNotTypDes(idx0, "");
			lFrameworkResponseArea.setXza9s1rNotEmpId(idx0, "");
			lFrameworkResponseArea.setXza9s1rNotEmpNm(idx0, "");
			lFrameworkResponseArea.setXza9s1rNotPrcDt(idx0, "");
			lFrameworkResponseArea.setXza9s1rTmnInd(idx0, Types.SPACE_CHAR);
			lFrameworkResponseArea.setXza9s1rTmnEmpId(idx0, "");
			lFrameworkResponseArea.setXza9s1rTmnEmpNm(idx0, "");
			lFrameworkResponseArea.setXza9s1rTmnPrcDt(idx0, "");
		}
	}

	public void initDfhcommarea() {
		dfhcommarea.setPpcServiceDataSize(0);
		dfhcommarea.setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		dfhcommarea.setPpcOperation("");
		dfhcommarea.setPpcErrorReturnCodeFormatted("0000");
		dfhcommarea.setPpcFatalErrorMessage("");
		dfhcommarea.setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcNonLogErrMsg(idx0, "");
		}
		dfhcommarea.setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= DfhcommareaMu0x0004.PPC_WARNINGS_MAXOCCURS; idx0++) {
			dfhcommarea.setPpcWarnMsg(idx0, "");
		}
	}

	public void initLServiceContractArea() {
		lServiceContractArea.setiActNbr("");
		lServiceContractArea.setiUserid("");
		for (int idx0 = 1; idx0 <= LServiceContractAreaXz0x90s0.O_POLICY_LIST_MAXOCCURS; idx0++) {
			lServiceContractArea.setoTkPolId(idx0, "");
			lServiceContractArea.setoTkRltTypCd(idx0, "");
			lServiceContractArea.setoPolNbr(idx0, "");
			lServiceContractArea.setoPolEffDt(idx0, "");
			lServiceContractArea.setoPolExpDt(idx0, "");
			lServiceContractArea.setoQteNbr(idx0, 0);
			lServiceContractArea.setoActNbr(idx0, "");
			lServiceContractArea.setoActNbrFmt(idx0, "");
			lServiceContractArea.setoPriRskStAbb(idx0, "");
			lServiceContractArea.setoPriRskStDes(idx0, "");
			lServiceContractArea.setoLobCd(idx0, "");
			lServiceContractArea.setoLobDes(idx0, "");
			lServiceContractArea.setoTobCd(idx0, "");
			lServiceContractArea.setoTobDes(idx0, "");
			lServiceContractArea.setoSegCd(idx0, "");
			lServiceContractArea.setoSegDes(idx0, "");
			lServiceContractArea.setoLgeActInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoMnlPolInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoCurStaCd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoCurStaDes(idx0, "");
			lServiceContractArea.setoCurTrsTypCd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoCurTrsTypDes(idx0, "");
			lServiceContractArea.setoCurPndTrsInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoCurOgnCd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoCurOgnDes(idx0, "");
			lServiceContractArea.setoCoverageParts(idx0, "");
			lServiceContractArea.setoBondInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoPbBranchNbr(idx0, "");
			lServiceContractArea.setoPbBranchDesc(idx0, "");
			lServiceContractArea.setoWrtPremAmt(idx0, 0);
			lServiceContractArea.setoPolActInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoPndCncInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoSchCncDt(idx0, "");
			lServiceContractArea.setoNotTypCd(idx0, "");
			lServiceContractArea.setoNotTypDes(idx0, "");
			lServiceContractArea.setoNotEmpId(idx0, "");
			lServiceContractArea.setoNotEmpNm(idx0, "");
			lServiceContractArea.setoNotPrcDt(idx0, "");
			lServiceContractArea.setoTmnInd(idx0, Types.SPACE_CHAR);
			lServiceContractArea.setoTmnEmpId(idx0, "");
			lServiceContractArea.setoTmnEmpNm(idx0, "");
			lServiceContractArea.setoTmnPrcDt(idx0, "");
		}
	}

	public void setSsPol(short ssPol) {
		this.ssPol = ssPol;
	}

	public short getSsPol() {
		return this.ssPol;
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
