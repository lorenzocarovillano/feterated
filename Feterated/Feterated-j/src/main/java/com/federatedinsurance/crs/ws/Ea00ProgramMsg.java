/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-00-PROGRAM-MSG<br>
 * Variable: EA-00-PROGRAM-MSG from program TS030199<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea00ProgramMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-00-PROGRAM-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-00-PROGRAM-MSG-1
	private String flr2 = "PROGRAM";
	//Original name: FILLER-EA-00-PROGRAM-MSG-2
	private String flr3 = "TS030199";
	//Original name: FILLER-EA-00-PROGRAM-MSG-3
	private String flr4 = "COMPILED ON";
	//Original name: EA-00-DATE
	private String dateFld = DefaultValues.stringVal(Len.DATE_FLD);
	//Original name: FILLER-EA-00-PROGRAM-MSG-4
	private String flr5 = " AT";
	//Original name: EA-00-TIME
	private String timeFld = DefaultValues.stringVal(Len.TIME_FLD);

	//==== METHODS ====
	public String getEa00ProgramMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa00ProgramMsgBytes());
	}

	public byte[] getEa00ProgramMsgBytes() {
		byte[] buffer = new byte[Len.EA00_PROGRAM_MSG];
		return getEa00ProgramMsgBytes(buffer, 1);
	}

	public byte[] getEa00ProgramMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, dateFld, Len.DATE_FLD);
		position += Len.DATE_FLD;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, timeFld, Len.TIME_FLD);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setDateFld(String dateFld) {
		this.dateFld = Functions.subString(dateFld, Len.DATE_FLD);
	}

	public String getDateFld() {
		return this.dateFld;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setTimeFldFormatted(String timeFld) {
		String field = Functions.leftPad(timeFld, Len.TIME_FLD, Types.SPACE_CHAR);
		this.timeFld = Functions.subString(field, strLen(field) - Len.TIME_FLD + 1, Len.TIME_FLD);
	}

	public String getTimeFld() {
		return this.timeFld;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DATE_FLD = 8;
		public static final int TIME_FLD = 8;
		public static final int FLR1 = 1;
		public static final int FLR2 = 8;
		public static final int FLR3 = 9;
		public static final int FLR4 = 12;
		public static final int FLR5 = 4;
		public static final int EA00_PROGRAM_MSG = DATE_FLD + TIME_FLD + FLR1 + FLR2 + FLR3 + FLR4 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
