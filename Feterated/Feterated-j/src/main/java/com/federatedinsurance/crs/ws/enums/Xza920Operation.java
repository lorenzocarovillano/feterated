/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZA920-OPERATION<br>
 * Variable: XZA920-OPERATION from copybook XZ0A9020<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xza920Operation {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.OPERATION);
	public static final String FRM = "GetRecipientListByFormSeqNbr";
	public static final String NOT_FLD = "GetRecipientListByNotification";

	//==== METHODS ====
	public void setOperation(String operation) {
		this.value = Functions.subString(operation, Len.OPERATION);
	}

	public String getOperation() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int OPERATION = 32;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
