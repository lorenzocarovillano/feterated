/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: TF-REC-FOUND-FLAG<br>
 * Variable: TF-REC-FOUND-FLAG from copybook TS020TBL<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class TfRecFoundFlag {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setTfRecFoundFlag(char tfRecFoundFlag) {
		this.value = tfRecFoundFlag;
	}

	public char getTfRecFoundFlag() {
		return this.value;
	}

	public boolean isNotFound() {
		return value == NOT_FOUND;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TF_REC_FOUND_FLAG = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
