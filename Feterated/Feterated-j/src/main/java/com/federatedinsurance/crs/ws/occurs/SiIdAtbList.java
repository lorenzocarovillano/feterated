/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: SI-ID-ATB-LIST<br>
 * Variables: SI-ID-ATB-LIST from program XZ001000<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class SiIdAtbList {

	//==== PROPERTIES ====
	//Original name: SI-ID-ATB-NM
	private String nm = DefaultValues.stringVal(Len.NM);
	//Original name: SI-ID-ATB-VALUE
	private String value2 = DefaultValues.stringVal(Len.VALUE2);

	//==== METHODS ====
	public void setIdAtbListBytes(byte[] buffer, int offset) {
		int position = offset;
		nm = MarshalByte.readString(buffer, position, Len.NM);
		position += Len.NM;
		value2 = MarshalByte.readString(buffer, position, Len.VALUE2);
	}

	public byte[] getIdAtbListBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, nm, Len.NM);
		position += Len.NM;
		MarshalByte.writeString(buffer, position, value2, Len.VALUE2);
		return buffer;
	}

	public void initIdAtbListSpaces() {
		nm = "";
		value2 = "";
	}

	public void setNm(String nm) {
		this.nm = Functions.subString(nm, Len.NM);
	}

	public String getNm() {
		return this.nm;
	}

	public void setValue2(String value2) {
		this.value2 = Functions.subString(value2, Len.VALUE2);
	}

	public String getValue2() {
		return this.value2;
	}

	public String getValue2Formatted() {
		return Functions.padBlanks(getValue2(), Len.VALUE2);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int NM = 100;
		public static final int VALUE2 = 100;
		public static final int ID_ATB_LIST = NM + VALUE2;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
