/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-03-MISSING-INPUT<br>
 * Variable: EA-03-MISSING-INPUT from program XZ0G0001<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea03MissingInput {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-03-MISSING-INPUT
	private String flr1 = "PGM =";
	//Original name: EA-03-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-03-MISSING-INPUT-1
	private String flr2 = " PARA =";
	//Original name: EA-03-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-03-MISSING-INPUT-2
	private String flr3 = " INVALID PARMS";
	//Original name: FILLER-EA-03-MISSING-INPUT-3
	private String flr4 = "ERROR. NOT ALL";
	//Original name: FILLER-EA-03-MISSING-INPUT-4
	private String flr5 = "REQUIRED PARMS";
	//Original name: FILLER-EA-03-MISSING-INPUT-5
	private String flr6 = "WERE PASSED IN.";

	//==== METHODS ====
	public String getEa03MissingInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa03MissingInputBytes());
	}

	public byte[] getEa03MissingInputBytes() {
		byte[] buffer = new byte[Len.EA03_MISSING_INPUT];
		return getEa03MissingInputBytes(buffer, 1);
	}

	public byte[] getEa03MissingInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR3);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 15;
		public static final int EA03_MISSING_INPUT = PROGRAM_NAME + PARAGRAPH_NBR + FLR1 + FLR2 + 4 * FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
