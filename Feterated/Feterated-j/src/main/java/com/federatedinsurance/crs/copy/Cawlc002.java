/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CAWLC002<br>
 * Variable: CAWLC002 from copybook CAWLC002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cawlc002 {

	//==== PROPERTIES ====
	//Original name: CW02C-CLIENT-TAB-CHK-SUM
	private String clientTabChkSum = DefaultValues.stringVal(Len.CLIENT_TAB_CHK_SUM);
	//Original name: CW02C-CLIENT-ID-KCRE
	private String clientIdKcre = DefaultValues.stringVal(Len.CLIENT_ID_KCRE);
	//Original name: CW02C-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: CW02C-CLIENT-TAB-KEY
	private Cw02fClientTabKey clientTabKey = new Cw02fClientTabKey();
	//Original name: CW02C-CLIENT-TAB-DATA
	private Cw02cClientTabData clientTabData = new Cw02cClientTabData();

	//==== METHODS ====
	public void setClientTabRowFormatted(String data) {
		byte[] buffer = new byte[Len.CLIENT_TAB_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.CLIENT_TAB_ROW);
		setClientTabRowBytes(buffer, 1);
	}

	public String getClientTabRowFormatted() {
		return MarshalByteExt.bufferToStr(getClientTabRowBytes());
	}

	/**Original name: CW02C-CLIENT-TAB-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CAWLC002 - CLIENT_TAB TABLE                                    *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#        DATE        PROG#     DESCRIPTION                   *
	 *  ---------- ----------- --------- ------------------------------*
	 *             13 Mar 2001 EPDI265   GENERATED                     *
	 *  TO07602-35 11 Aug 2010 E404KXS   ADDED MATCH CODE FIELDS       *
	 *  PP02700    06 Dec 2010 E404DLP   ADDED ACQUIRED SOURCE CD FIELD*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getClientTabRowBytes() {
		byte[] buffer = new byte[Len.CLIENT_TAB_ROW];
		return getClientTabRowBytes(buffer, 1);
	}

	public void setClientTabRowBytes(byte[] buffer, int offset) {
		int position = offset;
		setClientTabFixedBytes(buffer, position);
		position += Len.CLIENT_TAB_FIXED;
		setClientTabDatesBytes(buffer, position);
		position += Len.CLIENT_TAB_DATES;
		clientTabKey.setClientTabKeyBytes(buffer, position);
		position += Cw02fClientTabKey.Len.CLIENT_TAB_KEY;
		clientTabData.setClientTabDataBytes(buffer, position);
	}

	public byte[] getClientTabRowBytes(byte[] buffer, int offset) {
		int position = offset;
		getClientTabFixedBytes(buffer, position);
		position += Len.CLIENT_TAB_FIXED;
		getClientTabDatesBytes(buffer, position);
		position += Len.CLIENT_TAB_DATES;
		clientTabKey.getClientTabKeyBytes(buffer, position);
		position += Cw02fClientTabKey.Len.CLIENT_TAB_KEY;
		clientTabData.getClientTabDataBytes(buffer, position);
		return buffer;
	}

	public void setClientTabFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		clientTabChkSum = MarshalByte.readFixedString(buffer, position, Len.CLIENT_TAB_CHK_SUM);
		position += Len.CLIENT_TAB_CHK_SUM;
		clientIdKcre = MarshalByte.readString(buffer, position, Len.CLIENT_ID_KCRE);
	}

	public byte[] getClientTabFixedBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, clientTabChkSum, Len.CLIENT_TAB_CHK_SUM);
		position += Len.CLIENT_TAB_CHK_SUM;
		MarshalByte.writeString(buffer, position, clientIdKcre, Len.CLIENT_ID_KCRE);
		return buffer;
	}

	public void setClientTabChkSumFormatted(String clientTabChkSum) {
		this.clientTabChkSum = Trunc.toUnsignedNumeric(clientTabChkSum, Len.CLIENT_TAB_CHK_SUM);
	}

	public int getClientTabChkSum() {
		return NumericDisplay.asInt(this.clientTabChkSum);
	}

	public void setClientIdKcre(String clientIdKcre) {
		this.clientIdKcre = Functions.subString(clientIdKcre, Len.CLIENT_ID_KCRE);
	}

	public String getClientIdKcre() {
		return this.clientIdKcre;
	}

	public void setClientTabDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getClientTabDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public Cw02cClientTabData getClientTabData() {
		return clientTabData;
	}

	public Cw02fClientTabKey getClientTabKey() {
		return clientTabKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_TAB_CHK_SUM = 9;
		public static final int CLIENT_ID_KCRE = 32;
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CLIENT_TAB_FIXED = CLIENT_TAB_CHK_SUM + CLIENT_ID_KCRE;
		public static final int CLIENT_TAB_DATES = TRANS_PROCESS_DT;
		public static final int CLIENT_TAB_ROW = CLIENT_TAB_FIXED + CLIENT_TAB_DATES + Cw02fClientTabKey.Len.CLIENT_TAB_KEY
				+ Cw02cClientTabData.Len.CLIENT_TAB_DATA;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
