/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-01-FATAL-ERROR-MSG<br>
 * Variable: EA-01-FATAL-ERROR-MSG from program TS030199<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01FatalErrorMsgTs030199 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-1
	private String flr2 = "TS030199 -";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-2
	private String flr3 = "FATAL ERROR;";
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-3
	private String flr4 = "SQL CODE:";
	//Original name: EA-01-SQL-CODE
	private String code = DefaultValues.stringVal(Len.CODE);
	//Original name: FILLER-EA-01-FATAL-ERROR-MSG-4
	private String flr5 = ", ERROR MSG:";
	//Original name: EA-01-SQL-MESSAGE
	private String message = DefaultValues.stringVal(Len.MESSAGE);

	//==== METHODS ====
	public String getEa01FatalErrorMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa01FatalErrorMsgBytes());
	}

	public byte[] getEa01FatalErrorMsgBytes() {
		byte[] buffer = new byte[Len.EA01_FATAL_ERROR_MSG];
		return getEa01FatalErrorMsgBytes(buffer, 1);
	}

	public byte[] getEa01FatalErrorMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, code, Len.CODE);
		position += Len.CODE;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, message, Len.MESSAGE);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setCode(long code) {
		this.code = PicFormatter.display("-9(5)").format(code).toString();
	}

	public long getCode() {
		return PicParser.display("-9(5)").parseLong(this.code);
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setMessage(String message) {
		this.message = Functions.subString(message, Len.MESSAGE);
	}

	public String getMessage() {
		return this.message;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CODE = 6;
		public static final int MESSAGE = 70;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 13;
		public static final int FLR4 = 10;
		public static final int EA01_FATAL_ERROR_MSG = CODE + MESSAGE + FLR1 + FLR2 + 2 * FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
