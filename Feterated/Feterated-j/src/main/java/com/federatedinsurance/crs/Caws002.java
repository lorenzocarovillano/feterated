/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalMsgTransprtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalNlbeWngTxtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalSecDpDfltVDao;
import com.federatedinsurance.crs.copy.Cawlc002;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.Caws002Data;
import com.federatedinsurance.crs.ws.DfhcommareaCaws002;
import com.federatedinsurance.crs.ws.UbocCommInfoCaws002;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsHoldArea;
import com.federatedinsurance.crs.ws.WsNonlogPlaceholderValues;
import com.federatedinsurance.crs.ws.enums.HsddDflOfsPer;
import com.federatedinsurance.crs.ws.enums.HsddDflTypInd;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: CAWS002<br>
 * <pre>AUTHOR.  CSC.
 * DATE-WRITTEN. 26 FEB 2002.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - SAVANNAH DYNAMIC ENTITLEMENT DATA PRIVACY/  **
 * *                 DEFAULT PROGRAM.                            **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * SKELETON VERSION - S21BBA3                                  **
 * *                                                             **
 * * COMPONENT VERSION - 1.1  FEBRUARY 12, 2002                  **
 * *                                                             **
 * * SKELETON TEMPLATE USED FOR GENERATION - GEDPSKEL            **
 * *                                                             **
 * * PURPOSE -       SAVANNAH DYNAMIC ENTITLEMENT SECURITY PGM.  **
 * *                 GENERATED DEFAULT/DATA PRIVACY MODULE       **
 * *                 ASSOCIATED WITH A SPECIFIC DATA TABLE AND   **
 * *                 BUS. DATA OBJECT.  THIS PROGRAM WILL SET    **
 * *                 DATA PRIVACY AND DEFAULT VALUES BASED ON    **
 * *                 SUPPORT TABLE HAL_SEC_DP_DFLT_V.  THIS PGM  **
 * *                 MODULE NAME MUST BE ADDED TO THE EXISTING   **
 * *                 TABLE HAL_BO_MDU_XRF_V (HBMX_DP_DFL_MDU_NM).**
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       WAYS:                                 **
 * *                                                             **
 * *                       CAWS002 IS A UTILITY LINKED TO FROM **
 * *                       THE 'CLIENT_TAB_V' BDO OR FROM THE
 * *                       SET_DEFAULT_HUB PROGRAM HALOUSDH.     **
 * *                                                             **
 * * DATA ACCESS METHODS - UMT STORAGE RECORDS                   **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *  NOTE: THIS MODULE SHOWS EXAMPLE CODE OF THE TYPES OF DATA  **
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * --------  ---------  --------   ----------------------------**
 * * NEW PGM   30 NOV 01   07077     BUILT FOR DYNAMIC ENTITLEMENT*
 * *                                 PHASE 3B.                   **
 * * NEW PGM   15 JAN 02   03539     THE EMBEDDED SUBSTITUTION   **
 * *                                 FILES ARE GENERATED FROM    **
 * *                                 REXX PROGRAMS,THEY HAVE     **
 * *                                 NO TEMPLATES(NT)            **
 * * 18384B    12 FEB 02   03539    -CHANGED PARAGRAPH 7050-     **
 * *                                 DUE TO INBOUND/OUTBOUND     **
 * *                                 ATTRIBUTES NOT BEING SET    **
 * *                                 CORRECTLY                   **
 * *                                -CHANGED PARAGRAPH 3000-     **
 * *                                 REVISED CODE WHEN COLUMN    **
 * *                                 NAME EQUAL TO SPACES        **
 * *                                -CHANGED WS-NUM-TBL-ENTRIES  **
 * *                                 FROM PIC(02) TO (03)        **
 * *                                -ADDED IGNORE VALUE FOR      **
 * *                                 COLUMN INDICATOR            **
 * * 18384C    27 FEB 02   07077    -FIX DUE TO 18384B ABOVE     **
 * * 18384D    04 MAR 02   03539    -ADDED DP INFO TO UBOC BUFFER**
 * * 20072     29 MAR 02   03539    -CODE TO CHECK VALUE OF      **
 * *                                 UBOC-SECURITY-CONTEXT       **
 * *                                 FROM PIC(02) TO (03)        **
 * * 24863A    24 JUL 02   03539    -REMOVED FROM PARA 1000-     **
 * *                                 SET UBOC-UOW-OK TO TRUE.    **
 * * 24863B    24 JUL 02   03539    -IN PARA 1000- CHANGED       **
 * *                                 LOGGING OF ERRORS AFTER     **
 * *                                 CALL TO XPXCDAT.            **
 * *                                -ADDED IN WORKING STORAGE,   **
 * *                                 WS-BUS-OBJ-NM FOR           **
 * *                                 PERFORMANCE MONITORING      **
 * ****************************************************************
 * *                                 COLUMN INDICATOR            **
 * ****************************************************************</pre>*/
public class Caws002 extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>* DCLGEN USED FOR ACCESSING DATA SECURITY.
	 * *   EXEC SQL
	 * *       INCLUDE HALLGDPS
	 * *   END-EXEC.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS
	 *        05  CIDP-TABLE-NAME                PIC X(18).
	 *        05  CIDP-TABLE-ROW                 PIC X(4982).</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private ExecContext execContext = null;
	private HalSecDpDfltVDao halSecDpDfltVDao = new HalSecDpDfltVDao(dbAccessStatus);
	private HalMsgTransprtVDao halMsgTransprtVDao = new HalMsgTransprtVDao(dbAccessStatus);
	private HalNlbeWngTxtVDao halNlbeWngTxtVDao = new HalNlbeWngTxtVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private Caws002Data ws = new Caws002Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaCaws002 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaCaws002 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		return1();
		return 0;
	}

	public static Caws002 getInstance() {
		return (Programs.getInstance(Caws002.class));
	}

	/**Original name: 0000-MAINLINE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  MAIN PROCESSING CONTROL                                        *
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		// COB_CODE: PERFORM 1000-INITIALIZE.
		initialize();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		// COB_CODE:      IF NOT UBOC-HALT-AND-RETURN
		//                   PERFORM 2000-PROCESS
		//                ELSE
		//           * IF AN ERROR HAS OCCURED BLANK THE DATA BUFFER SO THAT
		//           * THERE IS LESS CHANCE OF UNAUTORISED DATA MAKING ITS WAY
		//           * BACK TO THE USER
		//                   GO TO 0000-RETURN
		//                END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 2000-PROCESS
			process();
		} else {
			// IF AN ERROR HAS OCCURED BLANK THE DATA BUFFER SO THAT
			// THERE IS LESS CHANCE OF UNAUTORISED DATA MAKING ITS WAY
			// BACK TO THE USER
			// COB_CODE: MOVE SPACES TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer("");
			// COB_CODE: GO TO 0000-RETURN
			return1();
		}
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 7000-INBOUND-DP
		//           END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 7000-INBOUND-DP
			inboundDp();
		}
		// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
		//              PERFORM 8000-TERMINATE
		//           END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: PERFORM 8000-TERMINATE
			terminate();
		}
	}

	/**Original name: 0000-RETURN<br>*/
	private void return1() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 1000-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  INITIALISE PARAMETERS & WORKING STORAGE                        *
	 * *****************************************************************
	 * * GENERAL INITIALISATION.</pre>*/
	private void initialize() {
		Xpiodat xpiodat = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE ZERO TO HUOH-HIER-SEQ-NBR.
		ws.getDclhalUowObjHierV().setHierSeqNbr(((short) 0));
		// COB_CODE: INITIALIZE CW02C-CLIENT-TAB-ROW.
		initClientTabRow();
		//*** INITIALIZE PUGID-VALUES.
		// COB_CODE: IF NOT SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
		//              MOVE CIDP-TABLE-ROW  TO CW02C-CLIENT-TAB-ROW
		//           END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()) {
			// COB_CODE: MOVE UBOC-APP-DATA-BUFFER(1:UBOC-APP-DATA-BUFFER-LENGTH)
			//             TO WS-DATA-PRIVACY-INFO
			ws.setWsDataPrivacyInfoFormatted(
					dfhcommarea.getUbocRecord().getAppDataBufferFormatted().substring((1) - 1, dfhcommarea.getUbocRecord().getAppDataBufferLength()));
			// COB_CODE: MOVE CIDP-TABLE-ROW  TO CW02C-CLIENT-TAB-ROW
			ws.getCawlc002().setClientTabRowFormatted(ws.getCidpTableInfo().getTableRowFormatted());
		}
		// COB_CODE: INITIALIZE WS-HOLD-AREA.
		initWsHoldArea();
		// COB_CODE: MOVE 999999999 TO WS-HOLD-INITIAL-AMT.
		ws.getWsSpecificWorkAreas().setWsHoldInitialAmt(Trunc.toDecimal(999999999, 14, 2));
		// COB_CODE: COMPUTE WS-HOLD-INITIAL-AMT EQUAL WS-HOLD-INITIAL-AMT * -1.
		ws.getWsSpecificWorkAreas().setWsHoldInitialAmt(Trunc.toDecimal(ws.getWsSpecificWorkAreas().getWsHoldInitialAmt().multiply(-1), 14, 2));
		// COB_CODE: MOVE WS-HOLD-INITIAL-AMT TO WS-HOLD-AMT.
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldAmt(Trunc.toDecimal(ws.getWsSpecificWorkAreas().getWsHoldInitialAmt(), 14, 2));
		// COB_CODE: INITIALIZE WS-ACT-CURRENT-ISO-TS-AREA.
		initWsActCurrentIsoTsArea();
		// COB_CODE: SET DPER-DATA-PRIV-CHECK-OK TO TRUE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setDataPrivCheckOk();
		// COB_CODE: SET DPER-DATA-PRIV-CHECK-OK TO TRUE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setDataPrivCheckOk();
		// THE NEXT LINE HAS BEEN GENERATED WITH THE CALCULATED NUMBER
		// OF DATA ROWS IN THE WORKING STORAGE TABLE ABOVE, AND MUST
		// REMAIN IN SYNC WITH THE OCCURS CLAUSE OF THAT TABLE.
		// COB_CODE: MOVE 28 TO WS-NUM-TBL-ENTRIES.
		ws.getWsSpecificWorkAreas().setWsNumTblEntries(((short) 28));
		//    SET UP WORKING STORAGE VALUES FOR CURSOR READS HERE
		// COB_CODE: MOVE UBOC-UOW-NAME TO WS-UOW-NM.
		ws.getWsGenericFields().setUowNm(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-SEC-GROUP-NAME TO WS-SEC-GRP-NM.
		ws.getWsGenericFields().setSecGrpNm(dfhcommarea.getUbocRecord().getCommInfo().getUbocSecGroupName());
		// COB_CODE: MOVE UBOC-AUTH-USERID    TO WS-AUTH-USERID.
		ws.getWsGenericFields().setAuthUserid(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: MOVE UBOC-SEC-ASSOCIATION-TYPE TO WS-SEC-ASC-TYP.
		ws.getWsGenericFields().setSecAscTyp(dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getSecAssociationType());
		// COB_CODE: IF UBOC-NO-CONTEXT
		//           OR UBOC-SECURITY-CONTEXT GREATER THAN SPACES
		//              MOVE UBOC-SECURITY-CONTEXT TO WS-FULL-CONTEXT
		//           ELSE
		//               PERFORM 1090-READ-URQM-CTX-MSG
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().isNoContext()
				|| Characters.GT_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext())) {
			// COB_CODE: MOVE UBOC-SECURITY-CONTEXT TO WS-FULL-CONTEXT
			ws.getWsSpecificWorkAreas().getWsContextRedef()
					.setWsFullContext(dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext());
		} else {
			// COB_CODE: PERFORM 1090-READ-URQM-CTX-MSG
			readUrqmCtxMsg();
		}
		//* GET S3 DATE
		// COB_CODE: MOVE SPACES TO DATE-STRUCTURE.
		ws.getDateStructure().initDateStructureSpaces();
		// COB_CODE: MOVE 'C' TO DATE-FUNCTION.
		ws.getDateStructure().setDateFunctionFormatted("C");
		// COB_CODE: MOVE SPACES TO DATE-LANGUAGE.
		ws.getDateStructure().setDateLanguage("");
		// COB_CODE: MOVE 'S3' TO DATE-INP-FORMAT.
		ws.getDateStructure().setDateInpFormat("S3");
		// COB_CODE: MOVE 'B' TO DATE-OUT-FORMAT.
		ws.getDateStructure().setDateOutFormat("B");
		// COB_CODE: MOVE SPACES TO DATE-INPUT.
		ws.getDateStructure().setDateInput("");
		//*************************************************************         ***
		//                                                            *         ...
		//    XPXCDAT - COPYBOOK USED TO LINK TO XPIODAT, THE ON-LINE *    04/11/01
		//              COBOL DATE ROUTINE.                           *    XPXCDAT
		//                                                            *       LV004
		// IR REF    DATE   RELEASE            DESCRIPTION            *    00006
		// XXXXXX  DDMMMYY    X.X    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *    00007
		// XXXXXX  16JAN03  DNF     CHANGED TO R70 STRUTURE. FEDERATED*    00008
		// 002607  06JUL00  IND9176 ADDED CODE IN ORDER TO MAKE IT A  *    00008
		//                          DYNAMIC CALL TO XPIODAT.          *    00009
		//*************************************************************    00010
		//    MOVE LENGTH OF DATE-STRUCTURE TO DATE-STRUCTURE-LENGTH.      00012
		//**  EXEC CICS LINK                                               00014
		//**       PROGRAM  ('XPIODAT')                                    00015
		//**       COMMAREA (DATE-STRUCTURE)                               00016
		//**       LENGTH   (DATE-STRUCTURE-LENGTH)                        00017
		//**       NOHANDLE                                                00018
		//**       END-EXEC.                                               00019
		//**                                                               00020
		//    MOVE 'XPIODAT ' TO WS-XPIODAT-MODULE.                        00021
		//    CALL WS-XPIODAT-MODULE USING                                 00022
		//         DFHEIBLK                                                00023
		//         DFHCOMMAREA                                             00024
		//         DATE-STRUCTURE.                                         00025
		// COB_CODE: CALL 'XPIODAT' USING DATE-STRUCTURE.
		xpiodat = Xpiodat.getInstance();
		xpiodat.run(ws.getDateStructure());
		// COB_CODE: IF DATE-RETURN-CODE NOT EQUAL 'OK'
		//           OR DATE-OUTPUT EQUAL SPACES
		//               GO TO 1000-INITIALIZE-X
		//           END-IF.
		if (!Conditions.eq(ws.getDateStructure().getDateReturnCode(), "OK") || Characters.EQ_SPACE.test(ws.getDateStructure().getDateOutput())) {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-IAP-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalIapFailed();
			// COB_CODE: SET IAP-DATE-RETURN-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setIapDateReturnError();
			// COB_CODE: MOVE '1000-INITIALIZE' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1000-INITIALIZE");
			// COB_CODE: MOVE 'IAP DATE CALL FAILED' TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("IAP DATE CALL FAILED");
			// COB_CODE: STRING 'IAP RETURN CODE: '  DATE-RETURN-CODE ';'
			//                  'EXTENDED ERROR: '   DATE-EXTENDED-ERROR ';'
			//                  'OUTPUT: '           DATE-OUTPUT ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "IAP RETURN CODE: ", ws.getDateStructure().getDateReturnCodeFormatted(), ";", "EXTENDED ERROR: ",
							ws.getDateStructure().getScratchAreaRedef().getDateExtendedErrorFormatted(), ";", "OUTPUT: ",
							ws.getDateStructure().getDateOutputFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1000-INITIALIZE-X
			return;
		}
		// COB_CODE: MOVE DATE-OUTPUT TO WS-SE3-CUR-ISO-DATE-TIME.
		ws.getWsSe3CurIsoDateTime().setWsSe3CurIsoDateTimeFormatted(ws.getDateStructure().getDateOutputFormatted());
		//* THIS AREA IS TO BE USED TO RETRIEVE THE ASSOCIATED INFORMATION
		//* TO BE COMPARED AGAINST THE USER INFO RETRIEVED ABOVE (E.G. THE
		//* RISK STATE OF THE POLICY, ETC.)  IF NO INFO NEEDED, COMMENT
		//* OUT THE LINES BELOW.
		// COB_CODE: PERFORM 1020-GET-EXTERNAL-VALS.
		getExternalVals();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 1000-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-INITIALIZE-X
			return;
		}
	}

	/**Original name: 1020-GET-EXTERNAL-VALS_FIRST_SENTENCES<br>*/
	private void getExternalVals() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EVALUATE WS-APPLICATION-NM
		//              WHEN 'BCWS'
		//                 PERFORM 1050-GET-EXT-BCWS-VALS
		//              WHEN 'CHF'
		//                 PERFORM 1050-GET-EXT-CHF-VALS
		//              WHEN 'CMT'
		//                 PERFORM 1050-GET-EXT-CMT-VALS
		//              WHEN 'CIWS'
		//                 PERFORM 1050-GET-EXT-CIWS-VALS
		//              WHEN 'CWS'
		//                 PERFORM 1050-GET-EXT-CWS-VALS
		//              WHEN 'DWS'
		//                 PERFORM 1050-GET-EXT-DWS-VALS
		//              WHEN 'SAV_ARCH'
		//                 PERFORM 1050-GET-EXT-ARCH-VALS
		//              WHEN 'UWS'
		//                 PERFORM 1050-GET-EXT-UWS-VALS
		//              WHEN 'WIP'
		//                 PERFORM 1050-GET-EXT-WIP-VALS
		//              WHEN OTHER
		//                 GO TO 1020-GET-EXTERNAL-VALS-X
		//           END-EVALUATE.
		switch (ws.getWsSpecificWorkAreas().getWsApplicationNm()) {

		case "BCWS":// COB_CODE: PERFORM 1050-GET-EXT-BCWS-VALS
			getExtBcwsVals();
			break;

		case "CHF":// COB_CODE: PERFORM 1050-GET-EXT-CHF-VALS
			getExtChfVals();
			break;

		case "CMT":// COB_CODE: PERFORM 1050-GET-EXT-CMT-VALS
			getExtCmtVals();
			break;

		case "CIWS":// COB_CODE: PERFORM 1050-GET-EXT-CIWS-VALS
			getExtCiwsVals();
			break;

		case "CWS":// COB_CODE: PERFORM 1050-GET-EXT-CWS-VALS
			getExtCwsVals();
			break;

		case "DWS":// COB_CODE: PERFORM 1050-GET-EXT-DWS-VALS
			getExtDwsVals();
			break;

		case "SAV_ARCH":// COB_CODE: PERFORM 1050-GET-EXT-ARCH-VALS
			getExtArchVals();
			break;

		case "UWS":// COB_CODE: PERFORM 1050-GET-EXT-UWS-VALS
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=CAWS002.CBL:line=852, because the code is unreachable.
			break;

		case "WIP":// COB_CODE: PERFORM 1050-GET-EXT-WIP-VALS
			getExtWipVals();
			break;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-BUS-PROCESS-FAILED  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusProcessFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE UBOC-UOW-KEY-REPLACE-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore());
			// COB_CODE: MOVE '1020-GET-EXTERNAL-VALS-X'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1020-GET-EXTERNAL-VALS-X");
			// COB_CODE: STRING 'UNKNOWN APPLICATION-NM SPECIFIED; '
			//                  'WS-APPLICATION-NM: '
			//                  WS-APPLICATION-NM
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ERR-COMMENT
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ERR_COMMENT, "UNKNOWN APPLICATION-NM SPECIFIED; ", "WS-APPLICATION-NM: ",
					ws.getWsSpecificWorkAreas().getWsApplicationNmFormatted());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalErrComment(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrCommentFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1020-GET-EXTERNAL-VALS-X
			return;
		}
	}

	/**Original name: 1050-GET-EXT-BCWS-VALS_FIRST_SENTENCES<br>*/
	private void getExtBcwsVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-CHF-VALS_FIRST_SENTENCES<br>*/
	private void getExtChfVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-CMT-VALS_FIRST_SENTENCES<br>*/
	private void getExtCmtVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-CIWS-VALS_FIRST_SENTENCES<br>*/
	private void getExtCiwsVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-CWS-VALS_FIRST_SENTENCES<br>*/
	private void getExtCwsVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-DWS-VALS_FIRST_SENTENCES<br>*/
	private void getExtDwsVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-ARCH-VALS_FIRST_SENTENCES<br>*/
	private void getExtArchVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1050-GET-EXT-WIP-VALS_FIRST_SENTENCES<br>*/
	private void getExtWipVals() {
		// COB_CODE: CONTINUE.
		//continue
	}

	/**Original name: 1090-READ-URQM-CTX-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR A CONTEXT MESSAGE                     *
	 *                                                                 *
	 *  ONLY ONE ROW FOR 'DATA_PRIVACY_CONTEXT' SHOULD BE FOUND.       *
	 *  THE CONTEXT WILL THEN BE USED TO READ THE HAL_UOW_FUN_SEC TABLE*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readUrqmCtxMsg() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID       TO URQM-ID.
		ws.getUrqmCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-DATA-PRIV-CTX-LIT TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(ws.getWsSpecificWorkAreas().getWsDataPrivCtxLit());
		// COB_CODE: MOVE 1                 TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(1);
		// COB_CODE: MOVE SPACES  TO URQM-UOW-MESSAGE-BUFFER.
		ws.getUrqmCommon().initUowMessageBufferSpaces();
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF NORMAL, MOVE CONTEXT NAME TO UBOC-SECURITY-CONTEXT
		//* IF NOTFND, SET UBOC-NO-CONTEXT TO TRUE
		//* OTHERWISE WE HAVE AN UNEXPECTED ERROR.
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 1090-READ-URQM-CTX-MSG-X
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET UBOC-NO-CONTEXT TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().setNoContext();
			// COB_CODE: GO TO 1090-READ-URQM-CTX-MSG-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '1090-READ-URQM-CTX-MSG'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1090-READ-URQM-CTX-MSG");
			// COB_CODE: MOVE 'READ REQ MSG STORE FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ MSG STORE FAILED");
			// COB_CODE: STRING 'URQM-KEY=' URQM-KEY ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "URQM-KEY=", ws.getUrqmCommon().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
		// COB_CODE: MOVE URQM-MSG-DATA(1:URQM-BUS-OBJ-DATA-LENGTH)
		//               TO HCTXC-CONTEXT-INFO.
		ws.getHallcctx().setInfoFormatted(ws.getUrqmCommon().getMsgDataFormatted().substring((1) - 1, ws.getUrqmCommon().getBusObjDataLength()));
		// COB_CODE: MOVE HCTXC-CONTEXT TO UBOC-SECURITY-CONTEXT
		//                                 WS-FULL-CONTEXT.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().setSecurityContext(ws.getHallcctx().getT());
		ws.getWsSpecificWorkAreas().getWsContextRedef().setWsFullContext(ws.getHallcctx().getT());
	}

	/**Original name: 2000-PROCESS_FIRST_SENTENCES<br>
	 * <pre> THE FIRST CURSOR WILL READ WITH DEFAULT INFORMATION (SPACE, 999)
	 *  AND PROCESS ANY HITS.</pre>*/
	private void process() {
		// COB_CODE: SET WS-NOT-END-OF-CURSOR1 TO TRUE.
		ws.getWsBdoSwitches().getEndOfCursor1Sw().setNotEndOfCursor1();
		// COB_CODE: PERFORM 2010-PROCESS-CURSOR1
		//             UNTIL WS-END-OF-CURSOR1
		//                OR UBOC-HALT-AND-RETURN.
		while (!(ws.getWsBdoSwitches().getEndOfCursor1Sw().isEndOfCursor1()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
			processCursor1();
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 2000-PROCESS-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-PROCESS-X
			return;
		}
	}

	/**Original name: 2010-PROCESS-CURSOR1_FIRST_SENTENCES<br>*/
	private void processCursor1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: EXEC SQL
		//              OPEN WS-CURSOR1
		//           END-EXEC.
		halSecDpDfltVDao.openWsCursor1(ws);
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2010-PROCESS-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_SEC_DP_DFLT_V'   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SEC_DP_DFLT_V");
			// COB_CODE: MOVE '2010-PROCESS-CURSOR1'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2010-PROCESS-CURSOR1");
			// COB_CODE: MOVE 'OPEN CURSOR1 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR1 FAILED");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'UOW-NM='
			//                   WS-UOW-NM ';'
			//                  'SEC-GRP-NM='
			//                   WS-SEC-GRP-NM ';'
			//                 DELIMITED BY SIZE
			//                 INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "UOW-NM=",
							ws.getWsGenericFields().getUowNmFormatted(), ";", "SEC-GRP-NM=", ws.getWsGenericFields().getSecGrpNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2010-PROCESS-CURSOR1-X
			return;
		}
		// COB_CODE: PERFORM 2025-FETCH-CURSOR1
		//               UNTIL UBOC-HALT-AND-RETURN
		//                   OR WS-END-OF-CURSOR1.
		while (!(dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getEndOfCursor1Sw().isEndOfCursor1())) {
			fetchCursor1();
		}
		// COB_CODE: EXEC SQL
		//              CLOSE WS-CURSOR1
		//           END-EXEC.
		halSecDpDfltVDao.closeWsCursor1();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2010-PROCESS-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_SEC_DP_DFLT_V'   TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SEC_DP_DFLT_V");
			// COB_CODE: MOVE '2010-PROCESS-CURSOR1'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2010-PROCESS-CURSOR1");
			// COB_CODE: MOVE 'CLOSE CURSOR1 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR1 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2010-PROCESS-CURSOR1-X
			return;
		}
	}

	/**Original name: 2025-FETCH-CURSOR1_FIRST_SENTENCES<br>*/
	private void fetchCursor1() {
		// COB_CODE: EXEC SQL
		//            FETCH WS-CURSOR1
		//           INTO   :HSDD-UOW-NM
		//                 ,:HSDD-SEC-GRP-NM
		//                 ,:HSDD-SEC-ASC-TYP
		//                 ,:HSDD-GEN-TXT-FLD-1
		//                 ,:HSDD-GEN-TXT-FLD-2
		//                 ,:HSDD-GEN-TXT-FLD-3
		//                 ,:HSDD-GEN-TXT-FLD-4
		//                 ,:HSDD-GEN-TXT-FLD-5
		//                 ,:HSDD-BUS-OBJ-NM
		//                 ,:HSDD-REQ-TYP-CD
		//                 ,:HSDD-EFFECTIVE-DT
		//                 ,:HSDD-EXPIRATION-DT
		//                 ,:HSDD-SEQ-NBR
		//                 ,:HSDD-CMN-NM
		//                 ,:HSDD-CMN-ATR-IND
		//                 ,:HSDD-DFL-TYP-IND
		//                 ,:HSDD-DFL-DTA-AMT
		//                  :WS-DFL-DTA-AMT-NI
		//                 ,:HSDD-DFL-DTA-TXT
		//                 ,:HSDD-DFL-OFS-NBR
		//                  :WS-DFL-OFS-NBR-NI
		//                 ,:HSDD-DFL-OFS-PER
		//                 ,:HSDD-CONTEXT
		//           END-EXEC.
		halSecDpDfltVDao.fetchWsCursor1(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 2025-FETCH-CURSOR1-X
		//               WHEN OTHER
		//                   GO TO 2025-FETCH-CURSOR1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
			ws.getWsBdoSwitches().getEndOfCursor1Sw().setEndOfCursor1();
			// COB_CODE: GO TO 2025-FETCH-CURSOR1-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE WS-PROGRAM-NAME         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsSpecificWorkAreas().getWsProgramName());
			// COB_CODE: MOVE '2025-FETCH-CURSOR1'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2025-FETCH-CURSOR1");
			// COB_CODE: MOVE 'FETCH FROM CURSOR1 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM CURSOR1 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2025-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 2025-FETCH-CURSOR1-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2025-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: PERFORM 3000-PROCESS-RECORD.
		processRecord();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 2025-FETCH-CURSOR1-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2025-FETCH-CURSOR1-X
			return;
		}
		// COB_CODE: INITIALIZE WS-HOLD-AREA.
		initWsHoldArea();
		// COB_CODE: MOVE WS-HOLD-INITIAL-AMT TO WS-HOLD-AMT.
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldAmt(Trunc.toDecimal(ws.getWsSpecificWorkAreas().getWsHoldInitialAmt(), 14, 2));
	}

	/**Original name: 3000-PROCESS-RECORD_FIRST_SENTENCES<br>*/
	private void processRecord() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF UBOC-NO-CONTEXT
		//           OR WS-CONTEXT-FIRST-10 EQUAL HSDD-CONTEXT
		//           OR WS-FULL-CONTEXT     EQUAL HSDD-CONTEXT
		//           OR HSDD-CONTEXT        EQUAL SPACES
		//              CONTINUE
		//           ELSE
		//               GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocSecAndDataPrivInfo().isNoContext()
				|| Conditions.eq(ws.getWsSpecificWorkAreas().getWsContextRedef().getContextFirst10(), ws.getDclhalSecDpDflt().getContext())
				|| Conditions.eq(ws.getWsSpecificWorkAreas().getWsContextRedef().getWsFullContext(), ws.getDclhalSecDpDflt().getContext())
				|| Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getContext())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		// IF THE COLUMN NAME IS SPACES,THE ENTIRE ROW SHOULD BE CONSIDERED
		// TO BE EXCLUDED.  NO REASON TO PROCESS FURTHER, SO RETURN.
		// COB_CODE: IF HSDD-CMN-NM = SPACES
		//              END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getCmnNm())) {
			// COB_CODE: IF NOT SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
			//              END-IF
			//           ELSE
			//              END-IF
			//           END-IF
			if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()) {
				// COB_CODE: IF (HSDD-REQ-TYP-DATA-PRIVACY
				//           OR  HSDD-REQ-TYP-BOTH)
				//              GO TO 3000-PROCESS-RECORD-X
				//           ELSE
				//              GO TO 3000-PROCESS-RECORD-X
				//           END-IF
				if (ws.getDclhalSecDpDflt().getReqTypCd().isDataPrivacy() || ws.getDclhalSecDpDflt().getReqTypCd().isBoth()) {
					// COB_CODE: SET DPER-ROW-EXCLUDED TO TRUE
					dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setRowExcluded();
					// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
					ws.getWsBdoSwitches().getEndOfCursor1Sw().setEndOfCursor1();
					// COB_CODE: GO TO 3000-PROCESS-RECORD-X
					return;
				} else {
					// COB_CODE: GO TO 3000-PROCESS-RECORD-X
					return;
				}
			} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()) {
				// COB_CODE: IF SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
				//              END-IF
				//           END-IF
				// COB_CODE: IF (HSDD-REQ-TYP-DEFAULT
				//           OR  HSDD-REQ-TYP-BOTH)
				//              GO TO 3000-PROCESS-RECORD-X
				//           ELSE
				//              GO TO 3000-PROCESS-RECORD-X
				//           END-IF
				if (ws.getDclhalSecDpDflt().getReqTypCd().isDefaultFld() || ws.getDclhalSecDpDflt().getReqTypCd().isBoth()) {
					// COB_CODE: SET DPER-ROW-EXCLUDED TO TRUE
					dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setRowExcluded();
					// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
					ws.getWsBdoSwitches().getEndOfCursor1Sw().setEndOfCursor1();
					// COB_CODE: GO TO 3000-PROCESS-RECORD-X
					return;
				} else {
					// COB_CODE: GO TO 3000-PROCESS-RECORD-X
					return;
				}
			}
		}
		// THIS PARAGRAPH WILL READ THROUGH THE COPYBOOK TABLE UNTIL
		// IT FINDS A MATCH FOR THE COLUMN NAME JUST READ FROM DB2.
		// IF NO MATCH FOUND, AN ERROR WILL BE RETURNED.
		// COB_CODE: SET WS-RECORD-NOT-PROCESSED TO TRUE.
		ws.getWsSpecificWorkAreas().getWsRecordProcessedSw().setNotProcessed();
		// COB_CODE: PERFORM 3200-PROCESS-TABLE-ENTRY
		//              VARYING TBL-IDX FROM 1 BY 1
		//              UNTIL TBL-IDX GREATER THAN WS-NUM-TBL-ENTRIES
		//                 OR WS-RECORD-PROCESSED
		//                 OR UBOC-HALT-AND-RETURN.
		ws.setTblIdx(1);
		while (!(ws.getTblIdx() > ws.getWsSpecificWorkAreas().getWsNumTblEntries()
				|| ws.getWsSpecificWorkAreas().getWsRecordProcessedSw().isProcessed()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn())) {
			processTableEntry();
			ws.setTblIdx(Trunc.toInt(ws.getTblIdx() + 1, 9));
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
		// COB_CODE: IF TBL-IDX GREATER THAN WS-NUM-TBL-ENTRIES
		//           AND WS-RECORD-NOT-PROCESSED
		//              GO TO 3000-PROCESS-RECORD-X
		//           END-IF.
		if (ws.getTblIdx() > ws.getWsSpecificWorkAreas().getWsNumTblEntries()
				&& ws.getWsSpecificWorkAreas().getWsRecordProcessedSw().isNotProcessed()) {
			// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '3000-PROCESS-RECORD'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3000-PROCESS-RECORD");
			// COB_CODE: MOVE 'REQUESTED COLUMN NAME NOT FOUND IN TABLE'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("REQUESTED COLUMN NAME NOT FOUND IN TABLE");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'HSDD-CMN-NM= '
			//                   HSDD-CMN-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HSDD-BUS-OBJ-NM = ",
					ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ", ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3000-PROCESS-RECORD-X
			return;
		}
	}

	/**Original name: 3200-PROCESS-TABLE-ENTRY_FIRST_SENTENCES<br>*/
	private void processTableEntry() {
		// COB_CODE: IF WS-COLUMN-NAME(TBL-IDX) NOT = HSDD-CMN-NM
		//              GO TO 3200-PROCESS-TABLE-ENTRY-X
		//           END-IF.
		if (!Conditions.eq(ws.getCw02cClientTabTab().getWsColumnName(ws.getTblIdx()), ws.getDclhalSecDpDflt().getCmnNm())) {
			// COB_CODE: GO TO 3200-PROCESS-TABLE-ENTRY-X
			return;
		}
		// COB_CODE: IF (HSDD-REQ-TYP-DEFAULT
		//               OR HSDD-REQ-TYP-BOTH)
		//           AND NOT SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
		//               SET WS-IGNORE-COL-IND TO TRUE
		//           END-IF.
		if ((ws.getDclhalSecDpDflt().getReqTypCd().isDefaultFld() || ws.getDclhalSecDpDflt().getReqTypCd().isBoth())
				&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()) {
			// COB_CODE: SET WS-IGNORE-COL-IND TO TRUE
			ws.getWsSpecificWorkAreas().getWsHoldArea().setIgnoreColInd();
		}
		// COB_CODE: IF  HSDD-REQ-TYP-DATA-PRIVACY
		//           AND SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
		//               SET WS-IGNORE-COL-IND TO TRUE
		//           END-IF.
		if (ws.getDclhalSecDpDflt().getReqTypCd().isDataPrivacy()
				&& dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()) {
			// COB_CODE: SET WS-IGNORE-COL-IND TO TRUE
			ws.getWsSpecificWorkAreas().getWsHoldArea().setIgnoreColInd();
		}
		//  IF THIS IS A SET DEFAULT REQUEST, PERFORM BOTH DEFAULT
		//  AND DATA PRIVACY (COL. IND.) PROCESSING.
		// COB_CODE: IF SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
		//           AND (HSDD-REQ-TYP-DEFAULT
		//            OR  HSDD-REQ-TYP-BOTH)
		//              END-IF
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()
				&& (ws.getDclhalSecDpDflt().getReqTypCd().isDefaultFld() || ws.getDclhalSecDpDflt().getReqTypCd().isBoth())) {
			// COB_CODE: PERFORM 3400-PROCESS-DEFAULTS
			processDefaults();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 3200-PROCESS-TABLE-ENTRY-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-PROCESS-TABLE-ENTRY-X
				return;
			}
			// COB_CODE: PERFORM 3600-PROCESS-DATA-PRIVACY
			processDataPrivacy();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 3200-PROCESS-TABLE-ENTRY-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-PROCESS-TABLE-ENTRY-X
				return;
			}
		}
		//  IF THIS IS NOT A SET DEFAULT REQUEST, PERFORM ONLY THE
		//  DATA PRIVACY (COL. IND.) PROCESSING, IF IT CORRESPONDS TO
		//  THE TYPE OF REQUEST ON THE DATABSE ROW.
		// COB_CODE: IF NOT SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO
		//           AND (HSDD-REQ-TYP-DATA-PRIVACY
		//            OR  HSDD-REQ-TYP-BOTH)
		//              END-IF
		//           END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSetDefaultsRequest()
				&& (ws.getDclhalSecDpDflt().getReqTypCd().isDataPrivacy() || ws.getDclhalSecDpDflt().getReqTypCd().isBoth())) {
			// COB_CODE: PERFORM 3600-PROCESS-DATA-PRIVACY
			processDataPrivacy();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 3200-PROCESS-TABLE-ENTRY-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3200-PROCESS-TABLE-ENTRY-X
				return;
			}
			//  IF THIS IS A LINK REQUEST TO ANOTHER MODULE, LINK AND RETURN
			// COB_CODE: IF HSDD-DFL-TYP-LINK
			//              END-IF
			//           END-IF
			if (ws.getDclhalSecDpDflt().getDflTypInd().isLink()) {
				// COB_CODE: MOVE HSDD-DFL-DTA-TXT TO WS-LINK-TO-PGM-NM
				ws.getWsSpecificWorkAreas().getWsHoldArea().setLinkToPgmNm(ws.getDclhalSecDpDflt().getDflDtaTxt());
				// COB_CODE: PERFORM 3700-LINK-TO-REQ-PGM
				linkToReqPgm();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//              GO TO 3200-PROCESS-TABLE-ENTRY-X
				//           END-IF
				if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3200-PROCESS-TABLE-ENTRY-X
					return;
				}
			}
		}
		// COB_CODE: IF NOT HSDD-DFL-TYP-LINK
		//              PERFORM 5000-LOAD-CPYBK-VALUES
		//           END-IF.
		if (!ws.getDclhalSecDpDflt().getDflTypInd().isLink()) {
			// COB_CODE: PERFORM 5000-LOAD-CPYBK-VALUES
			loadCpybkValues();
		}
		// COB_CODE: SET WS-RECORD-PROCESSED TO TRUE.
		ws.getWsSpecificWorkAreas().getWsRecordProcessedSw().setProcessed();
	}

	/**Original name: 3400-PROCESS-DEFAULTS_FIRST_SENTENCES<br>*/
	private void processDefaults() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF HSDD-DFL-TYP-IND EQUAL SPACE
		//              GO TO 3400-PROCESS-DEFAULTS-X.
		if (Conditions.eq(ws.getDclhalSecDpDflt().getDflTypInd().getDflTypInd(), Types.SPACE_CHAR)) {
			// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X.
			return;
		}
		// COB_CODE: EVALUATE TRUE
		//               WHEN HSDD-DFL-TYP-CHAR
		//                    TO WS-HOLD-DTA-TXT
		//               WHEN HSDD-DFL-TYP-NUM
		//                  END-IF
		//               WHEN HSDD-DFL-TYP-DATE
		//                  END-IF
		//               WHEN HSDD-DFL-TYP-TS
		//                  END-IF
		//               WHEN HSDD-DFL-TYP-LINK
		//                    END-IF
		//           END-EVALUATE.
		switch (ws.getDclhalSecDpDflt().getDflTypInd().getDflTypInd()) {

		case HsddDflTypInd.CHAR_FLD:// COB_CODE: IF HSDD-DFL-DTA-TXT = SPACE
			//              GO TO 3400-PROCESS-DEFAULTS-X
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getDflDtaTxt())) {
				// COB_CODE: SET WS-LOG-ERROR                TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
				// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
				// COB_CODE: MOVE '3400-PROCESS-DEFAULTS'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-PROCESS-DEFAULTS");
				// COB_CODE: MOVE 'DEFAULT DATA TEXT NOT VALUED'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DEFAULT DATA TEXT NOT VALUED");
				// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
				//                   HSDD-BUS-OBJ-NM ';'
				//                  'HSDD-CMN-NM= '
				//                   HSDD-CMN-NM ';'
				//                  'HSDD-DFL-DTA-TXT= '
				//                   HSDD-DFL-DTA-TXT     ';'
				//                  DELIMITED BY SIZE
				//                  INTO EFAL-OBJ-DATA-KEY
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
						new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
								ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-DFL-DTA-TXT= ",
								ws.getDclhalSecDpDflt().getDflDtaTxtFormatted(), ";" });
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
				return;
			}
			// COB_CODE: MOVE HSDD-DFL-DTA-TXT
			//             TO WS-HOLD-DTA-TXT
			ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getDclhalSecDpDflt().getDflDtaTxt());
			break;

		case HsddDflTypInd.NUM:// COB_CODE: IF WS-DFL-DTA-AMT-NI LESS THAN ZERO
			//              GO TO 3400-PROCESS-DEFAULTS-X
			//           ELSE
			//              END-IF
			//           END-IF
			if (ws.getWsSpecificWorkAreas().getWsDflDtaAmtNi() < 0) {
				// COB_CODE: SET WS-LOG-ERROR                TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
				// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
				// COB_CODE: MOVE '3400-PROCESS-DEFAULTS'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-PROCESS-DEFAULTS");
				// COB_CODE: MOVE 'DEFAULT AMOUNT NOT NUMERIC'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DEFAULT AMOUNT NOT NUMERIC");
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
				return;
			} else {
				// COB_CODE: MOVE HSDD-DFL-DTA-AMT TO WS-HOLD-AMT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldAmt(Trunc.toDecimal(ws.getDclhalSecDpDflt().getDflDtaAmt(), 14, 2));
				// COB_CODE: IF WS-HOLD-AMT < 0
				//              MOVE '-' TO WS-HOLD-SIGN
				//           ELSE
				//              MOVE '+' TO WS-HOLD-SIGN
				//           END-IF
				if (ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldAmt().compareTo(0) < 0) {
					// COB_CODE: MOVE '-' TO WS-HOLD-SIGN
					ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldSignFormatted("-");
				} else {
					// COB_CODE: MOVE '+' TO WS-HOLD-SIGN
					ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldSignFormatted("+");
				}
			}
			break;

		case HsddDflTypInd.DATE_FLD:// COB_CODE: IF HSDD-DFL-DTA-TXT = SPACE
			//              GO TO 3400-PROCESS-DEFAULTS-X
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getDflDtaTxt())) {
				// COB_CODE: SET WS-LOG-ERROR                TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
				// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
				// COB_CODE: MOVE '3400-PROCESS-DEFAULTS'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-PROCESS-DEFAULTS");
				// COB_CODE: MOVE 'DEFAULT DATE IS SPACES'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("DEFAULT DATE IS SPACES");
				// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
				//                   HSDD-BUS-OBJ-NM ';'
				//                  'HSDD-CMN-NM= '
				//                   HSDD-CMN-NM ';'
				//                  'HSDD-DFL-DTA-TXT= '
				//                   HSDD-DFL-DTA-TXT     ';'
				//                  DELIMITED BY SIZE
				//                  INTO EFAL-OBJ-DATA-KEY
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
						new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
								ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-DFL-DTA-TXT= ",
								ws.getDclhalSecDpDflt().getDflDtaTxtFormatted(), ";" });
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
				return;
			}
			// COB_CODE: IF HSDD-DFL-DTA-TXT = 'S3_DATE'
			//           OR HSDD-DFL-DTA-TXT = 'S3-DATE'
			//              MOVE WS-SE3-CUR-ISO-DATE TO WS-HOLD-DTA-TXT
			//           ELSE
			//              END-IF
			//           END-IF
			if (Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3_DATE")
					|| Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3-DATE")) {
				// COB_CODE: MOVE WS-SE3-CUR-ISO-DATE TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsSe3CurIsoDateTime().getSe3CurIsoDate());
			} else if (Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT_DATE")
					|| Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT-DATE")) {
				// COB_CODE: IF HSDD-DFL-DTA-TXT = 'CURRENT_DATE'
				//           OR HSDD-DFL-DTA-TXT = 'CURRENT-DATE'
				//                 TO WS-HOLD-DTA-TXT
				//           ELSE
				//              END-IF
				//           END-IF
				// COB_CODE: IF WS-ACT-CUR-ISO-DATE EQUAL SPACE
				//              END-IF
				//           END-IF
				if (Characters.EQ_SPACE.test(ws.getWsActCurIsoDateTime().getCurIsoDate())) {
					// COB_CODE: PERFORM 3440-GET-ACTUAL-DT-TS
					getActualDtTs();
					// COB_CODE: IF UBOC-HALT-AND-RETURN
					//              GO TO 3400-PROCESS-DEFAULTS-X
					//           END-IF
					if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
						// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
						return;
					}
				}
				// COB_CODE: MOVE WS-ACT-CUR-ISO-DATE
				//             TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsActCurIsoDateTime().getCurIsoDate());
			} else {
				// COB_CODE: MOVE HSDD-DFL-DTA-TXT TO WS-VALID-DATE-HOLD
				ws.getWsSpecificWorkAreas().getWsHoldArea().setValidDateHold(ws.getDclhalSecDpDflt().getDflDtaTxt());
				// COB_CODE: PERFORM 3460-VALIDATE-DATE-FORMAT
				validateDateFormat();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//               GO TO 3400-PROCESS-DEFAULTS-X
				//           END-IF
				if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
					return;
				}
				// COB_CODE: IF WS-VALID-DATE
				//                TO WS-HOLD-DTA-TXT
				//           ELSE
				//              GO TO 3400-PROCESS-DEFAULTS-X
				//           END-IF
				if (ws.getWsBdoSwitches().getValidDateSw().isValidDate()) {
					// COB_CODE: MOVE HSDD-DFL-DTA-TXT
					//             TO WS-HOLD-DTA-TXT
					ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getDclhalSecDpDflt().getDflDtaTxt());
				} else {
					// COB_CODE: SET WS-LOG-ERROR                TO TRUE
					ws.getWsLogWarningOrErrorSw().setError();
					// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
					// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
					// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
					ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
					// COB_CODE: MOVE '3400-PROCESS-DEFAULTS'
					//             TO EFAL-ERR-PARAGRAPH
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-PROCESS-DEFAULTS");
					// COB_CODE: MOVE 'INVALID DEFAULT DATE'
					//             TO EFAL-ERR-COMMENT
					ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID DEFAULT DATE");
					// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
					//                   HSDD-BUS-OBJ-NM ';'
					//                  'HSDD-CMN-NM= '
					//                   HSDD-CMN-NM ';'
					//                  'HSDD-DFL-DTA-TXT= '
					//                   HSDD-DFL-DTA-TXT     ';'
					//                  DELIMITED BY SIZE
					//                  INTO EFAL-OBJ-DATA-KEY
					concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
							new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
									ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-DFL-DTA-TXT= ",
									ws.getDclhalSecDpDflt().getDflDtaTxtFormatted(), ";" });
					ws.getWsEstoInfo().getEstoDetailBuffer()
							.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
					// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
					logWarningOrError();
					// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
					return;
				}
			}
			// COB_CODE: IF WS-DFL-OFS-NBR-NI NOT LESS THAN ZERO
			//           AND HSDD-DFL-OFS-PER NOT EQUAL SPACES
			//              MOVE WS-CALC-TIMESTAMP TO WS-HOLD-DTA-TXT
			//           END-IF
			if (ws.getWsSpecificWorkAreas().getWsDflOfsNbrNi() >= 0
					&& !Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getDflOfsPer().getDflOfsPer())) {
				// COB_CODE: MOVE HSDD-DFL-OFS-NBR TO DATE-ADJUST-NUMB
				ws.getDateStructure().setDateAdjustNumb(TruncAbs.toInt(ws.getDclhalSecDpDflt().getDflOfsNbr(), 5));
				// COB_CODE: PERFORM 3450-CALC-OFFSET-DT
				calcOffsetDt();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//               GO TO 3400-PROCESS-DEFAULTS-X
				//           END-IF
				if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
					return;
				}
				//              MOVE WS-CALC-DT TO WS-HOLD-DTA-TXT
				// COB_CODE: MOVE WS-CALC-TIMESTAMP TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsSpecificWorkAreas().getWsHoldArea().getCalcTimestamp());
			}
			break;

		case HsddDflTypInd.TS:// COB_CODE: IF  HSDD-DFL-DTA-TXT NOT = 'S3_TIMESTAMP'
			//           AND HSDD-DFL-DTA-TXT NOT = 'S3-TIMESTAMP'
			//           AND HSDD-DFL-DTA-TXT NOT = 'CURRENT_TIMESTAMP'
			//           AND HSDD-DFL-DTA-TXT NOT = 'CURRENT-TIMESTAMP'
			//              GO TO 3400-PROCESS-DEFAULTS-X
			//           END-IF
			if (!Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3_TIMESTAMP")
					&& !Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3-TIMESTAMP")
					&& !Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT_TIMESTAMP")
					&& !Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT-TIMESTAMP")) {
				// COB_CODE: SET WS-LOG-ERROR                TO TRUE
				ws.getWsLogWarningOrErrorSw().setError();
				// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
				// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
				// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
				ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
				// COB_CODE: MOVE '3400-PROCESS-DEFAULTS'
				//             TO EFAL-ERR-PARAGRAPH
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3400-PROCESS-DEFAULTS");
				// COB_CODE: MOVE 'INVALID TIMESTAMP REQUEST'
				//             TO EFAL-ERR-COMMENT
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID TIMESTAMP REQUEST");
				// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
				//                   HSDD-BUS-OBJ-NM ';'
				//                  'HSDD-DFL-DTA-TXT= '
				//                   HSDD-DFL-DTA-TXT     ';'
				//                  DELIMITED BY SIZE
				//                  INTO EFAL-OBJ-DATA-KEY
				concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
						new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-DFL-DTA-TXT= ",
								ws.getDclhalSecDpDflt().getDflDtaTxtFormatted(), ";" });
				ws.getWsEstoInfo().getEstoDetailBuffer()
						.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
				// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
				logWarningOrError();
				// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
				return;
			}
			// COB_CODE: IF HSDD-DFL-DTA-TXT = 'S3_TIMESTAMP'
			//           OR HSDD-DFL-DTA-TXT = 'S3-TIMESTAMP'
			//             MOVE WS-CURRENT-SE3-TIMESTAMP TO WS-HOLD-DTA-TXT
			//           ELSE
			//              END-IF
			//           END-IF
			if (Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3_TIMESTAMP")
					|| Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "S3-TIMESTAMP")) {
				// COB_CODE: MOVE WS-CURRENT-SE3-TIMESTAMP TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsSe3CurIsoDateTime().getWsCurrentSe3TimestampFormatted());
			} else if (Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT_TIMESTAMP")
					|| Conditions.eq(ws.getDclhalSecDpDflt().getDflDtaTxt(), "CURRENT-TIMESTAMP")) {
				// COB_CODE: IF HSDD-DFL-DTA-TXT = 'CURRENT_TIMESTAMP'
				//           OR HSDD-DFL-DTA-TXT = 'CURRENT-TIMESTAMP'
				//                TO WS-HOLD-DTA-TXT
				//           END-IF
				// COB_CODE: IF WS-ACT-CUR-ISO-DATE EQUAL SPACE
				//              END-IF
				//           END-IF
				if (Characters.EQ_SPACE.test(ws.getWsActCurIsoDateTime().getCurIsoDate())) {
					// COB_CODE: PERFORM 3440-GET-ACTUAL-DT-TS
					getActualDtTs();
					// COB_CODE: IF UBOC-HALT-AND-RETURN
					//              GO TO 3400-PROCESS-DEFAULTS-X
					//           END-IF
					if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
						// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
						return;
					}
				}
				// COB_CODE: MOVE WS-ACT-CURRENT-TIMESTAMP
				//             TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsActCurIsoDateTime().getWsActCurrentTimestamp());
			}
			// COB_CODE: IF WS-DFL-OFS-NBR-NI NOT LESS THAN ZERO
			//           AND HSDD-DFL-OFS-PER NOT EQUAL SPACES
			//              MOVE WS-CALC-TIMESTAMP TO WS-HOLD-DTA-TXT
			//           END-IF
			if (ws.getWsSpecificWorkAreas().getWsDflOfsNbrNi() >= 0
					&& !Characters.EQ_SPACE.test(ws.getDclhalSecDpDflt().getDflOfsPer().getDflOfsPer())) {
				// COB_CODE: MOVE HSDD-DFL-OFS-NBR TO DATE-ADJUST-NUMB
				ws.getDateStructure().setDateAdjustNumb(TruncAbs.toInt(ws.getDclhalSecDpDflt().getDflOfsNbr(), 5));
				// COB_CODE: PERFORM 3450-CALC-OFFSET-DT
				calcOffsetDt();
				// COB_CODE: IF UBOC-HALT-AND-RETURN
				//               GO TO 3400-PROCESS-DEFAULTS-X
				//           END-IF
				if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
					// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
					return;
				}
				// COB_CODE: MOVE WS-CALC-TIMESTAMP TO WS-HOLD-DTA-TXT
				ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt(ws.getWsSpecificWorkAreas().getWsHoldArea().getCalcTimestamp());
			}
			break;

		case HsddDflTypInd.LINK:// COB_CODE: MOVE HSDD-DFL-DTA-TXT TO WS-LINK-TO-PGM-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLinkToPgmNm(ws.getDclhalSecDpDflt().getDflDtaTxt());
			// COB_CODE: PERFORM 3700-LINK-TO-REQ-PGM
			linkToReqPgm();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 3400-PROCESS-DEFAULTS-X
			//           END-IF
			if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 3400-PROCESS-DEFAULTS-X
				return;
			}
			break;

		default:
			break;
		}
	}

	/**Original name: 3440-GET-ACTUAL-DT-TS_FIRST_SENTENCES<br>*/
	private void getActualDtTs() {
		// COB_CODE: EXEC SQL
		//              SELECT CURRENT_TIMESTAMP
		//              INTO :WS-ACT-CURRENT-TIMESTAMP
		//               FROM HAL_MSG_TRANSPRT_V
		//                WHERE UOW_NM  = :WS-UOW-NM
		//           END-EXEC.
		ws.getWsActCurIsoDateTime().setWsActCurrentTimestamp(
				halMsgTransprtVDao.selectByWsUowNm(ws.getWsGenericFields().getUowNm(), ws.getWsActCurIsoDateTime().getWsActCurrentTimestamp()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 3440-GET-ACTUAL-DT-TS-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE WS-PROGRAM-NAME         TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsSpecificWorkAreas().getWsProgramName());
			// COB_CODE: MOVE '3440-GET-ACTUAL-DT-TS'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3440-GET-ACTUAL-DT-TS");
			// COB_CODE: MOVE 'SELECT OF OF CURRENT_TIMESTAMP FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT OF OF CURRENT_TIMESTAMP FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3440-GET-ACTUAL-DT-TS-X
			return;
		}
	}

	/**Original name: 3450-CALC-OFFSET-DT_FIRST_SENTENCES<br>
	 * <pre> FROM DEV.DEV.S3PBDUMY.SOURCE(PHBOCRW)</pre>*/
	private void calcOffsetDt() {
		ConcatUtil concatUtil = null;
		Xpiodat xpiodat = null;
		// COB_CODE: MOVE SPACES TO DATE-STRUCTURE.
		ws.getDateStructure().initDateStructureSpaces();
		// COB_CODE: MOVE 'C'    TO DATE-FUNCTION.
		ws.getDateStructure().setDateFunctionFormatted("C");
		// COB_CODE: MOVE 'B'    TO DATE-OUT-FORMAT.
		ws.getDateStructure().setDateOutFormat("B");
		// COB_CODE: MOVE 'B'    TO DATE-INP-FORMAT.
		ws.getDateStructure().setDateInpFormat("B");
		// COB_CODE: EVALUATE TRUE
		//              WHEN HSDD-OFS-PER-HOURS
		//                 MOVE 'HD'   TO DATE-ADJ-FORMAT
		//              WHEN HSDD-OFS-PER-DAYS
		//                 MOVE 'DD'   TO DATE-ADJ-FORMAT
		//              WHEN HSDD-OFS-PER-WEEKS
		//                 MOVE 'WD'   TO DATE-ADJ-FORMAT
		//              WHEN HSDD-OFS-PER-MONTHS
		//                 MOVE 'MD'   TO DATE-ADJ-FORMAT
		//              WHEN HSDD-OFS-PER-YEARS
		//                 MOVE 'YD'   TO DATE-ADJ-FORMAT
		//              WHEN OTHER
		//                 GO TO 3450-CALC-OFFSET-DT-X
		//           END-EVALUATE.
		switch (ws.getDclhalSecDpDflt().getDflOfsPer().getDflOfsPer()) {

		case HsddDflOfsPer.HOURS:// COB_CODE: MOVE 'HD'   TO DATE-ADJ-FORMAT
			ws.getDateStructure().setDateAdjFormat("HD");
			break;

		case HsddDflOfsPer.DAYS:// COB_CODE: MOVE 'DD'   TO DATE-ADJ-FORMAT
			ws.getDateStructure().setDateAdjFormat("DD");
			break;

		case HsddDflOfsPer.WEEKS:// COB_CODE: MOVE 'WD'   TO DATE-ADJ-FORMAT
			ws.getDateStructure().setDateAdjFormat("WD");
			break;

		case HsddDflOfsPer.MONTHS:// COB_CODE: MOVE 'MD'   TO DATE-ADJ-FORMAT
			ws.getDateStructure().setDateAdjFormat("MD");
			break;

		case HsddDflOfsPer.YEARS:// COB_CODE: MOVE 'YD'   TO DATE-ADJ-FORMAT
			ws.getDateStructure().setDateAdjFormat("YD");
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '3450-CALC-OFFSET-DT'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3450-CALC-OFFSET-DT");
			// COB_CODE: MOVE 'INVALID OFFSET PERIOD REQUESTED.'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID OFFSET PERIOD REQUESTED.");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'HSDD-CMN-NM= '
			//                   HSDD-CMN-NM ';'
			//                  'HSDD-DFL-OFS-PER= '
			//                   HSDD-DFL-OFS-PER     ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
							ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-DFL-OFS-PER= ",
							ws.getDclhalSecDpDflt().getDflOfsPer().getDflOfsPerFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3450-CALC-OFFSET-DT-X
			return;
		}
		// COB_CODE: IF HSDD-DFL-OFS-NBR > 0
		//              MOVE '+' TO DATE-ADJUST-SIGN
		//           ELSE
		//              MOVE '-' TO DATE-ADJUST-SIGN
		//           END-IF.
		if (ws.getDclhalSecDpDflt().getDflOfsNbr() > 0) {
			// COB_CODE: MOVE '+' TO DATE-ADJUST-SIGN
			ws.getDateStructure().setDateAdjustSignFormatted("+");
		} else {
			// COB_CODE: MOVE '-' TO DATE-ADJUST-SIGN
			ws.getDateStructure().setDateAdjustSignFormatted("-");
		}
		// COB_CODE: MOVE HSDD-DFL-OFS-NBR    TO DATE-ADJUST-NUMB
		ws.getDateStructure().setDateAdjustNumb(TruncAbs.toInt(ws.getDclhalSecDpDflt().getDflOfsNbr(), 5));
		// COB_CODE: MOVE WS-HOLD-DTA-TXT     TO DATE-INPUT.
		ws.getDateStructure().setDateInput(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
		//*************************************************************         ***
		//                                                            *         ...
		//    XPXCDAT - COPYBOOK USED TO LINK TO XPIODAT, THE ON-LINE *    04/11/01
		//              COBOL DATE ROUTINE.                           *    XPXCDAT
		//                                                            *       LV004
		// IR REF    DATE   RELEASE            DESCRIPTION            *    00006
		// XXXXXX  DDMMMYY    X.X    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX *    00007
		// XXXXXX  16JAN03  DNF     CHANGED TO R70 STRUTURE. FEDERATED*    00008
		// 002607  06JUL00  IND9176 ADDED CODE IN ORDER TO MAKE IT A  *    00008
		//                          DYNAMIC CALL TO XPIODAT.          *    00009
		//*************************************************************    00010
		//    MOVE LENGTH OF DATE-STRUCTURE TO DATE-STRUCTURE-LENGTH.      00012
		//**  EXEC CICS LINK                                               00014
		//**       PROGRAM  ('XPIODAT')                                    00015
		//**       COMMAREA (DATE-STRUCTURE)                               00016
		//**       LENGTH   (DATE-STRUCTURE-LENGTH)                        00017
		//**       NOHANDLE                                                00018
		//**       END-EXEC.                                               00019
		//**                                                               00020
		//    MOVE 'XPIODAT ' TO WS-XPIODAT-MODULE.                        00021
		//    CALL WS-XPIODAT-MODULE USING                                 00022
		//         DFHEIBLK                                                00023
		//         DFHCOMMAREA                                             00024
		//         DATE-STRUCTURE.                                         00025
		// COB_CODE: CALL 'XPIODAT' USING DATE-STRUCTURE.
		xpiodat = Xpiodat.getInstance();
		xpiodat.run(ws.getDateStructure());
		//    MOVE DATE-OUTPUT TO WS-CALC-DT.
		// COB_CODE: MOVE DATE-OUTPUT TO WS-CALC-TIMESTAMP.
		ws.getWsSpecificWorkAreas().getWsHoldArea().setCalcTimestamp(ws.getDateStructure().getDateOutput());
	}

	/**Original name: 3460-VALIDATE-DATE-FORMAT_FIRST_SENTENCES<br>*/
	private void validateDateFormat() {
		Halrvdt1 halrvdt1 = null;
		StringParam wsValidDateHold = null;
		// COB_CODE: MOVE SPACE            TO WS-VALID-DATE-SW.
		ws.getWsBdoSwitches().getValidDateSw().setValidDateSw(Types.SPACE_CHAR);
		// COB_CODE: CALL 'HALRVDT1' USING WS-VALID-DATE-HOLD
		//                                 WS-VALID-DATE-SW.
		halrvdt1 = Halrvdt1.getInstance();
		wsValidDateHold = new StringParam(ws.getWsSpecificWorkAreas().getWsHoldArea().getValidDateHold(), WsHoldArea.Len.VALID_DATE_HOLD);
		halrvdt1.run(wsValidDateHold, ws.getWsBdoSwitches().getValidDateSw());
		ws.getWsSpecificWorkAreas().getWsHoldArea().setValidDateHold(wsValidDateHold.getString());
	}

	/**Original name: 3600-PROCESS-DATA-PRIVACY_FIRST_SENTENCES<br>*/
	private void processDataPrivacy() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT HSDD-VALID-COL-IND-VALUE
		//              GO TO 3600-PROCESS-DATA-PRIVACY-X
		//           ELSE
		//                TO WS-HOLD-CI
		//           END-IF.
		if (!ws.getDclhalSecDpDflt().getCmnAtrInd().isValidColIndValue()) {
			// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '3600-PROCESS-DATA-PRIVACY'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3600-PROCESS-DATA-PRIVACY");
			// COB_CODE: MOVE 'INVALID COLUMN INDICATOR VALUE.'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID COLUMN INDICATOR VALUE.");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'HSDD-CMN-NM= '
			//                   HSDD-CMN-NM ';'
			//                  'HSDD-CMN-ATR-IND= '
			//                   HSDD-CMN-ATR-IND     ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
							ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-CMN-ATR-IND= ",
							String.valueOf(ws.getDclhalSecDpDflt().getCmnAtrInd().getCmnAtrInd()), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3600-PROCESS-DATA-PRIVACY-X
			return;
		} else {
			// COB_CODE: MOVE HSDD-CMN-ATR-IND
			//             TO WS-HOLD-CI
			ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldCi(ws.getDclhalSecDpDflt().getCmnAtrInd().getCmnAtrInd());
		}
	}

	/**Original name: 3700-LINK-TO-REQ-PGM_FIRST_SENTENCES<br>*/
	private void linkToReqPgm() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE WS-ASSOC-BUS-OBJ-NM TO CIDP-TABLE-NAME.
		ws.getCidpTableInfo().setTableName(ws.getWsSpecificWorkAreas().getWsAssocBusObjNm());
		//    MOVE HSDD-CONTEXT TO UBOC-SECURITY-CONTEX.
		// COB_CODE: MOVE CW02C-CLIENT-TAB-ROW
		//               TO CIDP-TABLE-ROW.
		ws.getCidpTableInfo().setTableRow(ws.getCawlc002().getClientTabRowFormatted());
		// COB_CODE: MOVE WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsDataPrivacyInfoFormatted());
		// COB_CODE: MOVE LENGTH OF WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) Caws002Data.Len.WS_DATA_PRIVACY_INFO));
		// COB_CODE: EXEC CICS LINK
		//              PROGRAM  (WS-LINK-TO-PGM-NM)
		//              COMMAREA (UBOC-COMM-INFO)
		//              LENGTH   (LENGTH OF UBOC-COMM-INFO)
		//              RESP     (WS-RESPONSE-CODE)
		//              RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWS002", execContext).commarea(dfhcommarea.getUbocRecord().getCommInfo()).length(UbocCommInfoCaws002.Len.COMM_INFO)
				.link(ws.getWsSpecificWorkAreas().getWsHoldArea().getLinkToPgmNm());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 3700-LINK-TO-REQ-PGM-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE WS-LINK-TO-PGM-NM
			//             TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsSpecificWorkAreas().getWsHoldArea().getLinkToPgmNm());
			// COB_CODE: MOVE '3700-LINK-TO-REQ-PGM'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3700-LINK-TO-REQ-PGM");
			// COB_CODE: MOVE 'LINK TO REQUESTED MODULE FAILED'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("LINK TO REQUESTED MODULE FAILED");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'HSDD-CMN-NM= '
			//                   HSDD-CMN-NM ';'
			//                  'HSDD-DFL-DTA-TXT= '
			//                   HSDD-DFL-DTA-TXT     ';'
			//                DELIMITED BY SIZE
			//                INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "HSDD-BUS-OBJ-NM = ", ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ",
							ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";", "HSDD-DFL-DTA-TXT= ", ws.getDclhalSecDpDflt().getDflDtaTxtFormatted(),
							";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 3700-LINK-TO-REQ-PGM-X
			return;
		}
		// COB_CODE: IF NOT DPER-ROW-EXCLUDED
		//                TO CW02C-CLIENT-TAB-ROW
		//           END-IF.
		if (!dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().isRowExcluded()) {
			// COB_CODE: MOVE UBOC-APP-DATA-BUFFER(1:UBOC-APP-DATA-BUFFER-LENGTH)
			//             TO WS-DATA-PRIVACY-INFO
			ws.setWsDataPrivacyInfoFormatted(
					dfhcommarea.getUbocRecord().getAppDataBufferFormatted().substring((1) - 1, dfhcommarea.getUbocRecord().getAppDataBufferLength()));
			// COB_CODE: MOVE CIDP-TABLE-ROW
			//             TO CW02C-CLIENT-TAB-ROW
			ws.getCawlc002().setClientTabRowFormatted(ws.getCidpTableInfo().getTableRowFormatted());
		}
	}

	/**Original name: 5000-LOAD-CPYBK-VALUES_FIRST_SENTENCES<br>
	 * <pre> TRANS-PROCESS-DT IS NOT PART OF THE TABLE, BUT IS CREATED AS
	 *  PART OF ALL COMMUNICATION COPYBOOKS. IT DOES NOT HAVE A CI.</pre>*/
	private void loadCpybkValues() {
		ConcatUtil concatUtil = null;
		// COB_CODE:      EVALUATE HSDD-CMN-NM
		//           * TRANS-PROCESS-DT IS NOT PART OF THE TABLE, BUT IS CREATED AS
		//           * PART OF ALL COMMUNICATION COPYBOOKS. IT DOES NOT HAVE A CI.
		//                  WHEN 'CW02C-TRANS-PROCESS-DT'
		//                     END-IF
		//           * BEGINNING OF KEY FIELDS
		//           * FOR KEY FIELDS, THE COLUMN INDICATOR WILL ONLY BE OVERLAYED IF
		//           * THE NEW VALUE IS NOT SPACES.
		//                  WHEN 'CW02C-CLIENT-ID'
		//                      END-IF
		//                  WHEN 'CW02C-HISTORY-VLD-NBR'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-EFF-DT'
		//                      END-IF
		//           * END OF KEY FIELDS
		//           * BEGINNING OF DATA FIELDS
		//           * FOR DATA FIELDS, THE COLUMN INDICATOR WILL ALWAYS BE OVERLAYED
		//           * SINCE SPACES IS A VALID DEFAULT VALUE FOR DATA FIELDS.
		//                  WHEN 'CW02C-CICL-PRI-SUB-CD'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-FST-NM'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-LST-NM'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-MDL-NM'
		//                      END-IF
		//                  WHEN 'CW02C-NM-PFX'
		//                      END-IF
		//                  WHEN 'CW02C-NM-SFX'
		//                      END-IF
		//                  WHEN 'CW02C-PRIMARY-PRO-DSN-CD'
		//                      END-IF
		//                  WHEN 'CW02C-LEG-ENT-CD'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-SDX-CD'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-OGN-INCEPT-DT'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-ADD-NM-IND'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-DOB-DT'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-BIR-ST-CD'
		//                      END-IF
		//                  WHEN 'CW02C-GENDER-CD'
		//                      END-IF
		//                  WHEN 'CW02C-PRI-LGG-CD'
		//                      END-IF
		//                  WHEN 'CW02C-USER-ID'
		//                      END-IF
		//                  WHEN 'CW02C-STATUS-CD'
		//                      END-IF
		//                  WHEN 'CW02C-TERMINAL-ID'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-EXP-DT'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-EFF-ACY-TS'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-EXP-ACY-TS'
		//                      END-IF
		//                  WHEN 'CW02C-STATUTORY-TLE-CD'
		//                      END-IF
		//                  WHEN 'CW02C-CICL-LNG-NM'
		//                      END-IF
		//           * IF THE COLUMN NAME IS 'ENTIRE-ROW' IT IS USED AS AN INDICATION
		//           * THAT ALL FIELDS SHOULD HAVE THE DATA PRIVACY COLUMN INDICATOR
		//           * APPLIED - EG. MAKE ALL FIELDS READ ONLY
		//                  WHEN 'CW02C-ENTIRE-ROW'
		//                      END-IF
		//           * END OF DATA FIELDS
		//                   WHEN OTHER
		//                      GO TO 5000-LOAD-CPYBK-VALUES-X
		//                END-EVALUATE.
		switch (ws.getDclhalSecDpDflt().getCmnNm()) {

		case "CW02C-TRANS-PROCESS-DT":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-TRANS-PROCESS-DT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-TRANS-PROCESS-DT
				ws.getCawlc002().setTransProcessDt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// BEGINNING OF KEY FIELDS
			// FOR KEY FIELDS, THE COLUMN INDICATOR WILL ONLY BE OVERLAYED IF
			// THE NEW VALUE IS NOT SPACES.
			break;

		case "CW02C-CLIENT-ID":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CLIENT-ID
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CLIENT-ID
				ws.getCawlc002().getClientTabKey().setClientId(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CLIENT-ID-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CLIENT-ID-CI
				ws.getCawlc002().getClientTabKey().setClientIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-HISTORY-VLD-NBR":// COB_CODE: IF WS-HOLD-AMT NOT EQUAL WS-HOLD-INITIAL-AMT
			//                                TO CW02C-HISTORY-VLD-NBR-SIGN
			//           END-IF
			if (ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldAmt().compareTo(ws.getWsSpecificWorkAreas().getWsHoldInitialAmt()) != 0) {
				// COB_CODE: MOVE WS-HOLD-AMT TO CW02C-HISTORY-VLD-NBR
				ws.getCawlc002().getClientTabKey().setCw02cHistoryVldNbr(TruncAbs.toInt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldAmt(), 5));
				// COB_CODE: MOVE WS-HOLD-SIGN
				//                             TO CW02C-HISTORY-VLD-NBR-SIGN
				ws.getCawlc002().getClientTabKey().setHistoryVldNbrSign(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldSign());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-HISTORY-VLD-NBR-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-HISTORY-VLD-NBR-CI
				ws.getCawlc002().getClientTabKey().setHistoryVldNbrCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-EFF-DT":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EFF-DT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EFF-DT
				ws.getCawlc002().getClientTabKey().setCiclEffDt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-EFF-DT-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EFF-DT-CI
				ws.getCawlc002().getClientTabKey().setCiclEffDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			// END OF KEY FIELDS
			// BEGINNING OF DATA FIELDS
			// FOR DATA FIELDS, THE COLUMN INDICATOR WILL ALWAYS BE OVERLAYED
			// SINCE SPACES IS A VALID DEFAULT VALUE FOR DATA FIELDS.
			break;

		case "CW02C-CICL-PRI-SUB-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-PRI-SUB-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-PRI-SUB-CD
				ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-PRI-SUB-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-PRI-SUB-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-FST-NM":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-FST-NM
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-FST-NM
				ws.getCawlc002().getClientTabData().setCw02cCiclFstNm(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-FST-NM-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-FST-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclFstNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-LST-NM":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-LST-NM
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-LST-NM
				ws.getCawlc002().getClientTabData().setCw02cCiclLstNm(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-LST-NM-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-LST-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclLstNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-MDL-NM":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-MDL-NM
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-MDL-NM
				ws.getCawlc002().getClientTabData().setCw02cCiclMdlNm(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-MDL-NM-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-MDL-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclMdlNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-NM-PFX":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-NM-PFX
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-NM-PFX
				ws.getCawlc002().getClientTabData().setCw02cNmPfx(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-NM-PFX-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-NM-PFX-CI
				ws.getCawlc002().getClientTabData().setCw02cNmPfxCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-NM-SFX":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-NM-SFX
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-NM-SFX
				ws.getCawlc002().getClientTabData().setCw02cNmSfx(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-NM-SFX-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-NM-SFX-CI
				ws.getCawlc002().getClientTabData().setCw02cNmSfxCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-PRIMARY-PRO-DSN-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-PRIMARY-PRO-DSN-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-PRIMARY-PRO-DSN-CD
				ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-PRIMARY-PRO-DSN-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-PRIMARY-PRO-DSN-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-LEG-ENT-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-LEG-ENT-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-LEG-ENT-CD
				ws.getCawlc002().getClientTabData().setCw02cLegEntCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-LEG-ENT-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-LEG-ENT-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cLegEntCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-SDX-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-SDX-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-SDX-CD
				ws.getCawlc002().getClientTabData().setCw02cCiclSdxCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-SDX-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-SDX-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclSdxCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-OGN-INCEPT-DT":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-OGN-INCEPT-DT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-OGN-INCEPT-DT
				ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-OGN-INCEPT-DT-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-OGN-INCEPT-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-ADD-NM-IND":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-ADD-NM-IND
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-ADD-NM-IND
				ws.getCawlc002().getClientTabData()
						.setCw02cCiclAddNmIndFormatted(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxtFormatted());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-ADD-NM-IND-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-ADD-NM-IND-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-DOB-DT":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-DOB-DT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-DOB-DT
				ws.getCawlc002().getClientTabData().setCw02cCiclDobDt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-DOB-DT-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-DOB-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclDobDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-BIR-ST-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-BIR-ST-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-BIR-ST-CD
				ws.getCawlc002().getClientTabData().setCw02cCiclBirStCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-BIR-ST-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-BIR-ST-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclBirStCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-GENDER-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-GENDER-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-GENDER-CD
				ws.getCawlc002().getClientTabData().setCw02cGenderCdFormatted(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxtFormatted());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-GENDER-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-GENDER-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cGenderCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-PRI-LGG-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-PRI-LGG-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-PRI-LGG-CD
				ws.getCawlc002().getClientTabData().setCw02cPriLggCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-PRI-LGG-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-PRI-LGG-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cPriLggCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-USER-ID":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-USER-ID
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-USER-ID
				ws.getCawlc002().getClientTabData().setCw02cUserId(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-USER-ID-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-USER-ID-CI
				ws.getCawlc002().getClientTabData().setCw02cUserIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-STATUS-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-STATUS-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-STATUS-CD
				ws.getCawlc002().getClientTabData().setCw02cStatusCdFormatted(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxtFormatted());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-STATUS-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-STATUS-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cStatusCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-TERMINAL-ID":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-TERMINAL-ID
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-TERMINAL-ID
				ws.getCawlc002().getClientTabData().setCw02cTerminalId(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-TERMINAL-ID-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-TERMINAL-ID-CI
				ws.getCawlc002().getClientTabData().setCw02cTerminalIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-EXP-DT":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EXP-DT
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EXP-DT
				ws.getCawlc002().getClientTabData().setCw02cCiclExpDt(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-EXP-DT-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EXP-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclExpDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-EFF-ACY-TS":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EFF-ACY-TS
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EFF-ACY-TS
				ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTs(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-EFF-ACY-TS-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EFF-ACY-TS-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTsCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-EXP-ACY-TS":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EXP-ACY-TS
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-EXP-ACY-TS
				ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTs(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-EXP-ACY-TS-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EXP-ACY-TS-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTsCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-STATUTORY-TLE-CD":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-STATUTORY-TLE-CD
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-STATUTORY-TLE-CD
				ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCd(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-STATUTORY-TLE-CD-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-STATUTORY-TLE-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			break;

		case "CW02C-CICL-LNG-NM":// COB_CODE: IF WS-HOLD-DTA-TXT NOT EQUAL SPACES
			//              MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-LNG-NM
			//           END-IF
			if (!Characters.EQ_SPACE.test(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt())) {
				// COB_CODE: MOVE WS-HOLD-DTA-TXT TO CW02C-CICL-LNG-NM
				ws.getCawlc002().getClientTabData().setCw02cCiclLngNm(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldDtaTxt());
			}
			// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-LNG-NM-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-LNG-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclLngNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			// IF THE COLUMN NAME IS 'ENTIRE-ROW' IT IS USED AS AN INDICATION
			// THAT ALL FIELDS SHOULD HAVE THE DATA PRIVACY COLUMN INDICATOR
			// APPLIED - EG. MAKE ALL FIELDS READ ONLY
			break;

		case "CW02C-ENTIRE-ROW":// COB_CODE: IF NOT WS-IGNORE-COL-IND
			//              MOVE WS-HOLD-CI TO CW02C-CICL-LNG-NM-CI
			//           END-IF
			if (!ws.getWsSpecificWorkAreas().getWsHoldArea().isIgnoreColInd()) {
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CLIENT-ID-CI
				ws.getCawlc002().getClientTabKey().setClientIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-HISTORY-VLD-NBR-CI
				ws.getCawlc002().getClientTabKey().setHistoryVldNbrCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EFF-DT-CI
				ws.getCawlc002().getClientTabKey().setCiclEffDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-PRI-SUB-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-FST-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclFstNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-LST-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclLstNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-MDL-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclMdlNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-NM-PFX-CI
				ws.getCawlc002().getClientTabData().setCw02cNmPfxCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-NM-SFX-CI
				ws.getCawlc002().getClientTabData().setCw02cNmSfxCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-PRIMARY-PRO-DSN-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-LEG-ENT-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cLegEntCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-SDX-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclSdxCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-OGN-INCEPT-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-ADD-NM-IND-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-DOB-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclDobDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-BIR-ST-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclBirStCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-GENDER-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cGenderCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-PRI-LGG-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cPriLggCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-USER-ID-CI
				ws.getCawlc002().getClientTabData().setCw02cUserIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-STATUS-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cStatusCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-TERMINAL-ID-CI
				ws.getCawlc002().getClientTabData().setCw02cTerminalIdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EXP-DT-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclExpDtCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EFF-ACY-TS-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTsCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-EXP-ACY-TS-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTsCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-STATUTORY-TLE-CD-CI
				ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCdCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
				// COB_CODE: MOVE WS-HOLD-CI TO CW02C-CICL-LNG-NM-CI
				ws.getCawlc002().getClientTabData().setCw02cCiclLngNmCi(ws.getWsSpecificWorkAreas().getWsHoldArea().getHoldCi());
			}
			// END OF DATA FIELDS
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-DATA-PRIVACY-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDataPrivacyFailed();
			// COB_CODE: SET BUSP-INVALID-SUPPLIED-VALUE TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setBuspInvalidSuppliedValue();
			// COB_CODE: MOVE '5000-LOAD-CPYBK-VALUES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5000-LOAD-CPYBK-VALUES");
			// COB_CODE: MOVE 'INVALID COLUMN NAME UPDATE REQUEST.'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("INVALID COLUMN NAME UPDATE REQUEST.");
			// COB_CODE: STRING 'HSDD-BUS-OBJ-NM = '
			//                   HSDD-BUS-OBJ-NM ';'
			//                  'HSDD-CMN-NM= '
			//                   HSDD-CMN-NM ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, new String[] { "HSDD-BUS-OBJ-NM = ",
					ws.getDclhalSecDpDflt().getBusObjNmFormatted(), ";", "HSDD-CMN-NM= ", ws.getDclhalSecDpDflt().getCmnNmFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5000-LOAD-CPYBK-VALUES-X
			return;
		}
		// COB_CODE: SET DPER-ROW-ALTERED TO TRUE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocDataPrivRetCode().setRowAltered();
	}

	/**Original name: 7000-INBOUND-DP_FIRST_SENTENCES<br>*/
	private void inboundDp() {
		// COB_CODE: PERFORM 7050-CHECK-FOR-INBOUND-REQ.
		checkForInboundReq();
		// COB_CODE: IF WS-INBOUND-REQ
		//              CONTINUE
		//            ELSE
		//              GO TO 7000-INBOUND-DP-X
		//           END-IF.
		if (ws.getWsSpecificWorkAreas().getWsInboundReqSw().isInboundReq()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 7000-INBOUND-DP-X
			return;
		}
		//* MUST VERIFY DATA PORTION ONLY - KEY SHOULD NOT BE CHANGED
		// COB_CODE: MOVE CW02C-CICL-PRI-SUB-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclPriSubCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-PRI-SUB-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-PRI-SUB-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclPriSubCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-PRI-SUB-CD NOT > SPACES
			//           *           MOVE CW02N-CICL-PRI-SUB-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-PRI-SUB-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-PRI-SUB-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-PRI-SUB-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-PRI-SUB-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-PRI-SUB-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-FST-NM-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclFstNmCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-FST-NM-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-FST-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclFstNmCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclFstNm(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-FST-NM NOT > SPACES
			//           *           MOVE CW02N-CICL-FST-NM TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-FST-NM TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-FST-NM' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-FST-NM");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-FST-NM-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-FST-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclFstNmCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-LST-NM-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclLstNmCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-LST-NM-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-LST-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclLstNmCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclLstNm(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-LST-NM NOT > SPACES
			//           *           MOVE CW02N-CICL-LST-NM TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-LST-NM TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-LST-NM' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-LST-NM");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-LST-NM-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-LST-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclLstNmCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-MDL-NM-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclMdlNmCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-MDL-NM-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-MDL-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclMdlNmCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclMdlNm(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-MDL-NM NOT > SPACES
			//           *           MOVE CW02N-CICL-MDL-NM TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-MDL-NM TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-MDL-NM' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-MDL-NM");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-MDL-NM-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-MDL-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclMdlNmCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-NM-PFX-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cNmPfxCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-NM-PFX-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-NM-PFX-CI
			ws.getCawlc002().getClientTabData().setCw02cNmPfxCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cNmPfx(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-NM-PFX NOT > SPACES
			//           *           MOVE CW02N-NM-PFX TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-NM-PFX TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-NM-PFX' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-NM-PFX");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-NM-PFX-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-NM-PFX-CI
			ws.getCawlc002().getClientTabData().setCw02cNmPfxCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-NM-SFX-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cNmSfxCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-NM-SFX-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-NM-SFX-CI
			ws.getCawlc002().getClientTabData().setCw02cNmSfxCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cNmSfx(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-NM-SFX NOT > SPACES
			//           *           MOVE CW02N-NM-SFX TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-NM-SFX TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-NM-SFX' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-NM-SFX");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-NM-SFX-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-NM-SFX-CI
			ws.getCawlc002().getClientTabData().setCw02cNmSfxCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-PRIMARY-PRO-DSN-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cPrimaryProDsnCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-PRIMARY-PRO-DSN-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-PRIMARY-PRO-DSN-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cPrimaryProDsnCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-PRIMARY-PRO-DSN-CD NOT > SPACES
			//           *           MOVE CW02N-PRIMARY-PRO-DSN-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-PRIMARY-PRO-DSN-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-PRIMARY-PRO-DSN-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-PRIMARY-PRO-DSN-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-PRIMARY-PRO-DSN-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-PRIMARY-PRO-DSN-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-LEG-ENT-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cLegEntCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-LEG-ENT-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-LEG-ENT-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cLegEntCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cLegEntCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-LEG-ENT-CD NOT > SPACES
			//           *           MOVE CW02N-LEG-ENT-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-LEG-ENT-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-LEG-ENT-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-LEG-ENT-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-LEG-ENT-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-LEG-ENT-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cLegEntCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-SDX-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclSdxCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-SDX-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-SDX-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclSdxCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclSdxCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-SDX-CD NOT > SPACES
			//           *           MOVE CW02N-CICL-SDX-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-SDX-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-SDX-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-SDX-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-SDX-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-SDX-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclSdxCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-OGN-INCEPT-DT-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclOgnInceptDtCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-OGN-INCEPT-DT-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-OGN-INCEPT-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclOgnInceptDt(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-OGN-INCEPT-DT NOT > SPACES
			//           *           MOVE CW02N-CICL-OGN-INCEPT-DT TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-OGN-INCEPT-DT TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-OGN-INCEPT-DT' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-OGN-INCEPT-DT");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-OGN-INCEPT-DT-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-OGN-INCEPT-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-ADD-NM-IND-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclAddNmIndCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-ADD-NM-IND-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-ADD-NM-IND-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclAddNmInd(), Types.SPACE_CHAR)) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-ADD-NM-IND NOT > SPACES
			//           *           MOVE CW02N-CICL-ADD-NM-IND TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-ADD-NM-IND TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-ADD-NM-IND' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-ADD-NM-IND");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-ADD-NM-IND-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-ADD-NM-IND-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-DOB-DT-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclDobDtCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-DOB-DT-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-DOB-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclDobDtCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclDobDt(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-DOB-DT NOT > SPACES
			//           *           MOVE CW02N-CICL-DOB-DT TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-DOB-DT TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-DOB-DT' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-DOB-DT");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-DOB-DT-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-DOB-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclDobDtCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-BIR-ST-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclBirStCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-BIR-ST-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-BIR-ST-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclBirStCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclBirStCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-BIR-ST-CD NOT > SPACES
			//           *           MOVE CW02N-CICL-BIR-ST-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-BIR-ST-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-BIR-ST-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-BIR-ST-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-BIR-ST-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-BIR-ST-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclBirStCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-GENDER-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cGenderCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-GENDER-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-GENDER-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cGenderCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cGenderCd(), Types.SPACE_CHAR)) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-GENDER-CD NOT > SPACES
			//           *           MOVE CW02N-GENDER-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-GENDER-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-GENDER-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-GENDER-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-GENDER-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-GENDER-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cGenderCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-PRI-LGG-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cPriLggCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-PRI-LGG-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-PRI-LGG-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cPriLggCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cPriLggCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-PRI-LGG-CD NOT > SPACES
			//           *           MOVE CW02N-PRI-LGG-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-PRI-LGG-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-PRI-LGG-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-PRI-LGG-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-PRI-LGG-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-PRI-LGG-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cPriLggCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-USER-ID-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cUserIdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-USER-ID-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-USER-ID-CI
			ws.getCawlc002().getClientTabData().setCw02cUserIdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cUserId(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-USER-ID NOT > SPACES
			//           *           MOVE CW02N-USER-ID TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-USER-ID TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-USER-ID' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-USER-ID");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-USER-ID-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-USER-ID-CI
			ws.getCawlc002().getClientTabData().setCw02cUserIdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-STATUS-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cStatusCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-STATUS-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-STATUS-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cStatusCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cStatusCd(), Types.SPACE_CHAR)) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-STATUS-CD NOT > SPACES
			//           *           MOVE CW02N-STATUS-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-STATUS-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-STATUS-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-STATUS-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-STATUS-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-STATUS-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cStatusCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-TERMINAL-ID-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cTerminalIdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-TERMINAL-ID-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-TERMINAL-ID-CI
			ws.getCawlc002().getClientTabData().setCw02cTerminalIdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cTerminalId(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-TERMINAL-ID NOT > SPACES
			//           *           MOVE CW02N-TERMINAL-ID TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-TERMINAL-ID TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-TERMINAL-ID' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-TERMINAL-ID");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-TERMINAL-ID-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-TERMINAL-ID-CI
			ws.getCawlc002().getClientTabData().setCw02cTerminalIdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-EXP-DT-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclExpDtCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-EXP-DT-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-EXP-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclExpDtCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclExpDt(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-EXP-DT NOT > SPACES
			//           *           MOVE CW02N-CICL-EXP-DT TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-EXP-DT TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-EXP-DT' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-EXP-DT");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-EXP-DT-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-EXP-DT-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclExpDtCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-EFF-ACY-TS-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclEffAcyTsCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-EFF-ACY-TS-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-EFF-ACY-TS-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTsCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclEffAcyTs(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-EFF-ACY-TS NOT > SPACES
			//           *           MOVE CW02N-CICL-EFF-ACY-TS TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-EFF-ACY-TS TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-EFF-ACY-TS' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-EFF-ACY-TS");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-EFF-ACY-TS-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-EFF-ACY-TS-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTsCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-EXP-ACY-TS-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclExpAcyTsCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-EXP-ACY-TS-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-EXP-ACY-TS-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTsCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclExpAcyTs(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-EXP-ACY-TS NOT > SPACES
			//           *           MOVE CW02N-CICL-EXP-ACY-TS TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-EXP-ACY-TS TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-EXP-ACY-TS' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-EXP-ACY-TS");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-EXP-ACY-TS-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-EXP-ACY-TS-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTsCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-STATUTORY-TLE-CD-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cStatutoryTleCdCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-STATUTORY-TLE-CD-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-STATUTORY-TLE-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCdCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cStatutoryTleCd(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-STATUTORY-TLE-CD NOT > SPACES
			//           *           MOVE CW02N-STATUTORY-TLE-CD TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-STATUTORY-TLE-CD TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-STATUTORY-TLE-CD' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-STATUTORY-TLE-CD");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-STATUTORY-TLE-CD-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-STATUTORY-TLE-CD-CI
			ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCdCi(Types.SPACE_CHAR);
		}
		// COB_CODE: MOVE CW02C-CICL-LNG-NM-CI
		//             TO COM-OUTGOING-COL-IND-VAL.
		ws.getHallcom().getOutgoingColIndVal().setOutgoingColIndVal(ws.getCawlc002().getClientTabData().getCw02cCiclLngNmCi());
		// COB_CODE: IF COM-OUTGOING-RETAINABLE-CI-VAL
		//                TO CW02C-CICL-LNG-NM-CI
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getHallcom().getOutgoingColIndVal().isComOutgoingRetainableCiVal()) {
			// COB_CODE: MOVE COM-RETAIN-VAL-COL-IND
			//             TO CW02C-CICL-LNG-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclLngNmCi(ws.getHallcom().getRetainValColInd());
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()
				&& !Conditions.gt(ws.getCawlc002().getClientTabData().getCw02cCiclLngNm(), "")) {
			// COB_CODE:         IF COM-IS-REQUIRED-COL-IND
			//                   AND CW02C-CICL-LNG-NM NOT > SPACES
			//           *           MOVE CW02N-CICL-LNG-NM TO WS-LIT-FIELD-NM
			//                       PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			//                   ELSE
			//                       END-IF
			//                   END-IF
			//           MOVE CW02N-CICL-LNG-NM TO WS-LIT-FIELD-NM
			// COB_CODE: MOVE 'CW02C-CICL-LNG-NM' TO WS-LIT-FIELD-NM
			ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("CW02C-CICL-LNG-NM");
			// COB_CODE: PERFORM 7060-WRITE-NON-LOGGABLE-ERR
			writeNonLoggableErr();
		} else if (ws.getHallcom().getOutgoingColIndVal().isComIsRequiredColInd()) {
			// COB_CODE: IF COM-IS-REQUIRED-COL-IND
			//              MOVE SPACES TO CW02C-CICL-LNG-NM-CI
			//           END-IF
			// COB_CODE: MOVE SPACES TO CW02C-CICL-LNG-NM-CI
			ws.getCawlc002().getClientTabData().setCw02cCiclLngNmCi(Types.SPACE_CHAR);
		}
	}

	/**Original name: 7050-CHECK-FOR-INBOUND-REQ_FIRST_SENTENCES<br>*/
	private void checkForInboundReq() {
		// COB_CODE: IF SIMPLE-EDIT-AND-PRIM-KEYS OF UBOC-COMM-INFO
		//              SET WS-INBOUND-REQ TO TRUE
		//           ELSE
		//              SET WS-NOT-INBOUND-REQ TO TRUE
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocPassThruAction().isSimpleEditAndPrimKeys()) {
			// COB_CODE: SET WS-INBOUND-REQ TO TRUE
			ws.getWsSpecificWorkAreas().getWsInboundReqSw().setInboundReq();
		} else {
			// COB_CODE: SET WS-NOT-INBOUND-REQ TO TRUE
			ws.getWsSpecificWorkAreas().getWsInboundReqSw().setNotInboundReq();
		}
	}

	/**Original name: 7060-WRITE-NON-LOGGABLE-ERR_FIRST_SENTENCES<br>*/
	private void writeNonLoggableErr() {
		// COB_CODE: SET WS-NON-LOGGABLE-BUS-ERR TO TRUE.
		ws.getWsNonLoggableWarnOrErrSw().setBusErr();
		// COB_CODE: MOVE 'CLIENT_TAB_V'
		//             TO NLBE-FAILED-TABLE-OR-FILE.
		ws.getNlbeCommon().setFailedTableOrFile("CLIENT_TAB_V");
		// COB_CODE: MOVE WS-LIT-FIELD-NM
		//             TO NLBE-FAILED-COLUMN-OR-FIELD.
		ws.getNlbeCommon().setFailedColumnOrField(ws.getWsSpecificWorkAreas().getWsHoldArea().getLitFieldNm());
		// COB_CODE: MOVE 'GEN_REQFLD' TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode("GEN_REQFLD");
		// COB_CODE: MOVE SPACES TO WS-NONLOG-PLACEHOLDER-VALUES.
		ws.getWsNonlogPlaceholderValues().initWsNonlogPlaceholderValuesSpaces();
		// COB_CODE: MOVE WS-LIT-FIELD-NM
		//             TO WS-NONLOG-ERR-COL1-NAME.
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Name(ws.getWsSpecificWorkAreas().getWsHoldArea().getLitFieldNm());
		// COB_CODE: PERFORM 9050-PROC-NON-LOG-WRN-OR-ERR.
		procNonLogWrnOrErr();
	}

	/**Original name: 8000-TERMINATE_FIRST_SENTENCES<br>*/
	private void terminate() {
		// COB_CODE: MOVE WS-ASSOC-BUS-OBJ-NM TO CIDP-TABLE-NAME.
		ws.getCidpTableInfo().setTableName(ws.getWsSpecificWorkAreas().getWsAssocBusObjNm());
		// COB_CODE: MOVE CW02C-CLIENT-TAB-ROW
		//               TO CIDP-TABLE-ROW.
		ws.getCidpTableInfo().setTableRow(ws.getCawlc002().getClientTabRowFormatted());
		// COB_CODE: MOVE LENGTH OF CW02C-CLIENT-TAB-ROW
		//             TO CIDP-TABLE-ROW-LENGTH.
		ws.getCidpTableInfo().setTableRowLength(((short) Cawlc002.Len.CLIENT_TAB_ROW));
		// COB_CODE: MOVE WS-DATA-PRIVACY-INFO TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsDataPrivacyInfoFormatted());
		// COB_CODE: GO TO 0000-RETURN.
		return1();
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning()
				&& dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsSpecificWorkAreas().getWsProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsSpecificWorkAreas().getWsProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWS002", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.getUbocRecord().setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.getUbocRecord().setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: 9050-PROC-NON-LOG-WRN-OR-ERR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNING OR ERROR.                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void procNonLogWrnOrErr() {
		// COB_CODE: IF WS-NON-LOGGABLE-BUS-ERR
		//               PERFORM 9070-WRITE-NLBE-UMT-REC
		//           ELSE
		//               PERFORM 9080-WRITE-WARN-UMT-REC
		//           END-IF.
		if (ws.getWsNonLoggableWarnOrErrSw().isBusErr()) {
			// COB_CODE: PERFORM 9070-WRITE-NLBE-UMT-REC
			writeNlbeUmtRec();
		} else {
			// COB_CODE: PERFORM 9080-WRITE-WARN-UMT-REC
			writeWarnUmtRec();
		}
	}

	/**Original name: 9070-WRITE-NLBE-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE BUSINESS ERRORS                            *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeNlbeUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID              TO NLBE-ID.
		ws.getNlbeCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS  TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
		// COB_CODE: ADD 1 TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeq(Trunc.toShort(1 + ws.getNlbeCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'NONLOG_BUSLOGIC_ERRORS' TO NLBE-FAILURE-TYPE.
		ws.getNlbeCommon().setFailureType("NONLOG_BUSLOGIC_ERRORS");
		// COB_CODE: MOVE WS-PROGRAM-NAME          TO NLBE-FAILED-MODULE.
		ws.getNlbeCommon().setFailedModule(ws.getWsSpecificWorkAreas().getWsProgramName());
		// COB_CODE: MOVE NLBE-ERROR-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getNlbeCommon().getErrorCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9070-WRITE-NLBE-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO NLBE-ERROR-CODE.
		ws.getNlbeCommon().setErrorCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO NLBE-NONLOGGABLE-BP-ERR-TEXT.
		ws.getNlbeCommon().setNonloggableBpErrText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-NL-BL-ERRS-STORE)
		//             FROM   (WS-NLBE-UMT-AREA)
		//             LENGTH (LENGTH OF WS-NLBE-UMT-AREA)
		//             RIDFLD (NLBE-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespNlBlErrsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsNlbeUmtAreaBytes());
			iRowData.setKey(ws.getNlbeCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//                        GO TO 9070-WRITE-NLBE-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9070-WRITE-NLBE-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9070-WRITE-NLBE-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW NLBE STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW NLBE STORE FAILED");
			// COB_CODE: STRING 'NLBE-ID= '       NLBE-ID        ';'
			//                  'NLBE-REC-SEQ= '  NLBE-REC-SEQ   ';'
			//                  'NLBE-ERROR-CODE= '
			//                                    NLBE-ERROR-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "NLBE-ID= ", ws.getNlbeCommon().getIdFormatted(), ";", "NLBE-REC-SEQ= ", ws.getNlbeCommon().getRecSeqAsString(),
							";", "NLBE-ERROR-CODE= ", ws.getNlbeCommon().getErrorCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9070-WRITE-NLBE-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-NONLOG-BL-ERRS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrNonlogBlErrs(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrs(), 9));
	}

	/**Original name: 9080-WRITE-WARN-UMT-REC_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  REPORT NON LOGGABLE WARNINGS                                   *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void writeWarnUmtRec() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE UBOC-MSG-ID        TO UWRN-ID.
		ws.getUwrnCommon().setId(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE UBOC-NBR-WARNINGS  TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: ADD 1 TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeq(Trunc.toShort(1 + ws.getUwrnCommon().getRecSeq(), 3));
		// COB_CODE: MOVE 'WARNINGS'         TO UWRN-FAILURE-TYPE.
		ws.getUwrnCommon().setFailureType("WARNINGS");
		// COB_CODE: MOVE WS-PROGRAM-NAME    TO UWRN-FAILED-MODULE.
		ws.getUwrnCommon().setFailedModule(ws.getWsSpecificWorkAreas().getWsProgramName());
		// COB_CODE: MOVE UWRN-WARNING-CODE TO HNLB-ERR-WNG-CD.
		ws.getDclhalNlbeWngTxtV().setHnlbErrWngCd(ws.getUwrnCommon().getWarningCode());
		// COB_CODE: PERFORM 9090-CONVERT-ERROR-TO-TEXT.
		convertErrorToText();
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//               GO TO 9080-WRITE-WARN-UMT-REC-X
		//           END-IF.
		if (dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: MOVE HNLB-ERR-WNG-CD    TO UWRN-WARNING-CODE.
		ws.getUwrnCommon().setWarningCode(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
		//    MOVE HNLB-ERR-WNG-TXT   TO UWRN-WARNING-TEXT.
		// COB_CODE: MOVE WS-ERR-WNG-TXT-D   TO UWRN-WARNING-TEXT.
		ws.getUwrnCommon().setWarningText(ws.getWsNonlogPlaceholderValues().getErrWngTxtD());
		// COB_CODE: EXEC CICS
		//             WRITE FILE(UBOC-UOW-RESP-WARNINGS-STORE)
		//             FROM   (WS-WARNING-UMT-AREA)
		//             LENGTH (LENGTH OF WS-WARNING-UMT-AREA)
		//             RIDFLD (UWRN-KEY)
		//             RESP   (WS-RESPONSE-CODE)
		//             RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createTO(ws.getWsWarningUmtAreaBytes());
			iRowData.setKey(ws.getUwrnCommon().getKeyBytes());
			iRowDAO.insert(iRowData);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                       CONTINUE
		//                    WHEN OTHER
		//                       GO TO 9080-WRITE-WARN-UMT-REC-X
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default:// COB_CODE: SET WS-LOG-ERROR                        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED OF WS-ESTO-INFO    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-UMT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteUmt();
			// COB_CODE: MOVE '9080-WRITE-WARN-UMT-REC'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9080-WRITE-WARN-UMT-REC");
			// COB_CODE: MOVE 'WRITE TO UOW WARNING STORE FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE TO UOW WARNING STORE FAILED");
			// COB_CODE: STRING 'UWRN-ID= '       UWRN-ID        ';'
			//                  'UWRN-REC-SEQ= '  UWRN-REC-SEQ   ';'
			//                  'UWRN-WARNING-CODE= '
			//                                    UWRN-WARNING-CODE  ';'
			//                  DELIMITED BY SIZE
			//                      INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY,
					new String[] { "UWRN-ID= ", ws.getUwrnCommon().getIdFormatted(), ";", "UWRN-REC-SEQ= ", ws.getUwrnCommon().getRecSeqAsString(),
							";", "UWRN-WARNING-CODE= ", ws.getUwrnCommon().getWarningCodeFormatted(), ";" });
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9080-WRITE-WARN-UMT-REC-X
			return;
		}
		// COB_CODE: ADD 1 TO UBOC-NBR-WARNINGS.
		dfhcommarea.getUbocRecord().getCommInfo()
				.setUbocNbrWarnings(Trunc.toInt(1 + dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarnings(), 9));
	}

	/**Original name: 9090-CONVERT-ERROR-TO-TEXT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CONVERT ERROR TO TEXT                                          *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void convertErrorToText() {
		Halrplac halrplac = null;
		StringParam wsNonlogErrCol1Value = null;
		StringParam wsNonlogErrCol2Name = null;
		StringParam wsNonlogErrCol2Value = null;
		StringParam wsNonlogErrContextText = null;
		StringParam wsNonlogErrContextValue = null;
		StringParam wsNonlogErrAlltxtText = null;
		StringParam wsErrWngTxtD = null;
		// COB_CODE: MOVE SPACES TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD("");
		// COB_CODE: MOVE WS-APPLICATION-NM TO APP-NM.
		ws.getDclhalNlbeWngTxtV().setAppNm(ws.getWsSpecificWorkAreas().getWsApplicationNm());
		// COB_CODE: EXEC SQL
		//               SELECT HNLB_ERR_WNG_TXT
		//               INTO  :HNLB-ERR-WNG-TXT
		//               FROM HAL_NLBE_WNG_TXT_V
		//               WHERE (APP_NM            = :APP-NM
		//                   OR APP_NM            = '          ')
		//                 AND HNLB_ERR_WNG_CD   = :HNLB-ERR-WNG-CD
		//           END-EXEC.
		this.ws.getDclhalNlbeWngTxtV().setHnlbErrWngTxt(halNlbeWngTxtVDao.selectRec(ws.getDclhalNlbeWngTxtV().getAppNm(),
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd(), ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxt()));
		// COB_CODE:      EVALUATE TRUE
		//                    WHEN ERD-SQL-GOOD
		//                        CONTINUE
		//                    WHEN ERD-SQL-NOT-FOUND
		//           *            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                    WHEN OTHER
		//                        GO TO 9090-CONVERT-ERROR-TO-TEXT-X
		//                END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND://            MOVE HNLB-ERR-WNG-CD   TO HNLB-ERR-WNG-TXT
			// COB_CODE: MOVE HNLB-ERR-WNG-CD   TO WS-ERR-WNG-TXT-D
			ws.getWsNonlogPlaceholderValues().setErrWngTxtD(ws.getDclhalNlbeWngTxtV().getHnlbErrWngCd());
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR                      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_NLBE_WNG_TXT_V'
			//                TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_NLBE_WNG_TXT_V");
			// COB_CODE: MOVE '9090-CONVERT-ERROR-TO-TEXT'
			//                TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("9090-CONVERT-ERROR-TO-TEXT");
			// COB_CODE: MOVE 'READ HAL_NLBE_WNG_TXT FAILED'
			//                TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ HAL_NLBE_WNG_TXT FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 9090-CONVERT-ERROR-TO-TEXT-X
			return;
		}
		//* SUBSTITUTE PLACEHOLDERS IN MESSAGE
		// COB_CODE: MOVE HNLB-ERR-WNG-TXT-D (1:HNLB-ERR-WNG-TXT-L)
		//             TO WS-ERR-WNG-TXT-D.
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(
				ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtDFormatted().substring((1) - 1, ws.getDclhalNlbeWngTxtV().getHnlbErrWngTxtL()));
		// COB_CODE: CALL 'HALRPLAC' USING WS-NONLOG-ERR-COL1-NAME
		//                                 WS-NONLOG-ERR-COL1-VALUE
		//                                 WS-NONLOG-ERR-COL2-NAME
		//                                 WS-NONLOG-ERR-COL2-VALUE
		//                                 WS-NONLOG-ERR-CONTEXT-TEXT
		//                                 WS-NONLOG-ERR-CONTEXT-VALUE
		//                                 WS-NONLOG-ERR-ALLTXT-TEXT
		//                                 WS-ERR-WNG-TXT-D.
		halrplac = Halrplac.getInstance();
		wsNonlogErrCol1Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol1Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL1_VALUE);
		wsNonlogErrCol2Name = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Name(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_NAME);
		wsNonlogErrCol2Value = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrCol2Value(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_COL2_VALUE);
		wsNonlogErrContextText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_TEXT);
		wsNonlogErrContextValue = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrContextValue(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_CONTEXT_VALUE);
		wsNonlogErrAlltxtText = new StringParam(ws.getWsNonlogPlaceholderValues().getNonlogErrAlltxtText(),
				WsNonlogPlaceholderValues.Len.NONLOG_ERR_ALLTXT_TEXT);
		wsErrWngTxtD = new StringParam(ws.getWsNonlogPlaceholderValues().getErrWngTxtD(), WsNonlogPlaceholderValues.Len.ERR_WNG_TXT_D);
		halrplac.run(new Object[] { ws.getWsNonlogPlaceholderValues(), wsNonlogErrCol1Value, wsNonlogErrCol2Name, wsNonlogErrCol2Value,
				wsNonlogErrContextText, wsNonlogErrContextValue, wsNonlogErrAlltxtText, wsErrWngTxtD });
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol1Value(wsNonlogErrCol1Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Name(wsNonlogErrCol2Name.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrCol2Value(wsNonlogErrCol2Value.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextText(wsNonlogErrContextText.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrContextValue(wsNonlogErrContextValue.getString());
		ws.getWsNonlogPlaceholderValues().setNonlogErrAlltxtText(wsNonlogErrAlltxtText.getString());
		ws.getWsNonlogPlaceholderValues().setErrWngTxtD(wsErrWngTxtD.getString());
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getUbocRecord().getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getUbocRecord().getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowName(), UbocCommInfoCaws002.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocMsgId(), UbocCommInfoCaws002.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocSessionId(), UbocCommInfoCaws002.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserid(), UbocCommInfoCaws002.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqMsgStore(), UbocCommInfoCaws002.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfoCaws002.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespDataStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW.test(
				dfhcommarea.getUbocRecord().getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getUbocRecord().getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfoCaws002.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	public void initClientTabRow() {
		ws.getCawlc002().setClientTabChkSumFormatted("000000000");
		ws.getCawlc002().setClientIdKcre("");
		ws.getCawlc002().setTransProcessDt("");
		ws.getCawlc002().getClientTabKey().setClientIdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabKey().setClientId("");
		ws.getCawlc002().getClientTabKey().setHistoryVldNbrCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabKey().setHistoryVldNbrSign(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabKey().setCw02cHistoryVldNbrFormatted("00000");
		ws.getCawlc002().getClientTabKey().setCiclEffDtCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabKey().setCiclEffDt("");
		ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclPriSubCd("");
		ws.getCawlc002().getClientTabData().setCw02cCiclFstNmCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclFstNm("");
		ws.getCawlc002().getClientTabData().setCw02cCiclLstNmCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclLstNm("");
		ws.getCawlc002().getClientTabData().setCw02cCiclMdlNmCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclMdlNm("");
		ws.getCawlc002().getClientTabData().setCw02cNmPfxCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cNmPfx("");
		ws.getCawlc002().getClientTabData().setCw02cNmSfxCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cNmSfx("");
		ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cPrimaryProDsnCd("");
		ws.getCawlc002().getClientTabData().setCw02cLegEntCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cLegEntCd("");
		ws.getCawlc002().getClientTabData().setCw02cCiclSdxCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclSdxCd("");
		ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDtNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclOgnInceptDt("");
		ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclAddNmIndNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclAddNmInd(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclDobDtCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclDobDt("");
		ws.getCawlc002().getClientTabData().setCw02cCiclBirStCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclBirStCd("");
		ws.getCawlc002().getClientTabData().setCw02cGenderCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cGenderCd(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cPriLggCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cPriLggCd("");
		ws.getCawlc002().getClientTabData().setCw02cUserIdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cUserId("");
		ws.getCawlc002().getClientTabData().setCw02cStatusCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cStatusCd(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cTerminalIdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cTerminalId("");
		ws.getCawlc002().getClientTabData().setCw02cCiclExpDtCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclExpDt("");
		ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclEffAcyTs("");
		ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTsCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclExpAcyTs("");
		ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cStatutoryTleCd("");
		ws.getCawlc002().getClientTabData().setCw02cCiclLngNmCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclLngNmNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclLngNm("");
		ws.getCawlc002().getClientTabData().setCw02cLoLstNmMchCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cLoLstNmMchCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cLoLstNmMchCd("");
		ws.getCawlc002().getClientTabData().setCw02cHiLstNmMchCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cHiLstNmMchCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cHiLstNmMchCd("");
		ws.getCawlc002().getClientTabData().setCw02cLoFstNmMchCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cLoFstNmMchCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cLoFstNmMchCd("");
		ws.getCawlc002().getClientTabData().setCw02cHiFstNmMchCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cHiFstNmMchCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cHiFstNmMchCd("");
		ws.getCawlc002().getClientTabData().setCw02cCiclAcqSrcCdCi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclAcqSrcCdNi(Types.SPACE_CHAR);
		ws.getCawlc002().getClientTabData().setCw02cCiclAcqSrcCd("");
	}

	public void initWsHoldArea() {
		ws.getWsSpecificWorkAreas().getWsHoldArea().setLitFieldNm("");
		ws.getWsSpecificWorkAreas().getWsHoldArea().setValidDateHold("");
		ws.getWsSpecificWorkAreas().getWsHoldArea().setLinkToPgmNm("");
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldDtaTxt("");
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldAmt(new AfDecimal(0, 14, 2));
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldSign(Types.SPACE_CHAR);
		ws.getWsSpecificWorkAreas().getWsHoldArea().setHoldCi(Types.SPACE_CHAR);
		ws.getWsSpecificWorkAreas().getWsHoldArea().setCalcTimestamp("");
	}

	public void initWsActCurrentIsoTsArea() {
		ws.getWsActCurIsoDateTime().setWsActCurrentTimestamp("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
