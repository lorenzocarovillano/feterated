/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLFRM-REC-ATC-TYP<br>
 * Variable: DCLFRM-REC-ATC-TYP from copybook XZH00019<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclfrmRecAtcTyp {

	//==== PROPERTIES ====
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: FRM-NBR
	private String frmNbr = DefaultValues.stringVal(Len.FRM_NBR);
	//Original name: FRM-EDT-DT
	private String frmEdtDt = DefaultValues.stringVal(Len.FRM_EDT_DT);
	//Original name: FRM-SPE-ATC-CD
	private String frmSpeAtcCd = DefaultValues.stringVal(Len.FRM_SPE_ATC_CD);
	//Original name: INSD-REC-IND
	private char insdRecInd = DefaultValues.CHAR_VAL;
	//Original name: ADD-INS-REC-IND
	private char addInsRecInd = DefaultValues.CHAR_VAL;
	//Original name: LPE-MGE-REC-IND
	private char lpeMgeRecInd = DefaultValues.CHAR_VAL;
	//Original name: CER-REC-IND
	private char cerRecInd = DefaultValues.CHAR_VAL;
	//Original name: ANI-REC-IND
	private char aniRecInd = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	public void setFrmNbr(String frmNbr) {
		this.frmNbr = Functions.subString(frmNbr, Len.FRM_NBR);
	}

	public String getFrmNbr() {
		return this.frmNbr;
	}

	public void setFrmEdtDt(String frmEdtDt) {
		this.frmEdtDt = Functions.subString(frmEdtDt, Len.FRM_EDT_DT);
	}

	public String getFrmEdtDt() {
		return this.frmEdtDt;
	}

	public void setFrmSpeAtcCd(String frmSpeAtcCd) {
		this.frmSpeAtcCd = Functions.subString(frmSpeAtcCd, Len.FRM_SPE_ATC_CD);
	}

	public String getFrmSpeAtcCd() {
		return this.frmSpeAtcCd;
	}

	public void setInsdRecInd(char insdRecInd) {
		this.insdRecInd = insdRecInd;
	}

	public char getInsdRecInd() {
		return this.insdRecInd;
	}

	public void setAddInsRecInd(char addInsRecInd) {
		this.addInsRecInd = addInsRecInd;
	}

	public char getAddInsRecInd() {
		return this.addInsRecInd;
	}

	public void setLpeMgeRecInd(char lpeMgeRecInd) {
		this.lpeMgeRecInd = lpeMgeRecInd;
	}

	public char getLpeMgeRecInd() {
		return this.lpeMgeRecInd;
	}

	public void setCerRecInd(char cerRecInd) {
		this.cerRecInd = cerRecInd;
	}

	public char getCerRecInd() {
		return this.cerRecInd;
	}

	public void setAniRecInd(char aniRecInd) {
		this.aniRecInd = aniRecInd;
	}

	public char getAniRecInd() {
		return this.aniRecInd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int FRM_NBR = 30;
		public static final int FRM_EDT_DT = 10;
		public static final int FRM_SPE_ATC_CD = 10;
		public static final int FRM_EFF_DT = 10;
		public static final int FRM_EXP_DT = 10;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
