/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC010-WARNINGS<br>
 * Variables: XZC010-WARNINGS from copybook XZC010C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc010Warnings {

	//==== PROPERTIES ====
	//Original name: XZC010-WARN-MSG
	private String xzc010WarnMsg = DefaultValues.stringVal(Len.XZC010_WARN_MSG);

	//==== METHODS ====
	public void setXzc010WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc010WarnMsg = MarshalByte.readString(buffer, position, Len.XZC010_WARN_MSG);
	}

	public byte[] getXzc010WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc010WarnMsg, Len.XZC010_WARN_MSG);
		return buffer;
	}

	public void initXzc010WarningsSpaces() {
		xzc010WarnMsg = "";
	}

	public void setXzc010WarnMsg(String xzc010WarnMsg) {
		this.xzc010WarnMsg = Functions.subString(xzc010WarnMsg, Len.XZC010_WARN_MSG);
	}

	public String getXzc010WarnMsg() {
		return this.xzc010WarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC010_WARN_MSG = 500;
		public static final int XZC010_WARNINGS = XZC010_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
