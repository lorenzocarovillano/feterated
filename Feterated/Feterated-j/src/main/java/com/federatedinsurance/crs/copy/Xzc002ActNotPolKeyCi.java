/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;

/**Original name: XZC002-ACT-NOT-POL-KEY-CI<br>
 * Variable: XZC002-ACT-NOT-POL-KEY-CI from copybook XZ0C0002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzc002ActNotPolKeyCi {

	//==== PROPERTIES ====
	//Original name: XZC002-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC002-POL-NBR-CI
	private char polNbrCi = DefaultValues.CHAR_VAL;

	//==== METHODS ====
	public void setActNotPolKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		polNbrCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotPolKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, polNbrCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setPolNbrCi(char polNbrCi) {
		this.polNbrCi = polNbrCi;
	}

	public char getPolNbrCi() {
		return this.polNbrCi;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int POL_NBR_CI = 1;
		public static final int ACT_NOT_POL_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI + POL_NBR_CI;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
