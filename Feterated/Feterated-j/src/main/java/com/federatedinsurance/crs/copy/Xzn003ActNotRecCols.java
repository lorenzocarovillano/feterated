/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: XZN003-ACT-NOT-REC-COLS<br>
 * Variable: XZN003-ACT-NOT-REC-COLS from copybook XZ0N0003<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzn003ActNotRecCols {

	//==== PROPERTIES ====
	//Original name: XZN003-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: XZN003-REC-TYP-CD
	private String recTypCd = "Rec Typ Cd";
	//Original name: XZN003-REC-CLT-ID
	private String recCltId = "Rec Clt Id";
	//Original name: XZN003-REC-NM
	private String recNm = "Rec Nm";
	//Original name: XZN003-REC-ADR-ID
	private String recAdrId = "Rec Adr Id";
	//Original name: XZN003-LIN-1-ADR
	private String lin1Adr = "Lin 1 Adr";
	//Original name: XZN003-LIN-2-ADR
	private String lin2Adr = "Lin 2 Adr";
	//Original name: XZN003-CIT-NM
	private String citNm = "Cit Nm";
	//Original name: XZN003-ST-ABB
	private String stAbb = "St Abb";
	//Original name: XZN003-PST-CD
	private String pstCd = "Pst Cd";
	//Original name: XZN003-MNL-IND
	private String mnlInd = "Mnl Ind";
	//Original name: XZN003-CER-NBR
	private String cerNbr = "Cer Nbr";

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getRecTypCd() {
		return this.recTypCd;
	}

	public String getRecCltId() {
		return this.recCltId;
	}

	public String getRecNm() {
		return this.recNm;
	}

	public String getRecAdrId() {
		return this.recAdrId;
	}

	public String getLin1Adr() {
		return this.lin1Adr;
	}

	public String getLin2Adr() {
		return this.lin2Adr;
	}

	public String getCitNm() {
		return this.citNm;
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public String getPstCd() {
		return this.pstCd;
	}

	public String getMnlInd() {
		return this.mnlInd;
	}

	public String getCerNbr() {
		return this.cerNbr;
	}
}
