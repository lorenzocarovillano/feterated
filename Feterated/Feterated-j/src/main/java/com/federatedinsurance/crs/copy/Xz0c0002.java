/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ0C0002<br>
 * Variable: XZ0C0002 from copybook XZ0C0002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz0c0002 {

	//==== PROPERTIES ====
	//Original name: XZC002-ACT-NOT-POL-FIXED
	private Xzc002ActNotPolFixed actNotPolFixed = new Xzc002ActNotPolFixed();
	//Original name: XZC002-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC002-ACT-NOT-POL-KEY
	private Xzc002ActNotPolKey actNotPolKey = new Xzc002ActNotPolKey();
	//Original name: XZC002-ACT-NOT-POL-KEY-CI
	private Xzc002ActNotPolKeyCi actNotPolKeyCi = new Xzc002ActNotPolKeyCi();
	//Original name: XZC002-ACT-NOT-POL-DATA
	private Xzc002ActNotPolData actNotPolData = new Xzc002ActNotPolData();
	//Original name: XZC002-POL-TYP-DES
	private String polTypDes = DefaultValues.stringVal(Len.POL_TYP_DES);

	//==== METHODS ====
	public void setActNotPolRowFormatted(String data) {
		byte[] buffer = new byte[Len.ACT_NOT_POL_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.ACT_NOT_POL_ROW);
		setActNotPolRowBytes(buffer, 1);
	}

	public String getActNotPolRowFormatted() {
		return MarshalByteExt.bufferToStr(getActNotPolRowBytes());
	}

	/**Original name: XZC002-ACT-NOT-POL-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0C0002 - ACT_NOT_POL TABLE                                   *
	 *             FRONT END/ BACK END INTERFACE DESCRIPTION           *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *  TO07614 30 Sep 2008 E404GRK   GENERATED                        *
	 *  TO07614 09 Mar 2009 E404DLP   CHANGED ADR-SEQ-NBR TO ADR-ID    *
	 *  20163.20 13 Jul 2018 E404DMW  UPDATED ADDRESS AND CLIENT IDS   *
	 *                                FROM 20 TO 64                    *
	 * *****************************************************************</pre>*/
	public byte[] getActNotPolRowBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_POL_ROW];
		return getActNotPolRowBytes(buffer, 1);
	}

	public void setActNotPolRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotPolFixed.setActNotPolFixedBytes(buffer, position);
		position += Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		setActNotPolDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_DATES;
		actNotPolKey.setActNotPolKeyBytes(buffer, position);
		position += Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY;
		actNotPolKeyCi.setActNotPolKeyCiBytes(buffer, position);
		position += Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI;
		actNotPolData.setActNotPolDataBytes(buffer, position);
		position += Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA;
		setExtensionFieldsBytes(buffer, position);
	}

	public byte[] getActNotPolRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotPolFixed.getActNotPolFixedBytes(buffer, position);
		position += Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED;
		getActNotPolDatesBytes(buffer, position);
		position += Len.ACT_NOT_POL_DATES;
		actNotPolKey.getActNotPolKeyBytes(buffer, position);
		position += Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY;
		actNotPolKeyCi.getActNotPolKeyCiBytes(buffer, position);
		position += Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI;
		actNotPolData.getActNotPolDataBytes(buffer, position);
		position += Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA;
		getExtensionFieldsBytes(buffer, position);
		return buffer;
	}

	public void setActNotPolDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotPolDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getTransProcessDtFormatted() {
		return Functions.padBlanks(getTransProcessDt(), Len.TRANS_PROCESS_DT);
	}

	public void setExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		polTypDes = MarshalByte.readString(buffer, position, Len.POL_TYP_DES);
	}

	public byte[] getExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polTypDes, Len.POL_TYP_DES);
		return buffer;
	}

	public void setPolTypDes(String polTypDes) {
		this.polTypDes = Functions.subString(polTypDes, Len.POL_TYP_DES);
	}

	public String getPolTypDes() {
		return this.polTypDes;
	}

	public Xzc002ActNotPolData getActNotPolData() {
		return actNotPolData;
	}

	public Xzc002ActNotPolFixed getActNotPolFixed() {
		return actNotPolFixed;
	}

	public Xzc002ActNotPolKey getActNotPolKey() {
		return actNotPolKey;
	}

	public Xzc002ActNotPolKeyCi getActNotPolKeyCi() {
		return actNotPolKeyCi;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_PROCESS_DT = 10;
		public static final int POL_TYP_DES = 30;
		public static final int ACT_NOT_POL_DATES = TRANS_PROCESS_DT;
		public static final int EXTENSION_FIELDS = POL_TYP_DES;
		public static final int ACT_NOT_POL_ROW = Xzc002ActNotPolFixed.Len.ACT_NOT_POL_FIXED + ACT_NOT_POL_DATES
				+ Xzc002ActNotPolKey.Len.ACT_NOT_POL_KEY + Xzc002ActNotPolKeyCi.Len.ACT_NOT_POL_KEY_CI + Xzc002ActNotPolData.Len.ACT_NOT_POL_DATA
				+ EXTENSION_FIELDS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
