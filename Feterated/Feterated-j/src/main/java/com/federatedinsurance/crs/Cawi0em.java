/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.copy.CwemfClientEmailRow;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.Ts020tbl;
import com.federatedinsurance.crs.ws.Cawi0emData;
import com.federatedinsurance.crs.ws.DfhcommareaTs020100;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: CAWI0EM<br>
 * <pre>AUTHOR.       FEDERATED INSURANCE.
 * DATE-WRITTEN. 08 MAR 2007.
 * ***************************************************************
 *   PROGRAM TITLE - COMM SHELL REQUEST/RESPONSE FORMATTER FOR   *
 *                   THE BDO CAWD0EM                             *
 *                                                               *
 *   PLATFORM - HOST CICS                                        *
 *                                                               *
 *   PURPOSE -  INSERT A SINGLE ROW ON THE REQUEST UMT, OR       *
 *              RETRIEVE A SINGLE ROW FROM THE RESPONSE UMT FOR  *
 *              THE BDO CAWD0EM                                  *
 *              ANY FRAMEWORK REQUEST OR RESPONSE MODULE SHOULD  *
 *              USE THIS PROGRAM.                                *
 *                                                               *
 *   PROGRAM INITIATION - LINKED TO FROM A FRAMEWORK REQUEST     *
 *                        MODULE OR A FRAMEWORK RESPONSE MODULE. *
 *                                                               *
 *   DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
 *                         OUTPUT RETURNED VIA DFHCOMMAREA       *
 *                                                               *
 * ***************************************************************
 * ***************************************************************
 *   NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
 *         VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
 *         APPLICATION CODING.                                   *
 *                                                               *
 *       T E M P L A T E   M A I N T E N A N C E   L O G         *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TS129    07JUL05   E404LJL   INITIAL TEMPLATE VERSION        *
 * ***************************************************************
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  PP00015  08MAR07   E404ASW   INITIAL PROGRAM                 *
 *                                                               *
 * ***************************************************************</pre>*/
public class Cawi0em extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>***************************************************************
	 *  START OF:                                                    *
	 *      DCLGEN INCLUDES FOR SUPPORT TABLE TRANSLATIONS.          *
	 * ***************************************************************
	 * ***************************************************************
	 *  START OF:                                                    *
	 *      GENERAL BPO/COMM SHELL PROGRAM WORKING-STORAGE           *
	 *      (INCLUDED IN ALL BPOS/COMM SHELL PROGRAMS)               *
	 * ***************************************************************
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS</pre>*/
	private Sqlca sqlca = new Sqlca();
	//Original name: WORKING-STORAGE
	private Cawi0emData ws = new Cawi0emData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaTs020100 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaTs020100 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Cawi0em getInstance() {
		return (Programs.getInstance(Cawi0em.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  CONTROLS MAINLINE PROGRAM PROCESSING                         *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: IF EIBCALEN IS EQUAL TO ZERO
		//               END-EXEC
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: EXEC CICS RETURN
			//           END-EXEC
			throw new TpReturnException();
		}
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING.
		beginningHousekeeping();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: IF TF-REQUEST-FORMATTER-CALL
		//               PERFORM 3000-CREATE-REQUEST-ROW
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getTs020tbl().getTfRequestResponseFlag().isRequestFormatterCall()) {
			// COB_CODE: PERFORM 3000-CREATE-REQUEST-ROW
			createRequestRow();
		} else if (ws.getTs020tbl().getTfRequestResponseFlag().isResponseFormatterCall()) {
			// COB_CODE: IF TF-RESPONSE-FORMATTER-CALL
			//               PERFORM 4000-CREATE-RESPONSE-ROW
			//           END-IF
			// COB_CODE: PERFORM 4000-CREATE-RESPONSE-ROW
			createResponseRow();
		}
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM STARTUP/INITIALIZATION PROCESSING                    *
	 * ***************************************************************
	 *  INITIALIZE ERROR PROCESSING FIELDS</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE ESTO-STORE-INFO
		//                      ESTO-RETURN-INFO.
		initEstoStoreInfo();
		initEstoReturnInfo();
		// RETRIEVE THE DATA PASSED TO THIS MODULE
		// COB_CODE: MOVE CSC-DATA-BUFFER(1:CSC-DATA-BUFFER-LENGTH)
		//                                       TO TABLE-FORMATTER-DATA.
		ws.setTableFormatterDataFormatted(dfhcommarea.getCommunicationShellCommon().getCscDataBufferFormatted().substring((1) - 1,
				dfhcommarea.getCommunicationShellCommon().getCscDataBufferLength()));
	}

	/**Original name: 3000-CREATE-REQUEST-ROW_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  CREATE A SINGLE REQUEST ROW ON THE REQUEST UMT.              *
	 *  USES A FRAMEWORK SUPPLIED ROUTINE TO UPDATE THE UMT.         *
	 * ***************************************************************</pre>*/
	private void createRequestRow() {
		// COB_CODE: MOVE TF-DATA-BUFFER(1:LENGTH OF CWEMF-CLIENT-EMAIL-ROW)
		//                                       TO CWEMF-CLIENT-EMAIL-ROW.
		ws.getCwemfClientEmailRow().setCwemfClientEmailRowFormatted(
				ws.getTs020tbl().getTfDataBufferFormatted().substring((1) - 1, CwemfClientEmailRow.Len.CWEMF_CLIENT_EMAIL_ROW));
		// COB_CODE: PERFORM 3100-FORMAT-REQUEST-FIELDS.
		//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=CAWI0EM.CBL:line=352, because the code is unreachable.
		// COB_CODE: SET HALRURQA-WRITE-FUNC     TO TRUE.
		ws.getHallurqa().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: IF TF-BUSINESS-OBJECT-NM = SPACES OR LOW-VALUES
		//               MOVE WS-BUS-OBJ-NAME    TO HALRURQA-BUS-OBJ-NM
		//           ELSE
		//                                       TO HALRURQA-BUS-OBJ-NM
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs020tbl().getTfBusinessObjectNm())
				|| Characters.EQ_LOW.test(ws.getTs020tbl().getTfBusinessObjectNm(), Ts020tbl.Len.TF_BUSINESS_OBJECT_NM)) {
			// COB_CODE: MOVE WS-BUS-OBJ-NAME    TO HALRURQA-BUS-OBJ-NM
			ws.getHallurqa().setBusObjNm(ws.getWsMiscWorkFlds().getBusObjName());
		} else {
			// COB_CODE: MOVE TF-BUSINESS-OBJECT-NM
			//                                   TO HALRURQA-BUS-OBJ-NM
			ws.getHallurqa().setBusObjNm(ws.getTs020tbl().getTfBusinessObjectNm());
		}
		// COB_CODE: MOVE TF-ACTION-CODE         TO HALRURQA-ACTION-CODE.
		ws.getHallurqa().setActionCode(ws.getTs020tbl().getTfActionCode().getTfActionCode());
		// COB_CODE: MOVE LENGTH OF CWEMF-CLIENT-EMAIL-ROW
		//                                       TO HALRURQA-BUS-OBJ-DATA-LENGTH.
		ws.getHallurqa().setBusObjDataLength(((short) CwemfClientEmailRow.Len.CWEMF_CLIENT_EMAIL_ROW));
		// COB_CODE: MOVE CWEMF-CLIENT-EMAIL-ROW TO REQUEST-DATA-BUFFER.
		ws.setRequestDataBuffer(ws.getCwemfClientEmailRow().getCwemfClientEmailRowFormatted());
		// COB_CODE: MOVE LENGTH OF REQUEST-UMT-MODULE-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Cawi0emData.Len.REQUEST_UMT_MODULE_DATA));
		// COB_CODE: MOVE REQUEST-UMT-MODULE-DATA
		//                                       TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getRequestUmtModuleDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  (CF-REQUEST-UMT-MODULE)
		//                COMMAREA (DFHCOMMAREA)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWI0EM", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA).link(ws.getCfRequestUmtModule(),
				new Ts020100());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '3000-CREATE-REQUEST-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3000-CREATE-REQUEST-ROW");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("3000-CREATE-REQUEST-ROW");
			// COB_CODE: MOVE CF-REQUEST-UMT-MODULE
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().setFailedLinkPgmName(ws.getCfRequestUmtModule());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 3000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 4000-CREATE-RESPONSE-ROW_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  RETRIEVE A SINGLE RESPONSE ROW FROM THE RESPONSE UMT.        *
	 *  USES A FRAMEWORK SUPPLIED ROUTINE TO READ THE UMT.           *
	 * ***************************************************************</pre>*/
	private void createResponseRow() {
		// COB_CODE: SET HALRRESP-READ-FUNC      TO TRUE.
		ws.getHallresp().getFunction().setHalrrespReadFunc();
		// COB_CODE: IF TF-BUSINESS-OBJECT-NM = SPACES OR LOW-VALUES
		//               MOVE WS-BUS-OBJ-NAME    TO HALRRESP-BUS-OBJ-NM
		//           ELSE
		//                                       TO HALRRESP-BUS-OBJ-NM
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs020tbl().getTfBusinessObjectNm())
				|| Characters.EQ_LOW.test(ws.getTs020tbl().getTfBusinessObjectNm(), Ts020tbl.Len.TF_BUSINESS_OBJECT_NM)) {
			// COB_CODE: MOVE WS-BUS-OBJ-NAME    TO HALRRESP-BUS-OBJ-NM
			ws.getHallresp().setBusObjNm(ws.getWsMiscWorkFlds().getBusObjName());
		} else {
			// COB_CODE: MOVE TF-BUSINESS-OBJECT-NM
			//                                   TO HALRRESP-BUS-OBJ-NM
			ws.getHallresp().setBusObjNm(ws.getTs020tbl().getTfBusinessObjectNm());
		}
		// COB_CODE: MOVE TF-REC-SEQ             TO HALRRESP-REC-SEQ.
		ws.getHallresp().setRecSeqFormatted(ws.getTs020tbl().getTfRecSeqFormatted());
		// COB_CODE: MOVE LENGTH OF RESPONSE-UMT-MODULE-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Cawi0emData.Len.RESPONSE_UMT_MODULE_DATA));
		// COB_CODE: MOVE RESPONSE-UMT-MODULE-DATA
		//                                       TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getResponseUmtModuleDataFormatted());
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  (CF-RESPONSE-UMT-MODULE)
		//                COMMAREA (DFHCOMMAREA)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWI0EM", execContext).commarea(dfhcommarea).length(DfhcommareaTs020100.Len.DFHCOMMAREA).link(ws.getCfResponseUmtModule(),
				new Ts020200());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* CHECKS THE SUCCESS OF THE LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP (NORMAL)
		//               GO TO 4000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE '4000-CREATE-RESPONSE-ROW'
			//                                   TO EFAL-ERR-PARAGRAPH
			//                                      CSC-FAILED-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4000-CREATE-RESPONSE-ROW");
			dfhcommarea.getCommunicationShellCommon().setCscFailedParagraph("4000-CREATE-RESPONSE-ROW");
			// COB_CODE: MOVE CF-RESPONSE-UMT-MODULE
			//                                   TO WS-FAILED-LINK-PGM-NAME
			ws.getWsMiscWorkFlds().setFailedLinkPgmName(ws.getCfResponseUmtModule());
			// COB_CODE: PERFORM 9100-PROGRAM-LINK-FAILED
			programLinkFailed();
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4000-EXIT
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		// COB_CODE: MOVE CSC-DATA-BUFFER        TO RESPONSE-UMT-MODULE-DATA.
		ws.setResponseUmtModuleDataFormatted(dfhcommarea.getCommunicationShellCommon().getCscDataBufferFormatted());
		// COB_CODE: MOVE RESPONSE-DATA-BUFFER
		//                (1:LENGTH OF CWEMF-CLIENT-EMAIL-ROW)
		//                                       TO CWEMF-CLIENT-EMAIL-ROW.
		ws.getCwemfClientEmailRow().setCwemfClientEmailRowFormatted(
				ws.getResponseDataBufferFormatted().substring((1) - 1, CwemfClientEmailRow.Len.CWEMF_CLIENT_EMAIL_ROW));
		// COB_CODE: MOVE HALRRESP-REC-FOUND-SW  TO TF-REC-FOUND-FLAG.
		ws.getTs020tbl().getTfRecFoundFlag().setTfRecFoundFlag(ws.getHallresp().getRecFoundSw().getRecFoundSw());
		// COB_CODE: IF TF-RECORD-NOT-FOUND
		//               GO TO 4000-EXIT
		//           END-IF.
		if (ws.getTs020tbl().getTfRecFoundFlag().isNotFound()) {
			// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
			//                                   TO CSC-DATA-BUFFER-LENGTH
			dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Cawi0emData.Len.TABLE_FORMATTER_DATA));
			// COB_CODE: MOVE TABLE-FORMATTER-DATA
			//                                   TO CSC-DATA-BUFFER
			dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
		//    PERFORM 4100-FORMAT-RESPONSE-FIELDS.
		//    PERFORM 4200-TRANSLATE-SUPPORT-VALUE.
		// COB_CODE: MOVE CWEMF-CLIENT-EMAIL-ROW TO TF-DATA-BUFFER.
		ws.getTs020tbl().setTfDataBuffer(ws.getCwemfClientEmailRow().getCwemfClientEmailRowFormatted());
		// COB_CODE: MOVE LENGTH OF TABLE-FORMATTER-DATA
		//                                       TO CSC-DATA-BUFFER-LENGTH.
		dfhcommarea.getCommunicationShellCommon().setCscDataBufferLength(((short) Cawi0emData.Len.TABLE_FORMATTER_DATA));
		// COB_CODE: MOVE TABLE-FORMATTER-DATA   TO CSC-DATA-BUFFER.
		dfhcommarea.getCommunicationShellCommon().setCscDataBuffer(ws.getTableFormatterDataFormatted());
	}

	/**Original name: 9100-PROGRAM-LINK-FAILED_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  PERFORM COMMON LOGIC TO LOG AN ERROR ON A CICS LINK          *
	 * ***************************************************************</pre>*/
	private void programLinkFailed() {
		// COB_CODE: SET WS-LOG-ERROR            TO TRUE.
		ws.getWsLogWarningOrErrorSw().setError();
		// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
		// COB_CODE: SET EFAL-CICS-FAILED        TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
		// COB_CODE: SET ETRA-CICS-LINK          TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
		// COB_CODE: MOVE WS-PROGRAM-NAME        TO EFAL-ERR-OBJECT-NAME
		//                                          CSC-FAILED-MODULE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsMiscWorkFlds().getProgramName());
		dfhcommarea.getCommunicationShellCommon().setCscFailedModule(ws.getWsMiscWorkFlds().getProgramName());
		// COB_CODE: MOVE WS-PROGRAM-LINK-FAILED TO EFAL-ERR-COMMENT.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment(ws.getWsMiscWorkFlds().getProgramLinkFailedFormatted());
		// COB_CODE: IF WS-RESPONSE-CODE < ZERO
		//               MOVE '-'                TO EFAL-CICS-ERR-RESP-SIGN
		//           ELSE
		//               MOVE '+'                TO EFAL-CICS-ERR-RESP-SIGN
		//           END-IF.
		if (ws.getWsNotSpecificMisc().getResponseCode() < 0) {
			// COB_CODE: MOVE '-'                TO EFAL-CICS-ERR-RESP-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO EFAL-CICS-ERR-RESP-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		}
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EFAL-CICS-ERR-RESP
		//                                          CSC-EIBRESP-DISPLAY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
		dfhcommarea.getCommunicationShellCommon().setCscEibrespDisplay(ws.getWsNotSpecificMisc().getResponseCode());
		// COB_CODE: IF WS-RESPONSE-CODE2 < ZERO
		//               MOVE '-'                TO EFAL-CICS-ERR-RESP2-SIGN
		//           ELSE
		//               MOVE '+'                TO EFAL-CICS-ERR-RESP2-SIGN
		//           END-IF.
		if (ws.getWsNotSpecificMisc().getResponseCode2() < 0) {
			// COB_CODE: MOVE '-'                TO EFAL-CICS-ERR-RESP2-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
		} else {
			// COB_CODE: MOVE '+'                TO EFAL-CICS-ERR-RESP2-SIGN
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		}
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EFAL-CICS-ERR-RESP2
		//                                          CSC-EIBRESP2-DISPLAY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
		dfhcommarea.getCommunicationShellCommon().setCscEibresp2Display(ws.getWsNotSpecificMisc().getResponseCode2());
		// COB_CODE: MOVE WS-PROGRAM-LINK-FAILED TO EFAL-OBJ-DATA-KEY.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(ws.getWsMiscWorkFlds().getProgramLinkFailedFormatted());
		// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR.
		logWarningOrError();
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsMiscWorkFlds().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getUbocCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsMiscWorkFlds().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getUbocCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getUbocCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("CAWI0EM", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getUbocCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setUbocAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setUbocAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
