/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.DclhalertErrTranSupV;
import com.federatedinsurance.crs.copy.Hallesto;
import com.federatedinsurance.crs.copy.Halluidg;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program HALOETRA<br>
 * Generated as a class for rule WS.<br>*/
public class HaloetraData {

	//==== PROPERTIES ====
	//Original name: DCLHALERT-ERR-TRAN-SUP-V
	private DclhalertErrTranSupV dclhalertErrTranSupV = new DclhalertErrTranSupV();
	//Original name: WS-WORK-AREAS
	private WsWorkAreas wsWorkAreas = new WsWorkAreas();
	//Original name: W-WORKFIELDS
	private WWorkfields wWorkfields = new WWorkfields();
	//Original name: HALLESTO
	private Hallesto hallesto = new Hallesto();
	//Original name: WS-HALOUBOC-LINKAGE
	private Dfhcommarea wsHaloubocLinkage = new Dfhcommarea();
	//Original name: HALLUIDG
	private Halluidg halluidg = new Halluidg();

	//==== METHODS ====
	public void setWsHaloetraLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALOETRA_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALOETRA_LINKAGE);
		setWsHaloetraLinkageBytes(buffer, 1);
	}

	/**Original name: WS-HALOETRA-LINKAGE<br>
	 * <pre>---*
	 *  HALOETRA - LINKAGE
	 * ---*</pre>*/
	public byte[] getWsHaloetraLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALOETRA_LINKAGE];
		return getWsHaloetraLinkageBytes(buffer, 1);
	}

	public void setWsHaloetraLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallesto.setEstoStoreInfoBytes(buffer, position);
		position += Hallesto.Len.ESTO_STORE_INFO;
		hallesto.setEstoReturnInfoBytes(buffer, position);
	}

	public byte[] getWsHaloetraLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		hallesto.getEstoStoreInfoBytes(buffer, position);
		position += Hallesto.Len.ESTO_STORE_INFO;
		hallesto.getEstoReturnInfoBytes(buffer, position);
		return buffer;
	}

	public void setWsHalouidgLinkageFormatted(String data) {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		MarshalByte.writeString(buffer, 1, data, Len.WS_HALOUIDG_LINKAGE);
		setWsHalouidgLinkageBytes(buffer, 1);
	}

	public String getWsHalouidgLinkageFormatted() {
		return MarshalByteExt.bufferToStr(getWsHalouidgLinkageBytes());
	}

	/**Original name: WS-HALOUIDG-LINKAGE<br>
	 * <pre>---*
	 *  LINKAGE - FOR CALL TO HALUIDB/HALOUIDG
	 * ---*</pre>*/
	public byte[] getWsHalouidgLinkageBytes() {
		byte[] buffer = new byte[Len.WS_HALOUIDG_LINKAGE];
		return getWsHalouidgLinkageBytes(buffer, 1);
	}

	public void setWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.setUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.setUidgCaOutputBytes(buffer, position);
	}

	public byte[] getWsHalouidgLinkageBytes(byte[] buffer, int offset) {
		int position = offset;
		halluidg.getUidgCaIncomingBytes(buffer, position);
		position += Halluidg.Len.UIDG_CA_INCOMING;
		halluidg.getUidgCaOutputBytes(buffer, position);
		return buffer;
	}

	public DclhalertErrTranSupV getDclhalertErrTranSupV() {
		return dclhalertErrTranSupV;
	}

	public Hallesto getHallesto() {
		return hallesto;
	}

	public Halluidg getHalluidg() {
		return halluidg;
	}

	public Dfhcommarea getWsHaloubocLinkage() {
		return wsHaloubocLinkage;
	}

	public WsWorkAreas getWsWorkAreas() {
		return wsWorkAreas;
	}

	public WWorkfields getwWorkfields() {
		return wWorkfields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_HALOETRA_LINKAGE = Hallesto.Len.ESTO_STORE_INFO + Hallesto.Len.ESTO_RETURN_INFO;
		public static final int WS_HALOUIDG_LINKAGE = Halluidg.Len.UIDG_CA_INCOMING + Halluidg.Len.UIDG_CA_OUTPUT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
