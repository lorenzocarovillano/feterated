/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: UBOC-OBJECT-LOGGABLE-PROBLEMS<br>
 * Variable: UBOC-OBJECT-LOGGABLE-PROBLEMS from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocObjectLoggableProblems {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOGGABLE_ERRORS = 'E';
	public static final char LOGGABLE_WARNINGS = 'W';
	public static final char OK = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setUbocObjectLoggableProblems(char ubocObjectLoggableProblems) {
		this.value = ubocObjectLoggableProblems;
	}

	public void setUbocObjectLoggableProblemsFormatted(String ubocObjectLoggableProblems) {
		setUbocObjectLoggableProblems(Functions.charAt(ubocObjectLoggableProblems, Types.CHAR_SIZE));
	}

	public char getUbocObjectLoggableProblems() {
		return this.value;
	}

	public boolean isLoggableErrors() {
		return value == LOGGABLE_ERRORS;
	}

	public void setLoggableErrors() {
		value = LOGGABLE_ERRORS;
	}

	public boolean isUbocUowLoggableWarnings() {
		return value == LOGGABLE_WARNINGS;
	}

	public void setLoggableWarnings() {
		value = LOGGABLE_WARNINGS;
	}

	public boolean isOk() {
		return value == OK;
	}

	public void setUbocUowOk() {
		setUbocObjectLoggableProblemsFormatted(String.valueOf(OK));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_OBJECT_LOGGABLE_PROBLEMS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
