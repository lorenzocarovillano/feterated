/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DATA-TO-PASS<br>
 * Variable: DATA-TO-PASS from program XZ003000<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class DataToPass extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: FILLER-DATA-TO-PASS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.DATA_TO_PASS;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDataToPassBytes(buf);
	}

	public String getDataToPassFormatted() {
		return getFlr1Formatted();
	}

	public void setDataToPassBytes(byte[] buffer) {
		setDataToPassBytes(buffer, 1);
	}

	public byte[] getDataToPassBytes() {
		byte[] buffer = new byte[Len.DATA_TO_PASS];
		return getDataToPassBytes(buffer, 1);
	}

	public void setDataToPassBytes(byte[] buffer, int offset) {
		int position = offset;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getDataToPassBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr1Formatted() {
		return Functions.padBlanks(getFlr1(), Len.FLR1);
	}

	@Override
	public byte[] serialize() {
		return getDataToPassBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 32000;
		public static final int DATA_TO_PASS = FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
