/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.ActNotRecTypXz0b9020;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecTyp;
import com.federatedinsurance.crs.copy.DclactNotFrmRec;
import com.federatedinsurance.crs.copy.DclactNotRec;
import com.federatedinsurance.crs.copy.DclhalNlbeWngTxtV;
import com.federatedinsurance.crs.copy.DclrecTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;
import com.federatedinsurance.crs.ws.enums.WsNonLoggableWarnOrErrSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B9020<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b9020Data implements IActNotFrmRecTyp {

	//==== PROPERTIES ====
	private ActNotRecTypXz0b9020 actNotRecTypXz0b9020 = new ActNotRecTypXz0b9020(this);
	//Original name: CF-FORMAT-ADDRESS-PGM
	private String cfFormatAddressPgm = "TS529099";
	//Original name: EA-02-NOTHING-FOUND-MSG
	private Ea02NothingFoundMsg ea02NothingFoundMsg = new Ea02NothingFoundMsg();
	//Original name: EA-03-NOTHING-FOUND-BY-NOT-MSG
	private Ea03NothingFoundByNotMsg ea03NothingFoundByNotMsg = new Ea03NothingFoundByNotMsg();
	//Original name: FORMAT-ADDRESS-PARMS
	private Ts52901 ts52901 = new Ts52901();
	/**Original name: NI-CER-NBR<br>
	 * <pre>*  NULL-INDICATORS.</pre>*/
	private short niCerNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-REC-NM
	private short niRecNm = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-LIN-1-ADR
	private short niLin1Adr = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-LIN-2-ADR
	private short niLin2Adr = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-CIT-NM
	private short niCitNm = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-ST-ABB
	private short niStAbb = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-PST-CD
	private short niPstCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-FLAG
	private boolean swEndOfCursorFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b9020 workingStorageArea = new WorkingStorageAreaXz0b9020();
	//Original name: WS-XZ0A9020-ROW
	private WsXz0a9020Row wsXz0a9020Row = new WsXz0a9020Row();
	//Original name: WS-XZ0A9021-ROW
	private WsXz0a9021Row wsXz0a9021Row = new WsXz0a9021Row();
	//Original name: DCLACT-NOT-REC
	private DclactNotRec dclactNotRec = new DclactNotRec();
	//Original name: DCLACT-NOT-FRM-REC
	private DclactNotFrmRec dclactNotFrmRec = new DclactNotFrmRec();
	//Original name: DCLREC-TYP
	private DclrecTyp dclrecTyp = new DclrecTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: WS-NON-LOGGABLE-WARN-OR-ERR-SW
	private WsNonLoggableWarnOrErrSw wsNonLoggableWarnOrErrSw = new WsNonLoggableWarnOrErrSw();
	//Original name: WS-NONLOG-PLACEHOLDER-VALUES
	private WsNonlogPlaceholderValues wsNonlogPlaceholderValues = new WsNonlogPlaceholderValues();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();
	//Original name: DCLHAL-NLBE-WNG-TXT-V
	private DclhalNlbeWngTxtV dclhalNlbeWngTxtV = new DclhalNlbeWngTxtV();

	//==== METHODS ====
	public String getCfFormatAddressPgm() {
		return this.cfFormatAddressPgm;
	}

	public String getCfFormatAddressPgmFormatted() {
		return Functions.padBlanks(getCfFormatAddressPgm(), Len.CF_FORMAT_ADDRESS_PGM);
	}

	public void setNiCerNbr(short niCerNbr) {
		this.niCerNbr = niCerNbr;
	}

	public short getNiCerNbr() {
		return this.niCerNbr;
	}

	public short getNiRecNm() {
		return this.niRecNm;
	}

	public void setNiLin1Adr(short niLin1Adr) {
		this.niLin1Adr = niLin1Adr;
	}

	public short getNiLin1Adr() {
		return this.niLin1Adr;
	}

	public void setNiLin2Adr(short niLin2Adr) {
		this.niLin2Adr = niLin2Adr;
	}

	public short getNiLin2Adr() {
		return this.niLin2Adr;
	}

	public void setNiCitNm(short niCitNm) {
		this.niCitNm = niCitNm;
	}

	public short getNiCitNm() {
		return this.niCitNm;
	}

	public void setNiStAbb(short niStAbb) {
		this.niStAbb = niStAbb;
	}

	public short getNiStAbb() {
		return this.niStAbb;
	}

	public void setNiPstCd(short niPstCd) {
		this.niPstCd = niPstCd;
	}

	public short getNiPstCd() {
		return this.niPstCd;
	}

	public void setSwEndOfCursorFlag(boolean swEndOfCursorFlag) {
		this.swEndOfCursorFlag = swEndOfCursorFlag;
	}

	public boolean isSwEndOfCursorFlag() {
		return this.swEndOfCursorFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	/**Original name: WS-WARNING-UMT-AREA<br>
	 * <pre>* WARNING RESPONSE UMT MSG AREA</pre>*/
	public byte[] getWsWarningUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_WARNING_UMT_AREA];
		return getWsWarningUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsWarningUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		uwrnCommon.getUwrnCommonBytes(buffer, position);
		return buffer;
	}

	/**Original name: WS-NLBE-UMT-AREA<br>
	 * <pre>* NON-LOGGABLE BUS ERRS AREA</pre>*/
	public byte[] getWsNlbeUmtAreaBytes() {
		byte[] buffer = new byte[Len.WS_NLBE_UMT_AREA];
		return getWsNlbeUmtAreaBytes(buffer, 1);
	}

	public byte[] getWsNlbeUmtAreaBytes(byte[] buffer, int offset) {
		int position = offset;
		nlbeCommon.getNlbeCommonBytes(buffer, position);
		return buffer;
	}

	public ActNotRecTypXz0b9020 getActNotRecTypXz0b9020() {
		return actNotRecTypXz0b9020;
	}

	@Override
	public String getCerNbr() {
		return dclactNotRec.getCerNbr();
	}

	@Override
	public void setCerNbr(String cerNbr) {
		this.dclactNotRec.setCerNbr(cerNbr);
	}

	@Override
	public String getCerNbrObj() {
		if (getNiCerNbr() >= 0) {
			return getCerNbr();
		} else {
			return null;
		}
	}

	@Override
	public void setCerNbrObj(String cerNbrObj) {
		if (cerNbrObj != null) {
			setCerNbr(cerNbrObj);
			setNiCerNbr(((short) 0));
		} else {
			setNiCerNbr(((short) -1));
		}
	}

	@Override
	public String getCitNm() {
		return dclactNotRec.getCitNm();
	}

	@Override
	public void setCitNm(String citNm) {
		this.dclactNotRec.setCitNm(citNm);
	}

	@Override
	public String getCitNmObj() {
		if (getNiCitNm() >= 0) {
			return getCitNm();
		} else {
			return null;
		}
	}

	@Override
	public void setCitNmObj(String citNmObj) {
		if (citNmObj != null) {
			setCitNm(citNmObj);
			setNiCitNm(((short) 0));
		} else {
			setNiCitNm(((short) -1));
		}
	}

	public DclactNotFrmRec getDclactNotFrmRec() {
		return dclactNotFrmRec;
	}

	public DclactNotRec getDclactNotRec() {
		return dclactNotRec;
	}

	public DclhalNlbeWngTxtV getDclhalNlbeWngTxtV() {
		return dclhalNlbeWngTxtV;
	}

	public DclrecTyp getDclrecTyp() {
		return dclrecTyp;
	}

	public Ea02NothingFoundMsg getEa02NothingFoundMsg() {
		return ea02NothingFoundMsg;
	}

	public Ea03NothingFoundByNotMsg getEa03NothingFoundByNotMsg() {
		return ea03NothingFoundByNotMsg;
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	@Override
	public String getLin1Adr() {
		return dclactNotRec.getLin1Adr();
	}

	@Override
	public void setLin1Adr(String lin1Adr) {
		this.dclactNotRec.setLin1Adr(lin1Adr);
	}

	@Override
	public String getLin1AdrObj() {
		if (getNiLin1Adr() >= 0) {
			return getLin1Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin1AdrObj(String lin1AdrObj) {
		if (lin1AdrObj != null) {
			setLin1Adr(lin1AdrObj);
			setNiLin1Adr(((short) 0));
		} else {
			setNiLin1Adr(((short) -1));
		}
	}

	@Override
	public String getLin2Adr() {
		return dclactNotRec.getLin2Adr();
	}

	@Override
	public void setLin2Adr(String lin2Adr) {
		this.dclactNotRec.setLin2Adr(lin2Adr);
	}

	@Override
	public String getLin2AdrObj() {
		if (getNiLin2Adr() >= 0) {
			return getLin2Adr();
		} else {
			return null;
		}
	}

	@Override
	public void setLin2AdrObj(String lin2AdrObj) {
		if (lin2AdrObj != null) {
			setLin2Adr(lin2AdrObj);
			setNiLin2Adr(((short) 0));
		} else {
			setNiLin2Adr(((short) -1));
		}
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getPstCd() {
		return dclactNotRec.getPstCd();
	}

	@Override
	public void setPstCd(String pstCd) {
		this.dclactNotRec.setPstCd(pstCd);
	}

	@Override
	public String getPstCdObj() {
		if (getNiPstCd() >= 0) {
			return getPstCd();
		} else {
			return null;
		}
	}

	@Override
	public void setPstCdObj(String pstCdObj) {
		if (pstCdObj != null) {
			setPstCd(pstCdObj);
			setNiPstCd(((short) 0));
		} else {
			setNiPstCd(((short) -1));
		}
	}

	@Override
	public String getRecLngDes() {
		return dclrecTyp.getRecLngDes();
	}

	@Override
	public void setRecLngDes(String recLngDes) {
		this.dclrecTyp.setRecLngDes(recLngDes);
	}

	@Override
	public String getRecNm() {
		return FixedStrings.get(dclactNotRec.getRecNmText(), dclactNotRec.getRecNmLen());
	}

	@Override
	public void setRecNm(String recNm) {
		this.dclactNotRec.setRecNmText(recNm);
		this.dclactNotRec.setRecNmLen((((short) strLen(recNm))));
	}

	@Override
	public short getRecSeqNbr() {
		return dclactNotRec.getRecSeqNbr();
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.dclactNotRec.setRecSeqNbr(recSeqNbr);
	}

	@Override
	public short getRecSrOrdNbr() {
		return dclrecTyp.getRecSrOrdNbr();
	}

	@Override
	public void setRecSrOrdNbr(short recSrOrdNbr) {
		this.dclrecTyp.setRecSrOrdNbr(recSrOrdNbr);
	}

	@Override
	public String getRecTypCd() {
		return dclactNotRec.getRecTypCd();
	}

	@Override
	public void setRecTypCd(String recTypCd) {
		this.dclactNotRec.setRecTypCd(recTypCd);
	}

	@Override
	public String getStAbb() {
		return dclactNotRec.getStAbb();
	}

	@Override
	public void setStAbb(String stAbb) {
		this.dclactNotRec.setStAbb(stAbb);
	}

	@Override
	public String getStAbbObj() {
		if (getNiStAbb() >= 0) {
			return getStAbb();
		} else {
			return null;
		}
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		if (stAbbObj != null) {
			setStAbb(stAbbObj);
			setNiStAbb(((short) 0));
		} else {
			setNiStAbb(((short) -1));
		}
	}

	public Ts52901 getTs52901() {
		return ts52901;
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b9020 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNonLoggableWarnOrErrSw getWsNonLoggableWarnOrErrSw() {
		return wsNonLoggableWarnOrErrSw;
	}

	public WsNonlogPlaceholderValues getWsNonlogPlaceholderValues() {
		return wsNonlogPlaceholderValues;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a9020Row getWsXz0a9020Row() {
		return wsXz0a9020Row;
	}

	public WsXz0a9021Row getWsXz0a9021Row() {
		return wsXz0a9021Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;
		public static final int WS_NLBE_UMT_AREA = NlbeCommon.Len.NLBE_COMMON;
		public static final int WS_WARNING_UMT_AREA = UwrnCommon.Len.UWRN_COMMON;
		public static final int CF_FORMAT_ADDRESS_PGM = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
