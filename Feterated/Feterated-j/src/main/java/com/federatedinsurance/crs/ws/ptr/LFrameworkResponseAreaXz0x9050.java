/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.lang.types.AfDecimal;

/**Original name: L-FRAMEWORK-RESPONSE-AREA<br>
 * Variable: L-FRAMEWORK-RESPONSE-AREA from program XZ0X9050<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkResponseAreaXz0x9050 extends BytesClass {

	//==== PROPERTIES ====
	public static final int L_FW_RESP_XZ0A9051_MAXOCCURS = 100;
	public static final String XZA950R_GET_POL_LIST_BY_NOT = "GetPolicyListByNotification";
	public static final String XZA950R_GET_POL_LIST_BY_FRM = "GetPolicyListByForm";

	//==== CONSTRUCTORS ====
	public LFrameworkResponseAreaXz0x9050() {
	}

	public LFrameworkResponseAreaXz0x9050(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_RESPONSE_AREA;
	}

	public void setXza950rMaxPolRowsReturned(short xza950rMaxPolRowsReturned) {
		writeBinaryShort(Pos.XZA950R_MAX_POL_ROWS_RETURNED, xza950rMaxPolRowsReturned);
	}

	/**Original name: XZA950R-MAX-POL-ROWS-RETURNED<br>*/
	public short getXza950rMaxPolRowsReturned() {
		return readBinaryShort(Pos.XZA950R_MAX_POL_ROWS_RETURNED);
	}

	public void setXza950rOperation(String xza950rOperation) {
		writeString(Pos.XZA950R_OPERATION, xza950rOperation, Len.XZA950R_OPERATION);
	}

	/**Original name: XZA950R-OPERATION<br>*/
	public String getXza950rOperation() {
		return readString(Pos.XZA950R_OPERATION, Len.XZA950R_OPERATION);
	}

	public void setXza950rCsrActNbr(String xza950rCsrActNbr) {
		writeString(Pos.XZA950R_CSR_ACT_NBR, xza950rCsrActNbr, Len.XZA950R_CSR_ACT_NBR);
	}

	/**Original name: XZA950R-CSR-ACT-NBR<br>*/
	public String getXza950rCsrActNbr() {
		return readString(Pos.XZA950R_CSR_ACT_NBR, Len.XZA950R_CSR_ACT_NBR);
	}

	public void setXza950rNotPrcTs(String xza950rNotPrcTs) {
		writeString(Pos.XZA950R_NOT_PRC_TS, xza950rNotPrcTs, Len.XZA950R_NOT_PRC_TS);
	}

	/**Original name: XZA950R-NOT-PRC-TS<br>*/
	public String getXza950rNotPrcTs() {
		return readString(Pos.XZA950R_NOT_PRC_TS, Len.XZA950R_NOT_PRC_TS);
	}

	public void setXza950rFrmSeqNbr(int xza950rFrmSeqNbr) {
		writeInt(Pos.XZA950R_FRM_SEQ_NBR, xza950rFrmSeqNbr, Len.Int.XZA950R_FRM_SEQ_NBR);
	}

	/**Original name: XZA950R-FRM-SEQ-NBR<br>*/
	public int getXza950rFrmSeqNbr() {
		return readNumDispInt(Pos.XZA950R_FRM_SEQ_NBR, Len.XZA950R_FRM_SEQ_NBR);
	}

	public String getlFwRespXz0a9051Formatted(int lFwRespXz0a9051Idx) {
		int position = Pos.lFwRespXz0a9051(lFwRespXz0a9051Idx - 1);
		return readFixedString(position, Len.L_FW_RESP_XZ0A9051);
	}

	public void setXza951NinCltId(int xza951NinCltIdIdx, String xza951NinCltId) {
		int position = Pos.xza951NinCltId(xza951NinCltIdIdx - 1);
		writeString(position, xza951NinCltId, Len.XZA951_NIN_CLT_ID);
	}

	/**Original name: XZA951-NIN-CLT-ID<br>*/
	public String getXza951NinCltId(int xza951NinCltIdIdx) {
		int position = Pos.xza951NinCltId(xza951NinCltIdIdx - 1);
		return readString(position, Len.XZA951_NIN_CLT_ID);
	}

	public void setXza951NinAdrId(int xza951NinAdrIdIdx, String xza951NinAdrId) {
		int position = Pos.xza951NinAdrId(xza951NinAdrIdIdx - 1);
		writeString(position, xza951NinAdrId, Len.XZA951_NIN_ADR_ID);
	}

	/**Original name: XZA951-NIN-ADR-ID<br>*/
	public String getXza951NinAdrId(int xza951NinAdrIdIdx) {
		int position = Pos.xza951NinAdrId(xza951NinAdrIdIdx - 1);
		return readString(position, Len.XZA951_NIN_ADR_ID);
	}

	public void setXza951WfStartedInd(int xza951WfStartedIndIdx, char xza951WfStartedInd) {
		int position = Pos.xza951WfStartedInd(xza951WfStartedIndIdx - 1);
		writeChar(position, xza951WfStartedInd);
	}

	/**Original name: XZA951-WF-STARTED-IND<br>*/
	public char getXza951WfStartedInd(int xza951WfStartedIndIdx) {
		int position = Pos.xza951WfStartedInd(xza951WfStartedIndIdx - 1);
		return readChar(position);
	}

	public void setXza951PolBilStaCd(int xza951PolBilStaCdIdx, char xza951PolBilStaCd) {
		int position = Pos.xza951PolBilStaCd(xza951PolBilStaCdIdx - 1);
		writeChar(position, xza951PolBilStaCd);
	}

	/**Original name: XZA951-POL-BIL-STA-CD<br>*/
	public char getXza951PolBilStaCd(int xza951PolBilStaCdIdx) {
		int position = Pos.xza951PolBilStaCd(xza951PolBilStaCdIdx - 1);
		return readChar(position);
	}

	public void setXza951PolNbr(int xza951PolNbrIdx, String xza951PolNbr) {
		int position = Pos.xza951PolNbr(xza951PolNbrIdx - 1);
		writeString(position, xza951PolNbr, Len.XZA951_POL_NBR);
	}

	/**Original name: XZA951-POL-NBR<br>*/
	public String getXza951PolNbr(int xza951PolNbrIdx) {
		int position = Pos.xza951PolNbr(xza951PolNbrIdx - 1);
		return readString(position, Len.XZA951_POL_NBR);
	}

	public void setXza951PolTypCd(int xza951PolTypCdIdx, String xza951PolTypCd) {
		int position = Pos.xza951PolTypCd(xza951PolTypCdIdx - 1);
		writeString(position, xza951PolTypCd, Len.XZA951_POL_TYP_CD);
	}

	/**Original name: XZA951-POL-TYP-CD<br>*/
	public String getXza951PolTypCd(int xza951PolTypCdIdx) {
		int position = Pos.xza951PolTypCd(xza951PolTypCdIdx - 1);
		return readString(position, Len.XZA951_POL_TYP_CD);
	}

	public void setXza951PolTypDes(int xza951PolTypDesIdx, String xza951PolTypDes) {
		int position = Pos.xza951PolTypDes(xza951PolTypDesIdx - 1);
		writeString(position, xza951PolTypDes, Len.XZA951_POL_TYP_DES);
	}

	/**Original name: XZA951-POL-TYP-DES<br>*/
	public String getXza951PolTypDes(int xza951PolTypDesIdx) {
		int position = Pos.xza951PolTypDes(xza951PolTypDesIdx - 1);
		return readString(position, Len.XZA951_POL_TYP_DES);
	}

	public void setXza951PolPriRskStAbb(int xza951PolPriRskStAbbIdx, String xza951PolPriRskStAbb) {
		int position = Pos.xza951PolPriRskStAbb(xza951PolPriRskStAbbIdx - 1);
		writeString(position, xza951PolPriRskStAbb, Len.XZA951_POL_PRI_RSK_ST_ABB);
	}

	/**Original name: XZA951-POL-PRI-RSK-ST-ABB<br>*/
	public String getXza951PolPriRskStAbb(int xza951PolPriRskStAbbIdx) {
		int position = Pos.xza951PolPriRskStAbb(xza951PolPriRskStAbbIdx - 1);
		return readString(position, Len.XZA951_POL_PRI_RSK_ST_ABB);
	}

	public void setXza951NotEffDt(int xza951NotEffDtIdx, String xza951NotEffDt) {
		int position = Pos.xza951NotEffDt(xza951NotEffDtIdx - 1);
		writeString(position, xza951NotEffDt, Len.XZA951_NOT_EFF_DT);
	}

	/**Original name: XZA951-NOT-EFF-DT<br>*/
	public String getXza951NotEffDt(int xza951NotEffDtIdx) {
		int position = Pos.xza951NotEffDt(xza951NotEffDtIdx - 1);
		return readString(position, Len.XZA951_NOT_EFF_DT);
	}

	public void setXza951PolEffDt(int xza951PolEffDtIdx, String xza951PolEffDt) {
		int position = Pos.xza951PolEffDt(xza951PolEffDtIdx - 1);
		writeString(position, xza951PolEffDt, Len.XZA951_POL_EFF_DT);
	}

	/**Original name: XZA951-POL-EFF-DT<br>*/
	public String getXza951PolEffDt(int xza951PolEffDtIdx) {
		int position = Pos.xza951PolEffDt(xza951PolEffDtIdx - 1);
		return readString(position, Len.XZA951_POL_EFF_DT);
	}

	public void setXza951PolExpDt(int xza951PolExpDtIdx, String xza951PolExpDt) {
		int position = Pos.xza951PolExpDt(xza951PolExpDtIdx - 1);
		writeString(position, xza951PolExpDt, Len.XZA951_POL_EXP_DT);
	}

	/**Original name: XZA951-POL-EXP-DT<br>*/
	public String getXza951PolExpDt(int xza951PolExpDtIdx) {
		int position = Pos.xza951PolExpDt(xza951PolExpDtIdx - 1);
		return readString(position, Len.XZA951_POL_EXP_DT);
	}

	public void setXza951PolDueAmt(int xza951PolDueAmtIdx, AfDecimal xza951PolDueAmt) {
		int position = Pos.xza951PolDueAmt(xza951PolDueAmtIdx - 1);
		writeDecimal(position, xza951PolDueAmt.copy());
	}

	/**Original name: XZA951-POL-DUE-AMT<br>*/
	public AfDecimal getXza951PolDueAmt(int xza951PolDueAmtIdx) {
		int position = Pos.xza951PolDueAmt(xza951PolDueAmtIdx - 1);
		return readDecimal(position, Len.Int.XZA951_POL_DUE_AMT, Len.Fract.XZA951_POL_DUE_AMT);
	}

	public void setXza951MasterCompanyNbr(int xza951MasterCompanyNbrIdx, String xza951MasterCompanyNbr) {
		int position = Pos.xza951MasterCompanyNbr(xza951MasterCompanyNbrIdx - 1);
		writeString(position, xza951MasterCompanyNbr, Len.XZA951_MASTER_COMPANY_NBR);
	}

	/**Original name: XZA951-MASTER-COMPANY-NBR<br>*/
	public String getXza951MasterCompanyNbr(int xza951MasterCompanyNbrIdx) {
		int position = Pos.xza951MasterCompanyNbr(xza951MasterCompanyNbrIdx - 1);
		return readString(position, Len.XZA951_MASTER_COMPANY_NBR);
	}

	public void setXza951MasterCompanyDes(int xza951MasterCompanyDesIdx, String xza951MasterCompanyDes) {
		int position = Pos.xza951MasterCompanyDes(xza951MasterCompanyDesIdx - 1);
		writeString(position, xza951MasterCompanyDes, Len.XZA951_MASTER_COMPANY_DES);
	}

	/**Original name: XZA951-MASTER-COMPANY-DES<br>*/
	public String getXza951MasterCompanyDes(int xza951MasterCompanyDesIdx) {
		int position = Pos.xza951MasterCompanyDes(xza951MasterCompanyDesIdx - 1);
		return readString(position, Len.XZA951_MASTER_COMPANY_DES);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_RESPONSE_AREA = 1;
		public static final int L_FW_RESP_XZ0A9050 = L_FRAMEWORK_RESPONSE_AREA;
		public static final int XZA950R_POLICY_LIST_HEADER = L_FW_RESP_XZ0A9050;
		public static final int XZA950R_MAX_POL_ROWS_RETURNED = XZA950R_POLICY_LIST_HEADER;
		public static final int XZA950R_OPERATION = XZA950R_MAX_POL_ROWS_RETURNED + Len.XZA950R_MAX_POL_ROWS_RETURNED;
		public static final int XZA950R_CSR_ACT_NBR = XZA950R_OPERATION + Len.XZA950R_OPERATION;
		public static final int XZA950R_NOT_PRC_TS = XZA950R_CSR_ACT_NBR + Len.XZA950R_CSR_ACT_NBR;
		public static final int XZA950R_FRM_SEQ_NBR = XZA950R_NOT_PRC_TS + Len.XZA950R_NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int lFwRespXz0a9051(int idx) {
			return XZA950R_FRM_SEQ_NBR + Len.XZA950R_FRM_SEQ_NBR + idx * Len.L_FW_RESP_XZ0A9051;
		}

		public static int xza951PolicyListDetail(int idx) {
			return lFwRespXz0a9051(idx);
		}

		public static int xza951TechnicalKey(int idx) {
			return xza951PolicyListDetail(idx);
		}

		public static int xza951NinCltId(int idx) {
			return xza951TechnicalKey(idx);
		}

		public static int xza951NinAdrId(int idx) {
			return xza951NinCltId(idx) + Len.XZA951_NIN_CLT_ID;
		}

		public static int xza951WfStartedInd(int idx) {
			return xza951NinAdrId(idx) + Len.XZA951_NIN_ADR_ID;
		}

		public static int xza951PolBilStaCd(int idx) {
			return xza951WfStartedInd(idx) + Len.XZA951_WF_STARTED_IND;
		}

		public static int xza951PolNbr(int idx) {
			return xza951PolBilStaCd(idx) + Len.XZA951_POL_BIL_STA_CD;
		}

		public static int xza951PolicyType(int idx) {
			return xza951PolNbr(idx) + Len.XZA951_POL_NBR;
		}

		public static int xza951PolTypCd(int idx) {
			return xza951PolicyType(idx);
		}

		public static int xza951PolTypDes(int idx) {
			return xza951PolTypCd(idx) + Len.XZA951_POL_TYP_CD;
		}

		public static int xza951PolPriRskStAbb(int idx) {
			return xza951PolTypDes(idx) + Len.XZA951_POL_TYP_DES;
		}

		public static int xza951NotEffDt(int idx) {
			return xza951PolPriRskStAbb(idx) + Len.XZA951_POL_PRI_RSK_ST_ABB;
		}

		public static int xza951PolEffDt(int idx) {
			return xza951NotEffDt(idx) + Len.XZA951_NOT_EFF_DT;
		}

		public static int xza951PolExpDt(int idx) {
			return xza951PolEffDt(idx) + Len.XZA951_POL_EFF_DT;
		}

		public static int xza951PolDueAmt(int idx) {
			return xza951PolExpDt(idx) + Len.XZA951_POL_EXP_DT;
		}

		public static int xza951MasterCompany(int idx) {
			return xza951PolDueAmt(idx) + Len.XZA951_POL_DUE_AMT;
		}

		public static int xza951MasterCompanyNbr(int idx) {
			return xza951MasterCompany(idx);
		}

		public static int xza951MasterCompanyDes(int idx) {
			return xza951MasterCompanyNbr(idx) + Len.XZA951_MASTER_COMPANY_NBR;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA950R_MAX_POL_ROWS_RETURNED = 2;
		public static final int XZA950R_OPERATION = 32;
		public static final int XZA950R_CSR_ACT_NBR = 9;
		public static final int XZA950R_NOT_PRC_TS = 26;
		public static final int XZA950R_FRM_SEQ_NBR = 5;
		public static final int XZA951_NIN_CLT_ID = 64;
		public static final int XZA951_NIN_ADR_ID = 64;
		public static final int XZA951_WF_STARTED_IND = 1;
		public static final int XZA951_POL_BIL_STA_CD = 1;
		public static final int XZA951_TECHNICAL_KEY = XZA951_NIN_CLT_ID + XZA951_NIN_ADR_ID + XZA951_WF_STARTED_IND + XZA951_POL_BIL_STA_CD;
		public static final int XZA951_POL_NBR = 25;
		public static final int XZA951_POL_TYP_CD = 3;
		public static final int XZA951_POL_TYP_DES = 30;
		public static final int XZA951_POLICY_TYPE = XZA951_POL_TYP_CD + XZA951_POL_TYP_DES;
		public static final int XZA951_POL_PRI_RSK_ST_ABB = 2;
		public static final int XZA951_NOT_EFF_DT = 10;
		public static final int XZA951_POL_EFF_DT = 10;
		public static final int XZA951_POL_EXP_DT = 10;
		public static final int XZA951_POL_DUE_AMT = 10;
		public static final int XZA951_MASTER_COMPANY_NBR = 2;
		public static final int XZA951_MASTER_COMPANY_DES = 40;
		public static final int XZA951_MASTER_COMPANY = XZA951_MASTER_COMPANY_NBR + XZA951_MASTER_COMPANY_DES;
		public static final int XZA951_POLICY_LIST_DETAIL = XZA951_TECHNICAL_KEY + XZA951_POL_NBR + XZA951_POLICY_TYPE + XZA951_POL_PRI_RSK_ST_ABB
				+ XZA951_NOT_EFF_DT + XZA951_POL_EFF_DT + XZA951_POL_EXP_DT + XZA951_POL_DUE_AMT + XZA951_MASTER_COMPANY;
		public static final int L_FW_RESP_XZ0A9051 = XZA951_POLICY_LIST_DETAIL;
		public static final int XZA950R_POLICY_LIST_HEADER = XZA950R_MAX_POL_ROWS_RETURNED + XZA950R_OPERATION + XZA950R_CSR_ACT_NBR
				+ XZA950R_NOT_PRC_TS + XZA950R_FRM_SEQ_NBR;
		public static final int L_FW_RESP_XZ0A9050 = XZA950R_POLICY_LIST_HEADER;
		public static final int L_FRAMEWORK_RESPONSE_AREA = L_FW_RESP_XZ0A9050
				+ LFrameworkResponseAreaXz0x9050.L_FW_RESP_XZ0A9051_MAXOCCURS * L_FW_RESP_XZ0A9051;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA950R_FRM_SEQ_NBR = 5;
			public static final int XZA951_POL_DUE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int XZA951_POL_DUE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
