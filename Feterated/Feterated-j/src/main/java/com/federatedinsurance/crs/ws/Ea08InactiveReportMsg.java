/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-08-INACTIVE-REPORT-MSG<br>
 * Variable: EA-08-INACTIVE-REPORT-MSG from program TS030099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea08InactiveReportMsg {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG
	private char flr1 = Types.SPACE_CHAR;
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG-1
	private String flr2 = "TS030099 -";
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG-2
	private String flr3 = "WARNING:";
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG-3
	private String flr4 = "INACTIVE REPORT";
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG-4
	private String flr5 = " USED.";
	//Original name: FILLER-EA-08-INACTIVE-REPORT-MSG-5
	private String flr6 = "REPORT:";
	//Original name: EA-08-REPORT-NUMBER
	private String ea08ReportNumber = DefaultValues.stringVal(Len.EA08_REPORT_NUMBER);

	//==== METHODS ====
	public String getEa08InactiveReportMsgFormatted() {
		return MarshalByteExt.bufferToStr(getEa08InactiveReportMsgBytes());
	}

	public byte[] getEa08InactiveReportMsgBytes() {
		byte[] buffer = new byte[Len.EA08_INACTIVE_REPORT_MSG];
		return getEa08InactiveReportMsgBytes(buffer, 1);
	}

	public byte[] getEa08InactiveReportMsgBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		position += Len.FLR4;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, ea08ReportNumber, Len.EA08_REPORT_NUMBER);
		return buffer;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setEa08ReportNumber(String ea08ReportNumber) {
		this.ea08ReportNumber = Functions.subString(ea08ReportNumber, Len.EA08_REPORT_NUMBER);
	}

	public String getEa08ReportNumber() {
		return this.ea08ReportNumber;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EA08_REPORT_NUMBER = 6;
		public static final int FLR1 = 1;
		public static final int FLR2 = 11;
		public static final int FLR3 = 9;
		public static final int FLR4 = 15;
		public static final int FLR5 = 8;
		public static final int EA08_INACTIVE_REPORT_MSG = EA08_REPORT_NUMBER + FLR1 + FLR2 + FLR3 + FLR4 + 2 * FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
