/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: SAVE-AREA<br>
 * Variable: SAVE-AREA from program TS529099<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class SaveAreaTs529099 {

	//==== PROPERTIES ====
	//Original name: SA-SPACE-COUNT
	private short spaceCount = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-BREAK-LEN
	private short breakLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-CITY-LEN
	private short cityLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-ST-LEN
	private short stLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-FORMATTED-LINE-LEN
	private short formattedLineLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: SA-DISPLAY-NAME-LEN
	private short displayNameLen = DefaultValues.BIN_SHORT_VAL;
	/**Original name: SA-CITY-STATE-ZIP-LINE<br>
	 * <pre>    THIS LINE WILL NEED TO BE DEFINED AT LEAST THE COMBINED SIZE
	 *       OF THE CITY, STATE, ZIP, AND ALL DELIMITERS FIELDS.</pre>*/
	private String cityStateZipLine = DefaultValues.stringVal(Len.CITY_STATE_ZIP_LINE);

	//==== METHODS ====
	public void setSpaceCount(short spaceCount) {
		this.spaceCount = spaceCount;
	}

	public short getSpaceCount() {
		return this.spaceCount;
	}

	public void setBreakLen(short breakLen) {
		this.breakLen = breakLen;
	}

	public short getBreakLen() {
		return this.breakLen;
	}

	public void setCityLen(short cityLen) {
		this.cityLen = cityLen;
	}

	public short getCityLen() {
		return this.cityLen;
	}

	public void setStLen(short stLen) {
		this.stLen = stLen;
	}

	public short getStLen() {
		return this.stLen;
	}

	public void setFormattedLineLen(short formattedLineLen) {
		this.formattedLineLen = formattedLineLen;
	}

	public short getFormattedLineLen() {
		return this.formattedLineLen;
	}

	public void setDisplayNameLen(short displayNameLen) {
		this.displayNameLen = displayNameLen;
	}

	public short getDisplayNameLen() {
		return this.displayNameLen;
	}

	public void setCityStateZipLine(String cityStateZipLine) {
		this.cityStateZipLine = Functions.subString(cityStateZipLine, Len.CITY_STATE_ZIP_LINE);
	}

	public String getCityStateZipLine() {
		return this.cityStateZipLine;
	}

	public String getCityStateZipLineFormatted() {
		return Functions.padBlanks(getCityStateZipLine(), Len.CITY_STATE_ZIP_LINE);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CITY_STATE_ZIP_LINE = 100;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
