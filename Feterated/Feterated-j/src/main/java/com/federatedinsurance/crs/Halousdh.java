/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.storage.KeyType;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.tp.storage.IRowDAO;
import com.bphx.ctu.af.tp.storage.IRowData;
import com.bphx.ctu.af.tp.storage.RowDAOFactory;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.commons.data.dao.HalBoMduXrfVUowObjHierPrcSeqDao;
import com.federatedinsurance.crs.commons.data.dao.HalScrnScriptVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUniversalCtVDao;
import com.federatedinsurance.crs.commons.data.dao.HalUowFunSecVDao;
import com.federatedinsurance.crs.copy.Halluhdr;
import com.federatedinsurance.crs.copy.Hallusw;
import com.federatedinsurance.crs.copy.HufscHalUowFunSecRow;
import com.federatedinsurance.crs.copy.Sqlca;
import com.federatedinsurance.crs.copy.UrqmCommon;
import com.federatedinsurance.crs.ws.DfhcommareaHalouieh;
import com.federatedinsurance.crs.ws.HalousdhData;
import com.federatedinsurance.crs.ws.UbocCommInfoCaws002;
import com.federatedinsurance.crs.ws.WsEstoInfo;
import com.federatedinsurance.crs.ws.WsTsq2DataArea;
import com.federatedinsurance.crs.ws.enums.UbocPassThruAction;
import com.federatedinsurance.crs.ws.redefines.CidpTableInfo;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: HALOUSDH<br>
 * <pre>AUTHOR.  CSC.
 * DATE-WRITTEN. MAY 2001.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE -  SET DEFAULT HUB (DYNAMIC ENTITLEMENT)      **
 * *                 -THIS PROGRAM IS USED TO CALL ALL OF THE    **
 * *                  DATA PRIVACY MODULES WITHIN THE UOW BEING  **
 * *                  EXECUTED WITH AN ACTIVITY TYPE OF          **
 * *                  'SET_DEFAULTS'.                            **
 * *                 -THE DP MODULES WILL THEN APPLY THE DATA    **
 * *                  PRIVACY RULES TO 'DUMMY' RECORDS AND WRITE **
 * *                  THEM OUT TO THE HEADER AND DATA UMT'S.     **
 * *                                                             **
 * * PLATFORM - I-BASE                                           **
 * *                                                             **
 * * OPERATING SYSTEM - MVS                                      **
 * *                                                             **
 * * LANGUAGE - COBOL                                            **
 * *                                                             **
 * * PURPOSE -                                                   **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS PROGRAM IS STARTED IN THE FOLLOW-**
 * *                       ING WAYS:                             **
 * *                                                             **
 * *                       1)LINK FROM THE MESSAGE CONTROL       **
 * *                         MODULE (HALOMCM).                   **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #      DATE      PROG    DESCRIPTION                     **
 * * --------  --------  ------  --------------------------------**
 * * ARCH2.3   10MAY01   07077   NEW PROGRAM.                    **
 * * 32052     02MAY03   07077  -PERFORMANCE ENHANCEMENT - ADDED **
 * *                             TSQ LOGIC TO REDUCE TCB SWITCHING*
 * *                            -PROGRAM ESSENTIALLY RE-WRITTEN   *
 * * 31753     21APR03   18448  -SCRIPTING SUPPORT                *
 * * 31753A    19MAY03   18448  -FURTHER CLEANUP OF PROGRAM.  ADD *
 * *                             FLOWER BOXES TO ALL SECTIONS.    *
 * *                             RENUMBER SECTIONS SO THAT THEY   *
 * *                             ARE CALLED FROM THE MAINLINE     *
 * *                             SECTION IN SEQUENCE.  PHYSICALLY *
 * *                             MOVE THE SECTIONS SO THAT THEY   *
 * *                             ARE IN THE PROGRAM IN THE PROPER *
 * *                             ORDER.  REMOVE UNUSED PROCESSING.*
 * * 34103     11AUG03   18448   USE SWITCHES TSQ, IF AVAILABLE.  *
 * ****************************************************************</pre>*/
public class Halousdh extends Program {

	//==== PROPERTIES ====
	/**Original name: SQLCA<br>
	 * <pre>* DCLGEN USED FOR ACCESSING DATA SECURITY.
	 * *   EXEC SQL
	 * *       INCLUDE HALLGDPS
	 * *   END-EXEC.
	 * *****************************************************************
	 * **CSC *  START OF:                          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**
	 * **CSC *  (NO IAP REFERENCES)                *BUSINESS FRAMEWORK**
	 * **CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**
	 * **CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * **CSC *  VERSION 1.0 OCT. 16, 2001          *BUSINESS FRAMEWORK**
	 * **CSC *  CASE 17241  PRGMR AICI448          *BUSINESS FRAMEWORK**
	 * **CSC *                                     *BUSINESS FRAMEWORK**
	 * *****************************************************************
	 * * SQL AREAS
	 *        05  CIDP-TABLE-NAME                PIC X(18).
	 *        05  CIDP-TABLE-ROW                 PIC X(4982).</pre>*/
	private Sqlca sqlca = new Sqlca();
	private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
	private HalBoMduXrfVUowObjHierPrcSeqDao halBoMduXrfVUowObjHierPrcSeqDao = new HalBoMduXrfVUowObjHierPrcSeqDao(dbAccessStatus);
	private HalUowFunSecVDao halUowFunSecVDao = new HalUowFunSecVDao(dbAccessStatus);
	private HalUniversalCtVDao halUniversalCtVDao = new HalUniversalCtVDao(dbAccessStatus);
	private HalScrnScriptVDao halScrnScriptVDao = new HalScrnScriptVDao(dbAccessStatus);
	//Original name: WORKING-STORAGE
	private HalousdhData ws = new HalousdhData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHalouieh dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHalouieh dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		main1();
		returnToFrontEnd();
		return 0;
	}

	public static Halousdh getInstance() {
		return (Programs.getInstance(Halousdh.class));
	}

	/**Original name: 0000-MAIN_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *   THE 0000-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING THE    *
	 *   PROCESSING OF THE FUNCTION PASSED TO IT.                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void main1() {
		// COB_CODE: PERFORM 0100-INITIALIZE.
		initialize();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// READ REQUEST UMT FOR PRESENCE OF A CONTEXT REQUEST
		// COB_CODE: PERFORM 1000-READ-URQM-CTX-MSG.
		readUrqmCtxMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 2000-UNLOAD-CSR1 WILL FETCH ALL OF CURSOR1 AND LOAD THE DATA
		// INTO TSQ1. TSQ1 WILL BE PROCESSED AFTER ALL DB2 READS HAVE BEEN
		// COMPLETED, INCLUDING FETCHING ALL DATA FROM CURSOR 2.
		// COB_CODE: PERFORM 2000-UNLOAD-CSR1.
		unloadCsr1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 2100-UNLOAD-CSR2 WILL FETCH ALL OF CURSOR1 AND LOAD THE DATA
		// INTO TSQ2. TSQ2 WILL BE PROCESSED AFTER TSQ1.
		// COB_CODE: PERFORM 2100-UNLOAD-CSR2.
		unloadCsr2();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 2200-UNLOAD-CSR3 WILL FETCH ALL OF THE SCRIPT ROWS AND LOAD THEM
		// INTO TSQ3. TSQ3 WILL BE PROCESSED AFTER TSQ2.
		// COB_CODE: PERFORM 2200-UNLOAD-CSR3.
		unloadCsr3();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// READ REQUEST UMT FOR PRESENCE OF A SET_DEFAULT REQUEST
		// COB_CODE: PERFORM 3000-READ-REQ-MSG-UMT.
		readReqMsgUmt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 4000-EXECUTE-TSQ1-LOOP WILL READ THROUGH TSQ1 (WHICH NOW
		// CONTAINS THE RESULTS OF CURSOR1), AND WILL CALL THE APPROPRIATE
		// BUSINESS DATA PRIVACY OBJECTS AND WRITE THE CORRESPONDING SET
		// DEFAULT ROWS TO THE HALFUDAT UMT.
		// COB_CODE: PERFORM 4000-EXECUTE-TSQ1-LOOP.
		executeTsq1Loop();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 4100-EXECUTE-TSQ2-LOOP WILL READ THROUGH TSQ2 (WHICH NOW
		// CONTAINS THE RESULTS OF CURSOR2), AND WILL WRITE THE FETCHED
		// FUNCTION SECURITY ROWS TO THE HALFUDAT UMT.
		// COB_CODE: PERFORM 4100-EXECUTE-TSQ2-LOOP.
		executeTsq2Loop();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
		// 4200-EXECUTE-TSQ3-LOOP WILL READ THROUGH TSQ3 (WHICH NOW
		// CONTAINS THE RETRIEVED SCRIPTS), AND WILL WRITE THE FETCHED
		// ROWS TO THE HALFUDAT UMT.
		// COB_CODE: PERFORM 4200-EXECUTE-TSQ3-LOOP.
		executeTsq3Loop();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0000-RETURN-TO-FRONT-END
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
			returnToFrontEnd();
		}
	}

	/**Original name: 0000-RETURN-TO-FRONT-END<br>*/
	private void returnToFrontEnd() {
		// COB_CODE: PERFORM 8000-DELETE-QUEUES.
		deleteQueues();
		// COB_CODE: EXEC CICS
		//                RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0100-INITIALIZE_FIRST_SENTENCES<br>
	 * <pre>***************************************************************
	 *  A) INITIALIZE THE UMT RECORD SEQ NBRS OF THE UOW RESPONSE
	 *     UMTS TO ZERO. THEY WILL BE INCREMENTED WHEN WRITTEN TO.
	 *  B) SET 'NO ERRORS' TO TRUE.
	 *  C) RETRIEVE THE SERIES 3 DATE FOR USE BY THE CURSORS.
	 *  D) SET DUMMY CHARACTERS USED TO DETERMINE IF FRONT-END HAS
	 *     CHANGED VALUE IN A FIELD.
	 *  E) PERFORM ANY OTHER BDO SPECIFIC INITIALIZATION.
	 * ***************************************************************
	 * **  MOVE ZERO TO UDAT-REC-SEQ.
	 * **  SET WS-RESP-DATA-ROWS-NOT-WRIT TO TRUE.
	 * **  SET WS-FIRST-RESP-DATA-REC TO TRUE.
	 * **  SET WS-IUD-NOT-PROCESSED TO TRUE.
	 * ** MOVED FROM THE MAINLINE SECTION TO THIS SECTION.</pre>*/
	private void initialize() {
		// COB_CODE: PERFORM IWAE-INITIALIZE-WARN-MSG.
		iwaeInitializeWarnMsg();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0100-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0100-INITIALIZE-X
			return;
		}
		// COB_CODE: PERFORM VCOM-VALIDATE-COMMAREA.
		vcomValidateCommarea();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0100-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0100-INITIALIZE-X
			return;
		}
		//** END OF MOVE
		// COB_CODE: SET REQ-UMT-DFLT-REC-NOT-FND TO TRUE.
		ws.getWsSwitches().getReqUmtDfltRecSw().setNotFnd();
		// COB_CODE: MOVE 'N' TO WS-MASTER-SWITCH-IND.
		ws.getWsBdoSwitches().getMasterSwitchInd().setMasterSwitchIndFormatted("N");
		// COB_CODE: MOVE 'N' TO WS-READ-FOR-MSTR-SWT-IND.
		ws.getWsBdoSwitches().getReadForMstrSwtInd().setReadForMstrSwtIndFormatted("N");
		// COB_CODE: MOVE SPACES TO WS-DATA-UMT-AREA.
		ws.initWsDataUmtAreaSpaces();
		// COB_CODE: MOVE SPACES TO WS-HDR-UMT-AREA.
		ws.initWsHdrUmtAreaSpaces();
		// COB_CODE: MOVE UBOC-AUTH-USERID TO WS-AUTH-USERID-FOR-CURSOR.
		ws.getWsWorkFields().setWsAuthUseridForCursor(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: PERFORM 0150-GEN-TSQ-NAMES.
		genTsqNames();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 0100-INITIALIZE-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0100-INITIALIZE-X
			return;
		}
	}

	/**Original name: 0150-GEN-TSQ-NAMES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  CREATE TSQ NAMES FOR QUEUES USED TO STORE INFO FETCHED FROM
	 *  FUN SEC, SCRIPTING, AND HIERARCHY CURSORS.
	 * *****************************************************************</pre>*/
	private void genTsqNames() {
		ConcatUtil concatUtil = null;
		// COB_CODE: MOVE 1 TO UIDG-UNIT-NBR.
		ws.getHalluidg().setUidgUnitNbr(((short) 1));
		// COB_CODE: SET UIDG-RANDOM-GLOBAL-ID TO TRUE.
		ws.getHalluidg().getUidgIdType().setUidgRandomGlobalId();
		// COB_CODE: MOVE LENGTH OF HALOUIDG-LINKAGE
		//                TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HalousdhData.Len.HALOUIDG_LINKAGE));
		// COB_CODE: MOVE HALOUIDG-LINKAGE TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer(ws.getHalouidgLinkageFormatted());
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM  ('HALOUIDG')
		//                COMMAREA (UBOC-COMM-INFO)
		//                LENGTH   (LENGTH OF UBOC-COMM-INFO)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUSDH", execContext).commarea(dfhcommarea.getCommInfo()).length(UbocCommInfoCaws002.Len.COMM_INFO).link("HALOUIDG",
				new Halouidg());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 0150-GEN-TSQ-NAMES-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR       TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK     TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE 'HALOUIDG'
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HALOUIDG");
			// COB_CODE: MOVE '0150-GEN-TSQ-NAMES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("0150-GEN-TSQ-NAMES");
			// COB_CODE: MOVE 'ERROR LINKING TO HALOUIDG'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ERROR LINKING TO HALOUIDG");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'ID TYPE: '
			//               UIDG-ID-TYPE
			//             DELIMITED BY SIZE
			//             INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey("ID TYPE: " + ws.getHalluidg().getUidgIdType().getUidgIdType());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 0150-GEN-TSQ-NAMES-X
			return;
		}
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 0150-GEN-TSQ-NAMES-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 0150-GEN-TSQ-NAMES-X
			return;
		}
		// COB_CODE: MOVE UBOC-APP-DATA-BUFFER TO HALOUIDG-LINKAGE.
		ws.setHalouidgLinkageFormatted(dfhcommarea.getAppDataBufferFormatted());
		// COB_CODE: STRING EIBTRMID
		//                  UIDG-GENERATED-ID
		//             DELIMITED BY SIZE
		//             INTO WS-TASKNBR-TSQ1
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(HalousdhData.Len.WS_TASKNBR_TSQ1, execContext.getFormatted().getTerminalId(),
				ws.getHalluidg().getUidgErrorIdOutput().getUidgGeneratedIdFormatted());
		ws.setWsTasknbrTsq1(concatUtil.replaceInString(ws.getWsTasknbrTsq1Formatted()));
		// COB_CODE: MOVE WS-TASKNBR-TSQ1 TO WS-TASKNBR-TSQ2
		//                                   WS-TASKNBR-TSQ3.
		ws.setWsTasknbrTsq2(ws.getWsTasknbrTsq1());
		ws.setWsTasknbrTsq3(ws.getWsTasknbrTsq1());
		// COB_CODE: MOVE SPACES TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer("");
		// COB_CODE: MOVE 1 TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) 1));
	}

	/**Original name: 1000-READ-URQM-CTX-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THE REQUEST UMT FOR A CONTEXT MESSAGE                     *
	 *                                                                 *
	 *  ONLY ONE ROW FOR 'DATA_PRIVACY_CONTEXT' SHOULD BE FOUND.       *
	 *  THE CONTEXT WILL THEN BE USED TO READ THE HAL_UOW_FUN_SEC TABLE*
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readUrqmCtxMsg() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: MOVE UBOC-MSG-ID          TO URQM-ID.
		ws.getUrqmCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-DATA-PRIV-CTX-LIT TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(ws.getWsSpecificWorkAreas().getDataPrivCtxLit());
		// COB_CODE: MOVE 1                    TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(1);
		// COB_CODE: MOVE SPACES  TO URQM-UOW-MESSAGE-BUFFER.
		ws.getUrqmCommon().initUowMessageBufferSpaces();
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF NORMAL, MOVE CONTEXT NAME TO UBOC-SECURITY-CONTEXT
		//* IF NOTFND, SET UBOC-NO-CONTEXT TO TRUE
		//* OTHERWISE WE HAVE AN UNEXPECTED ERROR.
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 1000-READ-URQM-CTX-MSG-X
		//               WHEN OTHER
		//                   GO TO 1000-READ-URQM-CTX-MSG-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET UBOC-NO-CONTEXT TO TRUE
			dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().setNoContext();
			// COB_CODE: GO TO 1000-READ-URQM-CTX-MSG-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '1000-READ-URQM-CTX-MSG'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("1000-READ-URQM-CTX-MSG");
			// COB_CODE: MOVE 'READ REQ MSG STORE FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ MSG STORE FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'URQM-KEY=' URQM-KEY ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("URQM-KEY=").append(ws.getUrqmCommon().getKeyFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 1000-READ-URQM-CTX-MSG-X
			return;
		}
		// COB_CODE: MOVE URQM-MSG-DATA(1:URQM-BUS-OBJ-DATA-LENGTH)
		//               TO HCTXC-CONTEXT-INFO.
		ws.getHallcctx().setInfoFormatted(ws.getUrqmCommon().getMsgDataFormatted().substring((1) - 1, ws.getUrqmCommon().getBusObjDataLength()));
		// COB_CODE: MOVE HCTXC-CONTEXT TO UBOC-SECURITY-CONTEXT.
		dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().setSecurityContext(ws.getHallcctx().getT());
	}

	/**Original name: 2000-UNLOAD-CSR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  FETCH CURSOR UNTIL THERE ARE NO MORE ROWS ON              *
	 *      HAL_BO_MDU_XRF_V  OR UNTIL THERE IS A LOGGABLE ERROR      *
	 * ****************************************************************</pre>*/
	private void unloadCsr1() {
		// COB_CODE: PERFORM 2010-OPEN-CURSOR1.
		openCursor1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-UNLOAD-CSR1-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-UNLOAD-CSR1-X
			return;
		}
		// COB_CODE: PERFORM 2020-COPY-CSR1-TO-TSQ1
		//             UNTIL UBOC-UOW-LOGGABLE-ERRORS
		//                OR UBOC-HALT-AND-RETURN
		//                OR WS-END-OF-CURSOR1.
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getEndOfCursor1Sw().isEndOfCursor1())) {
			copyCsr1ToTsq1();
		}
		// COB_CODE: PERFORM 2030-CLOSE-CURSOR1.
		closeCursor1();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2000-UNLOAD-CSR1-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2000-UNLOAD-CSR1-X
			return;
		}
	}

	/**Original name: 2010-OPEN-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  INITIALISE CURSOR SWITCH                                  *
	 *  B>  MOVE UOW NAME AND DATA PROCESS BRANCH TYPE TO             *
	 *      DCLGEN FOR CURSOR1 CURSOR                                 *
	 *  C>  OPEN CURSOR1                                              *
	 * ****************************************************************</pre>*/
	private void openCursor1() {
		// COB_CODE: SET WS-NOT-END-OF-CURSOR1 TO TRUE.
		ws.getWsBdoSwitches().getEndOfCursor1Sw().setNotEndOfCursor1();
		// COB_CODE: MOVE UBOC-UOW-NAME TO HUPS-UOW-NM
		//                                 HUOH-UOW-NM.
		ws.getDclhalUowPrcSeq().setHupsUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		ws.getDclhalUowObjHierV().setUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-PRIMARY-BUS-OBJ TO HUOH-ROOT-BOBJ-NM.
		ws.getDclhalUowObjHierV().setRootBobjNm(dfhcommarea.getCommInfo().getUbocPrimaryBusObj());
		// COB_CODE: EXEC SQL
		//               OPEN CURSOR1
		//           END-EXEC.
		halBoMduXrfVUowObjHierPrcSeqDao.openCursor1(ws.getDclhalUowPrcSeq().getHupsUowNm(), ws.getDclhalUowObjHierV().getUowNm());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 2010-OPEN-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF_V'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF_V");
			// COB_CODE: MOVE '2010-OPEN-CURSOR1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2010-OPEN-CURSOR1");
			// COB_CODE: MOVE 'OPEN CUR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CUR FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HUPS-UOW-NM      = '    HUPS-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(new StringBuffer(256).append("HUPS-UOW-NM      = ")
					.append(ws.getDclhalUowPrcSeq().getHupsUowNmFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2010-OPEN-CURSOR1-X
			return;
		}
	}

	/**Original name: 2020-COPY-CSR1-TO-TSQ1_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  COPY RETRIEVED ROWS TO INTERMEDIATE STORAGE IN THE QUEUE.
	 * *****************************************************************</pre>*/
	private void copyCsr1ToTsq1() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC SQL
		//               FETCH CURSOR1
		//               INTO :WS-CUR1-BOBJ-NM,
		//                    :HBMX-DP-DFL-MDU-NM
		//           END-EXEC.
		halBoMduXrfVUowObjHierPrcSeqDao.fetchCursor1(ws);
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    GO TO 2020-COPY-CSR1-TO-TSQ1-X
		//               WHEN OTHER
		//                    GO TO 2020-COPY-CSR1-TO-TSQ1-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR1 TO TRUE
			ws.getWsBdoSwitches().getEndOfCursor1Sw().setEndOfCursor1();
			// COB_CODE: GO TO 2020-COPY-CSR1-TO-TSQ1-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF_V");
			// COB_CODE: MOVE '2020-COPY-CSR1-TO-TSQ1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2020-COPY-CSR1-TO-TSQ1");
			// COB_CODE: MOVE 'FETCH CUR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CUR FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'WS-CUR1-BOBJ-NM  = ' WS-CUR1-BOBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(new StringBuffer(256).append("WS-CUR1-BOBJ-NM  = ")
					.append(ws.getWsWorkFields().getWsCur1BobjNmFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2020-COPY-CSR1-TO-TSQ1-X
			return;
		}
		// COB_CODE: MOVE SPACES             TO WS-TSQ1-DATA-AREA.
		ws.initWsTsq1DataAreaSpaces();
		// COB_CODE: MOVE WS-CUR1-BOBJ-NM    TO WS-TSQ1-BOBJ-NM.
		ws.setWsTsq1BobjNm(ws.getWsWorkFields().getWsCur1BobjNm());
		// COB_CODE: MOVE HBMX-DP-DFL-MDU-NM TO WS-TSQ1-DFL-MDU-NM.
		ws.setWsTsq1DflMduNm(ws.getDclhalBoMduXrfV().getDpDflMduNm());
		// COB_CODE: EXEC CICS
		//               WRITEQ TS MAIN
		//               QNAME  (WS-TSQ1-QUEUE-NAME)
		//               FROM   (WS-TSQ1-DATA-AREA)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalousdhData.Len.WS_TSQ1_DATA_AREA);
		tsQueueData.setData(ws.getWsTsq1DataAreaBytes());
		TsQueueManager.insert(execContext, ws.getWsTsq1QueueNameFormatted(), tsQueueData);
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2020-COPY-CSR1-TO-TSQ1-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE WS-TSQ1-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq1QueueNameFormatted());
			// COB_CODE: MOVE '2020-COPY-CSR1-TO-TSQ1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2020-COPY-CSR1-TO-TSQ1");
			// COB_CODE: MOVE 'WRITE OF HALOUSDH TSQ1 FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE OF HALOUSDH TSQ1 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2020-COPY-CSR1-TO-TSQ1-X
			return;
		}
	}

	/**Original name: 2030-CLOSE-CURSOR1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  CLOSE CURSOR                                              *
	 * ****************************************************************</pre>*/
	private void closeCursor1() {
		// COB_CODE: EXEC SQL
		//               CLOSE CURSOR1
		//           END-EXEC.
		halBoMduXrfVUowObjHierPrcSeqDao.closeCursor1();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 2030-CLOSE-CURSOR1-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_BO_MDU_XRF_V'      TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_BO_MDU_XRF_V");
			// COB_CODE: MOVE '2030-CLOSE-CURSOR1'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2030-CLOSE-CURSOR1");
			// COB_CODE: MOVE 'CLOSE CUR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CUR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2030-CLOSE-CURSOR1-X
			return;
		}
	}

	/**Original name: 2100-UNLOAD-CSR2_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  RETRIEVE THE ALLOWABLE FUNCTIONS FOR THE USER'S SECURITY LEVEL.
	 * ****************************************************************</pre>*/
	private void unloadCsr2() {
		// COB_CODE: PERFORM 2110-OPEN-CURSOR2.
		openCursor2();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2100-UNLOAD-CSR2-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2100-UNLOAD-CSR2-X
			return;
		}
		// COB_CODE: PERFORM 2120-COPY-CSR2-TO-TSQ2
		//             UNTIL UBOC-UOW-LOGGABLE-ERRORS
		//                OR UBOC-HALT-AND-RETURN
		//                OR WS-END-OF-CURSOR2.
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getEndOfCursor2Sw().isEndOfCursor2())) {
			copyCsr2ToTsq2();
		}
		// COB_CODE: PERFORM 2130-CLOSE-CURSOR2.
		closeCursor2();
	}

	/**Original name: 2110-OPEN-CURSOR2_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  OPEN UOW FUNCTIONS CURSOR.
	 * ****************************************************************</pre>*/
	private void openCursor2() {
		// COB_CODE: SET WS-NOT-END-OF-CURSOR2 TO TRUE.
		ws.getWsBdoSwitches().getEndOfCursor2Sw().setNotEndOfCursor2();
		// COB_CODE: MOVE UBOC-UOW-NAME TO HUFSH-UOW-NM.
		ws.getHufshUowFunSecRow().setUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-SEC-GROUP-NAME TO HUFSH-SEC-GRP-NM.
		ws.getHufshUowFunSecRow().setSecGrpNm(dfhcommarea.getCommInfo().getUbocSecGroupName());
		// COB_CODE: IF UBOC-NO-CONTEXT
		//              MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
		//           ELSE
		//              MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().isNoContext()) {
			// COB_CODE: MOVE SPACES TO HUFSH-CONTEXT
			//                          WS-FULL-CONTEXT
			ws.getHufshUowFunSecRow().setContext("");
			ws.getWsWorkFields().getWsContextRedef().setWsFullContext("");
			// COB_CODE: MOVE SPACES TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr("");
			// COB_CODE: MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr(ws.getWsWorkFields().getWsContextRedef().getContextFirst10());
		} else {
			// COB_CODE: MOVE UBOC-SECURITY-CONTEXT TO HUFSH-CONTEXT
			//                                         WS-FULL-CONTEXT
			ws.getHufshUowFunSecRow().setContext(dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext());
			ws.getWsWorkFields().getWsContextRedef().setWsFullContext(dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext());
			// COB_CODE: MOVE SPACES TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr("");
			// COB_CODE: MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr(ws.getWsWorkFields().getWsContextRedef().getContextFirst10());
		}
		// COB_CODE: MOVE UBOC-SEC-ASSOCIATION-TYPE TO HUFSH-SEC-ASC-TYP.
		ws.getHufshUowFunSecRow().setSecAscTyp(dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().getSecAssociationType());
		// COB_CODE: EXEC SQL
		//               OPEN CURSOR2
		//           END-EXEC.
		halUowFunSecVDao.openCursor2(ws);
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 2110-OPEN-CURSOR2-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_UOW_FUN_SEC_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_FUN_SEC_V");
			// COB_CODE: MOVE '2110-OPEN-CURSOR2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2110-OPEN-CURSOR2");
			// COB_CODE: MOVE 'OPEN CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN CURSOR FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HUFSH-UOW-NM      = '    HUFSH-UOW-NM      ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalObjDataKey(new StringBuffer(256).append("HUFSH-UOW-NM      = ")
					.append(ws.getHufshUowFunSecRow().getUowNmFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2110-OPEN-CURSOR2-X
			return;
		}
	}

	/**Original name: 2120-COPY-CSR2-TO-TSQ2_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  COPY RETRIEVED ROWS TO INTERMEDIATE STORAGE IN THE QUEUE.
	 * *****************************************************************</pre>*/
	private void copyCsr2ToTsq2() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC SQL
		//               FETCH CURSOR2
		//                INTO :HUFSH-SEC-ASC-TYP
		//                    ,:HUFSH-FUN-SEQ-NBR
		//                    ,:HUFSH-AUT-ACTION-CD
		//                    ,:HUFSH-AUT-FUN-NM
		//                    ,:HUFSH-LNK-TO-MDU-NM
		//                    ,:HUFSH-APP-NM
		//           END-EXEC.
		halUowFunSecVDao.fetchCursor2(ws.getHufshUowFunSecRow());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                    CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                    GO TO 2120-COPY-CSR2-TO-TSQ2-X
		//               WHEN OTHER
		//                    GO TO 2120-COPY-CSR2-TO-TSQ2-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-CURSOR2 TO TRUE
			ws.getWsBdoSwitches().getEndOfCursor2Sw().setEndOfCursor2();
			// COB_CODE: GO TO 2120-COPY-CSR2-TO-TSQ2-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_UOW_FUN_SEC_V'    TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_FUN_SEC_V");
			// COB_CODE: MOVE '2120-COPY-CSR2-TO-TSQ2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2120-COPY-CSR2-TO-TSQ2");
			// COB_CODE: MOVE 'FETCH CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH CURSOR FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HUFSH-UOW-NM  = ' HUFSH-UOW-NM ';'
			//                  'SEC-GRP-NM    = ' HUFSH-SEC-GRP-NM ';'
			//                  'HUFSH-CONTEXT = ' HUFSH-CONTEXT ';'
			//                  'HUFSH-SEC-ASC-TYP = ' HUFSH-SEC-ASC-TYP ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HUFSH-UOW-NM  = ").append(ws.getHufshUowFunSecRow().getUowNmFormatted())
							.append(";").append("SEC-GRP-NM    = ").append(ws.getHufshUowFunSecRow().getSecGrpNmFormatted()).append(";")
							.append("HUFSH-CONTEXT = ").append(ws.getHufshUowFunSecRow().getContextFormatted()).append(";")
							.append("HUFSH-SEC-ASC-TYP = ").append(ws.getHufshUowFunSecRow().getSecAscTypFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2120-COPY-CSR2-TO-TSQ2-X
			return;
		}
		// COB_CODE: MOVE SPACES               TO WS-TSQ2-DATA-AREA.
		ws.getWsTsq2DataArea().initWsTsq2DataAreaSpaces();
		// COB_CODE: MOVE HUFSH-UOW-NM         TO WS-TSQ2-UOW-NM.
		ws.getWsTsq2DataArea().setUowNm(ws.getHufshUowFunSecRow().getUowNm());
		// COB_CODE: MOVE HUFSH-SEC-GRP-NM     TO WS-TSQ2-SEC-GRP-NM.
		ws.getWsTsq2DataArea().setSecGrpNm(ws.getHufshUowFunSecRow().getSecGrpNm());
		// COB_CODE: MOVE HUFSH-CONTEXT        TO WS-TSQ2-CONTEXT.
		ws.getWsTsq2DataArea().setContext(ws.getHufshUowFunSecRow().getContext());
		// COB_CODE: MOVE HUFSH-SEC-ASC-TYP    TO WS-TSQ2-SEC-ASC-TYP.
		ws.getWsTsq2DataArea().setSecAscTyp(ws.getHufshUowFunSecRow().getSecAscTyp());
		// COB_CODE: MOVE HUFSH-FUN-SEQ-NBR    TO WS-TSQ2-FUN-SEQ-NBR.
		ws.getWsTsq2DataArea().setFunSeqNbr(ws.getHufshUowFunSecRow().getFunSeqNbr());
		// COB_CODE: MOVE HUFSH-AUT-ACTION-CD  TO WS-TSQ2-AUT-ACTION-CD.
		ws.getWsTsq2DataArea().setAutActionCd(ws.getHufshUowFunSecRow().getAutActionCd());
		// COB_CODE: MOVE HUFSH-AUT-FUN-NM     TO WS-TSQ2-AUT-FUN-NM.
		ws.getWsTsq2DataArea().setAutFunNm(ws.getHufshUowFunSecRow().getAutFunNm());
		// COB_CODE: MOVE HUFSH-LNK-TO-MDU-NM  TO WS-TSQ2-LNK-TO-MDU-NM.
		ws.getWsTsq2DataArea().setLnkToMduNm(ws.getHufshUowFunSecRow().getLnkToMduNm());
		// COB_CODE: MOVE HUFSH-APP-NM         TO WS-TSQ2-APP-NM.
		ws.getWsTsq2DataArea().setAppNm(ws.getHufshUowFunSecRow().getAppNm());
		// COB_CODE: EXEC CICS
		//               WRITEQ TS MAIN
		//               QNAME  (WS-TSQ2-QUEUE-NAME)
		//               FROM   (WS-TSQ2-DATA-AREA)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(WsTsq2DataArea.Len.WS_TSQ2_DATA_AREA);
		tsQueueData.setData(ws.getWsTsq2DataArea().getWsTsq2DataAreaBytes());
		TsQueueManager.insert(execContext, ws.getWsTsq2QueueNameFormatted(), tsQueueData);
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2120-COPY-CSR2-TO-TSQ2-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE WS-TSQ2-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq2QueueNameFormatted());
			// COB_CODE: MOVE '2120-COPY-CSR2-TO-TSQ2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2120-COPY-CSR2-TO-TSQ2");
			// COB_CODE: MOVE 'WRITE OF HALOUSDH TSQ2 FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE OF HALOUSDH TSQ2 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2120-COPY-CSR2-TO-TSQ2-X
			return;
		}
	}

	/**Original name: 2130-CLOSE-CURSOR2_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  CLOSE UOW FUNCTIONS CURSOR.
	 * ****************************************************************</pre>*/
	private void closeCursor2() {
		// COB_CODE: EXEC SQL
		//               CLOSE CURSOR2
		//           END-EXEC.
		halUowFunSecVDao.closeCursor2();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//              GO TO 2130-CLOSE-CURSOR2-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_UOW_FUN_SEC_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UOW_FUN_SEC_V");
			// COB_CODE: MOVE '2130-CLOSE-CURSOR2'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2130-CLOSE-CURSOR2");
			// COB_CODE: MOVE 'CLOSE CURSOR FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE CURSOR FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2130-CLOSE-CURSOR2-X
			return;
		}
	}

	/**Original name: 2200-UNLOAD-CSR3_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF SCRIPTING HAS BEEN ACTIVATED, RETRIEVE ANY SCRIPTS FOR THE *
	 *  UOW, SECURITY GROUP, AND CONTEXT.                             *
	 * ****************************************************************</pre>*/
	private void unloadCsr3() {
		// COB_CODE: MOVE 'ALL_UOWS' TO HUC2H-ENTRY-KEY-CD.
		ws.getDclgenHalUniversalCt2().setEntryKeyCd("ALL_UOWS");
		// COB_CODE: MOVE 'SCREEN_SCRIPT_RETRIEVAL' TO HUC2H-TBL-LBL-TXT.
		ws.getDclgenHalUniversalCt2().setTblLblTxt("SCREEN_SCRIPT_RETRIEVAL");
		// COB_CODE: EXEC SQL
		//              SELECT HUC2_ENTRY_DTA_TXT
		//                INTO :HUC2H-ENTRY-DTA-TXT
		//                FROM HAL_UNIVERSAL_CT_V
		//               WHERE HUC2_ENTRY_KEY_CD =: HUC2H-ENTRY-KEY-CD
		//                 AND HUC2_TBL_LBL_TXT  =: HUC2H-TBL-LBL-TXT
		//           END-EXEC.
		ws.getDclgenHalUniversalCt2().setEntryDtaTxt(halUniversalCtVDao.selectRec(ws.getDclgenHalUniversalCt2().getEntryKeyCd(),
				ws.getDclgenHalUniversalCt2().getTblLblTxt(), ws.getDclgenHalUniversalCt2().getEntryDtaTxt()));
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 2200-UNLOAD-CSR3-X
		//               WHEN OTHER
		//                   GO TO 2200-UNLOAD-CSR3-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: GO TO 2200-UNLOAD-CSR3-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED         OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-SELECT OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2Select();
			// COB_CODE: MOVE 'HAL_UNIVERSAL_CT_V' TO EFAL-ERR-OBJECT-NAME
			//             OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_UNIVERSAL_CT_V");
			// COB_CODE: MOVE '2200-UNLOAD-CSR3'
			//             TO EFAL-ERR-PARAGRAPH  OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2200-UNLOAD-CSR3");
			// COB_CODE: MOVE 'SELECT SCREEN_SCRIPT_RETRIEVAL FLAG FAILED'
			//                TO EFAL-ERR-COMMENT    OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("SELECT SCREEN_SCRIPT_RETRIEVAL FLAG FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HUC2H-ENTRY-KEY-CD='
			//                   HUC2H-ENTRY-KEY-CD  ';'
			//                  'HUC2H-TBL-LBL-TXT='
			//                   HUC2H-TBL-LBL-TXT   ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HUC2H-ENTRY-KEY-CD=")
							.append(ws.getDclgenHalUniversalCt2().getEntryKeyCdFormatted()).append(";").append("HUC2H-TBL-LBL-TXT=")
							.append(ws.getDclgenHalUniversalCt2().getTblLblTxtFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2200-UNLOAD-CSR3-X
			return;
		}
		// COB_CODE: PERFORM 2210-PROCESS-SCRIPTS.
		processScripts();
	}

	/**Original name: 2210-PROCESS-SCRIPTS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  OPEN CURSOR USED TO READ HAL_SCRN_SCRIPT_V
	 *  WITH INFORMATION PASSED IN HALLUBOC LINKAGE.
	 *  RETRIEVE THE APPROPRIATE ROWS BASED ON THIS CURSOR.
	 *  CLOSE THE CURSOR.
	 * ****************************************************************</pre>*/
	private void processScripts() {
		// COB_CODE: MOVE UBOC-UOW-NAME         TO HSSVH-UOW-NM.
		ws.getHssvhHalScrnScriptRow().setUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-SEC-GROUP-NAME   TO HSSVH-SEC-GRP-NM.
		ws.getHssvhHalScrnScriptRow().setSecGrpNm(dfhcommarea.getCommInfo().getUbocSecGroupName());
		// COB_CODE: IF UBOC-NO-CONTEXT
		//              MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
		//           ELSE
		//              MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().isNoContext()) {
			// COB_CODE: MOVE SPACES TO HSSVH-HSSV-CONTEXT
			//                          WS-FULL-CONTEXT
			ws.getHssvhHalScrnScriptRow().setHssvContext("");
			ws.getWsWorkFields().getWsContextRedef().setWsFullContext("");
			// COB_CODE: MOVE SPACES TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr("");
			// COB_CODE: MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr(ws.getWsWorkFields().getWsContextRedef().getContextFirst10());
		} else {
			// COB_CODE: MOVE UBOC-SECURITY-CONTEXT TO HSSVH-HSSV-CONTEXT
			//                                         WS-FULL-CONTEXT
			ws.getHssvhHalScrnScriptRow().setHssvContext(dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext());
			ws.getWsWorkFields().getWsContextRedef().setWsFullContext(dfhcommarea.getCommInfo().getUbocSecAndDataPrivInfo().getSecurityContext());
			// COB_CODE: MOVE SPACES TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr("");
			// COB_CODE: MOVE WS-CONTEXT-FIRST-10 TO WS-CONTEXT-FIRST-10-FOR-CSR
			ws.getWsWorkFields().setWsContextFirst10ForCsr(ws.getWsWorkFields().getWsContextRedef().getContextFirst10());
		}
		// COB_CODE: EXEC SQL
		//              OPEN SCRIPTCUR
		//           END-EXEC.
		halScrnScriptVDao.openScriptcur(ws.getHssvhHalScrnScriptRow().getUowNm(), ws.getHssvhHalScrnScriptRow().getSecGrpNm(),
				ws.getWsWorkFields().getWsContextRedef().getWsFullContext(), ws.getWsWorkFields().getWsContextFirst10ForCsr());
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2210-PROCESS-SCRIPTS-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-OPEN-CSR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2OpenCsr();
			// COB_CODE: MOVE 'HAL_SCRN_SCRIPT_V'     TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SCRN_SCRIPT_V");
			// COB_CODE: MOVE '2210-PROCESS-SCRIPTS'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2210-PROCESS-SCRIPTS");
			// COB_CODE: MOVE 'OPEN SCRIPTCUR FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("OPEN SCRIPTCUR FAILED");
			// COB_CODE: MOVE HSSVH-HSSV-SEQ-NBR
			//             TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getHssvhHalScrnScriptRow().getHssvSeqNbr());
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HSSVH-UOW-NM='
			//                  HSSVH-UOW-NM ';'
			//                  'HSSVH-SEC-GRP-NM='
			//                  HSSVH-SEC-GRP-NM ';'
			//                  'HSSVH-HSSV-CONTEXT='
			//                  HSSVH-HSSV-CONTEXT ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HSSVH-UOW-NM=").append(ws.getHssvhHalScrnScriptRow().getUowNmFormatted())
							.append(";").append("HSSVH-SEC-GRP-NM=").append(ws.getHssvhHalScrnScriptRow().getSecGrpNmFormatted()).append(";")
							.append("HSSVH-HSSV-CONTEXT=").append(ws.getHssvhHalScrnScriptRow().getHssvContextFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2210-PROCESS-SCRIPTS-X
			return;
		}
		// COB_CODE: SET WS-NOT-END-OF-SCRIPTCUR TO TRUE.
		ws.getWsSwitches().getScriptProcessing().setNotEndOfScriptcur();
		// COB_CODE: SET WS-THE-FIRST-FETCH    TO TRUE.
		ws.getWsBdoSwitches().getFirstFetchSw().setTheFirstFetch();
		// COB_CODE: PERFORM 2220-FETCH-SCRIPTS
		//               UNTIL UBOC-HALT-AND-RETURN
		//                   OR WS-END-OF-SCRIPTCUR.
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getScriptProcessing().isEndOfScriptcur())) {
			fetchScripts();
		}
		// COB_CODE: EXEC SQL
		//              CLOSE SCRIPTCUR
		//           END-EXEC.
		halScrnScriptVDao.closeScriptcur();
		// COB_CODE: IF NOT ERD-SQL-GOOD
		//               GO TO 2210-PROCESS-SCRIPTS-X
		//           END-IF.
		if (!sqlca.isErdSqlGood()) {
			// COB_CODE: SET WS-LOG-WARNING           TO TRUE
			ws.getWsLogWarningOrErrorSw().setWarning();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-CLOSE-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2CloseCsr();
			// COB_CODE: MOVE 'HAL_SCRN_SCRIPT_V'        TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SCRN_SCRIPT_V");
			// COB_CODE: MOVE '2210-PROCESS-SCRIPTS'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2210-PROCESS-SCRIPTS");
			// COB_CODE: MOVE 'CLOSE SCRIPTCUR FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CLOSE SCRIPTCUR FAILED");
			// COB_CODE: MOVE HSSVH-HSSV-SEQ-NBR
			//             TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getHssvhHalScrnScriptRow().getHssvSeqNbr());
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HSSVH-UOW-NM='
			//                  HSSVH-UOW-NM ';'
			//                  'HSSVH-SEC-GRP-NM='
			//                  HSSVH-SEC-GRP-NM ';'
			//                  'HSSVH-HSSV-CONTEXT='
			//                  HSSVH-HSSV-CONTEXT ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HSSVH-UOW-NM=").append(ws.getHssvhHalScrnScriptRow().getUowNmFormatted())
							.append(";").append("HSSVH-SEC-GRP-NM=").append(ws.getHssvhHalScrnScriptRow().getSecGrpNmFormatted()).append(";")
							.append("HSSVH-HSSV-CONTEXT=").append(ws.getHssvhHalScrnScriptRow().getHssvContextFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2210-PROCESS-SCRIPTS-X
			return;
		}
	}

	/**Original name: 2220-FETCH-SCRIPTS_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  FETCH ROW DATA USING CURSOR WITH DATA FROM HALLUBOC LINKAGE
	 * ****************************************************************</pre>*/
	private void fetchScripts() {
		// COB_CODE: EXEC SQL
		//            FETCH SCRIPTCUR
		//            INTO :HSSVH-UOW-NM
		//                ,:HSSVH-SEC-GRP-NM
		//                ,:HSSVH-HSSV-CONTEXT
		//                ,:HSSVH-HSSV-SCRN-FLD-ID
		//                ,:HSSVH-HSSV-SEQ-NBR
		//                ,:HSSVH-HSSV-NEW-LIN-IND
		//                ,:HSSVH-HSSV-SCRIPT-TXT
		//           END-EXEC.
		halScrnScriptVDao.fetchScriptcur(ws.getHssvhHalScrnScriptRow());
		// COB_CODE: EVALUATE TRUE
		//               WHEN ERD-SQL-GOOD
		//                   CONTINUE
		//               WHEN ERD-SQL-NOT-FOUND
		//                   GO TO 2220-FETCH-SCRIPTS-X
		//               WHEN OTHER
		//                   GO TO 2220-FETCH-SCRIPTS-X
		//           END-EVALUATE.
		switch (sqlca.getErdSqlRed()) {

		case Sqlca.ERD_SQL_GOOD:// COB_CODE: CONTINUE
			//continue
			break;

		case Sqlca.ERD_SQL_NOT_FOUND:// COB_CODE: SET WS-END-OF-SCRIPTCUR TO TRUE
			ws.getWsSwitches().getScriptProcessing().setEndOfScriptcur();
			// COB_CODE: GO TO 2220-FETCH-SCRIPTS-X
			return;

		default:// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-DB2-FAILED          TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2Failed();
			// COB_CODE: SET ETRA-DB2-FETCH-CSR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraDb2FetchCsr();
			// COB_CODE: MOVE 'HAL_SCRN_SCRIPT_V'    TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("HAL_SCRN_SCRIPT_V");
			// COB_CODE: MOVE '2220-FETCH-SCRIPTS'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2220-FETCH-SCRIPTS");
			// COB_CODE: MOVE 'FETCH FROM CURSOR2 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("FETCH FROM CURSOR2 FAILED");
			// COB_CODE: MOVE HSSVH-HSSV-SEQ-NBR
			//             TO WS-DATA-KEY-DISP1
			ws.getWsNotSpecificMisc().setDataKeyDisp1(ws.getHssvhHalScrnScriptRow().getHssvSeqNbr());
			// COB_CODE: MOVE SPACE TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'HSSVH-UOW-NM='
			//                  HSSVH-UOW-NM ';'
			//                  'HSSVH-SEC-GRP-NM='
			//                  HSSVH-SEC-GRP-NM ';'
			//                  'HSSVH-HSSV-CONTEXT='
			//                  HSSVH-HSSV-CONTEXT ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("HSSVH-UOW-NM=").append(ws.getHssvhHalScrnScriptRow().getUowNmFormatted())
							.append(";").append("HSSVH-SEC-GRP-NM=").append(ws.getHssvhHalScrnScriptRow().getSecGrpNmFormatted()).append(";")
							.append("HSSVH-HSSV-CONTEXT=").append(ws.getHssvhHalScrnScriptRow().getHssvContextFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2220-FETCH-SCRIPTS-X
			return;
		}
		// COB_CODE: PERFORM 2230-STORE-SCRIPT.
		storeScript();
		// COB_CODE: SET WS-NOT-THE-FIRST-FETCH TO TRUE.
		ws.getWsBdoSwitches().getFirstFetchSw().setNotTheFirstFetch();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 2220-FETCH-SCRIPTS-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 2220-FETCH-SCRIPTS-X
			return;
		}
	}

	/**Original name: 2230-STORE-SCRIPT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  WRITE THE RETRIEVED SCREEN SCRIPT ROW TO THE RESPONSE MESSAGE.
	 * ****************************************************************</pre>*/
	private void storeScript() {
		TpOutputData tsQueueData = null;
		// COB_CODE: INITIALIZE WS-SCRIPT-ROW.
		initWsScriptRow();
		//* THESE FIELDS WILL ALWAYS BE VIEW-ONLY WHEN RETRIEVED DURING
		//* HALOUSDH PROCESSING
		// COB_CODE: SET COM-IS-VIEWONLY-COL-IND TO TRUE.
		ws.getHallcom().getOutgoingColIndVal().setComIsViewonlyColInd();
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-UOW-NM-CI.
		ws.getHssvcHalScrnScriptRow().setUowNmCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-UOW-NM
		//             TO HSSVC-UOW-NM.
		ws.getHssvcHalScrnScriptRow().setUowNm(ws.getHssvhHalScrnScriptRow().getUowNm());
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-SEC-GRP-NM-CI.
		ws.getHssvcHalScrnScriptRow().setSecGrpNmCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-SEC-GRP-NM
		//             TO HSSVC-SEC-GRP-NM.
		ws.getHssvcHalScrnScriptRow().setSecGrpNm(ws.getHssvhHalScrnScriptRow().getSecGrpNm());
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-HSSV-CONTEXT-CI.
		ws.getHssvcHalScrnScriptRow().setHssvContextCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-HSSV-CONTEXT
		//             TO HSSVC-HSSV-CONTEXT.
		ws.getHssvcHalScrnScriptRow().setHssvContext(ws.getHssvhHalScrnScriptRow().getHssvContext());
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-HSSV-SCRN-FLD-ID-CI.
		ws.getHssvcHalScrnScriptRow().setHssvScrnFldIdCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-HSSV-SCRN-FLD-ID
		//             TO HSSVC-HSSV-SCRN-FLD-ID.
		ws.getHssvcHalScrnScriptRow().setHssvScrnFldId(ws.getHssvhHalScrnScriptRow().getHssvScrnFldId());
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-HSSV-SEQ-NBR-CI.
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbrCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: IF HSSVH-HSSV-SEQ-NBR < 0
		//               MOVE '-' TO HSSVC-HSSV-SEQ-NBR-SIGN
		//           ELSE
		//               MOVE '+' TO HSSVC-HSSV-SEQ-NBR-SIGN
		//           END-IF.
		if (ws.getHssvhHalScrnScriptRow().getHssvSeqNbr() < 0) {
			// COB_CODE: MOVE '-' TO HSSVC-HSSV-SEQ-NBR-SIGN
			ws.getHssvcHalScrnScriptRow().setHssvSeqNbrSignFormatted("-");
		} else {
			// COB_CODE: MOVE '+' TO HSSVC-HSSV-SEQ-NBR-SIGN
			ws.getHssvcHalScrnScriptRow().setHssvSeqNbrSignFormatted("+");
		}
		// COB_CODE: MOVE HSSVH-HSSV-SEQ-NBR
		//             TO HSSVC-HSSV-SEQ-NBR.
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbr(TruncAbs.toLong(ws.getHssvhHalScrnScriptRow().getHssvSeqNbr(), 10));
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-HSSV-NEW-LIN-IND-CI.
		ws.getHssvcHalScrnScriptRow().setHssvNewLinIndCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-HSSV-NEW-LIN-IND
		//             TO HSSVC-HSSV-NEW-LIN-IND.
		ws.getHssvcHalScrnScriptRow().setHssvNewLinInd(ws.getHssvhHalScrnScriptRow().getHssvNewLinInd());
		// COB_CODE: MOVE COM-OUTGOING-COL-IND-VAL
		//             TO HSSVC-HSSV-SCRIPT-TXT-CI.
		ws.getHssvcHalScrnScriptRow().setHssvScriptTxtCi(ws.getHallcom().getOutgoingColIndVal().getOutgoingColIndVal());
		// COB_CODE: MOVE HSSVH-HSSV-SCRIPT-TXT-TEXT
		//                (1:HSSVH-HSSV-SCRIPT-TXT-LEN)
		//             TO HSSVC-HSSV-SCRIPT-TXT.
		ws.getHssvcHalScrnScriptRow().setHssvScriptTxt(ws.getHssvhHalScrnScriptRow().getHssvScriptTxtTextFormatted().substring((1) - 1,
				ws.getHssvhHalScrnScriptRow().getHssvScriptTxtLen()));
		// COB_CODE: EXEC CICS
		//               WRITEQ TS MAIN
		//               QNAME  (WS-TSQ3-QUEUE-NAME)
		//               FROM   (WS-SCRIPT-ROW)
		//               RESP   (WS-RESPONSE-CODE)
		//               RESP2  (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalousdhData.Len.WS_SCRIPT_ROW);
		tsQueueData.setData(ws.getWsScriptRowBytes());
		TsQueueManager.insert(execContext, ws.getWsTsq3QueueNameFormatted(), tsQueueData);
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2230-STORE-SCRIPT-X
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-WRITE-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsWriteTsq();
			// COB_CODE: MOVE WS-TSQ3-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq3QueueNameFormatted());
			// COB_CODE: MOVE '2230-STORE-SCRIPT'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("2230-STORE-SCRIPT");
			// COB_CODE: MOVE 'WRITE OF SCRIPT TSQ3 FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("WRITE OF SCRIPT TSQ3 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 2230-STORE-SCRIPT-X
			return;
		}
	}

	/**Original name: 3000-READ-REQ-MSG-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  PROCESS MESSAGE REQUEST FROM THE UOW REQUEST MESSAGE UMT.      *
	 *                                                                 *
	 *  ONLY ONE ROW IN THE UMT FOR THIS MODULE IS NEEDED IN THE UMT   *
	 *  FOR SET DEFAULTS PROCESSING TO CONTINUE.  IF NO ROW IS FOUND,  *
	 *  THEN SET DEFAULTS PROCESSING IS BYPASSED.                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readReqMsgUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: SET REQ-UMT-DFLT-REC-NOT-FND TO TRUE.
		ws.getWsSwitches().getReqUmtDfltRecSw().setNotFnd();
		// COB_CODE: MOVE UBOC-MSG-ID       TO URQM-ID.
		ws.getUrqmCommon().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-BUS-OBJ-NM     TO URQM-BUS-OBJ.
		ws.getUrqmCommon().setBusObj(ws.getWsSpecificWorkAreas().getBusObjNm());
		// COB_CODE: MOVE 1                 TO URQM-REC-SEQ.
		ws.getUrqmCommon().setRecSeq(1);
		// COB_CODE: MOVE SPACES  TO URQM-UOW-MESSAGE-BUFFER.
		ws.getUrqmCommon().initUowMessageBufferSpaces();
		// COB_CODE: EXEC CICS
		//                READ FILE  (UBOC-UOW-REQ-MSG-STORE)
		//                INTO       (URQM-COMMON)
		//                RIDFLD     (URQM-KEY)
		//                KEYLENGTH  (LENGTH OF URQM-KEY)
		//                RESP       (WS-RESPONSE-CODE)
		//                RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqMsgStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getUrqmCommon().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, UrqmCommon.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getUrqmCommon().setUrqmCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF NORMAL, PROCEED WITH SET DEFAULTS PROCESSING.
		//* IF NOTFND, BYPASS SET DEFAULTS PROCESSING AND RETURN TO HALOMCM
		//* OTHERWISE WE HAVE AN UNEXPECTED ERROR.
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SET REQ-UMT-DFLT-REC-FND TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 3000-READ-REQ-MSG-UMT-X
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET REQ-UMT-DFLT-REC-FND TO TRUE
			ws.getWsSwitches().getReqUmtDfltRecSw().setFnd();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: GO TO 3000-READ-REQ-MSG-UMT-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-MSG-STORE
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqMsgStore());
			// COB_CODE: MOVE '3000-READ-REQ-MSG-UMT'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("3000-READ-REQ-MSG-UMT");
			// COB_CODE: MOVE 'READ REQ MSG STORE FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ REQ MSG STORE FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'URQM-KEY=' URQM-KEY ';'
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("URQM-KEY=").append(ws.getUrqmCommon().getKeyFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 4000-EXECUTE-TSQ1-LOOP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THRU TSQ1 AND CALL THE SD/DP MODULES IS APPROPRIATE.
	 * *****************************************************************</pre>*/
	private void executeTsq1Loop() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN SET-DEFAULTS-REQUEST   OF URQM-COMMON
		//                  MOVE SPACES TO WS-HOLD-PASS-THRU-ACTION
		//               WHEN OTHER
		//                  SET SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO TO TRUE
		//           END-EVALUATE.
		switch (ws.getUrqmCommon().getActionCode().getUbocPassThruAction()) {

		case UbocPassThruAction.SET_DEFAULTS_REQUEST:// COB_CODE: SET SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getCommInfo().getUbocPassThruAction().setSetDefaultsRequest();
			// COB_CODE: MOVE SPACES TO WS-HOLD-PASS-THRU-ACTION
			ws.getWsWorkFields().setWsHoldPassThruAction("");
			break;

		default:// COB_CODE: MOVE UBOC-PASS-THRU-ACTION
			//           TO WS-HOLD-PASS-THRU-ACTION
			ws.getWsWorkFields().setWsHoldPassThruAction(dfhcommarea.getCommInfo().getUbocPassThruAction().getUbocPassThruAction());
			// COB_CODE: SET SET-DEFAULTS-REQUEST OF UBOC-COMM-INFO TO TRUE
			dfhcommarea.getCommInfo().getUbocPassThruAction().setSetDefaultsRequest();
			break;
		}
		// COB_CODE: SET WS-START-OF-TSQ1 TO TRUE.
		ws.getWsSwitches().getTsq1Sw().setStartOfTsq1();
		// COB_CODE: PERFORM 4010-PROCESS-TSQ1
		//              VARYING WS-TSQ1-QUEUE-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-TSQ1.
		ws.setWsTsq1QueueCnt(((short) 1));
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getTsq1Sw().isEndOfTsq1())) {
			processTsq1();
			ws.setWsTsq1QueueCnt(Trunc.toShort(ws.getWsTsq1QueueCnt() + 1, 4));
		}
		//*** IF THIS IS A SET_DEFAULT REQUEST, RETURN A SET_DEFAULT
		//*** HEADER AND DATA RESPONSE ROW
		// COB_CODE: IF REQ-UMT-DFLT-REC-FND
		//              END-IF
		//           END-IF.
		if (ws.getWsSwitches().getReqUmtDfltRecSw().isFnd()) {
			// COB_CODE: PERFORM 4040-BUILD-SDH-DATA-ROW
			buildSdhDataRow();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 0000-RETURN-TO-FRONT-END
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 0000-RETURN-TO-FRONT-END
				returnToFrontEnd();
			}
		}
		// COB_CODE: IF WS-HOLD-PASS-THRU-ACTION NOT EQUAL SPACES
		//                TO UBOC-PASS-THRU-ACTION
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsWorkFields().getWsHoldPassThruAction())) {
			// COB_CODE: MOVE WS-HOLD-PASS-THRU-ACTION
			//             TO UBOC-PASS-THRU-ACTION
			dfhcommarea.getCommInfo().getUbocPassThruAction().setUbocPassThruAction(ws.getWsWorkFields().getWsHoldPassThruAction());
		}
	}

	/**Original name: 4010-PROCESS-TSQ1_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  A>  FETCH OBJECT MODULE NAME                                  *
	 * ****************************************************************</pre>*/
	private void processTsq1() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS
		//                QNAME    (WS-TSQ1-QUEUE-NAME)
		//                INTO     (WS-TSQ1-DATA-AREA)
		//                ITEM     (WS-TSQ1-QUEUE-CNT)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalousdhData.Len.WS_TSQ1_DATA_AREA);
		TsQueueManager.read(execContext, ws.getWsTsq1QueueNameFormatted(), ws.getWsTsq1QueueCnt(), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.setWsTsq1DataAreaBytes(tsQueueData.getData());
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN DFHRESP(QIDERR)
		//               WHEN DFHRESP(ITEMERR)
		//                    GO TO 4010-PROCESS-TSQ1-X
		//               WHEN OTHER
		//                    GO TO 4010-PROCESS-TSQ1-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-TSQ1 TO TRUE
			ws.getWsSwitches().getTsq1Sw().setEndOfTsq1();
			// COB_CODE: GO TO 4010-PROCESS-TSQ1-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE WS-TSQ1-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq1QueueNameFormatted());
			// COB_CODE: MOVE '4010-PROCESS-TSQ1'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4010-PROCESS-TSQ1");
			// COB_CODE: MOVE 'READ OF HALOUSDH TSQ1 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF HALOUSDH TSQ1 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4010-PROCESS-TSQ1-X
			return;
		}
		// COB_CODE: MOVE WS-TSQ1-BOBJ-NM    TO CIDP-OBJECT-NAME
		ws.getCidpTableInfo().setObjectName(ws.getWsTsq1BobjNm());
		// COB_CODE: MOVE WS-TSQ1-DFL-MDU-NM TO WS-DATA-PRIV-MDU
		ws.getWsWorkFields().setWsDataPrivMdu(ws.getWsTsq1DflMduNm());
		// COB_CODE: MOVE SPACES             TO CIDP-OBJECT-ROW
		ws.getCidpTableInfo().setObjectRow("");
		//*IF THIS IS A SET_DEFAULT REQUEST, CHECK FOR SWITCHES.  IF NO
		//*SWITCHES, A RETURN-ALL SWITCH, OR A DIRECT MATCH CALL DP MODULE.
		//*IF THIS IS NOT A SET DEFAULTS REQUEST, CHECK SWITCHES PRIOR TO
		//*BUILDING ROWS FOR BDOS THAT HAVE NOT RETURNED ACTUAL DATA.
		// COB_CODE: IF REQ-UMT-DFLT-REC-FND
		//              END-IF
		//           ELSE
		//              PERFORM 4015-DP-FOR-NON-SET-DEFAULT
		//           END-IF.
		if (ws.getWsSwitches().getReqUmtDfltRecSw().isFnd()) {
			// COB_CODE: PERFORM 4020-CHECK-FOR-SWITCHES
			checkForSwitches();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//              GO TO 4010-PROCESS-TSQ1-X
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4010-PROCESS-TSQ1-X
				return;
			}
			// COB_CODE: IF WS-CALL-DATA-PRIV
			//              PERFORM 4030-LINK-TO-DATA-PRIV-MOD
			//           END-IF
			if (ws.getWsSwitches().getCallDataPrivSw().isCallDataPriv()) {
				// COB_CODE: PERFORM 4030-LINK-TO-DATA-PRIV-MOD
				linkToDataPrivMod();
			}
		} else {
			// COB_CODE: PERFORM 4015-DP-FOR-NON-SET-DEFAULT
			dpForNonSetDefault();
		}
	}

	/**Original name: 4015-DP-FOR-NON-SET-DEFAULT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  IF NOT SET_DEFAULT REQUEST, ONLY CALL THE CORRESPONDING
	 *  DATA PRIVACY MODULE IF THERE IS NO DATA BEING RETURNED VIA BDO.
	 *  THIS PROCESS SHOULD ALSO CHECK FOR THE EXISTENCE OF SWITCHES
	 *  PRIOR TO RETURNING DATA - EITHER RETURN-ALL OR A DIRECT MATCH.
	 * ****************************************************************</pre>*/
	private void dpForNonSetDefault() {
		// COB_CODE: PERFORM 4020-CHECK-FOR-SWITCHES.
		checkForSwitches();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 4015-DP-FOR-NON-SET-DEFAULT-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4015-DP-FOR-NON-SET-DEFAULT-X
			return;
		}
		// COB_CODE: IF WS-CALL-DATA-PRIV
		//               END-IF
		//           END-IF.
		if (ws.getWsSwitches().getCallDataPrivSw().isCallDataPriv()) {
			// COB_CODE: PERFORM 4050-READ-HEADER-UMT
			readHeaderUmt();
			// COB_CODE: IF UBOC-HALT-AND-RETURN
			//               GO TO 4015-DP-FOR-NON-SET-DEFAULT-X
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: GO TO 4015-DP-FOR-NON-SET-DEFAULT-X
				return;
			}
			// COB_CODE: IF WS-UMT-HDR-MISSING
			//               PERFORM 4030-LINK-TO-DATA-PRIV-MOD
			//           END-IF
			if (ws.getWsBdoSwitches().getUmtHdrSw().isMissing()) {
				// COB_CODE: PERFORM 4030-LINK-TO-DATA-PRIV-MOD
				linkToDataPrivMod();
			}
		}
	}

	/**Original name: 4020-CHECK-FOR-SWITCHES_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DECIPHER THE RETRIEVED SWITCH INFORMATION                     *
	 * ****************************************************************</pre>*/
	private void checkForSwitches() {
		// COB_CODE: IF MASTER-SWITCH-NOT-SET
		//              SET WS-DONT-CALL-DATA-PRIV TO TRUE
		//           END-IF.
		if (ws.getWsBdoSwitches().getMasterSwitchInd().isNotSet()) {
			// COB_CODE: SET WS-DONT-CALL-DATA-PRIV TO TRUE
			ws.getWsSwitches().getCallDataPrivSw().setDontCallDataPriv();
		}
		// NO SWITCHES OR 'RETURN ALL' SHOULD BE TREATED THE SAME
		// IF NO SWITCHES ARE PRESENT, MASTER-SWITCH WILL BE SET TO TRUE
		// COB_CODE: PERFORM 5130-CHECK-FOR-NO-SWITCHES
		checkForNoSwitches();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 4020-CHECK-FOR-SWITCHES-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4020-CHECK-FOR-SWITCHES-X
			return;
		}
		// COB_CODE: PERFORM 5120-CHECK-MSTR-SWT.
		checkMstrSwt();
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//              GO TO 4020-CHECK-FOR-SWITCHES-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 4020-CHECK-FOR-SWITCHES-X
			return;
		}
		// COB_CODE: IF MASTER-SWITCH-SET
		//              SET WS-CALL-DATA-PRIV TO TRUE
		//           ELSE
		//              END-IF
		//           END-IF.
		if (ws.getWsBdoSwitches().getMasterSwitchInd().isSetFld()) {
			// COB_CODE: SET WS-CALL-DATA-PRIV TO TRUE
			ws.getWsSwitches().getCallDataPrivSw().setCallDataPriv();
		} else {
			// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
			// COB_CODE: PERFORM 5100-READ-SWITCH-UMT
			readSwitchUmt();
			// COB_CODE: IF MATCHING-SWITCH-FOUND
			//           AND NOT UBOC-HALT-AND-RETURN
			//             SET WS-CALL-DATA-PRIV TO TRUE
			//           END-IF
			if (ws.getWsBdoSwitches().getMatchingSwitchSw().isFound()
					&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: SET WS-CALL-DATA-PRIV TO TRUE
				ws.getWsSwitches().getCallDataPrivSw().setCallDataPriv();
			}
		}
	}

	/**Original name: 4030-LINK-TO-DATA-PRIV-MOD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  A>  LINK TO OBJECT MODULE                                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void linkToDataPrivMod() {
		// COB_CODE: MOVE WS-DATA-PRIVACY-INFO TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer(ws.getWsDataPrivacyInfoFormatted());
		// COB_CODE: MOVE LENGTH OF WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HalousdhData.Len.WS_DATA_PRIVACY_INFO));
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM   (WS-DATA-PRIV-MDU)
		//                COMMAREA  (UBOC-COMM-INFO)
		//                LENGTH    (LENGTH OF UBOC-COMM-INFO)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUSDH", execContext).commarea(dfhcommarea.getCommInfo()).length(UbocCommInfoCaws002.Len.COMM_INFO)
				.link(ws.getWsWorkFields().getWsDataPrivMdu());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN OTHER
		//                   GO TO 4030-LINK-TO-DATA-PRIV-MOD-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF NOT DPER-ROW-EXCLUDED
			//              PERFORM 5000-WRITE-RESPONSE-ROWS
			//           END-IF
			if (!dfhcommarea.getCommInfo().getUbocDataPrivRetCode().isRowExcluded()) {
				// COB_CODE: MOVE UBOC-APP-DATA-BUFFER
				//             TO WS-DATA-PRIVACY-INFO
				ws.setWsDataPrivacyInfoFormatted(dfhcommarea.getAppDataBufferFormatted());
				// COB_CODE: PERFORM 5000-WRITE-RESPONSE-ROWS
				writeResponseRows();
			}
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE WS-DATA-PRIV-MDU
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getWsDataPrivMdu());
			// COB_CODE: MOVE '4030-LINK-TO-DATA-PRIV-MOD'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4030-LINK-TO-DATA-PRIV-MOD");
			// COB_CODE: MOVE 'CICS LINK TO BDO FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CICS LINK TO BDO FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4030-LINK-TO-DATA-PRIV-MOD-X
			return;
		}
	}

	/**Original name: 4040-BUILD-SDH-DATA-ROW_FIRST_SENTENCES<br>
	 * <pre>***********************************************************
	 *  BUILD AND WRITE THE SET DEFAULTS ROW TO THE RESPONSE UMTS.
	 * ***********************************************************</pre>*/
	private void buildSdhDataRow() {
		// COB_CODE: MOVE WS-BUS-OBJ-NM TO CIDP-OBJECT-NAME
		//                                 USDH-BUS-OBJ-NM.
		ws.getCidpTableInfo().setObjectName(ws.getWsSpecificWorkAreas().getBusObjNm());
		ws.setUsdhBusObjNm(ws.getWsSpecificWorkAreas().getBusObjNm());
		// COB_CODE: MOVE SPACES        TO CIDP-OBJECT-ROW.
		ws.getCidpTableInfo().setObjectRow("");
		// COB_CODE: MOVE USDH-SET-DEFAULTS-ROW
		//             TO UBOC-APP-DATA-BUFFER
		//                CIDP-OBJECT-ROW.
		dfhcommarea.setAppDataBuffer(ws.getUsdhSetDefaultsRowFormatted());
		ws.getCidpTableInfo().setObjectRow(ws.getUsdhSetDefaultsRowFormatted());
		// COB_CODE: MOVE LENGTH OF USDH-SET-DEFAULTS-ROW
		//             TO UBOC-APP-DATA-BUFFER-LENGTH
		//                CIDP-OBJECT-ROW-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HalousdhData.Len.USDH_SET_DEFAULTS_ROW));
		ws.getCidpTableInfo().setObjectRowLength(((short) HalousdhData.Len.USDH_SET_DEFAULTS_ROW));
		// COB_CODE: PERFORM 5000-WRITE-RESPONSE-ROWS.
		writeResponseRows();
	}

	/**Original name: 4050-READ-HEADER-UMT_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  DETERMINE IF DATA ROWS HAVE BEEN WRITTEN FOR THE BDO ASSOCIATED*
	 *  WITH THE RETRIEVED DATA PRIVACY/SET DEFAULTS MODULE.           *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void readHeaderUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: SET WS-UMT-HDR-EXISTS TO TRUE.
		ws.getWsBdoSwitches().getUmtHdrSw().setExists();
		// COB_CODE: MOVE WS-TSQ1-BOBJ-NM TO UHDR-BUS-OBJ-NM.
		ws.getHalluhdr().setBusObjNm(ws.getWsTsq1BobjNm());
		// COB_CODE: MOVE UBOC-MSG-ID TO UHDR-ID.
		ws.getHalluhdr().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-RESP-HEADER-STORE)
		//                INTO      (UHDR-COMMON)
		//                RIDFLD    (UHDR-KEY)
		//                KEYLENGTH (LENGTH OF UHDR-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowRespHeaderStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHalluhdr().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Halluhdr.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHalluhdr().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SET WS-UMT-HDR-EXISTS TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                   SET WS-UMT-HDR-MISSING TO TRUE
		//               WHEN OTHER
		//                   GO TO 4050-READ-HEADER-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET WS-UMT-HDR-EXISTS TO TRUE
			ws.getWsBdoSwitches().getUmtHdrSw().setExists();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET WS-UMT-HDR-MISSING TO TRUE
			ws.getWsBdoSwitches().getUmtHdrSw().setMissing();
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-RESP-HEADER-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore());
			// COB_CODE: MOVE '4050-READ-HEADER-UMT-X'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4050-READ-HEADER-UMT-X");
			// COB_CODE: MOVE 'READ RESP HEADER UMT FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ RESP HEADER UMT FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'UHDR-ID='         UHDR-ID         ';'
			//                  'UHDR-BUS-OBJ-NM=' UHDR-BUS-OBJ-NM ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("UHDR-ID=").append(ws.getHalluhdr().getIdFormatted()).append(";")
							.append("UHDR-BUS-OBJ-NM=").append(ws.getHalluhdr().getBusObjNmFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4050-READ-HEADER-UMT-X
			return;
		}
	}

	/**Original name: 4100-EXECUTE-TSQ2-LOOP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THRU TSQ2 AND WRITE THE FUN SEC INFO TO THE RESPONSE MSG.
	 * *****************************************************************</pre>*/
	private void executeTsq2Loop() {
		// COB_CODE: SET WS-START-OF-TSQ2 TO TRUE.
		ws.getWsSwitches().getTsq2Sw().setStartOfTsq2();
		// COB_CODE: PERFORM 4110-PROCESS-TSQ2
		//              VARYING WS-TSQ2-QUEUE-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-TSQ2.
		ws.setWsTsq2QueueCnt(((short) 1));
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getTsq2Sw().isEndOfTsq2())) {
			processTsq2();
			ws.setWsTsq2QueueCnt(Trunc.toShort(ws.getWsTsq2QueueCnt() + 1, 4));
		}
	}

	/**Original name: 4110-PROCESS-TSQ2_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ UOW FUNCTIONS FROM TSQ2 AND WRITE THEM TO THE UDAT UMT
	 * ****************************************************************</pre>*/
	private void processTsq2() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS
		//                QNAME    (WS-TSQ2-QUEUE-NAME)
		//                INTO     (WS-TSQ2-DATA-AREA)
		//                ITEM     (WS-TSQ2-QUEUE-CNT)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(WsTsq2DataArea.Len.WS_TSQ2_DATA_AREA);
		TsQueueManager.read(execContext, ws.getWsTsq2QueueNameFormatted(), ws.getWsTsq2QueueCnt(), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getWsTsq2DataArea().setWsTsq2DataAreaBytes(tsQueueData.getData());
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN DFHRESP(QIDERR)
		//               WHEN DFHRESP(ITEMERR)
		//                    GO TO 4110-PROCESS-TSQ2-X
		//               WHEN OTHER
		//                    GO TO 4110-PROCESS-TSQ2-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-TSQ2 TO TRUE
			ws.getWsSwitches().getTsq2Sw().setEndOfTsq2();
			// COB_CODE: GO TO 4110-PROCESS-TSQ2-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE WS-TSQ2-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq2QueueNameFormatted());
			// COB_CODE: MOVE '4110-PROCESS-TSQ2-X'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4110-PROCESS-TSQ2-X");
			// COB_CODE: MOVE 'READ OF HALOUSDH TSQ2 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF HALOUSDH TSQ2 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4110-PROCESS-TSQ2-X
			return;
		}
		// COB_CODE: MOVE WS-TSQ2-UOW-NM        TO HUFSH-UOW-NM.
		ws.getHufshUowFunSecRow().setUowNm(ws.getWsTsq2DataArea().getUowNm());
		// COB_CODE: MOVE WS-TSQ2-SEC-GRP-NM    TO HUFSH-SEC-GRP-NM.
		ws.getHufshUowFunSecRow().setSecGrpNm(ws.getWsTsq2DataArea().getSecGrpNm());
		// COB_CODE: MOVE WS-TSQ2-CONTEXT       TO HUFSH-CONTEXT.
		ws.getHufshUowFunSecRow().setContext(ws.getWsTsq2DataArea().getContext());
		// COB_CODE: MOVE WS-TSQ2-SEC-ASC-TYP   TO HUFSH-SEC-ASC-TYP.
		ws.getHufshUowFunSecRow().setSecAscTyp(ws.getWsTsq2DataArea().getSecAscTyp());
		// COB_CODE: MOVE WS-TSQ2-FUN-SEQ-NBR   TO HUFSH-FUN-SEQ-NBR.
		ws.getHufshUowFunSecRow().setFunSeqNbr(ws.getWsTsq2DataArea().getFunSeqNbr());
		// COB_CODE: MOVE WS-TSQ2-AUT-ACTION-CD TO HUFSH-AUT-ACTION-CD.
		ws.getHufshUowFunSecRow().setAutActionCd(ws.getWsTsq2DataArea().getAutActionCd());
		// COB_CODE: MOVE WS-TSQ2-AUT-FUN-NM    TO HUFSH-AUT-FUN-NM.
		ws.getHufshUowFunSecRow().setAutFunNm(ws.getWsTsq2DataArea().getAutFunNm());
		// COB_CODE: MOVE WS-TSQ2-LNK-TO-MDU-NM TO HUFSH-LNK-TO-MDU-NM.
		ws.getHufshUowFunSecRow().setLnkToMduNm(ws.getWsTsq2DataArea().getLnkToMduNm());
		// COB_CODE: MOVE WS-TSQ2-APP-NM        TO HUFSH-APP-NM.
		ws.getHufshUowFunSecRow().setAppNm(ws.getWsTsq2DataArea().getAppNm());
		// COB_CODE: PERFORM 4120-EVALUATE-FOR-RET.
		evaluateForRet();
	}

	/**Original name: 4120-EVALUATE-FOR-RET_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS THE RETRIEVED FUNCTION INFORMATION.
	 * ****************************************************************
	 *  IF THE ASSOCIATION TYPE OF THIS FETCHED ROW DOES NOT EQUAL
	 *  SPACES OR DSNE THE ASSOCATION TYPE ASSIGNED TO THIS USERS GROUP,
	 *  THEN DO NOT RETURN THIS ROW TO THE FRONT-END.
	 *  CANNOT MOVE BUS-OBJ-NM OF 'SET_DEFUALTS_HUB' TO FRONT END SINCE
	 *  THIS CALL MAY OR MAY NOT BE RETURNING SET_DEFAULT VALUES.  THE
	 *  PRESENCE OF THE 'SET_DEFAULTS_HUB' ROW WILL INVOKE DEFAULT LOGIC
	 *  ERRONEOUSLY IF WE ARE ONLY RETURNING THE ALLOWED FUNCTIONS.</pre>*/
	private void evaluateForRet() {
		// COB_CODE: MOVE SPACES TO CIDP-OBJECT-ROW
		//                          HUFSC-HAL-UOW-FUN-SEC-ROW.
		ws.getCidpTableInfo().setObjectRow("");
		ws.getHufscHalUowFunSecRow().initHufscHalUowFunSecRowSpaces();
		//*   MOVE 'FUNCTIONS' TO CIDP-OBJECT-NAME
		// COB_CODE: MOVE WS-FUNCTIONS-ROW-NAME TO CIDP-OBJECT-NAME
		//                                         USDH-BUS-OBJ-NM.
		ws.getCidpTableInfo().setObjectName(ws.getWsSpecificWorkAreas().getFunctionsRowName());
		ws.setUsdhBusObjNm(ws.getWsSpecificWorkAreas().getFunctionsRowName());
		// COB_CODE: MOVE UBOC-UOW-NAME         TO HUFSC-UOW-NM.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecKey().setUowNm(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE HUFSH-SEC-GRP-NM      TO HUFSC-SEC-GRP-NM.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecKey().setSecGrpNm(ws.getHufshUowFunSecRow().getSecGrpNm());
		// COB_CODE: MOVE HUFSH-CONTEXT         TO HUFSC-CONTEXT.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecKey().setContext(ws.getHufshUowFunSecRow().getContext());
		// COB_CODE: MOVE HUFSH-SEC-ASC-TYP     TO HUFSC-SEC-ASC-TYP.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecKey().setSecAscTyp(ws.getHufshUowFunSecRow().getSecAscTyp());
		// COB_CODE: MOVE HUFSH-FUN-SEQ-NBR     TO HUFSC-FUN-SEQ-NBR.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecKey().setFunSeqNbr(TruncAbs.toInt(ws.getHufshUowFunSecRow().getFunSeqNbr(), 5));
		// COB_CODE: MOVE HUFSH-AUT-ACTION-CD   TO HUFSC-AUT-ACTION-CD.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecData().setAutActionCd(ws.getHufshUowFunSecRow().getAutActionCd());
		// COB_CODE: MOVE HUFSH-AUT-FUN-NM      TO HUFSC-AUT-FUN-NM.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecData().setAutFunNm(ws.getHufshUowFunSecRow().getAutFunNm());
		// COB_CODE: MOVE HUFSH-APP-NM          TO HUFSC-APP-NM.
		ws.getHufscHalUowFunSecRow().getHalUowFunSecData().setAppNm(ws.getHufshUowFunSecRow().getAppNm());
		// COB_CODE: IF HUFSH-LNK-TO-MDU-NM > SPACES
		//              END-IF
		//           END-IF.
		if (Characters.GT_SPACE.test(ws.getHufshUowFunSecRow().getLnkToMduNm())) {
			// COB_CODE: MOVE HUFSH-LNK-TO-MDU-NM TO WS-DATA-PRIV-CTX-MDU
			ws.getWsWorkFields().setWsDataPrivCtxMdu(ws.getHufshUowFunSecRow().getLnkToMduNm());
			// COB_CODE: PERFORM 4130-LINK-TO-FUN-DP-MOD
			linkToFunDpMod();
			// COB_CODE: IF WS-DO-NOT-RET-FUN-ROW
			//              GO TO 4120-EVALUATE-FOR-RET-X
			//           ELSE
			//              MOVE 'FUNCTIONS' TO CIDP-OBJECT-NAME
			//           END-IF
			if (ws.getWsSwitches().getRetFunRowSw().isDoNotRetFunRow()) {
				// COB_CODE: GO TO 4120-EVALUATE-FOR-RET-X
				return;
			} else {
				// COB_CODE: MOVE 'FUNCTIONS' TO CIDP-OBJECT-NAME
				ws.getCidpTableInfo().setObjectName("FUNCTIONS");
			}
		}
		// COB_CODE: MOVE HUFSC-HAL-UOW-FUN-SEC-ROW
		//             TO UBOC-APP-DATA-BUFFER
		//                CIDP-OBJECT-ROW.
		dfhcommarea.setAppDataBuffer(ws.getHufscHalUowFunSecRow().getHufscHalUowFunSecRowFormatted());
		ws.getCidpTableInfo().setObjectRow(ws.getHufscHalUowFunSecRow().getHufscHalUowFunSecRowFormatted());
		// COB_CODE: MOVE LENGTH OF HUFSC-HAL-UOW-FUN-SEC-ROW
		//             TO UBOC-APP-DATA-BUFFER-LENGTH
		//                CIDP-OBJECT-ROW-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HufscHalUowFunSecRow.Len.HUFSC_HAL_UOW_FUN_SEC_ROW));
		ws.getCidpTableInfo().setObjectRowLength(((short) HufscHalUowFunSecRow.Len.HUFSC_HAL_UOW_FUN_SEC_ROW));
		// COB_CODE: PERFORM 5000-WRITE-RESPONSE-ROWS.
		writeResponseRows();
	}

	/**Original name: 4130-LINK-TO-FUN-DP-MOD_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  A>  LINK TO OBJECT MODULE                                      *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void linkToFunDpMod() {
		// COB_CODE: MOVE 'HAL_UOW_FUN_SEC_V' TO CIDP-TABLE-NAME.
		ws.getCidpTableInfo().setTableName("HAL_UOW_FUN_SEC_V");
		// COB_CODE: MOVE HUFSC-HAL-UOW-FUN-SEC-ROW
		//               TO CIDP-TABLE-ROW.
		ws.getCidpTableInfo().setTableRow(ws.getHufscHalUowFunSecRow().getHufscHalUowFunSecRowFormatted());
		// COB_CODE: MOVE WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER.
		dfhcommarea.setAppDataBuffer(ws.getWsDataPrivacyInfoFormatted());
		// COB_CODE: MOVE LENGTH OF WS-DATA-PRIVACY-INFO
		//             TO UBOC-APP-DATA-BUFFER-LENGTH.
		dfhcommarea.setAppDataBufferLength(((short) HalousdhData.Len.WS_DATA_PRIVACY_INFO));
		// COB_CODE: EXEC CICS LINK
		//                PROGRAM   (WS-DATA-PRIV-CTX-MDU)
		//                COMMAREA  (UBOC-COMM-INFO)
		//                LENGTH    (LENGTH OF UBOC-COMM-INFO)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUSDH", execContext).commarea(dfhcommarea.getCommInfo()).length(UbocCommInfoCaws002.Len.COMM_INFO)
				.link(ws.getWsWorkFields().getWsDataPrivCtxMdu());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN OTHER
		//                   GO TO 4130-LINK-TO-FUN-DP-MOD-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF DPER-DATA-PRIV-CHECK-OK
			//              SET WS-OK-TO-RET-FUN-ROW TO TRUE
			//           ELSE
			//              SET WS-DO-NOT-RET-FUN-ROW TO TRUE
			//           END-IF
			if (dfhcommarea.getCommInfo().getUbocDataPrivRetCode().isDperDataPrivCheckOk()) {
				// COB_CODE: SET WS-OK-TO-RET-FUN-ROW TO TRUE
				ws.getWsSwitches().getRetFunRowSw().setOkToRetFunRow();
			} else {
				// COB_CODE: SET WS-DO-NOT-RET-FUN-ROW TO TRUE
				ws.getWsSwitches().getRetFunRowSw().setDoNotRetFunRow();
			}
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-LINK           TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsLink();
			// COB_CODE: MOVE WS-DATA-PRIV-MDU
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsWorkFields().getWsDataPrivMdu());
			// COB_CODE: MOVE '4130-LINK-TO-FUN-DP-MOD'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4130-LINK-TO-FUN-DP-MOD");
			// COB_CODE: MOVE 'CICS LINK TO BPO FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("CICS LINK TO BPO FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4130-LINK-TO-FUN-DP-MOD-X
			return;
		}
	}

	/**Original name: 4200-EXECUTE-TSQ3-LOOP_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  READ THRU TSQ3 AND WRITE THE SCRIPTS TO THE RESPONSE MSG.
	 * *****************************************************************</pre>*/
	private void executeTsq3Loop() {
		// COB_CODE: SET WS-START-OF-TSQ3 TO TRUE.
		ws.getWsSwitches().getTsq3Sw().setStartOfTsq3();
		// COB_CODE: PERFORM 4210-PROCESS-TSQ3
		//              VARYING WS-TSQ3-QUEUE-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-TSQ3.
		ws.setWsTsq3QueueCnt(((short) 1));
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsSwitches().getTsq3Sw().isEndOfTsq3())) {
			processTsq3();
			ws.setWsTsq3QueueCnt(Trunc.toShort(ws.getWsTsq3QueueCnt() + 1, 4));
		}
	}

	/**Original name: 4210-PROCESS-TSQ3_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ SCRIPT ROWS FROM TSQ3 AND WRITE THEM TO THE UDAT UMT
	 * ****************************************************************</pre>*/
	private void processTsq3() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS
		//                QNAME    (WS-TSQ3-QUEUE-NAME)
		//                INTO     (WS-SCRIPT-ROW)
		//                ITEM     (WS-TSQ3-QUEUE-CNT)
		//                RESP     (WS-RESPONSE-CODE)
		//                RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(HalousdhData.Len.WS_SCRIPT_ROW);
		TsQueueManager.read(execContext, ws.getWsTsq3QueueNameFormatted(), ws.getWsTsq3QueueCnt(), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.setWsScriptRowBytes(tsQueueData.getData());
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    CONTINUE
		//               WHEN DFHRESP(QIDERR)
		//               WHEN DFHRESP(ITEMERR)
		//                    GO TO 4210-PROCESS-TSQ3-X
		//               WHEN OTHER
		//                    GO TO 4210-PROCESS-TSQ3-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-TSQ3 TO TRUE
			ws.getWsSwitches().getTsq3Sw().setEndOfTsq3();
			// COB_CODE: GO TO 4210-PROCESS-TSQ3-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR            TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR       TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE WS-TSQ3-QUEUE-NAME
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq3QueueNameFormatted());
			// COB_CODE: MOVE '4210-PROCESS-TSQ3-X'
			//               TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("4210-PROCESS-TSQ3-X");
			// COB_CODE: MOVE 'READ OF SCRIPTS TSQ3 FAILED'
			//               TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF SCRIPTS TSQ3 FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 4210-PROCESS-TSQ3-X
			return;
		}
		//* WRITE UHDR/UDAT ROW FOR RETRIEVED SCRIPT.
		// COB_CODE: MOVE WS-SCRNSCRPT-ROW-NAME   TO CIDP-OBJECT-NAME.
		ws.getCidpTableInfo().setObjectName(ws.getWsSpecificWorkAreas().getScrnscrptRowName());
		// COB_CODE: MOVE WS-SCRIPT-ROW           TO CIDP-OBJECT-ROW.
		ws.getCidpTableInfo().setObjectRow(ws.getWsScriptRowFormatted());
		// COB_CODE: MOVE LENGTH OF WS-SCRIPT-ROW TO CIDP-OBJECT-ROW-LENGTH.
		ws.getCidpTableInfo().setObjectRowLength(((short) HalousdhData.Len.WS_SCRIPT_ROW));
		// COB_CODE: PERFORM 5000-WRITE-RESPONSE-ROWS.
		writeResponseRows();
	}

	/**Original name: 5000-WRITE-RESPONSE-ROWS_FIRST_SENTENCES<br>
	 * <pre>***********************************************************
	 *  WRITE THE UDAT AND UHDR ROWS FOR THE ROWS RETURNED BY THE
	 *  CALLED DP MODULE.
	 *  WRITE THE UDAT AND UHDR ROWS FOR THIS MODULE IN ORDER TO
	 *  NOTIFY THE MIDDLEWARE AND FRONT-END THAT THE BDO AND BPO
	 *  ROWS CONTAINED IN THE RESPONSE ARE 'DEFAULTED' ROWS.
	 * ***********************************************************</pre>*/
	private void writeResponseRows() {
		Halrresp halrresp = null;
		// COB_CODE: SET HALRRESP-WRITE-FUNC TO TRUE.
		ws.getWsHalrrespLinkage().getFunction().setHalrurqaWriteFunc();
		// COB_CODE: MOVE CIDP-OBJECT-NAME   TO HALRRESP-BUS-OBJ-NM.
		ws.getWsHalrrespLinkage().setBusObjNm(ws.getCidpTableInfo().getObjectName());
		// COB_CODE: MOVE CIDP-OBJECT-ROW-LENGTH
		//                                   TO HALRRESP-BUS-OBJ-DATA-LEN.
		ws.getWsHalrrespLinkage().setBusObjDataLen(TruncAbs.toShort(ws.getCidpTableInfo().getObjectRowLength(), 4));
		// COB_CODE: CALL HALRRESP-HALRRESP-LIT USING
		//                DFHEIBLK
		//                DFHCOMMAREA
		//                UBOC-RECORD
		//                WS-HALRRESP-LINKAGE
		//                CIDP-OBJECT-ROW.
		halrresp = Halrresp.getInstance();
		halrresp.run(execContext, dfhcommarea, dfhcommarea, ws.getWsHalrrespLinkage(),
				new BasicBytesClass(ws.getCidpTableInfo().getArray(), CidpTableInfo.Pos.OBJECT_ROW - 1));
		// COB_CODE: IF UBOC-HALT-AND-RETURN
		//               GO TO 5000-WRITE-RESPONSE-ROWS-X
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
			// COB_CODE: GO TO 5000-WRITE-RESPONSE-ROWS-X
			return;
		}
	}

	/**Original name: 5100-READ-SWITCH-UMT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ SWITCH UMT FOR SPECIFIC ENTRY.
	 * ****************************************************************</pre>*/
	private void readSwitchUmt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: MOVE UBOC-MSG-ID TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE WS-TSQ1-BOBJ-NM TO USW-BUS-OBJ-SWITCH
		//                                   WS-SWITCH-OBJECT.
		ws.getHallusw().setBusObjSwitch(ws.getWsTsq1BobjNm());
		ws.getWsBdoWorkFields().setSwitchObject(ws.getWsTsq1BobjNm());
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//               CONTINUE
		//           ELSE
		//               GO TO 5100-READ-SWITCH-UMT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesTsq())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
			// COB_CODE: SET WS-READING-FOR-OBJECT     TO TRUE
			ws.getWsBdoSwitches().getSwitchesTypeSw().setObjectFld();
			// COB_CODE: PERFORM 5150-CHECK-SWITCH-TSQ
			checkSwitchTsq();
			// COB_CODE: GO TO 5100-READ-SWITCH-UMT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//                INTO      (USW-COMMON)
		//                RIDFLD    (USW-KEY)
		//                KEYLENGTH (LENGTH OF USW-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Hallusw.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   SET MATCHING-SWITCH-FOUND TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                   SET MATCHING-SWITCH-NOT-FOUND TO TRUE
		//               WHEN OTHER
		//                   GO TO 5100-READ-SWITCH-UMT-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET MATCHING-SWITCH-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setFound();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET MATCHING-SWITCH-NOT-FOUND TO TRUE
			ws.getWsBdoSwitches().getMatchingSwitchSw().setNotFound();
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: MOVE '5100-READ-SWITCH-UMT'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5100-READ-SWITCH-UMT");
			// COB_CODE: MOVE 'READ UOW REQ SWITCHES STORE FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ UOW REQ SWITCHES STORE FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'USW-ID='             USW-ID             ';'
			//                  'USW-BUS-OBJ-SWITCH=' USW-BUS-OBJ-SWITCH ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("USW-ID=").append(ws.getHallusw().getIdFormatted()).append(";")
							.append("USW-BUS-OBJ-SWITCH=").append(ws.getHallusw().getBusObjSwitchFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5100-READ-SWITCH-UMT-X
			return;
		}
	}

	/**Original name: 5120-CHECK-MSTR-SWT_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  LOOK FOR A MASTER SWITCH OF 'RETURN ALL' TO INDICATE THAT ALL
	 *  MODULES LISTED IN THE HIERARCHY TABLE SHOULD BE CALLED.
	 * ****************************************************************</pre>*/
	private void checkMstrSwt() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: IF READ-FOR-MSTR-SWT-YES
		//               GO TO 5120-CHECK-MSTR-SWT-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReadForMstrSwtInd().isYes()) {
			// COB_CODE: GO TO 5120-CHECK-MSTR-SWT-X
			return;
		}
		// COB_CODE: MOVE UBOC-MSG-ID TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE 'RETURN ALL' TO USW-BUS-OBJ-SWITCH
		//                                WS-SWITCH-OBJECT.
		ws.getHallusw().setBusObjSwitch("RETURN ALL");
		ws.getWsBdoWorkFields().setSwitchObject("RETURN ALL");
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//               CONTINUE
		//           ELSE
		//               GO TO 5120-CHECK-MSTR-SWT-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesTsq())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET MASTER-SWITCH-NOT-SET TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setNotSet();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
			// COB_CODE: SET WS-READING-FOR-MASTER TO TRUE
			ws.getWsBdoSwitches().getSwitchesTypeSw().setMaster();
			// COB_CODE: PERFORM 5150-CHECK-SWITCH-TSQ
			checkSwitchTsq();
			// COB_CODE: GO TO 5120-CHECK-MSTR-SWT-X
			return;
		}
		// COB_CODE: EXEC CICS
		//                READ FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//                INTO      (USW-COMMON)
		//                RIDFLD    (USW-KEY)
		//                KEYLENGTH (LENGTH OF USW-KEY)
		//                RESP      (WS-RESPONSE-CODE)
		//                RESP2     (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.select(iRowData, KeyType.EQUAL, Hallusw.Len.KEY, false);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                    SET READ-FOR-MSTR-SWT-YES TO TRUE
		//               WHEN DFHRESP(NOTFND)
		//                    SET READ-FOR-MSTR-SWT-YES TO TRUE
		//               WHEN OTHER
		//                    GO TO 5120-CHECK-MSTR-SWT-X
		//              END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET MASTER-SWITCH-NOT-SET TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setNotSet();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
		} else {
			// COB_CODE: SET WS-LOG-ERROR        TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED    TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-UMT  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadUmt();
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: MOVE '5120-CHECK-MSTR-SWT'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5120-CHECK-MSTR-SWT");
			// COB_CODE: MOVE 'READ UOW REQ SWITCHES STORE FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ UOW REQ SWITCHES STORE FAILED");
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'USW-ID='             USW-ID             ';'
			//                  'USW-BUS-OBJ-SWITCH=' USW-BUS-OBJ-SWITCH ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("USW-ID=").append(ws.getHallusw().getIdFormatted()).append(";")
							.append("USW-BUS-OBJ-SWITCH=").append(ws.getHallusw().getBusObjSwitchFormatted()).append(";").toString());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5120-CHECK-MSTR-SWT-X
			return;
		}
	}

	/**Original name: 5130-CHECK-FOR-NO-SWITCHES_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  DETERMINE IF ANY INDIVIDUAL SWITHCES EXIST.
	 * ****************************************************************</pre>*/
	private void checkForNoSwitches() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF READ-FOR-MSTR-SWT-YES
		//               GO TO 5130-CHECK-FOR-NO-SWITCHES-X
		//           END-IF.
		if (ws.getWsBdoSwitches().getReadForMstrSwtInd().isYes()) {
			// COB_CODE: GO TO 5130-CHECK-FOR-NO-SWITCHES-X
			return;
		}
		// COB_CODE: IF UBOC-UOW-REQ-SWITCHES-TSQ EQUAL SPACES
		//               CONTINUE
		//           ELSE
		//               GO TO 5130-CHECK-FOR-NO-SWITCHES-X
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesTsq())) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 5130-CHECK-FOR-NO-SWITCHES-X
			return;
		}
		// COB_CODE: MOVE UBOC-MSG-ID TO USW-ID.
		ws.getHallusw().setId(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: MOVE LOW-VALUES TO USW-BUS-OBJ-SWITCH.
		ws.getHallusw().setBusObjSwitch(LiteralGenerator.create(Types.LOW_CHAR_VAL, Hallusw.Len.BUS_OBJ_SWITCH));
		// COB_CODE: EXEC CICS
		//               STARTBR FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//               RIDFLD       (USW-KEY)
		//               KEYLENGTH    (LENGTH OF USW-KEY)
		//               GTEQ
		//               RESP         (WS-RESPONSE-CODE)
		//               RESP2        (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowDAO.openCursor(iRowData, KeyType.GREATER_OR_EQ, Hallusw.Len.KEY);
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   GO TO 5130-CHECK-FOR-NO-SWITCHES-X
		//               WHEN DFHRESP(NOTFND)
		//                   GO TO 5130-CHECK-FOR-NO-SWITCHES-X
		//               WHEN OTHER
		//                   GO TO 5130-CHECK-FOR-NO-SWITCHES-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 5135-READNEXT-SWITCH-ROW
			readnextSwitchRow();
			// COB_CODE: IF NOT UBOC-HALT-AND-RETURN
			//              PERFORM 5140-END-BROWSE
			//           END-IF
			if (!dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()) {
				// COB_CODE: PERFORM 5140-END-BROWSE
				endBrowse();
			}
			// COB_CODE: GO TO 5130-CHECK-FOR-NO-SWITCHES-X
			return;
		} else if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NOTFND) {
			// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
			ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
			// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
			ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
			// COB_CODE: GO TO 5130-CHECK-FOR-NO-SWITCHES-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-STARTBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsStartbrUmt();
			// COB_CODE: STRING 'USW-KEY = '  USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_OBJ_DATA_KEY, "USW-KEY = ", ws.getHallusw().getKeyFormatted(), ";");
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalObjDataKeyFormatted()));
			// COB_CODE: MOVE '5130-CHECK-FOR-NO-SWITCHES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5130-CHECK-FOR-NO-SWITCHES");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM STARTBR'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM STARTBR");
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5130-CHECK-FOR-NO-SWITCHES-X
			return;
		}
	}

	/**Original name: 5135-READNEXT-SWITCH-ROW_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  PROCESS SWITCH UMT.
	 * ****************************************************************</pre>*/
	private void readnextSwitchRow() {
		IRowDAO iRowDAO = null;
		IRowData iRowData = null;
		// COB_CODE: EXEC CICS
		//               READNEXT FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//               INTO          (USW-COMMON)
		//               RIDFLD        (USW-KEY)
		//               KEYLENGTH     (LENGTH OF USW-KEY)
		//               RESP          (WS-RESPONSE-CODE)
		//               RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.getRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowData = iRowDAO.createQueryTO();
			iRowData.setKey(ws.getHallusw().getKeyBytes());
			iRowData = iRowDAO.fetchNext(iRowData, Hallusw.Len.KEY);
			if (iRowDAO.getStatus().isSuccess()) {
				ws.getHallusw().setKeyBytes(iRowData.getKey());
				ws.getHallusw().setCommonBytes(iRowData.getData());
			}
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   END-IF
		//               WHEN OTHER
		//                   GO TO 5135-READNEXT-SWITCH-ROW-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: IF USW-ID NOT EQUAL UBOC-MSG-ID
			//              GO TO 5135-READNEXT-SWITCH-ROW-X
			//           END-IF
			if (!Conditions.eq(ws.getHallusw().getId(), dfhcommarea.getCommInfo().getUbocMsgId())) {
				// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
				ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
				// COB_CODE: SET READ-FOR-MSTR-SWT-YES TO TRUE
				ws.getWsBdoSwitches().getReadForMstrSwtInd().setYes();
				// COB_CODE: GO TO 5135-READNEXT-SWITCH-ROW-X
				return;
			}
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READNXT-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadnxtUmt();
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'USW-KEY = '  USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("USW-KEY = ").append(ws.getHallusw().getKeyFormatted()).append(";").toString());
			// COB_CODE: MOVE '5135-READNEXT-SWITCH-ROW'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5135-READNEXT-SWITCH-ROW");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM READNXT UMT'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM READNXT UMT");
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5135-READNEXT-SWITCH-ROW-X
			return;
		}
	}

	/**Original name: 5140-END-BROWSE_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  END PROCESSING OF SWITCH UMT.
	 * ****************************************************************</pre>*/
	private void endBrowse() {
		IRowDAO iRowDAO = null;
		// COB_CODE: EXEC CICS
		//               ENDBR FILE (UBOC-UOW-REQ-SWITCHES-STORE)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		iRowDAO = RowDAOFactory.removeRowDAO(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesStoreFormatted());
		if (iRowDAO != null) {
			iRowDAO.close();
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-ENDBR-UMT TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsEndbrUmt();
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'USW-KEY = ' USW-KEY ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("USW-KEY = ").append(ws.getHallusw().getKeyFormatted()).append(";").toString());
			// COB_CODE: MOVE '5140-END-BROWSE' TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5140-END-BROWSE");
			// COB_CODE: MOVE 'ENDBR OF SWITCH DATA UMT FAILED'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("ENDBR OF SWITCH DATA UMT FAILED");
			// COB_CODE: MOVE UBOC-UOW-REQ-SWITCHES-STORE
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 5150-CHECK-SWITCH-TSQ_FIRST_SENTENCES<br>
	 * <pre>****************************************************************
	 *  READ ROWS FROM THE UOW SWITCHES TSQ
	 * ****************************************************************</pre>*/
	private void checkSwitchTsq() {
		// COB_CODE: SET WS-START-OF-SWITCHES-TSQ TO TRUE.
		ws.getWsBdoSwitches().getSwitchesTsqSw().setStartOfSwitchesTsq();
		// COB_CODE: PERFORM 5155-READ-ALL-SWITCH-Q-ROWS
		//              VARYING WS-SWITCH-TSQ-CNT
		//              FROM 1 BY 1
		//              UNTIL UBOC-HALT-AND-RETURN
		//                 OR WS-END-OF-SWITCHES-TSQ.
		ws.getWsBdoWorkFields().setSwitchTsqCnt(((short) 1));
		while (!(dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().isUbocHaltAndReturn()
				|| ws.getWsBdoSwitches().getSwitchesTsqSw().isEndOfSwitchesTsq())) {
			readAllSwitchQRows();
			ws.getWsBdoWorkFields().setSwitchTsqCnt(Trunc.toShort(ws.getWsBdoWorkFields().getSwitchTsqCnt() + 1, 4));
		}
	}

	/**Original name: 5155-READ-ALL-SWITCH-Q-ROWS_FIRST_SENTENCES<br>*/
	private void readAllSwitchQRows() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//                READQ TS QNAME(UBOC-UOW-REQ-SWITCHES-TSQ)
		//                INTO          (USW-COMMON)
		//                ITEM          (WS-SWITCH-TSQ-CNT)
		//                RESP          (WS-RESPONSE-CODE)
		//                RESP2         (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(Hallusw.Len.COMMON);
		TsQueueManager.read(execContext, dfhcommarea.getCommInfo().getUbocUowReqSwitchesTsqFormatted(), ws.getWsBdoWorkFields().getSwitchTsqCnt(),
				tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getHallusw().setCommonBytes(tsQueueData.getData());
		}
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//             WHEN DFHRESP(NORMAL)
		//               CONTINUE
		//             WHEN DFHRESP(QIDERR)
		//             WHEN DFHRESP(ITEMERR)
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//             WHEN OTHER
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//           END-EVALUATE.
		if (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL) {
			// COB_CODE: CONTINUE
			//continue
		} else if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.ITEMERR)) {
			// COB_CODE: SET WS-END-OF-SWITCHES-TSQ TO TRUE
			ws.getWsBdoSwitches().getSwitchesTsqSw().setEndOfSwitchesTsq();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		} else {
			// COB_CODE: SET WS-LOG-ERROR             TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR        TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED         TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-READ-TSQ      TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsReadTsq();
			// COB_CODE: MOVE UBOC-UOW-LOCK-PROC-TSQ
			//                TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(dfhcommarea.getCommInfo().getUbocUowLockProcTsq());
			// COB_CODE: MOVE '5155-READ-ALL-SWITCH-Q-ROWS'
			//                TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("5155-READ-ALL-SWITCH-Q-ROWS");
			// COB_CODE: MOVE 'READ OF SWITCHES TSQ FAILED'
			//                TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("READ OF SWITCHES TSQ FAILED");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		}
		// COB_CODE: IF (UBOC-MSG-ID EQUAL USW-ID)
		//             AND (WS-SWITCH-OBJECT EQUAL USW-BUS-OBJ-SWITCH)
		//               GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
		//           END-IF.
		if (Conditions.eq(dfhcommarea.getCommInfo().getUbocMsgId(), ws.getHallusw().getId())
				&& Conditions.eq(ws.getWsBdoWorkFields().getSwitchObject(), ws.getHallusw().getBusObjSwitch())) {
			// COB_CODE: IF WS-READING-FOR-OBJECT
			//               SET MATCHING-SWITCH-FOUND TO TRUE
			//           ELSE
			//               SET MASTER-SWITCH-SET     TO TRUE
			//           END-IF
			if (ws.getWsBdoSwitches().getSwitchesTypeSw().isObjectFld()) {
				// COB_CODE: SET MATCHING-SWITCH-FOUND TO TRUE
				ws.getWsBdoSwitches().getMatchingSwitchSw().setFound();
			} else {
				// COB_CODE: SET MASTER-SWITCH-SET     TO TRUE
				ws.getWsBdoSwitches().getMasterSwitchInd().setSetFld();
			}
			// COB_CODE: SET WS-END-OF-SWITCHES-TSQ TO TRUE
			ws.getWsBdoSwitches().getSwitchesTsqSw().setEndOfSwitchesTsq();
			// COB_CODE: GO TO 5155-READ-ALL-SWITCH-Q-ROWS-X
			return;
		}
	}

	/**Original name: 8000-DELETE-QUEUES_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *  DELETE THE QUEUES USED DURING DP/SD PROCESSING BY THIS MODULE.
	 * *****************************************************************</pre>*/
	private void deleteQueues() {
		// COB_CODE: EXEC CICS
		//               DELETEQ TS
		//               QNAME       (WS-TSQ1-QUEUE-NAME)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWsTsq1QueueNameFormatted());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   GO TO 8000-DELETE-QUEUES-X
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'TSQ NAME= '   WS-TSQ1-QUEUE-NAME ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("TSQ NAME= ").append(ws.getWsTsq1QueueNameFormatted()).append(";").toString());
			// COB_CODE: MOVE '8000-DELETE-QUEUES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8000-DELETE-QUEUES");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ1'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ1");
			// COB_CODE: MOVE WS-TSQ1-QUEUE-NAME
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq1QueueNameFormatted());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
			// COB_CODE: GO TO 8000-DELETE-QUEUES-X
			return;
		}
		// COB_CODE: EXEC CICS
		//               DELETEQ TS
		//               QNAME       (WS-TSQ2-QUEUE-NAME)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWsTsq2QueueNameFormatted());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'TSQ NAME= '   WS-TSQ2-QUEUE-NAME ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("TSQ NAME= ").append(ws.getWsTsq2QueueNameFormatted()).append(";").toString());
			// COB_CODE: MOVE '8000-DELETE-QUEUES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8000-DELETE-QUEUES");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ2'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ2");
			// COB_CODE: MOVE WS-TSQ2-QUEUE-NAME
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq2QueueNameFormatted());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
		// COB_CODE: EXEC CICS
		//               DELETEQ TS
		//               QNAME       (WS-TSQ3-QUEUE-NAME)
		//               RESP        (WS-RESPONSE-CODE)
		//               RESP2       (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TsQueueManager.delete(execContext, ws.getWsTsq3QueueNameFormatted());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		// COB_CODE: EVALUATE WS-RESPONSE-CODE
		//               WHEN DFHRESP(NORMAL)
		//               WHEN DFHRESP(QIDERR)
		//                   CONTINUE
		//               WHEN OTHER
		//                   PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-EVALUATE.
		if ((TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.NORMAL)
				|| (TpConditionType.valueOf(ws.getWsNotSpecificMisc().getResponseCode()) == TpConditionType.QIDERR)) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: SET WS-LOG-ERROR      TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-SYSTEM-ERROR TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSystemError();
			// COB_CODE: SET EFAL-CICS-FAILED TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsFailed();
			// COB_CODE: SET ETRA-CICS-DELETE-TSQ TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEtraCicsDeleteTsq();
			// COB_CODE: MOVE SPACES TO EFAL-OBJ-DATA-KEY
			//skipped translation for moving SPACES to EFAL-OBJ-DATA-KEY; considered in STRING statement translation below
			// COB_CODE: STRING 'TSQ NAME= '   WS-TSQ3-QUEUE-NAME ';'
			//               DELIMITED BY SIZE
			//               INTO EFAL-OBJ-DATA-KEY
			//           END-STRING
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalObjDataKey(new StringBuffer(256).append("TSQ NAME= ").append(ws.getWsTsq3QueueNameFormatted()).append(";").toString());
			// COB_CODE: MOVE '8000-DELETE-QUEUES'
			//             TO EFAL-ERR-PARAGRAPH
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("8000-DELETE-QUEUES");
			// COB_CODE: MOVE 'BAD EIBRESP RETD FROM DELETE TSQ3'
			//             TO EFAL-ERR-COMMENT
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("BAD EIBRESP RETD FROM DELETE TSQ3");
			// COB_CODE: MOVE WS-TSQ3-QUEUE-NAME
			//             TO EFAL-ERR-OBJECT-NAME
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName(ws.getWsTsq3QueueNameFormatted());
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	/**Original name: 9000-LOG-WARNING-OR-ERROR_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  CALL HALOESTO TO LOG WARNING OR ERROR.                         *
	 *                                                                 *
	 * *****************************************************************
	 * * ONLY LOG IF:
	 * * A. A WARNING AND NOTHING BEEN LOGGED BEFORE OR
	 * * B. AN ERROR AND NO ERRORS LOGGED BEFORE</pre>*/
	private void logWarningOrError() {
		ConcatUtil concatUtil = null;
		// COB_CODE: IF NOT ( (WS-LOG-WARNING AND UBOC-UOW-OK) OR
		//                    (WS-LOG-ERROR   AND NOT UBOC-UOW-LOGGABLE-ERRORS) )
		//               GO TO 9000-LOG-WARNING-OR-ERROR-X
		//           END-IF.
		if (!(ws.getWsLogWarningOrErrorSw().isWarning() && dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isOk()
				|| ws.getWsLogWarningOrErrorSw().isError()
						&& !dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors())) {
			// COB_CODE: GO TO 9000-LOG-WARNING-OR-ERROR-X
			return;
		}
		// COB_CODE: IF WS-PROGRAM-NAME(1:3) = 'HAL'
		//               SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
		//             ELSE
		//               SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
		//           END-IF.
		if (Conditions.eq(ws.getWsSpecificWorkAreas().getProgramNameFormatted().substring((1) - 1, 3), "HAL")) {
			// COB_CODE: SET EFAL-S3-SAVARCH  OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savarch();
		} else {
			// COB_CODE: SET EFAL-S3-SAVANNAH OF WS-ESTO-INFO  TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalS3Savannah();
		}
		// COB_CODE: SET ESTO-FAILURE-LEVEL OF WS-ESTO-INFO    TO TRUE.
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setFailureLevel();
		// COB_CODE: SET ESTO-FAILURE-ROW-TYPE OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoCallEtraSw().setFailureRowType();
		// COB_CODE: MOVE '+'    TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
		//                          EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
		//                          EFAL-SEC-SYS-ID-SIGN OF WS-ESTO-INFO
		//                          EFAL-ETRA-PRIORITY-LEVEL-SIGN OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysIdSignFormatted("+");
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalEtraPriorityLevelSignFormatted("+");
		// COB_CODE: EVALUATE TRUE
		//               WHEN EFAL-DB2-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN EFAL-CICS-FAILED OF WS-ESTO-INFO
		//                         TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//               WHEN OTHER
		//                   MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
		//           END-EVALUATE.
		switch (ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedActionType()) {

		case EstoDetailBuffer.EFAL_DB2_FAILED:// COB_CODE: IF SQLCODE IS POSITIVE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (sqlca.getSqlcode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-DB2-ERR-SQLCODE-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcodeSignFormatted("-");
			}
			// COB_CODE: MOVE SQLCODE
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(TruncAbs.toLong(sqlca.getSqlcode(), 10));
			// COB_CODE: MOVE SQLERRMC
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc(sqlca.getSqlerrmc());
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;

		case EstoDetailBuffer.EFAL_CICS_FAILED:// COB_CODE: MOVE ZERO
			//                 TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES
			//                 TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: IF WS-RESPONSE-CODE IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrRespSignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE
			//                 TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode(), 10));
			// COB_CODE: IF WS-RESPONSE-CODE2 IS POSITIVE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//             ELSE
			//                 TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
			//           END-IF
			if (ws.getWsNotSpecificMisc().getResponseCode2() > 0) {
				// COB_CODE: MOVE '+'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("+");
			} else {
				// COB_CODE: MOVE '-'
				//             TO EFAL-CICS-ERR-RESP2-SIGN OF WS-ESTO-INFO
				ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2SignFormatted("-");
			}
			// COB_CODE: MOVE WS-RESPONSE-CODE2
			//                 TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(TruncAbs.toLong(ws.getWsNotSpecificMisc().getResponseCode2(), 10));
			break;

		default:// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlcode(0);
			// COB_CODE: MOVE SPACES  TO EFAL-DB2-ERR-SQLERRMC OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalDb2ErrSqlerrmc("");
			// COB_CODE: MOVE SPACES  TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrObjectName("");
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp(0);
			// COB_CODE: MOVE ZERO    TO EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCicsErrResp2(0);
			break;
		}
		// COB_CODE: MOVE UBOC-MSG-ID     TO ESTO-STORE-ID OF WS-ESTO-INFO
		//                                   EFAL-FAIL-LVL-GUID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoInputKey().setStoreId(dfhcommarea.getCommInfo().getUbocMsgId());
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailLvlGuid(dfhcommarea.getCommInfo().getUbocMsgId());
		// COB_CODE: IF WS-LOG-WARNING
		//               SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
		//           ELSE
		//               SET UBOC-HALT-AND-RETURN       TO TRUE
		//           END-IF.
		if (ws.getWsLogWarningOrErrorSw().isWarning()) {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-WARNINGS TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableWarnings();
		} else {
			// COB_CODE: SET UBOC-UOW-LOGGABLE-ERRORS   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().setLoggableErrors();
			// COB_CODE: SET UBOC-HALT-AND-RETURN       TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocProcessingStatusSw().setHaltAndReturn();
		}
		// COB_CODE: MOVE WS-PROGRAM-NAME  TO EFAL-FAILED-MODULE OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedModule(ws.getWsSpecificWorkAreas().getProgramName());
		// COB_CODE: MOVE COM-SEC-SYS-ID   TO EFAL-SEC-SYS-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalSecSysId(TruncAbs.toLong(ws.getHallcom().getSecSysId(), 10));
		// COB_CODE: MOVE UBOC-UOW-NAME    TO EFAL-UNIT-OF-WORK OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalUnitOfWork(dfhcommarea.getCommInfo().getUbocUowName());
		// COB_CODE: MOVE UBOC-AUTH-USERID TO EFAL-LOGON-USERID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalLogonUserid(dfhcommarea.getCommInfo().getUbocAuthUserid());
		// COB_CODE: SET EFAL-MAINFRAME OF WS-ESTO-INFO TO TRUE.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalMainframe();
		// COB_CODE: EXEC CICS ASSIGN
		//              APPLID(WS-APPLID)
		//           END-EXEC.
		ws.setWsApplid(execContext.getApplicationId());
		execContext.clearStatus();
		// COB_CODE: MOVE WS-APPLID TO EFAL-FAILED-LOCATION-ID OF WS-ESTO-INFO.
		ws.getWsEstoInfo().getEstoDetailBuffer().setEfalFailedLocationId(ws.getWsApplid());
		// COB_CODE: EXEC CICS LINK
		//             PROGRAM  ('HALOESTO')
		//             COMMAREA (WS-ESTO-INFO)
		//             LENGTH   (LENGTH OF WS-ESTO-INFO)
		//             RESP     (WS-RESPONSE-CODE)
		//             RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("HALOUSDH", execContext).commarea(ws.getWsEstoInfo()).length(WsEstoInfo.Len.WS_ESTO_INFO).link("HALOESTO", new Haloesto());
		ws.getWsNotSpecificMisc().setResponseCode(execContext.getResp());
		ws.getWsNotSpecificMisc().setResponseCode2(execContext.getResp2());
		//* IF THE LOGGABLE FAILURE OCCURRED IN THE MESSAGE CONTROL MODULE
		//* AND ERROR LOGGING WAS SUCCESSFUL, THEN SET THE "SUCCESS" FLAG
		//* TO TRUE.  IF THE LOGGABLE FAILURE OCCURRED AT THE OBJECT LEVEL
		//* THE "LOGGING SUCCESSFUL" OR "LOGGING FAILED" FLAG WHOULD HAVE
		//* ALREADY BEEN SET.  WE ONLY WANT TO OVERRIDE THAT SETTING IF
		//* ERROR LOGGING FAILS AT THIS LEVEL.  THEREFORE, IF ERROR LOGGING
		//* FAILED AT THE OBJECT LEVEL, BUT WAS SUCCESSFUL AT THIS LEVEL,
		//* THE "LOGGING FAILED" FLAG SHOULD REMAIN ON.
		// DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		// COB_CODE:      EVALUATE WS-RESPONSE-CODE
		//           * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
		//                    WHEN 0
		//                        CONTINUE
		//                    WHEN OTHER
		//           ** IF HALOESTO LINK ERROR THEN RECORD
		//           ** THE DETAILS IN MAINDRIVER COMMAREA
		//                                     UBOC-ERR-LOG-SQLCODE-DSPLY
		//                END-EVALUATE.
		switch (ws.getWsNotSpecificMisc().getResponseCode()) {

		case 0:// COB_CODE: CONTINUE
			//continue
			break;

		default://* IF HALOESTO LINK ERROR THEN RECORD
			//* THE DETAILS IN MAINDRIVER COMMAREA
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-WITHIN-BUS-OBJ     TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setWithinBusObj();
			// COB_CODE: MOVE WS-RESPONSE-CODE       TO
			//                        UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsNotSpecificMisc().getResponseCode());
			// COB_CODE: MOVE WS-RESPONSE-CODE2      TO
			//                        UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsNotSpecificMisc().getResponseCode2());
			// COB_CODE: MOVE ZERO                   TO
			//                        UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().setErrLogSqlcodeDsply(0);
			break;
		}
		//* CHECK THE CURRENT STATE OF THE 'LOG ONLY' SWITCH
		//* AND APPLY ERROR FLOOD IND AS REQUIRED
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-LOG-ONLY-NOT-SET
		//                 OR UBOC-LOG-ONLY-NOT-REQUIRED
		//                   END-IF
		//               WHEN UBOC-LOG-ONLY-REQUIRED
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//               WHEN OTHER
		//                   SET UBOC-LOG-ONLY-REQUIRED TO TRUE
		//           END-EVALUATE.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotSet()
				|| dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isNotRequired()) {
			// COB_CODE: IF ESTO-NOT-ERROR-FLOOD
			//              SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			//           ELSE
			//              SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
			//           END-IF
			if (ws.getWsEstoInfo().getEstoOutput().getFloodInd().isNotErrorFlood()) {
				// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
			} else {
				// COB_CODE: SET UBOC-LOG-ONLY-NOT-REQUIRED TO TRUE
				dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setNotRequired();
			}
		} else if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().isRequired()) {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		} else {
			// COB_CODE: SET UBOC-LOG-ONLY-REQUIRED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocLoggableErrLogOnlySw().setRequired();
		}
		//* IF HALOESTO HAD A PROBLEM STORING THE RECORD, THEN RECORD
		//* THE DETAILS IN UBOC COMMAREA
		// COB_CODE: IF NOT ESTO-TRAN-AND-STORAGE-OK OF WS-ESTO-INFO
		//                      INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
		//            END-IF.
		if (!ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().isTranAndStorageOk()) {
			// COB_CODE: SET UBOC-ERR-LOGGING-FAILED TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorsLoggedSw().setFailed();
			// COB_CODE: SET UBOC-INTERMEDIATE-LOG   TO TRUE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo().getErrorLoggingLvlSw().setIntermediateLog();
			// COB_CODE: MOVE ESTO-ERR-RESP-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibrespDsply(ws.getWsEstoInfo().getEstoOutput().getRespCd());
			// COB_CODE: MOVE ESTO-ERR-RESP2-CD OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-EIBRESP2-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogEibresp2Dsply(ws.getWsEstoInfo().getEstoOutput().getResp2Cd());
			// COB_CODE: MOVE ESTO-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-ERR-LOG-SQLCODE-DSPLY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocErrorLoggingInfo()
					.setErrLogSqlcodeDsply(ws.getWsEstoInfo().getEstoOutput().getSqlcode());
			// COB_CODE: STRING 'ERROR LOGGING FAILURE CODES :- '
			//                  ESTO-ERR-STORE-RETURN-CD OF WS-ESTO-INFO
			//                  ESTO-ERR-STORE-DETAIL-CD OF WS-ESTO-INFO
			//                  DELIMITED BY SIZE
			//                  INTO EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			concatUtil = ConcatUtil.buildString(EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT, "ERROR LOGGING FAILURE CODES :- ",
					ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().getStoreReturnCdAsString(),
					ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().getStoreDetailCdAsString());
			ws.getWsEstoInfo().getEstoDetailBuffer()
					.setEfalEtraErrorTxt(concatUtil.replaceInString(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxtFormatted()));
		}
		//* COPY ERROR DETAILS TO UBOC FOR LOGGABLE ERRORS ONLY
		// COB_CODE: IF UBOC-UOW-LOGGABLE-ERRORS
		//                                         TO UBOC-APP-DATA-BUFFER-LENGTH
		//           END-IF.
		if (dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectLoggableProblems().isLoggableErrors()) {
			// COB_CODE: MOVE EFAL-ETRA-ERROR-REF OF WS-ESTO-INFO
			//             TO UBOC-ERROR-CODE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setErrorCode(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorRef());
			// COB_CODE: MOVE EFAL-FAILED-MODULE OF WS-ESTO-INFO
			//             TO UBOC-FAILED-MODULE
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedModule(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalFailedModule());
			// COB_CODE: MOVE EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			//             TO UBOC-FAILED-PARAGRAPH
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setFailedParagraph(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalErrParagraph());
			// COB_CODE: MOVE EFAL-DB2-ERR-SQLCODE OF WS-ESTO-INFO
			//             TO UBOC-SQLCODE-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setSqlcodeDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalDb2ErrSqlcode());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibrespDisplay(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp());
			// COB_CODE: MOVE EFAL-CICS-ERR-RESP2 OF WS-ESTO-INFO
			//             TO UBOC-EIBRESP2-DISPLAY
			dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo()
					.setEibresp2Display(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalCicsErrResp2());
			// COB_CODE: MOVE EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//             TO UBOC-APP-DATA-BUFFER
			dfhcommarea.setAppDataBuffer(ws.getWsEstoInfo().getEstoDetailBuffer().getEfalEtraErrorTxt());
			// COB_CODE: MOVE LENGTH OF EFAL-ETRA-ERROR-TXT OF WS-ESTO-INFO
			//                                     TO UBOC-APP-DATA-BUFFER-LENGTH
			dfhcommarea.setAppDataBufferLength(((short) EstoDetailBuffer.Len.EFAL_ETRA_ERROR_TXT));
		}
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
	}

	/**Original name: IWAE-INITIALIZE-WARN-MSG_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************00004100
	 *                                                                 *00005000
	 *  INITIALIZE ERROR/WARNING STORAGE.                              *00006000
	 *                                                                 *00007000
	 * *****************************************************************00008000</pre>*/
	private void iwaeInitializeWarnMsg() {
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-MODULE.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedModule("");
		// COB_CODE: MOVE SPACES TO UBOC-FAILED-PARAGRAPH.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setFailedParagraph("");
		// COB_CODE: MOVE ZERO   TO UBOC-SQLCODE-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setSqlcodeDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibrespDisplay(0);
		// COB_CODE: MOVE ZERO   TO UBOC-EIBRESP2-DISPLAY.
		dfhcommarea.getCommInfo().getUbocErrorDetails().getUbocObjectErrorInfo().setEibresp2Display(0);
		// COB_CODE: INITIALIZE ESTO-STORE-INFO.
		initEstoStoreInfo();
		// COB_CODE: INITIALIZE ESTO-RETURN-INFO.
		initEstoReturnInfo();
		// COB_CODE: INITIALIZE WS-WARNING-UMT-AREA.
		initWsWarningUmtArea();
		// COB_CODE: INITIALIZE WS-NLBE-UMT-AREA.
		initWsNlbeUmtArea();
		// COB_CODE: MOVE UBOC-NBR-WARNINGS TO UWRN-REC-SEQ.
		ws.getUwrnCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrWarningsFormatted());
		// COB_CODE: MOVE UBOC-NBR-NONLOG-BL-ERRS TO NLBE-REC-SEQ.
		ws.getNlbeCommon().setRecSeqFormatted(dfhcommarea.getCommInfo().getUbocNbrNonlogBlErrsFormatted());
	}

	/**Original name: VCOM-VALIDATE-COMMAREA_FIRST_SENTENCES<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  VALIDATE ESSENTIAL COMMAREA FIELDS ARE FILLED                  *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	private void vcomValidateCommarea() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN UBOC-UOW-NAME = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-MSG-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-SESSION-ID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USERID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-AUTH-USER-CLIENTID = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-MSG-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-REQ-SWITCHES-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-HEADER-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-DATA-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-RESP-WARNINGS-STORE = SPACES OR LOW-VALUES
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//               WHEN UBOC-UOW-KEY-REPLACE-STORE = SPACES OR LOW-VALUE
		//                     TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
		//           END-EVALUATE.
		if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowName())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowName(), UbocCommInfoCaws002.Len.UBOC_UOW_NAME)) {
			// COB_CODE: SET COMA-UOW-NAME-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowNameBlank();
			// COB_CODE: MOVE 'UBOC-UOW-NAME ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-UOW-NAME ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocMsgId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocMsgId(), UbocCommInfoCaws002.Len.UBOC_MSG_ID)) {
			// COB_CODE: SET COMA-MSG-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaMsgIdBlank();
			// COB_CODE: MOVE 'UBOC-MSG-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-MSG-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocSessionId())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocSessionId(), UbocCommInfoCaws002.Len.UBOC_SESSION_ID)) {
			// COB_CODE: SET COMA-SESSION-ID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaSessionIdBlank();
			// COB_CODE: MOVE 'UBOC-SESSION-ID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-SESSION-ID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserid(), UbocCommInfoCaws002.Len.UBOC_AUTH_USERID)) {
			// COB_CODE: SET COMA-USERID-BLANK OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUseridBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USERID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USERID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocAuthUserClientid())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocAuthUserClientidFormatted())) {
			// COB_CODE: SET COMA-AUTH-USER-CLIENTID-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaAuthUserClientidBlank();
			// COB_CODE: MOVE 'UBOC-AUTH-USER-CLIENTID WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC-AUTH-USER-CLIENTID WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowReqMsgStore(), UbocCommInfoCaws002.Len.UBOC_UOW_REQ_MSG_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-MSG-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqMsgStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ MSG STO WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ MSG STO WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getCommInfo().getUbocUowReqSwitchesStore(), UbocCommInfoCaws002.Len.UBOC_UOW_REQ_SWITCHES_STORE)) {
			// COB_CODE: SET COMA-UOW-REQ-SWIT-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowReqSwitStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW REQ SWITCHES STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW REQ SWITCHES STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getCommInfo().getUbocUowRespHeaderStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_HEADER_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-HDR-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespHdrStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP HEADER STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP HEADER STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore())
				|| Characters.EQ_LOW.test(dfhcommarea.getCommInfo().getUbocUowRespDataStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_DATA_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-DATA-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespDataStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP DATA STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP DATA STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getCommInfo().getUbocUowRespWarningsStore(), UbocCommInfoCaws002.Len.UBOC_UOW_RESP_WARNINGS_STORE)) {
			// COB_CODE: SET COMA-UOW-RESP-WARN-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowRespWarnStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW RESP WARNINGS STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW RESP WARNINGS STORE WAS EMPTY");
		} else if (Characters.EQ_SPACE.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore()) || Characters.EQ_LOW
				.test(dfhcommarea.getCommInfo().getUbocUowKeyReplaceStore(), UbocCommInfoCaws002.Len.UBOC_UOW_KEY_REPLACE_STORE)) {
			// COB_CODE: SET COMA-UOW-KEY-REPL-STORE-BLANK
			//                                 OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setComaUowKeyReplStoreBlank();
			// COB_CODE: MOVE 'UBOC UOW KEY REPLACE STORE WAS EMPTY'
			//             TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrComment("UBOC UOW KEY REPLACE STORE WAS EMPTY");
		}
		// COB_CODE: IF ETRA-ERR-ACTION NOT = SPACES
		//               PERFORM 9000-LOG-WARNING-OR-ERROR
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getWsEstoInfo().getEstoDetailBuffer().getEtraErrAction())) {
			// COB_CODE: SET WS-LOG-ERROR                           TO TRUE
			ws.getWsLogWarningOrErrorSw().setError();
			// COB_CODE: SET EFAL-BUS-LOGIC-FAILURE OF WS-ESTO-INFO TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalBusLogicFailure();
			// COB_CODE: SET EFAL-COMMAREA-FAILED OF WS-ESTO-INFO   TO TRUE
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalCommareaFailed();
			// COB_CODE: MOVE 'VCOM-VALIDATE-COMMAREA'
			//             TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
			ws.getWsEstoInfo().getEstoDetailBuffer().setEfalErrParagraph("VCOM-VALIDATE-COMMAREA");
			// COB_CODE: PERFORM 9000-LOG-WARNING-OR-ERROR
			logWarningOrError();
		}
	}

	public void initWsScriptRow() {
		ws.getHssvcHalScrnScriptRow().setHalScrnScriptCsumFormatted("000000000");
		ws.getHssvcHalScrnScriptRow().setUowNmKcre("");
		ws.getHssvcHalScrnScriptRow().setSecGrpNmKcre("");
		ws.getHssvcHalScrnScriptRow().setHssvContextKcre("");
		ws.getHssvcHalScrnScriptRow().setHssvScrnFldIdKcre("");
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbrKcre("");
		ws.getHssvcHalScrnScriptRow().setTransProcessDt("");
		ws.getHssvcHalScrnScriptRow().setUowNm("");
		ws.getHssvcHalScrnScriptRow().setSecGrpNm("");
		ws.getHssvcHalScrnScriptRow().setHssvContext("");
		ws.getHssvcHalScrnScriptRow().setHssvScrnFldId("");
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbrSign(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbrFormatted("0000000000");
		ws.getHssvcHalScrnScriptRow().setUowNmCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setSecGrpNmCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvContextCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvScrnFldIdCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvSeqNbrCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvNewLinIndCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvNewLinInd(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvScriptTxtCi(Types.SPACE_CHAR);
		ws.getHssvcHalScrnScriptRow().setHssvScriptTxt("");
	}

	public void initEstoStoreInfo() {
		ws.getWsEstoInfo().getEstoInputKey().setStoreId("");
		ws.getWsEstoInfo().getEstoInputKey().getRecordingLevel().setRecordingLevel("");
		ws.getWsEstoInfo().getEstoInputKey().setErrSeqNumFormatted("00000");
		ws.getWsEstoInfo().getEstoCallEtraSw().setEstoCallEtraSw(Types.SPACE_CHAR);
		ws.getWsEstoInfo().getEstoDetailBuffer().setEstoDetailBuffer("");
	}

	public void initEstoReturnInfo() {
		ws.getWsEstoInfo().getEstoOutput().getStoreReturnCd().setStoreReturnCdFormatted("0");
		ws.getWsEstoInfo().getEstoOutput().getStoreDetailCd().setStoreDetailCdFormatted("00");
		ws.getWsEstoInfo().getEstoOutput().setRespCdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setResp2CdFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlcodeFormatted("0000000000");
		ws.getWsEstoInfo().getEstoOutput().setSqlerrmc("");
		ws.getWsEstoInfo().getEstoOutput().getFloodInd().setFloodInd(Types.SPACE_CHAR);
	}

	public void initWsWarningUmtArea() {
		ws.getUwrnCommon().setId("");
		ws.getUwrnCommon().setRecSeqFormatted("000");
		ws.getUwrnCommon().setFailureType("");
		ws.getUwrnCommon().setFailedModule("");
		ws.getUwrnCommon().setFailedTableOrFile("");
		ws.getUwrnCommon().setFailedColumnOrField("");
		ws.getUwrnCommon().setWarningCode("");
		ws.getUwrnCommon().setWarningText("");
	}

	public void initWsNlbeUmtArea() {
		ws.getNlbeCommon().setId("");
		ws.getNlbeCommon().setRecSeqFormatted("000");
		ws.getNlbeCommon().setFailureType("");
		ws.getNlbeCommon().setFailedModule("");
		ws.getNlbeCommon().setFailedTableOrFile("");
		ws.getNlbeCommon().setFailedColumnOrField("");
		ws.getNlbeCommon().setErrorCode("");
		ws.getNlbeCommon().setNonloggableBpErrText("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
