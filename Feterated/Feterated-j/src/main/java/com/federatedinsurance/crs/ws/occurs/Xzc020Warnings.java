/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC020-WARNINGS<br>
 * Variables: XZC020-WARNINGS from copybook XZC020C1<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xzc020Warnings {

	//==== PROPERTIES ====
	//Original name: XZC020-WARN-MSG
	private String xzc020WarnMsg = DefaultValues.stringVal(Len.XZC020_WARN_MSG);

	//==== METHODS ====
	public void setXzc020WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		xzc020WarnMsg = MarshalByte.readString(buffer, position, Len.XZC020_WARN_MSG);
	}

	public byte[] getXzc020WarningsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xzc020WarnMsg, Len.XZC020_WARN_MSG);
		return buffer;
	}

	public void initXzc020WarningsSpaces() {
		xzc020WarnMsg = "";
	}

	public void setXzc020WarnMsg(String xzc020WarnMsg) {
		this.xzc020WarnMsg = Functions.subString(xzc020WarnMsg, Len.XZC020_WARN_MSG);
	}

	public String getXzc020WarnMsg() {
		return this.xzc020WarnMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC020_WARN_MSG = 500;
		public static final int XZC020_WARNINGS = XZC020_WARN_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
