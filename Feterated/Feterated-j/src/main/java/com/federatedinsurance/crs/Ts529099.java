/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.inspect.InspectMatcher;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.federatedinsurance.crs.ws.AddressComponents;
import com.federatedinsurance.crs.ws.SaveAreaTs529099;
import com.federatedinsurance.crs.ws.Ts52901;
import com.federatedinsurance.crs.ws.Ts529099Data;
import com.federatedinsurance.crs.ws.redefines.AfAddressLines;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.ctu.utils.Strings;
import com.modernsystems.programs.Programs;

/**Original name: TS529099<br>
 * <pre>AUTHOR.        DARREN M. ALBERS
 * DATE-WRITTEN.  05 MAY 2009.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - ADDRESS FORMATTER ROUTINE                   **
 * *                                                             **
 * * PURPOSE -  THIS ROUTINE WILL TAKE IN SEVERAL COMPONENTS     **
 * *            OF AN ADDRESS AND RETURN A FORMATTED ADDRESS.    **
 * *                                                             **
 * * INPUT  -LINKAGE SECTION                                     **
 * *          : DISPLAY NAME                                     **
 * *          : NAME LINE 1                                      **
 * *          : NAME LINE 2                                      **
 * *          : ADDRESS 1                                        **
 * *          : ADDRESS 2                                        **
 * *          : CITY                                             **
 * *          : STATE                                            **
 * *          : POSTAL CODE                                      **
 * *                                                             **
 * * OUTPUT -LINKAGE SECTION                                     **
 * *          : FORMATTED NAME AND ADDRESS IN SIX LINES          **
 * *          : RETURN STATUS / ERROR CODE                       **
 * *                                                             **
 * * SAMPLE CALL:                                                **
 * *                                                             **
 * *      CALL ' TS529099' USING FORMAT-ADDRESS-PARMS.           **
 * *                                                             **
 * * ASSUMPTIONS/NOTES:                                          **
 * *        - CALLS WILL PASS IN EITHER A DISPLAY NAME OR VALUES **
 * *           IN THE NAME LINE FIELDS, BUT NOT BOTH.            **
 * *        - IF A NON-ZERO RETURN CODE IS RETURNED, THE         **
 * *           FORMATTED ADDRESS LINES WILL BE BLANK.            **
 * *        - THIS ROUTINE WILL ARRANGE THE FORMATTED ADDRESS    **
 * *           LINES SUCH THAT BLANK LINES WILL BE PLACED AFTER  **
 * *           ALL POPULATED LINES.                              **
 * *        - THIS ROUTINE WILL ALSO REMOVE ANY LEADING SPACES   **
 * *           FROM POPULATED FORMATTED ADDRESS LINES.           **
 * *        - ANY LOW-VALUES OR HIGH-VALUES PASSED INTO THE      **
 * *           ROUTINE WILL BE REPLACED BY SPACES.               **
 * *        - WHEN USING A DISPLAY NAME, THE ROUTINE WILL ATTEMPT**
 * *           TO WRAP THE NAME AMONG THE THREE NAME LINES, BUT  **
 * *           LINE BREAKING WILL OCCUR IF TOO MANY CONSECUTIVE  **
 * *           CHARACTERS ARE PASSED IN.                         **
 * *                                                             **
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TO07614   05/05/2009 E404DMA    NEW                         **
 * ****************************************************************</pre>*/
public class Ts529099 extends Program {

	//==== PROPERTIES ====
	private static final String N3100_B = "3100-B";
	private static final String N3100_A = "3100-A";
	//Original name: WORKING-STORAGE
	private Ts529099Data ws = new Ts529099Data();
	//Original name: L-FORMAT-ADDRESS-PARMS
	private Ts52901 ts52901;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(Ts52901 ts52901) {
		this.ts52901 = ts52901;
		startProgram();
		programExit();
		return 0;
	}

	public static Ts529099 getInstance() {
		return (Programs.getInstance(Ts529099.class));
	}

	/**Original name: 1000-START-PROGRAM<br>*/
	private void startProgram() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: PERFORM 3000-FORMAT-ADDRESS
		//              THRU 3000-EXIT.
		formatAddress();
		//    PROVIDE VAUES IN THE OUTPUT AREA ONLY AFTER A SUCCESSFUL RUN.
		// COB_CODE: MOVE AF-ADDRESS-LINES       TO L-FA-FORMATTED-ADDRESS-LINES.
		ts52901.getFormattedAddressLines().setFormattedAddressLinesBytes(ws.getAfAddressLines().getAfAddressLinesBytes());
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>******************************************************
	 *  INITIALIZE OUTPUT AND FORMATTING FIELDS, THEN CAPTURE AND
	 *  VALIDATE INPUTS.
	 * ******************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: SET L-FA-RC-GOOD-RUN        TO TRUE.
		ts52901.getReturnCode().setGoodRun();
		// COB_CODE: INITIALIZE ADDRESS-FORMATTED
		//                      L-FA-FORMATTED-ADDRESS-LINES.
		initAddressFormatted();
		initFormattedAddressLines();
		// COB_CODE: MOVE L-FA-AI-DISPLAY-NAME   TO AC-DISPLAY-NAME.
		ws.getAddressComponents().setDisplayName(ts52901.getAddressInputs().getDisplayName());
		// COB_CODE: MOVE L-FA-AI-NAME-LINE-1    TO AC-NAME-LINE-1.
		ws.getAddressComponents().setNameLine1(ts52901.getAddressInputs().getNameLine1());
		// COB_CODE: MOVE L-FA-AI-NAME-LINE-2    TO AC-NAME-LINE-2.
		ws.getAddressComponents().setNameLine2(ts52901.getAddressInputs().getNameLine2());
		// COB_CODE: MOVE L-FA-AI-ADR-LINE-1     TO AC-ADR-LINE-1.
		ws.getAddressComponents().setAdrLine1(ts52901.getAddressInputs().getAdrLine1());
		// COB_CODE: MOVE L-FA-AI-ADR-LINE-2     TO AC-ADR-LINE-2.
		ws.getAddressComponents().setAdrLine2(ts52901.getAddressInputs().getAdrLine2());
		// COB_CODE: MOVE L-FA-AI-CITY           TO AC-CITY.
		ws.getAddressComponents().setCity(ts52901.getAddressInputs().getCity());
		// COB_CODE: MOVE L-FA-AI-STATE-ABB      TO AC-STATE-ABB.
		ws.getAddressComponents().setStateAbb(ts52901.getAddressInputs().getStateAbb());
		// COB_CODE: MOVE L-FA-AI-POSTAL-CODE    TO AC-POSTAL-CODE.
		ws.getAddressComponents().setPostalCode(ts52901.getAddressInputs().getPostalCode());
		//    SINCE THEY ARE UNDESIRABLE, REMOVE ALL LOW AND HIGH VALUES.
		// COB_CODE: INSPECT ADDRESS-COMPONENTS
		//               REPLACING ALL LOW-VALUES BY SPACES
		//                         ALL HIGH-VALUES BY SPACES.
		ws.getAddressComponents().setAddressComponentsFormatted(ws.getAddressComponents().getAddressComponentsFormatted()
				.replace(String.valueOf(Types.LOW_CHAR_VAL), Types.SPACE_STRING).replace(String.valueOf(Types.HIGH_CHAR_VAL), Types.SPACE_STRING));
		// COB_CODE: PERFORM 2100-EDIT-ADDRESS-COMPONENTS
		//              THRU 2100-EXIT.
		editAddressComponents();
	}

	/**Original name: 2100-EDIT-ADDRESS-COMPONENTS<br>
	 * <pre>******************************************************
	 *  ENSURE MINIMUM INPUTS WERE PROVIDED
	 * ******************************************************
	 *     EITHER THE DISPLAY NAME OR THE NAME LINE(S) CAN BE FILLED IN,
	 *       BUT NOT BOTH.  ADDRESS LINE 2 IS AN OPTIONAL FIELD.  NAME
	 *       LINE 2 IS ALSO OPTIONAL WHEN NAME LINE 1 IS PROVIDED.
	 *       ALL OTHER FIELDS MUST BE POPULATED.</pre>*/
	private void editAddressComponents() {
		// COB_CODE: IF AC-DISPLAY-NAME = SPACES
		//               END-IF
		//           ELSE
		//               END-IF
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getAddressComponents().getDisplayName())) {
			// COB_CODE: IF AC-NAME-LINE-1 = SPACES
			//               GO TO 1000-PROGRAM-EXIT
			//           END-IF
			if (Characters.EQ_SPACE.test(ws.getAddressComponents().getNameLine1())) {
				// COB_CODE: SET L-FA-RC-INPUT-ERR
				//                               TO TRUE
				ts52901.getReturnCode().setInputErr();
				// COB_CODE: GO TO 1000-PROGRAM-EXIT
				programExit();
			}
		} else if (!Characters.EQ_SPACE.test(ws.getAddressComponents().getNameLine1())
				|| !Characters.EQ_SPACE.test(ws.getAddressComponents().getNameLine2())) {
			// COB_CODE: IF AC-NAME-LINE-1 NOT = SPACES
			//             OR
			//              AC-NAME-LINE-2 NOT = SPACES
			//               GO TO 1000-PROGRAM-EXIT
			//           END-IF
			// COB_CODE: SET L-FA-RC-INPUT-ERR
			//                               TO TRUE
			ts52901.getReturnCode().setInputErr();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		// COB_CODE: IF AC-ADR-LINE-1 = SPACES
		//             OR
		//              AC-CITY = SPACES
		//             OR
		//              AC-STATE-ABB = SPACES
		//             OR
		//              AC-POSTAL-CODE = SPACES
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getAddressComponents().getAdrLine1()) || Characters.EQ_SPACE.test(ws.getAddressComponents().getCity())
				|| Characters.EQ_SPACE.test(ws.getAddressComponents().getStateAbb())
				|| Characters.EQ_SPACE.test(ws.getAddressComponents().getPostalCode())) {
			// COB_CODE: SET L-FA-RC-INPUT-ERR   TO TRUE
			ts52901.getReturnCode().setInputErr();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
	}

	/**Original name: 3000-FORMAT-ADDRESS<br>
	 * <pre>******************************************************
	 *  CREATE FORMATTED NAME AND ADDRESS FROM PROVIDED COMPONENTS
	 * ******************************************************
	 *     CAPTURE THE LENGTH OF THE FORMATTED NAME/ADDRESS LINE.</pre>*/
	private void formatAddress() {
		// COB_CODE: MOVE LENGTH OF AF-TA-ADR-LINE
		//                                       TO SA-FORMATTED-LINE-LEN.
		ws.getSaveArea().setFormattedLineLen(Trunc.toShort(AfAddressLines.Len.TA_ADR_LINE, 4));
		// COB_CODE: PERFORM 3100-FORMAT-NAME
		//              THRU 3100-EXIT.
		rng3100FormatName();
		// COB_CODE: PERFORM 3200-FORMAT-ADDRESS
		//              THRU 3200-EXIT.
		rng3200FormatAddress();
		// COB_CODE: PERFORM 3300-REMOVE-BLANKS
		//              THRU 3300-EXIT.
		removeBlanks();
	}

	/**Original name: 3100-FORMAT-NAME<br>
	 * <pre>******************************************************
	 *  CREATE FORMATTED NAME FROM PROVIDED COMPONENTS
	 * ******************************************************
	 *     WHEN PROVIDED, MOVE THE SEPARATE NAME LINES TO THEIR
	 *       CORRESPONDING FORMATTED LINES AND EXIT.</pre>*/
	private String formatName() {
		// COB_CODE: IF AC-NAME-LINE-1 NOT = SPACES
		//               GO TO 3100-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getAddressComponents().getNameLine1())) {
			// COB_CODE: MOVE AC-NAME-LINE-1     TO AF-AL-LINE-1
			ws.getAfAddressLines().setAlLine1(ws.getAddressComponents().getNameLine1());
			// COB_CODE: MOVE AC-NAME-LINE-2     TO AF-AL-LINE-2
			ws.getAfAddressLines().setAlLine2(ws.getAddressComponents().getNameLine2());
			// COB_CODE: GO TO 3100-EXIT
			return "3100-EXIT";
		}
		//    THE DISPLAY NAME WAS USED AND WILL NEED TO BE DIVIDED BETWEEN
		//      THE FIRST THREE FORMATTED ADDRESS LINES.  SET UP FOR 1ST.
		//      CAPTURE THE LENGTH OF THE DISPLAY NAME LINE.
		// COB_CODE: MOVE +1                     TO SS-DN-START
		ws.setSsDnStart(((short) 1));
		// COB_CODE: MOVE CF-AL-LINE-1           TO SS-TA.
		ws.setSsTa(ws.getConstantFields().getAlLine1());
		// COB_CODE: MOVE LENGTH OF AC-DISPLAY-NAME
		//                                       TO SA-DISPLAY-NAME-LEN.
		ws.getSaveArea().setDisplayNameLen(((short) AddressComponents.Len.DISPLAY_NAME));
		return "";
	}

	/**Original name: 3100-A<br>
	 * <pre>    EXIT IF WE'VE MOVED THE ENTIRE DISPLAY NAME OR IF THERE ARE
	 *       NO MORE CHARACTERS TO MOVE.</pre>*/
	private String a() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		// COB_CODE: IF SS-DN-START > SA-DISPLAY-NAME-LEN
		//             OR
		//              AC-DISPLAY-NAME (SS-DN-START:) = SPACES
		//               GO TO 3100-EXIT
		//           END-IF.
		if (ws.getSsDnStart() > ws.getSaveArea().getDisplayNameLen()
				|| Conditions.eq(ws.getAddressComponents().getDisplayNameFormatted().substring((ws.getSsDnStart()) - 1), "")) {
			// COB_CODE: GO TO 3100-EXIT
			return "3100-EXIT";
		}
		//    IF NEEDED, ADJUST THE START POSITION SUCH THAT IT DOES NOT
		//      START ON A BLANK CHARACTER.
		// COB_CODE: IF AC-DISPLAY-NAME (SS-DN-START: 1) = SPACE
		//               ADD SA-SPACE-COUNT      TO SS-DN-START
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getDisplayNameFormatted().substring((ws.getSsDnStart()) - 1, ws.getSsDnStart()), "")) {
			// COB_CODE: MOVE +0                 TO SA-SPACE-COUNT
			ws.getSaveArea().setSpaceCount(((short) 0));
			// COB_CODE: INSPECT AC-DISPLAY-NAME (SS-DN-START:)
			//               TALLYING SA-SPACE-COUNT FOR LEADING SPACES
			iPattern = new InspectPattern(Types.SPACE_STRING).leading();
			iMatcher = new InspectMatcher(ws.getAddressComponents().getDisplayNameFormatted().substring((ws.getSsDnStart()) - 1), iPattern);
			ws.getSaveArea().setSpaceCount(Trunc.toShort(ws.getSaveArea().getSpaceCount() + iMatcher.count(), 4));
			// COB_CODE: ADD SA-SPACE-COUNT      TO SS-DN-START
			ws.setSsDnStart(Trunc.toShort(ws.getSaveArea().getSpaceCount() + ws.getSsDnStart(), 4));
		}
		// COB_CODE: PERFORM 3110-CALCULATE-LINE-BREAK-POS
		//              THRU 3110-EXIT.
		calculateLineBreakPos();
		return "";
	}

	/**Original name: 3100-B<br>
	 * <pre>    ADJUST THE END-POINT TO LAND ON A SPACE IF WE ARE CURRENTLY
	 *       ON A CHARACTER.  WHEN WE CANNOT FIND A SPACE TO BREAK ON,
	 *       RESET THE LINE BREAK POSITION TO CAPTURE AS MUCH AS WE CAN.
	 *       THE NAME WILL HAVE TO BE BROKEN OVER MULTIPLE LINES.</pre>*/
	private String b() {
		// COB_CODE: IF AC-DISPLAY-NAME (SS-DN-LINE-BREAK: 1) NOT = SPACES
		//               END-IF
		//           END-IF.
		if (!Conditions.eq(ws.getAddressComponents().getDisplayNameFormatted().substring((ws.getSsDnLineBreak()) - 1, ws.getSsDnLineBreak()), "")) {
			// COB_CODE: SUBTRACT +1             FROM SS-DN-LINE-BREAK
			ws.setSsDnLineBreak(Trunc.toShort(ws.getSsDnLineBreak() - 1, 4));
			// COB_CODE: IF SS-DN-LINE-BREAK > SS-DN-START
			//               GO TO 3100-B
			//           ELSE
			//                  THRU 3110-EXIT
			//           END-IF
			if (ws.getSsDnLineBreak() > ws.getSsDnStart()) {
				// COB_CODE: GO TO 3100-B
				return "3100-B";
			} else {
				// COB_CODE: PERFORM 3110-CALCULATE-LINE-BREAK-POS
				//              THRU 3110-EXIT
				calculateLineBreakPos();
			}
		}
		//    ERROR IF WE TRY TO PUT A NAME VALUE INTO A LINE RESERVED FOR
		//      THE ADDRESSES PARTS.
		// COB_CODE: IF SS-TA > CF-AL-LINE-3
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (ws.getSsTa() > ws.getConstantFields().getAlLine3()) {
			// COB_CODE: SET L-FA-RC-FORMATTED-NM-TOO-LONG
			//                                   TO TRUE
			ts52901.getReturnCode().setFormattedNmTooLong();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		//    BREAK POINT FOUND.  MOVE THE CONTENTS, ADJUST THE NEXT
		//      FORMATTING LINE TO USE, AND ADJUST THE START POSITION TO
		//      LOOK FOR THE NEXT BREAK.
		// COB_CODE: COMPUTE SA-BREAK-LEN = SS-DN-LINE-BREAK
		//                                - SS-DN-START.
		ws.getSaveArea().setBreakLen(Trunc.toShort(ws.getSsDnLineBreak() - ws.getSsDnStart(), 4));
		// COB_CODE: MOVE AC-DISPLAY-NAME (SS-DN-START: SA-BREAK-LEN)
		//                                       TO AF-TA-ADR-LINE (SS-TA).
		ws.getAfAddressLines().setTaAdrLine(ws.getSsTa(), ws.getAddressComponents().getDisplayNameFormatted().substring((ws.getSsDnStart()) - 1,
				ws.getSsDnStart() + ws.getSaveArea().getBreakLen() - 1));
		// COB_CODE: ADD +1                      TO SS-TA.
		ws.setSsTa(Trunc.toShort(1 + ws.getSsTa(), 4));
		// COB_CODE: COMPUTE SS-DN-START = SS-DN-LINE-BREAK.
		ws.setSsDnStart(ws.getSsDnLineBreak());
		// COB_CODE: GO TO 3100-A.
		return "3100-A";
	}

	/**Original name: 3110-CALCULATE-LINE-BREAK-POS<br>
	 * <pre>******************************************************
	 *  CALCUATE THE DESIRED LINE BREAK POSITION
	 * ******************************************************
	 *     COMPUTE THE BREAK POSITION TO BE THE LENGTH OF THE FORMATTED
	 *       LINE PLUS ONE.  IF IT LANDS ON A SPACE, WE CAN MOVE THE
	 *       ENTIRE CONTENTS OF THE LINE.</pre>*/
	private void calculateLineBreakPos() {
		// COB_CODE: COMPUTE SS-DN-LINE-BREAK = SS-DN-START
		//                                    + SA-FORMATTED-LINE-LEN.
		ws.setSsDnLineBreak(Trunc.toShort(ws.getSsDnStart() + ws.getSaveArea().getFormattedLineLen(), 4));
		//    WHEN NECESSARY, ADJUST THE LINE BREAK SUCH THAT IT WILL
		//      ALLOW THE REST OF THE DISPLAY NAME TO BE CAPTURED
		// COB_CODE: IF SS-DN-LINE-BREAK > SA-DISPLAY-NAME-LEN
		//                                        + 1
		//           END-IF.
		if (ws.getSsDnLineBreak() > ws.getSaveArea().getDisplayNameLen()) {
			// COB_CODE: COMPUTE SS-DN-LINE-BREAK = SA-DISPLAY-NAME-LEN
			//                                    + 1
			ws.setSsDnLineBreak(Trunc.toShort(ws.getSaveArea().getDisplayNameLen() + 1, 4));
		}
	}

	/**Original name: 3200-FORMAT-ADDRESS<br>
	 * <pre>******************************************************
	 *  CREATE FORMATTED ADDRESS LINES FROM PROVIDED COMPONENTS
	 * ******************************************************
	 *     CAPTURE THE ADDRESS LINES AS-IS.</pre>*/
	private void formatAddress1() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		// COB_CODE: MOVE AC-ADR-LINE-1          TO AF-AL-LINE-4.
		ws.getAfAddressLines().setAlLine4(ws.getAddressComponents().getAdrLine1());
		// COB_CODE: MOVE AC-ADR-LINE-2          TO AF-AL-LINE-5.
		ws.getAfAddressLines().setAlLine5(ws.getAddressComponents().getAdrLine2());
		//    WHEN NECESSARY, REMOVE ANY LEADING SPACES IN THE CITY FIELD.
		// COB_CODE: IF AC-CITY (1: 1) = SPACE
		//                                       TO AC-CITY
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getCityFormatted().substring((1) - 1, 1), "")) {
			// COB_CODE: MOVE +0                 TO SA-SPACE-COUNT
			ws.getSaveArea().setSpaceCount(((short) 0));
			// COB_CODE: INSPECT AC-CITY
			//               TALLYING SA-SPACE-COUNT FOR LEADING SPACES
			iPattern = new InspectPattern(Types.SPACE_STRING).leading();
			iMatcher = new InspectMatcher(ws.getAddressComponents().getCityFormatted(), iPattern);
			ws.getSaveArea().setSpaceCount(Trunc.toShort(ws.getSaveArea().getSpaceCount() + iMatcher.count(), 4));
			// COB_CODE: MOVE AC-CITY (SA-SPACE-COUNT + 1:)
			//                                   TO AC-CITY
			ws.getAddressComponents().setCity(ws.getAddressComponents().getCityFormatted().substring((ws.getSaveArea().getSpaceCount() + 1) - 1));
		}
		//    CAPTURE THE LENGTH OF THE CITY NAME.
		// COB_CODE: MOVE LENGTH OF AC-CITY      TO SA-CITY-LEN.
		ws.getSaveArea().setCityLen(((short) AddressComponents.Len.CITY));
	}

	/**Original name: 3200-A<br>*/
	private String a1() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		// COB_CODE: IF AC-CITY (SA-CITY-LEN: 1) = SPACES
		//               GO TO 3200-A
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getCityFormatted().substring((ws.getSaveArea().getCityLen()) - 1, ws.getSaveArea().getCityLen()),
				"")) {
			// COB_CODE: SUBTRACT +1             FROM SA-CITY-LEN
			ws.getSaveArea().setCityLen(Trunc.toShort(ws.getSaveArea().getCityLen() - 1, 4));
			// COB_CODE: GO TO 3200-A
			return "3200-A";
		}
		//    WHEN NECESSARY, REMOVE ANY LEADING SPACES IN THE STATE FIELD.
		// COB_CODE: IF AC-STATE-ABB (1: 1) = SPACE
		//                                       TO AC-STATE-ABB
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getStateAbbFormatted().substring((1) - 1, 1), "")) {
			// COB_CODE: MOVE +0                 TO SA-SPACE-COUNT
			ws.getSaveArea().setSpaceCount(((short) 0));
			// COB_CODE: INSPECT AC-STATE-ABB
			//               TALLYING SA-SPACE-COUNT FOR LEADING SPACES
			iPattern = new InspectPattern(Types.SPACE_STRING).leading();
			iMatcher = new InspectMatcher(ws.getAddressComponents().getStateAbbFormatted(), iPattern);
			ws.getSaveArea().setSpaceCount(Trunc.toShort(ws.getSaveArea().getSpaceCount() + iMatcher.count(), 4));
			// COB_CODE: MOVE AC-STATE-ABB (SA-SPACE-COUNT + 1:)
			//                                   TO AC-STATE-ABB
			ws.getAddressComponents()
					.setStateAbb(ws.getAddressComponents().getStateAbbFormatted().substring((ws.getSaveArea().getSpaceCount() + 1) - 1));
		}
		//    CAPTURE THE LENGTH OF THE STATE ABBREVIATION.
		// COB_CODE: MOVE LENGTH OF AC-STATE-ABB TO SA-ST-LEN.
		ws.getSaveArea().setStLen(((short) AddressComponents.Len.STATE_ABB));
		return "";
	}

	/**Original name: 3200-B<br>*/
	private String b1() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		ConcatUtil concatUtil = null;
		// COB_CODE: IF AC-STATE-ABB (SA-ST-LEN: 1) = SPACES
		//               GO TO 3200-B
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getStateAbbFormatted().substring((ws.getSaveArea().getStLen()) - 1, ws.getSaveArea().getStLen()),
				"")) {
			// COB_CODE: SUBTRACT +1             FROM SA-ST-LEN
			ws.getSaveArea().setStLen(Trunc.toShort(ws.getSaveArea().getStLen() - 1, 4));
			// COB_CODE: GO TO 3200-B
			return "3200-B";
		}
		//    WHEN NECESSARY, REMOVE ANY LEADING SPACES IN THE POSTAL CODE
		//        FIELD.
		// COB_CODE: IF AC-POSTAL-CODE (1: 1) = SPACE
		//                                       TO AC-POSTAL-CODE
		//           END-IF.
		if (Conditions.eq(ws.getAddressComponents().getPostalCodeFormatted().substring((1) - 1, 1), "")) {
			// COB_CODE: MOVE +0                 TO SA-SPACE-COUNT
			ws.getSaveArea().setSpaceCount(((short) 0));
			// COB_CODE: INSPECT AC-POSTAL-CODE
			//               TALLYING SA-SPACE-COUNT FOR LEADING SPACES
			iPattern = new InspectPattern(Types.SPACE_STRING).leading();
			iMatcher = new InspectMatcher(ws.getAddressComponents().getPostalCodeFormatted(), iPattern);
			ws.getSaveArea().setSpaceCount(Trunc.toShort(ws.getSaveArea().getSpaceCount() + iMatcher.count(), 4));
			// COB_CODE: MOVE AC-POSTAL-CODE (SA-SPACE-COUNT + 1:)
			//                                   TO AC-POSTAL-CODE
			ws.getAddressComponents()
					.setPostalCode(ws.getAddressComponents().getPostalCodeFormatted().substring((ws.getSaveArea().getSpaceCount() + 1) - 1));
		}
		//    STRING THE CITY, STATE, AND ZIP TOGETHER WITH DELIMIETERS
		//      AND WITHOUT EXTRA SPACES BETWEEN THE VALUES.
		//    WE WILL CREATE THE RESULT IN A WORKING STORAGE FIELD WERE
		//      THE FIELD CAN HOLD ALL POSSIBLE RESULTS AND THEN MOVE IT
		//      TO THE FORMATTED FIELD.  THIS WILL ALLOW US TO CHECK IF
		//      ANY TRUNCATION HAS OCCURRED.
		// COB_CODE: MOVE SPACES                 TO SA-CITY-STATE-ZIP-LINE.
		ws.getSaveArea().setCityStateZipLine("");
		// COB_CODE: STRING AC-CITY (1: SA-CITY-LEN)
		//                  CF-CITY-STATE-DLM
		//                  AC-STATE-ABB (1: SA-ST-LEN)
		//                  CF-STATE-ZIP-DLM
		//                  AC-POSTAL-CODE DELIMITED BY SIZE
		//               INTO SA-CITY-STATE-ZIP-LINE
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(SaveAreaTs529099.Len.CITY_STATE_ZIP_LINE,
				ws.getAddressComponents().getCityFormatted().substring((1) - 1, ws.getSaveArea().getCityLen()),
				ws.getConstantFields().getCityStateDlmFormatted(),
				ws.getAddressComponents().getStateAbbFormatted().substring((1) - 1, ws.getSaveArea().getStLen()),
				String.valueOf(ws.getConstantFields().getStateZipDlm()), ws.getAddressComponents().getPostalCodeFormatted());
		ws.getSaveArea().setCityStateZipLine(concatUtil.replaceInString(ws.getSaveArea().getCityStateZipLineFormatted()));
		// COB_CODE: MOVE SA-CITY-STATE-ZIP-LINE TO AF-AL-LINE-6.
		ws.getAfAddressLines().setAlLine6(ws.getSaveArea().getCityStateZipLine());
		//    ERROR IF A TRUNCATION HAS OCCURRED.
		// COB_CODE: IF AF-AL-LINE-6 NOT = SA-CITY-STATE-ZIP-LINE
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (!Conditions.eq(ws.getAfAddressLines().getAlLine6(), ws.getSaveArea().getCityStateZipLine())) {
			// COB_CODE: SET L-FA-RC-TRUNCATION-OCCURRED
			//                                   TO TRUE
			ts52901.getReturnCode().setTruncationOccurred();
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		return "";
	}

	/**Original name: 3300-REMOVE-BLANKS<br>
	 * <pre>******************************************************
	 *  REPOSITION BLANK LINES TO BE AFTER LINES FILLED IN AND
	 *  REMOVE ANY BLANK CHARACTERS LEADING ANY OF THE ADDRESS LINES
	 * ******************************************************
	 *   SINCE THE FIRST LINE WILL NEVER BE BLANK AND THERE IS NOTHING
	 *     THAT NEEDS TO BE DONE IF THE LAST LINE IS BLANK,
	 *     RUN THROUGH THE REST OF THE NAME AND ADDRESS LINES
	 *     TO REMOVE ANY BLANK LINES BEWTEEN ANY POPULATED LINES.</pre>*/
	private void removeBlanks() {
		// COB_CODE: PERFORM 3310-CHECK-FOR-BLANKS
		//              THRU 3310-EXIT
		//               WITH TEST BEFORE
		//               VARYING SS-TA FROM CF-AL-LINE-2 BY +1
		//               UNTIL SS-TA >= CF-AL-LINE-6.
		ws.setSsTa(ws.getConstantFields().getAlLine2());
		while (!(ws.getSsTa() >= ws.getConstantFields().getAlLine6())) {
			rng3310CheckForBlanks();
			ws.setSsTa(Trunc.toShort(ws.getSsTa() + 1, 4));
		}
		//    IN CASE ANY STRING RESULTS IN SPACES STARTING THE LINE,
		//      GO THROUGH AND REMOVE ALL LEADING SPACES.
		// COB_CODE: PERFORM 3320-REMOVE-LEADING-BLANKS
		//              THRU 3320-EXIT
		//               WITH TEST BEFORE
		//               VARYING SS-TA FROM CF-AL-LINE-1 BY +1
		//               UNTIL SS-TA > CF-AL-LINE-6.
		ws.setSsTa(ws.getConstantFields().getAlLine1());
		while (!(ws.getSsTa() > ws.getConstantFields().getAlLine6())) {
			removeLeadingBlanks();
			ws.setSsTa(Trunc.toShort(ws.getSsTa() + 1, 4));
		}
	}

	/**Original name: 3310-CHECK-FOR-BLANKS<br>
	 * <pre>******************************************************
	 *  DETERMINE IF ANY BLANK LINES EXIST IN THE FORMATTED OUTPUT
	 *   AND ADJUST THE POSITION OF THE NON-BLANK LINES WHEN NECESSARY
	 * ******************************************************
	 *     IF THE CURRENT LINE IS NOT BLANK, LEAVE IT ALONE AND CONTINUE
	 *       CHECKING FOR BLANK LINES.</pre>*/
	private String checkForBlanks() {
		// COB_CODE: IF AF-TA-ADR-LINE (SS-TA) NOT = SPACES
		//               GO TO 3310-EXIT
		//           END-IF.
		if (!Characters.EQ_SPACE.test(ws.getAfAddressLines().getTaAdrLine(ws.getSsTa()))) {
			// COB_CODE: GO TO 3310-EXIT
			return "3310-EXIT";
		}
		//    THE CURRENT LINE IS BLANK.  DETERMINE THE CURRENT POSITION
		//      OF THE LINE WITHIN THE TABLE.
		// COB_CODE: COMPUTE SS-TA-LINE-POS-CURRENT = (SS-TA
		//                                          - 1)
		//                                          * SA-FORMATTED-LINE-LEN
		//                                          + 1.
		ws.setSsTaLinePosCurrent(Trunc.toShort((ws.getSsTa() - 1) * ws.getSaveArea().getFormattedLineLen() + 1, 4));
		//    EXIT IF THERE IS NO MORE DATA LEFT IN THE TABLE AND SET FLAG
		//      TO END THE SCAN FOR BLANK LINES.
		// COB_CODE: IF AF-TABLE-OF-ADDRESS-LINES (SS-TA-LINE-POS-CURRENT:)
		//                                                              = SPACES
		//               GO TO 3310-EXIT
		//           END-IF.
		if (Conditions.eq(ws.getAfAddressLines().getAfTableOfAddressLinesFormatted().substring((ws.getSsTaLinePosCurrent()) - 1), "")) {
			// COB_CODE: MOVE CF-AL-LINE-6       TO SS-TA
			ws.setSsTa(ws.getConstantFields().getAlLine6());
			// COB_CODE: GO TO 3310-EXIT
			return "3310-EXIT";
		}
		//    DATA EXISTS FURTHER DOWN THE TABLE.  DETERMINE THE NEXT
		//      LINE'S POSITION IN THE TABLE.
		// COB_CODE: COMPUTE SS-TA-LINE-POS-NEXT = SS-TA-LINE-POS-CURRENT
		//                                       + SA-FORMATTED-LINE-LEN.
		ws.setSsTaLinePosNext(Trunc.toShort(ws.getSsTaLinePosCurrent() + ws.getSaveArea().getFormattedLineLen(), 4));
		return "";
	}

	/**Original name: 3310-A<br>
	 * <pre>    LOOP THROUGH AND MOVE ALL LINES IN THE TABLE FROM THIS
	 *       POSITION UP BY ONE UNTIL THE CURRENT LINE IS NO LONGER
	 *       BLANK.</pre>*/
	private String a2() {
		// COB_CODE: MOVE AF-TABLE-OF-ADDRESS-LINES (SS-TA-LINE-POS-NEXT:)
		//                                       TO AF-TABLE-OF-ADDRESS-LINES
		//                                         (SS-TA-LINE-POS-CURRENT:).
		ws.getAfAddressLines().setAfTableOfAddressLinesSubstring(
				ws.getAfAddressLines().getAfTableOfAddressLinesFormatted().substring((ws.getSsTaLinePosNext()) - 1), ws.getSsTaLinePosCurrent(), -1);
		// COB_CODE: IF AF-TA-ADR-LINE (SS-TA) = SPACES
		//               GO TO 3310-A
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getAfAddressLines().getTaAdrLine(ws.getSsTa()))) {
			// COB_CODE: GO TO 3310-A
			return "3310-A";
		}
		return "";
	}

	/**Original name: 3320-REMOVE-LEADING-BLANKS<br>
	 * <pre>******************************************************
	 *  REMOVES ANY LEADING BLANK CHARACTERS FROM THE INDIVIDUAL LINE
	 * ******************************************************
	 *     SKIP THIS LINE AND ALL FOLLOWING LINES IF THIS LINE IS BLANK.
	 *       AT THIS POINT, THERE SHOULD BE NO OTHER LINES FILLED IN.</pre>*/
	private void removeLeadingBlanks() {
		InspectPattern iPattern = null;
		InspectMatcher iMatcher = null;
		// COB_CODE: IF AF-TA-ADR-LINE (SS-TA) = SPACES
		//               GO TO 3320-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getAfAddressLines().getTaAdrLine(ws.getSsTa()))) {
			// COB_CODE: MOVE CF-AL-LINE-6       TO SS-TA
			ws.setSsTa(ws.getConstantFields().getAlLine6());
			// COB_CODE: GO TO 3320-EXIT
			return;
		}
		//    WHEN THE FIRST CHARACTER IN THE LINE IS NOT A SPACE, THEN
		//      THERE WILL BE NO CHARACTERS TO MOVE FORWARD ANYWAYS.  EXIT.
		// COB_CODE: IF AF-TA-ADR-LINE (SS-TA) (1: 1) NOT = SPACE
		//               GO TO 3320-EXIT
		//           END-IF.
		if (!Conditions.eq(ws.getAfAddressLines().getTaAdrLineFormatted(ws.getSsTa()).substring((1) - 1, 1), "")) {
			// COB_CODE: GO TO 3320-EXIT
			return;
		}
		//    BRING THE CONTENTS OF THE STRING TO THE FRONT OF THE FIELD.
		// COB_CODE: MOVE +0                     TO SA-SPACE-COUNT.
		ws.getSaveArea().setSpaceCount(((short) 0));
		// COB_CODE: INSPECT AF-TA-ADR-LINE (SS-TA)
		//               TALLYING SA-SPACE-COUNT FOR LEADING SPACES.
		iPattern = new InspectPattern(Types.SPACE_STRING).leading();
		iMatcher = new InspectMatcher(ws.getAfAddressLines().getTaAdrLineFormatted(ws.getSsTa()), iPattern);
		ws.getSaveArea().setSpaceCount(Trunc.toShort(ws.getSaveArea().getSpaceCount() + iMatcher.count(), 4));
		// COB_CODE: MOVE AF-TA-ADR-LINE (SS-TA) (SA-SPACE-COUNT + 1:)
		//                                       TO AF-TA-ADR-LINE (SS-TA).
		ws.getAfAddressLines().setTaAdrLine(ws.getSsTa(),
				ws.getAfAddressLines().getTaAdrLineFormatted(ws.getSsTa()).substring((ws.getSaveArea().getSpaceCount() + 1) - 1));
	}

	/**Original name: RNG_3100-FORMAT-NAME-_-3100-EXIT<br>*/
	private void rng3100FormatName() {
		String retcode = "";
		retcode = formatName();
		if (Strings.isEmptyOr(retcode, N3100_A)) {
			do {
				retcode = a();
				if (Strings.isEmptyOr(retcode, N3100_B)) {
					do {
						retcode = b();
					} while ((N3100_B.equals(retcode)));
				}
			} while ((N3100_A.equals(retcode)));
		}
	}

	/**Original name: RNG_3200-FORMAT-ADDRESS-_-3200-EXIT<br>*/
	private void rng3200FormatAddress() {
		String retcode = "";
		boolean goto3200A = false;
		boolean goto3200B = false;
		formatAddress1();
		do {
			goto3200A = false;
			retcode = a1();
		} while (retcode.equals("3200-A"));
		do {
			goto3200B = false;
			retcode = b1();
		} while (retcode.equals("3200-B"));
	}

	/**Original name: RNG_3310-CHECK-FOR-BLANKS-_-3310-EXIT<br>*/
	private void rng3310CheckForBlanks() {
		String retcode = "";
		boolean goto3310A = false;
		boolean goto3310Exit = false;
		retcode = checkForBlanks();
		if (!retcode.equals("3310-EXIT")) {
			do {
				goto3310A = false;
				retcode = a2();
			} while (retcode.equals("3310-A"));
		}
		goto3310Exit = false;
	}

	public void initAddressFormatted() {
		ws.getAfAddressLines().setAlLine1("");
		ws.getAfAddressLines().setAlLine2("");
		ws.getAfAddressLines().setAlLine3("");
		ws.getAfAddressLines().setAlLine4("");
		ws.getAfAddressLines().setAlLine5("");
		ws.getAfAddressLines().setAlLine6("");
	}

	public void initFormattedAddressLines() {
		ts52901.getFormattedAddressLines().setLine1("");
		ts52901.getFormattedAddressLines().setLine2("");
		ts52901.getFormattedAddressLines().setLine3("");
		ts52901.getFormattedAddressLines().setLine4("");
		ts52901.getFormattedAddressLines().setLine5("");
		ts52901.getFormattedAddressLines().setLine6("");
	}
}
