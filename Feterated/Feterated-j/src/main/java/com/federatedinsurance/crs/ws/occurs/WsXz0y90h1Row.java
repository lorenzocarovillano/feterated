/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.federatedinsurance.crs.copy.Xz0y90h1;

/**Original name: WS-XZ0Y90H1-ROW<br>
 * Variables: WS-XZ0Y90H1-ROW from program XZ0P90H0<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class WsXz0y90h1Row {

	//==== PROPERTIES ====
	//Original name: XZ0Y90H1
	private Xz0y90h1 xz0y90h1 = new Xz0y90h1();

	//==== METHODS ====
	public void setWsXz0y90h1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0y90h1.setPreparePolicyRowBytes(buffer, position);
	}

	public byte[] getWsXz0y90h1RowBytes(byte[] buffer, int offset) {
		int position = offset;
		xz0y90h1.getPreparePolicyRowBytes(buffer, position);
		return buffer;
	}

	public void initWsXz0y90h1RowSpaces() {
		xz0y90h1.initXz0y90h1Spaces();
	}

	public Xz0y90h1 getXz0y90h1() {
		return xz0y90h1;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_XZ0Y90H1_ROW = Xz0y90h1.Len.PREPARE_POLICY_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
