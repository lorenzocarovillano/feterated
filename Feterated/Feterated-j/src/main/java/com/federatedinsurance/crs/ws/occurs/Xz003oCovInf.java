/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ003O-COV-INF<br>
 * Variables: XZ003O-COV-INF from copybook XZ03CI1O<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Xz003oCovInf {

	//==== PROPERTIES ====
	//Original name: XZ003O-COV-CD
	private String xz003oCovCd = DefaultValues.stringVal(Len.XZC03O_COV_CD);

	//==== METHODS ====
	public void setCovInfBytes(byte[] buffer, int offset) {
		int position = offset;
		xz003oCovCd = MarshalByte.readString(buffer, position, Len.XZC03O_COV_CD);
	}

	public byte[] getCovInfBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, xz003oCovCd, Len.XZC03O_COV_CD);
		return buffer;
	}

	public void initCovInfLowValues() {
		xz003oCovCd = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.XZC03O_COV_CD);
	}

	public void initCovInfSpaces() {
		xz003oCovCd = "";
	}

	public void setXzc03oCovCd(String xzc03oCovCd) {
		this.xz003oCovCd = Functions.subString(xzc03oCovCd, Len.XZC03O_COV_CD);
	}

	public String getXzc03oCovCd() {
		return this.xz003oCovCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC03O_COV_CD = 10;
		public static final int COV_INF = XZC03O_COV_CD;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
