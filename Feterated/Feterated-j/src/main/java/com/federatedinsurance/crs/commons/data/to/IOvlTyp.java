/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [OVL_TYP]
 * 
 */
public interface IOvlTyp extends BaseSqlTo {

	/**
	 * Host Variable OVL-DES
	 * 
	 */
	String getOvlDes();

	void setOvlDes(String ovlDes);

	/**
	 * Host Variable XCLV-IND
	 * 
	 */
	char getXclvInd();

	void setXclvInd(char xclvInd);
};
