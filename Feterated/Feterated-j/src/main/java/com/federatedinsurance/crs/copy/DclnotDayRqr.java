/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.INotDayRqr;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: DCLNOT-DAY-RQR<br>
 * Variable: DCLNOT-DAY-RQR from copybook XZH00011<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclnotDayRqr implements INotDayRqr {

	//==== PROPERTIES ====
	//Original name: ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: POL-TYP-CD
	private String polTypCd = DefaultValues.stringVal(Len.POL_TYP_CD);
	//Original name: NBR-OF-NOT-DAY
	private short nbrOfNotDay = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	@Override
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	@Override
	public String getStAbb() {
		return this.stAbb;
	}

	public String getStAbbFormatted() {
		return Functions.padBlanks(getStAbb(), Len.ST_ABB);
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	@Override
	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getActNotTypCdFormatted() {
		return Functions.padBlanks(getActNotTypCd(), Len.ACT_NOT_TYP_CD);
	}

	@Override
	public void setPolTypCd(String polTypCd) {
		this.polTypCd = Functions.subString(polTypCd, Len.POL_TYP_CD);
	}

	@Override
	public String getPolTypCd() {
		return this.polTypCd;
	}

	public String getPolTypCdFormatted() {
		return Functions.padBlanks(getPolTypCd(), Len.POL_TYP_CD);
	}

	@Override
	public void setNbrOfNotDay(short nbrOfNotDay) {
		this.nbrOfNotDay = nbrOfNotDay;
	}

	@Override
	public short getNbrOfNotDay() {
		return this.nbrOfNotDay;
	}

	@Override
	public String getCfAllPolTyp() {
		throw new FieldNotMappedException("cfAllPolTyp");
	}

	@Override
	public void setCfAllPolTyp(String cfAllPolTyp) {
		throw new FieldNotMappedException("cfAllPolTyp");
	}

	@Override
	public int getWsDaysDifference() {
		throw new FieldNotMappedException("wsDaysDifference");
	}

	@Override
	public void setWsDaysDifference(int wsDaysDifference) {
		throw new FieldNotMappedException("wsDaysDifference");
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ST_ABB = 2;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int POL_TYP_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
