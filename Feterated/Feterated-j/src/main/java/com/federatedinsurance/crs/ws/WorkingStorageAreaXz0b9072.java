/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: WORKING-STORAGE-AREA<br>
 * Variable: WORKING-STORAGE-AREA from program XZ0B9072<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WorkingStorageAreaXz0b9072 {

	//==== PROPERTIES ====
	//Original name: WS-ROW-COUNT
	private short rowCount = ((short) 0);
	public static final short NOTHING_FOUND = ((short) 0);
	//Original name: WS-PROGRAM-NAME
	private String programName = "XZ0B9072";
	//Original name: WS-BUS-OBJ-NM-ACY-TAG-LIST
	private String busObjNmAcyTagList = "XZ_GET_ACY_TAG_LIST";

	//==== METHODS ====
	public void setRowCount(short rowCount) {
		this.rowCount = rowCount;
	}

	public short getRowCount() {
		return this.rowCount;
	}

	public boolean isNothingFound() {
		return rowCount == NOTHING_FOUND;
	}

	public void setNothingFound() {
		rowCount = NOTHING_FOUND;
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getProgramNameFormatted() {
		return Functions.padBlanks(getProgramName(), Len.PROGRAM_NAME);
	}

	public String getBusObjNmAcyTagList() {
		return this.busObjNmAcyTagList;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
