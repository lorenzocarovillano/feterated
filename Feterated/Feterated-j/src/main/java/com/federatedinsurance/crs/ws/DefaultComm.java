/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: DEFAULT-COMM<br>
 * Variable: DEFAULT-COMM from program HALRRESP<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DefaultComm extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: DEFAULT-COMM
	private char defaultComm;

	//==== METHODS ====
	public void setDefaultComm(char defaultComm) {
		this.defaultComm = defaultComm;
	}

	public void setDefaultCommFromBuffer(byte[] buffer, int offset) {
		setDefaultComm(MarshalByte.readChar(buffer, offset));
	}

	public void setDefaultCommFromBuffer(byte[] buffer) {
		setDefaultCommFromBuffer(buffer, 1);
	}

	public char getDefaultComm() {
		return this.defaultComm;
	}

	@Override
	public int getLength() {
		return Len.DEFAULT_COMM;
	}

	@Override
	public void deserialize(byte[] buf) {
		setDefaultCommFromBuffer(buf);
	}

	@Override
	public byte[] serialize() {
		return MarshalByteExt.chToBuffer(getDefaultComm());
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DEFAULT_COMM = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int DEFAULT_COMM = 0;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int DEFAULT_COMM = 0;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
