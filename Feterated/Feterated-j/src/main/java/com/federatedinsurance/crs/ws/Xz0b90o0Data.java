/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import static com.bphx.ctu.af.lang.AfSystem.strLen;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.core.data.util.FixedStrings;
import com.federatedinsurance.crs.commons.data.to.IActNotFrmRecCoTyp;
import com.federatedinsurance.crs.commons.data.to.IActNotTyp1;
import com.federatedinsurance.crs.copy.DclactNot;
import com.federatedinsurance.crs.copy.DclactNotRec;
import com.federatedinsurance.crs.copy.DclactNotTyp;
import com.federatedinsurance.crs.copy.DclrecCoTyp;
import com.federatedinsurance.crs.copy.Hallcom;
import com.federatedinsurance.crs.copy.NlbeCommon;
import com.federatedinsurance.crs.copy.UwrnCommon;
import com.federatedinsurance.crs.ws.enums.WsLogWarningOrErrorSw;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program XZ0B90O0<br>
 * Generated as a class for rule WS.<br>*/
public class Xz0b90o0Data implements IActNotTyp1, IActNotFrmRecCoTyp {

	//==== PROPERTIES ====
	//Original name: CONSTANT-FIELDS
	private ConstantFieldsXz0b90o0 constantFields = new ConstantFieldsXz0b90o0();
	/**Original name: NI-ACT-TYP-CD<br>
	 * <pre>**  NULL-INDICATORS.</pre>*/
	private short niActTypCd = DefaultValues.BIN_SHORT_VAL;
	//Original name: NI-EMP-ID
	private short niEmpId = DefaultValues.BIN_SHORT_VAL;
	//Original name: SW-END-OF-CURSOR-XML-REQ-FLAG
	private boolean swEndOfCursorXmlReqFlag = false;
	//Original name: WORKING-STORAGE-AREA
	private WorkingStorageAreaXz0b90o0 workingStorageArea = new WorkingStorageAreaXz0b90o0();
	//Original name: WS-XZ0A90O0-ROW
	private WsXz0a90o0Row wsXz0a90o0Row = new WsXz0a90o0Row();
	//Original name: DCLACT-NOT
	private DclactNot dclactNot = new DclactNot();
	//Original name: DCLACT-NOT-REC
	private DclactNotRec dclactNotRec = new DclactNotRec();
	//Original name: DCLACT-NOT-TYP
	private DclactNotTyp dclactNotTyp = new DclactNotTyp();
	//Original name: DCLREC-CO-TYP
	private DclrecCoTyp dclrecCoTyp = new DclrecCoTyp();
	//Original name: WS-HALRURQA-LINKAGE
	private WsHalrurqaLinkage wsHalrurqaLinkage = new WsHalrurqaLinkage();
	//Original name: WS-HALRRESP-LINKAGE
	private WsHalrrespLinkage wsHalrrespLinkage = new WsHalrrespLinkage();
	//Original name: WS-NOT-SPECIFIC-MISC
	private WsNotSpecificMisc wsNotSpecificMisc = new WsNotSpecificMisc();
	//Original name: HALLCOM
	private Hallcom hallcom = new Hallcom();
	//Original name: WS-APPLID
	private String wsApplid = DefaultValues.stringVal(Len.WS_APPLID);
	//Original name: WS-LOG-WARNING-OR-ERROR-SW
	private WsLogWarningOrErrorSw wsLogWarningOrErrorSw = new WsLogWarningOrErrorSw();
	//Original name: UWRN-COMMON
	private UwrnCommon uwrnCommon = new UwrnCommon();
	//Original name: NLBE-COMMON
	private NlbeCommon nlbeCommon = new NlbeCommon();
	//Original name: WS-ESTO-INFO
	private WsEstoInfo wsEstoInfo = new WsEstoInfo();

	//==== METHODS ====
	public void setNiActTypCd(short niActTypCd) {
		this.niActTypCd = niActTypCd;
	}

	public short getNiActTypCd() {
		return this.niActTypCd;
	}

	public void setNiEmpId(short niEmpId) {
		this.niEmpId = niEmpId;
	}

	public short getNiEmpId() {
		return this.niEmpId;
	}

	public void setSwEndOfCursorXmlReqFlag(boolean swEndOfCursorXmlReqFlag) {
		this.swEndOfCursorXmlReqFlag = swEndOfCursorXmlReqFlag;
	}

	public boolean isSwEndOfCursorXmlReqFlag() {
		return this.swEndOfCursorXmlReqFlag;
	}

	public void setWsApplid(String wsApplid) {
		this.wsApplid = Functions.subString(wsApplid, Len.WS_APPLID);
	}

	public String getWsApplid() {
		return this.wsApplid;
	}

	@Override
	public String getActNotTypCd() {
		return dclactNot.getActNotTypCd();
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.dclactNot.setActNotTypCd(actNotTypCd);
	}

	@Override
	public String getActTypCd() {
		return dclactNot.getActTypCd();
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.dclactNot.setActTypCd(actTypCd);
	}

	@Override
	public String getActTypCdObj() {
		if (getNiActTypCd() >= 0) {
			return getActTypCd();
		} else {
			return null;
		}
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		if (actTypCdObj != null) {
			setActTypCd(actTypCdObj);
			setNiActTypCd(((short) 0));
		} else {
			setNiActTypCd(((short) -1));
		}
	}

	public ConstantFieldsXz0b90o0 getConstantFields() {
		return constantFields;
	}

	@Override
	public String getCsrActNbr() {
		return dclactNot.getCsrActNbr();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.dclactNot.setCsrActNbr(csrActNbr);
	}

	public DclactNot getDclactNot() {
		return dclactNot;
	}

	public DclactNotRec getDclactNotRec() {
		return dclactNotRec;
	}

	public DclactNotTyp getDclactNotTyp() {
		return dclactNotTyp;
	}

	public DclrecCoTyp getDclrecCoTyp() {
		return dclrecCoTyp;
	}

	@Override
	public String getDocDes() {
		return FixedStrings.get(dclactNotTyp.getDocDesText(), dclactNotTyp.getDocDesLen());
	}

	@Override
	public void setDocDes(String docDes) {
		this.dclactNotTyp.setDocDesText(docDes);
		this.dclactNotTyp.setDocDesLen((((short) strLen(docDes))));
	}

	@Override
	public String getEmpId() {
		return dclactNot.getEmpId();
	}

	@Override
	public void setEmpId(String empId) {
		this.dclactNot.setEmpId(empId);
	}

	@Override
	public String getEmpIdObj() {
		if (getNiEmpId() >= 0) {
			return getEmpId();
		} else {
			return null;
		}
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		if (empIdObj != null) {
			setEmpId(empIdObj);
			setNiEmpId(((short) 0));
		} else {
			setNiEmpId(((short) -1));
		}
	}

	public Hallcom getHallcom() {
		return hallcom;
	}

	public NlbeCommon getNlbeCommon() {
		return nlbeCommon;
	}

	@Override
	public String getNotDt() {
		return dclactNot.getNotDt();
	}

	@Override
	public void setNotDt(String notDt) {
		this.dclactNot.setNotDt(notDt);
	}

	@Override
	public String getNotPrcTs() {
		return dclactNot.getNotPrcTs();
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.dclactNot.setNotPrcTs(notPrcTs);
	}

	public UwrnCommon getUwrnCommon() {
		return uwrnCommon;
	}

	public WorkingStorageAreaXz0b90o0 getWorkingStorageArea() {
		return workingStorageArea;
	}

	public WsEstoInfo getWsEstoInfo() {
		return wsEstoInfo;
	}

	public WsHalrrespLinkage getWsHalrrespLinkage() {
		return wsHalrrespLinkage;
	}

	public WsHalrurqaLinkage getWsHalrurqaLinkage() {
		return wsHalrurqaLinkage;
	}

	public WsLogWarningOrErrorSw getWsLogWarningOrErrorSw() {
		return wsLogWarningOrErrorSw;
	}

	public WsNotSpecificMisc getWsNotSpecificMisc() {
		return wsNotSpecificMisc;
	}

	public WsXz0a90o0Row getWsXz0a90o0Row() {
		return wsXz0a90o0Row;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WS_SE3_CUR_ISO_DATE = 10;
		public static final int WS_SE3_CUR_ISO_TIME = 16;
		public static final int WS_APPLID = 8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
