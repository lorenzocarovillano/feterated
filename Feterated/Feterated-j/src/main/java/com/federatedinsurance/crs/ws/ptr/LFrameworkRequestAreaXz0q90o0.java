/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q90O0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q90o0 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q90o0() {
	}

	public LFrameworkRequestAreaXz0q90o0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza9o0qMaxXmlReqRows(short xza9o0qMaxXmlReqRows) {
		writeBinaryShort(Pos.XZA9O0_MAX_XML_REQ_ROWS, xza9o0qMaxXmlReqRows);
	}

	/**Original name: XZA9O0Q-MAX-XML-REQ-ROWS<br>*/
	public short getXza9o0qMaxXmlReqRows() {
		return readBinaryShort(Pos.XZA9O0_MAX_XML_REQ_ROWS);
	}

	public void setXza9o0qCsrActNbr(String xza9o0qCsrActNbr) {
		writeString(Pos.XZA9O0_CSR_ACT_NBR, xza9o0qCsrActNbr, Len.XZA9O0_CSR_ACT_NBR);
	}

	/**Original name: XZA9O0Q-CSR-ACT-NBR<br>*/
	public String getXza9o0qCsrActNbr() {
		return readString(Pos.XZA9O0_CSR_ACT_NBR, Len.XZA9O0_CSR_ACT_NBR);
	}

	public void setXza9o0qNotPrcTs(String xza9o0qNotPrcTs) {
		writeString(Pos.XZA9O0_NOT_PRC_TS, xza9o0qNotPrcTs, Len.XZA9O0_NOT_PRC_TS);
	}

	/**Original name: XZA9O0Q-NOT-PRC-TS<br>*/
	public String getXza9o0qNotPrcTs() {
		return readString(Pos.XZA9O0_NOT_PRC_TS, Len.XZA9O0_NOT_PRC_TS);
	}

	public void setXza9o0qActNotTypCd(String xza9o0qActNotTypCd) {
		writeString(Pos.XZA9O0_ACT_NOT_TYP_CD, xza9o0qActNotTypCd, Len.XZA9O0_ACT_NOT_TYP_CD);
	}

	/**Original name: XZA9O0Q-ACT-NOT-TYP-CD<br>*/
	public String getXza9o0qActNotTypCd() {
		return readString(Pos.XZA9O0_ACT_NOT_TYP_CD, Len.XZA9O0_ACT_NOT_TYP_CD);
	}

	public void setXza9o0qNotDt(String xza9o0qNotDt) {
		writeString(Pos.XZA9O0_NOT_DT, xza9o0qNotDt, Len.XZA9O0_NOT_DT);
	}

	/**Original name: XZA9O0Q-NOT-DT<br>*/
	public String getXza9o0qNotDt() {
		return readString(Pos.XZA9O0_NOT_DT, Len.XZA9O0_NOT_DT);
	}

	public void setXza9o0qEmpId(String xza9o0qEmpId) {
		writeString(Pos.XZA9O0_EMP_ID, xza9o0qEmpId, Len.XZA9O0_EMP_ID);
	}

	/**Original name: XZA9O0Q-EMP-ID<br>*/
	public String getXza9o0qEmpId() {
		return readString(Pos.XZA9O0_EMP_ID, Len.XZA9O0_EMP_ID);
	}

	public void setXza9o0qActTypCd(String xza9o0qActTypCd) {
		writeString(Pos.XZA9O0_ACT_TYP_CD, xza9o0qActTypCd, Len.XZA9O0_ACT_TYP_CD);
	}

	/**Original name: XZA9O0Q-ACT-TYP-CD<br>*/
	public String getXza9o0qActTypCd() {
		return readString(Pos.XZA9O0_ACT_TYP_CD, Len.XZA9O0_ACT_TYP_CD);
	}

	public void setXza9o0qDocDes(String xza9o0qDocDes) {
		writeString(Pos.XZA9O0_DOC_DES, xza9o0qDocDes, Len.XZA9O0_DOC_DES);
	}

	/**Original name: XZA9O0Q-DOC-DES<br>*/
	public String getXza9o0qDocDes() {
		return readString(Pos.XZA9O0_DOC_DES, Len.XZA9O0_DOC_DES);
	}

	public void setXza9o0qTtyCoInd(char xza9o0qTtyCoInd) {
		writeChar(Pos.XZA9O0_TTY_CO_IND, xza9o0qTtyCoInd);
	}

	/**Original name: XZA9O0Q-TTY-CO-IND<br>*/
	public char getXza9o0qTtyCoInd() {
		return readChar(Pos.XZA9O0_TTY_CO_IND);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A90O0 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA9O0_XML_REQ_ROW = L_FW_REQ_XZ0A90O0;
		public static final int XZA9O0_MAX_XML_REQ_ROWS = XZA9O0_XML_REQ_ROW;
		public static final int XZA9O0_XML_REQ_INFO = XZA9O0_MAX_XML_REQ_ROWS + Len.XZA9O0_MAX_XML_REQ_ROWS;
		public static final int XZA9O0_CSR_ACT_NBR = XZA9O0_XML_REQ_INFO;
		public static final int XZA9O0_NOT_PRC_TS = XZA9O0_CSR_ACT_NBR + Len.XZA9O0_CSR_ACT_NBR;
		public static final int XZA9O0_ACT_NOT_TYP_CD = XZA9O0_NOT_PRC_TS + Len.XZA9O0_NOT_PRC_TS;
		public static final int XZA9O0_NOT_DT = XZA9O0_ACT_NOT_TYP_CD + Len.XZA9O0_ACT_NOT_TYP_CD;
		public static final int XZA9O0_EMP_ID = XZA9O0_NOT_DT + Len.XZA9O0_NOT_DT;
		public static final int XZA9O0_ACT_TYP_CD = XZA9O0_EMP_ID + Len.XZA9O0_EMP_ID;
		public static final int XZA9O0_DOC_DES = XZA9O0_ACT_TYP_CD + Len.XZA9O0_ACT_TYP_CD;
		public static final int XZA9O0_TTY_CO_IND = XZA9O0_DOC_DES + Len.XZA9O0_DOC_DES;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA9O0_MAX_XML_REQ_ROWS = 2;
		public static final int XZA9O0_CSR_ACT_NBR = 9;
		public static final int XZA9O0_NOT_PRC_TS = 26;
		public static final int XZA9O0_ACT_NOT_TYP_CD = 5;
		public static final int XZA9O0_NOT_DT = 10;
		public static final int XZA9O0_EMP_ID = 6;
		public static final int XZA9O0_ACT_TYP_CD = 2;
		public static final int XZA9O0_DOC_DES = 240;
		public static final int XZA9O0_TTY_CO_IND = 1;
		public static final int XZA9O0_XML_REQ_INFO = XZA9O0_CSR_ACT_NBR + XZA9O0_NOT_PRC_TS + XZA9O0_ACT_NOT_TYP_CD + XZA9O0_NOT_DT + XZA9O0_EMP_ID
				+ XZA9O0_ACT_TYP_CD + XZA9O0_DOC_DES + XZA9O0_TTY_CO_IND;
		public static final int XZA9O0_XML_REQ_ROW = XZA9O0_MAX_XML_REQ_ROWS + XZA9O0_XML_REQ_INFO;
		public static final int L_FW_REQ_XZ0A90O0 = XZA9O0_XML_REQ_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A90O0;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
