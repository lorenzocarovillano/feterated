/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;

/**Original name: WS-REQ-UMT-DFLT-REC-SW<br>
 * Variable: WS-REQ-UMT-DFLT-REC-SW from program HALOUSDH<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsReqUmtDfltRecSw {

	//==== PROPERTIES ====
	private char value = Types.SPACE_CHAR;
	public static final char FND = 'Y';
	public static final char NOT_FND = 'N';

	//==== METHODS ====
	public void setReqUmtDfltRecSw(char reqUmtDfltRecSw) {
		this.value = reqUmtDfltRecSw;
	}

	public char getReqUmtDfltRecSw() {
		return this.value;
	}

	public boolean isFnd() {
		return value == FND;
	}

	public void setFnd() {
		value = FND;
	}

	public void setNotFnd() {
		value = NOT_FND;
	}
}
