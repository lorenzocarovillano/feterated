/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DP-FUNCTION<br>
 * Variable: DP-FUNCTION from program TS030299<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class DpFunction {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.FUNCTION);
	public static final String IDENTIFY = "IDENTIFY";
	public static final String SIGNON = "SIGNON";
	public static final String CREATE_THREAD = "CREATE THREAD";
	public static final String TERMINATE_IDENTIFY = "TERMINATE IDENTIFY";

	//==== METHODS ====
	public void setFunction(String function) {
		this.value = Functions.subString(function, Len.FUNCTION);
	}

	public String getFunction() {
		return this.value;
	}

	public void setIdentify() {
		value = IDENTIFY;
	}

	public void setSignon() {
		value = SIGNON;
	}

	public void setCreateThread() {
		value = CREATE_THREAD;
	}

	public void setTerminateIdentify() {
		value = TERMINATE_IDENTIFY;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FUNCTION = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
