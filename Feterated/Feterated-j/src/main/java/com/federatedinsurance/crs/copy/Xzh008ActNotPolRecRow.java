/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNotPolRec;
import com.federatedinsurance.crs.commons.data.to.IXz0d0008Generic;

/**Original name: XZH008-ACT-NOT-POL-REC-ROW<br>
 * Variable: XZH008-ACT-NOT-POL-REC-ROW from copybook XZ0H0008<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzh008ActNotPolRecRow implements IXz0d0008Generic, IActNotPolRec {

	//==== PROPERTIES ====
	//Original name: XZH008-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZH008-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZH008-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: XZH008-REC-SEQ-NBR
	private short recSeqNbr = DefaultValues.BIN_SHORT_VAL;

	//==== METHODS ====
	public String getXzh008ActNotPolRecRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh008ActNotPolRecRowBytes());
	}

	public byte[] getXzh008ActNotPolRecRowBytes() {
		byte[] buffer = new byte[Len.XZH008_ACT_NOT_POL_REC_ROW];
		return getXzh008ActNotPolRecRowBytes(buffer, 1);
	}

	public byte[] getXzh008ActNotPolRecRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeBinaryShort(buffer, position, recSeqNbr);
		return buffer;
	}

	public void initXzh008ActNotPolRecRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		polNbr = "";
		recSeqNbr = Types.INVALID_BINARY_SHORT_VAL;
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	@Override
	public String getPolNbr() {
		return this.polNbr;
	}

	public String getPolNbrFormatted() {
		return Functions.padBlanks(getPolNbr(), Len.POL_NBR);
	}

	@Override
	public void setRecSeqNbr(short recSeqNbr) {
		this.recSeqNbr = recSeqNbr;
	}

	@Override
	public short getRecSeqNbr() {
		return this.recSeqNbr;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int POL_NBR = 25;
		public static final int REC_SEQ_NBR = 2;
		public static final int XZH008_ACT_NOT_POL_REC_ROW = CSR_ACT_NBR + NOT_PRC_TS + POL_NBR + REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
