/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ001000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz001000 {

	//==== PROPERTIES ====
	//Original name: CF-SC-GOOD-RETURN
	private short scGoodReturn = ((short) 0);
	//Original name: CF-MAX-TBL-ENTRIES-POL-NBR
	private short maxTblEntriesPolNbr = ((short) 10);
	//Original name: CF-ACT-NOT-TYP-CD
	private CfActNotTypCd actNotTypCd = new CfActNotTypCd();
	//Original name: CF-COMMERCIAL-LINE
	private String commercialLine = "CL";
	//Original name: CF-PERSONAL-LINE
	private String personalLine = "PL";
	//Original name: FILLER-CF-CN-ACT-NOT-CSR
	private String flr1 = "ACT_NOT_CSR";
	//Original name: CF-PARAGRAPH-NAMES
	private CfParagraphNamesXz001000 paragraphNames = new CfParagraphNamesXz001000();
	//Original name: CF-WORKFLOW-FIELDS
	private CfWorkflowFields workflowFields = new CfWorkflowFields();
	//Original name: CF-ADDED-BY-BUSINESS-WORKS
	private String addedByBusinessWorks = "NA";

	//==== METHODS ====
	public short getScGoodReturn() {
		return this.scGoodReturn;
	}

	public short getMaxTblEntriesPolNbr() {
		return this.maxTblEntriesPolNbr;
	}

	public String getCommercialLine() {
		return this.commercialLine;
	}

	public String getPersonalLine() {
		return this.personalLine;
	}

	public String getCnActNotCsrFormatted() {
		return getFlr1Formatted();
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr1Formatted() {
		return Functions.padBlanks(getFlr1(), Len.FLR1);
	}

	public void setAddedByBusinessWorks(String addedByBusinessWorks) {
		this.addedByBusinessWorks = Functions.subString(addedByBusinessWorks, Len.ADDED_BY_BUSINESS_WORKS);
	}

	public String getAddedByBusinessWorks() {
		return this.addedByBusinessWorks;
	}

	public CfActNotTypCd getActNotTypCd() {
		return actNotTypCd;
	}

	public CfParagraphNamesXz001000 getParagraphNames() {
		return paragraphNames;
	}

	public CfWorkflowFields getWorkflowFields() {
		return workflowFields;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ADDED_BY_BUSINESS_WORKS = 64;
		public static final int FLR1 = 18;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
