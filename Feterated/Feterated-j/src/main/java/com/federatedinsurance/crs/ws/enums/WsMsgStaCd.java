/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-MSG-STA-CD<br>
 * Variable: WS-MSG-STA-CD from program XZ0B90P0<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsMsgStaCd {

	//==== PROPERTIES ====
	private String value = DefaultValues.stringVal(Len.MSG_STA_CD);
	public static final String SUCCESS = "Success";
	public static final String SUCCESS_WITH_INFO = "SuccessWithInfo";
	public static final String SUCCESS_WITH_CHG = "SuccessWithChanges";

	//==== METHODS ====
	public void setMsgStaCd(String msgStaCd) {
		this.value = Functions.subString(msgStaCd, Len.MSG_STA_CD);
	}

	public String getMsgStaCd() {
		return this.value;
	}

	public boolean isSuccess() {
		return value.equals(SUCCESS);
	}

	public boolean isSuccessWithInfo() {
		return value.equals(SUCCESS_WITH_INFO);
	}

	public boolean isSuccessWithChg() {
		return value.equals(SUCCESS_WITH_CHG);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MSG_STA_CD = 25;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
