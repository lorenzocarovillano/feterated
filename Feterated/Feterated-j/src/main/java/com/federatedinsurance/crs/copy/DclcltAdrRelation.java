/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: DCLCLT-ADR-RELATION<br>
 * Variable: DCLCLT-ADR-RELATION from copybook MUH00122<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class DclcltAdrRelation {

	//==== PROPERTIES ====
	//Original name: CIAR-EXP-DT
	private String ciarExpDt = DefaultValues.stringVal(Len.CIAR_EXP_DT);
	//Original name: CIAR-SER-ST-CD
	private String ciarSerStCd = DefaultValues.stringVal(Len.CIAR_SER_ST_CD);

	//==== METHODS ====
	public void setCiarExpDt(String ciarExpDt) {
		this.ciarExpDt = Functions.subString(ciarExpDt, Len.CIAR_EXP_DT);
	}

	public String getCiarExpDt() {
		return this.ciarExpDt;
	}

	public void setCiarSerStCd(String ciarSerStCd) {
		this.ciarSerStCd = Functions.subString(ciarSerStCd, Len.CIAR_SER_ST_CD);
	}

	public String getCiarSerStCd() {
		return this.ciarSerStCd;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int CIAR_EFF_DT = 10;
		public static final int ADR_ID = 20;
		public static final int ADR_TYP_CD = 4;
		public static final int TCH_OBJECT_KEY = 20;
		public static final int CIAR_EXP_DT = 10;
		public static final int CIAR_SER_ADR1_TXT = 8;
		public static final int CIAR_SER_CIT_NM = 5;
		public static final int CIAR_SER_ST_CD = 3;
		public static final int CIAR_SER_PST_CD = 6;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CIAR_EFF_ACY_TS = 26;
		public static final int CIAR_EXP_ACY_TS = 26;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
