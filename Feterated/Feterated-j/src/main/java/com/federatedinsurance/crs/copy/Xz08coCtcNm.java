/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZ08CO-CTC-NM<br>
 * Variable: XZ08CO-CTC-NM from copybook XZ08CI1O<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xz08coCtcNm {

	//==== PROPERTIES ====
	//Original name: XZ08CO-CN-DSP-NM
	private String dspNm = DefaultValues.stringVal(Len.DSP_NM);
	//Original name: XZ08CO-CN-SRT-NM
	private String srtNm = DefaultValues.stringVal(Len.SRT_NM);
	//Original name: XZ08CO-CN-CMP-NM
	private String cmpNm = DefaultValues.stringVal(Len.CMP_NM);
	//Original name: XZ08CO-CN-LST-NM
	private String lstNm = DefaultValues.stringVal(Len.LST_NM);
	//Original name: XZ08CO-CN-FST-NM
	private String fstNm = DefaultValues.stringVal(Len.FST_NM);
	//Original name: XZ08CO-CN-MDL-NM
	private String mdlNm = DefaultValues.stringVal(Len.MDL_NM);
	//Original name: XZ08CO-CN-PFX
	private String pfx = DefaultValues.stringVal(Len.PFX);
	//Original name: XZ08CO-CN-SFX
	private String sfx = DefaultValues.stringVal(Len.SFX);

	//==== METHODS ====
	public void setCtcNmBytes(byte[] buffer) {
		setCtcNmBytes(buffer, 1);
	}

	public byte[] getCtcNmBytes() {
		byte[] buffer = new byte[Len.CTC_NM];
		return getCtcNmBytes(buffer, 1);
	}

	public void setCtcNmBytes(byte[] buffer, int offset) {
		int position = offset;
		dspNm = MarshalByte.readString(buffer, position, Len.DSP_NM);
		position += Len.DSP_NM;
		srtNm = MarshalByte.readString(buffer, position, Len.SRT_NM);
		position += Len.SRT_NM;
		cmpNm = MarshalByte.readString(buffer, position, Len.CMP_NM);
		position += Len.CMP_NM;
		lstNm = MarshalByte.readString(buffer, position, Len.LST_NM);
		position += Len.LST_NM;
		fstNm = MarshalByte.readString(buffer, position, Len.FST_NM);
		position += Len.FST_NM;
		mdlNm = MarshalByte.readString(buffer, position, Len.MDL_NM);
		position += Len.MDL_NM;
		pfx = MarshalByte.readString(buffer, position, Len.PFX);
		position += Len.PFX;
		sfx = MarshalByte.readString(buffer, position, Len.SFX);
	}

	public byte[] getCtcNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, dspNm, Len.DSP_NM);
		position += Len.DSP_NM;
		MarshalByte.writeString(buffer, position, srtNm, Len.SRT_NM);
		position += Len.SRT_NM;
		MarshalByte.writeString(buffer, position, cmpNm, Len.CMP_NM);
		position += Len.CMP_NM;
		MarshalByte.writeString(buffer, position, lstNm, Len.LST_NM);
		position += Len.LST_NM;
		MarshalByte.writeString(buffer, position, fstNm, Len.FST_NM);
		position += Len.FST_NM;
		MarshalByte.writeString(buffer, position, mdlNm, Len.MDL_NM);
		position += Len.MDL_NM;
		MarshalByte.writeString(buffer, position, pfx, Len.PFX);
		position += Len.PFX;
		MarshalByte.writeString(buffer, position, sfx, Len.SFX);
		return buffer;
	}

	public void initCtcNmLowValues() {
		dspNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.DSP_NM);
		srtNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.SRT_NM);
		cmpNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.CMP_NM);
		lstNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.LST_NM);
		fstNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.FST_NM);
		mdlNm = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.MDL_NM);
		pfx = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.PFX);
		sfx = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.SFX);
	}

	public void initCtcNmSpaces() {
		dspNm = "";
		srtNm = "";
		cmpNm = "";
		lstNm = "";
		fstNm = "";
		mdlNm = "";
		pfx = "";
		sfx = "";
	}

	public void setDspNm(String dspNm) {
		this.dspNm = Functions.subString(dspNm, Len.DSP_NM);
	}

	public String getDspNm() {
		return this.dspNm;
	}

	public void setSrtNm(String srtNm) {
		this.srtNm = Functions.subString(srtNm, Len.SRT_NM);
	}

	public String getSrtNm() {
		return this.srtNm;
	}

	public void setCmpNm(String cmpNm) {
		this.cmpNm = Functions.subString(cmpNm, Len.CMP_NM);
	}

	public String getCmpNm() {
		return this.cmpNm;
	}

	public void setLstNm(String lstNm) {
		this.lstNm = Functions.subString(lstNm, Len.LST_NM);
	}

	public String getLstNm() {
		return this.lstNm;
	}

	public void setFstNm(String fstNm) {
		this.fstNm = Functions.subString(fstNm, Len.FST_NM);
	}

	public String getFstNm() {
		return this.fstNm;
	}

	public void setMdlNm(String mdlNm) {
		this.mdlNm = Functions.subString(mdlNm, Len.MDL_NM);
	}

	public String getMdlNm() {
		return this.mdlNm;
	}

	public void setPfx(String pfx) {
		this.pfx = Functions.subString(pfx, Len.PFX);
	}

	public String getPfx() {
		return this.pfx;
	}

	public void setSfx(String sfx) {
		this.sfx = Functions.subString(sfx, Len.SFX);
	}

	public String getSfx() {
		return this.sfx;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int DSP_NM = 120;
		public static final int SRT_NM = 60;
		public static final int CMP_NM = 60;
		public static final int LST_NM = 60;
		public static final int FST_NM = 30;
		public static final int MDL_NM = 30;
		public static final int PFX = 4;
		public static final int SFX = 4;
		public static final int CTC_NM = DSP_NM + SRT_NM + CMP_NM + LST_NM + FST_NM + MDL_NM + PFX + SFX;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
