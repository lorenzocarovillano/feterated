/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0B9050<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0b9050 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-POL-TYP-CD
	private short maxPolTypCd = ((short) 9);
	//Original name: CF-ZERO
	private short zero = ((short) 0);

	//==== METHODS ====
	public short getMaxPolTypCd() {
		return this.maxPolTypCd;
	}

	public short getZero() {
		return this.zero;
	}
}
