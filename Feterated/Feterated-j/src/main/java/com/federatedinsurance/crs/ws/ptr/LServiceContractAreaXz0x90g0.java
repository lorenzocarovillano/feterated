/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;

/**Original name: L-SERVICE-CONTRACT-AREA<br>
 * Variable: L-SERVICE-CONTRACT-AREA from program XZ0X90G0<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LServiceContractAreaXz0x90g0 extends BytesClass {

	//==== PROPERTIES ====
	public static final int GO_CERT_REC_LIST_MAXOCCURS = 1000;

	//==== CONSTRUCTORS ====
	public LServiceContractAreaXz0x90g0() {
	}

	public LServiceContractAreaXz0x90g0(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_SERVICE_CONTRACT_AREA;
	}

	public void setGiTkNotPrcTs(String giTkNotPrcTs) {
		writeString(Pos.GI_TK_NOT_PRC_TS, giTkNotPrcTs, Len.GI_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9GI-TK-NOT-PRC-TS<br>*/
	public String getGiTkNotPrcTs() {
		return readString(Pos.GI_TK_NOT_PRC_TS, Len.GI_TK_NOT_PRC_TS);
	}

	public void setGiTkRecallCertNbr(String giTkRecallCertNbr) {
		writeString(Pos.GI_TK_RECALL_CERT_NBR, giTkRecallCertNbr, Len.GI_TK_RECALL_CERT_NBR);
	}

	/**Original name: XZT9GI-TK-RECALL-CERT-NBR<br>*/
	public String getGiTkRecallCertNbr() {
		return readString(Pos.GI_TK_RECALL_CERT_NBR, Len.GI_TK_RECALL_CERT_NBR);
	}

	public void setGiCsrActNbr(String giCsrActNbr) {
		writeString(Pos.GI_CSR_ACT_NBR, giCsrActNbr, Len.GI_CSR_ACT_NBR);
	}

	/**Original name: XZT9GI-CSR-ACT-NBR<br>*/
	public String getGiCsrActNbr() {
		return readString(Pos.GI_CSR_ACT_NBR, Len.GI_CSR_ACT_NBR);
	}

	public void setGiUserid(String giUserid) {
		writeString(Pos.GI_USERID, giUserid, Len.GI_USERID);
	}

	/**Original name: XZT9GI-USERID<br>*/
	public String getGiUserid() {
		return readString(Pos.GI_USERID, Len.GI_USERID);
	}

	public String getGiUseridFormatted() {
		return Functions.padBlanks(getGiUserid(), Len.GI_USERID);
	}

	public void setGoTkNotPrcTs(String goTkNotPrcTs) {
		writeString(Pos.GO_TK_NOT_PRC_TS, goTkNotPrcTs, Len.GO_TK_NOT_PRC_TS);
	}

	/**Original name: XZT9GO-TK-NOT-PRC-TS<br>*/
	public String getGoTkNotPrcTs() {
		return readString(Pos.GO_TK_NOT_PRC_TS, Len.GO_TK_NOT_PRC_TS);
	}

	public void setGoTkRecallCertNbr(String goTkRecallCertNbr) {
		writeString(Pos.GO_TK_RECALL_CERT_NBR, goTkRecallCertNbr, Len.GO_TK_RECALL_CERT_NBR);
	}

	/**Original name: XZT9GO-TK-RECALL-CERT-NBR<br>*/
	public String getGoTkRecallCertNbr() {
		return readString(Pos.GO_TK_RECALL_CERT_NBR, Len.GO_TK_RECALL_CERT_NBR);
	}

	public void setGoCsrActNbr(String goCsrActNbr) {
		writeString(Pos.GO_CSR_ACT_NBR, goCsrActNbr, Len.GO_CSR_ACT_NBR);
	}

	/**Original name: XZT9GO-CSR-ACT-NBR<br>*/
	public String getGoCsrActNbr() {
		return readString(Pos.GO_CSR_ACT_NBR, Len.GO_CSR_ACT_NBR);
	}

	public void setGoCertNbr(int goCertNbrIdx, String goCertNbr) {
		int position = Pos.xzt9goCertNbr(goCertNbrIdx - 1);
		writeString(position, goCertNbr, Len.GO_CERT_NBR);
	}

	/**Original name: XZT9GO-CERT-NBR<br>*/
	public String getGoCertNbr(int goCertNbrIdx) {
		int position = Pos.xzt9goCertNbr(goCertNbrIdx - 1);
		return readString(position, Len.GO_CERT_NBR);
	}

	public void setGoNmAdrLin1(int goNmAdrLin1Idx, String goNmAdrLin1) {
		int position = Pos.xzt9goNmAdrLin1(goNmAdrLin1Idx - 1);
		writeString(position, goNmAdrLin1, Len.GO_NM_ADR_LIN1);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-1<br>*/
	public String getGoNmAdrLin1(int goNmAdrLin1Idx) {
		int position = Pos.xzt9goNmAdrLin1(goNmAdrLin1Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN1);
	}

	public void setGoNmAdrLin2(int goNmAdrLin2Idx, String goNmAdrLin2) {
		int position = Pos.xzt9goNmAdrLin2(goNmAdrLin2Idx - 1);
		writeString(position, goNmAdrLin2, Len.GO_NM_ADR_LIN2);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-2<br>*/
	public String getGoNmAdrLin2(int goNmAdrLin2Idx) {
		int position = Pos.xzt9goNmAdrLin2(goNmAdrLin2Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN2);
	}

	public void setGoNmAdrLin3(int goNmAdrLin3Idx, String goNmAdrLin3) {
		int position = Pos.xzt9goNmAdrLin3(goNmAdrLin3Idx - 1);
		writeString(position, goNmAdrLin3, Len.GO_NM_ADR_LIN3);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-3<br>*/
	public String getGoNmAdrLin3(int goNmAdrLin3Idx) {
		int position = Pos.xzt9goNmAdrLin3(goNmAdrLin3Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN3);
	}

	public void setGoNmAdrLin4(int goNmAdrLin4Idx, String goNmAdrLin4) {
		int position = Pos.xzt9goNmAdrLin4(goNmAdrLin4Idx - 1);
		writeString(position, goNmAdrLin4, Len.GO_NM_ADR_LIN4);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-4<br>*/
	public String getGoNmAdrLin4(int goNmAdrLin4Idx) {
		int position = Pos.xzt9goNmAdrLin4(goNmAdrLin4Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN4);
	}

	public void setGoNmAdrLin5(int goNmAdrLin5Idx, String goNmAdrLin5) {
		int position = Pos.xzt9goNmAdrLin5(goNmAdrLin5Idx - 1);
		writeString(position, goNmAdrLin5, Len.GO_NM_ADR_LIN5);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-5<br>*/
	public String getGoNmAdrLin5(int goNmAdrLin5Idx) {
		int position = Pos.xzt9goNmAdrLin5(goNmAdrLin5Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN5);
	}

	public void setGoNmAdrLin6(int goNmAdrLin6Idx, String goNmAdrLin6) {
		int position = Pos.xzt9goNmAdrLin6(goNmAdrLin6Idx - 1);
		writeString(position, goNmAdrLin6, Len.GO_NM_ADR_LIN6);
	}

	/**Original name: XZT9GO-NM-ADR-LIN-6<br>*/
	public String getGoNmAdrLin6(int goNmAdrLin6Idx) {
		int position = Pos.xzt9goNmAdrLin6(goNmAdrLin6Idx - 1);
		return readString(position, Len.GO_NM_ADR_LIN6);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_SERVICE_CONTRACT_AREA = 1;
		public static final int XZT90G_SERVICE_INPUTS = L_SERVICE_CONTRACT_AREA;
		public static final int GI_TECHNICAL_KEY = XZT90G_SERVICE_INPUTS;
		public static final int GI_TK_NOT_PRC_TS = GI_TECHNICAL_KEY;
		public static final int GI_TK_RECALL_CERT_NBR = GI_TK_NOT_PRC_TS + Len.GI_TK_NOT_PRC_TS;
		public static final int GI_CSR_ACT_NBR = GI_TK_RECALL_CERT_NBR + Len.GI_TK_RECALL_CERT_NBR;
		public static final int GI_USERID = GI_CSR_ACT_NBR + Len.GI_CSR_ACT_NBR;
		public static final int XZT90G_SERVICE_OUTPUTS = GI_USERID + Len.GI_USERID;
		public static final int GO_TECHNICAL_KEY = XZT90G_SERVICE_OUTPUTS;
		public static final int GO_TK_NOT_PRC_TS = GO_TECHNICAL_KEY;
		public static final int GO_TK_RECALL_CERT_NBR = GO_TK_NOT_PRC_TS + Len.GO_TK_NOT_PRC_TS;
		public static final int GO_CSR_ACT_NBR = GO_TK_RECALL_CERT_NBR + Len.GO_TK_RECALL_CERT_NBR;
		public static final int GO_TABLE_OF_CERT_RECS = GO_CSR_ACT_NBR + Len.GO_CSR_ACT_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int xzt9goCertRecList(int idx) {
			return GO_TABLE_OF_CERT_RECS + idx * Len.GO_CERT_REC_LIST;
		}

		public static int xzt9goCertNbr(int idx) {
			return xzt9goCertRecList(idx);
		}

		public static int xzt9goNmAdrLin1(int idx) {
			return xzt9goCertNbr(idx) + Len.GO_CERT_NBR;
		}

		public static int xzt9goNmAdrLin2(int idx) {
			return xzt9goNmAdrLin1(idx) + Len.GO_NM_ADR_LIN1;
		}

		public static int xzt9goNmAdrLin3(int idx) {
			return xzt9goNmAdrLin2(idx) + Len.GO_NM_ADR_LIN2;
		}

		public static int xzt9goNmAdrLin4(int idx) {
			return xzt9goNmAdrLin3(idx) + Len.GO_NM_ADR_LIN3;
		}

		public static int xzt9goNmAdrLin5(int idx) {
			return xzt9goNmAdrLin4(idx) + Len.GO_NM_ADR_LIN4;
		}

		public static int xzt9goNmAdrLin6(int idx) {
			return xzt9goNmAdrLin5(idx) + Len.GO_NM_ADR_LIN5;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int GI_TK_NOT_PRC_TS = 26;
		public static final int GI_TK_RECALL_CERT_NBR = 25;
		public static final int GI_CSR_ACT_NBR = 9;
		public static final int GI_USERID = 8;
		public static final int GO_TK_NOT_PRC_TS = 26;
		public static final int GO_TK_RECALL_CERT_NBR = 25;
		public static final int GO_CSR_ACT_NBR = 9;
		public static final int GO_CERT_NBR = 25;
		public static final int GO_NM_ADR_LIN1 = 30;
		public static final int GO_NM_ADR_LIN2 = 30;
		public static final int GO_NM_ADR_LIN3 = 30;
		public static final int GO_NM_ADR_LIN4 = 30;
		public static final int GO_NM_ADR_LIN5 = 30;
		public static final int GO_NM_ADR_LIN6 = 30;
		public static final int GO_CERT_REC_LIST = GO_CERT_NBR + GO_NM_ADR_LIN1 + GO_NM_ADR_LIN2 + GO_NM_ADR_LIN3 + GO_NM_ADR_LIN4 + GO_NM_ADR_LIN5
				+ GO_NM_ADR_LIN6;
		public static final int GI_TECHNICAL_KEY = GI_TK_NOT_PRC_TS + GI_TK_RECALL_CERT_NBR;
		public static final int XZT90G_SERVICE_INPUTS = GI_TECHNICAL_KEY + GI_CSR_ACT_NBR + GI_USERID;
		public static final int GO_TECHNICAL_KEY = GO_TK_NOT_PRC_TS + GO_TK_RECALL_CERT_NBR;
		public static final int GO_TABLE_OF_CERT_RECS = LServiceContractAreaXz0x90g0.GO_CERT_REC_LIST_MAXOCCURS * GO_CERT_REC_LIST;
		public static final int XZT90G_SERVICE_OUTPUTS = GO_TECHNICAL_KEY + GO_CSR_ACT_NBR + GO_TABLE_OF_CERT_RECS;
		public static final int L_SERVICE_CONTRACT_AREA = XZT90G_SERVICE_INPUTS + XZT90G_SERVICE_OUTPUTS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
