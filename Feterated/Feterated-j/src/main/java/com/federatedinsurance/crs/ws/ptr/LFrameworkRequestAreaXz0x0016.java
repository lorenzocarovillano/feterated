/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.buffer.BytesClass;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X0016<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x0016 extends BytesClass {

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x0016() {
	}

	public LFrameworkRequestAreaXz0x0016(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXzc004ActNotPolFrmRowFormatted(String data) {
		writeString(Pos.XZC004Q_ACT_NOT_POL_FRM_ROW, data, Len.XZC004Q_ACT_NOT_POL_FRM_ROW);
	}

	public String getXzc004ActNotPolFrmRowFormatted() {
		return readFixedString(Pos.XZC004Q_ACT_NOT_POL_FRM_ROW, Len.XZC004Q_ACT_NOT_POL_FRM_ROW);
	}

	public void setXzc004qActNotPolFrmCsumFormatted(String xzc004qActNotPolFrmCsum) {
		writeString(Pos.XZC004Q_ACT_NOT_POL_FRM_CSUM, Trunc.toUnsignedNumeric(xzc004qActNotPolFrmCsum, Len.XZC004Q_ACT_NOT_POL_FRM_CSUM),
				Len.XZC004Q_ACT_NOT_POL_FRM_CSUM);
	}

	/**Original name: XZC004Q-ACT-NOT-POL-FRM-CSUM<br>*/
	public int getXzc004qActNotPolFrmCsum() {
		return readNumDispUnsignedInt(Pos.XZC004Q_ACT_NOT_POL_FRM_CSUM, Len.XZC004Q_ACT_NOT_POL_FRM_CSUM);
	}

	public String getXzc004rActNotPolFrmCsumFormatted() {
		return readFixedString(Pos.XZC004Q_ACT_NOT_POL_FRM_CSUM, Len.XZC004Q_ACT_NOT_POL_FRM_CSUM);
	}

	public void setXzc004qCsrActNbrKcre(String xzc004qCsrActNbrKcre) {
		writeString(Pos.XZC004Q_CSR_ACT_NBR_KCRE, xzc004qCsrActNbrKcre, Len.XZC004Q_CSR_ACT_NBR_KCRE);
	}

	/**Original name: XZC004Q-CSR-ACT-NBR-KCRE<br>*/
	public String getXzc004qCsrActNbrKcre() {
		return readString(Pos.XZC004Q_CSR_ACT_NBR_KCRE, Len.XZC004Q_CSR_ACT_NBR_KCRE);
	}

	public void setXzc004qNotPrcTsKcre(String xzc004qNotPrcTsKcre) {
		writeString(Pos.XZC004Q_NOT_PRC_TS_KCRE, xzc004qNotPrcTsKcre, Len.XZC004Q_NOT_PRC_TS_KCRE);
	}

	/**Original name: XZC004Q-NOT-PRC-TS-KCRE<br>*/
	public String getXzc004qNotPrcTsKcre() {
		return readString(Pos.XZC004Q_NOT_PRC_TS_KCRE, Len.XZC004Q_NOT_PRC_TS_KCRE);
	}

	public void setXzc004qFrmSeqNbrKcre(String xzc004qFrmSeqNbrKcre) {
		writeString(Pos.XZC004Q_FRM_SEQ_NBR_KCRE, xzc004qFrmSeqNbrKcre, Len.XZC004Q_FRM_SEQ_NBR_KCRE);
	}

	/**Original name: XZC004Q-FRM-SEQ-NBR-KCRE<br>*/
	public String getXzc004qFrmSeqNbrKcre() {
		return readString(Pos.XZC004Q_FRM_SEQ_NBR_KCRE, Len.XZC004Q_FRM_SEQ_NBR_KCRE);
	}

	public void setXzc004qPolNbrKcre(String xzc004qPolNbrKcre) {
		writeString(Pos.XZC004Q_POL_NBR_KCRE, xzc004qPolNbrKcre, Len.XZC004Q_POL_NBR_KCRE);
	}

	/**Original name: XZC004Q-POL-NBR-KCRE<br>*/
	public String getXzc004qPolNbrKcre() {
		return readString(Pos.XZC004Q_POL_NBR_KCRE, Len.XZC004Q_POL_NBR_KCRE);
	}

	public void setXzc004qTransProcessDt(String xzc004qTransProcessDt) {
		writeString(Pos.XZC004Q_TRANS_PROCESS_DT, xzc004qTransProcessDt, Len.XZC004Q_TRANS_PROCESS_DT);
	}

	/**Original name: XZC004Q-TRANS-PROCESS-DT<br>*/
	public String getXzc004qTransProcessDt() {
		return readString(Pos.XZC004Q_TRANS_PROCESS_DT, Len.XZC004Q_TRANS_PROCESS_DT);
	}

	public void setXzc004qCsrActNbr(String xzc004qCsrActNbr) {
		writeString(Pos.XZC004Q_CSR_ACT_NBR, xzc004qCsrActNbr, Len.XZC004Q_CSR_ACT_NBR);
	}

	/**Original name: XZC004Q-CSR-ACT-NBR<br>*/
	public String getXzc004qCsrActNbr() {
		return readString(Pos.XZC004Q_CSR_ACT_NBR, Len.XZC004Q_CSR_ACT_NBR);
	}

	public void setXzc004qNotPrcTs(String xzc004qNotPrcTs) {
		writeString(Pos.XZC004Q_NOT_PRC_TS, xzc004qNotPrcTs, Len.XZC004Q_NOT_PRC_TS);
	}

	/**Original name: XZC004Q-NOT-PRC-TS<br>*/
	public String getXzc004qNotPrcTs() {
		return readString(Pos.XZC004Q_NOT_PRC_TS, Len.XZC004Q_NOT_PRC_TS);
	}

	public void setXzc004qFrmSeqNbrSign(char xzc004qFrmSeqNbrSign) {
		writeChar(Pos.XZC004Q_FRM_SEQ_NBR_SIGN, xzc004qFrmSeqNbrSign);
	}

	/**Original name: XZC004Q-FRM-SEQ-NBR-SIGN<br>*/
	public char getXzc004qFrmSeqNbrSign() {
		return readChar(Pos.XZC004Q_FRM_SEQ_NBR_SIGN);
	}

	public void setXzc004qFrmSeqNbrFormatted(String xzc004qFrmSeqNbr) {
		writeString(Pos.XZC004Q_FRM_SEQ_NBR, Trunc.toUnsignedNumeric(xzc004qFrmSeqNbr, Len.XZC004Q_FRM_SEQ_NBR), Len.XZC004Q_FRM_SEQ_NBR);
	}

	/**Original name: XZC004Q-FRM-SEQ-NBR<br>*/
	public int getXzc004qFrmSeqNbr() {
		return readNumDispUnsignedInt(Pos.XZC004Q_FRM_SEQ_NBR, Len.XZC004Q_FRM_SEQ_NBR);
	}

	public String getXzc004rFrmSeqNbrFormatted() {
		return readFixedString(Pos.XZC004Q_FRM_SEQ_NBR, Len.XZC004Q_FRM_SEQ_NBR);
	}

	public void setXzc004qPolNbr(String xzc004qPolNbr) {
		writeString(Pos.XZC004Q_POL_NBR, xzc004qPolNbr, Len.XZC004Q_POL_NBR);
	}

	/**Original name: XZC004Q-POL-NBR<br>*/
	public String getXzc004qPolNbr() {
		return readString(Pos.XZC004Q_POL_NBR, Len.XZC004Q_POL_NBR);
	}

	public void setXzc004qCsrActNbrCi(char xzc004qCsrActNbrCi) {
		writeChar(Pos.XZC004Q_CSR_ACT_NBR_CI, xzc004qCsrActNbrCi);
	}

	public void setXzc004qCsrActNbrCiFormatted(String xzc004qCsrActNbrCi) {
		setXzc004qCsrActNbrCi(Functions.charAt(xzc004qCsrActNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC004Q-CSR-ACT-NBR-CI<br>*/
	public char getXzc004qCsrActNbrCi() {
		return readChar(Pos.XZC004Q_CSR_ACT_NBR_CI);
	}

	public void setXzc004qNotPrcTsCi(char xzc004qNotPrcTsCi) {
		writeChar(Pos.XZC004Q_NOT_PRC_TS_CI, xzc004qNotPrcTsCi);
	}

	public void setXzc004qNotPrcTsCiFormatted(String xzc004qNotPrcTsCi) {
		setXzc004qNotPrcTsCi(Functions.charAt(xzc004qNotPrcTsCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC004Q-NOT-PRC-TS-CI<br>*/
	public char getXzc004qNotPrcTsCi() {
		return readChar(Pos.XZC004Q_NOT_PRC_TS_CI);
	}

	public void setXzc004qFrmSeqNbrCi(char xzc004qFrmSeqNbrCi) {
		writeChar(Pos.XZC004Q_FRM_SEQ_NBR_CI, xzc004qFrmSeqNbrCi);
	}

	public void setXzc004qFrmSeqNbrCiFormatted(String xzc004qFrmSeqNbrCi) {
		setXzc004qFrmSeqNbrCi(Functions.charAt(xzc004qFrmSeqNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC004Q-FRM-SEQ-NBR-CI<br>*/
	public char getXzc004qFrmSeqNbrCi() {
		return readChar(Pos.XZC004Q_FRM_SEQ_NBR_CI);
	}

	public void setXzc004qPolNbrCi(char xzc004qPolNbrCi) {
		writeChar(Pos.XZC004Q_POL_NBR_CI, xzc004qPolNbrCi);
	}

	public void setXzc004qPolNbrCiFormatted(String xzc004qPolNbrCi) {
		setXzc004qPolNbrCi(Functions.charAt(xzc004qPolNbrCi, Types.CHAR_SIZE));
	}

	/**Original name: XZC004Q-POL-NBR-CI<br>*/
	public char getXzc004qPolNbrCi() {
		return readChar(Pos.XZC004Q_POL_NBR_CI);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0C0004 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZC004Q_ACT_NOT_POL_FRM_ROW = L_FW_REQ_XZ0C0004;
		public static final int XZC004Q_ACT_NOT_POL_FRM_FIXED = XZC004Q_ACT_NOT_POL_FRM_ROW;
		public static final int XZC004Q_ACT_NOT_POL_FRM_CSUM = XZC004Q_ACT_NOT_POL_FRM_FIXED;
		public static final int XZC004Q_CSR_ACT_NBR_KCRE = XZC004Q_ACT_NOT_POL_FRM_CSUM + Len.XZC004Q_ACT_NOT_POL_FRM_CSUM;
		public static final int XZC004Q_NOT_PRC_TS_KCRE = XZC004Q_CSR_ACT_NBR_KCRE + Len.XZC004Q_CSR_ACT_NBR_KCRE;
		public static final int XZC004Q_FRM_SEQ_NBR_KCRE = XZC004Q_NOT_PRC_TS_KCRE + Len.XZC004Q_NOT_PRC_TS_KCRE;
		public static final int XZC004Q_POL_NBR_KCRE = XZC004Q_FRM_SEQ_NBR_KCRE + Len.XZC004Q_FRM_SEQ_NBR_KCRE;
		public static final int XZC004Q_ACT_NOT_POL_FRM_DATES = XZC004Q_POL_NBR_KCRE + Len.XZC004Q_POL_NBR_KCRE;
		public static final int XZC004Q_TRANS_PROCESS_DT = XZC004Q_ACT_NOT_POL_FRM_DATES;
		public static final int XZC004Q_ACT_NOT_POL_FRM_KEY = XZC004Q_TRANS_PROCESS_DT + Len.XZC004Q_TRANS_PROCESS_DT;
		public static final int XZC004Q_CSR_ACT_NBR = XZC004Q_ACT_NOT_POL_FRM_KEY;
		public static final int XZC004Q_NOT_PRC_TS = XZC004Q_CSR_ACT_NBR + Len.XZC004Q_CSR_ACT_NBR;
		public static final int XZC004Q_FRM_SEQ_NBR_SIGN = XZC004Q_NOT_PRC_TS + Len.XZC004Q_NOT_PRC_TS;
		public static final int XZC004Q_FRM_SEQ_NBR = XZC004Q_FRM_SEQ_NBR_SIGN + Len.XZC004Q_FRM_SEQ_NBR_SIGN;
		public static final int XZC004Q_POL_NBR = XZC004Q_FRM_SEQ_NBR + Len.XZC004Q_FRM_SEQ_NBR;
		public static final int XZC004Q_ACT_NOT_POL_FRM_KEY_CI = XZC004Q_POL_NBR + Len.XZC004Q_POL_NBR;
		public static final int XZC004Q_CSR_ACT_NBR_CI = XZC004Q_ACT_NOT_POL_FRM_KEY_CI;
		public static final int XZC004Q_NOT_PRC_TS_CI = XZC004Q_CSR_ACT_NBR_CI + Len.XZC004Q_CSR_ACT_NBR_CI;
		public static final int XZC004Q_FRM_SEQ_NBR_CI = XZC004Q_NOT_PRC_TS_CI + Len.XZC004Q_NOT_PRC_TS_CI;
		public static final int XZC004Q_POL_NBR_CI = XZC004Q_FRM_SEQ_NBR_CI + Len.XZC004Q_FRM_SEQ_NBR_CI;
		public static final int XZC004Q_ACT_NOT_POL_FRM_DATA = XZC004Q_POL_NBR_CI + Len.XZC004Q_POL_NBR_CI;
		public static final int FLR1 = XZC004Q_ACT_NOT_POL_FRM_DATA;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZC004Q_ACT_NOT_POL_FRM_CSUM = 9;
		public static final int XZC004Q_CSR_ACT_NBR_KCRE = 32;
		public static final int XZC004Q_NOT_PRC_TS_KCRE = 32;
		public static final int XZC004Q_FRM_SEQ_NBR_KCRE = 32;
		public static final int XZC004Q_POL_NBR_KCRE = 32;
		public static final int XZC004Q_TRANS_PROCESS_DT = 10;
		public static final int XZC004Q_CSR_ACT_NBR = 9;
		public static final int XZC004Q_NOT_PRC_TS = 26;
		public static final int XZC004Q_FRM_SEQ_NBR_SIGN = 1;
		public static final int XZC004Q_FRM_SEQ_NBR = 5;
		public static final int XZC004Q_POL_NBR = 25;
		public static final int XZC004Q_CSR_ACT_NBR_CI = 1;
		public static final int XZC004Q_NOT_PRC_TS_CI = 1;
		public static final int XZC004Q_FRM_SEQ_NBR_CI = 1;
		public static final int XZC004Q_POL_NBR_CI = 1;
		public static final int XZC004Q_ACT_NOT_POL_FRM_FIXED = XZC004Q_ACT_NOT_POL_FRM_CSUM + XZC004Q_CSR_ACT_NBR_KCRE + XZC004Q_NOT_PRC_TS_KCRE
				+ XZC004Q_FRM_SEQ_NBR_KCRE + XZC004Q_POL_NBR_KCRE;
		public static final int XZC004Q_ACT_NOT_POL_FRM_DATES = XZC004Q_TRANS_PROCESS_DT;
		public static final int XZC004Q_ACT_NOT_POL_FRM_KEY = XZC004Q_CSR_ACT_NBR + XZC004Q_NOT_PRC_TS + XZC004Q_FRM_SEQ_NBR_SIGN
				+ XZC004Q_FRM_SEQ_NBR + XZC004Q_POL_NBR;
		public static final int XZC004Q_ACT_NOT_POL_FRM_KEY_CI = XZC004Q_CSR_ACT_NBR_CI + XZC004Q_NOT_PRC_TS_CI + XZC004Q_FRM_SEQ_NBR_CI
				+ XZC004Q_POL_NBR_CI;
		public static final int FLR1 = 1;
		public static final int XZC004Q_ACT_NOT_POL_FRM_DATA = FLR1;
		public static final int XZC004Q_ACT_NOT_POL_FRM_ROW = XZC004Q_ACT_NOT_POL_FRM_FIXED + XZC004Q_ACT_NOT_POL_FRM_DATES
				+ XZC004Q_ACT_NOT_POL_FRM_KEY + XZC004Q_ACT_NOT_POL_FRM_KEY_CI + XZC004Q_ACT_NOT_POL_FRM_DATA;
		public static final int L_FW_REQ_XZ0C0004 = XZC004Q_ACT_NOT_POL_FRM_ROW;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0C0004;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
