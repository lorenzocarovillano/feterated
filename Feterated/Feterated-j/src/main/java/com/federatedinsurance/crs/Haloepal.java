/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpReturnException;
import com.federatedinsurance.crs.ws.DfhcommareaHaloetra;
import com.federatedinsurance.crs.ws.HaloepalData;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.programs.Programs;

/**Original name: HALOEPAL<br>
 * <pre>--------------------------------------------------------------*
 *   PROGRAM TITLE - SERIES III INTERFACE - ERROR LOGGING ALERT  *
 *                                          MODULE               *
 *   WRITTEN BY:  PMSC                                           *
 *                                                               *
 *   DATE WRITTEN: JUN 2000                                      *
 *                                                               *
 *   PLATFORM - HOST                                             *
 *                                                               *
 *   OPERATING SYSTEM - MVS                                      *
 *                                                               *
 *   LANGUAGE - MVS COBOL                                        *
 *                                                               *
 *   PURPOSE - THIS MODULE IS THE MAIN ERROR RECORDING DRIVER    *
 *   MODULE WHICH IS 'STARTED' VIA TRANSACTION 'HELG'.           *
 *                                                               *
 *   PROGRAM INITIATION - CICS START ISSUED BY HALOMDRV, WHEN ALL*
 *                        ERROR ITEMS HAVE BEEN WRITTEN TO VSAM  *
 *                        FILE.                                  *
 *                                                               *
 *   DATA ACCESS METHODS - PASSED INFORMATION                    *
 *                                                               *
 * --------------------------------------------------------------*
 * --------------------------------------------------------------*
 *                 M A I N T E N A N C E    L O G
 *  SI#      DATE      PRGMR     DESCRIPTION
 *  -------- --------- -------   --------------------------------
 *  SAVANNAH 29JUN00   LCP       CREATED.
 *  17241    20FEB02   18448     REMOVE REFERENCE TO IAP.
 * --------------------------------------------------------------*</pre>*/
public class Haloepal extends BatchProgram {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private HaloepalData ws = new HaloepalData();
	private ExecContext execContext = null;
	//Original name: DFHCOMMAREA
	private DfhcommareaHaloetra dfhcommarea = new DfhcommareaHaloetra();

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaHaloetra dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		registerArgListeners();
		mainline();
		return1();
		deleteArgListeners();
		return 0;
	}

	public static Haloepal getInstance() {
		return (Programs.getInstance(Haloepal.class));
	}

	/**Original name: 0000-MAINLINE<br>
	 * <pre>***************************************************************
	 *  A) INITALIZE                                                 *
	 *  B) OTHER STUFF NOT YET CODED                                 *
	 *  C) MORE OTHER STUFF                                          *
	 * ***************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 0100-INITIALIZE.
		rng0100Initialize();
	}

	/**Original name: 0000-RETURN<br>*/
	private void return1() {
		// COB_CODE: EXEC CICS RETURN END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 0100-INITIALIZE<br>
	 * <pre>***************************************************************
	 *  A>  INITIALIZE                                               *
	 * ***************************************************************</pre>*/
	private String initialize() {
		// COB_CODE: INITIALIZE W-WORKFIELDS.
		initWWorkfields();
		// COB_CODE: IF EIBCALEN = ZERO
		//              GO TO 0100-INITIALIZE-X
		//           END-IF.
		if (execContext.getCommAreaLen() == 0) {
			// COB_CODE: GO TO 0100-INITIALIZE-X
			return "0100-INITIALIZE-X";
		}
		// COB_CODE: MOVE DFHCOMMAREA(1:LENGTH OF WS-EPAL-LINK)
		//             TO WS-EPAL-LINK.
		ws.setWsEpalLinkFormatted(dfhcommarea.getDfhcommareaFormatted().substring((1) - 1, HaloepalData.Len.WS_EPAL_LINK));
		return "";
	}

	/**Original name: RNG_0100-INITIALIZE<br>*/
	private void rng0100Initialize() {
		String retcode = "";
		boolean goto0100InitializeX = false;
		boolean gotoExitlabel = false;
		retcode = initialize();
		if (!retcode.equals("0100-INITIALIZE-X")) {
			gotoExitlabel = true;
		}
		if (!gotoExitlabel) {
			goto0100InitializeX = false;
			throw new ReturnException();
		}
	}

	public void initWWorkfields() {
		ws.setwResponse(0);
		ws.setwResponse2(0);
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}

	public void deleteArgListeners() {
		execContext.getCommAreaLenNotifier().deleteListener(dfhcommarea.getLnkCaDataListener());
	}

	public void registerArgListeners() {
		execContext.getCommAreaLenNotifier().addListener(dfhcommarea.getLnkCaDataListener());
		execContext.getCommAreaLenNotifier().notifyListeners();
	}
}
