/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.ICltAdrRelationObj;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for tables [CLT_ADR_RELATION, CLT_OBJ_RELATION]
 * 
 */
public class CltAdrRelationObjDao extends BaseSqlDao<ICltAdrRelationObj> {

	public CltAdrRelationObjDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<ICltAdrRelationObj> getToClass() {
		return ICltAdrRelationObj.class;
	}

	public String selectByCiorShwObjKey(String ciorShwObjKey, String dft) {
		return buildQuery("selectByCiorShwObjKey").bind("ciorShwObjKey", ciorShwObjKey).scalarResultString(dft);
	}
}
