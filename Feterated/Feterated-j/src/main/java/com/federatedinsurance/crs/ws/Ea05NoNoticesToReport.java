/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-05-NO-NOTICES-TO-REPORT<br>
 * Variable: EA-05-NO-NOTICES-TO-REPORT from program XZ004000<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea05NoNoticesToReport {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-05-NO-NOTICES-TO-REPORT
	private String flr1 = " PGM = XZ004000";
	//Original name: FILLER-EA-05-NO-NOTICES-TO-REPORT-1
	private String flr2 = " -";
	//Original name: FILLER-EA-05-NO-NOTICES-TO-REPORT-2
	private String flr3 = "(INFORMATIONAL)";
	//Original name: FILLER-EA-05-NO-NOTICES-TO-REPORT-3
	private String flr4 = " NO NOTICES TO";
	//Original name: FILLER-EA-05-NO-NOTICES-TO-REPORT-4
	private String flr5 = "REPORT.";

	//==== METHODS ====
	public String getEa05NoNoticesToReportFormatted() {
		return MarshalByteExt.bufferToStr(getEa05NoNoticesToReportBytes());
	}

	public byte[] getEa05NoNoticesToReportBytes() {
		byte[] buffer = new byte[Len.EA05_NO_NOTICES_TO_REPORT];
		return getEa05NoNoticesToReportBytes(buffer, 1);
	}

	public byte[] getEa05NoNoticesToReportBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 15;
		public static final int FLR2 = 3;
		public static final int FLR5 = 7;
		public static final int EA05_NO_NOTICES_TO_REPORT = 3 * FLR1 + FLR2 + FLR5;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
