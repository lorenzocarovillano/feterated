/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: CF-NO-TTY-FON-WNG-NOT-TYP<br>
 * Variable: CF-NO-TTY-FON-WNG-NOT-TYP from program XZ0P90H0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfNoTtyFonWngNotTyp {

	//==== PROPERTIES ====
	//Original name: CF-NT-CNI
	private String cni = "CNI";
	//Original name: CF-NT-NPC
	private String npc = "NPC";
	//Original name: CF-NT-CRW
	private String crw = "CRW";

	//==== METHODS ====
	public String getCni() {
		return this.cni;
	}

	public String getNpc() {
		return this.npc;
	}

	public String getCrw() {
		return this.crw;
	}
}
