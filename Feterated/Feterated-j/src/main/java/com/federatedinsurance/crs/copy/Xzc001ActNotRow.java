/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZC001-ACT-NOT-ROW<br>
 * Variable: XZC001-ACT-NOT-ROW from copybook XZ0C0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzc001ActNotRow {

	//==== PROPERTIES ====
	//Original name: XZC001-ACT-NOT-FIXED
	private Xzc001ActNotFixed actNotFixed = new Xzc001ActNotFixed();
	//Original name: XZC001-TRANS-PROCESS-DT
	private String transProcessDt = DefaultValues.stringVal(Len.TRANS_PROCESS_DT);
	//Original name: XZC001-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZC001-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZC001-CSR-ACT-NBR-CI
	private char csrActNbrCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-NOT-PRC-TS-CI
	private char notPrcTsCi = DefaultValues.CHAR_VAL;
	//Original name: XZC001-ACT-NOT-DATA
	private Xzc001ActNotData actNotData = new Xzc001ActNotData();
	//Original name: XZC001-ACT-NOT-TYP-DESC
	private String actNotTypDesc = DefaultValues.stringVal(Len.ACT_NOT_TYP_DESC);

	//==== METHODS ====
	public void setXzc001ActNotRowFormatted(String data) {
		byte[] buffer = new byte[Len.XZC001_ACT_NOT_ROW];
		MarshalByte.writeString(buffer, 1, data, Len.XZC001_ACT_NOT_ROW);
		setXzc001ActNotRowBytes(buffer, 1);
	}

	public String getXzc001ActNotRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzc001ActNotRowBytes());
	}

	public byte[] getXzc001ActNotRowBytes() {
		byte[] buffer = new byte[Len.XZC001_ACT_NOT_ROW];
		return getXzc001ActNotRowBytes(buffer, 1);
	}

	public void setXzc001ActNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFixed.setActNotFixedBytes(buffer, position);
		position += Xzc001ActNotFixed.Len.ACT_NOT_FIXED;
		setActNotDatesBytes(buffer, position);
		position += Len.ACT_NOT_DATES;
		setActNotKeyBytes(buffer, position);
		position += Len.ACT_NOT_KEY;
		setActNotKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_KEY_CI;
		actNotData.setActNotDataBytes(buffer, position);
		position += Xzc001ActNotData.Len.ACT_NOT_DATA;
		setExtensionFieldsBytes(buffer, position);
	}

	public byte[] getXzc001ActNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotFixed.getActNotFixedBytes(buffer, position);
		position += Xzc001ActNotFixed.Len.ACT_NOT_FIXED;
		getActNotDatesBytes(buffer, position);
		position += Len.ACT_NOT_DATES;
		getActNotKeyBytes(buffer, position);
		position += Len.ACT_NOT_KEY;
		getActNotKeyCiBytes(buffer, position);
		position += Len.ACT_NOT_KEY_CI;
		actNotData.getActNotDataBytes(buffer, position);
		position += Xzc001ActNotData.Len.ACT_NOT_DATA;
		getExtensionFieldsBytes(buffer, position);
		return buffer;
	}

	public void setActNotDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		transProcessDt = MarshalByte.readString(buffer, position, Len.TRANS_PROCESS_DT);
	}

	public byte[] getActNotDatesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, transProcessDt, Len.TRANS_PROCESS_DT);
		return buffer;
	}

	public void setTransProcessDt(String transProcessDt) {
		this.transProcessDt = Functions.subString(transProcessDt, Len.TRANS_PROCESS_DT);
	}

	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getTransProcessDtFormatted() {
		return Functions.padBlanks(getTransProcessDt(), Len.TRANS_PROCESS_DT);
	}

	public String getActNotKeyFormatted() {
		return MarshalByteExt.bufferToStr(getActNotKeyBytes());
	}

	/**Original name: XZC001-ACT-NOT-KEY<br>*/
	public byte[] getActNotKeyBytes() {
		byte[] buffer = new byte[Len.ACT_NOT_KEY];
		return getActNotKeyBytes(buffer, 1);
	}

	public void setActNotKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		notPrcTs = MarshalByte.readString(buffer, position, Len.NOT_PRC_TS);
	}

	public byte[] getActNotKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	public void setActNotKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		notPrcTsCi = MarshalByte.readChar(buffer, position);
	}

	public byte[] getActNotKeyCiBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, csrActNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, notPrcTsCi);
		return buffer;
	}

	public void setCsrActNbrCi(char csrActNbrCi) {
		this.csrActNbrCi = csrActNbrCi;
	}

	public char getCsrActNbrCi() {
		return this.csrActNbrCi;
	}

	public void setNotPrcTsCi(char notPrcTsCi) {
		this.notPrcTsCi = notPrcTsCi;
	}

	public char getNotPrcTsCi() {
		return this.notPrcTsCi;
	}

	public void setExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypDesc = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_DESC);
	}

	public byte[] getExtensionFieldsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotTypDesc, Len.ACT_NOT_TYP_DESC);
		return buffer;
	}

	public void setActNotTypDesc(String actNotTypDesc) {
		this.actNotTypDesc = Functions.subString(actNotTypDesc, Len.ACT_NOT_TYP_DESC);
	}

	public String getActNotTypDesc() {
		return this.actNotTypDesc;
	}

	public Xzc001ActNotData getActNotData() {
		return actNotData;
	}

	public Xzc001ActNotFixed getActNotFixed() {
		return actNotFixed;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int TRANS_PROCESS_DT = 10;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_TYP_DESC = 35;
		public static final int ACT_NOT_DATES = TRANS_PROCESS_DT;
		public static final int ACT_NOT_KEY = CSR_ACT_NBR + NOT_PRC_TS;
		public static final int CSR_ACT_NBR_CI = 1;
		public static final int NOT_PRC_TS_CI = 1;
		public static final int ACT_NOT_KEY_CI = CSR_ACT_NBR_CI + NOT_PRC_TS_CI;
		public static final int EXTENSION_FIELDS = ACT_NOT_TYP_DESC;
		public static final int XZC001_ACT_NOT_ROW = Xzc001ActNotFixed.Len.ACT_NOT_FIXED + ACT_NOT_DATES + ACT_NOT_KEY + ACT_NOT_KEY_CI
				+ Xzc001ActNotData.Len.ACT_NOT_DATA + EXTENSION_FIELDS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
