/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program FNC02090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesFnc02090 {

	//==== PROPERTIES ====
	//Original name: EA-01-ERROR
	private Ea01ErrorFnc02090 ea01Error = new Ea01ErrorFnc02090();
	//Original name: EA-02-INVALID-INPUT
	private Ea02InvalidInputFnc02090 ea02InvalidInput = new Ea02InvalidInputFnc02090();
	//Original name: EA-03-CICS-LINK-ERROR
	private Ea03CicsLinkErrorFnc02090 ea03CicsLinkError = new Ea03CicsLinkErrorFnc02090();
	//Original name: EA-04-SOAP-FAULT
	private Ea04SoapFaultFnc02090 ea04SoapFault = new Ea04SoapFaultFnc02090();
	//Original name: EA-05-WEB-URI-ERROR
	private Ea05WebUriError ea05WebUriError = new Ea05WebUriError();
	//Original name: EA-07-TAR-SYS-NOT-FND
	private Ea07TarSysNotFnd ea07TarSysNotFnd = new Ea07TarSysNotFnd();
	//Original name: EA-08-SUB-PGM-ERROR
	private Ea08SubPgmError ea08SubPgmError = new Ea08SubPgmError();

	//==== METHODS ====
	public Ea01ErrorFnc02090 getEa01Error() {
		return ea01Error;
	}

	public Ea02InvalidInputFnc02090 getEa02InvalidInput() {
		return ea02InvalidInput;
	}

	public Ea03CicsLinkErrorFnc02090 getEa03CicsLinkError() {
		return ea03CicsLinkError;
	}

	public Ea04SoapFaultFnc02090 getEa04SoapFault() {
		return ea04SoapFault;
	}

	public Ea05WebUriError getEa05WebUriError() {
		return ea05WebUriError;
	}

	public Ea07TarSysNotFnd getEa07TarSysNotFnd() {
		return ea07TarSysNotFnd;
	}

	public Ea08SubPgmError getEa08SubPgmError() {
		return ea08SubPgmError;
	}
}
