/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-FIRST-CHAR-SW<br>
 * Variable: WS-FIRST-CHAR-SW from program CIWOSDX<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsFirstCharSw {

	//==== PROPERTIES ====
	private char value = 'N';
	public static final char NOT_LOADED = 'N';
	public static final char LOADED = 'Y';

	//==== METHODS ====
	public void setWsFirstCharSw(char wsFirstCharSw) {
		this.value = wsFirstCharSw;
	}

	public void setWsFirstCharSwFormatted(String wsFirstCharSw) {
		setWsFirstCharSw(Functions.charAt(wsFirstCharSw, Types.CHAR_SIZE));
	}

	public char getWsFirstCharSw() {
		return this.value;
	}

	public boolean isNotLoaded() {
		return value == NOT_LOADED;
	}

	public void setLoaded() {
		value = LOADED;
	}
}
