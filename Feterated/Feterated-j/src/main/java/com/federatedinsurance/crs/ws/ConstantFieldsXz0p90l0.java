/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: CONSTANT-FIELDS<br>
 * Variable: CONSTANT-FIELDS from program XZ0P90L0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ConstantFieldsXz0p90l0 {

	//==== PROPERTIES ====
	//Original name: CF-MAX-TBL-POLICIES
	private short maxTblPolicies = ((short) 30);
	//Original name: CF-ACT-NOT-TYP-CD
	private CfActNotTypCdXz0p90l0 actNotTypCd = new CfActNotTypCdXz0p90l0();
	//Original name: CF-ACT-NOT-STA-CD
	private CfActNotStaCd actNotStaCd = new CfActNotStaCd();
	//Original name: CF-ATC-COMMERCIAL-LINES
	private String atcCommercialLines = "CL";
	//Original name: CF-ATC-PERSONAL-LINES
	private String atcPersonalLines = "PL";
	//Original name: CF-ADDED-BY-BUSINESS-WORKS
	private String addedByBusinessWorks = "NA";
	//Original name: CF-BILLING-USERID
	private String billingUserid = "BILLSRV";
	//Original name: CF-GET-BILLING-DETAIL-SVC-INTF
	private String getBillingDetailSvcIntf = "BX0G0003";
	//Original name: CF-PL-PRODUCER-NBR
	private String plProducerNbr = "4-000";
	//Original name: FILLER-CF-PL-PRODUCER-NM
	private String flr1 = "PERSONAL";
	//Original name: FILLER-CF-PL-PRODUCER-NM-1
	private String flr2 = "SERVICE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-2
	private String flr3 = "REPRESENTATIVE";
	//Original name: FILLER-CF-PL-PRODUCER-NM-3
	private String flr4 = "";
	//Original name: CF-SERVICE-PROXY
	private CfServiceProxyXz0p90k0 serviceProxy = new CfServiceProxyXz0p90k0();
	//Original name: CF-NO
	private char no = 'N';

	//==== METHODS ====
	public short getMaxTblPolicies() {
		return this.maxTblPolicies;
	}

	public String getAtcCommercialLines() {
		return this.atcCommercialLines;
	}

	public String getAtcPersonalLines() {
		return this.atcPersonalLines;
	}

	public String getAddedByBusinessWorks() {
		return this.addedByBusinessWorks;
	}

	public String getBillingUserid() {
		return this.billingUserid;
	}

	public String getGetBillingDetailSvcIntf() {
		return this.getBillingDetailSvcIntf;
	}

	public String getPlProducerNbr() {
		return this.plProducerNbr;
	}

	public String getPlProducerNmFormatted() {
		return MarshalByteExt.bufferToStr(getPlProducerNmBytes());
	}

	/**Original name: CF-PL-PRODUCER-NM<br>*/
	public byte[] getPlProducerNmBytes() {
		byte[] buffer = new byte[Len.PL_PRODUCER_NM];
		return getPlProducerNmBytes(buffer, 1);
	}

	public byte[] getPlProducerNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public char getNo() {
		return this.no;
	}

	public CfActNotStaCd getActNotStaCd() {
		return actNotStaCd;
	}

	public CfActNotTypCdXz0p90l0 getActNotTypCd() {
		return actNotTypCd;
	}

	public CfServiceProxyXz0p90k0 getServiceProxy() {
		return serviceProxy;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 9;
		public static final int FLR2 = 8;
		public static final int FLR3 = 14;
		public static final int FLR4 = 89;
		public static final int PL_PRODUCER_NM = FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
