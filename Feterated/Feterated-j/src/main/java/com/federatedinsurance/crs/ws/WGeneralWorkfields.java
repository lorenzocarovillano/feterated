/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.ws.enums.WIdBuildingErr;
import com.federatedinsurance.crs.ws.enums.WMsgIdProcessing;
import com.federatedinsurance.crs.ws.enums.WScrambleProcess;
import com.federatedinsurance.crs.ws.enums.WUbocMsgIdScan;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: W-GENERAL-WORKFIELDS<br>
 * Variable: W-GENERAL-WORKFIELDS from program HALOUIDG<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WGeneralWorkfields {

	//==== PROPERTIES ====
	/**Original name: W-SECONDS-SINCE-1970<br>
	 * <pre>16178      03  WS-PROCESSING-STATUS-SW   PIC X.
	 * 16178          88 ID-PROCESSING-COMPLETE  VALUE 'I'.
	 * 16178          88 CONTINUE-ID-PROCESSING  VALUE 'C'.
	 * *   03       WS-ERROR-MSG-NBR    PIC S9(4)     BINARY.
	 * *   03       WS-DATA-DISP        PIC -Z(8)9.
	 * *   03       W-ERROR-MESSAGE     PIC  X(128).
	 * *   03       W-COUNT             PIC  S9(09) BINARY.
	 * *   03       W-DATE              PIC  X(10).
	 * *   03       W-HOURS.
	 * *     05     W-HOURS-NUM         PIC  99.
	 * *   03       W-MINUTES.
	 * *     05     W-MINUTES-NUM       PIC  99.
	 * *   03       W-SECONDS.
	 * *     05     W-SECONDS-NUM       PIC  99.
	 * *   03       W-HUNDREDTHS.
	 * *     05     W-HUNDREDTHS-NUM    PIC  99.
	 * *   03       W-MILLISECS.
	 * *     05     W-MILLISECS-NUM     PIC  9999.</pre>*/
	private int secondsSince1970 = DefaultValues.BIN_INT_VAL;
	//Original name: W-SECONDS-SINCE-1970-NUM
	private String secondsSince1970Num = DefaultValues.stringVal(Len.SECONDS_SINCE1970_NUM);
	/**Original name: W-16-CHAR-TS-ID<br>
	 * <pre>*                                PIC  9(9).</pre>*/
	private String w16CharTsId = DefaultValues.stringVal(Len.W16_CHAR_TS_ID);
	//Original name: W-ABSTIME
	private long abstime = DefaultValues.BIN_LONG_VAL;
	//Original name: W-ABSTIME-NUM
	private String abstimeNum = DefaultValues.stringVal(Len.ABSTIME_NUM);
	//Original name: W-32-CHAR-UBOC-ID
	private String w32CharUbocId = DefaultValues.stringVal(Len.W32_CHAR_UBOC_ID);
	//Original name: W-32-CHAR-GEND-ID
	private String w32CharGendId = DefaultValues.stringVal(Len.W32_CHAR_GEND_ID);
	//Original name: W-SCRAMBLED-MSG-ID
	private String scrambledMsgId = DefaultValues.stringVal(Len.SCRAMBLED_MSG_ID);
	//Original name: W-FRONTEND-CNTR
	private short frontendCntr = DefaultValues.SHORT_VAL;
	//Original name: W-BACKEND-CNTR
	private short backendCntr = DefaultValues.SHORT_VAL;
	//Original name: W-SCRAMBLED-CNTR
	private short scrambledCntr = DefaultValues.SHORT_VAL;
	//Original name: W-UBOC-MSG-ID-SCAN
	private WUbocMsgIdScan ubocMsgIdScan = new WUbocMsgIdScan();
	//Original name: W-MSG-ID-PROCESSING
	private WMsgIdProcessing msgIdProcessing = new WMsgIdProcessing();
	//Original name: W-SCRAMBLE-PROCESS
	private WScrambleProcess scrambleProcess = new WScrambleProcess();
	//Original name: W-ID-BUILDING-ERR
	private WIdBuildingErr idBuildingErr = new WIdBuildingErr();
	//Original name: W-CHAR-INDEX
	private short charIndex = DefaultValues.SHORT_VAL;
	//Original name: W-TEMP-DIGIT
	private String tempDigit = DefaultValues.stringVal(Len.TEMP_DIGIT);

	//==== METHODS ====
	public void setSecondsSince1970(int secondsSince1970) {
		this.secondsSince1970 = secondsSince1970;
	}

	public int getSecondsSince1970() {
		return this.secondsSince1970;
	}

	public void setSecondsSince1970Num(long secondsSince1970Num) {
		this.secondsSince1970Num = NumericDisplay.asString(secondsSince1970Num, Len.SECONDS_SINCE1970_NUM);
	}

	public long getSecondsSince1970Num() {
		return NumericDisplay.asLong(this.secondsSince1970Num);
	}

	public String getSecondsSince1970NumFormatted() {
		return this.secondsSince1970Num;
	}

	public String getSecondsSince1970NumAsString() {
		return getSecondsSince1970NumFormatted();
	}

	public void setW16CharTsId(String w16CharTsId) {
		this.w16CharTsId = Functions.subString(w16CharTsId, Len.W16_CHAR_TS_ID);
	}

	public String getW16CharTsId() {
		return this.w16CharTsId;
	}

	public String getW16CharTsIdFormatted() {
		return Functions.padBlanks(getW16CharTsId(), Len.W16_CHAR_TS_ID);
	}

	public void setAbstime(long abstime) {
		this.abstime = abstime;
	}

	public long getAbstime() {
		return this.abstime;
	}

	public void setAbstimeNum(long abstimeNum) {
		this.abstimeNum = NumericDisplay.asString(abstimeNum, Len.ABSTIME_NUM);
	}

	public long getAbstimeNum() {
		return NumericDisplay.asLong(this.abstimeNum);
	}

	public String getAbstimeNumFormatted() {
		return this.abstimeNum;
	}

	public void setW32CharUbocId(String w32CharUbocId) {
		this.w32CharUbocId = Functions.subString(w32CharUbocId, Len.W32_CHAR_UBOC_ID);
	}

	public String getW32CharUbocId() {
		return this.w32CharUbocId;
	}

	public String getW32CharUbocIdFormatted() {
		return Functions.padBlanks(getW32CharUbocId(), Len.W32_CHAR_UBOC_ID);
	}

	public void setW32CharGendId(String w32CharGendId) {
		this.w32CharGendId = Functions.subString(w32CharGendId, Len.W32_CHAR_GEND_ID);
	}

	public String getW32CharGendId() {
		return this.w32CharGendId;
	}

	public String getW32CharGendIdFormatted() {
		return Functions.padBlanks(getW32CharGendId(), Len.W32_CHAR_GEND_ID);
	}

	public void setScrambledMsgId(String scrambledMsgId) {
		this.scrambledMsgId = Functions.subString(scrambledMsgId, Len.SCRAMBLED_MSG_ID);
	}

	public String getScrambledMsgId() {
		return this.scrambledMsgId;
	}

	public String getScrambledMsgIdFormatted() {
		return Functions.padBlanks(getScrambledMsgId(), Len.SCRAMBLED_MSG_ID);
	}

	public void setFrontendCntr(short frontendCntr) {
		this.frontendCntr = frontendCntr;
	}

	public short getFrontendCntr() {
		return this.frontendCntr;
	}

	public void setBackendCntr(short backendCntr) {
		this.backendCntr = backendCntr;
	}

	public short getBackendCntr() {
		return this.backendCntr;
	}

	public void setScrambledCntr(short scrambledCntr) {
		this.scrambledCntr = scrambledCntr;
	}

	public short getScrambledCntr() {
		return this.scrambledCntr;
	}

	public void setCharIndex(short charIndex) {
		this.charIndex = charIndex;
	}

	public short getCharIndex() {
		return this.charIndex;
	}

	public void setTempDigitFormatted(String tempDigit) {
		this.tempDigit = Trunc.toUnsignedNumeric(tempDigit, Len.TEMP_DIGIT);
	}

	public short getTempDigit() {
		return NumericDisplay.asShort(this.tempDigit);
	}

	public String getTempDigitFormatted() {
		return this.tempDigit;
	}

	public WIdBuildingErr getIdBuildingErr() {
		return idBuildingErr;
	}

	public WMsgIdProcessing getMsgIdProcessing() {
		return msgIdProcessing;
	}

	public WScrambleProcess getScrambleProcess() {
		return scrambleProcess;
	}

	public WUbocMsgIdScan getUbocMsgIdScan() {
		return ubocMsgIdScan;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SECONDS_SINCE1970_NUM = 10;
		public static final int W16_CHAR_TS_ID = 16;
		public static final int ABSTIME_NUM = 16;
		public static final int W32_CHAR_UBOC_ID = 32;
		public static final int W32_CHAR_GEND_ID = 32;
		public static final int SCRAMBLED_MSG_ID = 32;
		public static final int FRONTEND_CNTR = 2;
		public static final int SCRAMBLED_CNTR = 2;
		public static final int TEMP_DIGIT = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
