/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;

/**Original name: XZT005-SERVICE-OUTPUTS<br>
 * Variable: XZT005-SERVICE-OUTPUTS from copybook XZ0Z0005<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzt005ServiceOutputs {

	//==== PROPERTIES ====
	//Original name: XZT05O-TECHNICAL-KEY
	private Xzt05oTechnicalKey technicalKey = new Xzt05oTechnicalKey();
	//Original name: XZT05O-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZT05O-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZT05O-ACT-NOT-TYP-DESC
	private String actNotTypDesc = DefaultValues.stringVal(Len.ACT_NOT_TYP_DESC);
	//Original name: XZT05O-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZT05O-PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: XZT05O-PDC-NM
	private String pdcNm = DefaultValues.stringVal(Len.PDC_NM);
	//Original name: XZT05O-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZT05O-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZT05O-CER-HLD-NOT-IND
	private char cerHldNotInd = DefaultValues.CHAR_VAL;
	//Original name: XZT05O-ADD-CNC-DAY
	private int addCncDay = DefaultValues.INT_VAL;
	//Original name: XZT05O-REA-DES
	private String reaDes = DefaultValues.stringVal(Len.REA_DES);
	//Original name: XZT05O-FRM-ATC-IND
	private char frmAtcInd = DefaultValues.CHAR_VAL;
	//Original name: FILLER-XZT005-SERVICE-OUTPUTS
	private String flr1 = DefaultValues.stringVal(Len.FLR1);

	//==== METHODS ====
	public void setXzt005ServiceOutputsBytes(byte[] buffer) {
		setXzt005ServiceOutputsBytes(buffer, 1);
	}

	public void setXzt005ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		technicalKey.setTechnicalKeyBytes(buffer, position);
		position += Xzt05oTechnicalKey.Len.TECHNICAL_KEY;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		setActNotTypBytes(buffer, position);
		position += Len.ACT_NOT_TYP;
		notDt = MarshalByte.readString(buffer, position, Len.NOT_DT);
		position += Len.NOT_DT;
		setPdcBytes(buffer, position);
		position += Len.PDC;
		totFeeAmt.assign(MarshalByte.readDecimal(buffer, position, Len.Int.TOT_FEE_AMT, Len.Fract.TOT_FEE_AMT));
		position += Len.TOT_FEE_AMT;
		stAbb = MarshalByte.readString(buffer, position, Len.ST_ABB);
		position += Len.ST_ABB;
		cerHldNotInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		addCncDay = MarshalByte.readInt(buffer, position, Len.ADD_CNC_DAY);
		position += Len.ADD_CNC_DAY;
		reaDes = MarshalByte.readString(buffer, position, Len.REA_DES);
		position += Len.REA_DES;
		frmAtcInd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr1 = MarshalByte.readString(buffer, position, Len.FLR1);
	}

	public byte[] getXzt005ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		technicalKey.getTechnicalKeyBytes(buffer, position);
		position += Xzt05oTechnicalKey.Len.TECHNICAL_KEY;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		getActNotTypBytes(buffer, position);
		position += Len.ACT_NOT_TYP;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		getPdcBytes(buffer, position);
		position += Len.PDC;
		MarshalByte.writeDecimal(buffer, position, totFeeAmt.copy());
		position += Len.TOT_FEE_AMT;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeChar(buffer, position, cerHldNotInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeInt(buffer, position, addCncDay, Len.ADD_CNC_DAY);
		position += Len.ADD_CNC_DAY;
		MarshalByte.writeString(buffer, position, reaDes, Len.REA_DES);
		position += Len.REA_DES;
		MarshalByte.writeChar(buffer, position, frmAtcInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setActNotTypBytes(byte[] buffer, int offset) {
		int position = offset;
		actNotTypCd = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		actNotTypDesc = MarshalByte.readString(buffer, position, Len.ACT_NOT_TYP_DESC);
	}

	public byte[] getActNotTypBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, actNotTypDesc, Len.ACT_NOT_TYP_DESC);
		return buffer;
	}

	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public void setActNotTypDesc(String actNotTypDesc) {
		this.actNotTypDesc = Functions.subString(actNotTypDesc, Len.ACT_NOT_TYP_DESC);
	}

	public String getActNotTypDesc() {
		return this.actNotTypDesc;
	}

	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	public String getNotDt() {
		return this.notDt;
	}

	public void setPdcBytes(byte[] buffer, int offset) {
		int position = offset;
		pdcNbr = MarshalByte.readString(buffer, position, Len.PDC_NBR);
		position += Len.PDC_NBR;
		pdcNm = MarshalByte.readString(buffer, position, Len.PDC_NM);
	}

	public byte[] getPdcBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position += Len.PDC_NBR;
		MarshalByte.writeString(buffer, position, pdcNm, Len.PDC_NM);
		return buffer;
	}

	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public void setPdcNm(String pdcNm) {
		this.pdcNm = Functions.subString(pdcNm, Len.PDC_NM);
	}

	public String getPdcNm() {
		return this.pdcNm;
	}

	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public void setCerHldNotInd(char cerHldNotInd) {
		this.cerHldNotInd = cerHldNotInd;
	}

	public char getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	public void setAddCncDay(int addCncDay) {
		this.addCncDay = addCncDay;
	}

	public int getAddCncDay() {
		return this.addCncDay;
	}

	public void setReaDes(String reaDes) {
		this.reaDes = Functions.subString(reaDes, Len.REA_DES);
	}

	public String getReaDes() {
		return this.reaDes;
	}

	public void setFrmAtcInd(char frmAtcInd) {
		this.frmAtcInd = frmAtcInd;
	}

	public char getFrmAtcInd() {
		return this.frmAtcInd;
	}

	public void setFlr1(String flr1) {
		this.flr1 = Functions.subString(flr1, Len.FLR1);
	}

	public String getFlr1() {
		return this.flr1;
	}

	public Xzt05oTechnicalKey getTechnicalKey() {
		return technicalKey;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int ACT_NOT_TYP_DESC = 35;
		public static final int NOT_DT = 10;
		public static final int PDC_NBR = 5;
		public static final int PDC_NM = 120;
		public static final int ST_ABB = 2;
		public static final int REA_DES = 500;
		public static final int FLR1 = 500;
		public static final int ACT_NOT_TYP = ACT_NOT_TYP_CD + ACT_NOT_TYP_DESC;
		public static final int PDC = PDC_NBR + PDC_NM;
		public static final int TOT_FEE_AMT = 10;
		public static final int CER_HLD_NOT_IND = 1;
		public static final int ADD_CNC_DAY = 5;
		public static final int FRM_ATC_IND = 1;
		public static final int XZT005_SERVICE_OUTPUTS = Xzt05oTechnicalKey.Len.TECHNICAL_KEY + CSR_ACT_NBR + ACT_NOT_TYP + NOT_DT + PDC + TOT_FEE_AMT
				+ ST_ABB + CER_HLD_NOT_IND + ADD_CNC_DAY + REA_DES + FRM_ATC_IND + FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
