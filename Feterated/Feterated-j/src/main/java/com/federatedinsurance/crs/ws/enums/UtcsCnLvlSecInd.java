/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: UTCS-CN-LVL-SEC-IND<br>
 * Variable: UTCS-CN-LVL-SEC-IND from copybook HALLUTCS<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UtcsCnLvlSecInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char CHK_CONTROL_LVL_SEC = 'Y';
	public static final char NO_CONTROL_LVL_SEC = 'N';

	//==== METHODS ====
	public void setCnLvlSecInd(char cnLvlSecInd) {
		this.value = cnLvlSecInd;
	}

	public char getCnLvlSecInd() {
		return this.value;
	}

	public boolean isChkControlLvlSec() {
		return value == CHK_CONTROL_LVL_SEC;
	}
}
