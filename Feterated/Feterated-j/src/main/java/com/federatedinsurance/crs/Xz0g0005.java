/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.pointer.IGetmainManager;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.ws.DfhcommareaXz0g0005;
import com.federatedinsurance.crs.ws.WsProxyProgramArea;
import com.federatedinsurance.crs.ws.Xz0g0005Data;
import com.federatedinsurance.crs.ws.ptr.LServiceContractAreaXz0x0005;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZ0G0005<br>
 * <pre>AUTHOR.       Gary Liedtke.
 * DATE-WRITTEN. 17 MAR 2009.
 * ***************************************************************
 *   PROGRAM TITLE - BATCH COBOL FRAMEWORK PROXY PROGRAM FOR     *
 *                 UOW : XZ_GET_ACT_NOT                          *
 *                 OPERATIONS : GetAccountNotificationDtlByActTS *
 *                                                               *
 *     PURPOSE: The purpose of the program is to pass contract   *
 *              information from a consumer to the COBOL Business*
 *              Framework.                                       *
 *                                                               *
 *     NOTES & ASSUMPTIONS :                                     *
 *     *********************                                     *
 *      - INPUT/OUTPUT = LINKAGE SECTION                         *
 *      - THE MAX COPYBOOK SIZE ALLOWED FOR THE CONSUMER         *
 *        CONTRACT IS 32,767 BYTES                               *
 *                                                               *
 *                                                               *
 * ***************************************************************
 *                                                               *
 *     A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
 *                                                               *
 *    WR #    DATE     EMP ID              DESCRIPTION           *
 *  -------- --------- -------   ------------------------------- *
 *  TO07614  17MAR09   E404GCL   INITIAL PROGRAM                 *
 *  PP02500  07SEP12   E404BPO   RECOMPILE FOR XZ0T0005 CHANGE   *
 * ***************************************************************</pre>*/
public class Xz0g0005 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	@Inject
	private IGetmainManager cicsStorageManager;
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Xz0g0005Data ws = new Xz0g0005Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXz0g0005 dfhcommarea;
	/**Original name: L-SERVICE-CONTRACT<br>
	 * <pre>**  SERVICE CONTRACT AREA</pre>*/
	private LServiceContractAreaXz0x0005 lServiceContract = new LServiceContractAreaXz0x0005(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXz0g0005 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		programExit();
		return 0;
	}

	public static Xz0g0005 getInstance() {
		return (Programs.getInstance(Xz0g0005.class));
	}

	/**Original name: 1000-MAINLINE<br>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF XZT005-NO-ERROR-CODE
		//               CONTINUE
		//           ELSE
		//               GO TO 1000-PROGRAM-EXIT
		//           END-IF.
		if (dfhcommarea.getXzt005ErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
		}
		// COB_CODE: PERFORM 3000-CALL-SERVICE
		//              THRU 3000-EXIT.
		callService();
		// COB_CODE: PERFORM 8000-ENDING-HOUSEKEEPING
		//              THRU 8000-EXIT.
		endingHousekeeping();
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM INITIALIZATION AND OTHER SETUP TASKS.  ALLOCATE      *
	 *  SERVICE INPUT AND SERVICE OUTPUT AREAS.  GET ADDRESSABILITY  *
	 *  VIA THE POINTER PASSED IN.                                   *
	 * ***************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: INITIALIZE SERVICE-PROXY-CONTRACT
		//                      XZT005-ERROR-RETURN-CODE.
		initServiceProxyContract();
		dfhcommarea.getXzt005ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		//***************************************************************
		// FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
		// PROGRAM BEFORE CALLING THE SERVICE PROXY PROGRAM.            *
		//***************************************************************
		// COB_CODE: PERFORM 2100-ALLOCATE-SERVICE-MEMORY
		//              THRU 2100-EXIT.
		allocateServiceMemory();
		// COB_CODE: IF XZT005-FATAL-ERROR-CODE
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getXzt005ErrorReturnCode().isFatalErrorCode()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
	}

	/**Original name: 2100-ALLOCATE-SERVICE-MEMORY<br>
	 * <pre>***************************************************************
	 *  ALLOCATE THE ADDRESS FOR SENDING AND RECEIVING SERVICE INPUT *
	 *  AND OUTPUT DATA.                                             *
	 * ***************************************************************</pre>*/
	private void allocateServiceMemory() {
		// COB_CODE: MOVE LENGTH OF L-SERVICE-CONTRACT
		//                                       TO PPC-SERVICE-DATA-SIZE.
		ws.getServiceProxyContract().setPpcServiceDataSize(LServiceContractAreaXz0x0005.Len.L_SERVICE_CONTRACT_AREA);
		// COB_CODE: EXEC CICS GETMAIN
		//               SET (PPC-SERVICE-DATA-POINTER)
		//               FLENGTH (PPC-SERVICE-DATA-SIZE)
		//               INITIMG (CF-INITIALIZATION-VALUE)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		ws.getServiceProxyContract().setPpcServiceDataPointer(cicsStorageManager.getmainNonshared(execContext,
				ws.getServiceProxyContract().getPpcServiceDataSize(), ws.getCfInitializationValue()));
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 2100-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET XZT005-FATAL-ERROR-CODE
			//               EA-02-PN-2100
			//               EA-02-GETMAIN       TO TRUE
			dfhcommarea.getXzt005ErrorReturnCode().setFatalErrorCode();
			ws.getEa02GetmainFreemainError().getParagraphNbr().setPn2100();
			ws.getEa02GetmainFreemainError().getFlr4().setGetmain();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-02-RESP
			ws.getEa02GetmainFreemainError().setResp(TruncAbs.toInt(ws.getWsResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
			ws.getEa02GetmainFreemainError().setResp2(ws.getWsResponseCode2Formatted());
			// COB_CODE: MOVE EA-02-GETMAIN-FREEMAIN-ERROR
			//                                   TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getEa02GetmainFreemainError().getEa02GetmainFreemainErrorFormatted());
			// COB_CODE: GO TO 2100-EXIT
			return;
		}
		//    SERVICE PROXY IS EXPECTING ADDRESS OF INPUT AND OUTPUT
		//    IN SERVICE DATA POINTER.
		// COB_CODE: SET ADDRESS OF L-SERVICE-CONTRACT
		//                                       TO PPC-SERVICE-DATA-POINTER.
		lServiceContract = ((pointerManager.resolve(ws.getServiceProxyContract().getPpcServiceDataPointer(),
				LServiceContractAreaXz0x0005.class)));
		// COB_CODE: INITIALIZE L-SERVICE-CONTRACT.
		initLServiceContract();
	}

	/**Original name: 3000-CALL-SERVICE<br>
	 * <pre>***************************************************************
	 *  PERFORM SET UP AND MAKE THE CALL TO THE SERVICE PROXY PROGRAM*
	 * ***************************************************************</pre>*/
	private void callService() {
		// COB_CODE: PERFORM 3100-SET-UP-INPUT
		//              THRU 3100-EXIT.
		setUpInput();
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM (CF-SERVICE-PROXY-PGM)
		//               COMMAREA (SERVICE-PROXY-CONTRACT)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZ0G0005", execContext).commarea(ws.getServiceProxyContract()).length(WsProxyProgramArea.Len.WS_PROXY_PROGRAM_AREA)
				.link(ws.getCfServiceProxyPgm(), new Xz0x0005());
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 3000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET XZT005-FATAL-ERROR-CODE
			//               EA-01-PN-3000       TO TRUE
			dfhcommarea.getXzt005ErrorReturnCode().setFatalErrorCode();
			ws.getEa01CicsLinkError().setPn3000();
			// COB_CODE: MOVE CF-SERVICE-PROXY-PGM
			//                                   TO EA-01-LINK-PGM-NAME
			ws.getEa01CicsLinkError().setLinkPgmName(ws.getCfServiceProxyPgm());
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-01-RESP
			ws.getEa01CicsLinkError().setResp(TruncAbs.toInt(ws.getWsResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-01-RESP2
			ws.getEa01CicsLinkError().setResp2(ws.getWsResponseCode2Formatted());
			// COB_CODE: MOVE EA-01-CICS-LINK-ERROR
			//                                   TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getEa01CicsLinkError().getEa01CicsLinkErrorFormatted());
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: IF PPC-NO-ERROR-CODE
		//               CONTINUE
		//           ELSE
		//               END-IF
		//           END-IF.
		if (ws.getServiceProxyContract().getPpcErrorReturnCode().isDsdNoErrorCode()) {
			// COB_CODE: CONTINUE
			//continue
		} else {
			// COB_CODE: PERFORM 3300-CAPTURE-SVC-ERROR-INFO
			//              THRU 3300-EXIT
			captureSvcErrorInfo();
			// COB_CODE: IF PPC-FATAL-ERROR-CODE
			//             OR
			//              PPC-NLBE-CODE
			//               GO TO 3000-EXIT
			//           END-IF
			if (ws.getServiceProxyContract().getPpcErrorReturnCode().isFatalErrorCode()
					|| ws.getServiceProxyContract().getPpcErrorReturnCode().isNlbeCode()) {
				// COB_CODE: GO TO 3000-EXIT
				return;
			}
		}
		// COB_CODE: PERFORM 3200-SET-UP-OUTPUT
		//              THRU 3200-EXIT.
		setUpOutput();
	}

	/**Original name: 3100-SET-UP-INPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE INPUT AREAS WITH VALUES FROM CONSUMER       *
	 *  CONTRACT                                                     *
	 * ***************************************************************
	 *   SERVICE PARAMETERS</pre>*/
	private void setUpInput() {
		// COB_CODE: MOVE XZT005-OPERATION       TO PPC-OPERATION.
		ws.getServiceProxyContract().setPpcOperation(dfhcommarea.getXzt005Operation());
		// COB_CODE: IF XZT005-BYPASS-SYNCPOINT
		//                                       TO TRUE
		//           ELSE
		//                                       TO TRUE
		//           END-IF.
		if (dfhcommarea.getXzt005BypassSyncpointInd().isBypassSyncpoint()) {
			// COB_CODE: SET PPC-BYPASS-SYNCPOINT-IN-MDRV
			//                                   TO TRUE
			ws.getServiceProxyContract().getPpcBypassSyncpointMdrvInd().setBypassSyncpointInMdrv();
		} else {
			// COB_CODE: SET PPC-DO-NOT-BYPASS-SYNCPOINT
			//                                   TO TRUE
			ws.getServiceProxyContract().getPpcBypassSyncpointMdrvInd().setPpcDoNotBypassSyncpoint();
		}
		//  SERVICE INPUT PARMATERS
		//    THE "XZT005-SERVICE-INPUTS" IS A FIELD IN THE PROXY COPYBOOK
		//    THE PROXY COYPBOOK IS INCLUDED IN THIS PROGRAM TWICE.  ONCE
		//    IN THE COPYBOOK THAT IS THE INTERFACE TO THIS PROGRAM AND
		//    BY DIRECT REFERENCE.
		// COB_CODE: MOVE XZT005-SERVICE-INPUTS  OF DFHCOMMAREA
		//                                       TO XZT005-SERVICE-INPUTS
		//                                       OF L-SERVICE-CONTRACT.
		lServiceContract.setXzt005ServiceInputsBytes(dfhcommarea.getXzt005ServiceInputsBytes());
		//  INITIALIZE THE REST OF THE CONSUMER CONTRACT
		// COB_CODE: INITIALIZE XZT005-SERVICE-OUTPUTS
		//                                       OF DFHCOMMAREA
		//                      XZT005-SERVICE-ERROR-INFO.
		initXzt005ServiceOutputs();
		initXzt005ServiceErrorInfo();
	}

	/**Original name: 3200-SET-UP-OUTPUT<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE OUTPUT AREA WITH VALUES FROM SERVICE        *
	 *  CONTRACT AND PROXY.                                          *
	 * ***************************************************************
	 *     THE "XZT005-SERVICE-OUTPUTS" IS A FIELD IN THE PROXY COPYBOOK
	 *     THE PROXY COYPBOOK IS INCLUDED IN THIS PROGRAM TWICE.  ONCE
	 *     IN THE COPYBOOK THAT IS THE INTERFACE TO THIS PROGRAM AND
	 *     BY DIRECT REFERENCE.</pre>*/
	private void setUpOutput() {
		// COB_CODE: MOVE XZT005-SERVICE-OUTPUTS OF L-SERVICE-CONTRACT
		//                                       TO XZT005-SERVICE-OUTPUTS
		//                                       OF DFHCOMMAREA.
		dfhcommarea.getXzt005ServiceOutputs().setXzt005ServiceOutputsBytes(lServiceContract.getXzt005ServiceOutputsBytes());
	}

	/**Original name: 3300-CAPTURE-SVC-ERROR-INFO<br>
	 * <pre>***************************************************************
	 *  POPULATE SERVICE ERROR AREA WITH VALUES FROM THE SERVICE     *
	 *  PROXY CONTRACT.                                              *
	 * ***************************************************************</pre>*/
	private void captureSvcErrorInfo() {
		// COB_CODE: MOVE PPC-ERROR-RETURN-CODE  TO XZT005-ERROR-RETURN-CODE.
		dfhcommarea.getXzt005ErrorReturnCode()
				.setErrorReturnCodeFormatted(ws.getServiceProxyContract().getPpcErrorReturnCode().getDsdErrorReturnCodeFormatted());
		//   CAPTURE THE HIGHEST LEVEL, FIRST OCCURING MESSAGE
		// COB_CODE: EVALUATE TRUE
		//               WHEN PPC-FATAL-ERROR-MESSAGE NOT = SPACES
		//                                       TO XZT005-ERROR-MESSAGE
		//               WHEN PPC-NON-LOGGABLE-ERROR-CNT > +0
		//                                       TO XZT005-ERROR-MESSAGE
		//               WHEN PPC-WARNING-CNT > +0
		//                                       TO XZT005-ERROR-MESSAGE
		//           END-EVALUATE.
		if (!Characters.EQ_SPACE.test(ws.getServiceProxyContract().getPpcFatalErrorMessage())) {
			// COB_CODE: MOVE PPC-FATAL-ERROR-MESSAGE
			//                               TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getServiceProxyContract().getPpcFatalErrorMessage());
		} else if (ws.getServiceProxyContract().getPpcNonLoggableErrorCnt() > 0) {
			// COB_CODE: MOVE PPC-NON-LOG-ERR-MSG (1)
			//                               TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getServiceProxyContract().getPpcNonLoggableErrors(1).getPpcNonLogErrMsg());
		} else if (ws.getServiceProxyContract().getPpcWarningCnt() > 0) {
			// COB_CODE: MOVE PPC-WARN-MSG (1)
			//                               TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getServiceProxyContract().getPpcWarnings(1).getPpcWarnMsg());
		}
	}

	/**Original name: 8000-ENDING-HOUSEKEEPING<br>
	 * <pre>***************************************************************
	 *  PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
	 * ***************************************************************</pre>*/
	private void endingHousekeeping() {
		// COB_CODE: EXEC CICS FREEMAIN
		//               DATA (L-SERVICE-CONTRACT)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		cicsStorageManager.freemain(execContext, pointerManager.addressOf(lServiceContract));
		ws.setWsResponseCode(execContext.getResp());
		ws.setWsResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//               GO TO 8000-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWsResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: SET XZT005-FATAL-ERROR-CODE
			//               EA-02-PN-8000
			//               EA-02-FREEMAIN      TO TRUE
			dfhcommarea.getXzt005ErrorReturnCode().setFatalErrorCode();
			ws.getEa02GetmainFreemainError().getParagraphNbr().setPn8000();
			ws.getEa02GetmainFreemainError().getFlr4().setFreemain();
			// COB_CODE: MOVE WS-RESPONSE-CODE   TO EA-02-RESP
			ws.getEa02GetmainFreemainError().setResp(TruncAbs.toInt(ws.getWsResponseCode(), 8));
			// COB_CODE: MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
			ws.getEa02GetmainFreemainError().setResp2(ws.getWsResponseCode2Formatted());
			// COB_CODE: MOVE EA-02-GETMAIN-FREEMAIN-ERROR
			//                                   TO XZT005-ERROR-MESSAGE
			dfhcommarea.setXzt005ErrorMessage(ws.getEa02GetmainFreemainError().getEa02GetmainFreemainErrorFormatted());
			// COB_CODE: GO TO 8000-EXIT
			return;
		}
	}

	public void initServiceProxyContract() {
		ws.getServiceProxyContract().setPpcServiceDataSize(0);
		ws.getServiceProxyContract().getPpcBypassSyncpointMdrvInd().setPpcBypassSyncpointMdrvInd(Types.SPACE_CHAR);
		ws.getServiceProxyContract().setPpcOperation("");
		ws.getServiceProxyContract().getPpcErrorReturnCode().setErrorReturnCodeFormatted("0000");
		ws.getServiceProxyContract().setPpcFatalErrorMessage("");
		ws.getServiceProxyContract().setPpcNonLoggableErrorCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_NON_LOGGABLE_ERRORS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcNonLoggableErrors(idx0).setPpcNonLogErrMsg("");
		}
		ws.getServiceProxyContract().setPpcWarningCntFormatted("0000");
		for (int idx0 = 1; idx0 <= WsProxyProgramArea.PPC_WARNINGS_MAXOCCURS; idx0++) {
			ws.getServiceProxyContract().getPpcWarnings(idx0).setPpcWarnMsg("");
		}
	}

	public void initLServiceContract() {
		lServiceContract.setXzt05iCsrActNbr("");
		lServiceContract.setXzt05iNotPrcTs("");
		lServiceContract.setXzt05iUserid("");
		lServiceContract.setXzt05oTkNotPrcTs("");
		lServiceContract.setXzt05oTkActOwnCltId("");
		lServiceContract.setXzt05oTkActOwnAdrId("");
		lServiceContract.setXzt05oTkEmpId("");
		lServiceContract.setXzt05oTkStaMdfTs("");
		lServiceContract.setXzt05oTkActNotStaCd("");
		lServiceContract.setXzt05oTkSegCd("");
		lServiceContract.setXzt05oTkActTypCd("");
		lServiceContract.setXzt05oTkActNotCsumFormatted("000000000");
		lServiceContract.setXzt05oCsrActNbr("");
		lServiceContract.setXzt05oActNotTypCd("");
		lServiceContract.setXzt05oActNotTypDesc("");
		lServiceContract.setXzt05oNotDt("");
		lServiceContract.setXzt05oPdcNbr("");
		lServiceContract.setXzt05oPdcNm("");
		lServiceContract.setXzt05oTotFeeAmt(new AfDecimal(0, 10, 2));
		lServiceContract.setXzt05oStAbb("");
		lServiceContract.setXzt05oCerHldNotInd(Types.SPACE_CHAR);
		lServiceContract.setXzt05oAddCncDay(0);
		lServiceContract.setXzt05oReaDes("");
		lServiceContract.setXzt05oFrmAtcInd(Types.SPACE_CHAR);
	}

	public void initXzt005ServiceOutputs() {
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setNotPrcTs("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setActOwnCltId("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setActOwnAdrId("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setEmpId("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setStaMdfTs("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setActNotStaCd("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setSegCd("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setActTypCd("");
		dfhcommarea.getXzt005ServiceOutputs().getTechnicalKey().setActNotCsumFormatted("000000000");
		dfhcommarea.getXzt005ServiceOutputs().setCsrActNbr("");
		dfhcommarea.getXzt005ServiceOutputs().setActNotTypCd("");
		dfhcommarea.getXzt005ServiceOutputs().setActNotTypDesc("");
		dfhcommarea.getXzt005ServiceOutputs().setNotDt("");
		dfhcommarea.getXzt005ServiceOutputs().setPdcNbr("");
		dfhcommarea.getXzt005ServiceOutputs().setPdcNm("");
		dfhcommarea.getXzt005ServiceOutputs().setTotFeeAmt(new AfDecimal(0, 10, 2));
		dfhcommarea.getXzt005ServiceOutputs().setStAbb("");
		dfhcommarea.getXzt005ServiceOutputs().setCerHldNotInd(Types.SPACE_CHAR);
		dfhcommarea.getXzt005ServiceOutputs().setAddCncDay(0);
		dfhcommarea.getXzt005ServiceOutputs().setReaDes("");
		dfhcommarea.getXzt005ServiceOutputs().setFrmAtcInd(Types.SPACE_CHAR);
	}

	public void initXzt005ServiceErrorInfo() {
		dfhcommarea.getXzt005ErrorReturnCode().setErrorReturnCodeFormatted("0000");
		dfhcommarea.setXzt005ErrorMessage("");
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
