/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.util.Functions;

/**Original name: CF-CONTAINER-INFO<br>
 * Variable: CF-CONTAINER-INFO from program XZ0B90S0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class CfContainerInfoXz0b90s0 {

	//==== PROPERTIES ====
	//Original name: CF-CI-CICS-CHANNEL
	private String cicsChannel = "XZPOLLISCALLCHN";
	//Original name: CF-CI-SVC-OUT-CONTAINER
	private String svcOutContainer = "XZPOLSVCLISOUT";
	//Original name: CF-CI-SVC-IN-CONTAINER
	private String svcInContainer = "XZPOLSVCLISIN";

	//==== METHODS ====
	public String getCicsChannel() {
		return this.cicsChannel;
	}

	public String getCicsChannelFormatted() {
		return Functions.padBlanks(getCicsChannel(), Len.CICS_CHANNEL);
	}

	public String getSvcOutContainer() {
		return this.svcOutContainer;
	}

	public String getSvcOutContainerFormatted() {
		return Functions.padBlanks(getSvcOutContainer(), Len.SVC_OUT_CONTAINER);
	}

	public String getSvcInContainer() {
		return this.svcInContainer;
	}

	public String getSvcInContainerFormatted() {
		return Functions.padBlanks(getSvcInContainer(), Len.SVC_IN_CONTAINER);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CICS_CHANNEL = 16;
		public static final int SVC_IN_CONTAINER = 16;
		public static final int SVC_OUT_CONTAINER = 16;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
