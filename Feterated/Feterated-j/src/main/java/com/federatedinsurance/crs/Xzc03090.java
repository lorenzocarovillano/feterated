/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import com.bphx.ctu.af.lang.collection.IAfSet;
import com.bphx.ctu.af.lang.collection.creation.CollectionCreator;
import com.bphx.ctu.af.tp.Channel;
import com.bphx.ctu.af.tp.ExecContext;
import com.bphx.ctu.af.tp.TpAccessStatus;
import com.bphx.ctu.af.tp.TpConditionType;
import com.bphx.ctu.af.tp.TpOutputData;
import com.bphx.ctu.af.tp.TpReturnException;
import com.bphx.ctu.af.tp.TpRunner;
import com.bphx.ctu.af.tp.TsQueueManager;
import com.bphx.ctu.af.util.Trunc;
import com.federatedinsurance.crs.copy.Ivoryh;
import com.federatedinsurance.crs.copy.Xz03ci1o;
import com.federatedinsurance.crs.copy.Xzc030ProgramOutput;
import com.federatedinsurance.crs.ws.DfhcommareaXzc03090;
import com.federatedinsurance.crs.ws.Ts571cb1;
import com.federatedinsurance.crs.ws.WorkingStorageAreaXzc03090;
import com.federatedinsurance.crs.ws.Xzc03090Data;
import com.federatedinsurance.crs.ws.occurs.Xz003oPolCovInf;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;

/**Original name: XZC03090<br>
 * <pre>AUTHOR.       B ONSTAD.
 * DATE-WRITTEN. 03 JUL 2012.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - GetCertPolicyListByAccount INTERFACE PROGRAM**
 * *                                                             **
 * * PURPOSE -  CALLS A CALLABLE WEB SERVICE TO INTERFACE WITH   **
 * *            A CMS WEB SERVICE TO RETRIEVE THE CERTIFICATE    **
 * *            POLICY LIST BY ACCOUNT.                          **
 * *                                                             **
 * * PROGRAM INITIATION -  THIS MODULE IS A CICS PROGRAM.  IT IS **
 * *                       LINKED TO BY AN APPLICATION PROGRAM.  **
 * *                                                             **
 * * DATA ACCESS METHODS - CICS LINKAGE                          **
 * *                       DB2 DATABASE                          **
 * *                                                             **
 * ****************************************************************
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #          DATE      PROG             DESCRIPTION        **
 * * --------   ---------  --------   ---------------------------**
 * * PP02500    07/03/2012 E404BPO    NEW                        **
 * * 11824      05/12/2016  E404JAL   WSRR MIGRATION.            **
 * * 11824.24   09/08/2016 E404JAL    ADD ADDITIONAL ENVIRONMENTS**
 * * 20163      04/08/2019 E404KXS    ADD ADDITIONAL ENVIRONMENTS**
 * *                                                             **
 * ****************************************************************</pre>*/
public class Xzc03090 extends Program {

	//==== PROPERTIES ====
	private ExecContext execContext = null;
	private IAfSet<String> channelSet = CollectionCreator.getStringFactory().createSet();
	//Original name: WORKING-STORAGE
	private Xzc03090Data ws = new Xzc03090Data();
	//Original name: DFHCOMMAREA
	private DfhcommareaXzc03090 dfhcommarea;

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(ExecContext execContext, DfhcommareaXzc03090 dfhcommarea) {
		this.execContext = execContext;
		this.dfhcommarea = dfhcommarea;
		mainline();
		exit();
		return 0;
	}

	public static Xzc03090 getInstance() {
		return (Programs.getInstance(Xzc03090.class));
	}

	/**Original name: 1000-MAINLINE<br>
	 * <pre>*****************************************************************
	 *   MAIN PROCESSING CONTROL
	 * *****************************************************************</pre>*/
	private void mainline() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: IF XZC03O-EC-INVALID-INPUT
		//               GO TO 1000-EXIT
		//           END-IF.
		if (dfhcommarea.getXzc030ProgramOutput().getErrorCode().isInvalidInput()) {
			// COB_CODE: GO TO 1000-EXIT
			exit();
		}
		// COB_CODE: PERFORM 3000-GET-CER-POL-LIS
		//              THRU 3000-EXIT.
		getCerPolLis();
	}

	/**Original name: 1000-EXIT<br>*/
	private void exit() {
		// COB_CODE: EXEC CICS
		//               RETURN
		//           END-EXEC.
		throw new TpReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>****************************************************************
	 *  INITIALIZE OUTPUT AND VERIFY INPUT.
	 * ****************************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: SET XZC03O-EC-NO-ERROR      TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setOk();
		// COB_CODE: PERFORM 2100-VERIFY-INPUT
		//              THRU 2100-EXIT.
		verifyInput();
		// COB_CODE: IF XZC03O-EC-INVALID-INPUT
		//               GO TO 2000-EXIT
		//           END-IF.
		if (dfhcommarea.getXzc030ProgramOutput().getErrorCode().isInvalidInput()) {
			// COB_CODE: GO TO 2000-EXIT
			return;
		}
		// COB_CODE: PERFORM 2200-DET-CICS-ENV-INF
		//              THRU 2200-EXIT.
		detCicsEnvInf();
		// COB_CODE: PERFORM 2300-DET-TAR-SYS
		//              THRU 2300-EXIT.
		detTarSys();
	}

	/**Original name: 2100-VERIFY-INPUT<br>
	 * <pre>****************************************************************
	 *  VERIFY THAT THE CONSUMER PASSED IN VALID INPUTS.
	 * ****************************************************************
	 *     VERIFY ACCOUNT NUMBER WAS PASSED IN</pre>*/
	private void verifyInput() {
		// COB_CODE: IF XZC03I-ACT-NBR = SPACES
		//             OR
		//              XZC03I-ACT-NBR = LOW-VALUES
		//                  THRU 9100-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(dfhcommarea.getiActNbr()) || Characters.EQ_LOW.test(dfhcommarea.getiActNbrFormatted())) {
			// COB_CODE: MOVE CF-PN-2100         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2100());
			// COB_CODE: PERFORM 9100-INVALID-INPUT-ERROR
			//              THRU 9100-EXIT
			invalidInputError();
		}
	}

	/**Original name: 2200-DET-CICS-ENV-INF<br>
	 * <pre>****************************************************************
	 *  GET THE CURRENT CICS REGION TO DETERMINE TARGET SYSTEM.
	 * ****************************************************************</pre>*/
	private void detCicsEnvInf() {
		// COB_CODE: EXEC CICS ASSIGN
		//               APPLID(WS-CICS-APPLID)
		//           END-EXEC.
		ws.getWorkingStorageArea().getCicsApplid().setCicsApplid(execContext.getApplicationId());
		execContext.clearStatus();
	}

	/**Original name: 2300-DET-TAR-SYS<br>
	 * <pre>*****************************************************************
	 *  GET TARGET SYSTEM BASED ON CURRENT CICS REGION
	 * *****************************************************************</pre>*/
	private void detTarSys() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN WS-CA-DEV1
		//                   SET WS-TS-DEV1      TO TRUE
		//               WHEN WS-CA-DEV2
		//                   SET WS-TS-DEV2      TO TRUE
		//               WHEN WS-CA-DEV4
		//                   SET WS-TS-DEV4      TO TRUE
		//               WHEN WS-CA-BETA1
		//                   SET WS-TS-BETA1     TO TRUE
		//               WHEN WS-CA-BETA2
		//                   SET WS-TS-BETA2     TO TRUE
		//               WHEN WS-CA-BETA4
		//                   SET WS-TS-BETA4     TO TRUE
		//               WHEN WS-CA-EDUC1
		//                   SET WS-TS-EDUC1     TO TRUE
		//               WHEN WS-CA-EDUC2
		//                   SET WS-TS-EDUC2     TO TRUE
		//               WHEN WS-CA-PROD
		//                   SET WS-TS-PROD      TO TRUE
		//               WHEN OTHER
		//                      THRU 9600-EXIT
		//           END-EVALUATE.
		if (ws.getWorkingStorageArea().getCicsApplid().isDev1()) {
			// COB_CODE: SET WS-TS-DEV1      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isDev2()) {
			// COB_CODE: SET WS-TS-DEV2      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isDev6()) {
			// COB_CODE: SET WS-TS-DEV4      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setDev4();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta1()) {
			// COB_CODE: SET WS-TS-BETA1     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta2()) {
			// COB_CODE: SET WS-TS-BETA2     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isBeta8()) {
			// COB_CODE: SET WS-TS-BETA4     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setBeta4();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isEduc1()) {
			// COB_CODE: SET WS-TS-EDUC1     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setEduc1();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isEduc2()) {
			// COB_CODE: SET WS-TS-EDUC2     TO TRUE
			ws.getWorkingStorageArea().getTarSys().setEduc2();
		} else if (ws.getWorkingStorageArea().getCicsApplid().isProd()) {
			// COB_CODE: SET WS-TS-PROD      TO TRUE
			ws.getWorkingStorageArea().getTarSys().setProd();
		} else {
			// COB_CODE: MOVE CF-PN-2300     TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn2300());
			// COB_CODE: PERFORM 9600-CICS-APPLID-ERROR
			//              THRU 9600-EXIT
			cicsApplidError();
		}
	}

	/**Original name: 3000-GET-CER-POL-LIS<br>
	 * <pre>*****************************************************************
	 *  CALL THE CRSXWCMSInquiryCallable SERVICE TO RETRIEVE
	 *  THE CERTIFICATE POLICY LIST BY ACCOUNT.
	 * *****************************************************************</pre>*/
	private void getCerPolLis() {
		// COB_CODE: PERFORM 3100-INIT-WEB-SVC-CALL
		//              THRU 3100-EXIT.
		initWebSvcCall();
		// COB_CODE: IF NOT XZC03O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!dfhcommarea.getXzc030ProgramOutput().getErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3200-MOVE-WEB-SVC-INPUT
		//              THRU 3200-EXIT.
		moveWebSvcInput();
		// COB_CODE: IF NOT XZC03O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!dfhcommarea.getXzc030ProgramOutput().getErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3500-CONTAINER-SETUP
		//              THRU 3500-EXIT.
		containerSetup();
		// COB_CODE: IF NOT XZC03O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!dfhcommarea.getXzc030ProgramOutput().getErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3300-LINK-TO-IVORY
		//              THRU 3300-EXIT.
		linkToIvory();
		// COB_CODE: IF NOT XZC03O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!dfhcommarea.getXzc030ProgramOutput().getErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
		// COB_CODE: PERFORM 3400-MOVE-WEB-SVC-OUTPUT
		//              THRU 3400-EXIT.
		moveWebSvcOutput();
		// COB_CODE: IF NOT XZC03O-EC-NO-ERROR
		//               GO TO 3000-EXIT
		//           END-IF.
		if (!dfhcommarea.getXzc030ProgramOutput().getErrorCode().isOk()) {
			// COB_CODE: GO TO 3000-EXIT
			return;
		}
	}

	/**Original name: 3100-INIT-WEB-SVC-CALL<br>
	 * <pre>*****************************************************************
	 *  INITIAL SETUP FOR CALLING IVORY WEB SERVICE PROGRAM
	 * *****************************************************************</pre>*/
	private void initWebSvcCall() {
		// COB_CODE: MOVE LOW-VALUES             TO NON-STATIC-FIELDS
		//                                          CALLABLE-INPUTS
		//                                          CALLABLE-OUTPUTS.
		ws.getIvoryh().getNonStaticFields().initNonStaticFieldsLowValues();
		ws.initCallableInputsLowValues();
		ws.initCallableOutputsLowValues();
		// COB_CODE: MOVE CF-SOAP-SERVICE        TO SOAP-SERVICE IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setService(ws.getConstantFields().getSoapService());
		// COB_CODE: MOVE CF-SOAP-OPERATION      TO SOAP-OPERATION IN IVORYH.
		ws.getIvoryh().getNonStaticFields().setOperation(ws.getConstantFields().getSoapOperation());
		// COB_CODE: MOVE CF-LV-ZLINUX-SRV       TO SOAP-LAYOUT-VERSION.
		ws.getIvoryh().setSoapLayoutVersion(ws.getConstantFields().getLvZlinuxSrv());
		// COB_CODE: SET SOAP-TRACE-STOR-CHARSET-EBCDIC
		//               SOAP-REQUEST-STOR-TYPE-CHCONT
		//               SOAP-RESPONSE-STOR-TYPE-CHCONT
		//               SOAP-TRACE-STOR-TYPE-TDQ
		//               SOAP-ERROR-STOR-TYPE-TDQ
		//                                       TO TRUE.
		ws.getIvoryh().getNonStaticFields().getTraceStorCharset().setEbcdic();
		ws.getIvoryh().getNonStaticFields().getRequestStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getResponseStorType().setChcont();
		ws.getIvoryh().getNonStaticFields().getTraceStorType().setTdq();
		ws.getIvoryh().getNonStaticFields().getErrorStorType().setTdq();
		// COB_CODE: MOVE CF-CI-INP-CONTAINER    TO SOAP-REQUEST-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setRequestStoreValue(ws.getConstantFields().getContainerInfo().getInpContainer());
		// COB_CODE: MOVE CF-CI-OUP-CONTAINER    TO SOAP-RESPONSE-STORE-VALUE.
		ws.getIvoryh().getNonStaticFields().setResponseStoreValue(ws.getConstantFields().getContainerInfo().getOupContainer());
		// COB_CODE: MOVE LENGTH OF CALLABLE-INPUTS
		//                                       TO SOAP-REQUEST-LENGTH.
		ws.getIvoryh().getNonStaticFields().setRequestLength(Xzc03090Data.Len.CALLABLE_INPUTS);
		// COB_CODE: MOVE LENGTH OF CALLABLE-OUTPUTS
		//                                       TO SOAP-RESPONSE-LENGTH.
		ws.getIvoryh().getNonStaticFields().setResponseLength(Xzc03090Data.Len.CALLABLE_OUTPUTS);
		// COB_CODE: MOVE CF-TRACE-STOR-VALUE    TO SOAP-TRACE-STOR-VALUE
		//                                          SOAP-ERROR-STOR-VALUE.
		ws.getIvoryh().getNonStaticFields().setTraceStorValue(ws.getConstantFields().getTraceStorValue());
		ws.getIvoryh().getNonStaticFields().setErrorStorValue(ws.getConstantFields().getTraceStorValue());
	}

	/**Original name: 3200-MOVE-WEB-SVC-INPUT<br>
	 * <pre>*****************************************************************
	 *  SETUP INPUT FOR THE SERVICE CALL TO IVORY
	 * *****************************************************************</pre>*/
	private void moveWebSvcInput() {
		// COB_CODE: INITIALIZE CALLABLE-INPUTS.
		initCallableInputs();
		// COB_CODE: PERFORM 3220-RETRIEVE-SVC-URL
		//              THRU 3220-EXIT.
		retrieveSvcUrl();
		// COB_CODE: MOVE XZC03I-ACT-NBR         TO XZ003I-ACT-NBR.
		ws.setXz003iActNbr(dfhcommarea.getiActNbr());
		// COB_CODE: MOVE WS-WEB-SVC-URL         TO XZ003I-TK-URI.
		ws.getFwppcstk().setTkUri(ws.getWorkingStorageArea().getWebSvcUrl());
		// COB_CODE: PERFORM 3230-RETRIEVE-USR-ID-PWD
		//              THRU 3230-EXIT.
		retrieveUsrIdPwd();
		// COB_CODE: MOVE WS-UP-USR-ID           TO XZ003I-TK-USERID.
		ws.getFwppcstk().setTkUserid(ws.getWorkingStorageArea().getUpUsrId());
		// COB_CODE: MOVE WS-UP-PWD              TO XZ003I-TK-PASSWORD.
		ws.getFwppcstk().setTkPassword(ws.getWorkingStorageArea().getUpPwd());
		// COB_CODE: MOVE WS-TAR-SYS             TO XZ003I-TK-FED-TAR-SYS.
		ws.getFwppcstk().setTkFedTarSys(ws.getWorkingStorageArea().getTarSys().getTarSys());
	}

	/**Original name: 3220-RETRIEVE-SVC-URL<br>
	 * <pre>*****************************************************************
	 *  CALL THE WEB SERVICE URI LOOKUP COMMON ROUTINE
	 * *****************************************************************</pre>*/
	private void retrieveSvcUrl() {
		// COB_CODE: INITIALIZE URI-LKU-LINKAGE.
		initTs571cb1();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO UL-SERVICE-MNEMONIC.
		ws.getTs571cb1().setServiceMnemonic(ws.getConstantFields().getWebSvcId());
		// CALL WEB SERVICE LOOKUP INTERFACE PROGRAM
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM  (CF-WEB-SVC-LKU-PGM)
		//               COMMAREA (URI-LKU-LINKAGE)
		//               LENGTH   (LENGTH OF URI-LKU-LINKAGE)
		//               RESP     (WS-RESPONSE-CODE)
		//               RESP2    (WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC03090", execContext).commarea(ws.getTs571cb1()).length(Ts571cb1.Len.TS571CB1)
				.link(ws.getConstantFields().getWebSvcLkuPgm(), new Ts571099());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// CHECK FOR ERROR FROM CICS LINK
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: MOVE CF-WEB-SVC-LKU-PGM TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getWebSvcLkuPgm());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		// CHECK IF RETURNING URI IS NOT VALID
		// COB_CODE: IF UL-URI = SPACES
		//             OR
		//              UL-URI = LOW-VALUES
		//                  THRU 9130-EXIT
		//           END-IF.
		if (Characters.EQ_SPACE.test(ws.getTs571cb1().getUri()) || Characters.EQ_LOW.test(ws.getTs571cb1().getUri(), Ts571cb1.Len.URI)) {
			// COB_CODE: MOVE CF-PN-3220         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3220());
			// COB_CODE: PERFORM 9130-WEB-URI-NOT-FOUND
			//              THRU 9130-EXIT
			webUriNotFound();
		}
		// COB_CODE: MOVE UL-URI                 TO WS-WEB-SVC-URL.
		ws.getWorkingStorageArea().setWebSvcUrl(ws.getTs571cb1().getUri());
	}

	/**Original name: 3230-RETRIEVE-USR-ID-PWD<br>
	 * <pre>*****************************************************************
	 *  READ THE QUEUE TO GET THE USERID AND PASSWORD.
	 * *****************************************************************</pre>*/
	private void retrieveUsrIdPwd() {
		TpOutputData tsQueueData = null;
		// COB_CODE: EXEC CICS
		//               READQ TS
		//               ITEM (1)
		//               QNAME ('UIDPCRS1')
		//               INTO (WS-USR-ID-PWD)
		//               RESP (WS-RESPONSE-CODE)
		//               RESP2 (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsQueueData = new TpOutputData();
		tsQueueData.setDataLen(WorkingStorageAreaXzc03090.Len.USR_ID_PWD);
		TsQueueManager.read(execContext, "UIDPCRS1", ((short) 1), tsQueueData);
		if (TpAccessStatus.isSuccess(execContext.getResp())) {
			ws.getWorkingStorageArea().setUsrIdPwdBytes(tsQueueData.getData());
		}
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9140-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3230         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3230());
			// COB_CODE: PERFORM 9140-CICS-READQ-ERROR
			//              THRU 9140-EXIT
			cicsReadqError();
		}
	}

	/**Original name: 3300-LINK-TO-IVORY<br>
	 * <pre>*****************************************************************
	 *  CALL THE IVORY PROCESSING PROGRAM
	 * *****************************************************************</pre>*/
	private void linkToIvory() {
		TpOutputData tsOutputData = null;
		// COB_CODE: EXEC CICS LINK
		//               PROGRAM(CF-CALLABLE-SVC-PROGRAM)
		//               CHANNEL(CF-CI-CMS-INQ-CHANNEL)
		//               RESP(WS-RESPONSE-CODE)
		//               RESP2(WS-RESPONSE-CODE2)
		//           END-EXEC.
		TpRunner.context("XZC03090", execContext).channel(ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted())
				.link(ws.getConstantFields().getCallableSvcProgram());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9110-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: MOVE CF-CALLABLE-SVC-PROGRAM
			//                                   TO EA-03-FAILED-LINK-PGM-NAME
			ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setFailedLinkPgmName(ws.getConstantFields().getCallableSvcProgram());
			// COB_CODE: PERFORM 9110-CICS-LINK-ERROR
			//              THRU 9110-EXIT
			cicsLinkError();
		}
		//    In order to do any further error checking, we must get the
		//    containers.
		// COB_CODE: EXEC CICS GET CONTAINER(CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (CF-CI-CMS-INQ-CHANNEL)
		//               INTO       (IVORYH)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		ws.getIvoryh().setIvoryhBytes(tsOutputData.getData());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS GET CONTAINER(CF-CI-OUP-CONTAINER)
		//               CHANNEL    (CF-CI-CMS-INQ-CHANNEL)
		//               INTO       (CALLABLE-OUTPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc03090Data.Len.CALLABLE_OUTPUTS);
		Channel.read(execContext, ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		ws.setCallableOutputsBytes(tsOutputData.getData());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: IF SOAP-RETURNCODE NOT = ZERO
		//                  THRU 9120-EXIT
		//           END-IF.
		if (ws.getIvoryh().getNonStaticFields().getReturncode() != 0) {
			// COB_CODE: MOVE CF-PN-3300         TO EA-01-PARAGRAPH-NBR
			ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3300());
			// COB_CODE: PERFORM 9120-SOAP-FAULT-ERROR
			//              THRU 9120-EXIT
			soapFaultError();
		}
	}

	/**Original name: 3400-MOVE-WEB-SVC-OUTPUT<br>
	 * <pre>*****************************************************************
	 *  PROCESS RESULTS.
	 * *****************************************************************</pre>*/
	private void moveWebSvcOutput() {
		// COB_CODE: INITIALIZE XZC030-PROGRAM-OUTPUT.
		initXzc030ProgramOutput();
		// COB_CODE: PERFORM 3410-MOVE-POL-COV-INF
		//              THRU 3410-EXIT.
		rng3410MovePolCovInf();
		// COB_CODE: PERFORM 3420-MOVE-MSG-INF
		//              THRU 3420-EXIT.
		rng3420MoveMsgInf();
	}

	/**Original name: 3410-MOVE-POL-COV-INF<br>
	 * <pre>*****************************************************************
	 *  MOVE POLICY COVERAGE INFO TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void movePolCovInf() {
		// COB_CODE: MOVE +0                     TO SS-PI.
		ws.getSubscripts().setPi(((short) 0));
	}

	/**Original name: 3410-A<br>*/
	private String a() {
		// COB_CODE: ADD +1                      TO SS-PI.
		ws.getSubscripts().setPi(Trunc.toShort(1 + ws.getSubscripts().getPi(), 4));
		// COB_CODE: IF SS-PI > CF-MAX-POL-COV-INF
		//             OR
		//              XZ003O-POL-NBR(SS-PI) = SPACES
		//               GO TO 3410-EXIT
		//           END-IF.
		if (ws.getSubscripts().getPi() > ws.getConstantFields().getMaxPolCovInf()
				|| Characters.EQ_SPACE.test(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getPolNbr())) {
			// COB_CODE: GO TO 3410-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ003O-POL-NBR(SS-PI)  TO XZC03O-POL-NBR(SS-PI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi())
				.setPolNbr(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getPolNbr());
		// COB_CODE: MOVE XZ003O-POL-ID(SS-PI)   TO XZC03O-POL-ID(SS-PI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi())
				.setPolId(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getPolId());
		// COB_CODE: MOVE XZ003O-LOB-CD(SS-PI)   TO XZC03O-LOB-CD(SS-PI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi())
				.setLobCd(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getLobCd());
		// COB_CODE: MOVE XZ003O-POL-EFF-DT(SS-PI)
		//                                       TO XZC03O-POL-EFF-DT(SS-PI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi())
				.setPolEffDt(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getPolEffDt());
		// COB_CODE: MOVE XZ003O-POL-EXP-DT(SS-PI)
		//                                       TO XZC03O-POL-EXP-DT(SS-PI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi())
				.setPolExpDt(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getPolExpDt());
		// COB_CODE: PERFORM 3411-MOVE-COV-INF
		//              THRU 3411-EXIT.
		rng3411MoveCovInf();
		// COB_CODE: GO TO 3410-A.
		return "3410-A";
	}

	/**Original name: 3411-MOVE-COV-INF<br>
	 * <pre>*****************************************************************
	 *  MOVE COVERAGE INFO TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void moveCovInf() {
		// COB_CODE: MOVE +0                     TO SS-CI.
		ws.getSubscripts().setCi(((short) 0));
	}

	/**Original name: 3411-A<br>*/
	private String a1() {
		// COB_CODE: ADD +1                      TO SS-CI.
		ws.getSubscripts().setCi(Trunc.toShort(1 + ws.getSubscripts().getCi(), 4));
		// COB_CODE: IF SS-CI > CF-MAX-COV-INF
		//             OR
		//              XZ003O-COV-CD(SS-PI,SS-CI) = SPACES
		//               GO TO 3411-EXIT
		//           END-IF.
		if (ws.getSubscripts().getCi() > ws.getConstantFields().getMaxCovInf() || Characters.EQ_SPACE
				.test(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getCovInf(ws.getSubscripts().getCi()).getXzc03oCovCd())) {
			// COB_CODE: GO TO 3411-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ003O-COV-CD(SS-PI,SS-CI)
		//                                       TO XZC03O-COV-CD(SS-PI,SS-CI).
		dfhcommarea.getXzc030ProgramOutput().getPolCovInf(ws.getSubscripts().getPi()).getCovInf(ws.getSubscripts().getCi())
				.setXzc03oCovCd(ws.getXz03ci1o().getPolCovInf(ws.getSubscripts().getPi()).getCovInf(ws.getSubscripts().getCi()).getXzc03oCovCd());
		// COB_CODE: GO TO 3411-A.
		return "3411-A";
	}

	/**Original name: 3420-MOVE-MSG-INF<br>
	 * <pre>*****************************************************************
	 *  MOVE MESSAGE INFO TO THE OUTPUT AREA
	 * *****************************************************************</pre>*/
	private void moveMsgInf() {
		// COB_CODE: MOVE XZ003O-MSG-STA-CD      TO XZC03O-MSG-STA-CD.
		dfhcommarea.getXzc030ProgramOutput().setMsgStaCd(ws.getXz03ci1o().getMsgStaCd());
		// COB_CODE: MOVE XZ003O-MSG-ERR-CD      TO XZC03O-MSG-ERR-CD.
		dfhcommarea.getXzc030ProgramOutput().setMsgErrCd(ws.getXz03ci1o().getMsgErrCd());
		// COB_CODE: MOVE XZ003O-MSG-STA-DES     TO XZC03O-MSG-STA-DES.
		dfhcommarea.getXzc030ProgramOutput().setMsgStaDes(ws.getXz03ci1o().getMsgStaDes());
		// COB_CODE: MOVE +0                     TO SS-EM.
		ws.getSubscripts().setEm(((short) 0));
	}

	/**Original name: 3420-A<br>*/
	private String a2() {
		// COB_CODE: ADD +1                      TO SS-EM.
		ws.getSubscripts().setEm(Trunc.toShort(1 + ws.getSubscripts().getEm(), 4));
		// COB_CODE: IF SS-EM > CF-MAX-MSG-INF
		//             OR
		//              XZ003O-EXT-MSG-STA-CD(SS-EM) = SPACES
		//               GO TO 3420-EXIT
		//           END-IF.
		if (ws.getSubscripts().getEm() > ws.getConstantFields().getMaxMsgInf()
				|| Characters.EQ_SPACE.test(ws.getXz03ci1o().getExtMsgStaLis(ws.getSubscripts().getEm()).getStaCd())) {
			// COB_CODE: GO TO 3420-EXIT
			return "";
		}
		// COB_CODE: MOVE XZ003O-EXT-MSG-STA-CD(SS-EM)
		//                                       TO XZC03O-EXT-MSG-STA-CD(SS-EM).
		dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(ws.getSubscripts().getEm())
				.setStaCd(ws.getXz03ci1o().getExtMsgStaLis(ws.getSubscripts().getEm()).getStaCd());
		// COB_CODE: MOVE XZ003O-EXT-MSG-STA-DES(SS-EM)
		//                                       TO XZC03O-EXT-MSG-STA-DES(SS-EM).
		dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(ws.getSubscripts().getEm())
				.setStaDes(ws.getXz03ci1o().getExtMsgStaLis(ws.getSubscripts().getEm()).getStaDes());
		// COB_CODE: MOVE XZ003O-EXT-MSG-ID-RFR(SS-EM)
		//                                       TO XZC03O-EXT-MSG-ID-RFR(SS-EM).
		dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(ws.getSubscripts().getEm())
				.setIdRfr(ws.getXz03ci1o().getExtMsgStaLis(ws.getSubscripts().getEm()).getIdRfr());
		// COB_CODE: GO TO 3420-A.
		return "3420-A";
	}

	/**Original name: 3500-CONTAINER-SETUP<br>
	 * <pre>*****************************************************************
	 *  SETUP THE CONTAINERS FOR CALLING GIICALS
	 * *****************************************************************</pre>*/
	private void containerSetup() {
		TpOutputData tsOutputData = null;
		// COB_CODE: MOVE CF-PN-3500             TO EA-01-PARAGRAPH-NBR.
		ws.getErrorAndAdviceMessages().getEa01Error().setParagraphNbr(ws.getConstantFields().getParagraphNames().getPn3500());
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-IVORYH-CONTAINER)
		//               CHANNEL    (CF-CI-CMS-INQ-CHANNEL)
		//               FROM       (IVORYH)
		//               FLENGTH    (LENGTH OF IVORYH)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Ivoryh.Len.IVORYH);
		tsOutputData.setData(ws.getIvoryh().getIvoryhBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getIvoryhContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-INP-CONTAINER)
		//               CHANNEL    (CF-CI-CMS-INQ-CHANNEL)
		//               FROM       (CALLABLE-INPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-INPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc03090Data.Len.CALLABLE_INPUTS);
		tsOutputData.setData(ws.getCallableInputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getInpContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
		// COB_CODE: INITIALIZE CALLABLE-OUTPUTS.
		initCallableOutputs();
		// COB_CODE: EXEC CICS PUT
		//               CONTAINER  (CF-CI-OUP-CONTAINER)
		//               CHANNEL    (CF-CI-CMS-INQ-CHANNEL)
		//               FROM       (CALLABLE-OUTPUTS)
		//               FLENGTH    (LENGTH OF CALLABLE-OUTPUTS)
		//               RESP       (WS-RESPONSE-CODE)
		//               RESP2      (WS-RESPONSE-CODE2)
		//           END-EXEC.
		tsOutputData = new TpOutputData();
		tsOutputData.setDataLen(Xzc03090Data.Len.CALLABLE_OUTPUTS);
		tsOutputData.setData(ws.getCallableOutputsBytes());
		Channel.write(execContext, ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted(),
				ws.getConstantFields().getContainerInfo().getOupContainerFormatted(), tsOutputData);
		channelSet.add(ws.getConstantFields().getContainerInfo().getCmsInqChannelFormatted());
		ws.getWorkingStorageArea().setResponseCode(execContext.getResp());
		ws.getWorkingStorageArea().setResponseCode2(execContext.getResp2());
		// COB_CODE: IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
		//                  THRU 9930-EXIT
		//           END-IF.
		if (TpConditionType.valueOf(ws.getWorkingStorageArea().getResponseCode()) != TpConditionType.NORMAL) {
			// COB_CODE: PERFORM 9930-CONTAINER-ERRORS
			//              THRU 9930-EXIT
			containerErrors();
		}
	}

	/**Original name: 9100-INVALID-INPUT-ERROR<br>
	 * <pre>*****************************************************************
	 *  INVALID INPUT ERROR
	 * *****************************************************************</pre>*/
	private void invalidInputError() {
		// COB_CODE: SET XZC03O-EC-INVALID-INPUT TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setInvalidInput();
		// COB_CODE: MOVE XZC03I-ACT-NBR         TO EA-02-ACCOUNT-NUMBER.
		ws.getErrorAndAdviceMessages().getEa02InvalidInput().setEa02AccountNumber(dfhcommarea.getiActNbr());
		// COB_CODE: MOVE EA-02-INVALID-INPUT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa02InvalidInput().getEa02InvalidInputFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9110-CICS-LINK-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS LINK ERROR
	 * *****************************************************************</pre>*/
	private void cicsLinkError() {
		// COB_CODE: SET XZC03O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-03-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-03-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa03CicsLinkError().setResponseCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-03-CICS-LINK-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa03CicsLinkError().getEa03CicsLinkErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9120-SOAP-FAULT-ERROR<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void soapFaultError() {
		// COB_CODE: SET XZC03O-EC-SOAP-FAULT    TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setSystemError();
		// COB_CODE: MOVE SOAP-RETURNCODE        TO EA-04-SOAP-RETURN-CODE.
		ws.getErrorAndAdviceMessages().getEa04SoapFault().setEa04SoapReturnCode(((int) (ws.getIvoryh().getNonStaticFields().getReturncode())));
		// COB_CODE: MOVE EA-04-SOAP-FAULT       TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error().setErrorMessage(ws.getErrorAndAdviceMessages().getEa04SoapFault().getEa04SoapFaultFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9130-WEB-URI-NOT-FOUND<br>
	 * <pre>*****************************************************************
	 *  SOAP FAULT ERROR
	 * *****************************************************************</pre>*/
	private void webUriNotFound() {
		// COB_CODE: SET XZC03O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setFault();
		// COB_CODE: MOVE CF-WEB-SVC-ID          TO EA-05-WEB-SRV-ID.
		ws.getErrorAndAdviceMessages().getEa05WebUriFault().setEa05WebSvcId(ws.getConstantFields().getWebSvcId());
		// COB_CODE: MOVE EA-05-WEB-URI-FAULT    TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa05WebUriFault().getEa05WebUriFaultFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9140-CICS-READQ-ERROR<br>
	 * <pre>*****************************************************************
	 *  CICS READQ ERROR
	 * *****************************************************************</pre>*/
	private void cicsReadqError() {
		// COB_CODE: SET XZC03O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-07-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-07-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa07ReadqError().setCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-07-READQ-ERROR      TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa07ReadqError().getEa07ReadqErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9600-CICS-APPLID-ERROR<br>
	 * <pre>*****************************************************************
	 *  CALL TO DATAFLUX SERVICE HAD AN ERROR
	 * *****************************************************************</pre>*/
	private void cicsApplidError() {
		// COB_CODE: SET XZC03O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setFault();
		// COB_CODE: MOVE WS-CICS-APPLID         TO EA-08-CICS-RGN.
		ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().setEa08CicsRgn(ws.getWorkingStorageArea().getCicsApplid().getCicsApplid());
		// COB_CODE: MOVE EA-08-TAR-SYS-NOT-FND  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa08TarSysNotFnd().getEa08TarSysNotFndFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: 9930-CONTAINER-ERRORS<br>
	 * <pre>**********************************************************
	 *   REPORT ON CONTAINER ERRORS, SET PARAGRAPH BEFOREHAND   *
	 *         MOVE CF-PN-9999         TO EA-01-PARAGRAPH-NBR   *
	 * **********************************************************</pre>*/
	private void containerErrors() {
		// COB_CODE: SET XZC03O-EC-SYSTEM-ERROR  TO TRUE.
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setFault();
		// COB_CODE: MOVE WS-RESPONSE-CODE       TO EA-06-RESPONSE-CODE.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode(ws.getWorkingStorageArea().getResponseCode());
		// COB_CODE: MOVE WS-RESPONSE-CODE2      TO EA-06-RESPONSE-CODE2.
		ws.getErrorAndAdviceMessages().getEa06ContainerError().setCode2(ws.getWorkingStorageArea().getResponseCode2());
		// COB_CODE: MOVE EA-06-CONTAINER-ERROR  TO EA-01-ERROR-MESSAGE.
		ws.getErrorAndAdviceMessages().getEa01Error()
				.setErrorMessage(ws.getErrorAndAdviceMessages().getEa06ContainerError().getEa06ContainerErrorFormatted());
		// COB_CODE: MOVE EA-01-ERROR            TO XZ003O-MSG-STA-DES.
		ws.getXz03ci1o().setMsgStaDes(ws.getErrorAndAdviceMessages().getEa01Error().getEa01ErrorFormatted());
		// COB_CODE: GO TO 1000-EXIT.
		exit();
	}

	/**Original name: RNG_3410-MOVE-POL-COV-INF-_-3410-EXIT<br>*/
	private void rng3410MovePolCovInf() {
		String retcode = "";
		boolean goto3410A = false;
		movePolCovInf();
		do {
			goto3410A = false;
			retcode = a();
		} while (retcode.equals("3410-A"));
	}

	/**Original name: RNG_3411-MOVE-COV-INF-_-3411-EXIT<br>*/
	private void rng3411MoveCovInf() {
		String retcode = "";
		boolean goto3411A = false;
		moveCovInf();
		do {
			goto3411A = false;
			retcode = a1();
		} while (retcode.equals("3411-A"));
	}

	/**Original name: RNG_3420-MOVE-MSG-INF-_-3420-EXIT<br>*/
	private void rng3420MoveMsgInf() {
		String retcode = "";
		boolean goto3420A = false;
		moveMsgInf();
		do {
			goto3420A = false;
			retcode = a2();
		} while (retcode.equals("3420-A"));
	}

	public void initCallableInputs() {
		ws.getFwppcstk().setTkUserid("");
		ws.getFwppcstk().setTkPassword("");
		ws.getFwppcstk().setTkUri("");
		ws.getFwppcstk().setTkFedTarSys("");
		ws.setXz003iActNbr("");
	}

	public void initTs571cb1() {
		ws.getTs571cb1().setServiceMnemonic("");
		ws.getTs571cb1().setUri("");
		ws.getTs571cb1().setCicsSysid("");
		ws.getTs571cb1().setServicePrefix("");
	}

	public void initXzc030ProgramOutput() {
		for (int idx0 = 1; idx0 <= Xzc030ProgramOutput.POL_COV_INF_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).setPolNbr("");
			dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).setPolId("");
			dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).setLobCd("");
			dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).setPolEffDt("");
			dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).setPolExpDt("");
			for (int idx1 = 1; idx1 <= Xz003oPolCovInf.COV_INF_MAXOCCURS; idx1++) {
				dfhcommarea.getXzc030ProgramOutput().getPolCovInf(idx0).getCovInf(idx1).setXzc03oCovCd("");
			}
		}
		dfhcommarea.getXzc030ProgramOutput().setMsgStaCd("");
		dfhcommarea.getXzc030ProgramOutput().setMsgErrCd("");
		dfhcommarea.getXzc030ProgramOutput().setMsgStaDes("");
		for (int idx0 = 1; idx0 <= Xzc030ProgramOutput.EXT_MSG_STA_LIS_MAXOCCURS; idx0++) {
			dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(idx0).setStaCd("");
			dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(idx0).setStaDes("");
			dfhcommarea.getXzc030ProgramOutput().getExtMsgStaLis(idx0).setIdRfr("");
		}
		dfhcommarea.getXzc030ProgramOutput().getErrorCode().setRetCd(((short) 0));
		dfhcommarea.getXzc030ProgramOutput().setErrorMessage("");
	}

	public void initCallableOutputs() {
		for (int idx0 = 1; idx0 <= Xz03ci1o.POL_COV_INF_MAXOCCURS; idx0++) {
			ws.getXz03ci1o().getPolCovInf(idx0).setPolNbr("");
			ws.getXz03ci1o().getPolCovInf(idx0).setPolId("");
			ws.getXz03ci1o().getPolCovInf(idx0).setLobCd("");
			ws.getXz03ci1o().getPolCovInf(idx0).setPolEffDt("");
			ws.getXz03ci1o().getPolCovInf(idx0).setPolExpDt("");
			for (int idx1 = 1; idx1 <= Xz003oPolCovInf.COV_INF_MAXOCCURS; idx1++) {
				ws.getXz03ci1o().getPolCovInf(idx0).getCovInf(idx1).setXzc03oCovCd("");
			}
		}
		ws.getXz03ci1o().setMsgStaCd("");
		ws.getXz03ci1o().setMsgErrCd("");
		ws.getXz03ci1o().setMsgStaDes("");
		for (int idx0 = 1; idx0 <= Xz03ci1o.EXT_MSG_STA_LIS_MAXOCCURS; idx0++) {
			ws.getXz03ci1o().getExtMsgStaLis(idx0).setStaCd("");
			ws.getXz03ci1o().getExtMsgStaLis(idx0).setStaDes("");
			ws.getXz03ci1o().getExtMsgStaLis(idx0).setIdRfr("");
		}
	}

	public ExecContext getExecContext() {
		return execContext;
	}

	public void setExecContext(ExecContext execContext) {
		this.execContext = execContext;
	}
}
