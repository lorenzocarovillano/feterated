/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-SC-EMP-NONEMP-CD<br>
 * Variable: L-SC-EMP-NONEMP-CD from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScEmpNonempCd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char EMP_CD = 'E';
	public static final char NONEMP_CD = 'N';
	public static final char BOTH_CD = 'B';

	//==== METHODS ====
	public void setEmpNonempCd(char empNonempCd) {
		this.value = empNonempCd;
	}

	public char getEmpNonempCd() {
		return this.value;
	}

	public void setEmpCd() {
		value = EMP_CD;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EMP_NONEMP_CD = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
