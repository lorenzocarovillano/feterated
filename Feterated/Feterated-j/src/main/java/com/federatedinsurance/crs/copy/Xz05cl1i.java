/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.occurs.Xz005iStaCdLis;

/**Original name: XZ05CL1I<br>
 * Copybook: XZ05CL1I from copybook XZ05CL1I<br>
 * Generated as a class for rule COPYBOOK_USAGE_THRESHOLD.<br>*/
public class Xz05cl1i {

	//==== PROPERTIES ====
	public static final int STA_CD_LIS_MAXOCCURS = 2;
	//Original name: XZ005I-ACT-NBR
	private String actNbr = DefaultValues.stringVal(Len.ACT_NBR);
	//Original name: XZ005I-STA-CD-LIS
	private Xz005iStaCdLis[] staCdLis = new Xz005iStaCdLis[STA_CD_LIS_MAXOCCURS];

	//==== CONSTRUCTORS ====
	public Xz05cl1i() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for (int staCdLisIdx = 1; staCdLisIdx <= STA_CD_LIS_MAXOCCURS; staCdLisIdx++) {
			staCdLis[staCdLisIdx - 1] = new Xz005iStaCdLis();
		}
	}

	public void initXz05cl1iLowValues() {
		initGetCerLisLowValues();
	}

	public byte[] getGetCerLisBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, actNbr, Len.ACT_NBR);
		position += Len.ACT_NBR;
		for (int idx = 1; idx <= STA_CD_LIS_MAXOCCURS; idx++) {
			staCdLis[idx - 1].getStaCdLisBytes(buffer, position);
			position += Xz005iStaCdLis.Len.STA_CD_LIS;
		}
		return buffer;
	}

	public void initGetCerLisLowValues() {
		actNbr = LiteralGenerator.create(Types.LOW_CHAR_VAL, Len.ACT_NBR);
		for (int idx = 1; idx <= STA_CD_LIS_MAXOCCURS; idx++) {
			staCdLis[idx - 1].initStaCdLisLowValues();
		}
	}

	public void setActNbr(String actNbr) {
		this.actNbr = Functions.subString(actNbr, Len.ACT_NBR);
	}

	public String getActNbr() {
		return this.actNbr;
	}

	public Xz005iStaCdLis getStaCdLis(int idx) {
		return staCdLis[idx - 1];
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_NBR = 9;
		public static final int GET_CER_LIS = ACT_NBR + Xz05cl1i.STA_CD_LIS_MAXOCCURS * Xz005iStaCdLis.Len.STA_CD_LIS;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
