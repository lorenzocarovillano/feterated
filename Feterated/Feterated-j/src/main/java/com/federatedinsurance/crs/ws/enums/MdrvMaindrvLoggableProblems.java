/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: MDRV-MAINDRV-LOGGABLE-PROBLEMS<br>
 * Variable: MDRV-MAINDRV-LOGGABLE-PROBLEMS from copybook HALLMDRV<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class MdrvMaindrvLoggableProblems {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char LOGGABLE_ERRS = 'E';
	public static final char LOGGABLE_WARNS = 'W';
	public static final char OK = Types.SPACE_CHAR;

	//==== METHODS ====
	public void setMaindrvLoggableProblems(char maindrvLoggableProblems) {
		this.value = maindrvLoggableProblems;
	}

	public void setMaindrvLoggableProblemsFormatted(String maindrvLoggableProblems) {
		setMaindrvLoggableProblems(Functions.charAt(maindrvLoggableProblems, Types.CHAR_SIZE));
	}

	public char getMaindrvLoggableProblems() {
		return this.value;
	}

	public boolean isLoggableErrs() {
		return value == LOGGABLE_ERRS;
	}

	public void setMdrvMaindrvrLoggableErrs() {
		value = LOGGABLE_ERRS;
	}

	public boolean isLoggableWarns() {
		return value == LOGGABLE_WARNS;
	}

	public void setOk() {
		setMaindrvLoggableProblemsFormatted(String.valueOf(OK));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAINDRV_LOGGABLE_PROBLEMS = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
