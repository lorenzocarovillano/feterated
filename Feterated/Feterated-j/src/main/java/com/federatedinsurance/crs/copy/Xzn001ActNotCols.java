/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

/**Original name: XZN001-ACT-NOT-COLS<br>
 * Variable: XZN001-ACT-NOT-COLS from copybook XZ0N0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Xzn001ActNotCols {

	//==== PROPERTIES ====
	//Original name: XZN001-TRANS-PROCESS-DT
	private String transProcessDt = "Transaction Processing Date";
	//Original name: XZN001-ACT-NOT-TYP-CD
	private String actNotTypCd = "Act Not Typ Cd";
	//Original name: XZN001-NOT-DT
	private String notDt = "Not Dt";
	//Original name: XZN001-ACT-OWN-CLT-ID
	private String actOwnCltId = "Act Own Clt Id";
	//Original name: XZN001-ACT-OWN-ADR-ID
	private String actOwnAdrId = "Act Own Adr Id";
	//Original name: XZN001-EMP-ID
	private String empId = "Emp Id";
	//Original name: XZN001-STA-MDF-TS
	private String staMdfTs = "Sta Mdf Ts";
	//Original name: XZN001-ACT-NOT-STA-CD
	private String actNotStaCd = "Act Not Sta Cd";
	//Original name: XZN001-PDC-NBR
	private String pdcNbr = "Pdc Nbr";
	//Original name: XZN001-PDC-NM
	private String pdcNm = "Pdc Nm";
	//Original name: XZN001-SEG-CD
	private String segCd = "Seg Cd";
	//Original name: XZN001-TOT-FEE-AMT
	private String totFeeAmt = "Tot Fee Amt";
	//Original name: XZN001-ST-ABB
	private String stAbb = "St Abb";
	//Original name: XZN001-CER-HLD-NOT-IND
	private String cerHldNotInd = "Cer Hld Not Ind";
	//Original name: XZN001-ADD-CNC-DAY
	private String addCncDay = "Add Cnc Day";
	//Original name: XZN001-REA-DES
	private String reaDes = "Rea Des";

	//==== METHODS ====
	public String getTransProcessDt() {
		return this.transProcessDt;
	}

	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	public String getNotDt() {
		return this.notDt;
	}

	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	public String getEmpId() {
		return this.empId;
	}

	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public String getPdcNm() {
		return this.pdcNm;
	}

	public String getSegCd() {
		return this.segCd;
	}

	public String getTotFeeAmt() {
		return this.totFeeAmt;
	}

	public String getStAbb() {
		return this.stAbb;
	}

	public String getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	public String getAddCncDay() {
		return this.addCncDay;
	}

	public String getReaDes() {
		return this.reaDes;
	}
}
