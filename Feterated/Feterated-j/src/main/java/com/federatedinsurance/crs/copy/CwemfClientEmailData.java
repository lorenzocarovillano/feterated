/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.CwemfMoreRowsSw;

/**Original name: CWEMF-CLIENT-EMAIL-DATA<br>
 * Variable: CWEMF-CLIENT-EMAIL-DATA from copybook CAWLF0EM<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class CwemfClientEmailData {

	//==== PROPERTIES ====
	/**Original name: CWEMF-EMAIL-TYPE-CD-CI<br>
	 * <pre>*  FIELDS PERTAINING TO COLUMNS ON TABLE:</pre>*/
	private char emailTypeCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-EMAIL-TYPE-CD
	private String emailTypeCd = DefaultValues.stringVal(Len.EMAIL_TYPE_CD);
	//Original name: CWEMF-USER-ID-CI
	private char userIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CWEMF-STATUS-CD-CI
	private char statusCdCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-TERMINAL-ID-CI
	private char terminalIdCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CWEMF-EXPIRATION-DT-CI
	private char expirationDtCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-EXPIRATION-DT
	private String expirationDt = DefaultValues.stringVal(Len.EXPIRATION_DT);
	//Original name: CWEMF-EFFECTIVE-ACY-TS-CI
	private char effectiveAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-EFFECTIVE-ACY-TS
	private String effectiveAcyTs = DefaultValues.stringVal(Len.EFFECTIVE_ACY_TS);
	//Original name: CWEMF-EXPIRATION-ACY-TS-CI
	private char expirationAcyTsCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-EXPIRATION-ACY-TS-NI
	private char expirationAcyTsNi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-EXPIRATION-ACY-TS
	private String expirationAcyTs = DefaultValues.stringVal(Len.EXPIRATION_ACY_TS);
	//Original name: CWEMF-CIEM-EMAIL-ADR-TXT-CI
	private char ciemEmailAdrTxtCi = DefaultValues.CHAR_VAL;
	//Original name: CWEMF-CIEM-EMAIL-ADR-TXT
	private String ciemEmailAdrTxt = DefaultValues.stringVal(Len.CIEM_EMAIL_ADR_TXT);
	/**Original name: CWEMF-MORE-ROWS-SW<br>
	 * <pre>* SWITCH FOR FETCH PRIORITY REQUEST.
	 * * IT HANDLES WHETHER THERE ARE MORE THAN ONE ROW OR NOT.</pre>*/
	private CwemfMoreRowsSw moreRowsSw = new CwemfMoreRowsSw();
	//Original name: CWEMF-EMAIL-TYPE-CD-DESC
	private String emailTypeCdDesc = DefaultValues.stringVal(Len.EMAIL_TYPE_CD_DESC);

	//==== METHODS ====
	public void setClientEmailDataBytes(byte[] buffer, int offset) {
		int position = offset;
		emailTypeCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		emailTypeCd = MarshalByte.readString(buffer, position, Len.EMAIL_TYPE_CD);
		position += Len.EMAIL_TYPE_CD;
		userIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		userId = MarshalByte.readString(buffer, position, Len.USER_ID);
		position += Len.USER_ID;
		statusCdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		statusCd = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		terminalId = MarshalByte.readString(buffer, position, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		expirationDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationDt = MarshalByte.readString(buffer, position, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		effectiveAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		effectiveAcyTs = MarshalByte.readString(buffer, position, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		expirationAcyTsCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTsNi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		expirationAcyTs = MarshalByte.readString(buffer, position, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		ciemEmailAdrTxtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciemEmailAdrTxt = MarshalByte.readString(buffer, position, Len.CIEM_EMAIL_ADR_TXT);
		position += Len.CIEM_EMAIL_ADR_TXT;
		moreRowsSw.setMoreRowsSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		emailTypeCdDesc = MarshalByte.readString(buffer, position, Len.EMAIL_TYPE_CD_DESC);
	}

	public byte[] getClientEmailDataBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, emailTypeCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, emailTypeCd, Len.EMAIL_TYPE_CD);
		position += Len.EMAIL_TYPE_CD;
		MarshalByte.writeChar(buffer, position, userIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, userId, Len.USER_ID);
		position += Len.USER_ID;
		MarshalByte.writeChar(buffer, position, statusCdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, statusCd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, terminalIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, terminalId, Len.TERMINAL_ID);
		position += Len.TERMINAL_ID;
		MarshalByte.writeChar(buffer, position, expirationDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationDt, Len.EXPIRATION_DT);
		position += Len.EXPIRATION_DT;
		MarshalByte.writeChar(buffer, position, effectiveAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
		position += Len.EFFECTIVE_ACY_TS;
		MarshalByte.writeChar(buffer, position, expirationAcyTsCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, expirationAcyTsNi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, expirationAcyTs, Len.EXPIRATION_ACY_TS);
		position += Len.EXPIRATION_ACY_TS;
		MarshalByte.writeChar(buffer, position, ciemEmailAdrTxtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciemEmailAdrTxt, Len.CIEM_EMAIL_ADR_TXT);
		position += Len.CIEM_EMAIL_ADR_TXT;
		MarshalByte.writeChar(buffer, position, moreRowsSw.getMoreRowsSw());
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, emailTypeCdDesc, Len.EMAIL_TYPE_CD_DESC);
		return buffer;
	}

	public void setEmailTypeCdCi(char emailTypeCdCi) {
		this.emailTypeCdCi = emailTypeCdCi;
	}

	public char getEmailTypeCdCi() {
		return this.emailTypeCdCi;
	}

	public void setEmailTypeCd(String emailTypeCd) {
		this.emailTypeCd = Functions.subString(emailTypeCd, Len.EMAIL_TYPE_CD);
	}

	public String getEmailTypeCd() {
		return this.emailTypeCd;
	}

	public void setUserIdCi(char userIdCi) {
		this.userIdCi = userIdCi;
	}

	public char getUserIdCi() {
		return this.userIdCi;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCdCi(char statusCdCi) {
		this.statusCdCi = statusCdCi;
	}

	public char getStatusCdCi() {
		return this.statusCdCi;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalIdCi(char terminalIdCi) {
		this.terminalIdCi = terminalIdCi;
	}

	public char getTerminalIdCi() {
		return this.terminalIdCi;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setExpirationDtCi(char expirationDtCi) {
		this.expirationDtCi = expirationDtCi;
	}

	public char getExpirationDtCi() {
		return this.expirationDtCi;
	}

	public void setExpirationDt(String expirationDt) {
		this.expirationDt = Functions.subString(expirationDt, Len.EXPIRATION_DT);
	}

	public String getExpirationDt() {
		return this.expirationDt;
	}

	public void setEffectiveAcyTsCi(char effectiveAcyTsCi) {
		this.effectiveAcyTsCi = effectiveAcyTsCi;
	}

	public char getEffectiveAcyTsCi() {
		return this.effectiveAcyTsCi;
	}

	public void setEffectiveAcyTs(String effectiveAcyTs) {
		this.effectiveAcyTs = Functions.subString(effectiveAcyTs, Len.EFFECTIVE_ACY_TS);
	}

	public String getEffectiveAcyTs() {
		return this.effectiveAcyTs;
	}

	public void setExpirationAcyTsCi(char expirationAcyTsCi) {
		this.expirationAcyTsCi = expirationAcyTsCi;
	}

	public char getExpirationAcyTsCi() {
		return this.expirationAcyTsCi;
	}

	public void setExpirationAcyTsNi(char expirationAcyTsNi) {
		this.expirationAcyTsNi = expirationAcyTsNi;
	}

	public char getExpirationAcyTsNi() {
		return this.expirationAcyTsNi;
	}

	public void setExpirationAcyTs(String expirationAcyTs) {
		this.expirationAcyTs = Functions.subString(expirationAcyTs, Len.EXPIRATION_ACY_TS);
	}

	public String getExpirationAcyTs() {
		return this.expirationAcyTs;
	}

	public void setCiemEmailAdrTxtCi(char ciemEmailAdrTxtCi) {
		this.ciemEmailAdrTxtCi = ciemEmailAdrTxtCi;
	}

	public char getCiemEmailAdrTxtCi() {
		return this.ciemEmailAdrTxtCi;
	}

	public void setCiemEmailAdrTxt(String ciemEmailAdrTxt) {
		this.ciemEmailAdrTxt = Functions.subString(ciemEmailAdrTxt, Len.CIEM_EMAIL_ADR_TXT);
	}

	public String getCiemEmailAdrTxt() {
		return this.ciemEmailAdrTxt;
	}

	public void setEmailTypeCdDesc(String emailTypeCdDesc) {
		this.emailTypeCdDesc = Functions.subString(emailTypeCdDesc, Len.EMAIL_TYPE_CD_DESC);
	}

	public String getEmailTypeCdDesc() {
		return this.emailTypeCdDesc;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int EMAIL_TYPE_CD = 3;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int EXPIRATION_DT = 10;
		public static final int EFFECTIVE_ACY_TS = 26;
		public static final int EXPIRATION_ACY_TS = 26;
		public static final int CIEM_EMAIL_ADR_TXT = 255;
		public static final int EMAIL_TYPE_CD_DESC = 40;
		public static final int EMAIL_TYPE_CD_CI = 1;
		public static final int USER_ID_CI = 1;
		public static final int STATUS_CD_CI = 1;
		public static final int STATUS_CD = 1;
		public static final int TERMINAL_ID_CI = 1;
		public static final int EXPIRATION_DT_CI = 1;
		public static final int EFFECTIVE_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_CI = 1;
		public static final int EXPIRATION_ACY_TS_NI = 1;
		public static final int CIEM_EMAIL_ADR_TXT_CI = 1;
		public static final int CLIENT_EMAIL_DATA = EMAIL_TYPE_CD_CI + EMAIL_TYPE_CD + USER_ID_CI + USER_ID + STATUS_CD_CI + STATUS_CD
				+ TERMINAL_ID_CI + TERMINAL_ID + EXPIRATION_DT_CI + EXPIRATION_DT + EFFECTIVE_ACY_TS_CI + EFFECTIVE_ACY_TS + EXPIRATION_ACY_TS_CI
				+ EXPIRATION_ACY_TS_NI + EXPIRATION_ACY_TS + CIEM_EMAIL_ADR_TXT_CI + CIEM_EMAIL_ADR_TXT + CwemfMoreRowsSw.Len.MORE_ROWS_SW
				+ EMAIL_TYPE_CD_DESC;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
