/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;

/**Original name: EA-09-MAX-POLICIES<br>
 * Variable: EA-09-MAX-POLICIES from program XZC06090<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea09MaxPolicies {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-09-MAX-POLICIES
	private String flr1 = "MORE POLICIES";
	//Original name: FILLER-EA-09-MAX-POLICIES-1
	private String flr2 = "EXIST THAN WHAT";
	//Original name: FILLER-EA-09-MAX-POLICIES-2
	private String flr3 = " CAN BE";
	//Original name: FILLER-EA-09-MAX-POLICIES-3
	private String flr4 = "RETURNED.";

	//==== METHODS ====
	public String getEa09MaxPoliciesFormatted() {
		return MarshalByteExt.bufferToStr(getEa09MaxPoliciesBytes());
	}

	public byte[] getEa09MaxPoliciesBytes() {
		byte[] buffer = new byte[Len.EA09_MAX_POLICIES];
		return getEa09MaxPoliciesBytes(buffer, 1);
	}

	public byte[] getEa09MaxPoliciesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR4);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 14;
		public static final int FLR2 = 15;
		public static final int FLR3 = 8;
		public static final int FLR4 = 9;
		public static final int EA09_MAX_POLICIES = FLR1 + FLR2 + FLR3 + FLR4;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
