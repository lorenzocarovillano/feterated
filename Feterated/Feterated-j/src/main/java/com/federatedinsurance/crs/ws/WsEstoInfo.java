/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.federatedinsurance.crs.copy.EstoInputKey;
import com.federatedinsurance.crs.copy.EstoOutput;
import com.federatedinsurance.crs.ws.enums.EstoCallEtraSw;
import com.federatedinsurance.crs.ws.redefines.EstoDetailBuffer;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-ESTO-INFO<br>
 * Variable: WS-ESTO-INFO from program HALOUIDG<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsEstoInfo extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: ESTO-INPUT-KEY
	private EstoInputKey estoInputKey = new EstoInputKey();
	/**Original name: ESTO-CALL-ETRA-SW<br>
	 * <pre>**       07 ESTO-ERR-SEQ-NUM                PIC 9(03).</pre>*/
	private EstoCallEtraSw estoCallEtraSw = new EstoCallEtraSw();
	//Original name: ESTO-DETAIL-BUFFER
	private EstoDetailBuffer estoDetailBuffer = new EstoDetailBuffer();
	//Original name: ESTO-OUTPUT
	private EstoOutput estoOutput = new EstoOutput();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_ESTO_INFO;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsEstoInfoBytes(buf);
	}

	public void setWsEstoLinkFormatted(String data) {
		byte[] buffer = new byte[Len.WS_ESTO_INFO];
		MarshalByte.writeString(buffer, 1, data, Len.WS_ESTO_INFO);
		setWsEstoInfoBytes(buffer, 1);
	}

	public void setWsEstoInfoBytes(byte[] buffer) {
		setWsEstoInfoBytes(buffer, 1);
	}

	public byte[] getWsEstoInfoBytes() {
		byte[] buffer = new byte[Len.WS_ESTO_INFO];
		return getWsEstoInfoBytes(buffer, 1);
	}

	public void setWsEstoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		setEstoStoreInfoBytes(buffer, position);
		position += Len.ESTO_STORE_INFO;
		setEstoReturnInfoBytes(buffer, position);
	}

	public byte[] getWsEstoInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		getEstoStoreInfoBytes(buffer, position);
		position += Len.ESTO_STORE_INFO;
		getEstoReturnInfoBytes(buffer, position);
		return buffer;
	}

	public void setEstoStoreInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoInputKey.setEstoInputKeyBytes(buffer, position);
		position += EstoInputKey.Len.ESTO_INPUT_KEY;
		estoCallEtraSw.setEstoCallEtraSw(MarshalByte.readChar(buffer, position));
		position += Types.CHAR_SIZE;
		estoDetailBuffer.setEstoDetailBufferFromBuffer(buffer, position);
	}

	public byte[] getEstoStoreInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoInputKey.getEstoInputKeyBytes(buffer, position);
		position += EstoInputKey.Len.ESTO_INPUT_KEY;
		MarshalByte.writeChar(buffer, position, estoCallEtraSw.getEstoCallEtraSw());
		position += Types.CHAR_SIZE;
		estoDetailBuffer.getEstoDetailBufferAsBuffer(buffer, position);
		return buffer;
	}

	public void setEstoReturnInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoOutput.setEstoOutputBytes(buffer, position);
	}

	public byte[] getEstoReturnInfoBytes(byte[] buffer, int offset) {
		int position = offset;
		estoOutput.getEstoOutputBytes(buffer, position);
		return buffer;
	}

	public EstoCallEtraSw getEstoCallEtraSw() {
		return estoCallEtraSw;
	}

	public EstoDetailBuffer getEstoDetailBuffer() {
		return estoDetailBuffer;
	}

	public EstoInputKey getEstoInputKey() {
		return estoInputKey;
	}

	public EstoOutput getEstoOutput() {
		return estoOutput;
	}

	@Override
	public byte[] serialize() {
		return getWsEstoInfoBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ESTO_STORE_INFO = EstoInputKey.Len.ESTO_INPUT_KEY + EstoCallEtraSw.Len.ESTO_CALL_ETRA_SW
				+ EstoDetailBuffer.Len.ESTO_DETAIL_BUFFER;
		public static final int ESTO_RETURN_INFO = EstoOutput.Len.ESTO_OUTPUT;
		public static final int WS_ESTO_INFO = ESTO_STORE_INFO + ESTO_RETURN_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
