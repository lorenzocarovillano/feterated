/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.federatedinsurance.crs.copy.Xza9o0XmlReqInfo;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0A90O0-ROW<br>
 * Variable: WS-XZ0A90O0-ROW from program XZ0B90O0<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0a90o0Row extends SerializableParameter {

	//==== PROPERTIES ====
	//Original name: XZA9O0-MAX-XML-REQ-ROWS
	private short maxXmlReqRows = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZA9O0-XML-REQ-INFO
	private Xza9o0XmlReqInfo xmlReqInfo = new Xza9o0XmlReqInfo();

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0A90O0_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0a90o0RowBytes(buf);
	}

	public String getWsXz0a90o0RowFormatted() {
		return getXmlReqRowFormatted();
	}

	public void setWsXz0a90o0RowBytes(byte[] buffer) {
		setWsXz0a90o0RowBytes(buffer, 1);
	}

	public byte[] getWsXz0a90o0RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0A90O0_ROW];
		return getWsXz0a90o0RowBytes(buffer, 1);
	}

	public void setWsXz0a90o0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setXmlReqRowBytes(buffer, position);
	}

	public byte[] getWsXz0a90o0RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getXmlReqRowBytes(buffer, position);
		return buffer;
	}

	public String getXmlReqRowFormatted() {
		return MarshalByteExt.bufferToStr(getXmlReqRowBytes());
	}

	/**Original name: XZA9O0-XML-REQ-ROW<br>
	 * <pre>*************************************************************
	 *  XZ0A90O0 - BPO COPYBOOK FOR                                *
	 *             UOW : XZ_GET_CNC_XML_REQ_LIST                   *
	 * *************************************************************
	 *  MAINTENANCE LOG                                            *
	 *                                                             *
	 *  SI#      DATE      PRGRMR     DESCRIPTION                  *
	 *  -------- --------- ---------- -----------------------------*
	 *  TO07614  31MAR2009 E404DAP    NEW                          *
	 *  TO07614  16APR2009 E404DAP    ADDED NOTIFICATION DATE      *
	 *  TO07614  04MAY2009 E404DAP    ADDED EMP ID AND TTY COPY IND*
	 *                                                             *
	 * *************************************************************</pre>*/
	public byte[] getXmlReqRowBytes() {
		byte[] buffer = new byte[Len.XML_REQ_ROW];
		return getXmlReqRowBytes(buffer, 1);
	}

	public void setXmlReqRowBytes(byte[] buffer, int offset) {
		int position = offset;
		maxXmlReqRows = MarshalByte.readBinaryShort(buffer, position);
		position += Types.SHORT_SIZE;
		xmlReqInfo.setXmlReqInfoBytes(buffer, position);
	}

	public byte[] getXmlReqRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, maxXmlReqRows);
		position += Types.SHORT_SIZE;
		xmlReqInfo.getXmlReqInfoBytes(buffer, position);
		return buffer;
	}

	public void setMaxXmlReqRows(short maxXmlReqRows) {
		this.maxXmlReqRows = maxXmlReqRows;
	}

	public short getMaxXmlReqRows() {
		return this.maxXmlReqRows;
	}

	public Xza9o0XmlReqInfo getXmlReqInfo() {
		return xmlReqInfo;
	}

	@Override
	public byte[] serialize() {
		return getWsXz0a90o0RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_XML_REQ_ROWS = 2;
		public static final int XML_REQ_ROW = MAX_XML_REQ_ROWS + Xza9o0XmlReqInfo.Len.XML_REQ_INFO;
		public static final int WS_XZ0A90O0_ROW = XML_REQ_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
