/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: PPC-NON-LOGGABLE-ERRORS<br>
 * Variables: PPC-NON-LOGGABLE-ERRORS from copybook TS020PRO<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class PpcNonLoggableErrors {

	//==== PROPERTIES ====
	//Original name: PPC-NON-LOG-ERR-MSG
	private String ppcNonLogErrMsg = DefaultValues.stringVal(Len.PPC_NON_LOG_ERR_MSG);

	//==== METHODS ====
	public void setPpcNonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		ppcNonLogErrMsg = MarshalByte.readString(buffer, position, Len.PPC_NON_LOG_ERR_MSG);
	}

	public byte[] getPpcNonLoggableErrorsBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, ppcNonLogErrMsg, Len.PPC_NON_LOG_ERR_MSG);
		return buffer;
	}

	public void initPpcNonLoggableErrorsSpaces() {
		ppcNonLogErrMsg = "";
	}

	public void setPpcNonLogErrMsg(String ppcNonLogErrMsg) {
		this.ppcNonLogErrMsg = Functions.subString(ppcNonLogErrMsg, Len.PPC_NON_LOG_ERR_MSG);
	}

	public String getPpcNonLogErrMsg() {
		return this.ppcNonLogErrMsg;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PPC_NON_LOG_ERR_MSG = 500;
		public static final int PPC_NON_LOGGABLE_ERRORS = PPC_NON_LOG_ERR_MSG;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
