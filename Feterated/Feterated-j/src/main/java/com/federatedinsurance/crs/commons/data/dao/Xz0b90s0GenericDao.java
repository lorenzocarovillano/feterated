/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IXz0b90s0Generic;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [XZ0B90S0_Generic]
 * 
 */
public class Xz0b90s0GenericDao extends BaseSqlDao<IXz0b90s0Generic> {

	public Xz0b90s0GenericDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IXz0b90s0Generic> getToClass() {
		return IXz0b90s0Generic.class;
	}

	public String setHostVarByWsCurrentDate(String wsCurrentDate, String dft) {
		return buildQuery("setHostVarByWsCurrentDate").bind("wsCurrentDate", wsCurrentDate).scalarResultString(dft);
	}
}
