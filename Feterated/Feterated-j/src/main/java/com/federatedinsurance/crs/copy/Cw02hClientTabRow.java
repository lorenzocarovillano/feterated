/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.util.Functions;

/**Original name: CW02H-CLIENT-TAB-ROW<br>
 * Variable: CW02H-CLIENT-TAB-ROW from copybook CAWLH002<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Cw02hClientTabRow {

	//==== PROPERTIES ====
	//Original name: CW02H-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW02H-HISTORY-VLD-NBR
	private short historyVldNbr = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-CICL-EFF-DT
	private String ciclEffDt = DefaultValues.stringVal(Len.CICL_EFF_DT);
	//Original name: CW02H-CICL-PRI-SUB-CD
	private String ciclPriSubCd = DefaultValues.stringVal(Len.CICL_PRI_SUB_CD);
	//Original name: CW02H-CICL-FST-NM
	private String ciclFstNm = DefaultValues.stringVal(Len.CICL_FST_NM);
	//Original name: CW02H-CICL-LST-NM
	private String ciclLstNm = DefaultValues.stringVal(Len.CICL_LST_NM);
	//Original name: CW02H-CICL-MDL-NM
	private String ciclMdlNm = DefaultValues.stringVal(Len.CICL_MDL_NM);
	//Original name: CW02H-NM-PFX
	private String nmPfx = DefaultValues.stringVal(Len.NM_PFX);
	//Original name: CW02H-NM-SFX
	private String nmSfx = DefaultValues.stringVal(Len.NM_SFX);
	//Original name: CW02H-PRIMARY-PRO-DSN-CD-NI
	private short primaryProDsnCdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-PRIMARY-PRO-DSN-CD
	private String primaryProDsnCd = DefaultValues.stringVal(Len.PRIMARY_PRO_DSN_CD);
	//Original name: CW02H-LEG-ENT-CD
	private String legEntCd = DefaultValues.stringVal(Len.LEG_ENT_CD);
	//Original name: CW02H-CICL-SDX-CD
	private String ciclSdxCd = DefaultValues.stringVal(Len.CICL_SDX_CD);
	//Original name: CW02H-CICL-OGN-INCEPT-DT-NI
	private short ciclOgnInceptDtNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-CICL-OGN-INCEPT-DT
	private String ciclOgnInceptDt = DefaultValues.stringVal(Len.CICL_OGN_INCEPT_DT);
	//Original name: CW02H-CICL-ADD-NM-IND-NI
	private short ciclAddNmIndNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-CICL-ADD-NM-IND
	private char ciclAddNmInd = DefaultValues.CHAR_VAL;
	//Original name: CW02H-CICL-DOB-DT
	private String ciclDobDt = DefaultValues.stringVal(Len.CICL_DOB_DT);
	//Original name: CW02H-CICL-BIR-ST-CD
	private String ciclBirStCd = DefaultValues.stringVal(Len.CICL_BIR_ST_CD);
	//Original name: CW02H-GENDER-CD
	private char genderCd = DefaultValues.CHAR_VAL;
	//Original name: CW02H-PRI-LGG-CD
	private String priLggCd = DefaultValues.stringVal(Len.PRI_LGG_CD);
	//Original name: CW02H-USER-ID
	private String userId = DefaultValues.stringVal(Len.USER_ID);
	//Original name: CW02H-STATUS-CD
	private char statusCd = DefaultValues.CHAR_VAL;
	//Original name: CW02H-TERMINAL-ID
	private String terminalId = DefaultValues.stringVal(Len.TERMINAL_ID);
	//Original name: CW02H-CICL-EXP-DT
	private String ciclExpDt = DefaultValues.stringVal(Len.CICL_EXP_DT);
	//Original name: CW02H-CICL-EFF-ACY-TS
	private String ciclEffAcyTs = DefaultValues.stringVal(Len.CICL_EFF_ACY_TS);
	//Original name: CW02H-CICL-EXP-ACY-TS
	private String ciclExpAcyTs = DefaultValues.stringVal(Len.CICL_EXP_ACY_TS);
	//Original name: CW02H-STATUTORY-TLE-CD
	private String statutoryTleCd = DefaultValues.stringVal(Len.STATUTORY_TLE_CD);
	//Original name: CW02H-CICL-LNG-NM-NI
	private short ciclLngNmNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-CICL-LNG-NM-LEN
	private short ciclLngNmLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: CW02H-CICL-LNG-NM-TEXT
	private String ciclLngNmText = DefaultValues.stringVal(Len.CICL_LNG_NM_TEXT);

	//==== METHODS ====
	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public String getClientIdFormatted() {
		return Functions.padBlanks(getClientId(), Len.CLIENT_ID);
	}

	public void setHistoryVldNbr(short historyVldNbr) {
		this.historyVldNbr = historyVldNbr;
	}

	public short getHistoryVldNbr() {
		return this.historyVldNbr;
	}

	public void setCiclEffDt(String ciclEffDt) {
		this.ciclEffDt = Functions.subString(ciclEffDt, Len.CICL_EFF_DT);
	}

	public String getCiclEffDt() {
		return this.ciclEffDt;
	}

	public void setCiclPriSubCd(String ciclPriSubCd) {
		this.ciclPriSubCd = Functions.subString(ciclPriSubCd, Len.CICL_PRI_SUB_CD);
	}

	public String getCiclPriSubCd() {
		return this.ciclPriSubCd;
	}

	public void setCiclFstNm(String ciclFstNm) {
		this.ciclFstNm = Functions.subString(ciclFstNm, Len.CICL_FST_NM);
	}

	public String getCiclFstNm() {
		return this.ciclFstNm;
	}

	public void setCiclLstNm(String ciclLstNm) {
		this.ciclLstNm = Functions.subString(ciclLstNm, Len.CICL_LST_NM);
	}

	public String getCiclLstNm() {
		return this.ciclLstNm;
	}

	public void setCiclMdlNm(String ciclMdlNm) {
		this.ciclMdlNm = Functions.subString(ciclMdlNm, Len.CICL_MDL_NM);
	}

	public String getCiclMdlNm() {
		return this.ciclMdlNm;
	}

	public void setNmPfx(String nmPfx) {
		this.nmPfx = Functions.subString(nmPfx, Len.NM_PFX);
	}

	public String getNmPfx() {
		return this.nmPfx;
	}

	public void setNmSfx(String nmSfx) {
		this.nmSfx = Functions.subString(nmSfx, Len.NM_SFX);
	}

	public String getNmSfx() {
		return this.nmSfx;
	}

	public void setPrimaryProDsnCdNi(short primaryProDsnCdNi) {
		this.primaryProDsnCdNi = primaryProDsnCdNi;
	}

	public short getPrimaryProDsnCdNi() {
		return this.primaryProDsnCdNi;
	}

	public void setPrimaryProDsnCd(String primaryProDsnCd) {
		this.primaryProDsnCd = Functions.subString(primaryProDsnCd, Len.PRIMARY_PRO_DSN_CD);
	}

	public String getPrimaryProDsnCd() {
		return this.primaryProDsnCd;
	}

	public void setLegEntCd(String legEntCd) {
		this.legEntCd = Functions.subString(legEntCd, Len.LEG_ENT_CD);
	}

	public String getLegEntCd() {
		return this.legEntCd;
	}

	public void setCiclSdxCd(String ciclSdxCd) {
		this.ciclSdxCd = Functions.subString(ciclSdxCd, Len.CICL_SDX_CD);
	}

	public String getCiclSdxCd() {
		return this.ciclSdxCd;
	}

	public void setCiclOgnInceptDtNi(short ciclOgnInceptDtNi) {
		this.ciclOgnInceptDtNi = ciclOgnInceptDtNi;
	}

	public short getCiclOgnInceptDtNi() {
		return this.ciclOgnInceptDtNi;
	}

	public void setCiclOgnInceptDt(String ciclOgnInceptDt) {
		this.ciclOgnInceptDt = Functions.subString(ciclOgnInceptDt, Len.CICL_OGN_INCEPT_DT);
	}

	public String getCiclOgnInceptDt() {
		return this.ciclOgnInceptDt;
	}

	public void setCiclAddNmIndNi(short ciclAddNmIndNi) {
		this.ciclAddNmIndNi = ciclAddNmIndNi;
	}

	public short getCiclAddNmIndNi() {
		return this.ciclAddNmIndNi;
	}

	public void setCiclAddNmInd(char ciclAddNmInd) {
		this.ciclAddNmInd = ciclAddNmInd;
	}

	public char getCiclAddNmInd() {
		return this.ciclAddNmInd;
	}

	public void setCiclDobDt(String ciclDobDt) {
		this.ciclDobDt = Functions.subString(ciclDobDt, Len.CICL_DOB_DT);
	}

	public String getCiclDobDt() {
		return this.ciclDobDt;
	}

	public void setCiclBirStCd(String ciclBirStCd) {
		this.ciclBirStCd = Functions.subString(ciclBirStCd, Len.CICL_BIR_ST_CD);
	}

	public String getCiclBirStCd() {
		return this.ciclBirStCd;
	}

	public void setGenderCd(char genderCd) {
		this.genderCd = genderCd;
	}

	public char getGenderCd() {
		return this.genderCd;
	}

	public void setPriLggCd(String priLggCd) {
		this.priLggCd = Functions.subString(priLggCd, Len.PRI_LGG_CD);
	}

	public String getPriLggCd() {
		return this.priLggCd;
	}

	public void setUserId(String userId) {
		this.userId = Functions.subString(userId, Len.USER_ID);
	}

	public String getUserId() {
		return this.userId;
	}

	public void setStatusCd(char statusCd) {
		this.statusCd = statusCd;
	}

	public char getStatusCd() {
		return this.statusCd;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = Functions.subString(terminalId, Len.TERMINAL_ID);
	}

	public String getTerminalId() {
		return this.terminalId;
	}

	public void setCiclExpDt(String ciclExpDt) {
		this.ciclExpDt = Functions.subString(ciclExpDt, Len.CICL_EXP_DT);
	}

	public String getCiclExpDt() {
		return this.ciclExpDt;
	}

	public void setCiclEffAcyTs(String ciclEffAcyTs) {
		this.ciclEffAcyTs = Functions.subString(ciclEffAcyTs, Len.CICL_EFF_ACY_TS);
	}

	public String getCiclEffAcyTs() {
		return this.ciclEffAcyTs;
	}

	public void setCiclExpAcyTs(String ciclExpAcyTs) {
		this.ciclExpAcyTs = Functions.subString(ciclExpAcyTs, Len.CICL_EXP_ACY_TS);
	}

	public String getCiclExpAcyTs() {
		return this.ciclExpAcyTs;
	}

	public void setStatutoryTleCd(String statutoryTleCd) {
		this.statutoryTleCd = Functions.subString(statutoryTleCd, Len.STATUTORY_TLE_CD);
	}

	public String getStatutoryTleCd() {
		return this.statutoryTleCd;
	}

	public short getCiclLngNmNi() {
		return this.ciclLngNmNi;
	}

	public void setCiclLngNmLen(short ciclLngNmLen) {
		this.ciclLngNmLen = ciclLngNmLen;
	}

	public short getCiclLngNmLen() {
		return this.ciclLngNmLen;
	}

	public void setCiclLngNmText(String ciclLngNmText) {
		this.ciclLngNmText = Functions.subString(ciclLngNmText, Len.CICL_LNG_NM_TEXT);
	}

	public String getCiclLngNmText() {
		return this.ciclLngNmText;
	}

	public String getCiclLngNmTextFormatted() {
		return Functions.padBlanks(getCiclLngNmText(), Len.CICL_LNG_NM_TEXT);
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int CICL_EFF_DT = 10;
		public static final int CICL_PRI_SUB_CD = 4;
		public static final int CICL_FST_NM = 15;
		public static final int CICL_LST_NM = 30;
		public static final int CICL_MDL_NM = 15;
		public static final int NM_PFX = 4;
		public static final int NM_SFX = 4;
		public static final int PRIMARY_PRO_DSN_CD = 4;
		public static final int LEG_ENT_CD = 2;
		public static final int CICL_SDX_CD = 8;
		public static final int CICL_OGN_INCEPT_DT = 10;
		public static final int CICL_DOB_DT = 10;
		public static final int CICL_BIR_ST_CD = 3;
		public static final int PRI_LGG_CD = 3;
		public static final int USER_ID = 8;
		public static final int TERMINAL_ID = 8;
		public static final int CICL_EXP_DT = 10;
		public static final int CICL_EFF_ACY_TS = 26;
		public static final int CICL_EXP_ACY_TS = 26;
		public static final int STATUTORY_TLE_CD = 3;
		public static final int CICL_LNG_NM_TEXT = 132;
		public static final int LO_LST_NM_MCH_CD = 35;
		public static final int HI_LST_NM_MCH_CD = 35;
		public static final int LO_FST_NM_MCH_CD = 35;
		public static final int HI_FST_NM_MCH_CD = 35;
		public static final int CICL_ACQ_SRC_CD = 3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
