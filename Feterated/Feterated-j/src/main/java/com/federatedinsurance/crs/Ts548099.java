/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs;

import javax.inject.Inject;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.federatedinsurance.crs.ws.ConstantFields;
import com.federatedinsurance.crs.ws.LCicsPipeMaintenanceCnt;
import com.federatedinsurance.crs.ws.SaveArea;
import com.federatedinsurance.crs.ws.Ts548099Data;
import com.federatedinsurance.crs.ws.enums.LCmFunction;
import com.federatedinsurance.crs.ws.occurs.TbRaInf;
import com.federatedinsurance.crs.ws.ptr.LNullPtr;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;

/**Original name: TS548099<br>
 * <pre>AUTHOR.        DARREN M. ALBERS
 * DATE-WRITTEN.  21 NOV 2008.
 * ****************************************************************
 * *                                                             **
 * * PROGRAM TITLE - COBOL CICS PIPE MAINTENANCE PROGRAM         **
 * *                                                             **
 * * PURPOSE -  THIS PROGRAM WILL HANDLE SOME OF THE             **
 * *            FUNCTIONALITY OF THE CICS PIPE COMMANDS.  THE    **
 * *            COMMANDS SUPPORTED BY THIS ROUTINE INCLUDE:      **
 * *            * OPEN CICS PIPE                                 **
 * *            * CLOSE CICS PIPE                                **
 * *                                                             **
 * * INPUT  -LINKAGE SECTION                                     **
 * *          : FUNCTION                                         **
 * *          : CICS REGION (APPL ID)                            **
 * *          : PIPE IDENTIFICATION TOKENS                       **
 * *                                                             **
 * * OUTPUT -LINKAGE SECTION                                     **
 * *          : CICS REGION CONNECTED (APPL ID)                  **
 * *          : PIPE IDENTIFICATION TOKENS                       **
 * *          : ERROR INFORMATION                                **
 * *                                                             **
 * * ASSUMPTIONS/NOTES                                           **
 * *        - ERROR HANDLING HAS BEEN SIMPLIFIED FOR THE USERS   **
 * *           AS BEST AS POSSIBLE, BUT NOT EVERY ERROR CAN BE   **
 * *           PREDICTED BY THIS PROGRAM. THUS, ERROR MESSAGES   **
 * *           WILL VARY BASED ON TYPES OF ERRORS ENCOUNTERED.   **
 * *        - FOR CICS REGIONS ABLE TO TRANSITION, THE TRANSITION**
 * *           ORDER WILL GO FROM DAY REGION TO NIGHT REGION AND **
 * *           TO TRANSITION REGION IF NEEDED.                   **
 * *        - DUE TO THE PREVIOUS ASSUMPTION, THE MOST REGIONS   **
 * *           ABLE TO BE TESTED AND FAILED IS THREE.            **
 * *                                                             **
 * ****************************************************************
 * *               M A I N T E N A N C E    L O G                **
 * *                                                             **
 * * SI #         DATE      PROG             DESCRIPTION         **
 * * -------  ----------  --------   ----------------------------**
 * * TL000059  11/21/2008 E404DMA    NEW                         **
 * * TL000217  07/22/2010 E404DMA    ADD CUSTOM ERROR MESSAGE FOR**
 * *                                 WHEN USER FORGETS JCL LIB.  **
 * ****************************************************************</pre>*/
public class Ts548099 extends Program {

	//==== PROPERTIES ====
	@Inject
	private IPointerManager pointerManager;
	//Original name: WORKING-STORAGE
	private Ts548099Data ws = new Ts548099Data();
	//Original name: L-CICS-PIPE-MAINTENANCE-CNT
	private LCicsPipeMaintenanceCnt lCicsPipeMaintenanceCnt;
	/**Original name: L-NULL-PTR<br>
	 * <pre>THIS IS NEEDED FOR THE CICS CALLS</pre>*/
	private LNullPtr lNullPtr = new LNullPtr(null);

	//==== METHODS ====
	/**Original name: MAIN_SUBROUTINE<br>*/
	public long execute(LCicsPipeMaintenanceCnt lCicsPipeMaintenanceCnt) {
		this.lCicsPipeMaintenanceCnt = lCicsPipeMaintenanceCnt;
		processPipeRequest();
		programExit();
		return 0;
	}

	public static Ts548099 getInstance() {
		return (Programs.getInstance(Ts548099.class));
	}

	/**Original name: 1000-PROCESS-PIPE-REQUEST<br>*/
	private void processPipeRequest() {
		// COB_CODE: PERFORM 2000-BEGINNING-HOUSEKEEPING
		//              THRU 2000-EXIT.
		beginningHousekeeping();
		// COB_CODE: EVALUATE TRUE
		//               WHEN L-CM-FN-OPEN-PIPE
		//                      THRU 3000-EXIT
		//               WHEN L-CM-FN-CLOSE-PIPE
		//                      THRU 4000-EXIT
		//               WHEN OTHER
		//                   GO TO 1000-PROGRAM-EXIT
		//           END-EVALUATE.
		switch (lCicsPipeMaintenanceCnt.getFunction().getFunction()) {

		case LCmFunction.OPEN_PIPE:// COB_CODE: PERFORM 3000-OPEN-CICS-PIPE
			//              THRU 3000-EXIT
			rng3000OpenCicsPipe();
			break;

		case LCmFunction.CLOSE_PIPE:// COB_CODE: PERFORM 4000-CLOSE-CICS-PIPE
			//              THRU 4000-EXIT
			closeCicsPipe();
			break;

		default:// COB_CODE: SET L-CM-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND  TO TRUE
			lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setLogicError();
			ws.getSwErrorFoundFlag().setErrorFound();
			// COB_CODE: MOVE L-CM-FUNCTION  TO EA-02-FUNCTION-VAL
			ws.getErrorAndAdviceMessages().getEa02FunctionInvalid().setEa02FunctionVal(lCicsPipeMaintenanceCnt.getFunction().getFunction());
			// COB_CODE: MOVE EA-02-FUNCTION-INVALID
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa02FunctionInvalid().getEa02FunctionInvalidFormatted());
			// COB_CODE: GO TO 1000-PROGRAM-EXIT
			programExit();
			break;
		}
		//    SINCE VALUES CAN BE FOUND HERE EVEN WHEN NO ERRORS ARE FOUND,
		//      ALWAYS GIVE BACK THE CODES WE RECEIVED.
		// COB_CODE: MOVE EXCI-RESPONSE          TO L-CM-EI-EXCI-RESP.
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp(((int) (ws.getDfhxcplo().getExciReturnCode().getResponse())));
		// COB_CODE: MOVE EXCI-REASON            TO L-CM-EI-EXCI-RESP2.
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp2(((int) (ws.getDfhxcplo().getExciReturnCode().getReason())));
		// COB_CODE: MOVE EXCI-SUB-REASON1       TO L-CM-EI-EXCI-RESP3.
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp3(((int) (ws.getDfhxcplo().getExciReturnCode().getSubReason1())));
		// COB_CODE: MOVE EXCI-DPL-RESP          TO L-CM-EI-DPL-RESP.
		lCicsPipeMaintenanceCnt.getErrorInformation().setDplResp(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp())));
		// COB_CODE: MOVE EXCI-DPL-RESP2         TO L-CM-EI-DPL-RESP2.
		lCicsPipeMaintenanceCnt.getErrorInformation().setDplResp2(((int) (ws.getDfhxcplo().getExciDplRetarea().getResp2())));
	}

	/**Original name: 1000-PROGRAM-EXIT<br>*/
	private void programExit() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	/**Original name: 2000-BEGINNING-HOUSEKEEPING<br>
	 * <pre>******************************************************
	 *   INITIALIZE THE ERROR INFORMATION AND CAPTURE THE CICS APPL ID
	 * ******************************************************</pre>*/
	private void beginningHousekeeping() {
		// COB_CODE: SET SW-NO-ERROR-FOUND       TO TRUE.
		ws.getSwErrorFoundFlag().setNoErrorFound();
		// COB_CODE: INITIALIZE L-CM-ERROR-INFORMATION.
		initErrorInformation();
		// COB_CODE: MOVE L-CM-CICS-APPL-ID      TO SA-CICS-APPL-ID.
		ws.getSaveArea().getCicsApplId().setCicsApplId(lCicsPipeMaintenanceCnt.getCicsApplId().getCicsApplId());
	}

	/**Original name: 3000-OPEN-CICS-PIPE<br>
	 * <pre>******************************************************
	 *   THIS ROUTINE WILL PERFORM A CALL LEVEL EXCI REQUEST BY SETTING
	 *   UP A PIPE TO THE TARGET CICS SYSTEM WITH INITIALIZE USER,
	 *   ALLOCATE, AND OPEN PIPE CALLS.
	 * ******************************************************</pre>*/
	private String openCicsPipe() {
		// COB_CODE: INITIALIZE L-CM-PIPE-ID-TOKENS.
		initPipeIdTokens();
		// COB_CODE: SET ADDRESS OF L-NULL-PTR   TO NULLS.
		lNullPtr = ((pointerManager.resolve(pointerManager.getNullPointer(), LNullPtr.class)));
		// COB_CODE: PERFORM 3100-EDIT-INPUT
		//              THRU 3100-EXIT.
		editInput();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 3000-EXIT
			return "3000-EXIT";
		}
		// COB_CODE: PERFORM 3200-INITIALIZE-CICS-USER
		//              THRU 3200-EXIT.
		initializeCicsUser();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 3000-EXIT
			return "3000-EXIT";
		}
		//    CLEAR OUT THE LIST OF REGIONS ATTEMPTED AND START NEW
		// COB_CODE: INITIALIZE TABLE-OF-REGIONS-ATTEMPTED.
		initTableOfRegionsAttempted();
		// COB_CODE: MOVE +0                     TO SS-RA.
		ws.getSubscripts().setRa(((short) 0));
		return "";
	}

	/**Original name: 3000-A<br>
	 * <pre>    CAPTURE THE CURRENT REGION ATTEMPTING TO CONNECT TO AND
	 *       ASSUME WE WILL NOT NEED TO RETRY THE CONNECTION</pre>*/
	private String a() {
		// COB_CODE: ADD +1                      TO SS-RA.
		ws.getSubscripts().setRa(Trunc.toShort(1 + ws.getSubscripts().getRa(), 4));
		// COB_CODE: MOVE SA-CICS-APPL-ID        TO TB-RA-REGION (SS-RA).
		ws.getTbRaInf(ws.getSubscripts().getRa()).setTbRaRegion(ws.getSaveArea().getCicsApplId().getCicsApplId());
		// COB_CODE: SET SW-DO-NOT-RETRY-CONNECTION
		//                                       TO TRUE.
		ws.getSwRetryConnectionFlag().setDoNotRetryConnection();
		// COB_CODE: PERFORM 3300-ALLOCATE-CICS-PIPE
		//              THRU 3300-EXIT.
		allocateCicsPipe();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		// COB_CODE: PERFORM 3400-OPEN-CICS-PIPE
		//              THRU 3400-EXIT.
		openCicsPipe1();
		// COB_CODE: IF SW-ERROR-FOUND
		//               GO TO 3000-EXIT
		//           END-IF.
		if (ws.getSwErrorFoundFlag().isErrorFound()) {
			// COB_CODE: GO TO 3000-EXIT
			return "";
		}
		//    IF NEEDED, LOOP BACK AND RETRY THE PIPE CONNECTION
		// COB_CODE: IF SW-RETRY-CONNECTION
		//               GO TO 3000-A
		//           END-IF.
		if (ws.getSwRetryConnectionFlag().isRetryConnection()) {
			// COB_CODE: GO TO 3000-A
			return "3000-A";
		}
		//    PIPE ESTABLISHED.  CAPTURE PIPE INFORMATION FOR CALLER
		// COB_CODE: MOVE SA-CICS-APPL-ID        TO L-CM-CICS-APPL-ID.
		lCicsPipeMaintenanceCnt.getCicsApplId().setCicsApplId(ws.getSaveArea().getCicsApplId().getCicsApplId());
		// COB_CODE: MOVE SA-USER-TOKEN          TO L-CM-PI-USER-TOKEN.
		lCicsPipeMaintenanceCnt.setPiUserToken(ws.getSaveArea().getUserToken());
		// COB_CODE: MOVE SA-PIPE-TOKEN          TO L-CM-PI-PIPE-TOKEN.
		lCicsPipeMaintenanceCnt.setPiPipeToken(ws.getSaveArea().getPipeToken());
		return "";
	}

	/**Original name: 3100-EDIT-INPUT<br>
	 * <pre>******************************************************
	 *   CHECK USER INPUT USED FOR PIPE ESTABLISHMENT.
	 * ******************************************************</pre>*/
	private void editInput() {
		// COB_CODE: IF SA-CICS-APPL-ID-IS-BLANK
		//               GO TO 3100-EXIT
		//           END-IF.
		if (ws.getSaveArea().getCicsApplId().isApplIdIsBlank()) {
			// COB_CODE: SET L-CM-EI-ES-LOGIC-ERROR
			//               SW-ERROR-FOUND
			//               EA-01-CICS-REGION-PARM
			//                                   TO TRUE
			lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setLogicError();
			ws.getSwErrorFoundFlag().setErrorFound();
			ws.getErrorAndAdviceMessages().getEa01ParmNotFound().setEa01CicsRegionParm();
			// COB_CODE: MOVE EA-01-PARM-NOT-FOUND
			//                                   TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa01ParmNotFound().getEa01ParmNotFoundFormatted());
			// COB_CODE: GO TO 3100-EXIT
			return;
		}
	}

	/**Original name: 3200-INITIALIZE-CICS-USER<br>
	 * <pre>******************************************************
	 *   PERFORM THE INITIALIZE USER CALL.
	 * ******************************************************</pre>*/
	private void initializeCicsUser() {
		GenericParam version1 = null;
		GenericParam saUserToken = null;
		GenericParam initUser = null;
		// COB_CODE: CALL 'DFHXCIS'
		//               USING
		//                   VERSION-1
		//                   EXCI-RETURN-CODE
		//                   SA-USER-TOKEN
		//                   INIT-USER
		//                   CF-CICS-APP.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		saUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getUserToken()));
		initUser = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getInitUser()));
		DynamicCall.invoke("DFHXCIS", version1, ws.getDfhxcplo().getExciReturnCode(), saUserToken, initUser, ws.getConstantFields());
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getSaveArea().setUserTokenFromBuffer(saUserToken.getByteData());
		ws.getDfhxcplo().setInitUserFromBuffer(initUser.getByteData());
		// COB_CODE: IF EXCI-RESPONSE NOT = EXCI-NORMAL
		//               END-IF
		//           END-IF.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() != ws.getDfhxcrco().getExciNormal()) {
			// COB_CODE: PERFORM 9000-CICS-ERROR
			//              THRU 9000-EXIT
			cicsError();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 3200-EXIT
			//           END-IF
			if (ws.getSwErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 3200-EXIT
				return;
			}
		}
	}

	/**Original name: 3300-ALLOCATE-CICS-PIPE<br>
	 * <pre>******************************************************
	 *   PERFORM THE ALLOCATE PIPE CALL.
	 * ******************************************************</pre>*/
	private void allocateCicsPipe() {
		GenericParam version1 = null;
		GenericParam saUserToken = null;
		GenericParam allocatePipe = null;
		GenericParam specificPipe = null;
		// COB_CODE: CALL 'DFHXCIS'
		//               USING
		//                   VERSION-1
		//                   EXCI-RETURN-CODE
		//                   SA-USER-TOKEN
		//                   ALLOCATE-PIPE
		//                   SA-PIPE-TOKEN
		//                   SA-CICS-APPL-ID
		//                   SPECIFIC-PIPE.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		saUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getUserToken()));
		allocatePipe = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getAllocatePipe()));
		specificPipe = new GenericParam(MarshalByteExt.chToBuffer(ws.getDfhxcplo().getSpecificPipe()));
		DynamicCall.invoke("DFHXCIS", new Object[] { version1, ws.getDfhxcplo().getExciReturnCode(), saUserToken, allocatePipe, ws.getSaveArea(),
				ws.getSaveArea().getCicsApplId(), specificPipe });
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getSaveArea().setUserTokenFromBuffer(saUserToken.getByteData());
		ws.getDfhxcplo().setAllocatePipeFromBuffer(allocatePipe.getByteData());
		ws.getDfhxcplo().setSpecificPipeFromBuffer(specificPipe.getByteData());
		// COB_CODE: IF EXCI-RESPONSE NOT = EXCI-NORMAL
		//               END-IF
		//           END-IF.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() != ws.getDfhxcrco().getExciNormal()) {
			// COB_CODE: PERFORM 9000-CICS-ERROR
			//              THRU 9000-EXIT
			cicsError();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 3300-EXIT
			//           END-IF
			if (ws.getSwErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 3300-EXIT
				return;
			}
		}
	}

	/**Original name: 3400-OPEN-CICS-PIPE<br>
	 * <pre>******************************************************
	 *   PERFORM THE OPEN PIPE CALL.
	 * ******************************************************</pre>*/
	private void openCicsPipe1() {
		GenericParam version1 = null;
		GenericParam saUserToken = null;
		GenericParam openPipe = null;
		// COB_CODE: CALL 'DFHXCIS'
		//               USING
		//                   VERSION-1
		//                   EXCI-RETURN-CODE
		//                   SA-USER-TOKEN
		//                   OPEN-PIPE
		//                   SA-PIPE-TOKEN.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		saUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getUserToken()));
		openPipe = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getOpenPipe()));
		DynamicCall.invoke("DFHXCIS", version1, ws.getDfhxcplo().getExciReturnCode(), saUserToken, openPipe, ws.getSaveArea());
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getSaveArea().setUserTokenFromBuffer(saUserToken.getByteData());
		ws.getDfhxcplo().setOpenPipeFromBuffer(openPipe.getByteData());
		//    SPECIAL ERROR HANDLING LOGIC IS REQUIRED WHEN REGION CAN BE
		//      REDIRECTED.
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-RESPONSE = EXCI-NORMAL
		//                   CONTINUE
		//               WHEN EXCI-RESPONSE = EXCI-RETRYABLE
		//                   AND
		//                    SA-CICS-REGION-CAN-TRANSITION
		//                   GO TO 3400-EXIT
		//               WHEN OTHER
		//                   END-IF
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciNormal()) {
			// COB_CODE: CONTINUE
			//continue
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciRetryable()
				&& ws.getSaveArea().getCicsApplId().isRegionCanTransition()) {
			// COB_CODE: PERFORM 3410-ATTEMPT-REGION-TRANSFER
			//              THRU 3410-EXIT
			attemptRegionTransfer();
			// COB_CODE: GO TO 3400-EXIT
			return;
		} else {
			// COB_CODE: PERFORM 9000-CICS-ERROR
			//              THRU 9000-EXIT
			cicsError();
			// COB_CODE: IF SW-ERROR-FOUND
			//               GO TO 3400-EXIT
			//           END-IF
			if (ws.getSwErrorFoundFlag().isErrorFound()) {
				// COB_CODE: GO TO 3400-EXIT
				return;
			}
		}
	}

	/**Original name: 3410-ATTEMPT-REGION-TRANSFER<br>
	 * <pre>******************************************************
	 *   PERFORM SPECIAL ERROR HANDLING LOGIC FOR PIPE CONNECTION
	 * ******************************************************
	 *     DETERMINE WHICH APPROPRIATE PRODUCTION REGION WE NEED TO TRY
	 *       AND CONNECT TO.  THE DETERMINED ORDER OF PROD REGIONS TO
	 *       CONNECT TO ARE DAY REGIONS FIRST (SPECIFIED BY USER),
	 *       NIGHT REGION (RETRY ATTEMPT OR SPECIFIED BY USER), AND
	 *       TRANSITION REGION LAST (RETRY ATTEMPT OR SPECIFIED BY USER)</pre>*/
	private void attemptRegionTransfer() {
		// COB_CODE: IF SA-CICS-REGION-IS-NIGHT
		//                                       TO TRUE
		//           ELSE
		//                                       TO TRUE
		//           END-IF.
		if (ws.getSaveArea().getCicsApplId().isRegionIsNight()) {
			// COB_CODE: SET SA-CICS-REGION-IS-TRANSITION
			//                                   TO TRUE
			ws.getSaveArea().getCicsApplId().setRegionIsTransition();
		} else {
			// COB_CODE: SET SA-CICS-REGION-IS-NIGHT
			//                                   TO TRUE
			ws.getSaveArea().getCicsApplId().setRegionIsNight();
		}
		// COB_CODE: SET L-CM-EI-ES-WARNING
		//               L-CM-EI-ET-REGION-SWAPPED
		//                                       TO TRUE.
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setWarning();
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorType().setRegionSwapped();
		// COB_CODE: MOVE L-CM-CICS-APPL-ID      TO EA-03-ORIG-REGION.
		ws.getErrorAndAdviceMessages().getEa03RegionSwapped().setOrigRegion(lCicsPipeMaintenanceCnt.getCicsApplId().getCicsApplId());
		// COB_CODE: MOVE SA-CICS-APPL-ID        TO EA-03-REDIR-REGION.
		ws.getErrorAndAdviceMessages().getEa03RegionSwapped().setRedirRegion(ws.getSaveArea().getCicsApplId().getCicsApplId());
		// COB_CODE: MOVE EA-03-REGION-SWAPPED   TO L-CM-EI-MESSAGE.
		lCicsPipeMaintenanceCnt.getErrorInformation()
				.setMessage(ws.getErrorAndAdviceMessages().getEa03RegionSwapped().getEa03RegionSwappedFormatted());
		// COB_CODE: SET SW-RETRY-CONNECTION     TO TRUE.
		ws.getSwRetryConnectionFlag().setRetryConnection();
	}

	/**Original name: 4000-CLOSE-CICS-PIPE<br>
	 * <pre>******************************************************
	 *     THIS ROUTINE WILL PERFORM A CALL LEVEL EXCI TO SET THE
	 *     TARGET FILE CLOSED, AND CONTINUE BY CLOSING AND DEALLOCATING
	 *     THE PIPE TO THE TARGET CICS SYSTEM TO SHUT DOWN THE LINK
	 *     BEFORE EXITING TO MVS.
	 * ******************************************************</pre>*/
	private void closeCicsPipe() {
		GenericParam version1 = null;
		GenericParam saUserToken = null;
		GenericParam closePipe = null;
		GenericParam deallocatePipe = null;
		// COB_CODE: MOVE L-CM-PI-USER-TOKEN     TO SA-USER-TOKEN.
		ws.getSaveArea().setUserToken(lCicsPipeMaintenanceCnt.getPiUserToken());
		// COB_CODE: MOVE L-CM-PI-PIPE-TOKEN     TO SA-PIPE-TOKEN.
		ws.getSaveArea().setPipeToken(lCicsPipeMaintenanceCnt.getPiPipeToken());
		//    PERFORM CLOSE PIPE CALL.
		// COB_CODE: CALL 'DFHXCIS'
		//               USING
		//                   VERSION-1
		//                   EXCI-RETURN-CODE
		//                   SA-USER-TOKEN
		//                   CLOSE-PIPE
		//                   SA-PIPE-TOKEN.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		saUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getUserToken()));
		closePipe = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getClosePipe()));
		DynamicCall.invoke("DFHXCIS", version1, ws.getDfhxcplo().getExciReturnCode(), saUserToken, closePipe, ws.getSaveArea());
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getSaveArea().setUserTokenFromBuffer(saUserToken.getByteData());
		ws.getDfhxcplo().setClosePipeFromBuffer(closePipe.getByteData());
		// COB_CODE:      IF EXCI-RESPONSE NOT = EXCI-NORMAL
		//                       THRU 9000-EXIT
		//           *        CONTINUE PROCESSING AND TRY TO DEALLOCATE THE PIPE
		//                END-IF.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() != ws.getDfhxcrco().getExciNormal()) {
			// COB_CODE: PERFORM 9000-CICS-ERROR
			//              THRU 9000-EXIT
			cicsError();
			//        CONTINUE PROCESSING AND TRY TO DEALLOCATE THE PIPE
		}
		//    PERFORM DEALLOCATE PIPE CALL.
		// COB_CODE: CALL 'DFHXCIS'
		//               USING
		//                   VERSION-1
		//                   EXCI-RETURN-CODE
		//                   SA-USER-TOKEN
		//                   DEALLOCATE-PIPE
		//                   SA-PIPE-TOKEN.
		version1 = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getVersion1()));
		saUserToken = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getSaveArea().getUserToken()));
		deallocatePipe = new GenericParam(MarshalByteExt.binUnsignedIntToBuffer(ws.getDfhxcplo().getDeallocatePipe()));
		DynamicCall.invoke("DFHXCIS", version1, ws.getDfhxcplo().getExciReturnCode(), saUserToken, deallocatePipe, ws.getSaveArea());
		ws.getDfhxcplo().setVersion1FromBuffer(version1.getByteData());
		ws.getSaveArea().setUserTokenFromBuffer(saUserToken.getByteData());
		ws.getDfhxcplo().setDeallocatePipeFromBuffer(deallocatePipe.getByteData());
		// COB_CODE:      IF EXCI-RESPONSE NOT = EXCI-NORMAL
		//           *        IF AN ERROR OCCURRED DURING THE CLOSING OF THE PIPE,
		//           *          DO NOT OVERWRITE THE PREVIOUS ERROR INFO FOR THIS ERROR
		//                    GO TO 4000-EXIT
		//                END-IF.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() != ws.getDfhxcrco().getExciNormal()) {
			//        IF AN ERROR OCCURRED DURING THE CLOSING OF THE PIPE,
			//          DO NOT OVERWRITE THE PREVIOUS ERROR INFO FOR THIS ERROR
			// COB_CODE: IF SW-NO-ERROR-FOUND
			//                  THRU 9000-EXIT
			//           END-IF
			if (ws.getSwErrorFoundFlag().isNoErrorFound()) {
				// COB_CODE: PERFORM 9000-CICS-ERROR
				//              THRU 9000-EXIT
				cicsError();
			}
			// COB_CODE: GO TO 4000-EXIT
			return;
		}
	}

	/**Original name: 9000-CICS-ERROR<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING
	 * ******************************************************</pre>*/
	private void cicsError() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-RESPONSE = EXCI-WARNING
		//                      THRU 9010-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-RETRYABLE
		//                      THRU 9020-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-USER-ERROR
		//                      THRU 9030-EXIT
		//               WHEN EXCI-RESPONSE = EXCI-SYSTEM-ERROR
		//                      THRU 9040-EXIT
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciWarning()) {
			// COB_CODE: PERFORM 9010-WARNING-MSG
			//              THRU 9010-EXIT
			warningMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciRetryable()) {
			// COB_CODE: PERFORM 9020-RETRYABLE-MSG
			//              THRU 9020-EXIT
			retryableMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciUserError()) {
			// COB_CODE: PERFORM 9030-USER-MSG
			//              THRU 9030-EXIT
			userMsg();
		} else if (ws.getDfhxcplo().getExciReturnCode().getResponse() == ws.getDfhxcrco().getExciSystemError()) {
			// COB_CODE: PERFORM 9040-SYSTEM-MSG
			//              THRU 9040-EXIT
			systemMsg();
		}
	}

	/**Original name: 9010-WARNING-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - WARNING MESSAGE GENERATION
	 * ******************************************************</pre>*/
	private void warningMsg() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = PIPE-ALREADY-OPEN
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = PIPE-ALREADY-CLOSED
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CM-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getPipeAlreadyOpen()) {
			// COB_CODE: MOVE EA-21-WNG-PIPE-OPEN-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa21WngPipeOpenMsg().getEa21WngPipeOpenMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getPipeAlreadyClosed()) {
			// COB_CODE: MOVE EA-22-WNG-PIPE-CLOSED-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa22WngPipeClosedMsg().getEa22WngPipeClosedMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-20-GEN-WARNING-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa20GenWarningMsg().getEa20GenWarningMsgFormatted());
		}
		// COB_CODE: SET L-CM-EI-ES-WARNING
		//               L-CM-EI-ET-SYSTEM-WARNING
		//                                       TO TRUE.
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setWarning();
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorType().setSystemWarning();
	}

	/**Original name: 9020-RETRYABLE-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - RETRYABLE MESSAGE GENERATION
	 * ******************************************************</pre>*/
	private void retryableMsg() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = NO-PIPE
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = NO-CICS
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CM-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getNoPipe()) {
			// COB_CODE: PERFORM 9200-GET-LIST-OF-TRIED-REGIONS
			//              THRU 9200-EXIT
			rng9200GetListOfTriedRegions();
			// COB_CODE: MOVE SA-LIST-OF-REGIONS-ATTEMPTED
			//                               TO EA-41-REGIONS-ATTEMPTED
			ws.getErrorAndAdviceMessages().getEa41RtyNoFreeSessionsMsg().setEa41RegionsAttempted(ws.getSaveArea().getListOfRegionsAttempted());
			// COB_CODE: MOVE EA-41-RTY-NO-FREE-SESSIONS-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa41RtyNoFreeSessionsMsg().getEa41RtyNoFreeSessionsMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getNoCics()) {
			// COB_CODE: PERFORM 9200-GET-LIST-OF-TRIED-REGIONS
			//              THRU 9200-EXIT
			rng9200GetListOfTriedRegions();
			// COB_CODE: MOVE SA-LIST-OF-REGIONS-ATTEMPTED
			//                               TO EA-42-REGIONS-ATTEMPTED
			ws.getErrorAndAdviceMessages().getEa42RtyNoConnectionMsg().setEa42RegionsAttempted(ws.getSaveArea().getListOfRegionsAttempted());
			// COB_CODE: MOVE EA-42-RTY-NO-CONNECTION-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa42RtyNoConnectionMsg().getEa42RtyNoConnectionMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-40-GEN-RETRYABLE-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa40GenRetryableMsg().getEa40GenRetryableMsgFormatted());
		}
		// COB_CODE: SET L-CM-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setSystemError();
		ws.getSwErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9030-USER-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - USER ERROR MESSAGE GENERATION
	 * ******************************************************</pre>*/
	private void userMsg() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = INVALID-APPL-NAME
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = INVALID-USER-TOKEN
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = PIPE-NOT-CLOSED
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = PIPE-NOT-OPEN
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = INVALID-PIPE-TOKEN
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = DFHXCOPT-LOAD-FAILED
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CM-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidApplName()) {
			// COB_CODE: MOVE SA-CICS-APPL-ID
			//                               TO EA-61-APPL-ID
			ws.getErrorAndAdviceMessages().getEa61UsrInvalidApplIdMsg().setEa61ApplId(ws.getSaveArea().getCicsApplId().getCicsApplId());
			// COB_CODE: MOVE EA-61-USR-INVALID-APPL-ID-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa61UsrInvalidApplIdMsg().getEa61UsrInvalidApplIdMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidUserToken()) {
			// COB_CODE: MOVE SA-USER-TOKEN  TO EA-62-USER-TOKEN
			ws.getErrorAndAdviceMessages().getEa62UsrInvalidUsrTknMsg().setEa62UserToken(TruncAbs.toLong(ws.getSaveArea().getUserToken(), 10));
			// COB_CODE: MOVE EA-62-USR-INVALID-USR-TKN-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa62UsrInvalidUsrTknMsg().getEa62UsrInvalidUsrTknMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getPipeNotClosed()) {
			// COB_CODE: MOVE EA-63-USR-PIPE-NOT-CLOSED-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa63UsrPipeNotClosedMsg().getEa63UsrPipeNotClosedMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getPipeNotOpen()) {
			// COB_CODE: MOVE EA-64-USR-PIPE-NOT-OPEN-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa64UsrPipeNotOpenMsg().getEa64UsrPipeNotOpenMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getInvalidPipeToken()) {
			// COB_CODE: MOVE SA-PIPE-TOKEN  TO EA-65-PIPE-TOKEN
			ws.getErrorAndAdviceMessages().getEa65UsrInvalidPipTknMsg().setEa65PipeToken(TruncAbs.toLong(ws.getSaveArea().getPipeToken(), 10));
			// COB_CODE: MOVE EA-65-USR-INVALID-PIP-TKN-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa65UsrInvalidPipTknMsg().getEa65UsrInvalidPipTknMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getDfhxcoptLoadFailed()) {
			// COB_CODE: MOVE EA-66-USR-DFHXCOPT-LOAD-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa66UsrDfhxcoptLoadMsg().getEa66UsrDfhxcoptLoadMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-60-GEN-USER-ERROR-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa60GenUserErrorMsg().getEa60GenUserErrorMsgFormatted());
		}
		// COB_CODE: SET L-CM-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setSystemError();
		ws.getSwErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9040-SYSTEM-MSG<br>
	 * <pre>******************************************************
	 *     CICS ERROR HANDLING - SYSTEM ERROR MESSAGE GENERATION
	 * ******************************************************</pre>*/
	private void systemMsg() {
		// COB_CODE: EVALUATE TRUE
		//               WHEN EXCI-REASON = CICS-SVC-CALL-FAILURE
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN EXCI-REASON = IRC-CONNECT-FAILURE
		//                                       TO L-CM-EI-MESSAGE
		//               WHEN OTHER
		//                                       TO L-CM-EI-MESSAGE
		//           END-EVALUATE.
		if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getCicsSvcCallFailure()) {
			// COB_CODE: MOVE EA-81-SYS-CALL-CICS-FAIL-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa81SysCallCicsFailMsg().getEa81SysCallCicsFailMsgFormatted());
		} else if (ws.getDfhxcplo().getExciReturnCode().getReason() == ws.getDfhxcrco().getIrcConnectFailure()) {
			// COB_CODE: PERFORM 9200-GET-LIST-OF-TRIED-REGIONS
			//              THRU 9200-EXIT
			rng9200GetListOfTriedRegions();
			// COB_CODE: MOVE SA-LIST-OF-REGIONS-ATTEMPTED
			//                               TO EA-82-REGIONS-ATTEMPTED
			ws.getErrorAndAdviceMessages().getEa82SysConnectionFailMsg().setEa82RegionsAttempted(ws.getSaveArea().getListOfRegionsAttempted());
			// COB_CODE: MOVE EA-82-SYS-CONNECTION-FAIL-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa82SysConnectionFailMsg().getEa82SysConnectionFailMsgFormatted());
		} else {
			// COB_CODE: MOVE EA-80-GEN-SYSTEM-ERR-MSG
			//                               TO L-CM-EI-MESSAGE
			lCicsPipeMaintenanceCnt.getErrorInformation()
					.setMessage(ws.getErrorAndAdviceMessages().getEa80GenSystemErrMsg().getEa80GenSystemErrMsgFormatted());
		}
		// COB_CODE: SET L-CM-EI-ES-SYSTEM-ERROR
		//               SW-ERROR-FOUND          TO TRUE.
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setSystemError();
		ws.getSwErrorFoundFlag().setErrorFound();
	}

	/**Original name: 9200-GET-LIST-OF-TRIED-REGIONS<br>
	 * <pre>******************************************************
	 *     GENERATE LIST OF REGIONS WHERE CONNECTION ATTEMPTS WERE MADE
	 * ******************************************************</pre>*/
	private void getListOfTriedRegions() {
		// COB_CODE: MOVE +0                     TO SS-RA-LIST.
		ws.getSubscripts().setRaList(((short) 0));
		// COB_CODE: MOVE +1                     TO SS-TEXT-START.
		ws.getSubscripts().setTextStart(((short) 1));
		// COB_CODE: MOVE SPACES                 TO SA-LIST-OF-REGIONS-ATTEMPTED.
		ws.getSaveArea().setListOfRegionsAttempted("");
	}

	/**Original name: 9200-A<br>*/
	private String a1() {
		ConcatUtil concatUtil = null;
		// COB_CODE: ADD +1                      TO SS-RA-LIST.
		ws.getSubscripts().setRaList(Trunc.toShort(1 + ws.getSubscripts().getRaList(), 4));
		// COB_CODE: IF SS-RA-LIST > SS-RA
		//             OR
		//              SS-TEXT-START >= LENGTH OF SA-LIST-OF-REGIONS-ATTEMPTED
		//               GO TO 9200-EXIT
		//           END-IF.
		if (ws.getSubscripts().getRaList() > ws.getSubscripts().getRa()
				|| ws.getSubscripts().getTextStart() >= SaveArea.Len.LIST_OF_REGIONS_ATTEMPTED) {
			// COB_CODE: GO TO 9200-EXIT
			return "";
		}
		// COB_CODE: STRING TB-RA-REGION (SS-RA-LIST)
		//                  CF-SEPARATOR DELIMITED BY SIZE
		//               INTO SA-LIST-OF-REGIONS-ATTEMPTED (SS-TEXT-START :)
		//           END-STRING.
		concatUtil = ConcatUtil.buildString(SaveArea.Len.LIST_OF_REGIONS_ATTEMPTED,
				ws.getTbRaInf(ws.getSubscripts().getRaList()).getTbRaRegionFormatted(), ws.getConstantFields().getSeparatorFormatted());
		ws.getSaveArea().setListOfRegionsAttempted(
				Functions.setSubstring(ws.getSaveArea().getListOfRegionsAttempted(), concatUtil.getOutput(), ws.getSubscripts().getTextStart()));
		// COB_CODE: COMPUTE SS-TEXT-START = SS-TEXT-START
		//                                 + LENGTH OF TB-RA-REGION
		//                                 + LENGTH OF CF-SEPARATOR.
		ws.getSubscripts().setTextStart(((short) (ws.getSubscripts().getTextStart() + TbRaInf.Len.TB_RA_REGION + ConstantFields.Len.SEPARATOR)));
		// COB_CODE: GO TO 9200-A.
		return "9200-A";
	}

	/**Original name: RNG_3000-OPEN-CICS-PIPE-_-3000-EXIT<br>*/
	private void rng3000OpenCicsPipe() {
		String retcode = "";
		boolean goto3000A = false;
		boolean goto3000Exit = false;
		retcode = openCicsPipe();
		if (!retcode.equals("3000-EXIT")) {
			do {
				goto3000A = false;
				retcode = a();
			} while (retcode.equals("3000-A"));
		}
		goto3000Exit = false;
	}

	/**Original name: RNG_9200-GET-LIST-OF-TRIED-REGIONS-_-9200-EXIT<br>*/
	private void rng9200GetListOfTriedRegions() {
		String retcode = "";
		boolean goto9200A = false;
		getListOfTriedRegions();
		do {
			goto9200A = false;
			retcode = a1();
		} while (retcode.equals("9200-A"));
	}

	public void initErrorInformation() {
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorSeverity().setErrorSeverity(Types.SPACE_CHAR);
		lCicsPipeMaintenanceCnt.getErrorInformation().getErrorType().setErrorType(Types.SPACE_CHAR);
		lCicsPipeMaintenanceCnt.getErrorInformation().setMessage("");
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp(0);
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp2(0);
		lCicsPipeMaintenanceCnt.getErrorInformation().setExciResp3(0);
		lCicsPipeMaintenanceCnt.getErrorInformation().setDplResp(0);
		lCicsPipeMaintenanceCnt.getErrorInformation().setDplResp2(0);
	}

	public void initPipeIdTokens() {
		lCicsPipeMaintenanceCnt.setPiUserToken(0);
		lCicsPipeMaintenanceCnt.setPiPipeToken(0);
	}

	public void initTableOfRegionsAttempted() {
		for (int idx0 = 1; idx0 <= Ts548099Data.TB_RA_INF_MAXOCCURS; idx0++) {
			ws.getTbRaInf(idx0).setTbRaRegion("");
		}
	}
}
