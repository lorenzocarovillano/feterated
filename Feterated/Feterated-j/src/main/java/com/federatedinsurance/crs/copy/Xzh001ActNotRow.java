/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.commons.data.to.IActNot;
import com.modernsystems.jdbc.FieldNotMappedException;

/**Original name: XZH001-ACT-NOT-ROW<br>
 * Variable: XZH001-ACT-NOT-ROW from copybook XZ0H0001<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Xzh001ActNotRow implements IActNot {

	//==== PROPERTIES ====
	//Original name: XZH001-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZH001-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);
	//Original name: XZH001-ACT-NOT-TYP-CD
	private String actNotTypCd = DefaultValues.stringVal(Len.ACT_NOT_TYP_CD);
	//Original name: XZH001-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZH001-ACT-OWN-CLT-ID
	private String actOwnCltId = DefaultValues.stringVal(Len.ACT_OWN_CLT_ID);
	//Original name: XZH001-ACT-OWN-ADR-ID
	private String actOwnAdrId = DefaultValues.stringVal(Len.ACT_OWN_ADR_ID);
	//Original name: XZH001-EMP-ID-NI
	private short empIdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-EMP-ID
	private String empId = DefaultValues.stringVal(Len.EMP_ID);
	//Original name: XZH001-STA-MDF-TS
	private String staMdfTs = DefaultValues.stringVal(Len.STA_MDF_TS);
	//Original name: XZH001-ACT-NOT-STA-CD
	private String actNotStaCd = DefaultValues.stringVal(Len.ACT_NOT_STA_CD);
	//Original name: XZH001-PDC-NBR-NI
	private short pdcNbrNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-PDC-NBR
	private String pdcNbr = DefaultValues.stringVal(Len.PDC_NBR);
	//Original name: XZH001-PDC-NM-NI
	private short pdcNmNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-PDC-NM-LEN
	private short pdcNmLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-PDC-NM-TEXT
	private String pdcNmText = DefaultValues.stringVal(Len.PDC_NM_TEXT);
	//Original name: XZH001-SEG-CD-NI
	private short segCdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-SEG-CD
	private String segCd = DefaultValues.stringVal(Len.SEG_CD);
	//Original name: XZH001-ACT-TYP-CD-NI
	private short actTypCdNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: XZH001-TOT-FEE-AMT-NI
	private short totFeeAmtNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-TOT-FEE-AMT
	private AfDecimal totFeeAmt = new AfDecimal(DefaultValues.DEC_VAL, 10, 2);
	//Original name: XZH001-ST-ABB-NI
	private short stAbbNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-ST-ABB
	private String stAbb = DefaultValues.stringVal(Len.ST_ABB);
	//Original name: XZH001-CER-HLD-NOT-IND-NI
	private short cerHldNotIndNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-CER-HLD-NOT-IND
	private char cerHldNotInd = DefaultValues.CHAR_VAL;
	//Original name: XZH001-ADD-CNC-DAY-NI
	private short addCncDayNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-ADD-CNC-DAY
	private short addCncDay = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-REA-DES-NI
	private short reaDesNi = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-REA-DES-LEN
	private short reaDesLen = DefaultValues.BIN_SHORT_VAL;
	//Original name: XZH001-REA-DES-TEXT
	private String reaDesText = DefaultValues.stringVal(Len.REA_DES_TEXT);

	//==== METHODS ====
	public String getXzh001ActNotRowFormatted() {
		return MarshalByteExt.bufferToStr(getXzh001ActNotRowBytes());
	}

	public byte[] getXzh001ActNotRowBytes() {
		byte[] buffer = new byte[Len.XZH001_ACT_NOT_ROW];
		return getXzh001ActNotRowBytes(buffer, 1);
	}

	public byte[] getXzh001ActNotRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		position += Len.NOT_PRC_TS;
		MarshalByte.writeString(buffer, position, actNotTypCd, Len.ACT_NOT_TYP_CD);
		position += Len.ACT_NOT_TYP_CD;
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		MarshalByte.writeString(buffer, position, actOwnCltId, Len.ACT_OWN_CLT_ID);
		position += Len.ACT_OWN_CLT_ID;
		MarshalByte.writeString(buffer, position, actOwnAdrId, Len.ACT_OWN_ADR_ID);
		position += Len.ACT_OWN_ADR_ID;
		MarshalByte.writeBinaryShort(buffer, position, empIdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, empId, Len.EMP_ID);
		position += Len.EMP_ID;
		MarshalByte.writeString(buffer, position, staMdfTs, Len.STA_MDF_TS);
		position += Len.STA_MDF_TS;
		MarshalByte.writeString(buffer, position, actNotStaCd, Len.ACT_NOT_STA_CD);
		position += Len.ACT_NOT_STA_CD;
		MarshalByte.writeBinaryShort(buffer, position, pdcNbrNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, pdcNbr, Len.PDC_NBR);
		position += Len.PDC_NBR;
		MarshalByte.writeBinaryShort(buffer, position, pdcNmNi);
		position += Types.SHORT_SIZE;
		getPdcNmBytes(buffer, position);
		position += Len.PDC_NM;
		MarshalByte.writeBinaryShort(buffer, position, segCdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, segCd, Len.SEG_CD);
		position += Len.SEG_CD;
		MarshalByte.writeBinaryShort(buffer, position, actTypCdNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, actTypCd, Len.ACT_TYP_CD);
		position += Len.ACT_TYP_CD;
		MarshalByte.writeBinaryShort(buffer, position, totFeeAmtNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeDecimalAsPacked(buffer, position, totFeeAmt.copy());
		position += Len.TOT_FEE_AMT;
		MarshalByte.writeBinaryShort(buffer, position, stAbbNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, stAbb, Len.ST_ABB);
		position += Len.ST_ABB;
		MarshalByte.writeBinaryShort(buffer, position, cerHldNotIndNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeChar(buffer, position, cerHldNotInd);
		position += Types.CHAR_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, addCncDayNi);
		position += Types.SHORT_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, addCncDay);
		position += Types.SHORT_SIZE;
		MarshalByte.writeBinaryShort(buffer, position, reaDesNi);
		position += Types.SHORT_SIZE;
		getReaDesBytes(buffer, position);
		return buffer;
	}

	public void initXzh001ActNotRowSpaces() {
		csrActNbr = "";
		notPrcTs = "";
		actNotTypCd = "";
		notDt = "";
		actOwnCltId = "";
		actOwnAdrId = "";
		empIdNi = Types.INVALID_BINARY_SHORT_VAL;
		empId = "";
		staMdfTs = "";
		actNotStaCd = "";
		pdcNbrNi = Types.INVALID_BINARY_SHORT_VAL;
		pdcNbr = "";
		pdcNmNi = Types.INVALID_BINARY_SHORT_VAL;
		initPdcNmSpaces();
		segCdNi = Types.INVALID_BINARY_SHORT_VAL;
		segCd = "";
		actTypCdNi = Types.INVALID_BINARY_SHORT_VAL;
		actTypCd = "";
		totFeeAmtNi = Types.INVALID_BINARY_SHORT_VAL;
		totFeeAmt.setNaN();
		stAbbNi = Types.INVALID_BINARY_SHORT_VAL;
		stAbb = "";
		cerHldNotIndNi = Types.INVALID_BINARY_SHORT_VAL;
		cerHldNotInd = Types.SPACE_CHAR;
		addCncDayNi = Types.INVALID_BINARY_SHORT_VAL;
		addCncDay = Types.INVALID_BINARY_SHORT_VAL;
		reaDesNi = Types.INVALID_BINARY_SHORT_VAL;
		initReaDesSpaces();
	}

	@Override
	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	@Override
	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getCsrActNbrFormatted() {
		return Functions.padBlanks(getCsrActNbr(), Len.CSR_ACT_NBR);
	}

	@Override
	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	@Override
	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	public String getNotPrcTsFormatted() {
		return Functions.padBlanks(getNotPrcTs(), Len.NOT_PRC_TS);
	}

	@Override
	public void setActNotTypCd(String actNotTypCd) {
		this.actNotTypCd = Functions.subString(actNotTypCd, Len.ACT_NOT_TYP_CD);
	}

	@Override
	public String getActNotTypCd() {
		return this.actNotTypCd;
	}

	@Override
	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	@Override
	public String getNotDt() {
		return this.notDt;
	}

	@Override
	public void setActOwnCltId(String actOwnCltId) {
		this.actOwnCltId = Functions.subString(actOwnCltId, Len.ACT_OWN_CLT_ID);
	}

	@Override
	public String getActOwnCltId() {
		return this.actOwnCltId;
	}

	@Override
	public void setActOwnAdrId(String actOwnAdrId) {
		this.actOwnAdrId = Functions.subString(actOwnAdrId, Len.ACT_OWN_ADR_ID);
	}

	@Override
	public String getActOwnAdrId() {
		return this.actOwnAdrId;
	}

	public void setEmpIdNi(short empIdNi) {
		this.empIdNi = empIdNi;
	}

	public short getEmpIdNi() {
		return this.empIdNi;
	}

	@Override
	public void setEmpId(String empId) {
		this.empId = Functions.subString(empId, Len.EMP_ID);
	}

	@Override
	public String getEmpId() {
		return this.empId;
	}

	@Override
	public void setStaMdfTs(String staMdfTs) {
		this.staMdfTs = Functions.subString(staMdfTs, Len.STA_MDF_TS);
	}

	@Override
	public String getStaMdfTs() {
		return this.staMdfTs;
	}

	@Override
	public void setActNotStaCd(String actNotStaCd) {
		this.actNotStaCd = Functions.subString(actNotStaCd, Len.ACT_NOT_STA_CD);
	}

	@Override
	public String getActNotStaCd() {
		return this.actNotStaCd;
	}

	public void setPdcNbrNi(short pdcNbrNi) {
		this.pdcNbrNi = pdcNbrNi;
	}

	public short getPdcNbrNi() {
		return this.pdcNbrNi;
	}

	@Override
	public void setPdcNbr(String pdcNbr) {
		this.pdcNbr = Functions.subString(pdcNbr, Len.PDC_NBR);
	}

	@Override
	public String getPdcNbr() {
		return this.pdcNbr;
	}

	public void setPdcNmNi(short pdcNmNi) {
		this.pdcNmNi = pdcNmNi;
	}

	public short getPdcNmNi() {
		return this.pdcNmNi;
	}

	public byte[] getPdcNmBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, pdcNmLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, pdcNmText, Len.PDC_NM_TEXT);
		return buffer;
	}

	public void initPdcNmSpaces() {
		pdcNmLen = Types.INVALID_BINARY_SHORT_VAL;
		pdcNmText = "";
	}

	public void setPdcNmLen(short pdcNmLen) {
		this.pdcNmLen = pdcNmLen;
	}

	public short getPdcNmLen() {
		return this.pdcNmLen;
	}

	public void setPdcNmText(String pdcNmText) {
		this.pdcNmText = Functions.subString(pdcNmText, Len.PDC_NM_TEXT);
	}

	public String getPdcNmText() {
		return this.pdcNmText;
	}

	public String getPdcNmTextFormatted() {
		return Functions.padBlanks(getPdcNmText(), Len.PDC_NM_TEXT);
	}

	public void setSegCdNi(short segCdNi) {
		this.segCdNi = segCdNi;
	}

	public short getSegCdNi() {
		return this.segCdNi;
	}

	@Override
	public void setSegCd(String segCd) {
		this.segCd = Functions.subString(segCd, Len.SEG_CD);
	}

	@Override
	public String getSegCd() {
		return this.segCd;
	}

	public void setActTypCdNi(short actTypCdNi) {
		this.actTypCdNi = actTypCdNi;
	}

	public short getActTypCdNi() {
		return this.actTypCdNi;
	}

	@Override
	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	@Override
	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setTotFeeAmtNi(short totFeeAmtNi) {
		this.totFeeAmtNi = totFeeAmtNi;
	}

	public short getTotFeeAmtNi() {
		return this.totFeeAmtNi;
	}

	@Override
	public void setTotFeeAmt(AfDecimal totFeeAmt) {
		this.totFeeAmt.assign(totFeeAmt);
	}

	@Override
	public AfDecimal getTotFeeAmt() {
		return this.totFeeAmt.copy();
	}

	public void setStAbbNi(short stAbbNi) {
		this.stAbbNi = stAbbNi;
	}

	public short getStAbbNi() {
		return this.stAbbNi;
	}

	@Override
	public void setStAbb(String stAbb) {
		this.stAbb = Functions.subString(stAbb, Len.ST_ABB);
	}

	@Override
	public String getStAbb() {
		return this.stAbb;
	}

	public void setCerHldNotIndNi(short cerHldNotIndNi) {
		this.cerHldNotIndNi = cerHldNotIndNi;
	}

	public short getCerHldNotIndNi() {
		return this.cerHldNotIndNi;
	}

	@Override
	public void setCerHldNotInd(char cerHldNotInd) {
		this.cerHldNotInd = cerHldNotInd;
	}

	@Override
	public char getCerHldNotInd() {
		return this.cerHldNotInd;
	}

	public void setAddCncDayNi(short addCncDayNi) {
		this.addCncDayNi = addCncDayNi;
	}

	public short getAddCncDayNi() {
		return this.addCncDayNi;
	}

	@Override
	public void setAddCncDay(short addCncDay) {
		this.addCncDay = addCncDay;
	}

	@Override
	public short getAddCncDay() {
		return this.addCncDay;
	}

	public void setReaDesNi(short reaDesNi) {
		this.reaDesNi = reaDesNi;
	}

	public short getReaDesNi() {
		return this.reaDesNi;
	}

	public byte[] getReaDesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeBinaryShort(buffer, position, reaDesLen);
		position += Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, reaDesText, Len.REA_DES_TEXT);
		return buffer;
	}

	public void initReaDesSpaces() {
		reaDesLen = Types.INVALID_BINARY_SHORT_VAL;
		reaDesText = "";
	}

	public void setReaDesLen(short reaDesLen) {
		this.reaDesLen = reaDesLen;
	}

	public short getReaDesLen() {
		return this.reaDesLen;
	}

	public void setReaDesText(String reaDesText) {
		this.reaDesText = Functions.subString(reaDesText, Len.REA_DES_TEXT);
	}

	public String getReaDesText() {
		return this.reaDesText;
	}

	public String getReaDesTextFormatted() {
		return Functions.padBlanks(getReaDesText(), Len.REA_DES_TEXT);
	}

	@Override
	public String getActTypCdObj() {
		return getActTypCd();
	}

	@Override
	public void setActTypCdObj(String actTypCdObj) {
		setActTypCd(actTypCdObj);
	}

	@Override
	public Short getAddCncDayObj() {
		return (getAddCncDay());
	}

	@Override
	public void setAddCncDayObj(Short addCncDayObj) {
		setAddCncDay((addCncDayObj));
	}

	@Override
	public Character getCerHldNotIndObj() {
		return (getCerHldNotInd());
	}

	@Override
	public void setCerHldNotIndObj(Character cerHldNotIndObj) {
		setCerHldNotInd((cerHldNotIndObj));
	}

	@Override
	public String getEmpIdObj() {
		return getEmpId();
	}

	@Override
	public void setEmpIdObj(String empIdObj) {
		setEmpId(empIdObj);
	}

	@Override
	public String getPdcNbrObj() {
		return getPdcNbr();
	}

	@Override
	public void setPdcNbrObj(String pdcNbrObj) {
		setPdcNbr(pdcNbrObj);
	}

	@Override
	public String getPdcNm() {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public void setPdcNm(String pdcNm) {
		throw new FieldNotMappedException("pdcNm");
	}

	@Override
	public String getPdcNmObj() {
		return getPdcNm();
	}

	@Override
	public void setPdcNmObj(String pdcNmObj) {
		setPdcNm(pdcNmObj);
	}

	@Override
	public String getReaDes() {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public void setReaDes(String reaDes) {
		throw new FieldNotMappedException("reaDes");
	}

	@Override
	public String getReaDesObj() {
		return getReaDes();
	}

	@Override
	public void setReaDesObj(String reaDesObj) {
		setReaDes(reaDesObj);
	}

	@Override
	public String getSaNotPrcTsTime() {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public void setSaNotPrcTsTime(String saNotPrcTsTime) {
		throw new FieldNotMappedException("saNotPrcTsTime");
	}

	@Override
	public String getSegCdObj() {
		return getSegCd();
	}

	@Override
	public void setSegCdObj(String segCdObj) {
		setSegCd(segCdObj);
	}

	@Override
	public String getStAbbObj() {
		return getStAbb();
	}

	@Override
	public void setStAbbObj(String stAbbObj) {
		setStAbb(stAbbObj);
	}

	@Override
	public AfDecimal getTotFeeAmtObj() {
		return getTotFeeAmt().toString();
	}

	@Override
	public void setTotFeeAmtObj(AfDecimal totFeeAmtObj) {
		setTotFeeAmt(new AfDecimal(totFeeAmtObj, 10, 2));
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int ACT_NOT_TYP_CD = 5;
		public static final int NOT_DT = 10;
		public static final int ACT_OWN_CLT_ID = 64;
		public static final int ACT_OWN_ADR_ID = 64;
		public static final int EMP_ID = 6;
		public static final int STA_MDF_TS = 26;
		public static final int ACT_NOT_STA_CD = 2;
		public static final int PDC_NBR = 5;
		public static final int PDC_NM_TEXT = 120;
		public static final int SEG_CD = 3;
		public static final int ACT_TYP_CD = 2;
		public static final int ST_ABB = 2;
		public static final int REA_DES_TEXT = 500;
		public static final int EMP_ID_NI = 2;
		public static final int PDC_NBR_NI = 2;
		public static final int PDC_NM_NI = 2;
		public static final int PDC_NM_LEN = 2;
		public static final int PDC_NM = PDC_NM_LEN + PDC_NM_TEXT;
		public static final int SEG_CD_NI = 2;
		public static final int ACT_TYP_CD_NI = 2;
		public static final int TOT_FEE_AMT_NI = 2;
		public static final int TOT_FEE_AMT = 6;
		public static final int ST_ABB_NI = 2;
		public static final int CER_HLD_NOT_IND_NI = 2;
		public static final int CER_HLD_NOT_IND = 1;
		public static final int ADD_CNC_DAY_NI = 2;
		public static final int ADD_CNC_DAY = 2;
		public static final int REA_DES_NI = 2;
		public static final int REA_DES_LEN = 2;
		public static final int REA_DES = REA_DES_LEN + REA_DES_TEXT;
		public static final int XZH001_ACT_NOT_ROW = CSR_ACT_NBR + NOT_PRC_TS + ACT_NOT_TYP_CD + NOT_DT + ACT_OWN_CLT_ID + ACT_OWN_ADR_ID + EMP_ID_NI
				+ EMP_ID + STA_MDF_TS + ACT_NOT_STA_CD + PDC_NBR_NI + PDC_NBR + PDC_NM_NI + PDC_NM + SEG_CD_NI + SEG_CD + ACT_TYP_CD_NI + ACT_TYP_CD
				+ TOT_FEE_AMT_NI + TOT_FEE_AMT + ST_ABB_NI + ST_ABB + CER_HLD_NOT_IND_NI + CER_HLD_NOT_IND + ADD_CNC_DAY_NI + ADD_CNC_DAY + REA_DES_NI
				+ REA_DES;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 8;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}

		public static class Fract {

			//==== PROPERTIES ====
			public static final int TOT_FEE_AMT = 2;

			//==== CONSTRUCTORS ====
			private Fract() {
			}
		}
	}
}
