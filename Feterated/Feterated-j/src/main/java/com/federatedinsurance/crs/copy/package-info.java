/*
 * Copybook class(es) for program CAWI001
 * Copybook class(es) for program CAWI002
 * Copybook class(es) for program CAWI004
 * Copybook class(es) for program CAWI006
 * Copybook class(es) for program CAWI008
 * Copybook class(es) for program CAWI011
 * Copybook class(es) for program CAWI027
 * Copybook class(es) for program CAWI040
 * Copybook class(es) for program CAWI045
 * Copybook class(es) for program CAWI046
 * Copybook class(es) for program CAWI048
 * Copybook class(es) for program CAWI049
 * Copybook class(es) for program CAWI0EM
 * Copybook class(es) for program CAWICAC
 * Copybook class(es) for program CAWIGIK
 * Copybook class(es) for program CAWPCORC
 * Copybook class(es) for program CAWS002
 * Copybook class(es) for program CAWUIDB
 * Copybook class(es) for program CIWBNSRB
 * Copybook class(es) for program FNC02090
 * Copybook class(es) for program HALOEPAL
 * Copybook class(es) for program HALOESTO
 * Copybook class(es) for program HALOETRA
 * Copybook class(es) for program HALOUCHS
 * Copybook class(es) for program HALOUIDG
 * Copybook class(es) for program HALOUKRP
 * Copybook class(es) for program HALOUSDH
 * Copybook class(es) for program HALRLODR
 * Copybook class(es) for program HALRLOMG
 * Copybook class(es) for program HALRRESP
 * Copybook class(es) for program HALUIDB
 * Copybook class(es) for program TS020000
 * Copybook class(es) for program TS020100
 * Copybook class(es) for program TS020200
 * Copybook class(es) for program TS030199
 * Copybook class(es) for program TS529099
 * Copybook class(es) for program TS547099
 * Copybook class(es) for program TS548099
 * Copybook class(es) for program XPIODAT
 * Copybook class(es) for program XZ003000
 * Copybook class(es) for program XZ004000
 * Copybook class(es) for program XZ0B9010
 * Copybook class(es) for program XZ0B9020
 * Copybook class(es) for program XZ0B9050
 * Copybook class(es) for program XZ0B9070
 * Copybook class(es) for program XZ0B9071
 * Copybook class(es) for program XZ0B9072
 * Copybook class(es) for program XZ0B9080
 * Copybook class(es) for program XZ0B9081
 * Copybook class(es) for program XZ0B9090
 * Copybook class(es) for program XZ0B90O0
 * Copybook class(es) for program XZ0B90S0
 * Copybook class(es) for program XZ0D0001
 * Copybook class(es) for program XZ0D0002
 * Copybook class(es) for program XZ0D0003
 * Copybook class(es) for program XZ0D0004
 * Copybook class(es) for program XZ0D0005
 * Copybook class(es) for program XZ0D0006
 * Copybook class(es) for program XZ0D0007
 * Copybook class(es) for program XZ0D0008
 * Copybook class(es) for program XZ0F0001
 * Copybook class(es) for program XZ0F0002
 * Copybook class(es) for program XZ0F0003
 * Copybook class(es) for program XZ0G0004
 * Copybook class(es) for program XZ0G90R0
 * Copybook class(es) for program XZ0P0021
 * Copybook class(es) for program XZ0P9000
 * Copybook class(es) for program XZ0P90D0
 * Copybook class(es) for program XZ0P90E0
 * Copybook class(es) for program XZ0P90H0
 * Copybook class(es) for program XZ0P90K0
 * Copybook class(es) for program XZ0P90M0
 * Copybook class(es) for program XZ0U8000
 * Copybook class(es) for program XZ0U8010
 * Copybook class(es) for program XZ400000
 * Copybook class(es) for program XZC01090
 * Copybook class(es) for program XZC02090
 * Copybook class(es) for program XZC03090
 * Copybook class(es) for program XZC05090
 * Copybook class(es) for program XZC06090
 * Copybook class(es) for program XZC08090
 * Copybook class(es) for program XZC09090
 */
package com.federatedinsurance.crs.copy;
