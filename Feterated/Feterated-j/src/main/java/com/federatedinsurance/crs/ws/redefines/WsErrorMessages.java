/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.redefines;

import com.bphx.ctu.af.core.buffer.BytesAllocatingClass;

/**Original name: WS-ERROR-MESSAGES<br>
 * Variable: WS-ERROR-MESSAGES from program CIWBNSRB<br>
 * Generated as a class for rule REDEFINES_MULTIPLE.<br>*/
public class WsErrorMessages extends BytesAllocatingClass {

	//==== PROPERTIES ====
	public static final int FLR11_MAXOCCURS = 10;

	//==== CONSTRUCTORS ====
	public WsErrorMessages() {
		super();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_ERROR_MESSAGES;
	}

	@Override
	public void init() {
		int position = 1;
		writeString(position, "EBLANK NAME ENTERED", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WTYPE CODE CHANGED TO PERSONAL", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WTYPE CODE CHANGED TO COMPANY", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WCOMPANY NAME COMPONENT FOUND", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WONLY ONE NAME FOUND", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "W 2 NAMES CONJOINED BY AND/&/OR", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "EILLEGAL CHARACTERS IN NAME", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "W\"-\" IN NAME PARTS TREATED AS 1", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WSUPERFLUOUS WORD DROPPED", Len.FLR1);
		position += Len.FLR1;
		writeString(position, "WMORE THAN 1 MIDDLE NAME FOUND", Len.FLR1);
	}

	/**Original name: ERROR-STATUS<br>*/
	public char getErrorStatus(int errorStatusIdx) {
		int position = Pos.errorStatus(errorStatusIdx - 1);
		return readChar(position);
	}

	/**Original name: ERROR-MESSAGE<br>*/
	public String getErrorMessage(int errorMessageIdx) {
		int position = Pos.errorMessage(errorMessageIdx - 1);
		return readString(position, Len.ERROR_MESSAGE);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int WS_ERROR_MESSAGES = 1;
		public static final int FLR1 = WS_ERROR_MESSAGES;
		public static final int FLR2 = FLR1 + Len.FLR1;
		public static final int FLR3 = FLR2 + Len.FLR1;
		public static final int FLR4 = FLR3 + Len.FLR1;
		public static final int FLR5 = FLR4 + Len.FLR1;
		public static final int FLR6 = FLR5 + Len.FLR1;
		public static final int FLR7 = FLR6 + Len.FLR1;
		public static final int FLR8 = FLR7 + Len.FLR1;
		public static final int FLR9 = FLR8 + Len.FLR1;
		public static final int FLR10 = FLR9 + Len.FLR1;
		public static final int WS_ERR_MESSAGE = 1;

		//==== CONSTRUCTORS ====
		private Pos() {
		}

		//==== METHODS ====
		public static int flr11(int idx) {
			return WS_ERR_MESSAGE + idx * Len.FLR11;
		}

		public static int errorStatus(int idx) {
			return flr11(idx);
		}

		public static int errorMessage(int idx) {
			return errorStatus(idx) + Len.ERROR_STATUS;
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int FLR1 = 31;
		public static final int ERROR_STATUS = 1;
		public static final int ERROR_MESSAGE = 30;
		public static final int FLR11 = ERROR_STATUS + ERROR_MESSAGE;
		public static final int WS_ERROR_MESSAGES = 10 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
