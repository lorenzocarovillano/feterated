/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: TP-POLICIES<br>
 * Variable: TP-POLICIES from program XZ0P90L0<br>
 * Generated as a class for rule COMBINED_88_GROUP.<br>*/
public class TpPoliciesXz0p90l0 {

	//==== PROPERTIES ====
	public static final String END_OF_TABLE = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.TP_POLICIES);
	//Original name: TP-POL-NBR
	private String polNbr = DefaultValues.stringVal(Len.POL_NBR);
	//Original name: TP-POL-EFF-DT
	private String polEffDt = DefaultValues.stringVal(Len.POL_EFF_DT);
	//Original name: TP-POL-EXP-DT
	private String polExpDt = DefaultValues.stringVal(Len.POL_EXP_DT);
	//Original name: TP-NOT-EFF-DT
	private String notEffDt = DefaultValues.stringVal(Len.NOT_EFF_DT);

	//==== METHODS ====
	public String getTpPoliciesFormatted() {
		return MarshalByteExt.bufferToStr(getTpPoliciesBytes());
	}

	public byte[] getTpPoliciesBytes() {
		byte[] buffer = new byte[Len.TP_POLICIES];
		return getTpPoliciesBytes(buffer, 1);
	}

	public byte[] getTpPoliciesBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, polNbr, Len.POL_NBR);
		position += Len.POL_NBR;
		MarshalByte.writeString(buffer, position, polEffDt, Len.POL_EFF_DT);
		position += Len.POL_EFF_DT;
		MarshalByte.writeString(buffer, position, polExpDt, Len.POL_EXP_DT);
		position += Len.POL_EXP_DT;
		MarshalByte.writeString(buffer, position, notEffDt, Len.NOT_EFF_DT);
		return buffer;
	}

	public void initTpPoliciesHighValues() {
		polNbr = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_NBR);
		polEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EFF_DT);
		polExpDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.POL_EXP_DT);
		notEffDt = LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOT_EFF_DT);
	}

	public boolean isEndOfTable() {
		return Functions.trimAfter(getTpPoliciesFormatted()).equals(END_OF_TABLE);
	}

	public void setPolNbr(String polNbr) {
		this.polNbr = Functions.subString(polNbr, Len.POL_NBR);
	}

	public String getPolNbr() {
		return this.polNbr;
	}

	public void setPolEffDt(String polEffDt) {
		this.polEffDt = Functions.subString(polEffDt, Len.POL_EFF_DT);
	}

	public String getPolEffDt() {
		return this.polEffDt;
	}

	public void setPolExpDt(String polExpDt) {
		this.polExpDt = Functions.subString(polExpDt, Len.POL_EXP_DT);
	}

	public String getPolExpDt() {
		return this.polExpDt;
	}

	public void setNotEffDt(String notEffDt) {
		this.notEffDt = Functions.subString(notEffDt, Len.NOT_EFF_DT);
	}

	public String getNotEffDt() {
		return this.notEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int POL_NBR = 25;
		public static final int POL_EFF_DT = 10;
		public static final int POL_EXP_DT = 10;
		public static final int NOT_EFF_DT = 10;
		public static final int TP_POLICIES = POL_NBR + POL_EFF_DT + POL_EXP_DT + NOT_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
