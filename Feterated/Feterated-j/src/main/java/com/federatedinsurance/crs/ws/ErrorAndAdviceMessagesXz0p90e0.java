/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

/**Original name: ERROR-AND-ADVICE-MESSAGES<br>
 * Variable: ERROR-AND-ADVICE-MESSAGES from program XZ0P90E0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ErrorAndAdviceMessagesXz0p90e0 {

	//==== PROPERTIES ====
	//Original name: EA-01-NOTHING-ADDED-MSG
	private Ea01NothingAddedMsgXz0p90e0 ea01NothingAddedMsg = new Ea01NothingAddedMsgXz0p90e0();
	//Original name: EA-02-THIRD-PARTY-FORM-ERROR
	private Ea02ThirdPartyFormError ea02ThirdPartyFormError = new Ea02ThirdPartyFormError();
	//Original name: EA-03-NO-REC-ATTACHED-ERROR
	private Ea03NoRecAttachedError ea03NoRecAttachedError = new Ea03NoRecAttachedError();

	//==== METHODS ====
	public Ea01NothingAddedMsgXz0p90e0 getEa01NothingAddedMsg() {
		return ea01NothingAddedMsg;
	}

	public Ea02ThirdPartyFormError getEa02ThirdPartyFormError() {
		return ea02ThirdPartyFormError;
	}

	public Ea03NoRecAttachedError getEa03NoRecAttachedError() {
		return ea03NoRecAttachedError;
	}
}
