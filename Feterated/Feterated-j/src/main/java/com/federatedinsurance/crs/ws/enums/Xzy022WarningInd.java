/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: XZY022-WARNING-IND<br>
 * Variable: XZY022-WARNING-IND from copybook XZ0Y0022<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class Xzy022WarningInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char WARNINGS_OCC = 'Y';
	public static final char NO_WARNINGS_OCC = 'N';

	//==== METHODS ====
	public void setWarningInd(char warningInd) {
		this.value = warningInd;
	}

	public char getWarningInd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int WARNING_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
