/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ACT_NOT, ACT_NOT_POL, POL_DTA_EXT]
 * 
 */
public interface IActNotPolDtaExt extends BaseSqlTo {

	/**
	 * Host Variable CF-ACT-NOT-IMPENDING
	 * 
	 */
	String getCfActNotImpending();

	void setCfActNotImpending(String cfActNotImpending);

	/**
	 * Host Variable CF-ACT-NOT-NONPAY
	 * 
	 */
	String getCfActNotNonpay();

	void setCfActNotNonpay(String cfActNotNonpay);

	/**
	 * Host Variable CF-ACT-NOT-RESCIND
	 * 
	 */
	String getCfActNotRescind();

	void setCfActNotRescind(String cfActNotRescind);

	/**
	 * Host Variable ACT-TYP-CD
	 * 
	 */
	String getActTypCd();

	void setActTypCd(String actTypCd);

	/**
	 * Host Variable CF-ADDED-BY-BUSINESS-WORKS
	 * 
	 */
	String getCfAddedByBusinessWorks();

	void setCfAddedByBusinessWorks(String cfAddedByBusinessWorks);

	/**
	 * Host Variable POL-NBR
	 * 
	 */
	String getPolNbr();

	void setPolNbr(String polNbr);

	/**
	 * Host Variable CSR-ACT-NBR
	 * 
	 */
	String getCsrActNbr();

	void setCsrActNbr(String csrActNbr);

	/**
	 * Host Variable NOT-PRC-TS
	 * 
	 */
	String getNotPrcTs();

	void setNotPrcTs(String notPrcTs);

	/**
	 * Host Variable NOT-EFF-DT
	 * 
	 */
	String getNotEffDt();

	void setNotEffDt(String notEffDt);

	/**
	 * Host Variable POL-EFF-DT
	 * 
	 */
	String getPolEffDt();

	void setPolEffDt(String polEffDt);
};
