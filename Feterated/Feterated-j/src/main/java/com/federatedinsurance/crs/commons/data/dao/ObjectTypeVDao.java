/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IObjectTypeV;
import com.modernsystems.jdbc.BaseSqlDao;

/**
 * Data Access Object(DAO) for table [OBJECT_TYPE_V]
 * 
 */
public class ObjectTypeVDao extends BaseSqlDao<IObjectTypeV> {

	public ObjectTypeVDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IObjectTypeV> getToClass() {
		return IObjectTypeV.class;
	}

	public String selectByObjCd(String objCd, String dft) {
		return buildQuery("selectByObjCd").bind("objCd", objCd).scalarResultString(dft);
	}
}
