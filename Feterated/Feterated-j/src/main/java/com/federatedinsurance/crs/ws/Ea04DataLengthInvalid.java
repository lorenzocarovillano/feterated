/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.format.PicFormatter;
import com.bphx.ctu.af.util.format.PicParser;

/**Original name: EA-04-DATA-LENGTH-INVALID<br>
 * Variable: EA-04-DATA-LENGTH-INVALID from program TS547099<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Ea04DataLengthInvalid {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID
	private String flr1 = "TS547099 -";
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-1
	private String flr2 = "DATA LENGTH IS";
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-2
	private String flr3 = "INVALID (";
	//Original name: EA-04-BAD-DATA-LEN
	private String badDataLen = DefaultValues.stringVal(Len.BAD_DATA_LEN);
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-3
	private String flr4 = ").  LENGTH";
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-4
	private String flr5 = "MUST BE";
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-5
	private String flr6 = "FROM";
	//Original name: EA-04-MIN-DATA-LEN
	private String minDataLen = DefaultValues.stringVal(Len.MIN_DATA_LEN);
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-6
	private String flr7 = " THROUGH";
	//Original name: EA-04-MAX-DATA-LEN
	private String maxDataLen = DefaultValues.stringVal(Len.MAX_DATA_LEN);
	//Original name: FILLER-EA-04-DATA-LENGTH-INVALID-7
	private String flr8 = " INCLUSIVELY.";

	//==== METHODS ====
	public String getEa04DataLengthInvalidFormatted() {
		return MarshalByteExt.bufferToStr(getEa04DataLengthInvalidBytes());
	}

	public byte[] getEa04DataLengthInvalidBytes() {
		byte[] buffer = new byte[Len.EA04_DATA_LENGTH_INVALID];
		return getEa04DataLengthInvalidBytes(buffer, 1);
	}

	public byte[] getEa04DataLengthInvalidBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, badDataLen, Len.BAD_DATA_LEN);
		position += Len.BAD_DATA_LEN;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR5);
		position += Len.FLR5;
		MarshalByte.writeString(buffer, position, flr6, Len.FLR6);
		position += Len.FLR6;
		MarshalByte.writeString(buffer, position, minDataLen, Len.MIN_DATA_LEN);
		position += Len.MIN_DATA_LEN;
		MarshalByte.writeString(buffer, position, flr7, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, maxDataLen, Len.MAX_DATA_LEN);
		position += Len.MAX_DATA_LEN;
		MarshalByte.writeString(buffer, position, flr8, Len.FLR8);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public void setBadDataLen(long badDataLen) {
		this.badDataLen = PicFormatter.display("++,+(3),+(3),+(2)9").format(badDataLen).toString();
	}

	public long getBadDataLen() {
		return PicParser.display("++,+(3),+(3),+(2)9").parseLong(this.badDataLen);
	}

	public String getFlr4() {
		return this.flr4;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public String getFlr6() {
		return this.flr6;
	}

	public void setMinDataLen(long minDataLen) {
		this.minDataLen = PicFormatter.display("++(2)9").format(minDataLen).toString();
	}

	public long getMinDataLen() {
		return PicParser.display("++(2)9").parseLong(this.minDataLen);
	}

	public String getFlr7() {
		return this.flr7;
	}

	public void setMaxDataLen(long maxDataLen) {
		this.maxDataLen = PicFormatter.display("++,+(3),+(3),+(2)9").format(maxDataLen).toString();
	}

	public long getMaxDataLen() {
		return PicParser.display("++,+(3),+(3),+(2)9").parseLong(this.maxDataLen);
	}

	public String getFlr8() {
		return this.flr8;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BAD_DATA_LEN = 14;
		public static final int MIN_DATA_LEN = 4;
		public static final int MAX_DATA_LEN = 14;
		public static final int FLR1 = 11;
		public static final int FLR2 = 15;
		public static final int FLR3 = 9;
		public static final int FLR5 = 8;
		public static final int FLR6 = 5;
		public static final int FLR8 = 13;
		public static final int EA04_DATA_LENGTH_INVALID = BAD_DATA_LEN + MIN_DATA_LEN + MAX_DATA_LEN + 2 * FLR1 + FLR2 + 2 * FLR3 + FLR5 + FLR6
				+ FLR8;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
