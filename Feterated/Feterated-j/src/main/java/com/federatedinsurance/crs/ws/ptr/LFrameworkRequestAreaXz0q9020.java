/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0Q9020<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0q9020 extends BytesClass {

	//==== PROPERTIES ====
	public static final String XZA920_GET_REC_LIST_BY_FRM = "GetRecipientListByFormSeqNbr";
	public static final String XZA920_GET_REC_LIST_BY_NOT = "GetRecipientListByNotification";

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0q9020() {
	}

	public LFrameworkRequestAreaXz0q9020(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setXza920qMaxRecRows(short xza920qMaxRecRows) {
		writeBinaryShort(Pos.XZA920_MAX_REC_ROWS, xza920qMaxRecRows);
	}

	/**Original name: XZA920Q-MAX-REC-ROWS<br>*/
	public short getXza920qMaxRecRows() {
		return readBinaryShort(Pos.XZA920_MAX_REC_ROWS);
	}

	public void setXza920qOperation(String xza920qOperation) {
		writeString(Pos.XZA920_OPERATION, xza920qOperation, Len.XZA920_OPERATION);
	}

	/**Original name: XZA920Q-OPERATION<br>*/
	public String getXza920qOperation() {
		return readString(Pos.XZA920_OPERATION, Len.XZA920_OPERATION);
	}

	public void setXza920qCsrActNbr(String xza920qCsrActNbr) {
		writeString(Pos.XZA920_CSR_ACT_NBR, xza920qCsrActNbr, Len.XZA920_CSR_ACT_NBR);
	}

	/**Original name: XZA920Q-CSR-ACT-NBR<br>*/
	public String getXza920qCsrActNbr() {
		return readString(Pos.XZA920_CSR_ACT_NBR, Len.XZA920_CSR_ACT_NBR);
	}

	public void setXza920qNotPrcTs(String xza920qNotPrcTs) {
		writeString(Pos.XZA920_NOT_PRC_TS, xza920qNotPrcTs, Len.XZA920_NOT_PRC_TS);
	}

	/**Original name: XZA920Q-NOT-PRC-TS<br>*/
	public String getXza920qNotPrcTs() {
		return readString(Pos.XZA920_NOT_PRC_TS, Len.XZA920_NOT_PRC_TS);
	}

	public void setXza920qFrmSeqNbr(int xza920qFrmSeqNbr) {
		writeInt(Pos.XZA920_FRM_SEQ_NBR, xza920qFrmSeqNbr, Len.Int.XZA920Q_FRM_SEQ_NBR);
	}

	/**Original name: XZA920Q-FRM-SEQ-NBR<br>*/
	public int getXza920qFrmSeqNbr() {
		return readNumDispInt(Pos.XZA920_FRM_SEQ_NBR, Len.XZA920_FRM_SEQ_NBR);
	}

	public void setXza920qRecallRecSeqNbr(int xza920qRecallRecSeqNbr) {
		writeInt(Pos.XZA920_RECALL_REC_SEQ_NBR, xza920qRecallRecSeqNbr, Len.Int.XZA920Q_RECALL_REC_SEQ_NBR);
	}

	/**Original name: XZA920Q-RECALL-REC-SEQ-NBR<br>*/
	public int getXza920qRecallRecSeqNbr() {
		return readNumDispInt(Pos.XZA920_RECALL_REC_SEQ_NBR, Len.XZA920_RECALL_REC_SEQ_NBR);
	}

	public void setXza920qUserid(String xza920qUserid) {
		writeString(Pos.XZA920_USERID, xza920qUserid, Len.XZA920_USERID);
	}

	/**Original name: XZA920Q-USERID<br>*/
	public String getXza920qUserid() {
		return readString(Pos.XZA920_USERID, Len.XZA920_USERID);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int L_FW_REQ_XZ0A9020 = L_FRAMEWORK_REQUEST_AREA;
		public static final int XZA920_GET_REC_LIST_KEY = L_FW_REQ_XZ0A9020;
		public static final int XZA920_MAX_REC_ROWS = XZA920_GET_REC_LIST_KEY;
		public static final int XZA920_OPERATION = XZA920_MAX_REC_ROWS + Len.XZA920_MAX_REC_ROWS;
		public static final int XZA920_CSR_ACT_NBR = XZA920_OPERATION + Len.XZA920_OPERATION;
		public static final int XZA920_NOT_PRC_TS = XZA920_CSR_ACT_NBR + Len.XZA920_CSR_ACT_NBR;
		public static final int XZA920_FRM_SEQ_NBR = XZA920_NOT_PRC_TS + Len.XZA920_NOT_PRC_TS;
		public static final int XZA920_RECALL_REC_SEQ_NBR = XZA920_FRM_SEQ_NBR + Len.XZA920_FRM_SEQ_NBR;
		public static final int XZA920_USERID = XZA920_RECALL_REC_SEQ_NBR + Len.XZA920_RECALL_REC_SEQ_NBR;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int XZA920_MAX_REC_ROWS = 2;
		public static final int XZA920_OPERATION = 32;
		public static final int XZA920_CSR_ACT_NBR = 9;
		public static final int XZA920_NOT_PRC_TS = 26;
		public static final int XZA920_FRM_SEQ_NBR = 5;
		public static final int XZA920_RECALL_REC_SEQ_NBR = 5;
		public static final int XZA920_USERID = 8;
		public static final int XZA920_GET_REC_LIST_KEY = XZA920_MAX_REC_ROWS + XZA920_OPERATION + XZA920_CSR_ACT_NBR + XZA920_NOT_PRC_TS
				+ XZA920_FRM_SEQ_NBR + XZA920_RECALL_REC_SEQ_NBR + XZA920_USERID;
		public static final int L_FW_REQ_XZ0A9020 = XZA920_GET_REC_LIST_KEY;
		public static final int L_FRAMEWORK_REQUEST_AREA = L_FW_REQ_XZ0A9020;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int XZA920Q_FRM_SEQ_NBR = 5;
			public static final int XZA920Q_RECALL_REC_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
