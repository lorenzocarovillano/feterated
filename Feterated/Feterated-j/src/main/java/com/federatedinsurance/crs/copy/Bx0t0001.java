/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

/**Original name: BX0T0001<br>
 * Variable: BX0T0001 from copybook BX0T0001<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Bx0t0001 {

	//==== PROPERTIES ====
	//Original name: BXT01I-ACCOUNT-NBR
	private String bxt01iAccountNbr = DefaultValues.stringVal(Len.BXT01I_ACCOUNT_NBR);
	//Original name: BXT01I-START-DT
	private String bxt01iStartDt = DefaultValues.stringVal(Len.BXT01I_START_DT);
	//Original name: BXT01I-END-DT
	private String bxt01iEndDt = DefaultValues.stringVal(Len.BXT01I_END_DT);
	//Original name: BXT01I-USERID
	private String bxt01iUserid = DefaultValues.stringVal(Len.BXT01I_USERID);
	//Original name: BXT01O-ACCOUNT-NBR
	private String bxt01oAccountNbr = DefaultValues.stringVal(Len.BXT01O_ACCOUNT_NBR);
	//Original name: BXT01O-START-DT
	private String bxt01oStartDt = DefaultValues.stringVal(Len.BXT01O_START_DT);
	//Original name: BXT01O-END-DT
	private String bxt01oEndDt = DefaultValues.stringVal(Len.BXT01O_END_DT);
	//Original name: BXT01O-USERID
	private String bxt01oUserid = DefaultValues.stringVal(Len.BXT01O_USERID);
	//Original name: BXT01O-ACCOUNT-INFO
	private Bxt01oAccountInfo bxt01oAccountInfo = new Bxt01oAccountInfo();

	//==== METHODS ====
	public void setBxt001ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setBxt01iTechnicalKeyBytes(buffer, position);
	}

	public byte[] getBxt001ServiceInputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getBxt01iTechnicalKeyBytes(buffer, position);
		return buffer;
	}

	public void setBxt01iTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		bxt01iAccountNbr = MarshalByte.readString(buffer, position, Len.BXT01I_ACCOUNT_NBR);
		position += Len.BXT01I_ACCOUNT_NBR;
		bxt01iStartDt = MarshalByte.readString(buffer, position, Len.BXT01I_START_DT);
		position += Len.BXT01I_START_DT;
		bxt01iEndDt = MarshalByte.readString(buffer, position, Len.BXT01I_END_DT);
		position += Len.BXT01I_END_DT;
		bxt01iUserid = MarshalByte.readString(buffer, position, Len.BXT01I_USERID);
	}

	public byte[] getBxt01iTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, bxt01iAccountNbr, Len.BXT01I_ACCOUNT_NBR);
		position += Len.BXT01I_ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, bxt01iStartDt, Len.BXT01I_START_DT);
		position += Len.BXT01I_START_DT;
		MarshalByte.writeString(buffer, position, bxt01iEndDt, Len.BXT01I_END_DT);
		position += Len.BXT01I_END_DT;
		MarshalByte.writeString(buffer, position, bxt01iUserid, Len.BXT01I_USERID);
		return buffer;
	}

	public void setBxt01iAccountNbr(String bxt01iAccountNbr) {
		this.bxt01iAccountNbr = Functions.subString(bxt01iAccountNbr, Len.BXT01I_ACCOUNT_NBR);
	}

	public String getBxt01iAccountNbr() {
		return this.bxt01iAccountNbr;
	}

	public void setBxt01iStartDt(String bxt01iStartDt) {
		this.bxt01iStartDt = Functions.subString(bxt01iStartDt, Len.BXT01I_START_DT);
	}

	public String getBxt01iStartDt() {
		return this.bxt01iStartDt;
	}

	public void setBxt01iEndDt(String bxt01iEndDt) {
		this.bxt01iEndDt = Functions.subString(bxt01iEndDt, Len.BXT01I_END_DT);
	}

	public String getBxt01iEndDt() {
		return this.bxt01iEndDt;
	}

	public void setBxt01iUserid(String bxt01iUserid) {
		this.bxt01iUserid = Functions.subString(bxt01iUserid, Len.BXT01I_USERID);
	}

	public String getBxt01iUserid() {
		return this.bxt01iUserid;
	}

	public void setBxt001ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		setBxt01oTechnicalKeyBytes(buffer, position);
		position += Len.BXT01O_TECHNICAL_KEY;
		bxt01oAccountInfo.setBxt01oAccountInfoBytes(buffer, position);
	}

	public byte[] getBxt001ServiceOutputsBytes(byte[] buffer, int offset) {
		int position = offset;
		getBxt01oTechnicalKeyBytes(buffer, position);
		position += Len.BXT01O_TECHNICAL_KEY;
		bxt01oAccountInfo.getBxt01oAccountInfoBytes(buffer, position);
		return buffer;
	}

	public void setBxt01oTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		bxt01oAccountNbr = MarshalByte.readString(buffer, position, Len.BXT01O_ACCOUNT_NBR);
		position += Len.BXT01O_ACCOUNT_NBR;
		bxt01oStartDt = MarshalByte.readString(buffer, position, Len.BXT01O_START_DT);
		position += Len.BXT01O_START_DT;
		bxt01oEndDt = MarshalByte.readString(buffer, position, Len.BXT01O_END_DT);
		position += Len.BXT01O_END_DT;
		bxt01oUserid = MarshalByte.readString(buffer, position, Len.BXT01O_USERID);
	}

	public byte[] getBxt01oTechnicalKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, bxt01oAccountNbr, Len.BXT01O_ACCOUNT_NBR);
		position += Len.BXT01O_ACCOUNT_NBR;
		MarshalByte.writeString(buffer, position, bxt01oStartDt, Len.BXT01O_START_DT);
		position += Len.BXT01O_START_DT;
		MarshalByte.writeString(buffer, position, bxt01oEndDt, Len.BXT01O_END_DT);
		position += Len.BXT01O_END_DT;
		MarshalByte.writeString(buffer, position, bxt01oUserid, Len.BXT01O_USERID);
		return buffer;
	}

	public void setBxt01oAccountNbr(String bxt01oAccountNbr) {
		this.bxt01oAccountNbr = Functions.subString(bxt01oAccountNbr, Len.BXT01O_ACCOUNT_NBR);
	}

	public String getBxt01oAccountNbr() {
		return this.bxt01oAccountNbr;
	}

	public void setBxt01oStartDt(String bxt01oStartDt) {
		this.bxt01oStartDt = Functions.subString(bxt01oStartDt, Len.BXT01O_START_DT);
	}

	public String getBxt01oStartDt() {
		return this.bxt01oStartDt;
	}

	public void setBxt01oEndDt(String bxt01oEndDt) {
		this.bxt01oEndDt = Functions.subString(bxt01oEndDt, Len.BXT01O_END_DT);
	}

	public String getBxt01oEndDt() {
		return this.bxt01oEndDt;
	}

	public void setBxt01oUserid(String bxt01oUserid) {
		this.bxt01oUserid = Functions.subString(bxt01oUserid, Len.BXT01O_USERID);
	}

	public String getBxt01oUserid() {
		return this.bxt01oUserid;
	}

	public Bxt01oAccountInfo getBxt01oAccountInfo() {
		return bxt01oAccountInfo;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int BXT01I_ACCOUNT_NBR = 9;
		public static final int BXT01I_START_DT = 10;
		public static final int BXT01I_END_DT = 10;
		public static final int BXT01I_USERID = 8;
		public static final int BXT01I_TECHNICAL_KEY = BXT01I_ACCOUNT_NBR + BXT01I_START_DT + BXT01I_END_DT + BXT01I_USERID;
		public static final int BXT001_SERVICE_INPUTS = BXT01I_TECHNICAL_KEY;
		public static final int BXT01O_ACCOUNT_NBR = 9;
		public static final int BXT01O_START_DT = 10;
		public static final int BXT01O_END_DT = 10;
		public static final int BXT01O_USERID = 8;
		public static final int BXT01O_TECHNICAL_KEY = BXT01O_ACCOUNT_NBR + BXT01O_START_DT + BXT01O_END_DT + BXT01O_USERID;
		public static final int BXT001_SERVICE_OUTPUTS = BXT01O_TECHNICAL_KEY + Bxt01oAccountInfo.Len.BXT01O_ACCOUNT_INFO;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
