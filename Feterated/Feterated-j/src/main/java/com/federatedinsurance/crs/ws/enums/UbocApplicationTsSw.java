/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;

/**Original name: UBOC-APPLICATION-TS-SW<br>
 * Variable: UBOC-APPLICATION-TS-SW from copybook HALLUBOC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class UbocApplicationTsSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char NO_APP_TS_SET = Types.SPACE_CHAR;
	public static final char EXCEED_TS_SET = 'E';
	public static final char CICS_TS_SET = 'C';
	public static final char DB2_TS_SET = 'D';
	public static final char OTHER_TS_SET = 'O';

	//==== METHODS ====
	public void setUbocApplicationTsSw(char ubocApplicationTsSw) {
		this.value = ubocApplicationTsSw;
	}

	public void setUbocApplicationTsSwFormatted(String ubocApplicationTsSw) {
		setUbocApplicationTsSw(Functions.charAt(ubocApplicationTsSw, Types.CHAR_SIZE));
	}

	public char getUbocApplicationTsSw() {
		return this.value;
	}

	public void setUbocNoAppTsSet() {
		setUbocApplicationTsSwFormatted(String.valueOf(NO_APP_TS_SET));
	}

	public boolean isUbocExceedTsSet() {
		return value == EXCEED_TS_SET;
	}

	public void setUbocExceedTsSet() {
		value = EXCEED_TS_SET;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int UBOC_APPLICATION_TS_SW = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
