/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.ptr;

import com.bphx.ctu.af.core.buffer.BytesClass;

/**Original name: L-FRAMEWORK-REQUEST-AREA<br>
 * Variable: L-FRAMEWORK-REQUEST-AREA from program XZ0X9050<br>
 * Generated as a class for rule DATA_POINTER.<br>*/
public class LFrameworkRequestAreaXz0x9050 extends BytesClass {

	//==== PROPERTIES ====
	public static final String GET_POL_LIST_BY_NOT = "GetPolicyListByNotification";
	public static final String GET_POL_LIST_BY_FRM = "GetPolicyListByForm";

	//==== CONSTRUCTORS ====
	public LFrameworkRequestAreaXz0x9050() {
	}

	public LFrameworkRequestAreaXz0x9050(byte[] data) {
		super(data);
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.L_FRAMEWORK_REQUEST_AREA;
	}

	public void setMaxPolRowsReturned(short maxPolRowsReturned) {
		writeBinaryShort(Pos.MAX_POL_ROWS_RETURNED, maxPolRowsReturned);
	}

	/**Original name: XZA950Q-MAX-POL-ROWS-RETURNED<br>*/
	public short getMaxPolRowsReturned() {
		return readBinaryShort(Pos.MAX_POL_ROWS_RETURNED);
	}

	public void setOperation(String operation) {
		writeString(Pos.OPERATION, operation, Len.OPERATION);
	}

	/**Original name: XZA950Q-OPERATION<br>*/
	public String getOperation() {
		return readString(Pos.OPERATION, Len.OPERATION);
	}

	public void setCsrActNbr(String csrActNbr) {
		writeString(Pos.CSR_ACT_NBR, csrActNbr, Len.CSR_ACT_NBR);
	}

	/**Original name: XZA950Q-CSR-ACT-NBR<br>*/
	public String getCsrActNbr() {
		return readString(Pos.CSR_ACT_NBR, Len.CSR_ACT_NBR);
	}

	public void setNotPrcTs(String notPrcTs) {
		writeString(Pos.NOT_PRC_TS, notPrcTs, Len.NOT_PRC_TS);
	}

	/**Original name: XZA950Q-NOT-PRC-TS<br>*/
	public String getNotPrcTs() {
		return readString(Pos.NOT_PRC_TS, Len.NOT_PRC_TS);
	}

	public void setFrmSeqNbr(int frmSeqNbr) {
		writeInt(Pos.FRM_SEQ_NBR, frmSeqNbr, Len.Int.FRM_SEQ_NBR);
	}

	/**Original name: XZA950Q-FRM-SEQ-NBR<br>*/
	public int getFrmSeqNbr() {
		return readNumDispInt(Pos.FRM_SEQ_NBR, Len.FRM_SEQ_NBR);
	}

	//==== INNER CLASSES ====
	public static class Pos {

		//==== PROPERTIES ====
		public static final int L_FRAMEWORK_REQUEST_AREA = 1;
		public static final int POLICY_LIST_HEADER = L_FRAMEWORK_REQUEST_AREA;
		public static final int MAX_POL_ROWS_RETURNED = POLICY_LIST_HEADER;
		public static final int OPERATION = MAX_POL_ROWS_RETURNED + Len.MAX_POL_ROWS_RETURNED;
		public static final int CSR_ACT_NBR = OPERATION + Len.OPERATION;
		public static final int NOT_PRC_TS = CSR_ACT_NBR + Len.CSR_ACT_NBR;
		public static final int FRM_SEQ_NBR = NOT_PRC_TS + Len.NOT_PRC_TS;

		//==== CONSTRUCTORS ====
		private Pos() {
		}
	}

	public static class Len {

		//==== PROPERTIES ====
		public static final int MAX_POL_ROWS_RETURNED = 2;
		public static final int OPERATION = 32;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FRM_SEQ_NBR = 5;
		public static final int POLICY_LIST_HEADER = MAX_POL_ROWS_RETURNED + OPERATION + CSR_ACT_NBR + NOT_PRC_TS + FRM_SEQ_NBR;
		public static final int L_FRAMEWORK_REQUEST_AREA = POLICY_LIST_HEADER;

		//==== CONSTRUCTORS ====
		private Len() {
		}

		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public static final int FRM_SEQ_NBR = 5;

			//==== CONSTRUCTORS ====
			private Int() {
			}
		}
	}
}
