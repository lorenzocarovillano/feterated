/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.federatedinsurance.crs.commons.data.to.IFrmRecOvl;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;

/**
 * Data Access Object(DAO) for table [FRM_REC_OVL]
 * 
 */
public class FrmRecOvlDao extends BaseSqlDao<IFrmRecOvl> {

	private Cursor overlayCsr;
	private final IRowMapper<IFrmRecOvl> fetchOverlayCsrRm = buildNamedRowMapper(IFrmRecOvl.class, "frmNbr", "frmEdtDt", "recTypCd", "ovlEdlFrmNm",
			"stAbb", "spePrcCdObj", "ovlSrCd");

	public FrmRecOvlDao(DbAccessStatus dbAccessStatus) {
		super(dbAccessStatus);
	}

	@Override
	public Class<IFrmRecOvl> getToClass() {
		return IFrmRecOvl.class;
	}

	public DbAccessStatus openOverlayCsr() {
		overlayCsr = buildQuery("openOverlayCsr").open();
		return dbStatus;
	}

	public IFrmRecOvl fetchOverlayCsr(IFrmRecOvl iFrmRecOvl) {
		return fetch(overlayCsr, iFrmRecOvl, fetchOverlayCsrRm);
	}

	public DbAccessStatus closeOverlayCsr() {
		return closeCursor(overlayCsr);
	}
}
