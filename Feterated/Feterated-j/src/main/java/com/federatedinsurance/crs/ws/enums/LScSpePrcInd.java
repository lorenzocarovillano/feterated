/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: L-SC-SPE-PRC-IND<br>
 * Variable: L-SC-SPE-PRC-IND from copybook TT008001<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class LScSpePrcInd {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char SPE_PRC = 'Y';
	public static final char NOT_SPE_PRC = 'N';

	//==== METHODS ====
	public void setSpePrcInd(char spePrcInd) {
		this.value = spePrcInd;
	}

	public char getSpePrcInd() {
		return this.value;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int SPE_PRC_IND = 1;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
