/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws.enums;

import com.bphx.ctu.af.core.DefaultValues;

/**Original name: WS-PLACEHOLDER-FOUND-SW<br>
 * Variable: WS-PLACEHOLDER-FOUND-SW from program HALRPLAC<br>
 * Generated as a class for rule 88_GROUP.<br>*/
public class WsPlaceholderFoundSw {

	//==== PROPERTIES ====
	private char value = DefaultValues.CHAR_VAL;
	public static final char FOUND = 'Y';
	public static final char NOT_FOUND = 'N';

	//==== METHODS ====
	public void setPlaceholderFoundSw(char placeholderFoundSw) {
		this.value = placeholderFoundSw;
	}

	public char getPlaceholderFoundSw() {
		return this.value;
	}

	public boolean isFound() {
		return value == FOUND;
	}

	public void setFound() {
		value = FOUND;
	}

	public void setNotFound() {
		value = NOT_FOUND;
	}
}
