/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: WS-TYP-BASED-FIELDS<br>
 * Variable: WS-TYP-BASED-FIELDS from program XZ0B90Q0<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WsTypBasedFields {

	//==== PROPERTIES ====
	//Original name: WS-ACT-TYP-CD
	private String actTypCd = DefaultValues.stringVal(Len.ACT_TYP_CD);
	//Original name: WS-PDC-NBR-START
	private char pdcNbrStart = DefaultValues.CHAR_VAL;
	//Original name: FILLER-WS-PDC-NBR
	private char flr1 = '-';
	//Original name: WS-PDC-NBR-END
	private String pdcNbrEnd = DefaultValues.stringVal(Len.PDC_NBR_END);
	//Original name: WS-PDC-NM
	private String pdcNm = DefaultValues.stringVal(Len.PDC_NM);
	//Original name: WS-LAST-NAME
	private String lastName = DefaultValues.stringVal(Len.LAST_NAME);
	//Original name: WS-COWN-NM
	private String cownNm = "";

	//==== METHODS ====
	public void setActTypCd(String actTypCd) {
		this.actTypCd = Functions.subString(actTypCd, Len.ACT_TYP_CD);
	}

	public String getActTypCd() {
		return this.actTypCd;
	}

	public void setPdcNbrFormatted(String data) {
		byte[] buffer = new byte[Len.PDC_NBR];
		MarshalByte.writeString(buffer, 1, data, Len.PDC_NBR);
		setPdcNbrBytes(buffer, 1);
	}

	public String getPdcNbrFormatted() {
		return MarshalByteExt.bufferToStr(getPdcNbrBytes());
	}

	/**Original name: WS-PDC-NBR<br>*/
	public byte[] getPdcNbrBytes() {
		byte[] buffer = new byte[Len.PDC_NBR];
		return getPdcNbrBytes(buffer, 1);
	}

	public void setPdcNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		pdcNbrStart = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		flr1 = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		pdcNbrEnd = MarshalByte.readString(buffer, position, Len.PDC_NBR_END);
	}

	public byte[] getPdcNbrBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, pdcNbrStart);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, flr1);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, pdcNbrEnd, Len.PDC_NBR_END);
		return buffer;
	}

	public void setPdcNbrStart(char pdcNbrStart) {
		this.pdcNbrStart = pdcNbrStart;
	}

	public void setPdcNbrStartFormatted(String pdcNbrStart) {
		setPdcNbrStart(Functions.charAt(pdcNbrStart, Types.CHAR_SIZE));
	}

	public char getPdcNbrStart() {
		return this.pdcNbrStart;
	}

	public void setFlr1(char flr1) {
		this.flr1 = flr1;
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setPdcNbrEnd(String pdcNbrEnd) {
		this.pdcNbrEnd = Functions.subString(pdcNbrEnd, Len.PDC_NBR_END);
	}

	public String getPdcNbrEnd() {
		return this.pdcNbrEnd;
	}

	public void setPdcNm(String pdcNm) {
		this.pdcNm = Functions.subString(pdcNm, Len.PDC_NM);
	}

	public String getPdcNm() {
		return this.pdcNm;
	}

	public void setLastName(String lastName) {
		this.lastName = Functions.subString(lastName, Len.LAST_NAME);
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setCownNm(String cownNm) {
		this.cownNm = Functions.subString(cownNm, Len.COWN_NM);
	}

	public String getCownNm() {
		return this.cownNm;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int ACT_TYP_CD = 2;
		public static final int PDC_NBR_END = 3;
		public static final int PDC_NM = 120;
		public static final int LAST_NAME = 60;
		public static final int PDC_NBR_START = 1;
		public static final int FLR1 = 1;
		public static final int PDC_NBR = PDC_NBR_START + PDC_NBR_END + FLR1;
		public static final int COWN_NM = 45;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
