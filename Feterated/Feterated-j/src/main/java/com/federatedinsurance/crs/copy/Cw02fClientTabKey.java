/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: CW02F-CLIENT-TAB-KEY<br>
 * Variable: CW02F-CLIENT-TAB-KEY from copybook CAWLF002<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Cw02fClientTabKey {

	//==== PROPERTIES ====
	//Original name: CW02F-CLIENT-ID-CI
	private char clientIdCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CLIENT-ID
	private String clientId = DefaultValues.stringVal(Len.CLIENT_ID);
	//Original name: CW02F-HISTORY-VLD-NBR-CI
	private char historyVldNbrCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HISTORY-VLD-NBR-SIGN
	private char historyVldNbrSign = DefaultValues.CHAR_VAL;
	//Original name: CW02F-HISTORY-VLD-NBR
	private String historyVldNbr = DefaultValues.stringVal(Len.HISTORY_VLD_NBR);
	//Original name: CW02F-CICL-EFF-DT-CI
	private char ciclEffDtCi = DefaultValues.CHAR_VAL;
	//Original name: CW02F-CICL-EFF-DT
	private String ciclEffDt = DefaultValues.stringVal(Len.CICL_EFF_DT);

	//==== METHODS ====
	public void setClientTabKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		clientIdCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		clientId = MarshalByte.readString(buffer, position, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		historyVldNbrCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbrSign = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		historyVldNbr = MarshalByte.readFixedString(buffer, position, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		ciclEffDtCi = MarshalByte.readChar(buffer, position);
		position += Types.CHAR_SIZE;
		ciclEffDt = MarshalByte.readString(buffer, position, Len.CICL_EFF_DT);
	}

	public byte[] getClientTabKeyBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeChar(buffer, position, clientIdCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, clientId, Len.CLIENT_ID);
		position += Len.CLIENT_ID;
		MarshalByte.writeChar(buffer, position, historyVldNbrCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, historyVldNbrSign);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, historyVldNbr, Len.HISTORY_VLD_NBR);
		position += Len.HISTORY_VLD_NBR;
		MarshalByte.writeChar(buffer, position, ciclEffDtCi);
		position += Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, ciclEffDt, Len.CICL_EFF_DT);
		return buffer;
	}

	public void setClientIdCi(char clientIdCi) {
		this.clientIdCi = clientIdCi;
	}

	public char getClientIdCi() {
		return this.clientIdCi;
	}

	public void setClientId(String clientId) {
		this.clientId = Functions.subString(clientId, Len.CLIENT_ID);
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setHistoryVldNbrCi(char historyVldNbrCi) {
		this.historyVldNbrCi = historyVldNbrCi;
	}

	public char getHistoryVldNbrCi() {
		return this.historyVldNbrCi;
	}

	public void setHistoryVldNbrSign(char historyVldNbrSign) {
		this.historyVldNbrSign = historyVldNbrSign;
	}

	public char getHistoryVldNbrSign() {
		return this.historyVldNbrSign;
	}

	public void setCw02cHistoryVldNbr(int cw02cHistoryVldNbr) {
		this.historyVldNbr = NumericDisplay.asString(cw02cHistoryVldNbr, Len.HISTORY_VLD_NBR);
	}

	public void setCw02cHistoryVldNbrFormatted(String cw02cHistoryVldNbr) {
		this.historyVldNbr = Trunc.toUnsignedNumeric(cw02cHistoryVldNbr, Len.HISTORY_VLD_NBR);
	}

	public int getCw02cHistoryVldNbr() {
		return NumericDisplay.asInt(this.historyVldNbr);
	}

	public void setCiclEffDtCi(char ciclEffDtCi) {
		this.ciclEffDtCi = ciclEffDtCi;
	}

	public char getCiclEffDtCi() {
		return this.ciclEffDtCi;
	}

	public void setCiclEffDt(String ciclEffDt) {
		this.ciclEffDt = Functions.subString(ciclEffDt, Len.CICL_EFF_DT);
	}

	public String getCiclEffDt() {
		return this.ciclEffDt;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CLIENT_ID = 20;
		public static final int HISTORY_VLD_NBR = 5;
		public static final int CICL_EFF_DT = 10;
		public static final int CLIENT_ID_CI = 1;
		public static final int HISTORY_VLD_NBR_CI = 1;
		public static final int HISTORY_VLD_NBR_SIGN = 1;
		public static final int CICL_EFF_DT_CI = 1;
		public static final int CLIENT_TAB_KEY = CLIENT_ID_CI + CLIENT_ID + HISTORY_VLD_NBR_CI + HISTORY_VLD_NBR_SIGN + HISTORY_VLD_NBR
				+ CICL_EFF_DT_CI + CICL_EFF_DT;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
