/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;
import com.federatedinsurance.crs.ws.enums.Xzy021ActNotTypCd;
import com.federatedinsurance.crs.ws.occurs.Xzy021CncPolList;
import com.modernsystems.ctu.core.SerializableParameter;

/**Original name: WS-XZ0Y0021-ROW<br>
 * Variable: WS-XZ0Y0021-ROW from program XZ0P0021<br>
 * Generated as a class for rule USED_IN_CALL.<br>*/
public class WsXz0y0021Row extends SerializableParameter {

	//==== PROPERTIES ====
	public static final int CNC_POL_LIST_MAXOCCURS = 150;
	//Original name: XZY021-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: XZY021-ACT-TMN-DT
	private String actTmnDt = DefaultValues.stringVal(Len.ACT_TMN_DT);
	//Original name: XZY021-ACT-NOT-TYP-CD
	private Xzy021ActNotTypCd actNotTypCd = new Xzy021ActNotTypCd();
	//Original name: XZY021-CNC-POL-LIST
	private Xzy021CncPolList[] cncPolList = new Xzy021CncPolList[CNC_POL_LIST_MAXOCCURS];
	//Original name: XZY021-NOT-DT
	private String notDt = DefaultValues.stringVal(Len.NOT_DT);
	//Original name: XZY021-USERID
	private String userid = DefaultValues.stringVal(Len.USERID);

	//==== CONSTRUCTORS ====
	public WsXz0y0021Row() {
		init();
	}

	//==== METHODS ====
	@Override
	public int getLength() {
		return Len.WS_XZ0Y0021_ROW;
	}

	@Override
	public void deserialize(byte[] buf) {
		setWsXz0y0021RowBytes(buf);
	}

	public void init() {
		for (int cncPolListIdx = 1; cncPolListIdx <= CNC_POL_LIST_MAXOCCURS; cncPolListIdx++) {
			cncPolList[cncPolListIdx - 1] = new Xzy021CncPolList();
		}
	}

	public String getWsXz0y0021RowFormatted() {
		return getGetNotDtRowFormatted();
	}

	public void setWsXz0y0021RowBytes(byte[] buffer) {
		setWsXz0y0021RowBytes(buffer, 1);
	}

	public byte[] getWsXz0y0021RowBytes() {
		byte[] buffer = new byte[Len.WS_XZ0Y0021_ROW];
		return getWsXz0y0021RowBytes(buffer, 1);
	}

	public void setWsXz0y0021RowBytes(byte[] buffer, int offset) {
		int position = offset;
		setGetNotDtRowBytes(buffer, position);
	}

	public byte[] getWsXz0y0021RowBytes(byte[] buffer, int offset) {
		int position = offset;
		getGetNotDtRowBytes(buffer, position);
		return buffer;
	}

	public String getGetNotDtRowFormatted() {
		return MarshalByteExt.bufferToStr(getGetNotDtRowBytes());
	}

	/**Original name: XZY021-GET-NOT-DT-ROW<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *  XZ0Y0001 - GET NOTIFICATION DATE                               *
	 *                                                                 *
	 * *****************************************************************
	 *                                                                 *
	 * *****************************************************************
	 *  MAINTENANCE  LOG                                               *
	 *                                                                 *
	 *  SI#     DATE        PROG#     DESCRIPTION                      *
	 *  ------- ----------- --------- ---------------------------------*
	 *    15389 07/24/2017  E404JAL   NEW                              *
	 *                                                                 *
	 * *****************************************************************</pre>*/
	public byte[] getGetNotDtRowBytes() {
		byte[] buffer = new byte[Len.GET_NOT_DT_ROW];
		return getGetNotDtRowBytes(buffer, 1);
	}

	public void setGetNotDtRowBytes(byte[] buffer, int offset) {
		int position = offset;
		csrActNbr = MarshalByte.readString(buffer, position, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		actTmnDt = MarshalByte.readString(buffer, position, Len.ACT_TMN_DT);
		position += Len.ACT_TMN_DT;
		actNotTypCd.setActNotTypCd(MarshalByte.readString(buffer, position, Xzy021ActNotTypCd.Len.ACT_NOT_TYP_CD));
		position += Xzy021ActNotTypCd.Len.ACT_NOT_TYP_CD;
		for (int idx = 1; idx <= CNC_POL_LIST_MAXOCCURS; idx++) {
			if (position <= buffer.length) {
				cncPolList[idx - 1].setCncPolListBytes(buffer, position);
				position += Xzy021CncPolList.Len.CNC_POL_LIST;
			} else {
				cncPolList[idx - 1].initCncPolListSpaces();
				position += Xzy021CncPolList.Len.CNC_POL_LIST;
			}
		}
		notDt = MarshalByte.readString(buffer, position, Len.NOT_DT);
		position += Len.NOT_DT;
		userid = MarshalByte.readString(buffer, position, Len.USERID);
	}

	public byte[] getGetNotDtRowBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, actTmnDt, Len.ACT_TMN_DT);
		position += Len.ACT_TMN_DT;
		MarshalByte.writeString(buffer, position, actNotTypCd.getActNotTypCd(), Xzy021ActNotTypCd.Len.ACT_NOT_TYP_CD);
		position += Xzy021ActNotTypCd.Len.ACT_NOT_TYP_CD;
		for (int idx = 1; idx <= CNC_POL_LIST_MAXOCCURS; idx++) {
			cncPolList[idx - 1].getCncPolListBytes(buffer, position);
			position += Xzy021CncPolList.Len.CNC_POL_LIST;
		}
		MarshalByte.writeString(buffer, position, notDt, Len.NOT_DT);
		position += Len.NOT_DT;
		MarshalByte.writeString(buffer, position, userid, Len.USERID);
		return buffer;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public void setActTmnDt(String actTmnDt) {
		this.actTmnDt = Functions.subString(actTmnDt, Len.ACT_TMN_DT);
	}

	public String getActTmnDt() {
		return this.actTmnDt;
	}

	public void setNotDt(String notDt) {
		this.notDt = Functions.subString(notDt, Len.NOT_DT);
	}

	public String getNotDt() {
		return this.notDt;
	}

	public void setUserid(String userid) {
		this.userid = Functions.subString(userid, Len.USERID);
	}

	public String getUserid() {
		return this.userid;
	}

	public Xzy021ActNotTypCd getActNotTypCd() {
		return actNotTypCd;
	}

	public Xzy021CncPolList getCncPolList(int idx) {
		return cncPolList[idx - 1];
	}

	@Override
	public byte[] serialize() {
		return getWsXz0y0021RowBytes();
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int CSR_ACT_NBR = 9;
		public static final int ACT_TMN_DT = 10;
		public static final int NOT_DT = 10;
		public static final int USERID = 8;
		public static final int GET_NOT_DT_ROW = CSR_ACT_NBR + ACT_TMN_DT + Xzy021ActNotTypCd.Len.ACT_NOT_TYP_CD
				+ WsXz0y0021Row.CNC_POL_LIST_MAXOCCURS * Xzy021CncPolList.Len.CNC_POL_LIST + NOT_DT + USERID;
		public static final int WS_XZ0Y0021_ROW = GET_NOT_DT_ROW;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
