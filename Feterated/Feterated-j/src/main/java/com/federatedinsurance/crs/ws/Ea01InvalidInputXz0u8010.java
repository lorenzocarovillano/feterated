/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Functions;

/**Original name: EA-01-INVALID-INPUT<br>
 * Variable: EA-01-INVALID-INPUT from program XZ0U8010<br>
 * Generated as a class for rule RECORDS_THRESHOLD.<br>*/
public class Ea01InvalidInputXz0u8010 {

	//==== PROPERTIES ====
	//Original name: FILLER-EA-01-INVALID-INPUT
	private String flr1 = "PGM =";
	//Original name: EA-01-PROGRAM-NAME
	private String programName = DefaultValues.stringVal(Len.PROGRAM_NAME);
	//Original name: FILLER-EA-01-INVALID-INPUT-1
	private String flr2 = " PARA =";
	//Original name: EA-01-PARAGRAPH-NBR
	private String paragraphNbr = DefaultValues.stringVal(Len.PARAGRAPH_NBR);
	//Original name: FILLER-EA-01-INVALID-INPUT-2
	private String flr3 = " INVALID INPUT.";
	//Original name: FILLER-EA-01-INVALID-INPUT-3
	private String flr4 = " CSR-ACT-NBR =";
	//Original name: EA-01-CSR-ACT-NBR
	private String csrActNbr = DefaultValues.stringVal(Len.CSR_ACT_NBR);
	//Original name: FILLER-EA-01-INVALID-INPUT-4
	private String flr5 = ", NOT-PRC-TS =";
	//Original name: EA-01-NOT-PRC-TS
	private String notPrcTs = DefaultValues.stringVal(Len.NOT_PRC_TS);

	//==== METHODS ====
	public String getEa01InvalidInputFormatted() {
		return MarshalByteExt.bufferToStr(getEa01InvalidInputBytes());
	}

	public byte[] getEa01InvalidInputBytes() {
		byte[] buffer = new byte[Len.EA01_INVALID_INPUT];
		return getEa01InvalidInputBytes(buffer, 1);
	}

	public byte[] getEa01InvalidInputBytes(byte[] buffer, int offset) {
		int position = offset;
		MarshalByte.writeString(buffer, position, flr1, Len.FLR1);
		position += Len.FLR1;
		MarshalByte.writeString(buffer, position, programName, Len.PROGRAM_NAME);
		position += Len.PROGRAM_NAME;
		MarshalByte.writeString(buffer, position, flr2, Len.FLR2);
		position += Len.FLR2;
		MarshalByte.writeString(buffer, position, paragraphNbr, Len.PARAGRAPH_NBR);
		position += Len.PARAGRAPH_NBR;
		MarshalByte.writeString(buffer, position, flr3, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, flr4, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, csrActNbr, Len.CSR_ACT_NBR);
		position += Len.CSR_ACT_NBR;
		MarshalByte.writeString(buffer, position, flr5, Len.FLR3);
		position += Len.FLR3;
		MarshalByte.writeString(buffer, position, notPrcTs, Len.NOT_PRC_TS);
		return buffer;
	}

	public String getFlr1() {
		return this.flr1;
	}

	public void setProgramName(String programName) {
		this.programName = Functions.subString(programName, Len.PROGRAM_NAME);
	}

	public String getProgramName() {
		return this.programName;
	}

	public String getFlr2() {
		return this.flr2;
	}

	public void setParagraphNbr(String paragraphNbr) {
		this.paragraphNbr = Functions.subString(paragraphNbr, Len.PARAGRAPH_NBR);
	}

	public String getParagraphNbr() {
		return this.paragraphNbr;
	}

	public String getFlr3() {
		return this.flr3;
	}

	public String getFlr4() {
		return this.flr4;
	}

	public void setCsrActNbr(String csrActNbr) {
		this.csrActNbr = Functions.subString(csrActNbr, Len.CSR_ACT_NBR);
	}

	public String getCsrActNbr() {
		return this.csrActNbr;
	}

	public String getFlr5() {
		return this.flr5;
	}

	public void setNotPrcTs(String notPrcTs) {
		this.notPrcTs = Functions.subString(notPrcTs, Len.NOT_PRC_TS);
	}

	public String getNotPrcTs() {
		return this.notPrcTs;
	}

	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public static final int PROGRAM_NAME = 8;
		public static final int PARAGRAPH_NBR = 5;
		public static final int CSR_ACT_NBR = 9;
		public static final int NOT_PRC_TS = 26;
		public static final int FLR1 = 6;
		public static final int FLR2 = 8;
		public static final int FLR3 = 15;
		public static final int EA01_INVALID_INPUT = PROGRAM_NAME + PARAGRAPH_NBR + CSR_ACT_NBR + NOT_PRC_TS + FLR1 + FLR2 + 3 * FLR3;

		//==== CONSTRUCTORS ====
		private Len() {
		}
	}
}
