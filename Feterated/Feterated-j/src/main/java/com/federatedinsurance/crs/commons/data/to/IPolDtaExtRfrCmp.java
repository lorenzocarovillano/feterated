/*
 * Portions of this code incorporates licensed ModSys International Ltd technology
 * that may be used only in accordance with the ModSys International Ltd
 * license and services agreement.
 *
 * Copyright (c) 2004 - 2018, ModSys International Ltd, All rights reserved.
 */

package com.federatedinsurance.crs.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [POL_DTA_EXT, RFR_CMP]
 * 
 */
public interface IPolDtaExtRfrCmp extends BaseSqlTo {

	/**
	 * Host Variable CMP-CD
	 * 
	 */
	String getCd();

	void setCd(String cd);

	/**
	 * Host Variable CMP-DES
	 * 
	 */
	String getDes();

	void setDes(String des);
};
