//FXZ400  JOB (XZ4000,P),'CRS NOT RM PURGE ',
//        CLASS=P,
//        MSGCLASS=J,
//        MSGLEVEL=(1,1),
//        USER=FXZ
//*
//******************************************************************
//*
//*    MAINTENANCE HISTORY
//*    *******************
//*
//*   INFO     CHANGE
//*   NBR       DATE       III     DESCRIPTION
//*  *******  ********     ***     *********************************
//*  TO07614  04/03/09     GCL     NEW JOB.
//*
//******************************************************************
//*
//*  INCLUDE MEMBER=SDFHEXCI
//*  CICS LIBRARY REQUIRED FOR BATCH TO CICS PROGRAMS
//JOBLIB   DD DSN=CICSTSNR.CICS.SDFHEXCI.DUMMY,DISP=SHR
//* END OF INLINED INCLUDE
//*
//*
//UCCDB2   EXEC UCCDB2,                                 **SCHEDULER-1**
//           SSID='DBP1',
//           UTID='FXZ400',
//           TYPRUN='P'
//*          TYPRUN='R'
//*-----------------------------------------------------------------
//UCCDB201.QUIESCE  DD DSN=FED.SIIIPROD.CTLLIB(XZ000001),DISP=SHR
//*-----------------------------------------------------------------
//*
//******************************************************************
//XZ400005 EXEC XZ400066
//******************************************************************
//*
//IKJEFT01.CICSPARM DD *                               **SCHEDULER-2**
CICS9
//*CICSM   *FOR EXTENDED ONLINE IF RUN AT NIGHT WHEN PMCICS IS UP
//*CICS9   *FOR PRODUCTION IF RUN DURING THE DAY WHEN P9CICS IS UP
//*CICSX   *FOR PRODUCTION IF RUN AT NIGHT WHEN PXCICS IS UP
//*
//*
//IKJEFT01.SYSTSIN  DD *
 DSN SYSTEM(DBP1)
 RUN PROGRAM(XZ400000) -
 PLAN(PMSCPROD)
 END
/*
//
