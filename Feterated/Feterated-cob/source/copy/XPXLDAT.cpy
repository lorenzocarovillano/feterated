      ******************************************************************     ***
      *     COPYBOOK:  XPXLDAT                LENGTH:  271             *     ...
      *                                                                *05/16/97
      *                 M A I N T E N A N C E   L O G                  *XPXLDAT
      * 25JAN91- IR000732 ADDED DIFFERENCE CALCULATION                 *   LV007
      * 01AUG91- UPDATE FOR IAP4, AND WORKSTATION                       00006
      * 16MAY97- ADD ADJUSTMENT IN MINUTES                             *00007
      *                                                                *00008
      ******************************************************************00009
       01  DATE-STRUCTURE.                                              00010
               05  DATE-FUNCTION         PIC X(01).                     00011
      *              C = DATE CONVERSION                                00012
      *              D = DATE DIFFERENCE                                00013
               05  DATE-LANGUAGE         PIC X(03).                     00014
      *              SPACES = ALWAYS                                    00015
               05  DATE-INP-FORMAT       PIC X(02).                     00016
      *              J = YYYYDDD/HH:MM:SS                               00017
      *              G = YYYY/MM/DD/HH:MM:SS                            00018
      *              G1 = YYYY/MM/DD/HH:MM:SS                           00019
      *              G7 = YYYY/MM/DD/HH:MM:SS                           00020
      *              M = MM/DD/YYYY/HH:MM:SS                            00021
      *              M1 = MM/DD/YYYY/HH:MM:SS                           00022
      *              M7 = MM/DD/YYYY/HH:MM:SS                           00023
      *              D = DD/MM/YYYY/HH:MM:SS                            00024
      *              D1 = DD/MM/YYYY/HH:MM:SS                           00025
      *              D7 = DD/MM/YYYY/HH:MM:SS                           00026
      *              B = YYYY-MM-DD-HH.MM.SS.MMMMMM                     00027
      *              SY= (SYSTEM)                                       00028
      *              S3= IAP SERIES III DATE                            00029
      *              - ON WORKSTATION ONLY                              00030
      *              ?  = (G/M/D) FROM OS/2 CONTROL PANEL               00031
      *              ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL            00032
      *              ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL            00033
               05  DATE-OUT-FORMAT       PIC X(02).                     00034
      *              J = YYYYDDD HH:MM:SS                               00035
      *              G = YYYY/MM/DD HH:MM:SS                            00036
      *              M = MM/DD/YYYY HH:MM:SS                            00037
      *              D = DD/MM/YYYY HH:MM:SS                            00038
      *              W = DAY-OF-WEEK                                    00039
      *              AM= HH:MM:SS AM MONTH DD, YYYY                     00040
      *              AD= HH:MM:SS AM DD MONTH YYYY                      00041
      *              AS= HH:MM:SS AM DD MON YYYY                        00042
      *              B = YYYY-MM-DD-HH.MM.SS.MMMMMM                     00043
      *              - ON WORKSTATION ONLY                              00044
      *              ?  = (G/M/D) FROM OS/2 CONTROL PANEL               00045
      *              ?1 = (G1/M1/D1) FROM OS/2 CONTROL PANEL            00046
      *              ?7 = (G7/M7/D7) FROM OS/2 CONTROL PANEL            00047
      *              THIS IS AN INPUT FIELD FOR DIFFERENCE CALULATIONS  00048
               05  DATE-ADJ-FORMAT       PIC X(02).                     00049
      *              AD= +/- MINUTES                                    00050
      *              HD= +/- HOURS                                      00051
      *              DD= +/- DAYS                                       00052
      *              WD= +/- WEEKS                                      00053
      *              MD= +/- MONTHS                                     00054
      *              YD= +/- YEARS                                      00055
      *              *B= ABOVE IN FULL WORD BINARY  (HOST ONLY)         00056
      *              THIS IS AN OUTPUT FIELD FOR DIFFERENCE CALULATIONS 00057
               05  DATE-RETURN-CODE      PIC X(02).                     00058
      *              OK= GOOD NEWS                                      00059
      *              IF=INVALID FORMAT  (EDITING CHARACTERS)            00060
      *              ID=INVALID DATE    (>12 MONTHS,>31 DAYS)           00061
      *              IC=INVALID CODE    (DATE-XXX-FORMAT ERROR)         00062
      *              UC=UNSUPPORTED     (ON THIS PLATFORM)              00063
      *              OR=OUT OF RANGE FOR DIFFERENCE CALCULATIONS        00064
      *              XC=NOT IMPLEMENTED (YET)                           00065
               05  DATE-INPUT            PIC X(30).                     00066
      *              DATE TO EDIT OR REFORMAT                           00067
               05  DATE-OUTPUT           PIC X(30).                     00068
      *              REFORMATTED DATE RETURNED                          00069
      *              DATE FOR DIFFERENCE CALULATIONS                    00070
               05  DATE-ADJUST.                                         00071
                   10  DATE-ADJUST-SIGN  PIC X(01).                     00072
                   10  DATE-ADJUST-NUMB  PIC 9(05).                     00073
      *        BINARY FORMAT HOST ONLY                                  00074
               05  DATE-ADJUST-R   REDEFINES DATE-ADJUST.               00075
                   15  DATE-ADJUST-BIN   PIC S9(08), USAGE COMP.        00076
                   15  FILLER,           PIC X(02).                     00077
      *              ADD (OR SUBTRACT) THIS AMOUNT                      00078
      *            DIFFERENCE FIELD FOR DIFFERENCE CALULATIONS          00079
               05  DATE-DELIMITER-DATE   PIC X(01).                     00080
      *              / OR OTHER EDITING CHARACTER IN DATE               00081
      *              - ON WORKSTATION ONLY:                             00082
      *              ? GET DELIMITER FROM OS/2 CONTROL PANEL            00083
               05  DATE-DELIMITER-TIME   PIC X(01).                     00084
      *              : OR OTHER EDITING CHARACTER IN TIME               00085
      *              - ON WORKSTATION ONLY:                             00086
      *              ? GET DELIMITER FROM OS/2 CONTROL PANEL            00087
               05  DATE-AM-PM            PIC X(04).                     00088
      *              AMPM OR OTHER MORNING INDICATOR                    00089
      *              SPACES FOR 24 HOUR CLOCK                           00090
      *              - ON WORKSTATION ONLY:                             00091
      *              ? GET INDICATOR FROM OS/2 CONTROL PANEL            00092
               05  SCRATCH-AREA          PIC X(300).                    00093
      *     BELOW FOR INTERNAL USE ONLY                                 00094
               05  SCRATCH-AREA-REDEF REDEFINES SCRATCH-AREA.           00095
                   10 POSITION-IN-PROCESS PIC X(01).                    00096
                      88 DATE-FUNCTION-EDIT VALUE '0'.                  00097
                      88 DATE-INPUT-PROCESS VALUE '1'.                  00098
                      88 DATE-OUT-NO-ADJ    VALUE '2'.                  00099
                      88 DATE-OUT-PROCESS   VALUE '3'.                  00100
                      88 DATE-DIFF-FOR-OUT  VALUE '4'.                  00101
                   10 DATE-EXTENDED-ERROR PIC X(04).                    00102
                   10 FILLER,             PIC X(295).                   00103
      *                                                                 00104
      *---------------------------------------------------              00105
