      **************************************************************       CL240
      *   DGRUSTR SUBROUTINE PARMLIST WORKING STORAGE DEFINITIONS  *       CL240
      *                                                            *
      *                                                            *
      *   MAINTENANCE LOG:  (NOTE THAT ANY MODIFICATIONS WHICH ARE *
      *                     MADE TO THIS COPYBOOK MUST ALSO BE MADE*
      *                     TO COPYBOOK USTRPRMA WHICH IS USED BY  *
      *                     ASSEMBLER MODULES WHICH CALL DGRUSTR.  *
      *                     REF MOD# BE0031)                       *
      *                                                            *
      *   BE6007 - INITIALIZED STORAGE FOR ESA COMPATIBILITY.      *
      **************************************************************       CL240
       01  DGRUSTR-PARMLIST.
           05 FILLER                         PIC X(500)  VALUE SPACES.
           05 DGRUSTR-SOURCE-STRING-LEN      PIC S9(04) COMP VALUE +0.
           05 DGRUSTR-DEST-STRING-LEN        PIC S9(04) COMP VALUE +0.
           05 DGRUSTR-STRING-COUNT           PIC S9(04) COMP VALUE +0.
           05 DGRUSTR-STRING-POINTER         PIC S9(04) COMP VALUE +0.
           05 DGRUSTR-RETURN-CODE            PIC X(01)   VALUE SPACES.
              88 DGRUSTR-SUCCESSFUL                          VALUE '0'
                                                                   '1'
                                                                   '2'.
              88 DGRUSTR-NO-DELIMITER-MATCH                  VALUE '1'.
              88 DGRUSTR-SUCCESSFUL-PAD-ON-END               VALUE '2'.
      *                                                                    CL240
              88 DGRUSTR-FAILED                              VALUE '3'.
              88 DGRUSTR-PNTR-OUTSIDE-SRCE-STR               VALUE '3'.
           05 DGRUSTR-PAD-CHAR-RETURNED      PIC X(01)   VALUE SPACES.
           05 DGRUSTR-DELIM-VALUES           PIC X(250)  VALUE SPACES.
           05 DGRUSTR-DELIM-RETURNED         PIC X(250)  VALUE SPACES.
