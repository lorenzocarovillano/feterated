      ******************************************************************
      * DCLGEN TABLE(HAL_UOW_OBJ_HIER_V)                               *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE HAL_UOW_OBJ_HIER_V TABLE
           ( UOW_NM                         CHAR(32) NOT NULL,
             ROOT_BOBJ_NM                   CHAR(32) NOT NULL,
             HUOH_HIER_SEQ_NBR              SMALLINT NOT NULL,
             HUOH_PNT_BOBJ_NM               CHAR(32) NOT NULL,
             HUOH_CHD_BOBJ_NM               CHAR(32) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE HAL_UOW_OBJ_HIER_V                 *
      ******************************************************************
       01  DCLHAL-UOW-OBJ-HIER-V.
           10 HUOH-UOW-NM          PIC X(32).
           10 HUOH-ROOT-BOBJ-NM    PIC X(32).
           10 HUOH-HIER-SEQ-NBR    PIC S9(4) USAGE COMP.
           10 HUOH-PNT-BOBJ-NM     PIC X(32).
           10 HUOH-CHD-BOBJ-NM     PIC X(32).
