      ******************************************************************
      *                                                                *
      * XZ0H0008 - ACT_NOT_POL_REC TABLE                               *
      *            DCLGEN                                              *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#     DATE        PROG#     DESCRIPTION                      *
      * ------- ----------- --------- ---------------------------------*
      * PP02570 23 Sep 2010 E404DLP   GENERATED                        *
      *                                                                *
      ******************************************************************
           EXEC SQL DECLARE ACT_NOT_POL_REC TABLE
           (
             CSR_ACT_NBR                     CHAR(9) NOT NULL,
             NOT_PRC_TS                      TIMESTAMP NOT NULL,
             POL_NBR                         CHAR(25) NOT NULL,
             REC_SEQ_NBR                     SMALLINT NOT NULL
           ) END-EXEC.
      *****************************************************************
       01  XZH008-ACT-NOT-POL-REC-ROW.
           10  XZH008-CSR-ACT-NBR             PIC X(9).
           10  XZH008-NOT-PRC-TS              PIC X(26).
           10  XZH008-POL-NBR                 PIC X(25).
           10  XZH008-REC-SEQ-NBR             PIC S9(4) BINARY.
