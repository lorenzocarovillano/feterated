      ******************************************************************
      *                                                                *
      * XZ0H0006 - ACT_NOT_FRM TABLE                                   *
      *            DCLGEN                                              *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#     DATE        PROG#     DESCRIPTION                      *
      * ------- ----------- --------- ---------------------------------*
      * TO07614 24 Dec 2008 E404GRK   GENERATED                        *
      *                                                                *
      ******************************************************************
           EXEC SQL DECLARE ACT_NOT_FRM TABLE
           (
             CSR_ACT_NBR                     CHAR(9) NOT NULL,
             NOT_PRC_TS                      TIMESTAMP NOT NULL,
             FRM_SEQ_NBR                     SMALLINT NOT NULL,
             FRM_NBR                         CHAR(30) NOT NULL,
             FRM_EDT_DT                      DATE NOT NULL
           ) END-EXEC.
      *****************************************************************
       01  XZH006-ACT-NOT-FRM-ROW.
           10  XZH006-CSR-ACT-NBR             PIC X(9).
           10  XZH006-NOT-PRC-TS              PIC X(26).
           10  XZH006-FRM-SEQ-NBR             PIC S9(4) BINARY.
           10  XZH006-FRM-NBR                 PIC X(30).
           10  XZH006-FRM-EDT-DT              PIC X(10).
