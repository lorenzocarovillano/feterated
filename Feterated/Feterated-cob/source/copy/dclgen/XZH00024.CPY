      ******************************************************************
      * DCLGEN TABLE(CO_TYP)                                           *
      *        LIBRARY(BPI.FED.ROLL3.STAGE.DCLGEN(XZH00024))           *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE CO_TYP TABLE
           ( CO_CD                          CHAR(5) NOT NULL,
             CO_OFS_NBR                     SMALLINT NOT NULL,
             CO_DES                         CHAR(30) NOT NULL,
             ACY_IND                        CHAR(1) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE CO_TYP                             *
      ******************************************************************
       01  DCLCO-TYP.
           10 CO-CD                PIC X(5).
           10 CO-OFS-NBR           PIC S9(4) USAGE COMP.
           10 CO-DES               PIC X(30).
           10 ACY-IND              PIC X(1).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *
      ******************************************************************
