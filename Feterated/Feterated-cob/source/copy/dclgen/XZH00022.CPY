      ******************************************************************
      * DCLGEN TABLE(FRM_REC_OVL)                                      *
      *        LIBRARY(BPI.FED.ROLL3.STAGE.DCLGEN(XZH00022))           *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE FRM_REC_OVL TABLE
           ( FRM_NBR                        CHAR(30) NOT NULL,
             FRM_EDT_DT                     DATE NOT NULL,
             REC_TYP_CD                     CHAR(5) NOT NULL,
             OVL_EDL_FRM_NM                 CHAR(30) NOT NULL,
             ST_ABB                         CHAR(2) NOT NULL,
             OVL_SR_CD                      CHAR(5) NOT NULL,
             ACY_IND                        CHAR(1) NOT NULL,
             SPE_PRC_CD                     CHAR(8)
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE FRM_REC_OVL                        *
      ******************************************************************
       01  DCLFRM-REC-OVL.
           10 FRM-NBR              PIC X(30).
           10 FRM-EDT-DT           PIC X(10).
           10 REC-TYP-CD           PIC X(5).
           10 OVL-EDL-FRM-NM       PIC X(30).
           10 ST-ABB               PIC X(2).
           10 OVL-SR-CD            PIC X(5).
           10 ACY-IND              PIC X(1).
           10 SPE-PRC-CD           PIC X(8).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *
      ******************************************************************
