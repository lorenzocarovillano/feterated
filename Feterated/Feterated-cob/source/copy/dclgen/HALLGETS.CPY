      ******************************************************************
      * DCLGEN TABLE(HAL_ERR_TRAN_SUP_V)                               *
      ******************************************************************
           EXEC SQL DECLARE HAL_ERR_TRAN_SUP_V TABLE
           ( FAIL_ACY_CD                    CHAR(8) NOT NULL,
             APP_NM                         CHAR(10) NOT NULL,
             HETS_ERR_ATN_CD                CHAR(30) NOT NULL,
             HETS_TYP_ERR                   CHAR(30) NOT NULL,
             HETS_ERR_PTY_NBR               SMALLINT NOT NULL,
             HETS_ERR_TXT                   CHAR(100) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE HAL_ERR_TRAN_SUP_V                 *
      ******************************************************************
       01  DCLHALERT-ERR-TRAN-SUP-V.
           10 HETS-FAIL-ACY-CD      PIC X(8).
           10 APP-NM                PIC X(10).
           10 HETS-ERR-ATN-CD       PIC X(30).
           10 HETS-TYP-ERR          PIC X(30).
           10 HETS-ERR-PTY-NBR      PIC S9(4) USAGE COMP.
           10 HETS-ERR-TXT          PIC X(100).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *
      ******************************************************************
