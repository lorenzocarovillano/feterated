      ******************************************************************
      * DCLGEN TABLE(FED_SIC_CODE_V)                                   *
      *        LIBRARY(BPI.FED.ROLL2.STAGE.DCLGEN(MUH00296))           *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE FED_SIC_CODE_V TABLE
           ( TOB_CD                         CHAR(10) NOT NULL,
             CTR_NBR_CD                     SMALLINT NOT NULL,
             TOB_DES                        CHAR(40) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE FED_SIC_CODE_V                     *
      ******************************************************************
       01  DCLFED-SIC-CODE-V.
           10 TOB-CD               PIC X(10).
           10 CTR-NBR-CD           PIC S9(4) USAGE COMP.
           10 TOB-DES              PIC X(40).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *
      ******************************************************************
