      ******************************************************************
      * DCLGEN TABLE(HAL_LOK_OBJ_HDR_V)                                *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE HAL_LOK_OBJ_HDR_V TABLE
           ( TCH_KEY                        CHAR(126) NOT NULL,
             APP_ID                         CHAR(10) NOT NULL,
             LAST_ACY_TS                    TIMESTAMP NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE HAL_LOK_OBJ_HDR_V                  *
      ******************************************************************
       01  DCLHAL-LOK-OBJ-HDR.
           10 HLOH-TCH-KEY                  PIC X(126).
           10 HLOH-APP-ID                   PIC X(10).
           10 HLOH-LAST-ACY-TS              PIC X(26).
