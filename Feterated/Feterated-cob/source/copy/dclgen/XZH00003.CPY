      ******************************************************************
      * DCLGEN TABLE(ACT_NOT_FRM)                                      *
      *        LIBRARY(BPI.FED.ROLL3.STAGE.DCLGEN(XZH00003))           *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE ACT_NOT_FRM TABLE
           ( CSR_ACT_NBR                    CHAR(9) NOT NULL,
             NOT_PRC_TS                     TIMESTAMP NOT NULL,
             FRM_SEQ_NBR                    SMALLINT NOT NULL,
             FRM_NBR                        CHAR(30) NOT NULL,
             FRM_EDT_DT                     DATE NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE ACT_NOT_FRM                        *
      ******************************************************************
       01  DCLACT-NOT-FRM.
           10 CSR-ACT-NBR          PIC X(9).
           10 NOT-PRC-TS           PIC X(26).
           10 FRM-SEQ-NBR          PIC S9(4) USAGE COMP.
           10 FRM-NBR              PIC X(30).
           10 FRM-EDT-DT           PIC X(10).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 5       *
      ******************************************************************
