      **************************************************************
      * XZ0T90K0 - SERVICE CONTRACT COPYBOOK FOR                   *
      *            UOW       : XZ_PREPARE_IMP_NOTIFICATION         *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#      DATE      PRGRMR     DESCRIPTION                  *
      * -------- --------- ---------- -----------------------------*
      * TO07614  18MAR2009 E404DLP    NEW                          *
      *                                                            *
      **************************************************************
           03  XZT9K0-SERVICE-INPUTS.
               05  XZT9KI-CSR-ACT-NBR                  PIC X(09).
               05  XZT9KI-USERID                       PIC X(08).
           03  XZT9K0-SERVICE-OUTPUTS.
               05  XZT9KO-TECHNICAL-KEY.
                   07  XZT9KO-TK-NOT-PRC-TS            PIC X(26).
               05  XZT9KO-CSR-ACT-NBR                  PIC X(09).
