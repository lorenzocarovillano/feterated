      **************************************************************
      * XZ0T90O0 - SERVICE CONTRACT COPYBOOK FOR                   *
      *            UOW : XZ_GET_CNC_XML_REQ_LIST                   *
      *                                                            *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#      DATE      PRGRMR     DESCRIPTION                  *
      * -------- --------- ---------- -----------------------------*
      * TO07614  31MAR2009 E404DAP    NEW                          *
      * TO07614  16APR2009 E404DAP    ADDED NOTIFICATION DATE      *
      * TO07614  04MAY2009 E404DAP    ADDED EMP ID AND TTY COPY IND*
      *                                                            *
      **************************************************************
           03  XZT90O-SERVICE-INPUTS.
               05  XZT9OI-USERID                       PIC X(08).
           03  XZT90O-SERVICE-OUTPUTS.
               05  XZT9OO-TBL-OF-XML-REQ-ROWS.
                   07  XZT9OO-XML-REQ-ROW
                               OCCURS 1000 TIMES.
                       09  XZT9OO-TECHNICAL-KEY.
                           11  XZT9OO-TK-NOT-PRC-TS    PIC X(26).
                       09  XZT9OO-CSR-ACT-NBR          PIC X(09).
                       09  XZT9OO-ACT-NOT-TYP-CD       PIC X(05).
                       09  XZT9OO-NOT-DT               PIC X(10).
                       09  XZT9OO-EMP-ID               PIC X(06).
                       09  XZT9OO-ACT-TYP-CD           PIC X(02).
                       09  XZT9OO-DOC-DES              PIC X(240).
                       09  XZT9OO-TTY-CO-IND           PIC X(01).
