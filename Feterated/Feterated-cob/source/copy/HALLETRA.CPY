      *****************************************************************
      * HALLETRA                                                      *
      * ERROR TRANSALATION COPYBOOK - ONLY USED WITHIN HALLESTO       *
      * USED BY HALOESTO ERROR HANDLING MODULE TO TRANSFER DETAILS    *
      * OF FAILS THRU THE HALLESTO LINKAGE                            *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  08MAY00 LCP        INITIAL CODING
      * 14969     20JUL01 18448      ADDED '88' REQUIRED FOR
      *                              IMPLEMENTATION OF PHASE 2 DYNAMIC
      *                              ENTITLEMENTS (ARCH 2.3, INFRA 2.2)
      * 17238     05NOV01 18448      INFRA 2.3 ENHANCED LOCKING
      * 17241     27FEB02 18448      REMOVAL OF IAP ACCESS.
      * 20070A    28MAR02 18448      ADD '88' FOR LINKAGE CORRUPTION.
      * 20070B                       CONVERT ABEND TO LOGGABLE ERROR.
      * 20752     09APR02 18448      ADD '88'S FOR CORRUPTED NUMERICS.
      * 25168     22JUL02 18448      SUPPORT OF MSG TRANSPORT UTILS.
      * 25169     22JUL02 18448      SUPPORT OF SYNCPOINT PROCESSING.
      * 26151     09SEP02 03539      FLOOD/EXTENDED ERROR PROCESSING.
      * 28729     07JAN03 18448      NEW 88'S FOR MIP ENHANCEMENTS.
      * TS129     07JUN05 E404DNF    NEW 88'S FOR COBOL FRAMEWORK.
      * TS129     05JUL06 E404DNF    ADDED SET ENVIRONMENT ERROR.
      *****************************************************************
                09 ETRA-ACTION-BUFFER REDEFINES EFAL-ACTION-BUFFER.
                   12 ETRA-ERR-ACTION              PIC  X(30).
      ** DB2 ERRORS
                      88 ETRA-DB2-INSERT             VALUE 'INSERT'.
                      88 ETRA-DB2-UPDATE             VALUE 'UPDATE'.
                      88 ETRA-DB2-DELETE             VALUE 'DELETE'.
                      88 ETRA-DB2-SELECT             VALUE 'SELECT'.
                      88 ETRA-DB2-OPEN-CSR           VALUE 'OPEN CSR'.
                      88 ETRA-DB2-FETCH-CSR          VALUE 'FETCH CSR'.
                      88 ETRA-DB2-CLOSE-CSR          VALUE 'CLOSE CSR'.
                      88 ETRA-DB2-POSITION-CSR       VALUE
                                                       'POSITION CSR'.
17238                 88 ETRA-DB2-SET-CURRENT-TS     VALUE
17238                                                  'SET CURRENT TS'.
TS129                 88 ETRA-DB2-SET-ENVIRONMENT
TS129                          VALUE 'SET ENVIRONMENT'.
      ** CICS ERRORS
                      88 ETRA-CICS-READ-TSQ          VALUE 'READ TSQ'.
                      88 ETRA-CICS-UPDATE-TSQ        VALUE 'UPDATE TSQ'.
                      88 ETRA-CICS-WRITE-TSQ         VALUE 'WRITE TSQ'.
                      88 ETRA-CICS-DELETE-TSQ        VALUE 'DELETE TSQ'.
                      88 ETRA-CICS-READ-VSAM         VALUE 'READ VSAM'.
                      88 ETRA-CICS-UPDATE-VSAM       VALUE
                                                       'UPDATE VSAM'.
                      88 ETRA-CICS-WRITE-VSAM        VALUE 'WRITE VSAM'.
26151                 88 ETRA-CICS-DELETE-VSAM       VALUE
26151                                                 'DELETE VSAM'.
                      88 ETRA-CICS-STARTBR-UMT       VALUE
                                                      'STARTBR UMT'.
                      88 ETRA-CICS-READNXT-UMT       VALUE
                                                       'READNXT UMT'.
                      88 ETRA-CICS-ENDBR-UMT         VALUE 'ENDBR UMT'.
                      88 ETRA-CICS-READ-UMT          VALUE 'READ UMT'.
                      88 ETRA-CICS-UPDATE-UMT        VALUE 'UPDATE UMT'.
                      88 ETRA-CICS-WRITE-UMT         VALUE 'WRITE UMT'.
                      88 ETRA-CICS-REWRITE-UMT       VALUE
                                                       'REWRITE UMT'.
                      88 ETRA-CICS-DELETE-UMT        VALUE 'DELETE UMT'.
                      88 ETRA-CICS-LINK              VALUE 'LINK'.
                      88 ETRA-CICS-START             VALUE 'START'.
                      88 ETRA-CICS-XCTL              VALUE 'XCTL'.
                      88 ETRA-CICS-FUNCTION          VALUE 'FUNCTION'.
28729 ***             88 ETRA-CICS-CALL              VALUE 'CALL'.
                      88 ETRA-CICS-RETRIEVE          VALUE
                                                       'CICS RETRIEVE'.
17241                 88 ETRA-CICS-FORMATTIME        VALUE
17241                                                 'CICS FORMATTIME'.
25168                 88 ETRA-CICS-GETMAIN           VALUE
25168                                                 'CICS GETMAIN'.
25168                 88 ETRA-CICS-FREEMAIN          VALUE
25168                                                 'CICS FREEMAIN'.
25168                 88 ETRA-CICS-WEB-EXTRACT       VALUE
25168                                                'CICS WEB EXTRACT'.
25168                 88 ETRA-CICS-WEB-READ          VALUE
25168                                                 'CICS WEB READ'.
25168                 88 ETRA-CICS-WEB-SEND          VALUE
25168                                                 'CICS WEB SEND'.
25168                 88 ETRA-CICS-WEB-RECEIVE       VALUE
25168                                                'CICS WEB RECEIVE'.
25168                 88 ETRA-CICS-WEB-WRITE         VALUE
25168                                                 'CICS WEB WRITE'.
25168                 88 ETRA-CICS-DOC-CREATE
25168                                      VALUE 'CICS DOCUMENT CREATE'.
28729 ** TRANSFER ERRORS
28729                 88 ETRA-COBOL-CALL             VALUE 'COBOL CALL'.
      ** COMMAREA ERRORS
                      88 COMA-FIELD-NOT-VALUED
                                  VALUE 'FIELD NOT VALUED'.
                      88 COMA-MSG-ID-BLANK
                                  VALUE 'MSG ID BLANK'.
                      88 COMA-UOW-NAME-NOT-FILLED
                                  VALUE 'UNIT OF WORK NOT FILLED'.
                      88 COMA-UOW-MESS-BUFF-NOT-FILLED
                                  VALUE 'MESSAGE BUFFER NOT FILLED'.
20752                 88 COMA-REC-SEQ-INVALID
20752                             VALUE 'REC SEQ NOT NUMERIC'.
20752                 88 COMA-TOTAL-MSG-LENGTH-INVALID
20752                             VALUE 'TOTAL MSG LENGTH NOT NUMERIC'.
                      88 COMA-UOW-NUM-CHKS-INVALID
                                  VALUE 'UOW NUM CHUNKS NOT NUMERIC'.
                      88 COMA-UOW-BUFFER-LEN-INVALID
                                  VALUE 'UOW BUFFER LENGTH NOT NUMERIC'.
                      88 COMA-UOW-NAME-BLANK
                                  VALUE 'UOW NAME BLANK'.
                      88 COMA-USERID-BLANK
                                  VALUE 'USERID BLANK'.
                      88 COMA-AUTH-USER-CLIENTID-BLANK
                                  VALUE 'AUTH USER CLIENTID BLANK'.
                      88 COMA-UOW-REQ-MSG-STORE-BLANK
                                  VALUE 'UOW REQ MSG STORE BLANK'.
                      88 COMA-UOW-REQ-SWIT-STORE-BLANK
                                  VALUE 'UOW REQ SWITCHES STORE BLANK'.
                      88 COMA-UOW-RESP-HDR-STORE-BLANK
                                  VALUE 'UOW RESP HEADER STORE BLANK'.
                      88 COMA-UOW-RESP-DATA-STORE-BLANK
                                  VALUE 'UOW RESP DATA STORE BLANK'.
                      88 COMA-UOW-RESP-WARN-STORE-BLANK
                                  VALUE 'UOW RESP WARNINGS STORE BLANK'.
                      88 COMA-UOW-RESP-NLBE-STORE-BLANK
                                  VALUE 'UOW RSP NLBE STORE'.
                      88 COMA-UOW-KEY-REPL-STORE-BLANK
                                  VALUE 'UOW KEY REPLACE STORE BLANK'.
                      88 COMA-DEL-TIME-BLANK
                                  VALUE 'LOKS DEL TIME BLANK'.
                      88 COMA-LOCK-TYPE-BLANK
                                  VALUE 'LOCK TYPE BLANK'.
                      88 COMA-AUDT-BUS-OBJ-NM-BLANK
                                  VALUE 'AUDIT BUS OBJ NAME BLANK'.
                      88 COMA-SESSION-ID-BLANK
                                  VALUE 'SESSION IS BLANK'.
25169                 88 COMA-BAD-SYNCPOINT-FLAG
25169                             VALUE 'INVALID SYNCPOINT FLAG'.
25169                 88 COMA-BAD-REQ-MSG-PROC-FLAG
25169                             VALUE 'INVALID REQ MSG PROC FLAG'.
25169                 88 COMA-BAD-RESP-MSG-PROC-FLAG
25169                             VALUE 'INVALID RESP MSG PROC FLAG'.
      ** BUSINESS PROCESS ERRORS
                      88 BUSP-STORE-NAME-BLANK
                                  VALUE 'STORE NME BLANK'.
                      88 BUSP-UOW-MODULE-BLANK
                                  VALUE 'UOW MOD BLANK'.
                      88 BUSP-UOW-TRANSACT-NOT-FOUND
                                  VALUE 'UOW TRANSACTION ROW NOT FOUND'.
                      88 BUSP-LOCK-ACTION-BLANK
                                  VALUE 'LOCK ACTION BLANK'.
                      88 BUSP-CN-LVL-SEC-IND-BLANK
                                  VALUE 'CN LVL SEC IND BLANK'.
                      88 BUSP-CONT-LVL-SEC-MODULE-BLANK
                                  VALUE 'CNT LVL SEC MOD BLANK'.
                      88 BUSP-INVALID-SEC-MODULE
                                  VALUE 'SECURITY MOD INVALID'.
                      88 BUSP-DATA-PRIV-IND-BLANK
                                  VALUE 'DATA PRIVACY IND BLANK'.
                      88 BUSP-NO-MESS-ROWS-INV
                                  VALUE 'NO MESSAGE ROWS'.
                      88 BUSP-HDR-EXIST-CANT-TELL
                                  VALUE 'HDR EXIST CANT TELL'.
                      88 BUSP-INV-ACTION-CODE
                                  VALUE 'INV ACT CODE'.
                      88 BUSP-INVALID-SEC-ACTION
                                  VALUE 'INVALID SEC ACTION'.
                      88 BUSP-PARENT-MISSING
                                  VALUE 'PARENT MISSING'.
                      88 BUSP-KEY-CHANGED
                                  VALUE 'KEY CHANGED'.
                      88 BUSP-EXI-ROW-NOT-FOUND
                                  VALUE 'EXI ROW NOT FND'.
                      88 BUSP-INVALID-STORE-TYPE
                                  VALUE 'INV STO TYPE'.
                      88 BUSP-INV-PARAM-LEN
                                  VALUE 'INV PARA LEN'.
                      88 BUSP-KEY-AND-LABEL-SUPP
                                  VALUE 'KEY & LAB SUPP'.
                      88 BUSP-INVALID-FUNCTION       VALUE 'INV FUNC'.
                      88 BUSP-UOW-NOT-IN-SUPP-TAB
                                  VALUE 'UOW NOT ON SUP TAB'.
                      88 BUSP-STORE-TYPE-INV
                                  VALUE 'SPEC. STORE TYPE INV'.
                      88 BUSP-UOW-DATA-BUFF-BLANK
                                  VALUE 'UOW DATA BUFF BLANK'.
                      88 BUSP-INVALID-FILTER
                                  VALUE 'UOW FILTER INVALID'.
                      88 BUSP-INVALID-FETCH-CURSOR
                                  VALUE 'INVALID FETCH CURSOR'.
                      88 BUSP-UOW-REQ-UMT-EMPTY
                                  VALUE 'UOW REQ UMT EMPTY'.
                      88 BUSP-UOW-HDR-DATA-MISS-MATCH
                                  VALUE 'UOW HDR DATA MMATCH'.
                      88 BUSP-INVALID-UMT-REC-CNT
                                  VALUE 'INVALID UMT REC CNT'.
                      88 BUSP-DUPL-KEY-GENERATED
                                  VALUE 'DUPL. KEY GENERATED'.
                      88 BUSP-REQUEST-MSG-MISSING
                                  VALUE 'REQ MSG MISSING'.
                      88 BUSP-INVALID-SUB-BUS-OBJ
                                  VALUE 'INV SUB BUS OBJ'.
                      88 BUSP-INVALID-NULL-IND
                                  VALUE 'INV NULL IND'.
                      88 BUSP-INVALID-SIGN
                                  VALUE 'INVALID SIGN'.
                      88 BUSP-INVALID-COLUMN-IND
                                  VALUE 'INV COL IND'.
                      88 BUSP-REQUIRED-FIELD-BLANK
                                  VALUE 'REQUIRED FIELD BLANK'.
                      88 BUSP-INV-KEY-FIELD-CONTENTS
                                  VALUE 'INV KEY FIE CONTS'.
                      88 BUSP-INVALID-SUPPLIED-VALUE
                                  VALUE 'INV SUPPLIED VALUE'.
                      88 BUSP-LIMIT-EXCEEDED
                                  VALUE 'LIMIT EXCEEDED'.
                      88 BUSP-REQD-DATA-NOT-FOUND
                                  VALUE 'REQUIRED DATA NOT FOUND'.
                      88 BUSP-POL-ISSUE-FAILED
                                  VALUE 'POLICY ISSUE FAILED'.
26151                 88 BUSP-TRIGGER-WRITE-FAILED
26151                             VALUE 'TRIGGER WRITE FAILED'.
                      88 BUSP-INV-LINE-OF-BUS
                                  VALUE 'INVALID LINE OF BUSINESS'.
                      88 BUSP-EFF-DATE-OUT-OF-RANGE
                                  VALUE 'EFFECTIVE DATE OUT OF RANGE'.
                      88 BUSP-EXTRNL-VAL-NOTFND
                                  VALUE 'EXTERNAL VALUE NOT FOUND'.
                      88 BUSP-LEGACY-PROCESS-FAILED
                                  VALUE 'FAILURE IN LEGACY PROCESS'.
20070A                88 BUSP-UBOC-LINKAGE-CORRUPTED
20070A                            VALUE 'UBOC LINKAGE CORRUPTED'.
25168                 88 BUSP-INVALID-HTTP-METHOD
25168                             VALUE 'INVALID HTTP METHOD'.
25168                 88 BUSP-INVALID-CONTENT-LEN
25168                             VALUE 'INVALID HTTP CONTENT LENGTH'.
      ** SECURITY CHECKING ERRORS
                      88 SECU-SEC-SYS-ACCESS-DENIED
                                  VALUE 'SEC SYS ACCESS DENIED'.
                      88 SECU-SEC-UOW-ACCESS-DENIED
                                  VALUE 'SEC UOW ACCESS DENIED'.
                      88 SECU-SEC-INVALID-RETURN
                                  VALUE 'SEC INVALID RETURN'.
      ** DATA PRIVACY ERRORS
                      88 DATP-UNKNOWN-ASSOC-TYPE
                                  VALUE 'DATA PRIV UNKNOWN ASSOC TYPE'.
14969                 88 DATP-UNKNOWN-CONTEXT
14969                             VALUE 'DATA PRIV UNKNOWN CONTEXT'.
      ** IAP AND LOCKING ERRORS
                      88 IAP-MAX-SCH-ATTEMPTS-EXCEEDED
                                  VALUE 'MAX DFR ATTEMPTS'.
                      88 IAP-DATE-RETURN-ERROR
                                  VALUE 'DATE ROUTINE ERROR'.
                      88 IAP-FAILED-DFR-ACY-SCHEDULE
                                  VALUE 'FAILED TO SCHED DFR - DEL UMT'.
                      88 IAP-XPIOCHK-CALL-ERROR
                                  VALUE 'FAILURE IN CALL TO XPIOCHK'.
                      88 IAP-TSQ-NAME-GENERATION-ERROR
                                  VALUE 'FAILURE IN TSQNAMES CALL'.
                      88 LOK-BCMOCHK-CALL-ERROR
                                  VALUE 'FAILURE IN CALL TO BCMOCHK'.
17238                 88 LOK-INVALID-LEGACY-RET
17238                             VALUE 'INVALID LEGACY LOCK RETURN'.
20070B** SYSTEM ABEND
20070B                88 ETRA-ABEND-ENCOUNTERED
20070B                            VALUE 'ABEND ENCOUNTERED'.
TS129*** FEDERATED COBOL FRAMEWORK COMMON COPYBOOK ERRORS
TS129                 88 FRAM-OPERATION-NAME-BLANK
TS129                             VALUE 'OPERATION NAME IS BLANK'.
                   12 FILLER                       PIC  X(220).
