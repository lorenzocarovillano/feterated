      ******************************************************************
      *                                                                *
      * XZ0N0002 - ACT_NOT_POL TABLE                                   *
      *            NON-LOGGABLE ERR/WAR MIX CASE MESSAGES              *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#     DATE        PROG#     DESCRIPTION                      *
      * ------- ----------- --------- ---------------------------------*
      * TO07614 30 Sep 2008 E404GRK   GENERATED                        *
      * TO07614 09 Mar 2009 E404DLP   CHANGE ADR-SEQ-NBR TO ADR-ID     *
      *                                                                *
      ******************************************************************
           03  XZN002-ACT-NOT-POL-COLS.
               05  XZN002-TRANS-PROCESS-DT
                               VALUE 'Transaction Processing Date     '
                                                       PIC X(32).
               05  XZN002-CSR-ACT-NBR
                               VALUE 'Csr Act Nbr                     '
                                                       PIC X(32).
               05  XZN002-NOT-PRC-TS
                               VALUE 'Not Prc Ts                      '
                                                       PIC X(32).
               05  XZN002-POL-NBR
                               VALUE 'Pol Nbr                         '
                                                       PIC X(32).
               05  XZN002-POL-TYP-CD
                               VALUE 'Pol Typ Cd                      '
                                                       PIC X(32).
               05  XZN002-POL-PRI-RSK-ST-ABB
                               VALUE 'Pol Pri Rsk St Abb              '
                                                       PIC X(32).
               05  XZN002-NOT-EFF-DT
                               VALUE 'Not Eff Dt                      '
                                                       PIC X(32).
               05  XZN002-POL-EFF-DT
                               VALUE 'Pol Eff Dt                      '
                                                       PIC X(32).
               05  XZN002-POL-EXP-DT
                               VALUE 'Pol Exp Dt                      '
                                                       PIC X(32).
               05  XZN002-POL-DUE-AMT
                               VALUE 'Pol Due Amt                     '
                                                       PIC X(32).
               05  XZN002-NIN-CLT-ID
                               VALUE 'Nin Clt Id                      '
                                                       PIC X(32).
               05  XZN002-NIN-ADR-ID
                               VALUE 'Nin Adr Id                      '
                                                       PIC X(32).
               05  XZN002-WF-STARTED-IND
                               VALUE 'Wf Started Ind                  '
                                                       PIC X(32).
               05  XZN002-POL-BIL-STA-CD
                               VALUE 'Pol Bil Sta Cd                  '
                                                       PIC X(32).
           03  XZN002-ACT-NOT-POL-CTXT.
               05  XZN002-PROCESSING-CONTEXT-TEXT.
                   07  FILLER  value 'the account, '   PIC X(13).
                   07  FILLER  value ' notification '  PIC X(14).
                   07  FILLER  value 'timestamp and '  PIC X(14).
                   07  FILLER  value 'policy number '  PIC X(14).
                   07  FILLER  value 'of: '            PIC X(04).
