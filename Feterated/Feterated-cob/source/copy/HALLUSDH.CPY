      *****************************************************************
      * HALLUSDH                                                      *
      * COMMUNICATION COPYBOOK FOR HALOUDSH - SET DEFAULTS HUB.       *
      *                                                               *
      *****************************************************************
      * SI#       PRGRMR  DATE       DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * 14969     7077    10MAY2001  CREATED FOR DYNAMIC ENTITLEMENTS.*
      *                              (ARCHITECTURE 2.3)               *
      *****************************************************************
           05  USDH-SET-DEFAULTS-ROW.
               08  USDH-BUS-OBJ-NM            PIC X(32).
