      *****************************************************************
      * HALLCIDP                                                      *
      * COMMON INTERFACE TO DATA PRIVACY                              *
      * USED BY BUSINESS DATA OBJECTS TO PASS DATA ROWS FOR           *
      * DATA PRIVACY CHECKING.                                        *
      *****************************************************************
      * SI#       PRGRMR  DATE       DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  PM      31MAY2000  INITIAL CODING                   *
      * 14969     18448   25JUN2001  MADE CHANGES REQUIRED FOR        *
      *                              ENHANCED DATA PRIVACY AND SET    *
      *                              DEFAULTS PROCESSING.             *
      *                              (ARCHITECTURE 2.3)               *
      *****************************************************************
          03  CIDP-PASSED-INFO.
14969         05  CIDP-TABLE-INFO.
14969             07  CIDP-TABLE-NAME            PIC X(32).
14969             07  CIDP-TABLE-ROW-LENGTH      PIC S9(04) COMP.
14969             07  CIDP-TABLE-ROW             PIC X(4963).
14969         05  CIDP-OBJECT-INFO REDEFINES CIDP-TABLE-INFO.
14969             07  CIDP-OBJECT-NAME           PIC X(32).
14969             07  CIDP-OBJECT-ROW-LENGTH     PIC S9(04) COMP.
14969             07  CIDP-OBJECT-ROW            PIC X(4963).

14969**       05  CIDP-TABLE-NAME                PIC X(18).
14969**       05  CIDP-TABLE-ROW                 PIC X(4982).
