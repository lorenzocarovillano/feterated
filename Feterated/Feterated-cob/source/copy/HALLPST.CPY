      ******************************************************************
      * COPYBOOK FOR HAL_UOW_PRC_SEQ_V                                 *
      *                                                                *
      * SI NUMBER  DATE      PROG NBR  DESCRIPTION                     *
      * ---------  --------- --------- ------------------------------- *
      * XXXXXXXXX  MAY 09 00 18448     CREATED                         *
C15776* C15776     14AUG2001 99361     REMOVE PST-OBJ-MODULE COLUMN.   *
      ******************************************************************
           03  PST-UOW-PRC-SEQ.
              05 PST-UOW-NAME         PIC X(32).
              05 PST-UOW-SEQ-NBR      PIC S9(4) USAGE COMP.
              05 PST-ROOT-BUS-OBJ     PIC X(32).
C15776**      05 PST-OBJ-MODULE       PIC X(32).
              05 PST-BRNCH-PROC-TYP   PIC X(1).
                88  PST-EDIT-BRANCH      VALUE 'E'.
                88  PST-DATA-PROC-BRANCH VALUE 'D'.
                88  PST-ADDL-PROC-BRANCH VALUE 'A'.
