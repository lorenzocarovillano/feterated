      *****************************************************************
      * HALLGEUR                                                      *
      * COMMUNICATION COPYBOOK FOR ANY UOW CALL THAT DOES NOT REQUIRE *
      * A DATA RECORD FOR PROCESSING.                                 *
      *  - GENERIC EMPTY UTILITY RECORD -                             *
      *****************************************************************
      * SI#       PRGRMR  DATE       DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * 16323     18448   10SEP2001  CREATED.                         *
      *                                                               *
      *****************************************************************
           05  GEUR-GEN-EMPTY-DATA-ROW.
               08  GEUR-DUMMY-DATA-FIELD      PIC X(32).
