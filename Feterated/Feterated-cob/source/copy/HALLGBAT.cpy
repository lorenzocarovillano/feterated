      ******************************************************************
      * DCLGEN TABLE(HAL_BO_AUD_TGR_V)                                 *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        QUOTE                                                   *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE HAL_BO_AUD_TGR_V TABLE
           ( UOW_NM                         CHAR(32) NOT NULL,
             HBAT_BUS_OBJ_NM                CHAR(32) NOT NULL,
             HBAT_TGR_IND                   CHAR(1) NOT NULL,
             HBAT_MODULE_NM                 CHAR(8) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE HAL_BO_AUD_TGR_V                   *
      ******************************************************************
       01  DCLHAL-BO-AUD-TGR-V.
           10 HBAT-UOW-NM          PIC X(32).
           10 HBAT-BUS-OBJ-NM      PIC X(32).
           10 HBAT-TGR-IND         PIC X(1).
           10 HBAT-MODULE-NM       PIC X(8).
