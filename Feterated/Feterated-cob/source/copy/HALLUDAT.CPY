      **************************************************************
      * HALLUDAT                                                   *
      * UNIT OF WORK RESPONSE DATA UMT                             *
      * THIS COPYBOOK IS USED BY HALOMCM AND THE BUSINESS OBJECTS  *
      * TO READ/WRITE FROM/TO THE UOW RESPONSE DATA UMT.           *
      **************************************************************
      * MAINTENANCE LOG
      *
      * SI#      DATE      PRGMR     DESCRIPTION
      * -------- --------- --------- -------------------------------
      * SAVANNAH 11JAN00   18448     CREATED.
      * 28068    21NOV02   18448     INCREASE REC SEQ NUMBER.
      **************************************************************
           03 UDAT-COMMON.
              05  UDAT-KEY.
                  10  UDAT-ID                        PIC X(32).
                  10  UDAT-BUS-OBJ-NM                PIC X(32).
28068             10  UDAT-REC-SEQ                   PIC 9(05).
28068****         10  UDAT-REC-SEQ                   PIC 9(03).
              05  UDAT-UOW-BUFFER-LENGTH             PIC 9(04).
              05  UDAT-UOW-MESSAGE-BUFFER.
                  10 UDAT-UOW-BUS-OBJ-NM             PIC X(32).
                  10 UDAT-UOW-BUS-OBJ-DATA           PIC X(5000).
