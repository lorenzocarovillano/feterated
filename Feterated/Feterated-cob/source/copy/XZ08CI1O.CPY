      **************************************************************
      * XZ08CI1O - SERVICE CONTRACT COPYBOOK FOR                   *
      *            UOW        :PolicyInformation                   *
      *            OPERATIONS :getCncNoticeEligibleAddlInterestList*
      *                                                            *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#      DATE       PRGRMR     DESCRIPTION                 *
      * -------- ---------- ---------- ----------------------------*
      * 20163    08/24/2019  E404TJJ    NEW                         *
      *                                                            *
      **************************************************************
           03  XZ08CO-SERVICE-OUTPUTS.
               05  XZ08CO-RET-MSG.
                   07  XZ08CO-RET-CD                   PIC S9(04).
                       88  XZ08CO-RC-OK
                               VALUE +0000.
                       88  XZ08CO-RC-WARNING
                               VALUE +0100.
                       88  XZ08CO-RC-SYSTEM-ERROR
                               VALUE +0200.
                       88  XZ08CO-RC-FAULT
                               VALUE +0300.
                   07  XZ08CO-ERR-LIS.
                       09  XZ08CO-ERR-MSG
                                       OCCURS 10 TIMES PIC X(500).
                   07  XZ08CO-WNG-LIS.
                       09  XZ08CO-WNG-MSG
                                       OCCURS 10 TIMES PIC X(500).
               05  XZ08CO-ADDL-ITS-INFO
                                       OCCURS 250 TIMES.
                   07  XZ08CO-CTC-ID                   PIC X(20).
                   07  XZ08CO-CTC-NM.
                       09  XZ08CO-CN-DSP-NM            PIC X(120).
                       09  XZ08CO-CN-SRT-NM            PIC X(60).
                       09  XZ08CO-CN-CMP-NM            PIC X(60).
                       09  XZ08CO-CN-LST-NM            PIC X(60).
                       09  XZ08CO-CN-FST-NM            PIC X(30).
                       09  XZ08CO-CN-MDL-NM            PIC X(30).
                       09  XZ08CO-CN-PFX               PIC X(04).
                       09  XZ08CO-CN-SFX               PIC X(04).
                   07  XZ08CO-ADR.
                       09  XZ08CO-ADR-ID               PIC X(20).
                       09  XZ08CO-ADR-SEQ-NBR          PIC S9(05).
                       09  XZ08CO-ADR-1                PIC X(45).
                       09  XZ08CO-ADR-2                PIC X(45).
                       09  XZ08CO-CIT-NM               PIC X(30).
                       09  XZ08CO-CTY-NM               PIC X(30).
                       09  XZ08CO-ST.
                           11  XZ08CO-ST-ABB           PIC X(03).
                           11  XZ08CO-ST-NM            PIC X(25).
                       09  XZ08CO-PST-CD               PIC X(13).
                       09  XZ08CO-RLT-TYP.
                           11  XZ08CO-RT-CD            PIC X(04).
                           11  XZ08CO-RT-DES           PIC X(35).
                       09  XZ08CO-POL-LVL-IND          PIC X(01).
               05  XZ08C0-ERR-INF.
                   07  XZ08C0-ERR-IND                  PIC X(30).
                       88  XZ08C0-EI-NO-ERR
                               VALUE 'NoError'.
                       88  XZ08C0-EI-SOAP-FAULT
                               VALUE 'SOAPFault'.
                   07  XZ08C0-FAULT-CD                 PIC X(30).
                   07  XZ08C0-FAULT-STRING             PIC X(256).
                   07  XZ08C0-FAULT-ACTOR              PIC X(256).
                   07  XZ08C0-FAULT-DETAIL             PIC X(256).
                   07  XZ08C0-NON-SOAP-ERR-TXT         PIC X(256).
