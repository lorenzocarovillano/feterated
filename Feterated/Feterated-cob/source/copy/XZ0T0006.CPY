      **************************************************************
      * XZ0T0006 - SERVICE CONTRACT COPYBOOK FOR OPERATION         *
      *            XZ_MAINTAIN_ACT_NOT                             *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#      DATE      PRGRMR     DESCRIPTION                  *
      * -------- --------- ---------- -----------------------------*
      * TO07614  30SEP2008 E404GCL    NEW                          *
      * TO07614  04FEB2009 E404DLP    ADDED ADD-CNC-DAY FIELD      *
      * TO07614  09MAR2009 E404DLP    CHANGE ADR-SEQNBR TO ADR-ID  *
      * TO07614  16MAR2009 E404KXS    CHANGE PRT-CPL-TS TO         *
      *                               STA-MDF-TS AND UPDATE PDC_NM *
P02500* PP02500  09AUG2012 E404BPO    CHANGED CER-TRM-CD TO        *
P02500*                               CER-HLD-NOT-IND              *
      * 20163.20 13JUL2018 E404DMW    UPDATED ADDRESS AND CLIENT   *
      *                               IDS FROM 20 TO 64            *
      **************************************************************
           03  XZT006-SERVICE-INPUTS.
               05  XZT06I-TECHNICAL-KEY.
                   07  XZT06I-TK-NOT-PRC-TS            PIC X(26).
                   07  XZT06I-TK-ACT-OWN-CLT-ID        PIC X(64).
                   07  XZT06I-TK-ACT-OWN-ADR-ID        PIC X(64).
                   07  XZT06I-TK-EMP-ID                PIC X(06).
                   07  XZT06I-TK-STA-MDF-TS            PIC X(26).
                   07  XZT06I-TK-ACT-NOT-STA-CD        PIC X(02).
                   07  XZT06I-TK-SEG-CD                PIC X(03).
                   07  XZT06I-TK-ACT-TYP-CD            PIC X(02).
                   07  XZT06I-TK-ACT-NOT-CSUM          PIC 9(09).
               05  XZT06I-CSR-ACT-NBR                  PIC X(09).
               05  XZT06I-ACT-NOT-TYP-CD               PIC X(05).
               05  XZT06I-NOT-DT                       PIC X(10).
               05  XZT06I-PDC.
                   07  XZT06I-PDC-NBR                  PIC X(05).
                   07  XZT06I-PDC-NM                   PIC X(120).
               05  XZT06I-TOT-FEE-AMT                  PIC S9(08)V9(02).
               05  XZT06I-ST-ABB                       PIC X(02).
P02500         05  XZT06I-CER-HLD-NOT-IND              PIC X(01).
               05  XZT06I-ADD-CNC-DAY                  PIC S9(05).
               05  XZT06I-REA-DES                      PIC X(500).
               05  XZT06I-USERID                       PIC X(08).
           03  XZT006-SERVICE-OUTPUTS.
               05  XZT06O-TECHNICAL-KEY.
                   07  XZT06O-TK-NOT-PRC-TS            PIC X(26).
                   07  XZT06O-TK-ACT-OWN-CLT-ID        PIC X(64).
                   07  XZT06O-TK-ACT-OWN-ADR-ID        PIC X(64).
                   07  XZT06O-TK-EMP-ID                PIC X(06).
                   07  XZT06O-TK-STA-MDF-TS            PIC X(26).
                   07  XZT06O-TK-ACT-NOT-STA-CD        PIC X(02).
                   07  XZT06O-TK-SEG-CD                PIC X(03).
                   07  XZT06O-TK-ACT-TYP-CD            PIC X(02).
                   07  XZT06O-TK-ACT-NOT-CSUM          PIC 9(09).
               05  XZT06O-CSR-ACT-NBR                  PIC X(09).
               05  XZT06O-ACT-NOT-TYP.
                   07  XZT06O-ACT-NOT-TYP-CD           PIC X(05).
                   07  XZT06O-ACT-NOT-TYP-DESC         PIC X(35).
               05  XZT06O-NOT-DT                       PIC X(10).
               05  XZT06O-PDC.
                   07  XZT06O-PDC-NBR                  PIC X(05).
                   07  XZT06O-PDC-NM                   PIC X(120).
               05  XZT06O-TOT-FEE-AMT                  PIC S9(08)V9(02).
               05  XZT06O-ST-ABB                       PIC X(02).
P02500         05  XZT06O-CER-HLD-NOT-IND              PIC X(01).
               05  XZT06O-ADD-CNC-DAY                  PIC S9(05).
               05  XZT06O-REA-DES                      PIC X(500).
