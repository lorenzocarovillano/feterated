      *****************************************************************
      * HALLESTO                                                      *
      * INTERMEDIATE ERROR STORAGE COPYBOOK                           *
      * USED BY AQ2 MODULES TO WRITE ERROR INFO TO INTERMEDIATE       *
      * ERROR STORAGE.  USED BY HALOEREC TO RETRIEVE ERROR INFO,      *
      * INVOKE APPROPRIATE ERROR RECORDING SQL, AND TO DETERMINE THE  *
      * NEED FOR ERROR ASSISTANCE.                                    *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  31MAR00 18448      INITIAL CODING                   *
      * 17241     20DEC01 18448      REMOVE REFERENCES TO IAP.        *
      * 26151     09SEP02 03539      FLOOD/EXTENDED ERROR PROCESSING  *
      * 28068     21NOV02 18448      INCREASE REC SEQ NUMBER.
      *****************************************************************
      ** ERROR LOGGING INFORMATION
          03 ESTO-STORE-INFO.
             05 ESTO-INPUT-KEY.
                07 ESTO-STORE-ID                   PIC X(32).
                07 ESTO-RECORDING-LEVEL            PIC X(32).
                   88  ESTO-MAIN-DRVR-LEVEL
                                           VALUE 'HAL_ERR_LOG_MDRV_V'.
                   88  ESTO-MSG-CNTL-LEVEL
                                           VALUE 'HAL_ERR_LOG_MCM_V'.
                   88  ESTO-FAILURE-LEVEL
                                           VALUE 'HAL_ERR_LOG_FAIL_V'.
28068           07 ESTO-ERR-SEQ-NUM                PIC 9(05).
28068****       07 ESTO-ERR-SEQ-NUM                PIC 9(03).
             05 ESTO-CALL-ETRA-SW                  PIC X.
                88 ESTO-NO-FAILURE-LOG-ONLY          VALUE 'L'.
                88 ESTO-FAILURE-ROW-TYPE             VALUE 'F'.
             05 ESTO-DETAIL-BUFFER                 PIC X(1100).
CTU********     COPY HALLEDTL.
      *****************************************************************
      * HALLEDTL                                                      *
      * ERROR DETAIL BUFFER REDEFINES FOR HALLESTO COPYBOOK. THIS WILL*
      * ALLOW MOVEMENT OF SPECIFIC DETAIL ITEMS FOR INSERT INTO THE   *
      * ERROR DB2 TABLES.                                             *
      *****************************************************************
      * SI NUM    DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  05JUN00 LCP        INITIAL CODING                   *
      *****************************************************************
      **
      ** REDEFINE OF DETAIL BUFFER FOR FAILURE LEVEL LOGGING
              05 EDTL-FAILURE-DETAIL REDEFINES ESTO-DETAIL-BUFFER.
CTU****             COPY HALLEFAL.
      *****************************************************************
      * HALLEFAL                                                      *
      * ERROR LOGGING FAILURE RECORD COPYBOOK.                        *
      * DEFINES RECORD LAYOUT OF THE FAILURE DETAILS.                 *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  23JUN00 18448      INITIAL CODING                   *
      * SAVANNAH  23JUN00 18448      INITIAL CODING                   *
      * 20070B    29MAR02 18448      CONVERT ABENDS TO LOGGABLE ERRORS*
      * 28729     07JAN03 18448      NEW 88'S TO ACCOMMODATE MIP      *
      *                              IMPROVEMENTS.                    *
      *****************************************************************
                 07 EFAL-FAILURE-RECORD-INFO.
                    09 EFAL-FAIL-LVL-GUID            PIC X(32).
                    09 EFAL-FAIL-LVL-ERR-TIMESTAMP   PIC X(26).
                    09 EFAL-FAILED-APPLICATION       PIC X(10).
                       88 EFAL-S3-SAVARCH              VALUE 'SAVARCH'.
                       88 EFAL-S3-SAVANNAH             VALUE 'SAVANNAH'.
                       88 EFAL-S3-HEAL                 VALUE 'HEAL'.
                    09 EFAL-FAILED-PLATFORM          PIC X(10).
                       88 EFAL-MAINFRAME               VALUE
                                                            'MAINFRAME'.
                       88 EFAL-SERVER                  VALUE 'SERVER'.
                    09 EFAL-FAILURE-TYPE             PIC X.
                       88 EFAL-LOGGABLE-WARNING        VALUE 'W'.
                       88 EFAL-BUS-LOGIC-FAILURE       VALUE 'B'.
                       88 EFAL-SYSTEM-ERROR            VALUE 'S'.
                    09 EFAL-FAILED-ACTION-TYPE       PIC X(08).
                       88 EFAL-NO-FAILED-ACTION        VALUE SPACES.
                       88 EFAL-DB2-FAILED              VALUE 'DB2'.
                       88 EFAL-CICS-FAILED             VALUE 'CICS'.
                       88 EFAL-IAP-FAILED              VALUE 'IAP'.
                       88 EFAL-LOCKING-FAILED          VALUE 'LOCKING'.
                       88 EFAL-COMMAREA-FAILED         VALUE 'COMMAREA'.
                       88 EFAL-SECURITY-FAILED         VALUE 'SECURITY'.
                       88 EFAL-DATA-PRIVACY-FAILED     VALUE 'DATAPRIV'.
                       88 EFAL-AUDIT-PROCESS-FAILED    VALUE 'AUDIT'.
                       88 EFAL-BUS-PROCESS-FAILED      VALUE 'BUSPROC'.
20070B                 88 EFAL-ABEND-ENCOUNTERED       VALUE 'ABEND'.
28729                  88 EFAL-TRANSFER-FAILED         VALUE 'TRANSFER'.
                    09 EFAL-FAILED-MODULE            PIC X(32).
                    09 EFAL-ERR-PARAGRAPH            PIC X(30).
                    09 EFAL-ERR-OBJECT-NAME          PIC X(32).
                    09 EFAL-DB2-ERR-SQLCODE-SIGN     PIC X.
                    09 EFAL-DB2-ERR-SQLCODE          PIC 9(10).
                    09 EFAL-CICS-ERR-RESP-SIGN       PIC X.
                    09 EFAL-CICS-ERR-RESP            PIC 9(10).
                    09 EFAL-ERR-COMMENT              PIC X(50).
                  08   EFAL-HAL-ERR-LOG-FAIL-DATA.
                    09 EFAL-UNIT-OF-WORK             PIC X(32).
                    09 EFAL-FAILED-LOCATION-ID       PIC X(32).
                    09 EFAL-DB2-ERR-SQLERRMC         PIC X(70).
                    09 EFAL-CICS-ERR-RESP2-SIGN      PIC X.
                    09 EFAL-CICS-ERR-RESP2           PIC 9(10).
                    09 EFAL-LOGON-USERID             PIC X(32).
                    09 EFAL-SEC-SYS-ID-SIGN          PIC X.
                    09 EFAL-SEC-SYS-ID               PIC 9(10).
                    09 EFAL-OBJ-DATA-KEY             PIC X(200).
                    09 EFAL-ACTION-BUFFER            PIC X(250).
CTU****                  COPY HALLETRA.
      *****************************************************************
      * HALLETRA                                                      *
      * ERROR TRANSALATION COPYBOOK - ONLY USED WITHIN HALLESTO       *
      * USED BY HALOESTO ERROR HANDLING MODULE TO TRANSFER DETAILS    *
      * OF FAILS THRU THE HALLESTO LINKAGE                            *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  08MAY00 LCP        INITIAL CODING
      * 14969     20JUL01 18448      ADDED '88' REQUIRED FOR
      *                              IMPLEMENTATION OF PHASE 2 DYNAMIC
      *                              ENTITLEMENTS (ARCH 2.3, INFRA 2.2)
      * 17238     05NOV01 18448      INFRA 2.3 ENHANCED LOCKING
      * 17241     27FEB02 18448      REMOVAL OF IAP ACCESS.
      * 20070A    28MAR02 18448      ADD '88' FOR LINKAGE CORRUPTION.
      * 20070B                       CONVERT ABEND TO LOGGABLE ERROR.
      * 20752     09APR02 18448      ADD '88'S FOR CORRUPTED NUMERICS.
      * 25168     22JUL02 18448      SUPPORT OF MSG TRANSPORT UTILS.
      * 25169     22JUL02 18448      SUPPORT OF SYNCPOINT PROCESSING.
      * 26151     09SEP02 03539      FLOOD/EXTENDED ERROR PROCESSING.
      * 28729     07JAN03 18448      NEW 88'S FOR MIP ENHANCEMENTS.
      * TS129     07JUN05 E404DNF    NEW 88'S FOR COBOL FRAMEWORK.
      * TS129     05JUL06 E404DNF    ADDED SET ENVIRONMENT ERROR.
      *****************************************************************
                09 ETRA-ACTION-BUFFER REDEFINES EFAL-ACTION-BUFFER.
                   12 ETRA-ERR-ACTION              PIC  X(30).
      ** DB2 ERRORS
                      88 ETRA-DB2-INSERT             VALUE 'INSERT'.
                      88 ETRA-DB2-UPDATE             VALUE 'UPDATE'.
                      88 ETRA-DB2-DELETE             VALUE 'DELETE'.
                      88 ETRA-DB2-SELECT             VALUE 'SELECT'.
                      88 ETRA-DB2-OPEN-CSR           VALUE 'OPEN CSR'.
                      88 ETRA-DB2-FETCH-CSR          VALUE 'FETCH CSR'.
                      88 ETRA-DB2-CLOSE-CSR          VALUE 'CLOSE CSR'.
                      88 ETRA-DB2-POSITION-CSR       VALUE
                                                       'POSITION CSR'.
17238                 88 ETRA-DB2-SET-CURRENT-TS     VALUE
17238                                                  'SET CURRENT TS'.
TS129                 88 ETRA-DB2-SET-ENVIRONMENT
TS129                          VALUE 'SET ENVIRONMENT'.
      ** CICS ERRORS
                      88 ETRA-CICS-READ-TSQ          VALUE 'READ TSQ'.
                      88 ETRA-CICS-UPDATE-TSQ        VALUE 'UPDATE TSQ'.
                      88 ETRA-CICS-WRITE-TSQ         VALUE 'WRITE TSQ'.
                      88 ETRA-CICS-DELETE-TSQ        VALUE 'DELETE TSQ'.
                      88 ETRA-CICS-READ-VSAM         VALUE 'READ VSAM'.
                      88 ETRA-CICS-UPDATE-VSAM       VALUE
                                                       'UPDATE VSAM'.
                      88 ETRA-CICS-WRITE-VSAM        VALUE 'WRITE VSAM'.
26151                 88 ETRA-CICS-DELETE-VSAM       VALUE
26151                                                 'DELETE VSAM'.
                      88 ETRA-CICS-STARTBR-UMT       VALUE
                                                      'STARTBR UMT'.
                      88 ETRA-CICS-READNXT-UMT       VALUE
                                                       'READNXT UMT'.
                      88 ETRA-CICS-ENDBR-UMT         VALUE 'ENDBR UMT'.
                      88 ETRA-CICS-READ-UMT          VALUE 'READ UMT'.
                      88 ETRA-CICS-UPDATE-UMT        VALUE 'UPDATE UMT'.
                      88 ETRA-CICS-WRITE-UMT         VALUE 'WRITE UMT'.
                      88 ETRA-CICS-REWRITE-UMT       VALUE
                                                       'REWRITE UMT'.
                      88 ETRA-CICS-DELETE-UMT        VALUE 'DELETE UMT'.
                      88 ETRA-CICS-LINK              VALUE 'LINK'.
                      88 ETRA-CICS-START             VALUE 'START'.
                      88 ETRA-CICS-XCTL              VALUE 'XCTL'.
                      88 ETRA-CICS-FUNCTION          VALUE 'FUNCTION'.
28729 ***             88 ETRA-CICS-CALL              VALUE 'CALL'.
                      88 ETRA-CICS-RETRIEVE          VALUE
                                                       'CICS RETRIEVE'.
17241                 88 ETRA-CICS-FORMATTIME        VALUE
17241                                                 'CICS FORMATTIME'.
25168                 88 ETRA-CICS-GETMAIN           VALUE
25168                                                 'CICS GETMAIN'.
25168                 88 ETRA-CICS-FREEMAIN          VALUE
25168                                                 'CICS FREEMAIN'.
25168                 88 ETRA-CICS-WEB-EXTRACT       VALUE
25168                                                'CICS WEB EXTRACT'.
25168                 88 ETRA-CICS-WEB-READ          VALUE
25168                                                 'CICS WEB READ'.
25168                 88 ETRA-CICS-WEB-SEND          VALUE
25168                                                 'CICS WEB SEND'.
25168                 88 ETRA-CICS-WEB-RECEIVE       VALUE
25168                                                'CICS WEB RECEIVE'.
25168                 88 ETRA-CICS-WEB-WRITE         VALUE
25168                                                 'CICS WEB WRITE'.
25168                 88 ETRA-CICS-DOC-CREATE
25168                                      VALUE 'CICS DOCUMENT CREATE'.
28729 ** TRANSFER ERRORS
28729                 88 ETRA-COBOL-CALL             VALUE 'COBOL CALL'.
      ** COMMAREA ERRORS
                      88 COMA-FIELD-NOT-VALUED
                                  VALUE 'FIELD NOT VALUED'.
                      88 COMA-MSG-ID-BLANK
                                  VALUE 'MSG ID BLANK'.
                      88 COMA-UOW-NAME-NOT-FILLED
                                  VALUE 'UNIT OF WORK NOT FILLED'.
                      88 COMA-UOW-MESS-BUFF-NOT-FILLED
                                  VALUE 'MESSAGE BUFFER NOT FILLED'.
20752                 88 COMA-REC-SEQ-INVALID
20752                             VALUE 'REC SEQ NOT NUMERIC'.
20752                 88 COMA-TOTAL-MSG-LENGTH-INVALID
20752                             VALUE 'TOTAL MSG LENGTH NOT NUMERIC'.
                      88 COMA-UOW-NUM-CHKS-INVALID
                                  VALUE 'UOW NUM CHUNKS NOT NUMERIC'.
                      88 COMA-UOW-BUFFER-LEN-INVALID
                                  VALUE 'UOW BUFFER LENGTH NOT NUMERIC'.
                      88 COMA-UOW-NAME-BLANK
                                  VALUE 'UOW NAME BLANK'.
                      88 COMA-USERID-BLANK
                                  VALUE 'USERID BLANK'.
                      88 COMA-AUTH-USER-CLIENTID-BLANK
                                  VALUE 'AUTH USER CLIENTID BLANK'.
                      88 COMA-UOW-REQ-MSG-STORE-BLANK
                                  VALUE 'UOW REQ MSG STORE BLANK'.
                      88 COMA-UOW-REQ-SWIT-STORE-BLANK
                                  VALUE 'UOW REQ SWITCHES STORE BLANK'.
                      88 COMA-UOW-RESP-HDR-STORE-BLANK
                                  VALUE 'UOW RESP HEADER STORE BLANK'.
                      88 COMA-UOW-RESP-DATA-STORE-BLANK
                                  VALUE 'UOW RESP DATA STORE BLANK'.
                      88 COMA-UOW-RESP-WARN-STORE-BLANK
                                  VALUE 'UOW RESP WARNINGS STORE BLANK'.
                      88 COMA-UOW-RESP-NLBE-STORE-BLANK
                                  VALUE 'UOW RSP NLBE STORE'.
                      88 COMA-UOW-KEY-REPL-STORE-BLANK
                                  VALUE 'UOW KEY REPLACE STORE BLANK'.
                      88 COMA-DEL-TIME-BLANK
                                  VALUE 'LOKS DEL TIME BLANK'.
                      88 COMA-LOCK-TYPE-BLANK
                                  VALUE 'LOCK TYPE BLANK'.
                      88 COMA-AUDT-BUS-OBJ-NM-BLANK
                                  VALUE 'AUDIT BUS OBJ NAME BLANK'.
                      88 COMA-SESSION-ID-BLANK
                                  VALUE 'SESSION IS BLANK'.
25169                 88 COMA-BAD-SYNCPOINT-FLAG
25169                             VALUE 'INVALID SYNCPOINT FLAG'.
25169                 88 COMA-BAD-REQ-MSG-PROC-FLAG
25169                             VALUE 'INVALID REQ MSG PROC FLAG'.
25169                 88 COMA-BAD-RESP-MSG-PROC-FLAG
25169                             VALUE 'INVALID RESP MSG PROC FLAG'.
      ** BUSINESS PROCESS ERRORS
                      88 BUSP-STORE-NAME-BLANK
                                  VALUE 'STORE NME BLANK'.
                      88 BUSP-UOW-MODULE-BLANK
                                  VALUE 'UOW MOD BLANK'.
                      88 BUSP-UOW-TRANSACT-NOT-FOUND
                                  VALUE 'UOW TRANSACTION ROW NOT FOUND'.
                      88 BUSP-LOCK-ACTION-BLANK
                                  VALUE 'LOCK ACTION BLANK'.
                      88 BUSP-CN-LVL-SEC-IND-BLANK
                                  VALUE 'CN LVL SEC IND BLANK'.
                      88 BUSP-CONT-LVL-SEC-MODULE-BLANK
                                  VALUE 'CNT LVL SEC MOD BLANK'.
                      88 BUSP-INVALID-SEC-MODULE
                                  VALUE 'SECURITY MOD INVALID'.
                      88 BUSP-DATA-PRIV-IND-BLANK
                                  VALUE 'DATA PRIVACY IND BLANK'.
                      88 BUSP-NO-MESS-ROWS-INV
                                  VALUE 'NO MESSAGE ROWS'.
                      88 BUSP-HDR-EXIST-CANT-TELL
                                  VALUE 'HDR EXIST CANT TELL'.
                      88 BUSP-INV-ACTION-CODE
                                  VALUE 'INV ACT CODE'.
                      88 BUSP-INVALID-SEC-ACTION
                                  VALUE 'INVALID SEC ACTION'.
                      88 BUSP-PARENT-MISSING
                                  VALUE 'PARENT MISSING'.
                      88 BUSP-KEY-CHANGED
                                  VALUE 'KEY CHANGED'.
                      88 BUSP-EXI-ROW-NOT-FOUND
                                  VALUE 'EXI ROW NOT FND'.
                      88 BUSP-INVALID-STORE-TYPE
                                  VALUE 'INV STO TYPE'.
                      88 BUSP-INV-PARAM-LEN
                                  VALUE 'INV PARA LEN'.
                      88 BUSP-KEY-AND-LABEL-SUPP
                                  VALUE 'KEY & LAB SUPP'.
                      88 BUSP-INVALID-FUNCTION       VALUE 'INV FUNC'.
                      88 BUSP-UOW-NOT-IN-SUPP-TAB
                                  VALUE 'UOW NOT ON SUP TAB'.
                      88 BUSP-STORE-TYPE-INV
                                  VALUE 'SPEC. STORE TYPE INV'.
                      88 BUSP-UOW-DATA-BUFF-BLANK
                                  VALUE 'UOW DATA BUFF BLANK'.
                      88 BUSP-INVALID-FILTER
                                  VALUE 'UOW FILTER INVALID'.
                      88 BUSP-INVALID-FETCH-CURSOR
                                  VALUE 'INVALID FETCH CURSOR'.
                      88 BUSP-UOW-REQ-UMT-EMPTY
                                  VALUE 'UOW REQ UMT EMPTY'.
                      88 BUSP-UOW-HDR-DATA-MISS-MATCH
                                  VALUE 'UOW HDR DATA MMATCH'.
                      88 BUSP-INVALID-UMT-REC-CNT
                                  VALUE 'INVALID UMT REC CNT'.
                      88 BUSP-DUPL-KEY-GENERATED
                                  VALUE 'DUPL. KEY GENERATED'.
                      88 BUSP-REQUEST-MSG-MISSING
                                  VALUE 'REQ MSG MISSING'.
                      88 BUSP-INVALID-SUB-BUS-OBJ
                                  VALUE 'INV SUB BUS OBJ'.
                      88 BUSP-INVALID-NULL-IND
                                  VALUE 'INV NULL IND'.
                      88 BUSP-INVALID-SIGN
                                  VALUE 'INVALID SIGN'.
                      88 BUSP-INVALID-COLUMN-IND
                                  VALUE 'INV COL IND'.
                      88 BUSP-REQUIRED-FIELD-BLANK
                                  VALUE 'REQUIRED FIELD BLANK'.
                      88 BUSP-INV-KEY-FIELD-CONTENTS
                                  VALUE 'INV KEY FIE CONTS'.
                      88 BUSP-INVALID-SUPPLIED-VALUE
                                  VALUE 'INV SUPPLIED VALUE'.
                      88 BUSP-LIMIT-EXCEEDED
                                  VALUE 'LIMIT EXCEEDED'.
                      88 BUSP-REQD-DATA-NOT-FOUND
                                  VALUE 'REQUIRED DATA NOT FOUND'.
                      88 BUSP-POL-ISSUE-FAILED
                                  VALUE 'POLICY ISSUE FAILED'.
26151                 88 BUSP-TRIGGER-WRITE-FAILED
26151                             VALUE 'TRIGGER WRITE FAILED'.
                      88 BUSP-INV-LINE-OF-BUS
                                  VALUE 'INVALID LINE OF BUSINESS'.
                      88 BUSP-EFF-DATE-OUT-OF-RANGE
                                  VALUE 'EFFECTIVE DATE OUT OF RANGE'.
                      88 BUSP-EXTRNL-VAL-NOTFND
                                  VALUE 'EXTERNAL VALUE NOT FOUND'.
                      88 BUSP-LEGACY-PROCESS-FAILED
                                  VALUE 'FAILURE IN LEGACY PROCESS'.
20070A                88 BUSP-UBOC-LINKAGE-CORRUPTED
20070A                            VALUE 'UBOC LINKAGE CORRUPTED'.
25168                 88 BUSP-INVALID-HTTP-METHOD
25168                             VALUE 'INVALID HTTP METHOD'.
25168                 88 BUSP-INVALID-CONTENT-LEN
25168                             VALUE 'INVALID HTTP CONTENT LENGTH'.
      ** SECURITY CHECKING ERRORS
                      88 SECU-SEC-SYS-ACCESS-DENIED
                                  VALUE 'SEC SYS ACCESS DENIED'.
                      88 SECU-SEC-UOW-ACCESS-DENIED
                                  VALUE 'SEC UOW ACCESS DENIED'.
                      88 SECU-SEC-INVALID-RETURN
                                  VALUE 'SEC INVALID RETURN'.
      ** DATA PRIVACY ERRORS
                      88 DATP-UNKNOWN-ASSOC-TYPE
                                  VALUE 'DATA PRIV UNKNOWN ASSOC TYPE'.
14969                 88 DATP-UNKNOWN-CONTEXT
14969                             VALUE 'DATA PRIV UNKNOWN CONTEXT'.
      ** IAP AND LOCKING ERRORS
                      88 IAP-MAX-SCH-ATTEMPTS-EXCEEDED
                                  VALUE 'MAX DFR ATTEMPTS'.
                      88 IAP-DATE-RETURN-ERROR
                                  VALUE 'DATE ROUTINE ERROR'.
                      88 IAP-FAILED-DFR-ACY-SCHEDULE
                                  VALUE 'FAILED TO SCHED DFR - DEL UMT'.
                      88 IAP-XPIOCHK-CALL-ERROR
                                  VALUE 'FAILURE IN CALL TO XPIOCHK'.
                      88 IAP-TSQ-NAME-GENERATION-ERROR
                                  VALUE 'FAILURE IN TSQNAMES CALL'.
                      88 LOK-BCMOCHK-CALL-ERROR
                                  VALUE 'FAILURE IN CALL TO BCMOCHK'.
17238                 88 LOK-INVALID-LEGACY-RET
17238                             VALUE 'INVALID LEGACY LOCK RETURN'.
20070B** SYSTEM ABEND
20070B                88 ETRA-ABEND-ENCOUNTERED
20070B                            VALUE 'ABEND ENCOUNTERED'.
TS129*** FEDERATED COBOL FRAMEWORK COMMON COPYBOOK ERRORS
TS129                 88 FRAM-OPERATION-NAME-BLANK
TS129                             VALUE 'OPERATION NAME IS BLANK'.
                   12 FILLER                       PIC  X(220).
                    09 EFAL-ETRA-PRIORITY-LEVEL-SIGN PIC X.
                    09 EFAL-ETRA-PRIORITY-LEVEL      PIC 9(05).
                    09 EFAL-ETRA-ERROR-REF           PIC X(10).
                    09 EFAL-ETRA-ERROR-TXT           PIC X(100).
                 07 FILLER                           PIC X(93).
      **
      ** REDEFINE OF DETAIL BUFFER FOR MESSAGE CONTROL LEVEL LOGGING
              05 EDTL-MCM-DETAIL REDEFINES ESTO-DETAIL-BUFFER.
CTU****             COPY HALLEMCM.
      *****************************************************************
      * HALLEMCM                                                      *
      * ERROR LOGGING MESSAGE CONTROL INFORMATION COPYBOOK.           *
      * DEFINES THE MESSAGE CONTROL LEVEL INFORMATION RECORD CAPTURED *
      * BY ERROR LOGGING.                                             *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  22JUN00 18448      INITIAL CODING                   *
      *****************************************************************
                 07 EMCM-MCM-RECORD-INFO.
                    09 EMCM-MCM-LVL-GUID             PIC X(32).
                    09 EMCM-MCM-LVL-ERR-TIMESTAMP    PIC X(26).
                    09 EMCM-MSG-CNTL-MOD             PIC X(32).
                    09 EMCM-PRIMARY-BUS-OBJ          PIC X(32).
                    09 EMCM-STORE-TYPE               PIC X.
                    09 EMCM-PASS-THRU-ACTION         PIC X(20).
      ** UOW LEVEL REQUEST UMT ROW COUNTS OF INCOMING ROWS
                    09 EMCM-UOW-REQ-MSG-ROWS-SIGN    PIC X.
                    09 EMCM-UOW-REQ-MSG-ROWS         PIC 9(06).
                    09 EMCM-UOW-REQ-SWITCH-ROWS-SIGN PIC X.
                    09 EMCM-UOW-REQ-SWITCH-ROWS      PIC 9(06).
      ** UOW LEVEL RESPONSE UMT ROW COUNTS
                    09 EMCM-UOW-RESP-HDR-ROWS-SIGN   PIC X.
                    09 EMCM-UOW-RESP-HDR-ROWS        PIC 9(06).
                    09 EMCM-UOW-RESP-DATA-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-RESP-DATA-ROWS       PIC 9(06).
                    09 EMCM-UOW-RESP-WARN-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-RESP-WARN-ROWS       PIC 9(06).
                    09 EMCM-UOW-RESP-NLBE-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-RESP-NLBE-ROWS       PIC 9(06).
      ** COUNTS OF UOW LEVEL RESPONSE UMT ROWS THAT HAVE BEEN
      ** PROCESSED.
                    09 EMCM-UOW-PROC-HDR-ROWS-SIGN   PIC X.
                    09 EMCM-UOW-PROC-HDR-ROWS        PIC 9(06).
                    09 EMCM-UOW-PROC-DATA-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-PROC-DATA-ROWS       PIC 9(06).
                    09 EMCM-UOW-PROC-WARN-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-PROC-WARN-ROWS       PIC 9(06).
                    09 EMCM-UOW-PROC-NLBE-ROWS-SIGN  PIC X.
                    09 EMCM-UOW-PROC-NLBE-ROWS       PIC 9(06).
      ** MCM LOCKING INFO
                    09 EMCM-LOCK-TYPE                PIC X(10).
                    09 EMCM-UOW-LOCK-ACTION-CD       PIC X.
                    09 EMCM-FLDR-LOKS-DEL-TIME-SIGN  PIC X.
                    09 EMCM-FOLDER-LOKS-DEL-TIME     PIC 9(10).
                    09 EMCM-LOKS-UOW-CONTINUE-SW     PIC X.
      ** MCM SECURITY AND DATA PRIVACY INFO
                    09 EMCM-SEC-AND-DATAPRIV-INFO.
                       12 EMCM-APPLY-DATA-PRIVACY-SW PIC X.
                       12 EMCM-SECURITY-CONTEXT-NI   PIC X.
                       12 EMCM-SECURITY-CONTEXT      PIC X(20).
                       12 EMCM-SEC-AUT-NBR-SIGN      PIC X.
                       12 EMCM-SEC-AUT-NBR           PIC 9(05).
                       12 EMCM-SEC-ASSOC-TYPE-NI     PIC X.
                       12 EMCM-SEC-ASSOCIATION-TYPE  PIC X(08).
                    09 EMCM-DATA-PRIVACY-RETCODE-NI  PIC X.
                    09 EMCM-DATA-PRIVACY-RETCODE     PIC X(10).
      ** MCM AUDIT INFO
                    09 EMCM-AUDIT-PROCESSING-INFO.
                       12 EMCM-APPLY-AUDITS-SW       PIC X.
                       12 EMCM-AUDT-BUS-OBJ-NM-NI    PIC X.
                       12 EMCM-AUDT-BUS-OBJ-NM       PIC X(32).
                       12 EMCM-AUDT-EVENT-DATA-NI    PIC X.
                       12 EMCM-AUDT-EVENT-DATA       PIC X(100).
                    09 EMCM-HAL-EMCM-KEEP-CLR        PIC X.
                 07 FILLER                           PIC X(680).
      **
      ** REDEFINE OF DETAIL BUFFER FOR MAIN DRIVER LEVEL LOGGING
              05 EDTL-MDRV-DETAIL REDEFINES ESTO-DETAIL-BUFFER.
CTU****          COPY HALLEMDR.
      *****************************************************************
      * HALLEMDR                                                      *
      * ERROR LOGGING MAIN DRIVER INFORMATION RECORD COPYBOOK.        *
      * DEFINES THE MAIN DRIVER INFORMATION RECORD CAPTURED BY ERROR  *
      * LOGGING.                                                      *
      *****************************************************************
      * SI#       DATE    PRGMR      DESCRIPTION                      *
      * --------- ------- ---------- ---------------------------------*
      * SAVANNAH  23JUN00 18448      INITIAL CODING                   *
      *****************************************************************
                 07 EMDR-MDRV-INFO.
                    09 EMDR-MDRV-LVL-GUID           PIC X(32).
                    09 EMDR-MDRV-LVL-ERR-TIMESTAMP  PIC X(26).
                    09 EMDR-TERMINAL-ID             PIC X(08).
                    09 EMDR-MSG-TRANSFER-CD         PIC X.
                    09 EMDR-CURRENT-CHUNK-SIGN      PIC X.
                    09 EMDR-CURRENT-CHUNK           PIC 9(05).
                    09 EMDR-DEL-STORE-SW            PIC X.
                    09 EMDR-RETURN-WARNS-SW         PIC X.
                    09 EMDR-TOTAL-MSG-LENGTH-SIGN   PIC X.
                    09 EMDR-TOTAL-MSG-LENGTH        PIC 9(10).
                    09 EMDR-CURRENT-BUFFER-LEN-SIGN PIC X.
                    09 EMDR-CURRENT-BUFFER-LENGTH   PIC 9(10).
                    09 EMDR-TOTAL-NBR-CHUNKS-SIGN   PIC X.
                    09 EMDR-TOTAL-NBR-CHUNKS        PIC 9(05).
                 07 FILLER                          PIC X(997).
      ** RETURN INFORMATION TO CALLING MODULE.
          03 ESTO-RETURN-INFO.
             05 ESTO-OUTPUT.
                07 ESTO-ERR-STORE-RETURN-CD       PIC 9.
                   88 ESTO-TRAN-AND-STORAGE-OK      VALUE 0.
                   88 ESTO-ERR-TRANS-FAILED         VALUE 1.
                   88 ESTO-ERR-STORAGE-FAILED       VALUE 2.
                07 ESTO-ERR-STORE-DETAIL-CD       PIC 99.
                   88 NO-ERROR                      VALUE 00.
                   88 ESTO-FAILED-APP-UNKNOWN       VALUE 01.
                   88 ESTO-STORE-ID-EMPTY           VALUE 02.
                   88 ESTO-RECORDING-LEVEL-INVALID  VALUE 03.
                   88 ESTO-RECORDING-LEVEL-EMPTY    VALUE 04.
                   88 ESTO-FAILURE-TYPE-INVALID     VALUE 05.
                   88 ESTO-FAILURE-TYPE-EMPTY       VALUE 06.
                   88 ESTO-FAILURE-ACTION-INVALID   VALUE 07.
                   88 ESTO-FAILURE-ACTION-NOT-FILLED
                                                    VALUE 08.
                   88 ESTO-DETAIL-BUFFER-NOT-FILLED VALUE 09.
                   88 ESTO-CALL-ETRA-SW-NOT-SET     VALUE 10.
                   88 ESTO-DET-LVL-GUID-NOT-FILLED  VALUE 11.
                   88 ESTO-FAILED-PLTF-NOT-FILLED   VALUE 12.
                   88 ESTO-FAILED-MOD-NOT-FILLED    VALUE 13.
                   88 ESTO-ERR-PARA-NOT-FILLED      VALUE 14.
                   88 ESTO-ERR-COMM-NOT-FILLED      VALUE 15.
                   88 ESTO-UOW-NAME-NOT-FILLED      VALUE 16.
                   88 ESTO-USERID-NOT-FILLED        VALUE 17.
                   88 ESTO-ETRA-RETURN-DATA-INVALID VALUE 18.
                   88 ESTO-CALL-HALOETRA-FAILED     VALUE 19.
                   88 ESTO-READ-VSAM-FAILED         VALUE 20.
                   88 ESTO-STARTBR-VSAM-FAILED      VALUE 21.
                   88 ESTO-READNXT-VSAM-FAILED      VALUE 22.
                   88 ESTO-ENDBR-VSAM-FAILED        VALUE 23.
                   88 ESTO-WRITE-VSAM-FAILED        VALUE 24.
17241****          88 ESTO-XPIODAT-CALL-FAILED      VALUE 25.
17241              88 ESTO-FORMATTIME-CALL-FAILED   VALUE 25.
                   88 ESTO-ACTION-NOT-FILLED        VALUE 26.
                   88 ESTO-DB2-SQLCODE-NOT-FILLED   VALUE 27.
                   88 ESTO-CICS-ERR-RESP-NOT-FILLED VALUE 28.
                   88 ESTO-SUP-TAB-UNIQUE-DB2-ERR   VALUE 29.
                   88 ESTO-SUP-TAB-DEFAULT-NOT-FND  VALUE 30.
                   88 ESTO-SUP-TAB-NON-UQUE-DB2-ERR VALUE 31.
                   88 ESTO-WRITE-DETAIL-UMT-FAILED  VALUE 32.
26151              88 ESTO-STARTBR-ALT-IDX-FAILED   VALUE 33.
26151              88 ESTO-WRITE-DET-VSAM-FAILED    VALUE 34.
                07 ESTO-ERR-RESP-CD               PIC -Z(08)9.
                07 ESTO-ERR-RESP2-CD              PIC -Z(08)9.
                07 ESTO-ERR-SQLCODE               PIC -Z(08)9.
                07 ESTO-ERR-SQLERRMC              PIC  X(70).
                07 ESTO-ERR-FLOOD-IND             PIC X.
                   88 ESTO-NOT-ERROR-FLOOD          VALUE 'N'.
                   88 ESTO-ERROR-FLOOD              VALUE 'Y'.
                07 FILLER                         PIC X(50).
