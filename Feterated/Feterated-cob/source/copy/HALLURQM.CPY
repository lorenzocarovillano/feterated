      **************************************************************
      * HALLURQM                                                   *
      * UNIT OF WORK REQUEST MESSAGE UMT                           *
      * THIS COPYBOOK IS USED BY HALOMCM AND THE BUSINESS OBJECTS  *
      * TO READ/WRITE FROM/TO THE UOW REQUEST MESSAGE UMT.         *
      **************************************************************
      * MAINTENANCE LOG
      *
      * SI#      DATE      PRGMR     DESCRIPTION
      * -------- --------- --------- -------------------------------
      * SAVANNAH 11JAN00   18448     CREATED.
      * 26151    21NOV02   18448     ACCOMMODATE CLEANUP OF ERRORS.
      * 28068    21NOV02   18448     INCREASE REC SEQ NUMBER.
      **************************************************************
           03  URQM-COMMON.
              05  URQM-KEY.
26151           07  URQM-PARTIAL-KEY.
                    09  URQM-ID                    PIC X(32).
                    09  URQM-BUS-OBJ               PIC X(32).
28068           07  URQM-REC-SEQ                   PIC 9(05).
28068****       10  URQM-REC-SEQ                   PIC 9(03).
              05  URQM-UOW-BUFFER-LENGTH           PIC 9(04).
              05  URQM-UOW-MESSAGE-BUFFER.
                07 URQM-MSG-HDR.
      *** UOW MESSAGE DATA HEADER FOR DATA ROWS IN THE INCOMING
      *** MESSAGE AND THE UOW MESSAGE UMT.
                  09  URQM-MSG-BUS-OBJ-NM          PIC X(32).
                  09  URQM-ACTION-CODE             PIC X(20).
CTU*******            COPY HALLUACT.
      *****************************************************************
      ** FILE NAME: HALLUACT                                         **
      ** USE:  CONTAINS THE VALID UOW ACTIONS.  INCLUDED IN HALLUBOC **
      **       AND HALLURQM.                                         **
      *****************************************************************
      * SI#      DATE    PRGRMR     DESCRIPTION                       *
      * -------- ------- ---------- ----------------------------------*
      * SAVANNAH 12MAY00 18448      CREATED.                          *
      * 14969    11MAY01 07077      ADDED 'SET_DEFAULTS' TYPE ACTION  *
      *                             FOR DYNAMIC ENTITLEMENTS.         *
      *                             (ARCHITECTURE 2.3)                *
      * 15129    20JUL01 18448      UOW ACTION ENHANCEMENT FOR        *
      *                             INFRASTRUCTURE 2.2 INCLUDING      *
      *                             ADDITION OF UWS REQUIRED ACTION.  *
      * 17543F   28JAN02 18448      UOW ACTION ENHANCEMENT FOR        *
      *                             BUS FRAMEWORKS 2.3 INCLUDING      *
      *                             ADDITION OF MORE ACTIONS REQUIRED *
      *                             BY CLIENT AND UWS.                *
      * 17543P   25FEB02 18448      ADD NEW FETCH FUNCTION THAT WILL  *
      *                             BYPASS HIERARCHY PROCESSING FOR   *
      *                             RECURSIVE PARENT-CHILD            *
      *                             RELATIONSHIPS.                    *
      * 24371    28JUN02 18448      ADD NEW ACTION FOR UWS PROCESSING.*
      * 24864    25JUN02 18448      DELETE ORPHANED DETAILS DURING    *
      *                             UNATTENDED LOCK PURGE.            *
      * 25389    19AUG02 18448      PROVIDE BYPASS-SIMPLE-EDIT-STEP   *
      *                             ACTION FOR USE DURING SPECIALIZED *
      *                             CASES OF LEGACY DATA PORTING. THIS*
      *                             ACTION SHOULD NEVER BE USED BY    *
      *                             STANDARD S3+ UOWS. FAILURE WILL   *
      *                             RESULT.                           *
      * 28687    06JAN03 18448      ADD NEW UWS ACTION PER CASE 26196.*
      * 30643    21FEB03 18448      ADD NEW INFRASTRUCTURE ACTION     *
      * 31091    16APR03 18448      ADDED NEW ACTION CODE FOR DELETE  *
      *                             IN RENEWAL CHANGE.                *
      * 31754    11SEP03 18448      NEW ACTION FOR UPDATING SCRIPTS   *
      * 35631    29OCT03 18448      ADD NEW ACTION CODE FOR POLICY    *
      *                             ADMIN, PURGE-OOS-REQUEST.         *
      * YJ249   05/17/07 E404SXB    ADDED 88 FOR FETCH-AS-OF-NO-ADJ-REQ*
      *****************************************************************
                 88 NO-PASS-THRU-ACTION         VALUE SPACES.
                 88 FETCH-DATA-REQUEST          VALUE 'FETCH'.
30643            88 FETCH-ALL-DATA-REQUEST      VALUE 'FETCH_ALL'.
17543P           88 FETCH-AND-SKIP-HIER-REQUEST VALUE 'FETCH_SKIP_HIER'.
                 88 FETCH-PENDING-DATA-REQUEST  VALUE 'FETCH_PENDING'.
                 88 FETCH-WITH-EXACT-KEY-REQUEST
                                                VALUE 'FETCH_EXACT_KEY'.
                 88 FETCH-WITH-EXACT-KEY-ISS-REQ
                                                VALUE 'FETCH_EXACT_ISS'.
                 88 FETCH-ISSUED-ROWS-ONLY-REQ  VALUE
                                                    'FETCH_ISSUED_ROWS'.
                 88 FETCH-WITH-EXACT-KEY-PND-REQ VALUE
                                                    'FETCH_EXT_KEY_PND'.
17543F           88 FETCH-PRIORITY               VALUE
17543F                                              'FETCH_PRIORITY'.
                 88 FETCH-AS-OF-NO-ADJ-REQ   VALUE 'FETCH_AS_OF_NO_ADJ'.
                 88 UPDATE-DATA-REQUEST         VALUE 'UPDATE'.
                 88 UPDATE-AND-RETURN-DATA-REQUEST
                                                VALUE 'UPDATE_&_RETURN'.
                 88 INSERT-DATA-REQUEST         VALUE 'INSERT'.
                 88 INSERT-AND-RETURN-DATA-REQUEST
                                                VALUE 'INSERT_&_RETURN'.
                 88 CHANGE-DATA-REQUEST         VALUE 'CHANGE'.
                 88 CHANGE-AND-RETURN-DATA-REQUEST
                                                VALUE 'CHANGE_&_RETURN'.
                 88 CORRECT-DATA-REQUEST        VALUE 'CORRECT'.
                 88 CORRECT-AND-RETURN-DATA-REQ VALUE
                                                  'CORRECT_&_RETURN'.
31754            88 REPLACE-AND-RETURN-REQUEST  VALUE
31754                                             'REPLACE_&_RETURN'.
                 88 DELETE-DATA-REQUEST         VALUE 'DELETE'.
                 88 CASCADING-DELETE            VALUE
                                                   'CASCADING_DELETE'.
                 88 FILTER-ONLY-REQUEST         VALUE 'FILTER'.
                 88 SIMPLE-EDIT-AND-PRIM-KEYS   VALUE 'SIMPLE_EDIT'.
25389            88 BYPASS-SIMPLE-EDIT-STEP
25389                                       VALUE 'BYPASS_SIMPLE_EDITS'.
                 88 IGNORE-REQUEST              VALUE 'IGNORE'.
                 88 IGNORE-AND-RETURN-REQUEST   VALUE 'IGNORE_&_RETURN'.
                 88 SECURITY-RETRIEVAL-REQUEST  VALUE 'SEC_RETRIEVAL'.
24864            88 INFRA-UNATTENDED-LOCK-PURGE VALUE
24864                                            'UNATTD_LOCK_PURGE'.
                 88 ACTIVATE-PENDING-ROWS-REQUEST
                                            VALUE 'ACTIVATE_PENDING'.
                 88 FETCH-ALL-QUOTES        VALUE 'FETCH_ALL_QUOTES'.
14969            88 SET-DEFAULTS-REQUEST        VALUE 'SET_DEFAULTS'.
15129            88 ACCEPT-QUOTE-REQUEST        VALUE 'ACCEPT_QUOTE'.
15129            88 PURGE-PENDING-REQUEST       VALUE 'PURGE_PENDING'.
17543F           88 COPY-DATA-REQUEST           VALUE 'COPY'.
17543F           88 SET-POLICY-OOS              VALUE 'SET_POLICY_OOS'.
17543F           88 POLICY-CHANGE-REQUEST       VALUE 'POLICY_CHANGE'.
17543F           88 REN-CHG-FETCH-EXACT-KEY     VALUE 'RCHG_FETCH_EXCT'.
17543F           88 REN-CHG-FETCH-DATA-REQUEST  VALUE 'RCHG_FETCH'.
17543F           88 REN-CHG-UPDATE-DATA-REQUEST VALUE 'RCHG_UPDATE'.
31091            88 REN-CHG-DELETE-DATA-REQUEST VALUE 'RCHG_DELETE'.
17543F           88 AS-OF-FETCH-DATA-REQUEST    VALUE 'AS_OF_FETCH'.
24371            88 RESET-OOOS-TO-PREM          VALUE 'RESET_OOOS'.
28687            88 WRITE-GLOBAL-VALUES         VALUE 'WRITE_GLOBAL'.
35631            88 PURGE-OOS-REQUEST
35631                                         VALUE 'PURGE_OOS_REQUEST'.
                  09  URQM-BUS-OBJ-DATA-LENGTH     PIC 9(04).
                07 URQM-MSG-DATA                   PIC X(5000).
