      ******************************************************************
      ***CSC *  START OF:                          *BUSINESS FRAMEWORK**
      ***CSC *                                     *BUSINESS FRAMEWORK**
      ***CSC *  CONTROL PART OF DATA PRIVACY       *BUSINESS FRAMEWORK**
      ***CSC *  (THIS DOES NOT VARY BETWEEN BDOS)  *BUSINESS FRAMEWORK**
      ***CSC *                                     *BUSINESS FRAMEWORK**
      ***CSC *  VERSION 1.0 JAN. 03, 2002          *BUSINESS FRAMEWORK**
      ***CSC *  18384                              *BUSINESS FRAMEWORK**
      ******************************************************************
      ***********************************************************
      * READ THE HAL_MDU_XRF TABLE FOR THE SET DEFAULT/ DATA    *
      * PRIVACY MODULE AFFILIATED WITH THIS BDO, AND LINK TO IT *
      ***********************************************************
       CSDF-CALL-DATA-PRIVACY.

           MOVE WS-BUS-OBJ-NM TO HBMX-BUS-OBJ-NM.

           EXEC SQL
             SELECT HBMX_DP_DFL_MDU_NM
                INTO :HBMX-DP-DFL-MDU-NM
                FROM HAL_BO_MDU_XRF_V
               WHERE  BUS_OBJ_NM  = :HBMX-BUS-OBJ-NM
                 AND  HBMX_DP_DFL_MDU_NM <> ' '
           END-EXEC.

           EVALUATE SQLCODE
              WHEN ZERO
                  CONTINUE
              WHEN 100
                 SET DPER-DATA-PRIV-CHECK-OK TO TRUE
                 GO TO CSDF-CALL-DATA-PRIVACY-X
              WHEN OTHER
                 SET WS-LOG-ERROR                      TO TRUE
                 SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
                 SET EFAL-DB2-FAILED OF WS-ESTO-INFO   TO TRUE
                 SET ETRA-DB2-SELECT OF WS-ESTO-INFO   TO TRUE
                 MOVE 'HAL_BO_MDU_XRF_V'
                      TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
                 MOVE 'CSDF-CALL-DATA-PRIVACY'
                      TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
                 MOVE 'UNEXPECTED DB2 RETURN CODE'
                      TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
                 PERFORM 9000-LOG-WARNING-OR-ERROR
                 GO TO CSDF-CALL-DATA-PRIVACY-X
           END-EVALUATE.

       EXEC CICS LINK
           PROGRAM  (HBMX-DP-DFL-MDU-NM)
           COMMAREA (UBOC-RECORD)
           LENGTH   (LENGTH OF UBOC-RECORD)
           RESP     (WS-RESPONSE-CODE)
           RESP2    (WS-RESPONSE-CODE2)
       END-EXEC.
 


           EVALUATE WS-RESPONSE-CODE
      * DFHRESP(NORMAL) = 0                       INSERTED BY TRANSLATOR
              WHEN 0
                 CONTINUE
              WHEN OTHER
                 SET WS-LOG-ERROR                      TO TRUE
                 SET EFAL-SYSTEM-ERROR OF WS-ESTO-INFO TO TRUE
                 SET EFAL-CICS-FAILED OF WS-ESTO-INFO  TO TRUE
                 SET ETRA-CICS-LINK OF WS-ESTO-INFO    TO TRUE
                 MOVE HBMX-DP-DFL-MDU-NM
                      TO EFAL-ERR-OBJECT-NAME OF WS-ESTO-INFO
                 MOVE 'CSDF-CALL-DATA-PRIVACY'
                      TO EFAL-ERR-PARAGRAPH OF WS-ESTO-INFO
                 MOVE 'ERROR LINKING TO DP/DEFAULT MODULE'
                      TO EFAL-ERR-COMMENT OF WS-ESTO-INFO
                 PERFORM 9000-LOG-WARNING-OR-ERROR
                 GO TO CSDF-CALL-DATA-PRIVACY-X
           END-EVALUATE.

       CSDF-CALL-DATA-PRIVACY-X.
           EXIT.
