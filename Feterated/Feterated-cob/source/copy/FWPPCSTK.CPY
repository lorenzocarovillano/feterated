      ******************************************************************
      *                                                                *
      * FWPPCSTK - CALLABLE TECHNICAL KEY COPYBOOK FOR:                *
      *                POLICY PROCESSING CALLABLE SERVICES             *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#      DATE        PROG#     DESCRIPTION                     *
      * -------  ----------- --------- --------------------------------*
      * 11824    06/23/2016  E404JAL   NEW COPYBOOK.                   *
      ******************************************************************
           05  :FWTK:-TECHNICAL-KEYS.
               07  :FWTK:-TK-USERID                    PIC X(08).
               07  :FWTK:-TK-PASSWORD                  PIC X(08).
               07  :FWTK:-TK-URI                       PIC X(256).
               07  :FWTK:-TK-FED-TAR-SYS               PIC X(05).
