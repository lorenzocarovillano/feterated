      **************************************************************
      * HALLLODR                                                   *
      **************************************************************
      * HALRLODR (LOCK DRIVER) LINKAGE                             *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#     DATE      PROG#      DESCRIPTION                   *
      * ------- --------- ---------- ------------------------------*
      * NEW     06DEC2000 XXXX       NEW                           *
      * 17238   29OCT2001 18448      INFRA 2.3 LOCKING ENHANCEMENTS*
      *                              REMOVED APP-SPECIFIC 88'S.    *
      * 34504   28AUG2003 18448      DO NOT ALLOW VIRTUAL ROW LEVEL*
      *                              LOCKING ON INSERT REQUEST. AS *
      *                              WITH INSERT&RETURN, MAKE SURE *
      *                              THAT NO PESSI LOCKS EXIST AND *
      *                              IF OPTIS EXIST THEN ZAP THEM. *
      *                              HOWEVER, DO NOT ESTABLISH A   *
      *                              LOCK.  IF A LOCK IS ALREADY IN*
      *                              PLACE, THOUGH, UPDATE THE TIME*
      **************************************************************
           03 HALRLODR-INPUT-LINKAGE.
              05 HALRLODR-FUNCTION          PIC X(32).
                 88 HALRLODR-CREATE-ACCESS-LOCK
                            VALUE 'CREATE_ACCESS_LOCK              '.
17238            88 HALRLODR-CREATE-INSRET-LOCK
17238                       VALUE 'CREATE_INSRET_LOCK              '.
34504            88 HALRLODR-VERIFY-INSERT-ACCESS
34504                       VALUE 'VERIFY_INSERT_ACCESS            '.
                 88 HALRLODR-AUTH-ACCESS-LOCK
                            VALUE 'AUTHENTICATE_ACCESS_LOCK        '.
              05 HALRLODR-TCH-KEY           PIC X(126).
              05 HALRLODR-APP-ID            PIC X(10).
              05 HALRLODR-TABLE-NM          PIC X(18).
              05 HALRLODR-BUS-OBJ-NM        PIC X(32).
