      **************************************************************
      * HALLCCTX  (SECURITY CONTEXT)                               *
      **************************************************************
      * MAINTENANCE LOG                                            *
      *                                                            *
      * SI#     DATE      PROG#      DESCRIPTION                   *
      * ------- --------- ---------- ------------------------------*
      * 20072   28MAR2002  07077     NEW                           *
      **************************************************************
           03 HCTXC-CONTEXT-INFO.
              05 HCTXC-CONTEXT              PIC X(20).
              05 FILLER                     PIC X(32).
