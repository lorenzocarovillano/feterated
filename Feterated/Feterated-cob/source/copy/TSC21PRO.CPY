      ****************************************************************
      * TSC21PRO - PROXY COMMON LOGIC                                *
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR COMMON LOGIC      *
      *        COPYBOOK VERSIONING.                                  *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    06MAR06   E404LJL   INITIAL VERSION                 *
TL0113* TL000113 30JUN09   E404DMA   EXPAND FUNCTIONALITY SUCH THAT  *
      *                              DATA CAN BE PASSED VIA CHANNEL  *
      *                              & CONTAINERS, COMMAREA ONLY, OR *
      *                              REFERENCE STORAGE (POINTERS).   *
      ****************************************************************

TL0113 01  L-CONTRACT-LOCATION                         PIC X(01).

       PROCEDURE DIVISION.
       1000-MAINLINE.
      ****************************************************************
      * CONTROLS MAINLINE PROGRAM PROCESSING                         *
      ****************************************************************

           PERFORM 2000-BEGINNING-HOUSEKEEPING
              THRU 2000-EXIT.

           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 1000-EXIT
           END-IF.

           PERFORM 3000-CALL-FRAMEWORK
              THRU 3000-EXIT.

           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 1000-EXIT
           END-IF.

       1000-EXIT.

           PERFORM 8000-ENDING-HOUSEKEEPING
              THRU 8000-EXIT.

           EXEC CICS
               RETURN
           END-EXEC.
           GOBACK.


       2000-BEGINNING-HOUSEKEEPING.
      ****************************************************************
      * PERFORM INITIALIZATION AND OTHER SETUP TASKS                 *
TL0113* DETERMINE METHOD OF CAPTURING INPUT CONTRACTS TO GET         *
TL0113* ADDRESSABILITY TO THEM.                                      *
      ****************************************************************

TL0113     MOVE CF-PROGRAM-NAME        TO WS-PROGRAM-NAME.

TL0113     PERFORM 2500-CAPTURE-INPUT-CONTRACTS
TL0113        THRU 2500-EXIT.
TL0113     IF PPC-FATAL-ERROR-CODE
TL0113       OR
TL0113        PPC-NLBE-CODE
TL0113         GO TO 2000-EXIT
TL0113     END-IF.

           INITIALIZE DSD-ERROR-HANDLING-PARMS
                      PPC-ERROR-HANDLING-PARMS.
      ****************************************************************
      * FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
      * PROGRAM BEFORE CALLING THE MAIN DRIVER.                      *
      ****************************************************************

           PERFORM 2100-ALLOCATE-FW-MEMORY
              THRU 2100-EXIT.

           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 2000-EXIT
           END-IF.

           PERFORM 2200-GET-USERID
              THRU 2200-EXIT.

       2000-EXIT.
           EXIT.


       2100-ALLOCATE-FW-MEMORY.

      ****************************************************************
      * ALLOCATE THE FRAMEWORK REQUEST AND RESPONSE AREAS            *
      * FOR THIS OPERATION.                                          *
      ****************************************************************

           INITIALIZE MEMORY-ALLOCATION-PARMS.
           MOVE LENGTH OF L-FRAMEWORK-REQUEST-AREA
                                       TO MA-INPUT-DATA-SIZE.

           EXEC CICS GETMAIN
               SET(MA-INPUT-POINTER)
               FLENGTH(MA-INPUT-DATA-SIZE)
               INITIMG(CF-BLANK)
               RESP(WS-RESPONSE-CODE)
               RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2100-ALLOCATE-FW-MEMORY'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2100-EXIT
           END-IF.

           SET ADDRESS OF L-FRAMEWORK-REQUEST-AREA
                                       TO MA-INPUT-POINTER.
           INITIALIZE L-FRAMEWORK-REQUEST-AREA.

           MOVE LENGTH OF L-FRAMEWORK-RESPONSE-AREA
                                       TO MA-OUTPUT-DATA-SIZE.

           EXEC CICS GETMAIN
              SET(MA-OUTPUT-POINTER)
              FLENGTH(MA-OUTPUT-DATA-SIZE)
              INITIMG(CF-BLANK)
              RESP(WS-RESPONSE-CODE)
              RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2100-ALLOCATE-FW-MEMORY'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2100-EXIT
           END-IF.

           SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA
                                       TO MA-OUTPUT-POINTER.
           INITIALIZE L-FRAMEWORK-RESPONSE-AREA.

       2100-EXIT.
           EXIT.


       2200-GET-USERID.

      ****************************************************************
      * GET THE USERID FOR THIS TRANSACTION FROM CICS AND POPULATE   *
      * THE FRAMEWORK LEVEL USERID FIELD.  THIS CAN BE OVERRIDEN BY  *
      * A USER-SUPPLIED USERID IF NECESSARY.                         *
      ****************************************************************

           INITIALIZE CSC-AUTH-USERID.

           EXEC CICS ASSIGN
               USERID(CSC-AUTH-USERID)
           END-EXEC.

       2200-EXIT.
           EXIT.


TL0113 2500-CAPTURE-INPUT-CONTRACTS.
      ****************************************************************
      * DETERMINE IF THE DATA WAS PASSED USING CHANNELS AND          *
      * CONTAINERS, COMMAREA AND REFERENCE STORAGE, OR COMMARE ONLY. *
      * BASED ON THAT, CAPTURE THE PROXY COMMON CONTRACT AND SERVICE *
      * CONTRACT TO BE USED.                                         *
      ****************************************************************

      *    IF THE DATA WAS PASSED USING CHANNELS AND CONTAINERS, THEN
      *     THE COMMAREA WOULD NOT BE ALLOCATED, RESULTING IN A COMMAREA
      *     LENGTH OF ZERO.  IF THE CONTRACT IS IN REFERENCE STORAGE,
      *     THEN THE ONLY THING IN THE COMMAREA SHOULD BE THE PROXY
      *     COMMON COPYBOOK.

           EVALUATE TRUE
               WHEN EIBCALEN = +0
                   PERFORM 2510-CAPTURE-FROM-CHANNEL
                      THRU 2510-EXIT
               WHEN EIBCALEN = LENGTH OF PROXY-PROGRAM-COMMON
                             + LENGTH OF L-SERVICE-CONTRACT-AREA
                   PERFORM 2520-CAPTURE-FROM-COMMAREA
                      THRU 2520-EXIT
               WHEN OTHER
                   PERFORM 2530-CAPTURE-FROM-REF-STORAGE
                      THRU 2530-EXIT
           END-EVALUATE.

TL0113 2500-EXIT.
           EXIT.


TL0113 2510-CAPTURE-FROM-CHANNEL.
      ****************************************************************
      * DATA WAS PASSED USING CHANNELS AND CONTAINERS.  ALLOCATE AND *
      * CAPTURE THE PROXY COMMON CONTRACT FOLLOWED BY THE SERVICE    *
      * CONTRACT.                                                    *
      ****************************************************************

           PERFORM 2511-ALLOCATE-PROXY-CNT
              THRU 2511-EXIT.
           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 2510-EXIT
           END-IF.

           PERFORM 2512-GET-PROXY-COMMON-CNT
              THRU 2512-EXIT.
           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 2510-EXIT
           END-IF.

           PERFORM 2513-ALLOCATE-SERVICE-CONTRACT
              THRU 2513-EXIT.
           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 2510-EXIT
           END-IF.

      *    HAVE THE DEVELOPER SPECIFY THE LOCATION AND LENGTHS OF THE
      *     SERVICE CONTRACT INPUT AND OUTPUT AREAS.

           PERFORM 2514-CAPTURE-SVC-CTT-ATB
              THRU 2514-EXIT.

           PERFORM 2515-GET-SERVICE-CONTRACT
              THRU 2515-EXIT.
           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 2510-EXIT
           END-IF.

TL0113 2510-EXIT.
           EXIT.


TL0113 2511-ALLOCATE-PROXY-CNT.
      ****************************************************************
      * ALLOCATE & SET THE ADDRESSIBILITY OF THE PROXY COMMON CONTRACT
      ****************************************************************

           INITIALIZE MEMORY-ALLOCATION-PARMS.
           MOVE LENGTH OF DFHCOMMAREA  TO MA-INPUT-DATA-SIZE.

           EXEC CICS GETMAIN
               SET(MA-INPUT-POINTER)
               FLENGTH(MA-INPUT-DATA-SIZE)
               INITIMG(CF-BLANK)
               RESP(WS-RESPONSE-CODE)
               RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

      *!   IF AN ISSUE IS ENCOUNTERED HERE, AN ABEND WILL LIKELY OCCUR.
      *!    ALTHOUGH NOT DESIRED, THERE IS NOTHING ELSE THAT CAN BE DONE

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2511-ALLOCATE-PROXY-CNT'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2511-EXIT
           END-IF.

           SET ADDRESS OF DFHCOMMAREA  TO MA-INPUT-POINTER.
           INITIALIZE DFHCOMMAREA.

      *    WITH THE PROXY COMMON CONTRACT ALLOCATED, SPECIFY BOTH THE
      *     SIZE AND LOCATION OF THE INPUT AND OUTPUT AREAS.

           SET WS-PC-INPUT-PTR         TO ADDRESS OF PPC-INPUT-PARMS.
           SET WS-PC-OUTPUT-PTR        TO ADDRESS OF PPC-OUTPUT-PARMS.
           COMPUTE WS-PC-INPUT-LEN = LENGTH OF PPC-INPUT-PARMS.
           COMPUTE WS-PC-OUTPUT-LEN = LENGTH OF PPC-OUTPUT-PARMS.

TL0113 2511-EXIT.
           EXIT.


TL0113 2512-GET-PROXY-COMMON-CNT.
      ****************************************************************
      * CAPTURE THE INPUT PORTION OF THE PROXY COMMON CONTRACT       *
      ****************************************************************

           SET ADDRESS OF L-CONTRACT-LOCATION
                                       TO WS-PC-INPUT-PTR.

           EXEC CICS GET
               CONTAINER  (CF-CN-PROXY-CBK-INP-NM)
               INTO       (L-CONTRACT-LOCATION (1: WS-PC-INPUT-LEN))
               FLENGTH    (WS-PC-INPUT-LEN)
               RESP       (WS-RESPONSE-CODE)
               RESP2      (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2512-GET-PROXY-COMMON-CNT'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2512-EXIT
           END-IF.

TL0113 2512-EXIT.
           EXIT.


TL0113 2513-ALLOCATE-SERVICE-CONTRACT.
      ****************************************************************
      * ALLOCATE & SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT  *
      ****************************************************************

           INITIALIZE MEMORY-ALLOCATION-PARMS.
           MOVE LENGTH OF L-SERVICE-CONTRACT-AREA
                                       TO MA-INPUT-DATA-SIZE.

           EXEC CICS GETMAIN
               SET(MA-INPUT-POINTER)
               FLENGTH(MA-INPUT-DATA-SIZE)
               INITIMG(CF-BLANK)
               RESP(WS-RESPONSE-CODE)
               RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2513-ALLOCATE-SERVICE-CONTRACT'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2513-EXIT
           END-IF.

           SET ADDRESS OF L-SERVICE-CONTRACT-AREA
                                       TO MA-INPUT-POINTER.
           INITIALIZE L-SERVICE-CONTRACT-AREA.

TL0113 2513-EXIT.
           EXIT.


TL0113 2514-CAPTURE-SVC-CTT-ATB.
      ****************************************************************
      * CAPTURE BOTH THE SIZE AND LOCATION OF THE INPUT AND OUTPUT   *
      * AREAS OF THE SERVICE CONTRACT AS SPECIFIED BY THE DEVELOPER  *
      ****************************************************************

           PERFORM 12514-SET-SVC-CTT-ATB
              THRU 12514-EXIT.

TL0113 2514-EXIT.
           EXIT.


TL0113 2515-GET-SERVICE-CONTRACT.
      ****************************************************************
      * CAPTURE THE INPUT PORTION OF THE SERVICE CONTRACT            *
      ****************************************************************

           SET ADDRESS OF L-CONTRACT-LOCATION
                                       TO WS-SC-INPUT-PTR.

           EXEC CICS GET
               CONTAINER  (CF-CN-SERVICE-CBK-INP-NM)
               INTO       (L-CONTRACT-LOCATION (1: WS-SC-INPUT-LEN))
               FLENGTH    (WS-SC-INPUT-LEN)
               RESP       (WS-RESPONSE-CODE)
               RESP2      (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '2515-GET-SERVICE-CONTRACT'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 2515-EXIT
           END-IF.

TL0113 2515-EXIT.
           EXIT.


TL0113 2520-CAPTURE-FROM-COMMAREA.
      ****************************************************************
      * DATA WAS PASSED USING THE COMMAREA ONLY.  SINCE THE          *
      * ENVIRONMENT WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY,   *
      * SET THE ADDRESSIBILITY OF THE SERVICE CONTRACT.              *
      ****************************************************************

      *  SINCE THE SERVICE CONTRACT IS APPENDED TO THE PROXY CONTRACT,
      *   SET THE LOCATION OF THE SERVICE CONTRACT TO BE IMMEDIATELY
      *   AFTER THE PROXY CONTRACT.

           SET WS-CA-PTR               TO
                                        ADDRESS OF PROXY-PROGRAM-COMMON.
           ADD LENGTH OF PROXY-PROGRAM-COMMON
                                       TO WS-CA-PTR-VAL.
           SET ADDRESS OF L-SERVICE-CONTRACT-AREA
                                       TO WS-CA-PTR.

TL0113 2520-EXIT.
           EXIT.


TL0113 2530-CAPTURE-FROM-REF-STORAGE.
      ****************************************************************
      * DATA WAS PASSED USING REFERENCE STORAGE.  SINCE THE CALLER   *
      * WILL BE RESPONSIBLE FOR ALLOCATING THE MEMORY, SET THE       *
      * ADDRESSIBILITY OF THE SERVICE CONTRACT.                      *
      ****************************************************************

           SET ADDRESS OF L-SERVICE-CONTRACT-AREA
                                       TO PPC-SERVICE-DATA-POINTER.

TL0113 2530-EXIT.
           EXIT.


       3000-CALL-FRAMEWORK.
      ****************************************************************
      * PERFORM SET UP FOR AND MAKE THE CALL TO THE FRAMEWORK MAIN   *
      * DRIVER.                                                      *
      ****************************************************************

           PERFORM 3100-SET-UP-INPUT
              THRU 3100-EXIT.

           EXEC CICS LINK
               PROGRAM    (CF-MAIN-DRIVER)
               COMMAREA   (MAIN-DRIVER-DATA)
               RESP       (WS-RESPONSE-CODE)
               RESP2      (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '3000-CALL-FRAMEWORK'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 3000-EXIT
           END-IF.

           IF NOT DSD-NO-ERROR-CODE
               MOVE DSD-ERROR-HANDLING-PARMS
                                       TO PPC-ERROR-HANDLING-PARMS
           END-IF.

           IF PPC-FATAL-ERROR-CODE
             OR
              PPC-NLBE-CODE
               GO TO 3000-EXIT
           END-IF.

           PERFORM 3200-SET-UP-OUTPUT
              THRU 3200-EXIT.

       3000-EXIT.
           EXIT.


       3100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

     ** FRAMEWORK PARAMETERS
           MOVE PPC-OPERATION          TO CSC-OPERATION.
           MOVE CF-UNIT-OF-WORK        TO CSC-UNIT-OF-WORK.
           MOVE CF-REQUEST-MODULE      TO CSC-REQUEST-MODULE.
           MOVE CF-RESPONSE-MODULE     TO CSC-RESPONSE-MODULE.
           MOVE PPC-BYPASS-SYNCPOINT-MDRV-IND
                                       TO DSD-BYPASS-SYNCPOINT-MDRV-IND.

           PERFORM 13100-SET-UP-INPUT
              THRU 13100-EXIT.

       3100-EXIT.
           EXIT.


       3200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           PERFORM 13200-SET-UP-OUTPUT
              THRU 13200-EXIT.

TL0113     IF EIBCALEN = +0
TL0113         PERFORM 3210-OUTPUT-SVC-CONTRACT-CTA
TL0113            THRU 3210-EXIT
TL0113     END-IF.

       3200-EXIT.
           EXIT.


TL0113 3210-OUTPUT-SVC-CONTRACT-CTA.
      ****************************************************************
      * SINCE THE SERVICE CONTRACT WILL NOT BE UPDATED FURTHER FROM  *
      * THIS POINT, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW *
      * CONTAINER TO SEND BACK TO THE CALLER.                        *
      ****************************************************************

           SET ADDRESS OF L-CONTRACT-LOCATION
                                       TO WS-SC-OUTPUT-PTR.

           EXEC CICS PUT
               CONTAINER  (CF-CN-SERVICE-CBK-OUP-NM)
               FROM       (L-CONTRACT-LOCATION (1: WS-SC-OUTPUT-LEN))
               FLENGTH    (WS-SC-OUTPUT-LEN)
               RESP       (WS-RESPONSE-CODE)
               RESP2      (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '3210-OUTPUT-SVC-CONTRACT-CTA'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 3210-EXIT
           END-IF.

TL0113 3210-EXIT.
           EXIT.


       8000-ENDING-HOUSEKEEPING.

      ****************************************************************
      * PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
      ****************************************************************

           EXEC CICS FREEMAIN
               DATA(L-FRAMEWORK-REQUEST-AREA)
               RESP(WS-RESPONSE-CODE)
               RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '8000-FREE-FW-MEMORY-1'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
TL0113         PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
TL0113            THRU 8100-EXIT
               GO TO 8000-EXIT
           END-IF.

           EXEC CICS FREEMAIN
               DATA(L-FRAMEWORK-RESPONSE-AREA)
               RESP(WS-RESPONSE-CODE)
               RESP2(WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '8000-FREE-FW-MEMORY-2'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
TL0113         PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
TL0113            THRU 8100-EXIT
               GO TO 8000-EXIT
           END-IF.

TL0113*    IF USING CHANNELS AND CONTAINERS, DEALLOCATE THE SERVICE
TL0113*     CONTRACT AND THE PROXY CONTRACT.  OTHERWISE, THAT WILL BE
TL0113*     THE RESPONSIBILITY OF THE CALLER AND/OR THE CICS ENVIRONMENT
TL0113
TL0113     IF EIBCALEN = +0
TL0113         CONTINUE
TL0113     ELSE
TL0113         GO TO 8000-EXIT
TL0113     END-IF.
TL0113
TL0113*    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
TL0113*     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
TL0113
TL0113     IF ADDRESS OF L-SERVICE-CONTRACT-AREA NOT = NULLS
TL0113         EXEC CICS FREEMAIN
TL0113             DATA(L-SERVICE-CONTRACT-AREA)
TL0113             RESP(WS-RESPONSE-CODE)
TL0113             RESP2(WS-RESPONSE-CODE2)
TL0113         END-EXEC
TL0113     END-IF.
TL0113
TL0113     IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
TL0113       AND
TL0113        ADDRESS OF DFHCOMMAREA NOT = NULLS
TL0113         SET PPC-FATAL-ERROR-CODE
TL0113                                 TO TRUE
TL0113         MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
TL0113         MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
TL0113         STRING 'Failed module is '
TL0113                WS-PROGRAM-NAME
TL0113                '.  Failed paragraph is '
TL0113                '8000-FREE-FW-MEMORY-3'
TL0113                '.  Failed module SqlCode is 0'
TL0113                '.  Failed module EIBRESP Code is '
TL0113                WS-EIBRESP-DISPLAY
TL0113                '.  Failed module EIBRESP2 Code is '
TL0113                WS-EIBRESP2-DISPLAY
TL0113                '.'  DELIMITED BY SIZE
TL0113                INTO PPC-FATAL-ERROR-MESSAGE
TL0113         END-STRING
TL0113         PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
TL0113            THRU 8100-EXIT
TL0113         GO TO 8000-EXIT
TL0113     END-IF.
TL0113
TL0113     PERFORM 8100-OUTPUT-PROXY-COMMON-CTA
TL0113        THRU 8100-EXIT.
TL0113     IF PPC-FATAL-ERROR-CODE
TL0113       OR
TL0113        PPC-NLBE-CODE
TL0113         GO TO 8000-EXIT
TL0113     END-IF.
TL0113
TL0113*    IT IS POSSIBLE TO GET HERE WITHOUT THE CONTACT BEING
TL0113*     ALLOCATED, SO MAKE SURE IT NEEDS TO BE DECALLOCATED FIRST.
TL0113
TL0113     IF ADDRESS OF DFHCOMMAREA NOT = NULLS
TL0113         EXEC CICS FREEMAIN
TL0113             DATA(DFHCOMMAREA)
TL0113             RESP(WS-RESPONSE-CODE)
TL0113             RESP2(WS-RESPONSE-CODE2)
TL0113         END-EXEC
TL0113     END-IF.
TL0113
TL0113*!   RECORD AN ERROR IF ONE OCCURS AND THE MEMORY IS STILL
TL0113*!    ALLOCATED.  IF IT IS NOT ALLOCATED, THEN WE WOULD RUN INTO
TL0113*!    ADDRESSING ISSUES AND MOST LIKELY ABEND.  EITHER WAY THOUGH,
TL0113*!    THE ERROR WILL NOT GET BACK TO THE CONSUMER.
TL0113
TL0113     IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
TL0113       AND
TL0113        ADDRESS OF DFHCOMMAREA NOT = NULLS
TL0113         SET PPC-FATAL-ERROR-CODE
TL0113                                 TO TRUE
TL0113         MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
TL0113         MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
TL0113         STRING 'Failed module is '
TL0113                WS-PROGRAM-NAME
TL0113                '.  Failed paragraph is '
TL0113                '8000-FREE-FW-MEMORY-4'
TL0113                '.  Failed module SqlCode is 0'
TL0113                '.  Failed module EIBRESP Code is '
TL0113                WS-EIBRESP-DISPLAY
TL0113                '.  Failed module EIBRESP2 Code is '
TL0113                WS-EIBRESP2-DISPLAY
TL0113                '.'  DELIMITED BY SIZE
TL0113                INTO PPC-FATAL-ERROR-MESSAGE
TL0113         END-STRING
TL0113         GO TO 8000-EXIT
TL0113     END-IF.
TL0113
       8000-EXIT.
           EXIT.


TL0113 8100-OUTPUT-PROXY-COMMON-CTA.
      ****************************************************************
      * SINCE THE PROXY COMMON CONTRACT SHOULD NOT BE UPDATED ANY    *
      * FURTHER, PLACE THE OUTPUT AREA OF THE CONTRACT INTO A NEW    *
      * CONTAINER TO SEND BACK TO THE CALLER.                        *
      ****************************************************************

      *    IT IS POSSIBLE TO GET INTO THIS PARAGRAPH WHEN USING A METHOD
      *     OTHER THAN CHANNELS.  BECAUSE OF THIS, WE WILL MAKE SURE WE
      *     ARE UTILIZING A CHANNEL BEFORE PLACING THE COPYBOOK INTO A
      *     CONTAINER.

           IF NOT EIBCALEN = +0
               GO TO 8100-EXIT
           END-IF.

           SET ADDRESS OF L-CONTRACT-LOCATION
                                       TO WS-PC-OUTPUT-PTR.

           EXEC CICS PUT
               CONTAINER  (CF-CN-PROXY-CBK-OUP-NM)
               FROM       (L-CONTRACT-LOCATION (1: WS-PC-OUTPUT-LEN))
               FLENGTH    (WS-PC-OUTPUT-LEN)
               RESP       (WS-RESPONSE-CODE)
               RESP2      (WS-RESPONSE-CODE2)
           END-EXEC.

      *!   RECORD AN ERROR IF ONE OCCURS, BUT THE ERROR WILL NOT GET
      *!   BACK TO THE CONSUMER.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET PPC-FATAL-ERROR-CODE
                                       TO TRUE
               MOVE WS-RESPONSE-CODE   TO WS-EIBRESP-DISPLAY
               MOVE WS-RESPONSE-CODE2  TO WS-EIBRESP2-DISPLAY
               STRING 'Failed module is '
                      WS-PROGRAM-NAME
                      '.  Failed paragraph is '
                      '8100-OUTPUT-PROXY-COMMON'
                      '.  Failed module SqlCode is 0'
                      '.  Failed module EIBRESP Code is '
                      WS-EIBRESP-DISPLAY
                      '.  Failed module EIBRESP2 Code is '
                      WS-EIBRESP2-DISPLAY
                      '.'  DELIMITED BY SIZE
                      INTO PPC-FATAL-ERROR-MESSAGE
               END-STRING
               GO TO 8100-EXIT
           END-IF.

TL0113 8100-EXIT.
           EXIT.
