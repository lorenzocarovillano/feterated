      ******************************************************************
      * DCLGEN TABLE(HAL_NLBE_WNG_TXT_V)                               *
      ******************************************************************
           EXEC SQL DECLARE HAL_NLBE_WNG_TXT_V TABLE
           ( APP_NM                         CHAR(10) NOT NULL,
             HNLB_ERR_WNG_CD                CHAR(10) NOT NULL,
             HNLB_ERR_WNG_TXT               VARCHAR(500) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE HAL_NLBE_WNG_TXT_V                 *
      ******************************************************************
       01  DCLHAL-NLBE-WNG-TXT-V.
           10 APP-NM               PIC X(10).
           10 HNLB-ERR-WNG-CD      PIC X(10).
           10 HNLB-ERR-WNG-TXT.
              49  HNLB-ERR-WNG-TXT-L              PIC S9(4) COMP.
              49  HNLB-ERR-WNG-TXT-D              PIC X(500).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *
      ******************************************************************
