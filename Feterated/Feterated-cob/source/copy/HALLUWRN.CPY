      **************************************************************
      * HALLUWRN                                                   *
      * UNIT OF WORK RESPONSE WARNINGS UMT                         *
      * THIS COPYBOOK IS USED BY HALOMCM AND THE BUSINESS OBJECTS  *
      * TO READ/WRITE FROM/TO THE UOW RESPONSE WARNINGS UMT.       *
      **************************************************************
      * MAINTENANCE LOG
      *
      * SI#      DATE      PROG#     DESCRIPTION
      * -------- --------- --------- -------------------------------
      * SAVANNAH 11JAN00   18448     CREATED.
C14849* C14849   22JUN01   18600     INCREASE LENGTH OF WARNING TEXT
      **************************************************************
           03 UWRN-COMMON.
              05  UWRN-KEY.
                  10  UWRN-ID                        PIC X(32).
                  10  UWRN-REC-SEQ                   PIC 9(03).
              05  UWRN-UOW-WARNING-BUFFER.
                  10 UWRN-FAILURE-TYPE              PIC X(32)
                                                  VALUE 'WARNINGS'.
                  10 UWRN-FAILED-MODULE             PIC X(32).
                  10 UWRN-FAILED-TABLE-OR-FILE      PIC X(32).
                  10 UWRN-FAILED-COLUMN-OR-FIELD    PIC X(18).
                  10 UWRN-WARNING-CODE              PIC X(10).
C14849*           10 UWRN-WARNING-TEXT              PIC X(100).
C14849            10 UWRN-WARNING-TEXT              PIC X(500).
