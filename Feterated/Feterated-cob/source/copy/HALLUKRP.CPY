      ******************************************************************
      * COMMAREA FOR HALOUKRP (KEY REPLACEMENT)                        *
      ******************************************************************
      * CHANGE LOG                                                     *
      *                                                                *
      * SI#      DATE     PROG#     DESCRIPTION                        *
      * -------- -------- --------- -----------------------------------*
      * SAVANNAH 24MAR00  JAF       NEW                                *
C15748* C15748   08AUG01  ARSI600   INCREASE SIZE OF KEY REPLACEMENT   *
C15748*                             LABEL FROM 32 TO 150.              *
      * 24597    30OCT02  18448     ADD NEW FUNCTION NEEDED BY LOCKING *
      *                             PERFORMANCE ENHANCEMENT.           *
      ******************************************************************
           05  HALOUKRP-INPUT-FIELDS.
             07  HALOUKRP-FUNCTION          PIC X.
                 88 HALOUKRP-STORE-FUNC          VALUE 'S'.
                 88 HALOUKRP-REWRITE-FUNC        VALUE 'U'.
                 88 HALOUKRP-RETRIEVE-FUNC       VALUE 'R'.
                 88 HALOUKRP-CLEAR-MSG-ID-FUNC   VALUE 'C'.
24597            88 HALOUKRP-DELETE-FUNC         VALUE 'D'.
             07  HALOUKRP-BUS-OBJ-NM        PIC X(32).
C15748*      07  HALOUKRP-KEY-REPL-LABEL    PIC X(32).
C15748       07  HALOUKRP-KEY-REPL-LABEL    PIC X(150).
           05  HALOUKRP-INPUT-OUTPUT-FIELDS.
             07  HALOUKRP-KEY-REPL-KEY-LEN  PIC 9(4) BINARY.
             07  HALOUKRP-KEY-REPL-KEY      PIC X(40).
             07  HALOUKRP-RETURN-CODE       PIC 9(2).
                 88 HALOUKRP-OKAY                 VALUE 0.
                 88 HALOUKRP-NOTFND               VALUE 1.
