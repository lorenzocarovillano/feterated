      ******************************************************************
      *                                                                *
      * CAWLCORC - CLT_OBJ_RELATED_CLIENT_BPO                          *
      *            FRONT END/ BACK END INTERFACE DESCRIPTION           *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#     DATE        PROG#     DESCRIPTION                      *
      * ------- ----------- --------- ---------------------------------*
F66970* F66970   01/26/2004 E404ASW   NEW COPYBOOK                     *
      *                                                                *
      ******************************************************************
           05      CWORC-CLT-OBJ-RELATION-ROW.
             08    CWORC-CLT-OBJ-RELATION-FIXED.
               10  CWORC-CLT-OBJ-RELATION-CSUM      PIC 9(09).
               10  CWORC-TCH-OBJECT-KEY-KCRE        PIC X(32).
               10  CWORC-HISTORY-VLD-NBR-KCRE       PIC X(32).
               10  CWORC-CIOR-EFF-DT-KCRE           PIC X(32).
               10  CWORC-OBJ-SYS-ID-KCRE            PIC X(32).
               10  CWORC-CIOR-OBJ-SEQ-NBR-KCRE      PIC X(32).
               10  CWORC-CLIENT-ID-KCRE             PIC X(32).
             08    CWORC-CLT-OBJ-RELATION-DATES.
               10  CWORC-TRANS-PROCESS-DT           PIC X(10).
             08    CWORC-CLT-OBJ-RELATION-KEY.
               10  CWORC-TCH-OBJECT-KEY-CI          PIC X.
               10  CWORC-TCH-OBJECT-KEY             PIC X(20).
               10  CWORC-HISTORY-VLD-NBR-CI         PIC X.
               10  CWORC-HISTORY-VLD-NBR-SIGN       PIC X.
               10  CWORC-HISTORY-VLD-NBR            PIC 9(05).
               10  CWORC-CIOR-EFF-DT-CI             PIC X.
               10  CWORC-CIOR-EFF-DT                PIC X(10).
               10  CWORC-OBJ-SYS-ID-CI              PIC X.
               10  CWORC-OBJ-SYS-ID                 PIC X(04).
               10  CWORC-CIOR-OBJ-SEQ-NBR-CI        PIC X.
               10  CWORC-CIOR-OBJ-SEQ-NBR-SIGN      PIC X.
               10  CWORC-CIOR-OBJ-SEQ-NBR           PIC 9(05).
             08    CWORC-CLT-OBJ-RELATION-DATA.
      **  FIELDS PERTAINING TO COLUMNS ON TABLE:
               10  CWORC-CLIENT-ID-CI               PIC X.
               10  CWORC-CLIENT-ID                  PIC X(20).
               10  CWORC-RLT-TYP-CD-CI              PIC X.
               10  CWORC-RLT-TYP-CD                 PIC X(04).
               10  CWORC-OBJ-CD-CI                  PIC X.
               10  CWORC-OBJ-CD                     PIC X(04).
               10  CWORC-CIOR-SHW-OBJ-KEY-CI        PIC X.
               10  CWORC-CIOR-SHW-OBJ-KEY           PIC X(30).
               10  CWORC-ADR-SEQ-NBR-CI             PIC X.
               10  CWORC-ADR-SEQ-NBR-SIGN           PIC X.
               10  CWORC-ADR-SEQ-NBR                PIC 9(05).
               10  CWORC-USER-ID-CI                 PIC X.
               10  CWORC-USER-ID                    PIC X(08).
               10  CWORC-STATUS-CD-CI               PIC X.
               10  CWORC-STATUS-CD                  PIC X(01).
               10  CWORC-TERMINAL-ID-CI             PIC X.
               10  CWORC-TERMINAL-ID                PIC X(08).
               10  CWORC-CIOR-EXP-DT-CI             PIC X.
               10  CWORC-CIOR-EXP-DT                PIC X(10).
               10  CWORC-CIOR-EFF-ACY-TS-CI         PIC X.
               10  CWORC-CIOR-EFF-ACY-TS            PIC X(26).
               10  CWORC-CIOR-EXP-ACY-TS-CI         PIC X.
               10  CWORC-CIOR-EXP-ACY-TS            PIC X(26).
               10  CWORC-CIOR-LEG-ENT-CD-CI         PIC X.
               10  CWORC-CIOR-LEG-ENT-CD            PIC X(02).
               10  CWORC-CIOR-FST-NM-CI             PIC X.
               10  CWORC-CIOR-FST-NM                PIC X(30).
               10  CWORC-CIOR-LST-NM-CI             PIC X.
               10  CWORC-CIOR-LST-NM                PIC X(60).
               10  CWORC-CIOR-MDL-NM-CI             PIC X.
               10  CWORC-CIOR-MDL-NM                PIC X(30).
               10  CWORC-CIOR-NM-PFX-CI             PIC X.
               10  CWORC-CIOR-NM-PFX                PIC X(04).
               10  CWORC-CIOR-NM-SFX-CI             PIC X.
               10  CWORC-CIOR-NM-SFX                PIC X(04).
               10  CWORC-CIOR-LNG-NM-CI             PIC X.
               10  CWORC-CIOR-LNG-NM-NI             PIC X.
               10  CWORC-CIOR-LNG-NM                PIC X(132).
             08    CWORC-CLT-OBJ-INPUT-DATA.
      **  FIELDS AS INPUT TO BPO:
               10  CWORC-FILTER-TYPE                PIC X(02).
