      **************************************************************
      * HALLMON                                                    *
      **************************************************************
      * HALRMON (PERFORMANCE MONITORING API) LINKAGE               *
      **************************************************************
      * MAINTENANCE LOG
      *
      * SI#      DATE      PROG#      DESCRIPTION
      * -------- --------- ---------- ------------------------------
      * SAVANNAH 19DEC2000 JAF (NU)   NEW
      **************************************************************
           03 HALRMON-INPUT-LINKAGE.
              05 HALRMON-FUNCTION           PIC X(1).
                 88 HALRMON-BEGIN-FUNCTION  VALUE 'B'.
                 88 HALRMON-END-FUNCTION    VALUE 'E'.
                 88 HALRMON-INFO-FUNCTION   VALUE 'I'.
              05 HALRMON-BUS-OBJ-NM         PIC X(32).
              05 HALRMON-BUS-MOD-NM         PIC X(32).
              05 HALRMON-INFO-LABEL         PIC X(32).
              05 HALRMON-INFO-LENGTH        PIC S9(4) COMP.
