CLUSTER ------- CICSTS31.S3PLUS.P9.HALFEIED
     IN-CAT --- CATALOG.USER.CICS
     HISTORY
       DATASET-OWNER-----(NULL)     CREATION--------2019.230
       RELEASE----------------2     EXPIRATION------0000.000
     SMSDATA
       STORAGECLASS ---STANDARD     MANAGEMENTCLASS---(NULL)
       DATACLASS ----------DASD     LBACKUP ---0000.000.0000
                             LISTING FROM CATALOG -- CATALOG.USER.CICS
       CA-RECLAIM---------(YES)
       EATTR-------------(NULL)
       BWO STATUS------00000000     BWO TIMESTAMP---00000 00:00:00.0
       BWO---------------(NULL)
     RLSDATA
       LOG ----------------(NULL)   RECOVERY REQUIRED --(NO)     FRLOG ------------(NULL)
       VSAM QUIESCED -------(NO)    RLS IN USE ---------(NO)     LOGREPLICATE-------------(NO)
       LOGSTREAMID-----------------------------(NULL)
       RECOVERY TIMESTAMP LOCAL-----X'0000000000000000'
       RECOVERY TIMESTAMP GMT-------X'0000000000000000'
       DATABASE -----------------(NULL)
     ENCRYPTIONDATA
       DATA SET ENCRYPTION-----(NO)
     PROTECTION-PSWD-----(NULL)     RACF----------------(NO)
     ASSOCIATIONS
       DATA-----CICSTS31.S3PLUS.P9.HALFEIED.DATA
       INDEX----CICSTS31.S3PLUS.P9.HALFEIED.INDEX
DATA ---------- CICSTS31.S3PLUS.P9.HALFEIED.DATA
     IN-CAT --- CATALOG.USER.CICS
     HISTORY
       DATASET-OWNER-----(NULL)     CREATION--------2019.230
       RELEASE----------------2     EXPIRATION------0000.000
       ACCOUNT-INFO-----------------------------------(NULL)
     PROTECTION-PSWD-----(NULL)     RACF----------------(NO)
     ASSOCIATIONS
       CLUSTER--CICSTS31.S3PLUS.P9.HALFEIED
     ATTRIBUTES
       KEYLEN----------------69     AVGLRECL------------5129     BUFSPACE-----------18944     CISIZE--------------8192
       RKP--------------------0     MAXLRECL------------5129     EXCPEXIT----------(NULL)     CI/CA-----------------90
       SHROPTNS(2,3)      SPEED     UNIQUE           NOERASE     INDEXED       NOWRITECHK     UNORDERED          REUSE
       SPANNED
     STATISTICS
       REC-TOTAL--------------1     SPLITS-CI--------------0     EXCPS-----------------43
       REC-DELETED------------0     SPLITS-CA--------------0     EXTENTS---------------20
       REC-INSERTED-----------0     FREESPACE-%CI---------10     SYSTEM-TIMESTAMP:
       REC-UPDATED------------0     FREESPACE-%CA----------5          X'D69743F4E0E02B42'
       REC-RETRIEVED----------0     FREESPC---------56614912
     ALLOCATION
       SPACE-TYPE------CYLINDER     HI-A-RBA-------176947200
       SPACE-PRI-------------50     HI-U-RBA----------737280
       SPACE-SEC--------------5
     VOLUME
       VOLSER------------CICS14     PHYREC-SIZE---------8192     HI-A-RBA-------176947200     EXTENT-NUMBER---------20
       DEVTYPE------X'3010200F'     PHYRECS/TRK------------6     HI-U-RBA----------737280     EXTENT-TYPE--------X'40'
       VOLFLAG------------PRIME     TRACKS/CA-------------15
       EXTENTS:
                             LISTING FROM CATALOG -- CATALOG.USER.CICS
       LOW-CCHH-----X'04130000'     LOW-RBA----------------0     TRACKS---------------750
       HIGH-CCHH----X'0444000E'     HIGH-RBA--------36863999
       LOW-CCHH-----X'006B0000'     LOW-RBA---------36864000     TRACKS----------------75
       HIGH-CCHH----X'006F000E'     HIGH-RBA--------40550399
       LOW-CCHH-----X'00920000'     LOW-RBA---------40550400     TRACKS----------------75
       HIGH-CCHH----X'0096000E'     HIGH-RBA--------44236799
       LOW-CCHH-----X'00BE0000'     LOW-RBA---------44236800     TRACKS---------------225
       HIGH-CCHH----X'00CC000E'     HIGH-RBA--------55295999
       LOW-CCHH-----X'010D0000'     LOW-RBA---------55296000     TRACKS---------------150
       HIGH-CCHH----X'0116000E'     HIGH-RBA--------62668799
       LOW-CCHH-----X'014C0000'     LOW-RBA---------62668800     TRACKS----------------75
       HIGH-CCHH----X'0150000E'     HIGH-RBA--------66355199
       LOW-CCHH-----X'01630000'     LOW-RBA---------66355200     TRACKS---------------150
       HIGH-CCHH----X'016C000E'     HIGH-RBA--------73727999
       LOW-CCHH-----X'017B0000'     LOW-RBA---------73728000     TRACKS---------------225
       HIGH-CCHH----X'0189000E'     HIGH-RBA--------84787199
       LOW-CCHH-----X'01AE0000'     LOW-RBA---------84787200     TRACKS---------------225
       HIGH-CCHH----X'01BC000E'     HIGH-RBA--------95846399
       LOW-CCHH-----X'01E80000'     LOW-RBA---------95846400     TRACKS----------------75
       HIGH-CCHH----X'01EC000E'     HIGH-RBA--------99532799
       LOW-CCHH-----X'02040000'     LOW-RBA---------99532800     TRACKS---------------225
       HIGH-CCHH----X'0212000E'     HIGH-RBA-------110591999
       LOW-CCHH-----X'02150000'     LOW-RBA--------110592000     TRACKS---------------150
       HIGH-CCHH----X'021E000E'     HIGH-RBA-------117964799
       LOW-CCHH-----X'02A60000'     LOW-RBA--------117964800     TRACKS----------------75
       HIGH-CCHH----X'02AA000E'     HIGH-RBA-------121651199
       LOW-CCHH-----X'02B00000'     LOW-RBA--------121651200     TRACKS---------------150
       HIGH-CCHH----X'02B9000E'     HIGH-RBA-------129023999
       LOW-CCHH-----X'033B0000'     LOW-RBA--------129024000     TRACKS---------------150
       HIGH-CCHH----X'0344000E'     HIGH-RBA-------136396799
       LOW-CCHH-----X'03930000'     LOW-RBA--------136396800     TRACKS----------------75
       HIGH-CCHH----X'0397000E'     HIGH-RBA-------140083199
       LOW-CCHH-----X'03AC0000'     LOW-RBA--------140083200     TRACKS---------------150
       HIGH-CCHH----X'03B5000E'     HIGH-RBA-------147455999
       LOW-CCHH-----X'03BF0000'     LOW-RBA--------147456000     TRACKS---------------225
       HIGH-CCHH----X'03CD000E'     HIGH-RBA-------158515199
       LOW-CCHH-----X'03FE0000'     LOW-RBA--------158515200     TRACKS----------------75
       HIGH-CCHH----X'0402000E'     HIGH-RBA-------162201599
       LOW-CCHH-----X'049D0000'     LOW-RBA--------162201600     TRACKS---------------300
       HIGH-CCHH----X'04B0000E'     HIGH-RBA-------176947199
INDEX --------- CICSTS31.S3PLUS.P9.HALFEIED.INDEX
     IN-CAT --- CATALOG.USER.CICS
     HISTORY
       DATASET-OWNER-----(NULL)     CREATION--------2019.230
       RELEASE----------------2     EXPIRATION------0000.000
     PROTECTION-PSWD-----(NULL)     RACF----------------(NO)
     ASSOCIATIONS
                             LISTING FROM CATALOG -- CATALOG.USER.CICS
       CLUSTER--CICSTS31.S3PLUS.P9.HALFEIED
     ATTRIBUTES
       KEYLEN----------------69     AVGLRECL---------------0     BUFSPACE---------------0     CISIZE--------------2560
       RKP--------------------0     MAXLRECL------------2553     EXCPEXIT----------(NULL)     CI/CA-----------------17
       SHROPTNS(2,3)   RECOVERY     UNIQUE           NOERASE     NOWRITECHK     UNORDERED     REUSE
     STATISTICS
       REC-TOTAL--------------3     SPLITS-CI--------------1     EXCPS----------------198     INDEX:
       REC-DELETED------------0     SPLITS-CA--------------0     EXTENTS----------------7     LEVELS-----------------2
       REC-INSERTED-----------0     FREESPACE-%CI----------0     SYSTEM-TIMESTAMP:            ENTRIES/SECT-----------9
       REC-UPDATED-----------93     FREESPACE-%CA----------0          X'D69743F4E0E02B42' SEQ-SET-RBA----------------0
       REC-RETRIEVED----------0     FREESPC------------43520                              HI-LEVEL-RBA------------5120
     ALLOCATION
       SPACE-TYPE---------TRACK     HI-A-RBA----------652800
       SPACE-PRI--------------3     HI-U-RBA------------2560
       SPACE-SEC--------------1
     VOLUME
       VOLSER------------CICS14     PHYREC-SIZE---------2560     HI-A-RBA----------652800     EXTENT-NUMBER----------7
       DEVTYPE------X'3010200F'     PHYRECS/TRK-----------17     HI-U-RBA------------2560     EXTENT-TYPE--------X'40'
       VOLFLAG------------PRIME     TRACKS/CA--------------1
       EXTENTS:
       LOW-CCHH-----X'00110000'     LOW-RBA----------------0     TRACKS-----------------4
       HIGH-CCHH----X'00110003'     HIGH-RBA----------174079
       LOW-CCHH-----X'000D0009'     LOW-RBA-----------174080     TRACKS-----------------2
       HIGH-CCHH----X'000D000A'     HIGH-RBA----------261119
       LOW-CCHH-----X'000D000D'     LOW-RBA-----------261120     TRACKS-----------------2
       HIGH-CCHH----X'000D000E'     HIGH-RBA----------348159
       LOW-CCHH-----X'000F0003'     LOW-RBA-----------348160     TRACKS-----------------1
       HIGH-CCHH----X'000F0003'     HIGH-RBA----------391679
       LOW-CCHH-----X'000F0005'     LOW-RBA-----------391680     TRACKS-----------------1
       HIGH-CCHH----X'000F0005'     HIGH-RBA----------435199
       LOW-CCHH-----X'000F000B'     LOW-RBA-----------435200     TRACKS-----------------1
       HIGH-CCHH----X'000F000B'     HIGH-RBA----------478719
       LOW-CCHH-----X'00110006'     LOW-RBA-----------478720     TRACKS-----------------4
       HIGH-CCHH----X'00110009'     HIGH-RBA----------652799
