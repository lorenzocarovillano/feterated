       ID DIVISION.

       PROGRAM-ID.   XZ0X0021.
      *AUTHOR.       JERAMY LAWSON.
      *DATE-WRITTEN. 24 JUL 2017.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                    UOW        : XZ_DET_NOT_DT                *
      *                    OPERATIONS : determineNotificationDate    *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      *   15389  07/24/2017 E404JAL   INITIAL PROGRAM                *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-MAX-POL      VALUE +150              PIC S9(03).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_DET_NOT_DT'   PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q0021'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R0021'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X0021'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).

       01  SUBSCRIPTS.
           03  SS-PI                           BINARY PIC S9(04).

       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-PROGRAM-NAME                         PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.
           03  WS-OPERATION-NAME                       PIC X(32).
               88  WS-GET-NOT-DT
                               VALUE 'determineNotificationDate'.



       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-XZ0Y0021.
           COPY XZ0Y0021 REPLACING ==:XZY021:== BY ==XZY021Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-XZ0Y0021.
           COPY XZ0Y0021 REPLACING ==:XZY021:== BY ==XZY021R==.


      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T0021.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT021-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT021-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT021-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT021-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

           INITIALIZE L-FW-REQ-XZ0Y0021.

           MOVE PPC-OPERATION          TO WS-OPERATION-NAME.

           MOVE XZT21I-CSR-ACT-NBR     TO XZY021Q-CSR-ACT-NBR.

           MOVE XZT21I-ACT-TMN-DT      TO XZY021Q-ACT-TMN-DT.

           MOVE XZT21I-NOT-TYP-CD      TO XZY021Q-ACT-NOT-TYP-CD.

           MOVE +1                     TO SS-PI.

         13100-A.
           IF SS-PI > CF-MAX-POL
             OR
              XZT21I-POL-NBR-LIST(SS-PI) = SPACES
               CONTINUE
           ELSE
               MOVE XZT21I-POL-NBR-LIST(SS-PI)
                                       TO XZY021Q-CNC-POL-LIST(SS-PI)
               ADD +1 TO SS-PI
               GO TO 13100-A
           END-IF.


       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           MOVE XZY021R-CSR-ACT-NBR    TO XZT21O-CSR-ACT-NBR.

           MOVE XZY021R-NOT-DT         TO XZT21O-NOT-DT.

       13200-EXIT.
           EXIT.

