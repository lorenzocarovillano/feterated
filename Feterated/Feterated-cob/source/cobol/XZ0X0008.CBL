       ID DIVISION.

       PROGRAM-ID.   XZ0X0008.
      *AUTHOR.       Gary Liedtke.
      *DATE-WRITTEN. 03 OCT 2008.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                  UOW        : XZ_MAINTAIN_ACT_NOT_POL        *
      *                  OPERATIONS : AddAccountNotificationPolicy   *
      *                               UpdateAccountNotificationPolicy*
      *                               DeleteAccountNotificationPolicy*
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * TO07614  10/28/2008 E404GCL   INITIAL PROGRAM                *
      * TO07614  03/09/2009 E404DLP   CHANGED ADR-SEQ-NBR TO ADR-ID  *
      * TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-NEGATIVE-ONE VALUE -1                PIC S9(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_MAINTAIN_ACT_NOT_POL'
                                                       PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q0008'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R0008'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X0008'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).


       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-PROGRAM-NAME                         PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.
           03  WS-OPERATION-NAME                       PIC X(32).
               88  WS-ADD-ACT-NOT-POL
                               VALUE 'AddAccountNotificationPolicy'.
               88  WS-UPD-ACT-NOT-POL
                               VALUE 'UpdateAccountNotificationPolicy'.
               88  WS-DEL-ACT-NOT-POL
                               VALUE 'DeleteAccountNotificationPolicy'.
               88  WS-VALID-OPERATION
                               VALUE 'AddAccountNotificationPolicy'
                                     'UpdateAccountNotificationPolicy'
                                     'DeleteAccountNotificationPolicy'.


       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-ACT-NOT-POL.
           COPY XZ0C0002 REPLACING ==:XZC002:== BY ==XZC002Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-ACT-NOT-POL.
           COPY XZ0C0002 REPLACING ==:XZC002:== BY ==XZC002R==.


      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T0008.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT008-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT008-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT008-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT008-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      * THIS PARAGRAPH CAN CALL OTHER SUB-PARAGRAPHS AS NEEDED, AND  *
      * COULD CONTAIN LOOPS IF ARRAYS (TABLES) ARE INVOLVED.         *
      ****************************************************************

           MOVE PPC-OPERATION          TO WS-OPERATION-NAME.

           IF XZT08I-USERID NOT = SPACES
             AND
              XZT08I-USERID NOT = LOW-VALUES
               MOVE XZT08I-USERID      TO CSC-AUTH-USERID
           END-IF.

           EVALUATE TRUE
               WHEN WS-ADD-ACT-NOT-POL
                   MOVE XZT08I-CSR-ACT-NBR
                                       TO XZC002Q-CSR-ACT-NBR
                   MOVE SPACES         TO XZC002Q-CSR-ACT-NBR-KCRE
                   MOVE 'K'            TO XZC002Q-CSR-ACT-NBR-CI
                   MOVE XZT08I-TK-NOT-PRC-TS
                                       TO XZC002Q-NOT-PRC-TS
                   MOVE SPACES         TO XZC002Q-NOT-PRC-TS-KCRE
                   MOVE 'K'            TO XZC002Q-NOT-PRC-TS-CI
                   MOVE XZT08I-POL-NBR TO XZC002Q-POL-NBR
                   MOVE SPACES         TO XZC002Q-POL-NBR-KCRE
                   MOVE 'K'            TO XZC002Q-POL-NBR-CI

               WHEN WS-UPD-ACT-NOT-POL
                   MOVE XZT08I-CSR-ACT-NBR
                                       TO XZC002Q-CSR-ACT-NBR
                   MOVE XZT08I-TK-NOT-PRC-TS
                                       TO XZC002Q-NOT-PRC-TS
                   MOVE XZT08I-POL-NBR TO XZC002Q-POL-NBR
                   MOVE XZT08I-TK-ACT-NOT-POL-CSUM
                                       TO XZC002Q-ACT-NOT-POL-CSUM
               WHEN WS-DEL-ACT-NOT-POL
                   MOVE XZT08I-CSR-ACT-NBR
                                       TO XZC002Q-CSR-ACT-NBR
                   MOVE XZT08I-TK-NOT-PRC-TS
                                       TO XZC002Q-NOT-PRC-TS
                   MOVE XZT08I-POL-NBR TO XZC002Q-POL-NBR
                   MOVE XZT08I-TK-ACT-NOT-POL-CSUM
                                       TO XZC002Q-ACT-NOT-POL-CSUM
                   GO TO 13100-EXIT
           END-EVALUATE.



           MOVE XZT08I-POL-TYP-CD      TO XZC002Q-POL-TYP-CD.
           MOVE XZT08I-POL-PRI-RSK-ST-ABB
                                       TO XZC002Q-POL-PRI-RSK-ST-ABB.
           MOVE XZT08I-NOT-EFF-DT      TO XZC002Q-NOT-EFF-DT.
           IF XZT08I-NOT-EFF-DT NOT = SPACES
               MOVE CF-COL-IS-NOT-NULL TO XZC002Q-NOT-EFF-DT-NI
           ELSE
               MOVE CF-COL-IS-NULL     TO XZC002Q-NOT-EFF-DT-NI
           END-IF.
           MOVE XZT08I-POL-EFF-DT      TO XZC002Q-POL-EFF-DT.
           MOVE XZT08I-POL-EXP-DT      TO XZC002Q-POL-EXP-DT.
           MOVE XZT08I-POL-DUE-AMT     TO XZC002Q-POL-DUE-AMT.
           IF XZT08I-POL-DUE-AMT > +0
               MOVE CF-POSITIVE        TO XZC002Q-POL-DUE-AMT-SIGN
               MOVE CF-COL-IS-NOT-NULL TO XZC002Q-POL-DUE-AMT-NI
           ELSE
               IF XZT08I-POL-DUE-AMT < +0
                   MOVE CF-NEGATIVE    TO XZC002Q-POL-DUE-AMT-SIGN
                   MOVE CF-COL-IS-NOT-NULL
                                       TO XZC002Q-POL-DUE-AMT-NI
               ELSE
                   MOVE CF-COL-IS-NULL TO XZC002Q-POL-DUE-AMT-NI
               END-IF
           END-IF.
           MOVE XZT08I-TK-NIN-CLT-ID   TO XZC002Q-NIN-CLT-ID.
           MOVE XZT08I-TK-NIN-ADR-ID   TO XZC002Q-NIN-ADR-ID.
           MOVE XZT08I-TK-WF-STARTED-IND
                                       TO XZC002Q-WF-STARTED-IND.
           IF XZT08I-TK-WF-STARTED-IND NOT = SPACES
               MOVE CF-COL-IS-NOT-NULL TO XZC002Q-WF-STARTED-IND-NI
           ELSE
               MOVE CF-COL-IS-NULL     TO XZC002Q-WF-STARTED-IND-NI
           END-IF.
           MOVE XZT08I-TK-POL-BIL-STA-CD
                                       TO XZC002Q-POL-BIL-STA-CD.
           IF XZT08I-TK-POL-BIL-STA-CD NOT = SPACES
               MOVE CF-COL-IS-NOT-NULL TO XZC002Q-POL-BIL-STA-CD-NI
           ELSE
               MOVE CF-COL-IS-NULL     TO XZC002Q-POL-BIL-STA-CD-NI
           END-IF.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           IF WS-DEL-ACT-NOT-POL
               GO TO 13200-EXIT
           END-IF.

           MOVE XZC002R-CSR-ACT-NBR    TO XZT08O-CSR-ACT-NBR.
           MOVE XZC002R-NOT-PRC-TS     TO XZT08O-TK-NOT-PRC-TS.
           MOVE XZC002R-POL-NBR        TO XZT08O-POL-NBR.
           MOVE XZC002R-POL-TYP-CD     TO XZT08O-POL-TYP-CD.
           MOVE XZC002R-POL-TYP-DES    TO XZT08O-POL-TYP-DES.
           MOVE XZC002R-POL-PRI-RSK-ST-ABB
                                       TO XZT08O-POL-PRI-RSK-ST-ABB.
           MOVE XZC002R-NOT-EFF-DT     TO XZT08O-NOT-EFF-DT.
           MOVE XZC002R-POL-EFF-DT     TO XZT08O-POL-EFF-DT.
           MOVE XZC002R-POL-EXP-DT     TO XZT08O-POL-EXP-DT.
           MOVE XZC002R-POL-DUE-AMT    TO XZT08O-POL-DUE-AMT.
           IF XZC002R-POL-DUE-AMT-SIGN = CF-NEGATIVE
               COMPUTE XZT08O-POL-DUE-AMT = XZT08O-POL-DUE-AMT
                                          * CF-NEGATIVE-ONE
           END-IF.
           MOVE XZC002R-NIN-CLT-ID     TO XZT08O-TK-NIN-CLT-ID.
           MOVE XZC002R-NIN-ADR-ID     TO XZT08O-TK-NIN-ADR-ID.
           MOVE XZC002R-WF-STARTED-IND TO XZT08O-TK-WF-STARTED-IND.
           MOVE XZC002R-POL-BIL-STA-CD TO XZT08O-TK-POL-BIL-STA-CD.
           MOVE XZC002R-ACT-NOT-POL-CSUM
                                       TO XZT08O-TK-ACT-NOT-POL-CSUM.

       13200-EXIT.
           EXIT.

