       ID DIVISION.

       PROGRAM-ID.   XZ0X9000.
      *AUTHOR.       Gary Liedtke.
      *DATE-WRITTEN. 05 JAN 2009.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                  UOW        : XZ_PREPARE_CANCELLATION_WORDING*
      *                  OPERATIONS : PrepareCancellationWording     *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * TO07614  01/05/2009 E404GCL   INITIAL PROGRAM                *
      * TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-NEGATIVE-ONE VALUE -1                PIC S9(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_PREPARE_CANCELLATION_WORDING'
                                                       PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q9000'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE SPACES            PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X9000'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).


       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-PROGRAM-NAME                         PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.
           03  WS-OPERATION-NAME                       PIC X(32).
               88  WS-PREPARE-CANCELLATION-WRD
                               VALUE 'PrepareCancellationWording'.
               88  WS-VALID-OPERATION
                               VALUE 'PrepareCancellationWording'.


       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-PRP-CNC-WRD.
           COPY XZ0Y9000 REPLACING ==:XZY900:== BY ==XZY900Q==.

      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
      ** NO RESPONSE DATA EXPECTED
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  FILLER                                  PIC X(01).

      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T9000.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT900-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT900-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT900-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT900-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      * THIS PARAGRAPH CAN CALL OTHER SUB-PARAGRAPHS AS NEEDED, AND  *
      * COULD CONTAIN LOOPS IF ARRAYS (TABLES) ARE INVOLVED.         *
      ****************************************************************
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA FOR AN ADD.                                    *
      ****************************************************************

           MOVE PPC-OPERATION          TO WS-OPERATION-NAME.

      * IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
           IF XZT90I-USERID NOT = SPACES
             AND
              XZT90I-USERID NOT = LOW-VALUES
               MOVE XZT90I-USERID      TO CSC-AUTH-USERID
                                          XZY900Q-USERID
           END-IF.

           MOVE XZT90I-CSR-ACT-NBR     TO XZY900Q-CSR-ACT-NBR.
           MOVE XZT90I-NOT-PRC-TS      TO XZY900Q-NOT-PRC-TS.
           MOVE XZT90I-ACT-NOT-TYP-CD  TO XZY900Q-ACT-NOT-TYP-CD.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           IF WS-PREPARE-CANCELLATION-WRD
               GO TO 13200-EXIT
           END-IF.


       13200-EXIT.
           EXIT.

