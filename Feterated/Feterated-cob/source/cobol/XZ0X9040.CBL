       ID DIVISION.

       PROGRAM-ID.   XZ0X9040.
      *AUTHOR.       DAWN POSSEHL.
      *DATE-WRITTEN. 20 JAN 2009.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                  UOW       : XZ_GET_FRM_LIST                 *
      *                  OPERATION : GetFormListByNotification       *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * TO07614  01/20/2009 E404DLP   INITIAL PROGRAM                *
      * TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
P02756* PP02756  02/04/2011 E404DLP   ADDED PAGING FIELDS            *
P02756*                               INCREADED OCCURS               *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_GET_FRM_LIST' PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q9040'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R9040'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X9040'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).

       01  SUBSCRIPTS.
           03  SS-FL                                   PIC S9(04) COMP.

       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-MAX-FRM-ROWS                 BINARY  PIC S9(04).
           03  WS-PROGRAM-NAME VALUE 'XZ0X9040'        PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.


       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-XZ0A9040.
           COPY XZ0A9040 REPLACING ==:XZA940:== BY ==XZA940Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-XZ0A9040.
           COPY XZ0A9040 REPLACING ==:XZA940:== BY ==XZA940R==.
P02756*    03  L-FW-RESP-XZ0A9041      OCCURS 30 TIMES
P02756     03  L-FW-RESP-XZ0A9041      OCCURS 100 TIMES
                                       INDEXED BY IX-RS.
           COPY XZ0A9041 REPLACING ==:XZA941:== BY ==XZA941R==.


      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T9040.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT904-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT904-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT904-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT904-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

           COMPUTE WS-MAX-FRM-ROWS = LENGTH OF XZT94O-FRM-LIST-TBL
                                   / LENGTH OF XZT94O-FRM-LIST.

           MOVE WS-MAX-FRM-ROWS        TO XZA940Q-MAX-FRM-ROWS.

P02756* SET PAGING INFO
P02756     IF XZT94I-PI-START-POINT = 0
P02756         MOVE +1                 TO XZA940Q-PI-START-POINT
P02756     ELSE
P02756         MOVE XZT94I-PI-START-POINT
P02756                                 TO XZA940Q-PI-START-POINT
P02756     END-IF.

      *    IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
           IF XZT94I-USERID NOT = SPACES
             AND
              XZT94I-USERID NOT = LOW-VALUES
               MOVE XZT94I-USERID      TO CSC-AUTH-USERID
           END-IF.

      * SERVICE INPUT/REQUEST DATA
           MOVE XZT94I-CSR-ACT-NBR     TO XZA940Q-CSR-ACT-NBR.
           MOVE XZT94I-TK-NOT-PRC-TS   TO XZA940Q-NOT-PRC-TS.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

      * REQUEST KEYS
           MOVE XZA940R-CSR-ACT-NBR    TO XZT94O-CSR-ACT-NBR.
           MOVE XZA940R-NOT-PRC-TS     TO XZT94O-TK-NOT-PRC-TS.

           SET IX-RS                   TO +1.
           MOVE +1                     TO SS-FL.
      * LOOPING REQUIRED FOR POSSIBLE MULTIPLE FORM ROWS

         13200-A.

      * NO FORM NBR MEANS WE HAVE FINISHED
           IF XZA941R-FRM-NBR (IX-RS) = SPACES
               GO TO 13200-EXIT
           END-IF.

P02756* PAGING INFORMATION
P02756***  MOVE THIS FIELD IN EVERY TIME THROUGH THE LOOP.  THE LAST
P02756***  RESPONSE ROW WILL HAVE THIS FILLED IN IF THERE ARE MORE
P02756***  VALUES TO BE RETURNED.
P02756     MOVE XZA941R-PO-START-POINT (IX-RS)
P02756                                 TO  XZT94O-PO-START-POINT.

      * DETAIL FIELDS FOR EACH ROW OF DATA
           MOVE XZA941R-FRM-SEQ-NBR (IX-RS)
                                       TO XZT94O-TK-FRM-SEQ-NBR (SS-FL).
           MOVE XZA941R-FRM-NBR (IX-RS)
                                       TO XZT94O-FRM-NBR (SS-FL).
           MOVE XZA941R-FRM-EDT-DT (IX-RS)
                                       TO XZT94O-FRM-EDT-DT (SS-FL).

           IF IX-RS = WS-MAX-FRM-ROWS
               GO TO 13200-EXIT
           END-IF.

           SET IX-RS                   UP BY +1.
           ADD +1                      TO SS-FL.
           GO TO 13200-A.

       13200-EXIT.
           EXIT.

