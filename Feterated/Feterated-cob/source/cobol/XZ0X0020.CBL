       ID DIVISION.

       PROGRAM-ID.   XZ0X0020.
      *AUTHOR.       DAWN POSSEHL.
      *DATE-WRITTEN. 23 SEP 2010.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                    UOW        : XZ_MAINTAIN_ACT_NOT_POL_REC  *
      *                    OPERATIONS : AddAccountNotificationPolRec *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * PP02570  09/23/2010 E404DLP   INITIAL PROGRAM                *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_MAINTAIN_ACT_NOT_POL_REC'
                                                       PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q0020'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R0020'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X0020'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).

       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-PROGRAM-NAME                         PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.
           03  WS-OPERATION-NAME                       PIC X(32).
               88  WS-ADD-ACT-NOT-POL-REC
                               VALUE 'AddAccountNotificationPolRec'.



       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-XZ0C0008.
           COPY XZ0C0008 REPLACING ==:XZC008:== BY ==XZC008Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE RESPONSE FROM
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-XZ0C0008.
           COPY XZ0C0008 REPLACING ==:XZC008:== BY ==XZC008R==.


      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T0020.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT020-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT020-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT020-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT020-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

      * IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
           IF XZT20I-USERID NOT = SPACES
             AND
              XZT20I-USERID NOT = LOW-VALUES
               MOVE XZT20I-USERID      TO CSC-AUTH-USERID
           END-IF.

           MOVE PPC-OPERATION          TO WS-OPERATION-NAME.

           MOVE XZT20I-CSR-ACT-NBR     TO XZC008Q-CSR-ACT-NBR.
           MOVE XZT20I-TK-NOT-PRC-TS   TO XZC008Q-NOT-PRC-TS.
           MOVE XZT20I-TK-REC-SEQ-NBR  TO XZC008Q-REC-SEQ-NBR.

           IF XZT20I-TK-REC-SEQ-NBR  >= +0
               MOVE CF-POSITIVE        TO XZC008Q-REC-SEQ-NBR-SIGN
           ELSE
               MOVE CF-NEGATIVE        TO XZC008Q-REC-SEQ-NBR-SIGN
           END-IF.

           MOVE XZT20I-POL-NBR         TO XZC008Q-POL-NBR.

           EVALUATE TRUE
               WHEN WS-ADD-ACT-NOT-POL-REC
      *            ENSURE KEY IS NOT GENERATED, SINCE IT IS SUPPLIED
      *            BY THE USER.
                   MOVE SPACES         TO XZC008Q-CSR-ACT-NBR-KCRE
                                          XZC008Q-NOT-PRC-TS-KCRE
                                          XZC008Q-REC-SEQ-NBR-KCRE
                                          XZC008Q-POL-NBR-KCRE
                   MOVE 'K'            TO XZC008Q-CSR-ACT-NBR-CI
                                          XZC008Q-NOT-PRC-TS-CI
                                          XZC008Q-REC-SEQ-NBR-CI
                                          XZC008Q-POL-NBR-CI
           END-EVALUATE.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           MOVE XZC008R-CSR-ACT-NBR    TO XZT20O-CSR-ACT-NBR.
           MOVE XZC008R-NOT-PRC-TS     TO XZT20O-TK-NOT-PRC-TS.

           MOVE XZC008R-REC-SEQ-NBR    TO XZT20O-TK-REC-SEQ-NBR.
           IF XZC008R-REC-SEQ-NBR-SIGN = CF-NEGATIVE
               COMPUTE XZT20O-TK-REC-SEQ-NBR = XZT20O-TK-REC-SEQ-NBR
                                             * -1
           END-IF.

           MOVE XZC008R-POL-NBR        TO XZT20O-POL-NBR.
           MOVE XZC008R-ACT-NOT-POL-REC-CSUM
                                       TO XZT20O-TK-POL-REC-CSUM.

       13200-EXIT.
           EXIT.

