       IDENTIFICATION DIVISION.

       PROGRAM-ID.   XZ0G0006.
      *AUTHOR.       Gary Liedtke.
      *DATE-WRITTEN. 17 MAR 2009.
      ****************************************************************
      *  PROGRAM TITLE - BATCH COBOL FRAMEWORK PROXY PROGRAM FOR     *
      *                UOW : XZ_MAINTAIN_ACT_NOT                     *
      *                OPERATIONS : DeleteAccountNotification        *
      *                                                              *
      *    PURPOSE: The purpose of the program is to pass contract   *
      *             information from a consumer to the COBOL Business*
      *             Framework.                                       *
      *                                                              *
      *    NOTES & ASSUMPTIONS :                                     *
      *    *********************                                     *
      *     - INPUT/OUTPUT = LINKAGE SECTION                         *
      *     - THE MAX COPYBOOK SIZE ALLOWED FOR THE CONSUMER         *
      *       CONTRACT IS 32,767 BYTES                               *
      *                                                              *
      *                                                              *
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TO07614  18MAR09   E404GCL   INITIAL PROGRAM                 *
      * PP02500  07SEP12   E404BPO   RECOMPILE FOR XZ0T0006 CHANGE   *
      ****************************************************************

       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-INITIALIZATION-VALUE
                               VALUE SPACES            PIC X(01).
*NOTE * This is the name of the proxy program.
           03  CF-SERVICE-PROXY-PGM
                               VALUE 'XZ0X0006'        PIC X(08).

       01  ERROR-AND-ADVICE-MESSAGES.
           03  EA-01-CICS-LINK-ERROR.
               05  FILLER      VALUE 'PGM XZ0G0006 '   PIC X(13).
               05  FILLER      VALUE 'PARA = '         PIC X(07).
               05  EA-01-PARAGRAPH-NBR                 PIC X(04).
                   88  EA-01-PN-3000
                               VALUE '3000'.
               05  FILLER      VALUE ' CICS ERROR '    PIC X(12).
               05  FILLER      VALUE 'LINKING TO PGM ' PIC X(15).
               05  EA-01-LINK-PGM-NAME                 PIC X(08).
               05  FILLER      VALUE ' RESP = '        PIC X(08).
               05  EA-01-RESP                          PIC 9(08).
               05  FILLER      VALUE ' RESP2 = '       PIC X(09).
               05  EA-01-RESP2                         PIC X(05).
           03  EA-02-GETMAIN-FREEMAIN-ERROR.
               05  FILLER      VALUE 'PGM XZ0G0006 '   PIC X(13).
               05  FILLER      VALUE 'PARA = '         PIC X(07).
               05  EA-02-PARAGRAPH-NBR                 PIC X(04).
                   88  EA-02-PN-2100
                               VALUE '2100'.
                   88  EA-02-PN-8000
                               VALUE '8000'.
               05  FILLER      VALUE ' CICS ERROR '    PIC X(12).
               05  FILLER                              PIC X(11).
                   88  EA-02-GETMAIN
                               VALUE 'ALLOCATING '.
                   88  EA-02-FREEMAIN
                               VALUE 'RELEASING  '.
               05  FILLER      VALUE 'SYSTEM MEMORY '  PIC X(14).
               05  FILLER      VALUE ' RESP = '        PIC X(08).
               05  EA-02-RESP                          PIC 9(08).
               05  FILLER      VALUE ' RESP2 = '       PIC X(09).
               05  EA-02-RESP2                         PIC X(05).


       01  WS-MISC-WORK-FLDS.
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).


      ***  SERVICE PROXY CONTRACT AREA
       01  SERVICE-PROXY-CONTRACT.
           COPY TS020PRO.


       LINKAGE SECTION.

      ***  CONSUMER CONTRACT AREA
       01  DFHCOMMAREA.
           COPY XZ0Z0006.


      ***  SERVICE CONTRACT AREA
       01  L-SERVICE-CONTRACT.
           COPY XZ0T0006.


       PROCEDURE DIVISION.

       1000-MAINLINE.

           PERFORM 2000-BEGINNING-HOUSEKEEPING
              THRU 2000-EXIT.

           IF XZT006-NO-ERROR-CODE
               CONTINUE
           ELSE
               GO TO 1000-PROGRAM-EXIT
           END-IF.

           PERFORM 3000-CALL-SERVICE
              THRU 3000-EXIT.

           PERFORM 8000-ENDING-HOUSEKEEPING
              THRU 8000-EXIT.

       1000-PROGRAM-EXIT.
           EXEC CICS
               RETURN
           END-EXEC.
           GOBACK.


       2000-BEGINNING-HOUSEKEEPING.
      ****************************************************************
      * PERFORM INITIALIZATION AND OTHER SETUP TASKS.  ALLOCATE      *
      * SERVICE INPUT AND SERVICE OUTPUT AREAS.  GET ADDRESSABILITY  *
      * VIA THE POINTER PASSED IN.                                   *
      ****************************************************************

           INITIALIZE SERVICE-PROXY-CONTRACT
                      XZT006-ERROR-RETURN-CODE.

      ****************************************************************
      * FRAMEWORK FORMAT MEMORY NEEDS TO BE ALLOCATED BY THIS        *
      * PROGRAM BEFORE CALLING THE SERVICE PROXY PROGRAM.            *
      ****************************************************************

           PERFORM 2100-ALLOCATE-SERVICE-MEMORY
              THRU 2100-EXIT.

           IF XZT006-FATAL-ERROR-CODE
               GO TO 2000-EXIT
           END-IF.

       2000-EXIT.
           EXIT.


       2100-ALLOCATE-SERVICE-MEMORY.
      ****************************************************************
      * ALLOCATE THE ADDRESS FOR SENDING AND RECEIVING SERVICE INPUT *
      * AND OUTPUT DATA.                                             *
      ****************************************************************
           MOVE LENGTH OF L-SERVICE-CONTRACT
                                       TO PPC-SERVICE-DATA-SIZE.

           EXEC CICS GETMAIN
               SET (PPC-SERVICE-DATA-POINTER)
               FLENGTH (PPC-SERVICE-DATA-SIZE)
               INITIMG (CF-INITIALIZATION-VALUE)
               RESP (WS-RESPONSE-CODE)
               RESP2 (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET XZT006-FATAL-ERROR-CODE
                   EA-02-PN-2100
                   EA-02-GETMAIN       TO TRUE
               MOVE WS-RESPONSE-CODE   TO EA-02-RESP
               MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
               MOVE EA-02-GETMAIN-FREEMAIN-ERROR
                                       TO XZT006-ERROR-MESSAGE
               GO TO 2100-EXIT
           END-IF.

      *    SERVICE PROXY IS EXPECTING ADDRESS OF INPUT AND OUTPUT
      *    IN SERVICE DATA POINTER.
           SET ADDRESS OF L-SERVICE-CONTRACT
                                       TO PPC-SERVICE-DATA-POINTER.
           INITIALIZE L-SERVICE-CONTRACT.

       2100-EXIT.
           EXIT.


       3000-CALL-SERVICE.
      ****************************************************************
      * PERFORM SET UP AND MAKE THE CALL TO THE SERVICE PROXY PROGRAM*
      ****************************************************************

           PERFORM 3100-SET-UP-INPUT
              THRU 3100-EXIT.

           EXEC CICS LINK
               PROGRAM (CF-SERVICE-PROXY-PGM)
               COMMAREA (SERVICE-PROXY-CONTRACT)
               RESP (WS-RESPONSE-CODE)
               RESP2 (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET XZT006-FATAL-ERROR-CODE
                   EA-01-PN-3000       TO TRUE
               MOVE CF-SERVICE-PROXY-PGM
                                       TO EA-01-LINK-PGM-NAME
               MOVE WS-RESPONSE-CODE   TO EA-01-RESP
               MOVE WS-RESPONSE-CODE2  TO EA-01-RESP2
               MOVE EA-01-CICS-LINK-ERROR
                                       TO XZT006-ERROR-MESSAGE
               GO TO 3000-EXIT
           END-IF.

           IF PPC-NO-ERROR-CODE
               CONTINUE
           ELSE
               PERFORM 3300-CAPTURE-SVC-ERROR-INFO
                  THRU 3300-EXIT
               IF PPC-FATAL-ERROR-CODE
                 OR
                  PPC-NLBE-CODE
                   GO TO 3000-EXIT
               END-IF
           END-IF.

           PERFORM 3200-SET-UP-OUTPUT
              THRU 3200-EXIT.

       3000-EXIT.
           EXIT.


       3100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE SERVICE INPUT AREAS WITH VALUES FROM CONSUMER       *
      * CONTRACT                                                     *
      ****************************************************************

      *  SERVICE PARAMETERS

           MOVE XZT006-OPERATION       TO PPC-OPERATION.
           display 'g0006 ', XZT006-SERVICE-PARAMETERS.
           IF XZT006-BYPASS-SYNCPOINT
               SET PPC-BYPASS-SYNCPOINT-IN-MDRV
                                       TO TRUE
           ELSE
               SET PPC-DO-NOT-BYPASS-SYNCPOINT
                                       TO TRUE
           END-IF.

      *  SERVICE INPUT PARMATERS

*NOTE *    THE "XZT006-SERVICE-INPUTS" IS A FIELD IN THE PROXY COPYBOOK
*NOTE *    THE PROXY COYPBOOK IS INCLUDED IN THIS PROGRAM TWICE.  ONCE
*NOTE *    IN THE COPYBOOK THAT IS THE INTERFACE TO THIS PROGRAM AND
*NOTE *    BY DIRECT REFERENCE.
           MOVE XZT006-SERVICE-INPUTS  OF DFHCOMMAREA
                                       TO XZT006-SERVICE-INPUTS
                                       OF L-SERVICE-CONTRACT.

      *  INITIALIZE THE REST OF THE CONSUMER CONTRACT

           INITIALIZE XZT006-SERVICE-OUTPUTS
                                       OF DFHCOMMAREA
                      XZT006-SERVICE-ERROR-INFO.

       3100-EXIT.
           EXIT.


       3200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM SERVICE        *
      * CONTRACT AND PROXY.                                          *
      ****************************************************************

*NOTE *    THE "XZT006-SERVICE-OUTPUTS" IS A FIELD IN THE PROXY COPYBOOK
*NOTE *    THE PROXY COYPBOOK IS INCLUDED IN THIS PROGRAM TWICE.  ONCE
*NOTE *    IN THE COPYBOOK THAT IS THE INTERFACE TO THIS PROGRAM AND
*NOTE *    BY DIRECT REFERENCE.
           MOVE XZT006-SERVICE-OUTPUTS OF L-SERVICE-CONTRACT
                                       TO XZT006-SERVICE-OUTPUTS
                                       OF DFHCOMMAREA.

       3200-EXIT.
           EXIT.


       3300-CAPTURE-SVC-ERROR-INFO.
      ****************************************************************
      * POPULATE SERVICE ERROR AREA WITH VALUES FROM THE SERVICE     *
      * PROXY CONTRACT.                                              *
      ****************************************************************

           MOVE PPC-ERROR-RETURN-CODE  TO XZT006-ERROR-RETURN-CODE.

      *   CAPTURE THE HIGHEST LEVEL, FIRST OCCURING MESSAGE

           EVALUATE TRUE
               WHEN PPC-FATAL-ERROR-MESSAGE NOT = SPACES
                   MOVE PPC-FATAL-ERROR-MESSAGE
                                       TO XZT006-ERROR-MESSAGE
               WHEN PPC-NON-LOGGABLE-ERROR-CNT > +0
                   MOVE PPC-NON-LOG-ERR-MSG (1)
                                       TO XZT006-ERROR-MESSAGE
               WHEN PPC-WARNING-CNT > +0
                   MOVE PPC-WARN-MSG (1)
                                       TO XZT006-ERROR-MESSAGE
           END-EVALUATE.

       3300-EXIT.
           EXIT.


       8000-ENDING-HOUSEKEEPING.
      ****************************************************************
      * PERFORM CLEANUP TASKS SUCH AS FREEING MEMORY.                *
      ****************************************************************

           EXEC CICS FREEMAIN
               DATA (L-SERVICE-CONTRACT)
               RESP (WS-RESPONSE-CODE)
               RESP2 (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               SET XZT006-FATAL-ERROR-CODE
                   EA-02-PN-8000
                   EA-02-FREEMAIN      TO TRUE
               MOVE WS-RESPONSE-CODE   TO EA-02-RESP
               MOVE WS-RESPONSE-CODE2  TO EA-02-RESP2
               MOVE EA-02-GETMAIN-FREEMAIN-ERROR
                                       TO XZT006-ERROR-MESSAGE
               GO TO 8000-EXIT
           END-IF.

       8000-EXIT.
           EXIT.
