       IDENTIFICATION DIVISION.
       PROGRAM-ID.
                         HALRLEN1.
       AUTHOR.
                         J. A. FULCHER.
       DATE-WRITTEN.
                         SEP 2000.
       DATE-COMPILED.
      *****************************************************************
      **                                                             **
      ** PROGRAM TITLE -  DETERMINE THE LENGTH OF A FIELD.           **
      **                  FIELD LENGTH MUST BE BETWEEN 1 AND 999.    **
      **                                                             **
      ** PLATFORM - IBM MAINFRAME                                    **
      **                                                             **
      ** OPERATING SYSTEM - MVS                                      **
      **                                                             **
      ** LANGUAGE - COBOL                                            **
      **                                                             **
      ** PURPOSE -        DETERMINE THE LENGTH OF A FIELD            **
      **                  I.E. ESTABLISH THE POSITION OF THE LAST    **
      **                  NON SPACE CHARACTER IN A STRING.           **
      **                                                             **
      ** PROGRAM INITIATION -  THIS PROGRAM IS STARTED AS FOLLOWS:   **
      **                       1) STATICALLY CALLED BY A MODULE      **
      **                                                             **
      ** DATA ACCESS METHODS - DATA PASSED IN LINKAGE                **
      **                                                             **
      *****************************************************************
      *****************************************************************
      **               M A I N T E N A N C E    L O G                **
      **                                                             **
      ** SI #     DATE       PRGMR   DESCRIPTION                     **
      ** -------- ---------  ------  --------------------------------**
      ** SAVANNAH 23SEP00    JAF     NEW ROUTINE.                    **
      **                                                             **
      *****************************************************************
      *
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.
                         IBM-370.
       OBJECT-COMPUTER.
                         IBM-370.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       DATA DIVISION.
       FILE SECTION.
      *


       WORKING-STORAGE SECTION.

       01  FILLER                           PIC X(20)
           VALUE 'WS-START HALRLEN1  '.

       01  WS-WORK-FIELDS.
           03  WS-ERROR-FLAG                PIC X.
             88  ERROR-FLAGGED              VALUE 'Y'.
             88  ERROR-NOT-FLAGGED          VALUE 'N'.
           03  WS-STRING-LEN                PIC S9(4) BINARY.
           03  WS-POSN                      PIC S9(4) BINARY.

       LINKAGE SECTION.

       01  LI-STRING.
           03  LI-STRING-X                  PIC  X
                                            OCCURS 999
                                            INDEXED BY STRING-X-IND.

       01  LI-STRING-LEN                    PIC S9(4) BINARY.

       01  LO-STRING-LEN                    PIC S9(4) BINARY.


       PROCEDURE DIVISION USING LI-STRING LI-STRING-LEN LO-STRING-LEN.




       S0000-MAIN SECTION.
      ******************************************************************
      *                                                                *
      * CONTROLS PROCESSING                                            *
      *                                                                *
      ******************************************************************

           PERFORM 0100-INITIALIZATION.
           IF ERROR-FLAGGED
               GO TO 0000-MAIN-X
           END-IF.

           PERFORM 0200-DETERMINE-LENGTH.

       0000-MAIN-X.
           GOBACK.




       0100-INITIALIZATION SECTION.
      ******************************************************************
      *                                                                *
      * PERFORM INITIAL PROCESSING.                                    *
      *                                                                *
      ******************************************************************

           SET ERROR-NOT-FLAGGED TO TRUE.

      ** VALIDATE INPUT STRING LENGTH.
           IF LI-STRING-LEN < 0
               SET ERROR-FLAGGED TO TRUE
               MOVE 0 TO LO-STRING-LEN
               GO TO 0100-INITIALIZATION-X
           END-IF.

      ** DETERMINE INPUT STRING LENGTH.
           IF LI-STRING-LEN > 999
               MOVE 999 TO WS-STRING-LEN
           ELSE
               MOVE LI-STRING-LEN TO WS-STRING-LEN
           END-IF.

       0100-INITIALIZATION-X.
           EXIT.




       0200-DETERMINE-LENGTH SECTION.
      ******************************************************************
      *                                                                *
      * DETERMINE LAST NON SPACE CHARACTER IN INPUT STRING.            *
      *                                                                *
      ******************************************************************

           SET STRING-X-IND TO WS-STRING-LEN.
           PERFORM VARYING WS-POSN
                      FROM WS-STRING-LEN
                        BY -1
                     UNTIL WS-POSN < 1
                        OR LI-STRING-X(STRING-X-IND) NOT = SPACE
               SET STRING-X-IND DOWN BY 1
           END-PERFORM.

           MOVE WS-POSN TO LO-STRING-LEN.

       0200-DETERMINE-LENGTH-X.
           EXIT.
