       ID DIVISION.

       PROGRAM-ID.   XZ0X90O0.
      *AUTHOR.       DAVE POSSEHL.
      *DATE-WRITTEN. 02 APR 2009.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                  UOW       : XZ_GET_CNC_XML_REQ_LIST         *
      *                  OPERATION : GetCancXMLRequestList           *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * TO07614  04/02/2009 E404DAP   INITIAL PROGRAM                *
      * TO07614  04/16/2009 E404DAP   ADDED NOTIFICATION DATE        *
      * TO07614  05/04/2009 E404DAP   ADDED EMP ID AND TTY COPY IND  *
      * TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_GET_CNC_XML_REQ_LIST'
                                                       PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q90O0'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R90O0'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X90O0'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).


       01  SUBSCRIPTS.
           03  SS-TC                                   PIC S9(04) COMP.


       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-MAX-XML-REQ-ROWS             BINARY  PIC S9(04).
           03  WS-PROGRAM-NAME VALUE 'XZ0X90O0'        PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.


       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-XZ0A90O0.
           COPY XZ0A90O0 REPLACING ==:XZA9O0:== BY ==XZA9O0Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-XZ0A90O0      OCCURS 1000 TIMES
                                       INDEXED BY IX-RS.
           COPY XZ0A90O0 REPLACING ==:XZA9O0:== BY ==XZA9O0R==.


      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T90O0.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT90O-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT90O-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT90O-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT90O-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

      ***  CALCULATE MAX NUMBER OF OUTPUT ROWS
           COMPUTE WS-MAX-XML-REQ-ROWS
                               = LENGTH OF XZT9OO-TBL-OF-XML-REQ-ROWS
                               / LENGTH OF XZT9OO-XML-REQ-ROW.

           MOVE WS-MAX-XML-REQ-ROWS    TO XZA9O0Q-MAX-XML-REQ-ROWS.

      ***  IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
           IF XZT9OI-USERID NOT = SPACES
             AND
              XZT9OI-USERID NOT = LOW-VALUES
               MOVE XZT9OI-USERID      TO CSC-AUTH-USERID
           END-IF.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           SET IX-RS                   TO +1.
           MOVE +1                     TO SS-TC.

      ** LOOPING REQUIRED FOR POSSIBLE MULTIPLE XML REQ ROWS
         13200-A.

      ***  NO ACCOUNT NUMBER MEANS WE HAVE FINISHED
           IF XZA9O0R-CSR-ACT-NBR (IX-RS) = SPACES
               GO TO 13200-EXIT
           END-IF.

           MOVE XZA9O0R-CSR-ACT-NBR (IX-RS)
                                       TO XZT9OO-CSR-ACT-NBR (SS-TC).
           MOVE XZA9O0R-NOT-PRC-TS (IX-RS)
                                       TO XZT9OO-TK-NOT-PRC-TS (SS-TC).
           MOVE XZA9O0R-ACT-NOT-TYP-CD (IX-RS)
                                       TO XZT9OO-ACT-NOT-TYP-CD (SS-TC).
           MOVE XZA9O0R-NOT-DT (IX-RS) TO XZT9OO-NOT-DT (SS-TC).
           MOVE XZA9O0R-EMP-ID (IX-RS) TO XZT9OO-EMP-ID (SS-TC).
           MOVE XZA9O0R-ACT-TYP-CD (IX-RS)
                                       TO XZT9OO-ACT-TYP-CD (SS-TC).
           MOVE XZA9O0R-DOC-DES (IX-RS)
                                       TO XZT9OO-DOC-DES (SS-TC).
           MOVE XZA9O0R-TTY-CO-IND (IX-RS)
                                       TO XZT9OO-TTY-CO-IND (SS-TC).

           IF IX-RS = WS-MAX-XML-REQ-ROWS
               GO TO 13200-EXIT
           END-IF.

           SET IX-RS                   UP BY +1.
           ADD +1                      TO SS-TC.

           GO TO 13200-A.

       13200-EXIT.
           EXIT.

