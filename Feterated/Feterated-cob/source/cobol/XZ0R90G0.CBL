       ID DIVISION.                                                             
                                                                                
       PROGRAM-ID.   XZ0R90G0.                                                  
      *AUTHOR.       KRISTI SCHETTLER.                                          
      *DATE-WRITTEN. 02 MAR 2009.                                               
      ****************************************************************          
      *  PROGRAM TITLE - FRAMEWORK RESPONSE MODULE FOR               *          
      *                  UOW       : XZ_GET_CERT_RECIPIENT_LIST      *          
      *                  OPERATION : GetCertRecipientList            *          
      *                                                              *          
      *  PLATFORM - HOST CICS                                        *          
      *                                                              *          
      *  PURPOSE -  CONTROL THE COMMUNICATION SHELL RESPONSE PROCESS *          
      *                                                              *          
      *  PROGRAM INITIATION - LINKED TO FROM A FRAMEWORK MAIN DRIVER *          
      *                       PROGRAM(TS020000). NAME OF THE RESPONSE*          
      *                       MODULE IS PASSED AS A PARAMETER TO THE *          
      *                       MAIN DRIVER.                           *          
      *                                                              *          
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *          
      *                        OUTPUT RETURNED VIA DFHCOMMAREA (FOR  *          
      *                        COMMON PARAMETERS) AND CICS MAIN      *          
      *                        STORAGE (OPERATION SPECIFIC DATA)     *          
      *                                                              *          
      ****************************************************************          
      ****************************************************************          
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *          
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *          
      *        APPLICATION CODING.                                   *          
      *                                                              *          
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *          
      *                                                              *          
      *   WR #    DATE     EMP ID              DESCRIPTION           *          
      * -------- --------- -------   ------------------------------- *          
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *          
      * YJ249    27APR07   E404NEM   STDS CHGS                       *          
      ****************************************************************          
      ****************************************************************          
      *                                                              *          
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *          
      *                                                              *          
      *   WR #    DATE     EMP ID              DESCRIPTION           *          
      * ------- ---------- -------   ------------------------------- *          
      * TO07614 03/02/2009 E404KXS   INITIAL PROGRAM                 *          
      * PP02500 10/17/2012 E404BPO   RECOMPILE FOR XZ0A90G0 CHANGE   *          
      *                                                              *          
      ****************************************************************          
                                                                                
                                                                                
       ENVIRONMENT DIVISION.                                                    
       DATA DIVISION.                                                           
       WORKING-STORAGE SECTION.                                                 
                                                                                
                                                                                
       01  CONSTANT-FIELDS.                                                     
           03  CF-BPO-BUSINESS-OBJECT-NAMES.                                    
               05  CF-BUS-OBJ-NM-LIST-KEY                                       
                               VALUE 'XZ_GET_CERT_REC_LIST_KEY'                 
                                                       PIC X(32).               
               05  CF-BUS-OBJ-NM-LIST-DETAIL                                    
                               VALUE 'XZ_GET_CERT_REC_LIST_DETAIL'              
                                                       PIC X(32).               
           03  CF-INVALID-OPERATION.                                            
               05  FILLER      VALUE 'RESPONSE MODULE' PIC X(15).               
               05  FILLER      VALUE ' - INVALID '     PIC X(11).               
               05  FILLER      VALUE 'OPERATION'       PIC X(09).               
                                                                                
                                                                                
       01  ERROR-AND-ADVICE-MESSAGES.                                           
           03  EA-01-PROGRAM-LINK-FAILED.                                       
               05  FILLER      VALUE 'CALL TO '        PIC X(08).               
               05  EA-01-FAILED-LINK-PGM-NAME          PIC X(08).               
               05  FILLER      VALUE ' FAILED.'        PIC X(08).               
           03  EA-02-INVALID-OPERATION-MSG.                                     
               05  FILLER      VALUE 'XZ0R90G0 - '     PIC X(11).               
               05  FILLER      VALUE 'INVALID OPER: '  PIC X(14).               
               05  EA-02-INVALID-OPERATION-NAME        PIC X(32).               
                                                                                
                                                                                
       01  WORKING-STORAGE-AREA.                                                
           03  WS-PROGRAM-NAME VALUE 'XZ0R90G0'        PIC X(08).               
           03  WS-APPLICATION-NM                                                
                               VALUE 'CRS'             PIC X(08).               
           03  WS-OPERATION-NAME                       PIC X(32).               
               88  WS-GET-CERT-REC-LIST                                         
                               VALUE 'GetCertRecipientList'.                    
               88  WS-VALID-OPERATION                                           
                               VALUE 'GetCertRecipientList'.                    
                                                                                
      *** LAYOUT USED TO PASS DATA TO FRAMEWORK SUPPLIED                        
      *** RESPONSE UMT ACCESS MODULE.                                           
       01  WS-HALRRESP-LINKAGE.                                                 
       COPY HALLRESP.                                                           
                                                                                
                                                                                
      *** LAYOUT USED TO PASS DATA TO TABLE SPECIFIC                            
      *** DATA FORMATTER PROGRAMS                                               
       01  TABLE-FORMATTER-DATA.                                                
           COPY TS020TBL.                                                       
                                                                                
                                                                                
      ****************************************************************          
      * START OF:                                                    *          
      *     GENERAL BPO/COMM SHELL PROGRAM WORKING-STORAGE           *          
      *     (INCLUDED IN ALL BPOS/COMM SHELL PROGRAMS)               *          
      ****************************************************************          
                                                                                
                                                                                
                                                                                
                                                                                
      ******************************************************************        
      ***CSC *  START OF:                          *BUSINESS FRAMEWORK**        
      ***CSC *                                     *BUSINESS FRAMEWORK**        
      ***CSC *  COMMON WORKING-STORAGE             *BUSINESS FRAMEWORK**        
      ***CSC *  (NOT SPECIFIC TO ANY MODULE        *BUSINESS FRAMEWORK**        
      ***CSC *   OR TYPES OF MODULE)               *BUSINESS FRAMEWORK**        
      ***CSC *                                     *BUSINESS FRAMEWORK**        
      ***CSC *  VERSION 1.0 FEB. 02, 2001          *BUSINESS FRAMEWORK**        
      ***CSC *                                     *BUSINESS FRAMEWORK**        
      ******************************************************************        
                                                                                
      ** SQL AREAS                                                              
                                                                                
           EXEC SQL                                                             
               INCLUDE SQLCA                                                    
           END-EXEC.                                                            
                                                                                
           EXEC SQL                                                             
               INCLUDE XPXLERD                                                  
           END-EXEC.                                                            
                                                                                
       01  WS-NOT-SPECIFIC-MISC.                                                
           05  WS-HALRLODR-NAME          PIC X(08) VALUE 'HALRLODR'.            
           05  WS-HALRMON-NAME           PIC X(08) VALUE 'HALRMON '.            
           05  WS-RESPONSE-CODE          PIC S9(08) BINARY.                     
           05  WS-RESPONSE-CODE2         PIC S9(08) BINARY.                     
           05  WS-DATA-KEY-DISP1         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP2         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP3         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP4         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP5         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP6         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP7         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP8         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP9         PIC -Z(8)9.                            
           05  WS-DATA-KEY-DISP10        PIC -Z(8)9.                            
           05  WS-DATA-DEC-DISP1         PIC -Z(4)9V9(5).                       
           05  WS-PROCESS-CHILD-SW       PIC X(01) VALUE 'C'.                   
            88 CALL-CHILD                  VALUE 'C'.                           
            88 BYPASS-CHILD                VALUE 'B'.                           
           05  WS-CHILD-BUS-OBJ-NM       PIC X(32).                             
           05  WS-CHILD-BUS-OBJ-MDU      PIC X(32).                             
                                                                                
       01  COMMON-INFO.                                                         
           COPY HALLCOM.                                                        
                                                                                
       01  HALRMON-PERF-MONITOR-STORAGE.                                        
           03  HALRMON-DUMMY-INFO-TEXT   PIC X(01) VALUE SPACES.                
           COPY HALLMON.                                                        
                                                                                
      *** IAP DATE MODULE                                                       
           COPY XPXLDAT.                                                        
                                                                                
      *** GENERAL DATE VARIABLES                                                
       01 WS-DEFAULT-DATES.                                                     
           05  WS-DEFAULT-EXP-DT         PIC X(10) VALUE '9999-12-31'.          
           05  WS-DEFAULT-EXP-TS         PIC X(26)                              
                                     VALUE '9999-12-31-00.00.00.000000'.        
                                                                                
       01  WS-SE3-CURRENT-ISO-TS-AREA.                                          
           05 WS-SE3-CUR-ISO-DATE-TIME.                                         
              07 WS-SE3-CUR-ISO-DATE            PIC X(10).                      
              07 WS-SE3-CUR-ISO-TIME            PIC X(16).                      
           05 WS-CURRENT-SE3-TIMESTAMP REDEFINES                                
              WS-SE3-CUR-ISO-DATE-TIME.                                         
              07 WS-WORK-CUR-TS-YR              PIC X(4).                       
              07 WS-WORK-CUR-TS-SEP1            PIC X.                          
              07 WS-WORK-CUR-TS-MO              PIC X(2).                       
              07 WS-WORK-CUR-TS-SEP2            PIC X.                          
              07 WS-WORK-CUR-TS-DAY             PIC X(2).                       
              07 WS-WORK-CUR-TS-SEP3            PIC X.                          
              07 WS-WORK-CUR-TS-HR              PIC X(2).                       
              07 WS-WORK-CUR-TS-SEP4            PIC X.                          
              07 WS-WORK-CUR-TS-MIN             PIC X(2).                       
              07 WS-WORK-CUR-TS-SEP5            PIC X.                          
              07 WS-WORK-CUR-TS-SEC             PIC X(2).                       
              07 WS-WORK-CUR-TS-SEP6            PIC X.                          
              07 WS-WORK-CUR-TS-MILSEC          PIC X(6).                       
                                                                                
                                                                                
      ** DCLGEN USED FOR ACCESSING HAL_UOW_OBJ_HIER_V TABLE                     
           EXEC SQL                                                             
               INCLUDE HALLGUOH                                                 
           END-EXEC.                                                            
                                                                                
      ** DCLGEN USED FOR ACCESSING HAL_BO_MDU_XREF TABLE                        
           EXEC SQL                                                             
                INCLUDE HALLGBMX                                                
           END-EXEC.                                                            
                                                                                
      *** UOW BUSINESS OBJECT HIERARCHY TABLE                                   
      *** CURSORED TO FIND NEXT ENTRIES ON THE HIERARCHY TABLE                  
           EXEC SQL                                                             
             DECLARE BO-HIER-CURS0 CURSOR FOR                                   
                 SELECT HUOH_HIER_SEQ_NBR,                                      
                        HUOH_PNT_BOBJ_NM,                                       
                        HUOH_CHD_BOBJ_NM                                        
                 FROM   HAL_UOW_OBJ_HIER_V                                      
                 WHERE  UOW_NM             = :HUOH-UOW-NM                       
                   AND  ROOT_BOBJ_NM       = :HUOH-ROOT-BOBJ-NM                 
                   AND  HUOH_PNT_BOBJ_NM   = :HUOH-PNT-BOBJ-NM                  
                 ORDER BY HUOH_HIER_SEQ_NBR                                     
                 FOR READ ONLY                                                  
           END-EXEC.                                                            
                                                                                
                                                                                
      *****************************************************************         
      ***CSC *  START OF:                          *BUSINESS FRAMEWORK*         
      ***CSC *                                     *BUSINESS FRAMEWORK*         
      ***CSC *  ERRORS AND WARNING WORKING-STORAGE *BUSINESS FRAMEWORK*         
      ***CSC *  (SPECIFIC TO ERROR AND WARNING     *BUSINESS FRAMEWORK*         
      ***CSC *  REPORTING ONLY)                    *BUSINESS FRAMEWORK*         
      ***CSC *                                     *BUSINESS FRAMEWORK*         
      ***CSC *  VERSION 1.0 FEB 02, 2001           *BUSINESS FRAMEWORK*         
      *****************************************************************         
      *****************************************************************         
      **         M A I N T E N A N C E     L O G                     **         
      **                                                             **         
      ** CASE #    DATE      PGMR    DESCRIPTION                     **         
      ** ------  ---------  ------   ------------------------------- **         
C14849** C14849  6/22/2001  ARSI600  CHANGED LENGTH OF SOME          **         
      **                             PLACEHOLDER VALUES.             **         
      ** 024179  6/25/2002  SE3H929  ADD A SINGLE 500 BYTE NON-LOGG  **         
      **                             -ABLE ERROR/WARNING MESSAGE AND **         
      **                             INITIALIZE STORAGE FOR          **         
      **                             WS-NONLOG-PLACEHOLDER-VALUES.   **         
      *****************************************************************         
                                                                                
       01  WS-ERR-WARN-VARIABLES.                                               
           05  WS-APPLID                      PIC X(08).                        
                                                                                
       01  WS-ERR-WARN-SWITCHES.                                                
           05  WS-LOG-WARNING-OR-ERROR-SW     PIC X(01).                        
               88 WS-LOG-WARNING                VALUE 'W'.                      
               88 WS-LOG-ERROR                  VALUE 'E'.                      
           05  WS-NON-LOGGABLE-WARN-OR-ERR-SW PIC X(01).                        
               88 WS-NON-LOGGABLE-WARNING       VALUE 'W'.                      
               88 WS-NON-LOGGABLE-BUS-ERR       VALUE 'E'.                      
                                                                                
       01  WS-NONLOG-PLACEHOLDER-VALUES.                                        
           05  WS-NONLOG-ERR-COL1-NAME        PIC X(32)  VALUE SPACES.          
C14849*    05  WS-NONLOG-ERR-COL1-VALUE       PIC X(50).                        
C14849     05  WS-NONLOG-ERR-COL1-VALUE       PIC X(75)  VALUE SPACES.          
           05  WS-NONLOG-ERR-COL2-NAME        PIC X(32)  VALUE SPACES.          
C14849*    05  WS-NONLOG-ERR-COL2-VALUE       PIC X(50).                        
C14849     05  WS-NONLOG-ERR-COL2-VALUE       PIC X(75)  VALUE SPACES.          
C14849*    05  WS-NONLOG-ERR-CONTEXT-TEXT     PIC X(32).                        
C14849     05  WS-NONLOG-ERR-CONTEXT-TEXT     PIC X(100) VALUE SPACES.          
C14849*    05  WS-NONLOG-ERR-CONTEXT-VALUE    PIC X(50).                        
C14849     05  WS-NONLOG-ERR-CONTEXT-VALUE    PIC X(100) VALUE SPACES.          
024179     05  WS-NONLOG-ERR-ALLTXT-TEXT      PIC X(500) VALUE SPACES.          
C14849     05  WS-ERR-WNG-TXT-D               PIC X(500) VALUE SPACES.          
                                                                                
      ** WARNING RESPONSE UMT MSG AREA                                          
       01  WS-WARNING-UMT-AREA.                                                 
           COPY HALLUWRN.                                                       
                                                                                
      ** NON-LOGGABLE BUS ERRS AREA                                             
       01  WS-NLBE-UMT-AREA.                                                    
           COPY HALLNLBE.                                                       
                                                                                
      ** HALOESTO LOG WARNING/ ERROR LINKAGE                                    
       01  WS-ESTO-INFO.                                                        
           COPY HALLESTO.                                                       
                                                                                
      ** DCLGEN USED FOR ACCESSING THE NON-LOGGABLE BUSINESS                    
      ** ERRORS AND WARNINGS TRANSLATION TABLE                                  
           EXEC SQL                                                             
               INCLUDE HALLGNLB                                                 
           END-EXEC.                                                            
                                                                                
                                                                                
                                                                                
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
      *** COMMON LAYOUT USED BETWEEN ALL COMM SHELL PROGRAMS                    
       COPY TS020COM.                                                           
                                                                                
                                                                                
      ** RESPONSE/OUTPUT COPYBOOKS FOR THIS OPERATION                           
       01  L-FRAMEWORK-RESPONSE-AREA.                                           
           03  L-FW-RESP-XZ0A90G0.                                              
           COPY XZ0A90G0 REPLACING ==:XZA9G0:== BY ==XZA9G0==.                  
           03  L-FW-RESP-XZ0A90G1      OCCURS 1000 TIMES                        
                                       INDEXED BY IX-RS.                        
           COPY XZ0A90G1 REPLACING ==:XZA9G1:== BY ==XZA9G1==.                  
                                                                                
                                                                                
       PROCEDURE DIVISION.                                                      
                                                                                
       1000-MAINLINE SECTION.                                                   
      ****************************************************************          
      * CONTROLS MAINLINE PROGRAM PROCESSING                         *          
      ****************************************************************          
                                                                                
           PERFORM 2000-BEGINNING-HOUSEKEEPING.                                 
           IF UBOC-HALT-AND-RETURN                                              
               GO TO 1000-EXIT                                                  
           END-IF.                                                              
                                                                                
           PERFORM 3000-CREATE-OPERATION-RESP.                                  
           IF UBOC-HALT-AND-RETURN                                              
               GO TO 1000-EXIT                                                  
           END-IF.                                                              
                                                                                
       1000-EXIT.                                                               
                                                                                
           EXEC CICS                                                            
               RETURN                                                           
           END-EXEC.                                                            
           GOBACK.                                                              
                                                                                
                                                                                
       2000-BEGINNING-HOUSEKEEPING SECTION.                                     
      ****************************************************************          
      * PERFORM STARTUP/INITIALIZATION PROCESSING                    *          
      ****************************************************************          
                                                                                
      * INITIALIZE ERROR PROCESSING FIELDS                                      
           INITIALIZE ESTO-STORE-INFO                                           
                      ESTO-RETURN-INFO.                                         
                                                                                
           PERFORM 2100-DETERMINE-OPERATION.                                    
           IF UBOC-HALT-AND-RETURN                                              
               GO TO 2000-EXIT                                                  
           END-IF.                                                              
                                                                                
       2000-EXIT.                                                               
           EXIT.                                                                
                                                                                
                                                                                
       2100-DETERMINE-OPERATION SECTION.                                        
      ****************************************************************          
      * VALIDATE THAT THIS MODULE CAN BE CALLED FOR THIS OPERATION.  *          
      * IF NOT, RAISE A LOGGABLE ERROR.                              *          
      ****************************************************************          
                                                                                
           MOVE CSC-OPERATION          TO WS-OPERATION-NAME.                    
                                                                                
           IF WS-VALID-OPERATION                                                
               SET ADDRESS OF L-FRAMEWORK-RESPONSE-AREA                         
                                       TO MA-OUTPUT-POINTER                     
               GO TO 2100-EXIT                                                  
           END-IF.                                                              
                                                                                
           SET WS-LOG-ERROR            TO TRUE.                                 
           SET EFAL-SYSTEM-ERROR       TO TRUE.                                 
           SET EFAL-BUS-PROCESS-FAILED TO TRUE.                                 
           MOVE CF-INVALID-OPERATION   TO EFAL-ACTION-BUFFER.                   
           MOVE WS-PROGRAM-NAME        TO EFAL-ERR-OBJECT-NAME.                 
           MOVE '2100-DETERMINE-OPERATION'                                      
                                       TO EFAL-ERR-PARAGRAPH.                   
           MOVE WS-OPERATION-NAME      TO EA-02-INVALID-OPERATION-NAME.         
           MOVE EA-02-INVALID-OPERATION-MSG                                     
                                       TO EFAL-ERR-COMMENT                      
                                          EFAL-OBJ-DATA-KEY.                    
                                                                                
           PERFORM 9000-LOG-WARNING-OR-ERROR.                                   
                                                                                
       2100-EXIT.                                                               
           EXIT.                                                                
                                                                                
                                                                                
       3000-CREATE-OPERATION-RESP SECTION.                                      
      ******************************************************************        
      * RETRIEVE FRAMEWORK RESPONSE ROWS FROM RESPONSE DATA UMT        *        
      * A SEPARATE SECTION IS INCLUDED FOR EACH ROW RETRIEVED          *        
      ******************************************************************        
                                                                                
           PERFORM 3100-GET-REC-LIST-KEY.                                       
           IF UBOC-HALT-AND-RETURN                                              
               GO TO 3000-EXIT                                                  
           END-IF.                                                              
                                                                                
           PERFORM 3200-GET-REC-LIST-DETAIL.                                    
           IF UBOC-HALT-AND-RETURN                                              
               GO TO 3000-EXIT                                                  
           END-IF.                                                              
                                                                                
       3000-EXIT.                                                               
           EXIT.                                                                
                                                                                
                                                                                
       3100-GET-REC-LIST-KEY SECTION.                                           
      ******************************************************************        
      * RETRIEVE RESPONSE ROWS TO GET RECIPIENT KEYS                   *        
      ******************************************************************        
                                                                                
           SET HALRRESP-READ-FUNC      TO TRUE.                                 
                                                                                
           MOVE CF-BUS-OBJ-NM-LIST-KEY TO HALRRESP-BUS-OBJ-NM.                  
           MOVE 1                      TO HALRRESP-REC-SEQ.                     
           MOVE LENGTH OF L-FW-RESP-XZ0A90G0                                    
                                       TO HALRRESP-BUS-OBJ-DATA-LEN.            
           INITIALIZE L-FW-RESP-XZ0A90G0.                                       
                                                                                
           CALL HALRRESP-HALRRESP-LIT USING                                     
                DFHEIBLK                                                        
                DFHCOMMAREA                                                     
                UBOC-RECORD                                                     
                WS-HALRRESP-LINKAGE                                             
                L-FW-RESP-XZ0A90G0.                                             
                                                                                
           IF UBOC-HALT-AND-RETURN                                              
             OR                                                                 
              HALRRESP-REC-NOT-FOUND                                            
               GO TO 3100-EXIT                                                  
           END-IF.                                                              
                                                                                
       3100-EXIT.                                                               
           EXIT.                                                                
                                                                                
                                                                                
       3200-GET-REC-LIST-DETAIL SECTION.                                        
      ******************************************************************        
      * RETRIEVE RESPONSE ROWS TO GET RECIPIENT LIST INFORMATION       *        
      ******************************************************************        
                                                                                
           SET IX-RS                   TO +1.                                   
           SET HALRRESP-READ-FUNC      TO TRUE.                                 
                                                                                
           MOVE CF-BUS-OBJ-NM-LIST-DETAIL                                       
                                       TO HALRRESP-BUS-OBJ-NM.                  
           MOVE 1                      TO HALRRESP-REC-SEQ.                     
           MOVE LENGTH OF L-FW-RESP-XZ0A90G1(IX-RS)                             
                                       TO HALRRESP-BUS-OBJ-DATA-LEN.            
                                                                                
         3200-A.                                                                
                                                                                
           CALL HALRRESP-HALRRESP-LIT USING                                     
                DFHEIBLK                                                        
                DFHCOMMAREA                                                     
                UBOC-RECORD                                                     
                WS-HALRRESP-LINKAGE                                             
                L-FW-RESP-XZ0A90G1(IX-RS).                                      
                                                                                
           IF UBOC-HALT-AND-RETURN                                              
             OR                                                                 
              HALRRESP-REC-NOT-FOUND                                            
               GO TO 3200-EXIT                                                  
           END-IF.                                                              
                                                                                
           ADD 1                       TO HALRRESP-REC-SEQ.                     
           SET IX-RS                   UP BY +1.                                
           GO TO 3200-A.                                                        
                                                                                
       3200-EXIT.                                                               
           EXIT.                                                                
                                                                                
                                                                                
      *---*                                                                     
      *  COPYBOOK FOR REPORTING LOGGABLE AND NON LOGGABLE                       
      *  WARNINGS AND ERRORS                                                    
      *---*                                                                     
           EXEC SQL                                                             
               INCLUDE HALCWAER                                                 
           END-EXEC.                                                            
