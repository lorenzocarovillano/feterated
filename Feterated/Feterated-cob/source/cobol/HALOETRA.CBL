       IDENTIFICATION DIVISION.
       PROGRAM-ID.  HALOETRA.
      *--------------------------------------------------------------*
      *  PROGRAM TITLE - SERIES III INTERFACE - ERROR TRANSLATION    *
      *                                         MODULE               *
      *  WRITTEN BY:  PMSC                                           *
      *                                                              *
      *  DATE WRITTEN: APR 2000                                      *
      *                                                              *
      *  PLATFORM - HOST                                             *
      *                                                              *
      *  OPERATING SYSTEM - MVS                                      *
      *                                                              *
      *  LANGUAGE - MVS COBOL                                        *
      *                                                              *
      *  PURPOSE - THIS MODULE IS THE MAIN AQ2 ERROR TRANSLATION     *
      *            MODULE                                            *
      *                                                              *
      *  PROGRAM INITIATION - CICS LINK FROM HALOESTO WHICH IS THE   *
      *                       MAIN ERROR HANDLING MODULE             *
      *                                                              *
      *  DATA ACCESS METHODS - PASSED INFORMATION                    *
      *                                                              *
      *--------------------------------------------------------------*
      *--------------------------------------------------------------*
      *                M A I N T E N A N C E    L O G
      *
      * SI#      DATE      PRGMR     DESCRIPTION
      * -------- --------- --------- ---------------------------------
      * SAVANNAH 12APR00   LCP       CREATED.
      *
      * 17238    01NOV01   AICI448   ACCOUNT FOR LOCKING ERRORS.
      * 17241    13NOV01   03539     REPLACE REFERENCES TO IAP WITH
      *                              COMPARABLE INFRASTRUCTURE CODE.
      * 17241    20DEC01   18448     REMOVE IAP DATE ROUTINE COPYBOOK.
      * 17330    15JAN02   18448     NEW ID GENERATION ALGORITHM
      *  (GA)                        REQUIRES UBOC-MSG-ID. ALSO,
      *                              UBOC-UOW-NAME NEEDED FOR PROPER
      *                              FUNCTIONING OF HALCWAER IN CALL
      *                              TO ID GENERATOR.
      *                              (RETRO'D FROM BF 2.2 GA)
      * 18687    16JAN02   18448     NOW CALLING BF ID BUILDER TO
      *                              CREATE ERROR REF NUMBER.
      * 20070B   29MAR02   18448     CONVERT ABENDS TO LOGGABLE ERRORS
      *                              BEFORE RETURNING TO MIDDLEWARE.
      * 28729    07JAN03   18448     ADD PROCESSING FOR NEW 88S ADDED
      *                              FOR MIP USAGE IMPROVEMENTS.
      *--------------------------------------------------------------*
       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.

       FILE-CONTROL.

       DATA DIVISION.

       FILE SECTION.

      *--------------------------------------------------------------*
      *               W O R K I N G   S T O R A G E                  *
      *--------------------------------------------------------------*
       WORKING-STORAGE SECTION.

      *---*
      * START OF WORKING STORAGE
      *---*
       01  BEGIN-OF-WORKING-STORAGE.
           03  FILLER                      PIC X(08) VALUE 'HALOETRA'.
           03  FILLER                      PIC X(28)
               VALUE ' - START OF WORKING STORAGE'.

      *---*
      * SET UP SQL
      *---*
           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

17241***   EXEC SQL
17241***       INCLUDE XPXLERD
17241***   END-EXEC.

17241      EXEC SQL
17241          INCLUDE HALLERD
17241      END-EXEC.

           EXEC SQL
               INCLUDE HALLGETS
           END-EXEC.


17241**---*
17241** DATE ROUTINE COMMUNICATION AREA
17241**---*
17241**    COPY XPXLDAT.

      *---*
      * WORK AREAS.
      *---*
       01  WS-WORK-AREAS.
           03  WS-PROGRAM-NAME                 PIC X(08)
                                                  VALUE 'HALOETRA'.
           03  WS-HALOETRA-NAME                PIC X(08)
                                                  VALUE 'HALOETRA'.
           03  WS-TEMP-TIMESTAMP               PIC X(26).
           03  WS-RESPONSE-CODE                PIC S9(08) COMP.
           03  WS-RESPONSE-CODE2               PIC S9(08) COMP.
           03  WS-CICS-RESP.
             05 WS-CICS-RESP-CODE              PIC  9(9).
20070B     03  WS-ABEND-ABSTIME                PIC S9(15) COMP-3.
20070B     03  WS-ABEND-9-ABSTIME              PIC  9(15).

       01  W-WORKFIELDS.
           03  W-RESPONSE                      PIC S9(08) COMP.
           03  W-RESPONSE2                     PIC S9(08) COMP.
           03  W-COUNT                         PIC 9.
           03  W-READ-VSAM-IND                 PIC 9(01).
             88 VSAM-RECORD-NOT-FOUND             VALUE 0.
             88 VSAM-RECORD-FOUND                 VALUE 1.
           03  W-HALOUIDG-SEARCH-IND           PIC 9(01).
             88 DETERMINE-ERR-REF                 VALUE 0.
             88 ERROR-CODE-OK                     VALUE 1.
             88 ERROR-FOUND-NO-RETRY              VALUE 2.

      *---*
      * HALOETRA - LINKAGE
      *---*
       01  WS-HALOETRA-LINKAGE.
           COPY HALLESTO.

      *---*
18687 * HALOUBOC - LINKAGE - FOR CALL TO HALUIDB/HALOUIDG
      *---*
       01  WS-HALOUBOC-LINKAGE.
           COPY HALLUBOC.

      *---*
18687 * LINKAGE - FOR CALL TO HALUIDB/HALOUIDG
      *---*
       01  WS-HALOUIDG-LINKAGE.
           COPY HALLUIDG.

      *---*
      * END OF WORKING STORAGE
      *---*
       01  END-OF-WORKING-STORAGE.
           05  FILLER                    PIC X(08) VALUE 'HALOETRA'.
           05  FILLER                    PIC X(25)
               VALUE ' - END OF WORKING STORAGE'.

       LINKAGE SECTION.
       01  DFHCOMMAREA.
           03  LNK-CA-DATA                   PIC X OCCURS 32767
                                                DEPENDING ON EIBCALEN.

      *--------------------------------------------------------------*
      *           P R O C E D U R E   D I V I S I O N                *
      *--------------------------------------------------------------*
       PROCEDURE DIVISION.




       0000-MAINLINE SECTION.
      ****************************************************************
      * A) INITIALISE ARRAYS                                         *
      * B) VALIDATE ESTO-ACTION-BUFFER                               *
      * C) RUN MAIN PROCESS TO MATCH ACTION BUFFER ENTRIES TO ERRORS *
      * D) RETURN.                                                   *
      ****************************************************************

           MOVE DFHCOMMAREA(1:LENGTH OF WS-HALOETRA-LINKAGE)
                TO WS-HALOETRA-LINKAGE.

           PERFORM 0100-INITIALIZE.

           PERFORM 0120-VALIDATE-INPUT.

           IF NOT ESTO-TRAN-AND-STORAGE-OK
               GO TO 0000-RETURN
           END-IF.

      *---*
      *   PERFORM MATCHING
      *---*
           PERFORM 0500-PERFORM-MATCHING.


       0000-RETURN.
           MOVE WS-HALOETRA-LINKAGE TO DFHCOMMAREA.

           EXEC CICS RETURN END-EXEC.
           GOBACK.




       0100-INITIALIZE SECTION.
      ****************************************************************
      * A>  INITIALIZE                                               *
      ****************************************************************

           INITIALIZE W-WORKFIELDS.

       0100-INITIALIZE-X.
           EXIT.




       0120-VALIDATE-INPUT SECTION.
      ****************************************************************
      * THIS SECTION VALIDATES ESTO-ACTION-BUFFER AGAINST            *
      * EFAL-FAILED-ACTION-TYPE PRIOR TO MATCHING.                   *
      *                                                              *
      * A>  VALIDATE ESTO LINKAGE                                    *
      * B>  VALIDATE DB2 FAILS                                       *
      * C>  VALIDATE CICS FAILS                                      *
      * D>  VALIDATE IAP FAILS                --                     *
      * E>  VALIDATE COMMAREA FAILS           --                     *
      * F>  VALIDATE SECURITY FAILS           --  NEED TO BE DONE    *
      * G>  VALIDATE DATA PRIVACY FAILS       --                     *
      * H>  VALIDATE BUSINESS PROCESS FAILS   --                     *
      * H>  VALIDATE ABEND FAILS                                     *
28729 * H>  VALIDATE TRANSFER FAILS                                  *
      ****************************************************************

           IF ETRA-ERR-ACTION = (SPACES OR LOW-VALUES)
               SET ESTO-ERR-TRANS-FAILED         TO TRUE
               SET ESTO-ACTION-NOT-FILLED        TO TRUE
               GO TO 0120-VALIDATE-INPUT-X
           END-IF.

           IF EFAL-FAILED-ACTION-TYPE = (SPACES OR LOW-VALUES)
               SET ESTO-ERR-TRANS-FAILED         TO TRUE
               SET ESTO-FAILURE-ACTION-NOT-FILLED
                                                 TO TRUE
               GO TO 0120-VALIDATE-INPUT-X
           END-IF.

           EVALUATE EFAL-FAILED-ACTION-TYPE

               WHEN 'DB2'
                   IF EFAL-DB2-ERR-SQLCODE = ZEROS
                       SET ESTO-ERR-TRANS-FAILED         TO TRUE
                       SET ESTO-DB2-SQLCODE-NOT-FILLED   TO TRUE
                   END-IF

               WHEN 'CICS'
                   IF EFAL-CICS-ERR-RESP = ZEROS
                       SET ESTO-ERR-TRANS-FAILED         TO TRUE
                       SET ESTO-CICS-ERR-RESP-NOT-FILLED TO TRUE
                   END-IF

               WHEN 'IAP'
               WHEN 'COMMAREA'
               WHEN 'SECURITY'
               WHEN 'DATAPRIV'
               WHEN 'BUSPROC'
               WHEN 'AUDIT'
               WHEN 'LOCKING'
20070B         WHEN 'ABEND'
28729          WHEN 'TRANSFER'
                   CONTINUE
               WHEN OTHER
                   SET ESTO-ERR-TRANS-FAILED         TO TRUE
                   SET ESTO-FAILURE-ACTION-INVALID   TO TRUE
           END-EVALUATE.

       0120-VALIDATE-INPUT-X.
           EXIT.




       0500-PERFORM-MATCHING SECTION.
      ****************************************************************
      * A)PERFORM MATCHING OF ERRORS TO ERROR CODES AND PRORITY LEVEL*
      * B)UPDATE HALLESTO LINKAGE WITH RESULT                        *
      ****************************************************************
      *---*
      *   DETERMINE MATCHING SECTION
      *---*
       0500-MATCHING.
           EVALUATE EFAL-FAILED-ACTION-TYPE
               WHEN 'DB2'
                   PERFORM 0510-MATCH-DB2
               WHEN 'CICS'
                   PERFORM 0520-MATCH-CICS
               WHEN 'IAP'
                   PERFORM 0530-MATCH-IAP
               WHEN 'COMMAREA'
                   PERFORM 0540-MATCH-COMA
               WHEN 'SECURITY'
                   PERFORM 0550-MATCH-SECU
               WHEN 'DATAPRIV'
                   PERFORM 0560-MATCH-DATP
               WHEN 'BUSPROC'
                   PERFORM 0570-MATCH-BUSP
               WHEN 'AUDIT'
                   PERFORM 0580-MATCH-AUDT
17238          WHEN 'LOCKING'
17238              PERFORM 0590-MATCH-LOCK
20070B         WHEN 'ABEND'
20070B             PERFORM 0600-MATCH-ABEND
28729          WHEN 'TRANSFER'
28729              PERFORM 0610-MATCH-TRANSFER
               WHEN OTHER
                   MOVE '1111/111111' TO EFAL-ETRA-ERROR-REF
                   MOVE 'UNKNOWN'     TO EFAL-ETRA-ERROR-TXT
                   MOVE 10            TO EFAL-ETRA-PRIORITY-LEVEL
           END-EVALUATE.

       0500-PERFORM-MATCHING-X.
           EXIT.




       0510-MATCH-DB2 SECTION.
      ****************************************************************
      * A)MATCH DB2 ERRORS                                           *
      ****************************************************************
           MOVE EFAL-DB2-ERR-SQLCODE TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0510-MATCH-DB2-X.
           EXIT.




       0520-MATCH-CICS SECTION.
      ****************************************************************
      * A)MATCH CICS ERRORS                                          *
      ****************************************************************
           MOVE EFAL-CICS-ERR-RESP TO WS-CICS-RESP-CODE.
           MOVE WS-CICS-RESP       TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0520-MATCH-CICS-X.
           EXIT.




       0530-MATCH-IAP SECTION.
      ****************************************************************
      * A)MATCH CICS ERRORS                                          *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0530-MATCH-IAP-X.
           EXIT.




       0540-MATCH-COMA SECTION.
      ****************************************************************
      * A)MATCH COMMAREA ERRORS                                      *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0540-MATCH-COMA-X.
           EXIT.




       0550-MATCH-SECU SECTION.
      ****************************************************************
      * A)MATCH SECURITY ERRORS                                      *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0550-MATCH-SECU-X.
           EXIT.




       0560-MATCH-DATP SECTION.
      ****************************************************************
      * A)MATCH DATA PRIVACY ERRORS                                  *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0560-MATCH-DATP-X.
           EXIT.




       0570-MATCH-BUSP SECTION.
      ****************************************************************
      * A)MATCH BUSINESS PROCESS ERRORS                              *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0570-MATCH-BUSP-X.
           EXIT.




       0580-MATCH-AUDT SECTION.
      ****************************************************************
      * A)MATCH AUDIT ERRORS                                         *
      ****************************************************************
           MOVE SPACES          TO HETS-TYP-ERR.
           PERFORM 0700-MATCH-ON-SUP-TABLE.

       0580-MATCH-AUDT-X.
           EXIT.




17238  0590-MATCH-LOCK SECTION.
17238 ****************************************************************
17238 * A)MATCH LOCKING ERRORS                                       *
17238 ****************************************************************
17238      MOVE SPACES          TO HETS-TYP-ERR.
17238      PERFORM 0700-MATCH-ON-SUP-TABLE.
17238
17238  0590-MATCH-LOCK-X.
17238      EXIT.


20070B 0600-MATCH-ABEND SECTION.
20070B****************************************************************
20070B* A) CREATE AN ERROR REFERENCE OUTSIDE HALOUIDG BECAUSE DB2    *
20070B*    WILL BE CALLED WHICH MAY CAUSE AEY9 ABEND UNDER CERTAIN   *
20070B*    CIRCUMSTANCES.                                            *
20070B****************************************************************
20070B
20070B     EXEC CICS ASKTIME
20070B          ABSTIME   (WS-ABEND-ABSTIME)
20070B     END-EXEC.
20070B
20070B     MOVE WS-ABEND-ABSTIME   TO WS-ABEND-9-ABSTIME.
20070B
20070B     STRING WS-ABEND-9-ABSTIME(7:4)
20070B            '/'
20070B            WS-ABEND-9-ABSTIME(11:5)
20070B                  DELIMITED BY SIZE
20070B                           INTO EFAL-ETRA-ERROR-REF
20070B     END-STRING.
20070B
20070B     MOVE 'DEFAULT ABEND ERROR CODE'
20070B                             TO EFAL-ETRA-ERROR-TXT.
20070B     MOVE 90                 TO EFAL-ETRA-PRIORITY-LEVEL.
20070B
20070B 0600-MATCH-ABEND-X.
20070B     EXIT.


28729  0610-MATCH-TRANSFER SECTION.
28729 ****************************************************************
28729 * A)MATCH TRANSFER ERRORS                                      *
28729 ****************************************************************
28729      MOVE SPACES          TO HETS-TYP-ERR.
28729      PERFORM 0700-MATCH-ON-SUP-TABLE.
28729
28729  0610-MATCH-TRANSFER-X.
28729      EXIT.


       0700-MATCH-ON-SUP-TABLE SECTION.
      ****************************************************************
      * A)USING MATCHING KEYS FROM EACH CALLING SECTION SELECT       *
      *   UNIQUE ERROR ON SUP TABLE                                  *
      * B)USING KEY DATA FROM BEFORE TRY TO OBTAIN DEFAULT SETTINGS  *
      *   FOR THE ERROR                                              *
      ****************************************************************

           MOVE ETRA-ERR-ACTION         TO HETS-ERR-ATN-CD.
           MOVE EFAL-FAILED-APPLICATION TO APP-NM.
           MOVE EFAL-FAILED-ACTION-TYPE TO HETS-FAIL-ACY-CD.

           EXEC SQL
               SELECT HETS_ERR_PTY_NBR,
                      HETS_ERR_TXT
               INTO   :HETS-ERR-PTY-NBR,
                      :HETS-ERR-TXT
               FROM HAL_ERR_TRAN_SUP_V
               WHERE APP_NM             = :APP-NM
               AND   FAIL_ACY_CD        = :HETS-FAIL-ACY-CD
               AND   HETS_ERR_ATN_CD    = :HETS-ERR-ATN-CD
               AND   HETS_TYP_ERR       = :HETS-TYP-ERR
           END-EXEC.

           EVALUATE TRUE
               WHEN ERD-SQL-GOOD
                   PERFORM 0710-GENERATE-ERR-REF
                   MOVE HETS-ERR-PTY-NBR      TO
                                             EFAL-ETRA-PRIORITY-LEVEL
                   MOVE HETS-ERR-TXT          TO EFAL-ETRA-ERROR-TXT
                   GO TO 0700-MATCH-ON-SUP-TABLE-X
               WHEN ERD-SQL-NOT-FOUND
                   CONTINUE
               WHEN OTHER
                   SET ESTO-ERR-TRANS-FAILED       TO TRUE
                   SET ESTO-SUP-TAB-UNIQUE-DB2-ERR TO TRUE
                   MOVE SQLCODE                    TO ESTO-ERR-SQLCODE
                   MOVE SQLERRMC                   TO ESTO-ERR-SQLERRMC
                   GO TO 0700-MATCH-ON-SUP-TABLE-X
           END-EVALUATE.

           EXEC SQL
               SELECT HETS_ERR_PTY_NBR,
                      HETS_ERR_TXT
               INTO   :HETS-ERR-PTY-NBR,
                      :HETS-ERR-TXT
               FROM HAL_ERR_TRAN_SUP_V
               WHERE APP_NM             = :APP-NM
               AND   FAIL_ACY_CD        = :HETS-FAIL-ACY-CD
               AND   HETS_ERR_ATN_CD    = 'DEFAULT'
           END-EXEC.

           EVALUATE TRUE
               WHEN ERD-SQL-GOOD
                   PERFORM 0710-GENERATE-ERR-REF
                   MOVE HETS-ERR-PTY-NBR      TO
                                             EFAL-ETRA-PRIORITY-LEVEL
                   MOVE HETS-ERR-TXT          TO EFAL-ETRA-ERROR-TXT
                   GO TO 0700-MATCH-ON-SUP-TABLE-X
               WHEN ERD-SQL-NOT-FOUND
                   PERFORM 0710-GENERATE-ERR-REF
                   MOVE 10                    TO
                                             EFAL-ETRA-PRIORITY-LEVEL
                   MOVE 'DEFAULT TEXT NOT FOUND'
                                              TO EFAL-ETRA-ERROR-TXT
                   GO TO 0700-MATCH-ON-SUP-TABLE-X
               WHEN OTHER
                   SET ESTO-ERR-TRANS-FAILED       TO TRUE
                   SET ESTO-SUP-TAB-NON-UQUE-DB2-ERR
                                                   TO TRUE
                   MOVE SQLCODE                    TO ESTO-ERR-SQLCODE
                   MOVE SQLERRMC                   TO ESTO-ERR-SQLERRMC
                   GO TO 0700-MATCH-ON-SUP-TABLE-X
           END-EVALUATE.

       0700-MATCH-ON-SUP-TABLE-X.
           EXIT.




       0710-GENERATE-ERR-REF SECTION.
      ****************************************************************
      * A)GENERATE THE ERROR REFERENCE TO SEND BACK IN THE ERROR CODE*
      * FIELD                                                        *
      ****************************************************************
17330      INITIALIZE WS-HALOUBOC-LINKAGE.
           MOVE ZERO              TO UIDG-UNIT-NBR.
           MOVE 1                 TO W-COUNT.
           MOVE EFAL-LOGON-USERID TO UBOC-AUTH-USERID.
17330      MOVE EFAL-UNIT-OF-WORK TO UBOC-UOW-NAME.
17330      MOVE ESTO-STORE-ID     TO UBOC-MSG-ID.

           SET DETERMINE-ERR-REF TO TRUE.

           PERFORM 0720-GET-ERR-REF
               UNTIL W-COUNT > 5
               OR ERROR-CODE-OK
               OR ERROR-FOUND-NO-RETRY.

       0710-GENERATE-ERR-REF-X.
           EXIT.




       0720-GET-ERR-REF SECTION.
      ****************************************************************
18687 * A)GET THE ERROR REFERENCE FROM HALUIDB/HALOUIDG
      ****************************************************************
           SET UIDG-ERROR-ID TO TRUE.
           ADD 1 TO UIDG-UNIT-NBR.

           MOVE LENGTH OF WS-HALOUIDG-LINKAGE
                TO UBOC-APP-DATA-BUFFER-LENGTH.
           MOVE WS-HALOUIDG-LINKAGE TO UBOC-APP-DATA-BUFFER.

           EXEC CICS LINK
18687***        PROGRAM  ('HALOUIDG')
18687           PROGRAM  ('HALUIDB')
                COMMAREA (WS-HALOUBOC-LINKAGE)
                LENGTH   (LENGTH OF WS-HALOUBOC-LINKAGE)
                RESP     (WS-RESPONSE-CODE)
                RESP2    (WS-RESPONSE-CODE2)
           END-EXEC.

           IF WS-RESPONSE-CODE NOT = DFHRESP(NORMAL)
               MOVE '9999/99999' TO EFAL-ETRA-ERROR-REF
               SET ERROR-FOUND-NO-RETRY TO TRUE
               GO TO 0720-GET-ERR-REF-X
           END-IF.

           MOVE UBOC-APP-DATA-BUFFER TO WS-HALOUIDG-LINKAGE.

           IF UIDG-GENERATED-ERROR-REF = SPACES OR LOW-VALUES
               MOVE '8888/88888' TO EFAL-ETRA-ERROR-REF
               SET ERROR-FOUND-NO-RETRY TO TRUE
               GO TO 0720-GET-ERR-REF-X
           END-IF.

           EVALUATE TRUE
             WHEN UIDG-ERRID-SUCCESSFUL
               MOVE UIDG-GENERATED-ERROR-REF
                                     TO EFAL-ETRA-ERROR-REF
               SET ERROR-CODE-OK     TO TRUE
             WHEN UIDG-ERRID-DUPERR
               MOVE UIDG-GENERATED-ERROR-REF
                                     TO EFAL-ETRA-ERROR-REF
             WHEN UIDG-ERRID-SQLERR
               MOVE '7777/77777'     TO EFAL-ETRA-ERROR-REF
               SET ERROR-FOUND-NO-RETRY
                                     TO TRUE
               GO TO 0720-GET-ERR-REF-X
           END-EVALUATE.

           ADD 1 TO W-COUNT.

       0720-GET-ERR-REF-X.
           EXIT.
