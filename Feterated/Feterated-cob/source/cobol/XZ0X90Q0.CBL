       ID DIVISION.

       PROGRAM-ID.   XZ0X90Q0.
      *AUTHOR.       KRISTI SCHETTLER.
      *DATE-WRITTEN. 01 MAY 2009.
      ****************************************************************
      *  PROGRAM TITLE - COBOL FRAMEWORK PROXY PROGRAM FOR           *
      *                  UOW        : XZ_GET_NOT_INS_DTL             *
      *                  OPERATIONS : GetNotificationInsuredDtlByAct *
      *                                                              *
      *  PLATFORM - HOST CICS                                        *
      *                                                              *
      *  PURPOSE -  CONTROLS ALLOCATION OF FRAMEWORK STORAGE AND     *
      *             EXECUTION OF FRAMEWORK MAIN DRIVER               *
      *                                                              *
      *  PROGRAM INITIATION - LINKED TO FROM EITHER A COBOL CLIENT   *
      *                       PROGRAM OR FROM AN IVORY WRAPPER       *
      *                                                              *
      *  DATA ACCESS METHODS - INPUT RECEIVED VIA DFHCOMMAREA        *
      *                        AND SHARED MEMORY                     *
      *                        OUTPUT RETURNED VIA DFHCOMMAREA       *
      *                        AND SHARED MEMORY                     *
      *                                                              *
      ****************************************************************
      ****************************************************************
      *  NOTE: THIS LOG FOR FRAMEWORK USE ONLY FOR TEMPLATE          *
      *        VERSIONING.  USE THE SECOND 'MAINTENANCE LOG' FOR     *
      *        APPLICATION CODING.                                   *
      *                                                              *
      *      T E M P L A T E   M A I N T E N A N C E   L O G         *
      *                                                              *
      *   WR #    DATE     EMP ID              DESCRIPTION           *
      * -------- --------- -------   ------------------------------- *
      * TS129    01JUN06   E404LJL   INITIAL TEMPLATE VERSION        *
      ****************************************************************
      ****************************************************************
      *                                                              *
      *    A P P L I C A T I O N   M A I N T E N A N C E   L O G     *
      *                                                              *
      *   WR #    DATE      EMP ID              DESCRIPTION          *
      * -------- ---------  -------   ------------------------------ *
      * TO07614  03/31/2009 E404KXS   INITIAL PROGRAM                *
      * TL000143 02/18/2010 E404DMA   UPDATED TO DYNAMIC PROXY LOGIC *
      *                                                              *
      ****************************************************************


       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  CONSTANT-FIELDS.
           03  CF-BLANK        VALUE SPACES            PIC X(01).
           03  CF-MAIN-DRIVER  VALUE 'TS020000'        PIC X(08).
           03  CF-POSITIVE     VALUE '+'               PIC X(01).
           03  CF-NEGATIVE     VALUE '-'               PIC X(01).
           03  CF-NEGATIVE-ONE VALUE -1                PIC S9(01).
           03  CF-COL-IS-NOT-NULL
                               VALUE 'N'               PIC X(01).
           03  CF-COL-IS-NULL  VALUE 'Y'               PIC X(01).
           03  CF-UNIT-OF-WORK VALUE 'XZ_GET_NOT_INS_DTL'
                                                       PIC X(32).
           03  CF-REQUEST-MODULE
                               VALUE 'XZ0Q90Q0'        PIC X(08).
           03  CF-RESPONSE-MODULE
                               VALUE 'XZ0R90Q0'        PIC X(08).
           03  CF-PROGRAM-NAME VALUE 'XZ0X90Q0'        PIC X(08).
TL0113     03  CF-COPYBOOK-NAMES.
TL0113         05  CF-CN-PROXY-CBK-INP-NM
TL0113                         VALUE 'FWPXYCOMONINP'   PIC X(16).
TL0113         05  CF-CN-PROXY-CBK-OUP-NM
TL0113                         VALUE 'FWPXYCOMONOUP'   PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-INP-NM
TL0113                         VALUE 'FWPXYSERVICEINP' PIC X(16).
TL0113         05  CF-CN-SERVICE-CBK-OUP-NM
TL0113                         VALUE 'FWPXYSERVICEOUP' PIC X(16).


       01  WORKING-STORAGE-AREA.
TL0113     03  WS-SERVICE-CONTRACT-ATB.
TL0113         05  WS-SC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-SC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-SC-INPUT-PTR             POINTER.
TL0113         05  WS-SC-OUTPUT-PTR            POINTER.
TL0113     03  WS-PROXY-CONTRACT-ATB.
TL0113         05  WS-PC-INPUT-LEN             BINARY  PIC S9(08).
TL0113         05  WS-PC-OUTPUT-LEN            BINARY  PIC S9(08).
TL0113         05  WS-PC-INPUT-PTR             POINTER.
TL0113         05  WS-PC-OUTPUT-PTR            POINTER.
TL0113     03  WS-COMMAREA-ATB.
TL0113         05  WS-CA-PTR                   POINTER.
TL0113         05  WS-CA-PTR-VAL
TL0113                         REDEFINES WS-CA-PTR
TL0113                                         BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE                BINARY  PIC S9(08).
           03  WS-RESPONSE-CODE2               BINARY  PIC S9(08).
           03  WS-PROGRAM-NAME                         PIC X(08).
           03  WS-EIBRESP-DISPLAY                      PIC -Z(08)9.
           03  WS-EIBRESP2-DISPLAY                     PIC -Z(08)9.
           03  WS-OPERATION-NAME                       PIC X(32).
               88  WS-GET-NOT-INS-DTL
                               VALUE 'GetNotificationInsuredDtlByAct'.
               88  WS-VALID-OPERATION
                               VALUE 'GetNotificationInsuredDtlByAct'.


       01  MAIN-DRIVER-DATA.
           COPY TS020DRV.


       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY TS020PRO.


      ** "FRAMEWORK" REQUEST AREA FOR THIS OPERATION
      ** INCLUDE ALL BDO/BPO LAYOUTS THAT COMPRISE THE REQUEST TO
      ** THE BUSINESS FRAMEWORK.
       01  L-FRAMEWORK-REQUEST-AREA.
           03  L-FW-REQ-XZ0A90Q0.
           COPY XZ0A90Q0 REPLACING ==:XZA9Q0:== BY ==XZA9Q0Q==.


      ** "FRAMEWORK" RESPONSE AREA FOR THIS OPERATION
       01  L-FRAMEWORK-RESPONSE-AREA.
           03  L-FW-RESP-XZ0A90Q0.
           COPY XZ0A90Q0 REPLACING ==:XZA9Q0:== BY ==XZA9Q0R==.



      ** SERVICE "CONTRACT" COPYBOOK FOR THIS OPERATION
      ** BOTH THE REQUEST AND RESPONSE ARE IN THIS COPYBOOK
       01  L-SERVICE-CONTRACT-AREA.
       COPY XZ0T90Q0.

TL0113 COPY TSC21PRO.

TL0113 12514-SET-SVC-CTT-ATB.
      ****************************************************************
      * SET THE ATTRIBUTES PERTINIENT TO THE SERVICE CONTRACT        *
      ****************************************************************

           SET WS-SC-INPUT-PTR         TO ADDRESS OF
                                             XZT9Q0-SERVICE-INPUTS.
           SET WS-SC-OUTPUT-PTR        TO ADDRESS OF
                                             XZT9Q0-SERVICE-OUTPUTS.
           MOVE LENGTH OF XZT9Q0-SERVICE-INPUTS
                                       TO WS-SC-INPUT-LEN.
           MOVE LENGTH OF XZT9Q0-SERVICE-OUTPUTS
                                       TO WS-SC-OUTPUT-LEN.

TL0113 12514-EXIT.
           EXIT.

       13100-SET-UP-INPUT.
      ****************************************************************
      * POPULATE FRAMEWORK REQUEST AREAS WITH VALUES FROM SERVICE    *
      * INPUT AREA                                                   *
      ****************************************************************

           MOVE PPC-OPERATION          TO WS-OPERATION-NAME.

      * IF A USERID WAS SUPPLIED, PASS IT TO THE FRAMEWORK
           IF XZT9QI-USERID NOT = SPACES
             AND
              XZT9QI-USERID NOT = LOW-VALUES
               MOVE XZT9QI-USERID      TO CSC-AUTH-USERID
                                          XZA9Q0Q-USERID
           END-IF.

           MOVE XZT9QI-CSR-ACT-NBR     TO XZA9Q0Q-CSR-ACT-NBR.

       13100-EXIT.
           EXIT.


       13200-SET-UP-OUTPUT.
      ****************************************************************
      * POPULATE SERVICE OUTPUT AREA WITH VALUES FROM FRAMEWORK      *
      * RESPONSE AREA.                                               *
      ****************************************************************

           MOVE XZA9Q0R-ACT-OWN-CLT-ID TO XZT9QO-TK-ACT-OWN-CLT-ID.
           MOVE XZA9Q0R-ACT-OWN-ADR-ID TO XZT9QO-TK-ACT-OWN-ADR-ID.
           MOVE XZA9Q0R-SEG-CD         TO XZT9QO-TK-SEG-CD.
           MOVE XZA9Q0R-ACT-TYP-CD     TO XZT9QO-TK-ACT-TYP-CD.

           MOVE XZA9Q0R-CSR-ACT-NBR    TO XZT9QO-CSR-ACT-NBR.
           MOVE XZA9Q0R-PDC-NBR        TO XZT9QO-PDC-NBR.
           MOVE XZA9Q0R-PDC-NM         TO XZT9QO-PDC-NM.
           MOVE XZA9Q0R-ST-ABB         TO XZT9QO-ST-ABB.
           MOVE XZA9Q0R-LAST-NAME      TO XZT9QO-LAST-NAME.
           MOVE XZA9Q0R-NM-ADR-LIN-1   TO XZT9QO-NM-ADR-LIN-1.
           MOVE XZA9Q0R-NM-ADR-LIN-2   TO XZT9QO-NM-ADR-LIN-2.
           MOVE XZA9Q0R-NM-ADR-LIN-3   TO XZT9QO-NM-ADR-LIN-3.
           MOVE XZA9Q0R-NM-ADR-LIN-4   TO XZT9QO-NM-ADR-LIN-4.
           MOVE XZA9Q0R-NM-ADR-LIN-5   TO XZT9QO-NM-ADR-LIN-5.
           MOVE XZA9Q0R-NM-ADR-LIN-6   TO XZT9QO-NM-ADR-LIN-6.

       13200-EXIT.
           EXIT.

