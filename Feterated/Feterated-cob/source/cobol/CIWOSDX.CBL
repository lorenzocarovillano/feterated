       IDENTIFICATION DIVISION.
       PROGRAM-ID. CIWOSDX.
       AUTHOR.     PMSC.
           DATE-WRITTEN. SEPT 1994.
      *REMARKS.
      *****************************************************************
      **                                                             **
      ** PROGRAM TITLE - COBOL SOUNDEX ROUTINE                       **
      **                                                             **
      ** PLATFORM - HOST                                             **
      **                                                             **
      ** OPERATING SYSTEM - MVS                                      **
      **                                                             **
      ** LANGUAGE - IBM VS COBOL II                                  **
      **                                                             **
      ** PURPOSE -  TO CREATE PHONETIC CODE USING LAST NAME          **
      **                                                             **
      ** PROGRAM INITIATION -  THIS PROGRAM IS STARTED BY ONE OF THE **
      **                       FOLLOWING METHODS:                    **
      **                                                             **
      **                       1. IT IS DYNAMICALLY CALLED BY THE    **
      **                          CLIENT API CIWOF08.                **
      **                                                             **
      ** DATA ACCESS METHODS -                                       **
      **                                                             **
      *****************************************************************
           EJECT
      *****************************************************************
      *             M A I N T E N A N C E   L O G                     *
      *                                                               *
      *   SI #    DATE    EMP ID              DESCRIPTION             *
      *  ------ --------  ------    --------------------------------- *
      *         10/03/94  5833      SOURCE CODE CREATED.              *
      *                                                               *
042824*  042824 09/29/98  5833      REMOVED UNNEEDED COPYBOOKS.       *
      *  11397  10/27/16  E404DNF   RECOMPILED FOR STORCOPY WORK      *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *     PROGRAM TO CREATE PHONETIC CODE                           *
      *                                                               *
      *  FORMAT OF GENERATED CODE WILL BE THE FOLLOWING:              *
      *                                                               *
      *  FIRST CHARACTER WILL BE THE FIRST CHARACTRR OF LAST NAME.    *
      *  EACH SUBSEQUENT CHARACTER IN THE STRING WILL BE TRANSLATED   *
      *  ACCORDING TO THE FOLLOWING TABLE:                            *
      *    A, E, I, O, U, Y, H, W  =>  ' '                            *
      *    B, F, P, V              =>  '1'                            *
      *    C, G, J, K, Q, S, X, Z  =>  '2'                            *
      *    D, T                    =>  '3'                            *
      *    L                       =>  '4'                            *
      *    M, N                    =>  '5'                            *
      *    R                       =>  '6'                            *
      *                                                               *
      *     IF A CHARACTER TRANSLATES TO THE SAME VALUE AS THE PRE-   *
      *  VIOUS CHARACTER, IT WILL NOT BE ADDED TO THE PHONETIC CODE.  *
      *     IF A CHARACTER TRANSLATES TO A SPACE, IT WILL NOT BE      *
      *  ADDED TO THE CODE.                                           *
      *     THE PHONETIC CODE WILL BE EXACTLY FOUR BYTES. IF AFTER    *
      *  THE PRIOR STEPS THE CODE IS TOO SHORT, PAD IT OUT WITH ZEROS.*
      *  IF TOO LONG, TRUNCATE TO FOUR BYTES.                         *
      *****************************************************************
       ENVIRONMENT DIVISION.
042824*CONFIGURATION SECTION.
042824*SPECIAL-NAMES.
042824*    COPY XPXLSPEC.
042824*INPUT-OUTPUT SECTION.
042824*FILE-CONTROL.
042824*    COPY XPXLSELC.
       DATA DIVISION.
042824*FILE SECTION.
042824*    COPY XPXLFILE.
       WORKING-STORAGE SECTION.
       01  FILLER                           PIC X(40) VALUE
                             'WORKING-STORAGE FOR CIWOSDX BEGINS HERE'.
           EJECT
       01  WS-COPYRIGHT-INFO.
           05  FILLER                       PIC X(80)  VALUE
           '********************************************************'.
           05  FILLER                       PIC X(80)  VALUE
           '* CONFIDENTIAL INFORMATION - LIMITED DISTRIBUTION TO   *'.
           05  FILLER                       PIC X(80)  VALUE
           '* AUTHORIZED PERSONS ONLY.                             *'.
           05  FILLER                       PIC X(80)  VALUE
           '*                                                      *'.
           05  FILLER                       PIC X(80)  VALUE
           '* INSURANCE APPLICATION PLATFORM (IAP) AND ITS         *'.
           05  FILLER                       PIC X(80)  VALUE
           '* PROGRAMS AND DOCUMENTATION ARE THE CONFIDENTIAL,     *'.
           05  FILLER                       PIC X(80)  VALUE
           '* TRADE SECRET AND PROPRIETARY PRODUCT AND PROPERTY OF *'.
           05  FILLER                       PIC X(80)  VALUE
           '* POLICY MANAGEMENT SYSTEMS CORPORATION AND MAY NOT BE *'.
           05  FILLER                       PIC X(80)  VALUE
           '* REPRODUCED OR USED EXCEPT AS PERMITTED IN WRITING    *'.
           05  FILLER                       PIC X(80)  VALUE
           '* BY POLICY MANAGEMENT SYSTEMS CORPORATION. UNDER NO   *'.
           05  FILLER                       PIC X(80)  VALUE
           '* CIRCUMSTANCES SHOULD ANY PORTION OF THE PROGRAMS OR  *'.
           05  FILLER                       PIC X(80)  VALUE
           '* THEIR DOCUMENTATION BE DISCLOSED OR USED WITHOUT     *'.
           05  FILLER                       PIC X(80)  VALUE
           '* THE WRITTEN CONSENT OF POLICY MANAGEMENT SYSTEMS     *'.
           05  FILLER                       PIC X(80)  VALUE
           '* CORPORATION.                                         *'.
           05  FILLER                       PIC X(80)  VALUE
           '*                                                      *'.
           05  FILLER                       PIC X(80)  VALUE
           '* COPYRIGHT (C), 1990, POLICY MANAGEMENT SYSTEMS       *'.
           05  FILLER                       PIC X(80)  VALUE
           '* CORPORATION   UNPUBLISHED - RIGHTS RESERVED UNDER    *'.
           05  FILLER                       PIC X(80)  VALUE
           '* COPYRIGHT LAWS OF THE UNITED STATES. ALL RIGHTS      *'.
           05  FILLER                       PIC X(80)  VALUE
           '* RESERVED                                             *'.
           05  FILLER                       PIC X(80)  VALUE
           '********************************************************'.
      *
           EJECT
042824*01  FILLER                           PIC X(09) VALUE 'XPXLCOMM:'.
042824*01  WS-PASSED-PARMS.
042824*    COPY XPXLCOMM.
      *
042824*    COPY XPXLWORK.
042824*    EJECT
       01  WS-TMP-BUFFER.
           05 WS-TMP-BUFF                   OCCURS 60 TIMES
                                            INDEXED BY BUF-INDX.
              10 WS-BUF-CHAR                PIC X(01).
       01  WS-TMP-CTR                       PIC 9(04) VALUE 0.
       01  WS-SDX-CTR                       PIC 9(04) VALUE 0.
       01  WS-FIRST-CHAR-SW                 PIC X(01) VALUE 'N'.
           88 FIRST-CHAR-NOT-LOADED         VALUE 'N'.
           88 FIRST-CHAR-LOADED             VALUE 'Y'.
       01  WS-SOUNDEX-SW                    PIC X(01) VALUE 'N'.
           88 SOUNDEX-NOT-COMPLETE          VALUE 'N'.
           88 SOUNDEX-COMPLETE              VALUE 'Y'.
       01  WS-PREV-CHAR                     PIC X(01) VALUE SPACES.
       01  WS-ONE-CHAR                      PIC X(01) VALUE SPACES.
       01  WS-TALLY                         PIC 9(02) COMP VALUE ZEROS.
       01  WS-SDX-PASSED-PARMS.
           05 WS-LAST-NAME                  PIC X(60).
           05 WS-SOUNDEX-CD.
              10 WS-SOUNDEX                 OCCURS 4 TIMES
                                            INDEXED BY SDX-INDX.
                 15 WS-SNDX-CHAR            PIC X(01).
           05 WS-PGM-ID                     PIC X(08).
           05 WS-SDX-RET-CD                 PIC X(02).
       01  WS-CHARACTER-TABLE.
           05 WS-ALPHABET                   PIC X(26) VALUE
                                        'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
           05 WS-ALPHABET-CHARACTER      REDEFINES WS-ALPHABET.
              10 WS-ALPHABET-CHAR           PIC X(01) OCCURS 26 TIMES.
      *
       01  WS-TRANSLATE-TABLE.
           05 WS-TRANSLATE-VALUES           PIC X(26) VALUE
                                        ' 123 12  22455 12623 1 2 2'.
           05 WS-TRANSLATE-CHARACTER     REDEFINES WS-TRANSLATE-VALUES.
              10 WS-TRANSLATE-CHAR          PIC X(01) OCCURS 26 TIMES.
      *
       01  SMALLINT-CONVERT                 PIC S9(05) COMP-3.
       01  LARGEINT-CONVERT                 PIC S9(10) COMP-3.
      *
       01  FILLER                           PIC X(40) VALUE
                             'WORKING-STORAGE FOR CIWOSDX ENDS HERE'.
      *
       LINKAGE SECTION.
       01  DFHCOMMAREA                      PIC X(100).
           EJECT
       PROCEDURE DIVISION USING DFHCOMMAREA.
      *
       0100-MAIN.
      ***********************************************************
      *  THE 0100-MAIN PARAGRAPH IS RESPONSIBLE FOR CONTROLLING *
      *  THE PROCESSING OF THE FUNCTION PASSED TO IT.           *
      ***********************************************************
      *
           MOVE DFHCOMMAREA TO WS-SDX-PASSED-PARMS.
      *                                                         *
      ***SET PROGRAM NAME IN USER AREA FOR DEBUGGING*************
      *                                                         *
           MOVE 'CIWOSDX' TO WS-PGM-ID.
      *
           PERFORM 0200-GENERATE-SOUNDEX.
      *
       EXIT-MODULE.
      *
      ***********************************************************
      *    RESET THE COMMUNICATION AREA AND RETURN TO THE       *
      *    PROGRAM THAT CALLED THIS ROUTINE.                    *
      ***********************************************************
      *
           IF SOUNDEX-COMPLETE
              MOVE 0 TO WS-SDX-RET-CD
           ELSE
              MOVE '93' TO WS-SDX-RET-CD.
      *
           MOVE WS-SDX-PASSED-PARMS TO DFHCOMMAREA.
      *
042824*    COPY XPXCDONE.
      *
      * GOBACK REQUIRED BY COMPILER
      *
           GOBACK.
           EJECT
       0200-GENERATE-SOUNDEX.
      *
           MOVE 0 TO WS-SDX-CTR.
           MOVE 1 TO WS-TMP-CTR.
           MOVE ' ' TO WS-PREV-CHAR.
           MOVE 'N' TO WS-SOUNDEX-SW WS-FIRST-CHAR-SW.
           MOVE WS-LAST-NAME TO WS-TMP-BUFFER.
           SET SDX-INDX BUF-INDX TO 1.
      *
           PERFORM 0250-INSPECT-NAME THRU
                   0250-INSPECT-NAME-X VARYING WS-TMP-CTR FROM 1 BY 1
              UNTIL WS-TMP-CTR GREATER THAN LENGTH OF WS-TMP-BUFFER
              OR SOUNDEX-COMPLETE.
      *
           IF SOUNDEX-NOT-COMPLETE
              PERFORM UNTIL SOUNDEX-COMPLETE
                 MOVE 0 TO WS-SNDX-CHAR (SDX-INDX)
                 SET SDX-INDX UP BY 1
                 ADD 1 TO WS-SDX-CTR
                 IF WS-SDX-CTR = 4
                    SET SOUNDEX-COMPLETE TO TRUE
                 END-IF
              END-PERFORM.
      *
       0250-INSPECT-NAME.
      *
           MOVE ZERO TO WS-TALLY.
           MOVE WS-BUF-CHAR (BUF-INDX) TO WS-ONE-CHAR.
      *
           IF FIRST-CHAR-NOT-LOADED
              IF WS-ONE-CHAR > SPACES
                 MOVE WS-BUF-CHAR (BUF-INDX) TO WS-SNDX-CHAR (SDX-INDX)
                 SET FIRST-CHAR-LOADED TO TRUE
                 SET BUF-INDX SDX-INDX UP BY 1
                 ADD 1 TO WS-SDX-CTR
                 GO TO 0250-INSPECT-NAME-X.
      *
           INSPECT WS-ALPHABET TALLYING WS-TALLY
              FOR CHARACTERS BEFORE WS-ONE-CHAR.
      *
      ***   TRANSLATE VALUE WILL BE TALLY + 1 ***
           IF WS-TALLY < 26
              IF WS-TRANSLATE-CHAR (WS-TALLY + 1) > SPACE
                 IF WS-TRANSLATE-CHAR (WS-TALLY + 1) NOT = WS-PREV-CHAR
                    MOVE WS-TRANSLATE-CHAR (WS-TALLY + 1)
                       TO WS-SNDX-CHAR (SDX-INDX)
                    MOVE WS-TRANSLATE-CHAR (WS-TALLY + 1)
                       TO WS-PREV-CHAR
                    SET SDX-INDX UP BY 1
                    ADD 1 TO WS-SDX-CTR.
      *
           SET BUF-INDX UP BY 1.
      *
           IF WS-SDX-CTR = 4
              SET SOUNDEX-COMPLETE TO TRUE.
      *
       0250-INSPECT-NAME-X.
           EXIT.
           EJECT
042824*COPY XPXCABND.
