SET CURRENT SQLID = 'DBADEV';

     CREATE DATABASE                                                    
       XZDP901                                                          
         BUFFERPOOL BP1                                                 
         INDEXBP BP2                                                    
         STOGROUP DB2SMS                                                
         CCSID EBCDIC                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0101                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 12000                                                   
         SECQTY 1200                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_FRM                                                 
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,FRM_SEQ_NBR  SMALLINT NOT NULL                                  
       ,FRM_NBR  CHAR(30) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,FRM_EDT_DT  DATE NOT NULL                                       
        ,                                                               
         CONSTRAINT XZI01011                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,FRM_SEQ_NBR                                                  
         )                                                              
        )                                                               
         IN XZDP901.XZS0101                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01011                                             
       ON dbo.ACT_NOT_FRM                                              
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,FRM_SEQ_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 14400                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0102                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_POL                                                 
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,POL_NBR  CHAR(25) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,POL_TYP_CD  CHAR(3) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,POL_PRI_RSK_ST_ABB  CHAR(2) NOT NULL                            
         FOR SBCS DATA                                                  
       ,NOT_EFF_DT  DATE                                                
       ,POL_EFF_DT  DATE NOT NULL                                       
       ,POL_EXP_DT  DATE NOT NULL                                       
       ,POL_DUE_AMT  DECIMAL(10, 2)                                     
       ,NIN_CLT_ID  CHAR(64) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NIN_ADR_ID  CHAR(64) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,WF_STARTED_IND  CHAR(1)                                         
         FOR SBCS DATA                                                  
       ,POL_BIL_STA_CD  CHAR(1)                                         
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01021                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,POL_NBR                                                      
         )                                                              
        )                                                               
         IN XZDP901.XZS0102                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01021                                             
       ON dbo.ACT_NOT_POL                                              
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,POL_NBR ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.XZI01022                                             
       ON dbo.ACT_NOT_POL                                              
        (                                                               
         NIN_CLT_ID ASC                                                 
        ,CSR_ACT_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240000                                                  
         SECQTY 96000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 1 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0103                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_POL_FRM                                             
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,FRM_SEQ_NBR  SMALLINT NOT NULL                                  
       ,POL_NBR  CHAR(25) NOT NULL                                      
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01031                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,FRM_SEQ_NBR                                                  
          ,POL_NBR                                                      
         )                                                              
        )                                                               
         IN XZDP901.XZS0103                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01031                                             
       ON dbo.ACT_NOT_POL_FRM                                          
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,FRM_SEQ_NBR ASC                                                
        ,POL_NBR ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0104                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 24000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT                                                     
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,NOT_DT  DATE NOT NULL                                           
       ,ACT_OWN_CLT_ID  CHAR(64) NOT NULL                               
         FOR SBCS DATA                                                  
       ,ACT_OWN_ADR_ID  CHAR(64) NOT NULL                               
         FOR SBCS DATA                                                  
       ,EMP_ID  CHAR(6)                                                 
         FOR SBCS DATA                                                  
       ,STA_MDF_TS  TIMESTAMP NOT NULL                                  
       ,ACT_NOT_STA_CD  CHAR(2) NOT NULL                                
         FOR SBCS DATA                                                  
       ,PDC_NBR  CHAR(5)                                                
         FOR SBCS DATA                                                  
       ,PDC_NM  VARCHAR(120)                                            
         FOR SBCS DATA                                                  
       ,SEG_CD  CHAR(3)                                                 
         FOR SBCS DATA                                                  
       ,ACT_TYP_CD  CHAR(2)                                             
         FOR SBCS DATA                                                  
       ,TOT_FEE_AMT  DECIMAL(10, 2)                                     
       ,ST_ABB  CHAR(2)                                                 
         FOR SBCS DATA                                                  
       ,ADD_CNC_DAY  SMALLINT                                           
       ,REA_DES  VARCHAR(500)                                           
         FOR SBCS DATA                                                  
       ,CER_HLD_NOT_IND  CHAR(1)                                        
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01041                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
         )                                                              
        )                                                               
         IN XZDP901.XZS0104                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01041                                             
       ON dbo.ACT_NOT                                                  
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144000                                                  
         SECQTY 24000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.XZI01042                                             
       ON dbo.ACT_NOT                                                  
        (                                                               
         ACT_OWN_CLT_ID ASC                                             
        ,CSR_ACT_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240000                                                  
         SECQTY 96000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 1 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0105                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_WRD                                                 
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,POL_NBR  CHAR(25) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,ST_WRD_SEQ_CD  CHAR(5) NOT NULL                                 
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01051                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,POL_NBR                                                      
          ,ST_WRD_SEQ_CD                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS0105                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01051                                             
       ON dbo.ACT_NOT_WRD                                              
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,POL_NBR ASC                                                    
        ,ST_WRD_SEQ_CD ASC                                              
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.XZI01052                                             
       ON dbo.ACT_NOT_WRD                                              
        (                                                               
         NOT_PRC_TS DESC                                                
        ,POL_NBR ASC                                                    
        ,CSR_ACT_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0106                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_REC                                                 
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,REC_SEQ_NBR  SMALLINT NOT NULL                                  
       ,REC_TYP_CD  CHAR(5) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,REC_CLT_ID  CHAR(64)                                            
         FOR SBCS DATA                                                  
       ,REC_NM  VARCHAR(120)                                            
         FOR SBCS DATA                                                  
       ,REC_ADR_ID  CHAR(64)                                            
         FOR SBCS DATA                                                  
       ,LIN_1_ADR  CHAR(45)                                             
         FOR SBCS DATA                                                  
       ,LIN_2_ADR  CHAR(45)                                             
         FOR SBCS DATA                                                  
       ,CIT_NM  CHAR(30)                                                
         FOR SBCS DATA                                                  
       ,ST_ABB  CHAR(2)                                                 
         FOR SBCS DATA                                                  
       ,PST_CD  CHAR(13)                                                
         FOR SBCS DATA                                                  
       ,MNL_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,CER_NBR  CHAR(25)                                               
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01061                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,REC_SEQ_NBR                                                  
         )                                                              
        )                                                               
         IN XZDP901.XZS0106                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01061                                             
       ON dbo.ACT_NOT_REC                                              
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,REC_SEQ_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.XZI01062                                             
       ON dbo.ACT_NOT_REC                                              
        (                                                               
         REC_CLT_ID ASC                                                 
        ,CSR_ACT_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240000                                                  
         SECQTY 96000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 1 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0107                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 12000                                                   
         SECQTY 1200                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_FRM_REC                                             
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,FRM_SEQ_NBR  SMALLINT NOT NULL                                  
       ,REC_SEQ_NBR  SMALLINT NOT NULL                                  
        ,                                                               
         CONSTRAINT XZI01071                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,FRM_SEQ_NBR                                                  
          ,REC_SEQ_NBR                                                  
         )                                                              
        )                                                               
         IN XZDP901.XZS0107                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01071                                             
       ON dbo.ACT_NOT_FRM_REC                                          
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS DESC                                                
        ,FRM_SEQ_NBR ASC                                                
        ,REC_SEQ_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0108                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_POL_REC                                             
        (                                                               
        CSR_ACT_NBR  CHAR(9) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,NOT_PRC_TS  TIMESTAMP NOT NULL                                  
       ,POL_NBR  CHAR(25) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,REC_SEQ_NBR  SMALLINT NOT NULL                                  
        ,                                                               
         CONSTRAINT XZI01081                                            
         PRIMARY KEY                                                    
         (                                                              
          CSR_ACT_NBR                                                   
          ,NOT_PRC_TS                                                   
          ,POL_NBR                                                      
          ,REC_SEQ_NBR                                                  
         )                                                              
        )                                                               
         IN XZDP901.XZS0108                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01081                                             
       ON dbo.ACT_NOT_POL_REC                                          
        (                                                               
         CSR_ACT_NBR ASC                                                
        ,NOT_PRC_TS ASC                                                 
        ,POL_NBR ASC                                                    
        ,REC_SEQ_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS0109                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240000                                                  
         SECQTY 96000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.WC_NOT_RPT                                                  
        (                                                               
        POL_NBR  CHAR(7) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,POL_EFF_DT  DATE NOT NULL                                       
       ,STA_MDF_TS  TIMESTAMP NOT NULL                                  
       ,CMP_CD  CHAR(2)                                                 
         FOR SBCS DATA                                                  
       ,NOT_TYP_CD  CHAR(5)                                             
         FOR SBCS DATA                                                  
       ,CNC_REN_TYP_CD  CHAR(2)                                         
         FOR SBCS DATA                                                  
       ,REA_CNC_REN_CD  CHAR(2)                                         
         FOR SBCS DATA                                                  
       ,INSD_NM  CHAR(120)                                              
         FOR SBCS DATA                                                  
       ,INSD_ADR  CHAR(120)                                             
         FOR SBCS DATA                                                  
       ,LEG_ETY_DES  CHAR(40)                                           
         FOR SBCS DATA                                                  
       ,CNC_MAL_INSD_DT  DATE                                           
       ,NOT_EFF_DT  DATE                                                
        ,                                                               
         CONSTRAINT XZI01091                                            
         PRIMARY KEY                                                    
         (                                                              
          POL_NBR                                                       
          ,POL_EFF_DT                                                   
          ,STA_MDF_TS                                                   
         )                                                              
        )                                                               
         IN XZDP901.XZS0109                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01091                                             
       ON dbo.WC_NOT_RPT                                               
        (                                                               
         POL_NBR ASC                                                    
        ,POL_EFF_DT ASC                                                 
        ,STA_MDF_TS ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 1 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS01ST1                                               
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 2400                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 4                                                      
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_STA_TYP                                             
        (                                                               
        ACT_NOT_STA_CD  CHAR(2) NOT NULL                                
         FOR SBCS DATA                                                  
       ,ACT_NOT_STA_DES  CHAR(35) NOT NULL                              
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1A                                            
         PRIMARY KEY                                                    
         (                                                              
          ACT_NOT_STA_CD                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1A                                             
       ON dbo.ACT_NOT_STA_TYP                                          
        (                                                               
         ACT_NOT_STA_CD ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ACT_NOT_TYP                                                 
        (                                                               
        ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,ACT_NOT_DES  CHAR(35) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,DSY_ORD_NBR  SMALLINT                                           
       ,DOC_DES  VARCHAR(240)                                           
         FOR SBCS DATA                                                  
       ,ACT_NOT_PTH_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,ACT_NOT_PTH_DES  CHAR(35) NOT NULL                              
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01ST1                                            
         PRIMARY KEY                                                    
         (                                                              
          ACT_NOT_TYP_CD                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S11                                             
       ON dbo.ACT_NOT_TYP                                              
        (                                                               
         ACT_NOT_TYP_CD ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.CLT_RLT_REC_TYP                                             
        (                                                               
        CLT_RLT_TYP_CD  CHAR(4) NOT NULL                                
         FOR SBCS DATA                                                  
       ,REC_TYP_CD  CHAR(5) NOT NULL                                    
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01ST10                                           
         PRIMARY KEY                                                    
         (                                                              
          CLT_RLT_TYP_CD                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S10                                             
       ON dbo.CLT_RLT_REC_TYP                                          
        (                                                               
         CLT_RLT_TYP_CD ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.CO_TYP                                                      
        (                                                               
        CO_CD  CHAR(5) NOT NULL                                         
         FOR SBCS DATA                                                  
       ,CO_OFS_NBR  SMALLINT NOT NULL                                   
       ,CO_DES  CHAR(30) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1G                                            
         PRIMARY KEY                                                    
         (                                                              
          CO_CD                                                         
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1G                                             
       ON dbo.CO_TYP                                                   
        (                                                               
         CO_CD ASC                                                      
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FRM_GRP                                                     
        (                                                               
        EDL_FRM_NM  CHAR(30) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,DTA_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,DTA_GRP_FLD_NBR  SMALLINT NOT NULL                              
       ,TAG_NM  CHAR(30) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,JUS_CD  CHAR(1) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,SPE_PRC_CD  CHAR(8)                                             
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1H                                            
         PRIMARY KEY                                                    
         (                                                              
          EDL_FRM_NM                                                    
          ,DTA_GRP_NM                                                   
          ,DTA_GRP_FLD_NBR                                              
          ,TAG_NM                                                       
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1H                                             
       ON dbo.FRM_GRP                                                  
        (                                                               
         EDL_FRM_NM ASC                                                 
        ,DTA_GRP_NM ASC                                                 
        ,DTA_GRP_FLD_NBR ASC                                            
        ,TAG_NM ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FRM_PRC_TYP                                                 
        (                                                               
        FRM_NBR  CHAR(30) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,FRM_EDT_DT  DATE NOT NULL                                       
       ,ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,EDL_FRM_NM  CHAR(30) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,FRM_DES  CHAR(20) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,SPE_PRC_CD  CHAR(8)                                             
         FOR SBCS DATA                                                  
       ,DTN_CD  SMALLINT NOT NULL                                       
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1D                                            
         PRIMARY KEY                                                    
         (                                                              
          FRM_NBR                                                       
          ,FRM_EDT_DT                                                   
          ,ACT_NOT_TYP_CD                                               
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1D                                             
       ON dbo.FRM_PRC_TYP                                              
        (                                                               
         FRM_NBR ASC                                                    
        ,FRM_EDT_DT DESC                                                
        ,ACT_NOT_TYP_CD ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FRM_REC_ATC_TYP                                             
        (                                                               
        ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,FRM_NBR  CHAR(30) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,FRM_EDT_DT  DATE NOT NULL                                       
       ,FRM_SR_ORD_NBR  SMALLINT NOT NULL                               
       ,FRM_SPE_ATC_CD  CHAR(10)                                        
         FOR SBCS DATA                                                  
       ,FRM_EFF_DT  DATE NOT NULL                                       
       ,FRM_EXP_DT  DATE NOT NULL                                       
       ,INSD_REC_IND  CHAR(1) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,ADD_INS_REC_IND  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,LPE_MGE_REC_IND  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,CER_REC_IND  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,ANI_REC_IND  CHAR(1) NOT NULL WITH DEFAULT                      
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S14                                            
         PRIMARY KEY                                                    
         (                                                              
          ACT_NOT_TYP_CD                                                
          ,FRM_NBR                                                      
          ,FRM_EDT_DT                                                   
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S14                                             
       ON dbo.FRM_REC_ATC_TYP                                          
        (                                                               
         ACT_NOT_TYP_CD ASC                                             
        ,FRM_NBR ASC                                                    
        ,FRM_EDT_DT DESC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FRM_REC_OVL                                                 
        (                                                               
        FRM_NBR  CHAR(30) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,FRM_EDT_DT  DATE NOT NULL                                       
       ,REC_TYP_CD  CHAR(5) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OVL_EDL_FRM_NM  CHAR(30) NOT NULL                               
         FOR SBCS DATA                                                  
       ,ST_ABB  CHAR(2) NOT NULL WITH DEFAULT 'ZZ'                      
         FOR SBCS DATA                                                  
       ,OVL_SR_CD  CHAR(5) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SPE_PRC_CD  CHAR(8)                                             
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1C                                            
         PRIMARY KEY                                                    
         (                                                              
          FRM_NBR                                                       
          ,FRM_EDT_DT                                                   
          ,REC_TYP_CD                                                   
          ,OVL_EDL_FRM_NM                                               
          ,ST_ABB                                                       
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1C                                             
       ON dbo.FRM_REC_OVL                                              
        (                                                               
         FRM_NBR ASC                                                    
        ,FRM_EDT_DT DESC                                                
        ,REC_TYP_CD ASC                                                 
        ,OVL_EDL_FRM_NM ASC                                             
        ,ST_ABB ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 14400                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FRM_TAG                                                     
        (                                                               
        EDL_FRM_NM  CHAR(30) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,TAG_NM  CHAR(30) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,LEN_NBR  SMALLINT NOT NULL                                      
       ,TAG_OCC_CNT  SMALLINT NOT NULL                                  
       ,DEL_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1I                                            
         PRIMARY KEY                                                    
         (                                                              
          EDL_FRM_NM                                                    
          ,TAG_NM                                                       
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1I                                             
       ON dbo.FRM_TAG                                                  
        (                                                               
         EDL_FRM_NM ASC                                                 
        ,TAG_NM ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 24000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.NOT_CO_TYP                                                  
        (                                                               
        ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,CO_CD  CHAR(5) NOT NULL                                         
         FOR SBCS DATA                                                  
       ,SPE_CRT_CD  CHAR(5)                                             
         FOR SBCS DATA                                                  
       ,DTB_CD  CHAR(1) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1B                                            
         PRIMARY KEY                                                    
         (                                                              
          ACT_NOT_TYP_CD                                                
          ,CO_CD                                                        
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1B                                             
       ON dbo.NOT_CO_TYP                                               
        (                                                               
         ACT_NOT_TYP_CD ASC                                             
        ,CO_CD ASC                                                      
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.NOT_DAY_RQR                                                 
        (                                                               
        ST_ABB  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,POL_TYP_CD  CHAR(3) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,MIN_POL_EFF_RGEDAY  SMALLINT NOT NULL                           
       ,NBR_OF_NOT_DAY  SMALLINT NOT NULL                               
       ,MAX_POL_EFF_RGEDAY  SMALLINT                                    
        ,                                                               
         CONSTRAINT XZI01S13                                            
         PRIMARY KEY                                                    
         (                                                              
          ST_ABB                                                        
          ,ACT_NOT_TYP_CD                                               
          ,POL_TYP_CD                                                   
          ,MIN_POL_EFF_RGEDAY                                           
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S13                                             
       ON dbo.NOT_DAY_RQR                                              
        (                                                               
         ST_ABB ASC                                                     
        ,ACT_NOT_TYP_CD ASC                                             
        ,POL_TYP_CD ASC                                                 
        ,MIN_POL_EFF_RGEDAY ASC                                         
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.OVL_TYP                                                     
        (                                                               
        OVL_EDL_FRM_NM  CHAR(30) NOT NULL                               
         FOR SBCS DATA                                                  
       ,OVL_DES  CHAR(30) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,XCLV_IND  CHAR(1) NOT NULL                                      
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1F                                            
         PRIMARY KEY                                                    
         (                                                              
          OVL_EDL_FRM_NM                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1F                                             
       ON dbo.OVL_TYP                                                  
        (                                                               
         OVL_EDL_FRM_NM ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.REC_CO_TYP                                                  
        (                                                               
        REC_TYP_CD  CHAR(5) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,CO_CD  CHAR(5) NOT NULL                                         
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S19                                            
         PRIMARY KEY                                                    
         (                                                              
          REC_TYP_CD                                                    
          ,CO_CD                                                        
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S19                                             
       ON dbo.REC_CO_TYP                                               
        (                                                               
         REC_TYP_CD ASC                                                 
        ,CO_CD ASC                                                      
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.REC_TYP                                                     
        (                                                               
        REC_TYP_CD  CHAR(5) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,REC_SHT_DES  CHAR(13) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,REC_LNG_DES  CHAR(30) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,REC_SR_ORD_NBR  SMALLINT NOT NULL                               
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S1E                                            
         PRIMARY KEY                                                    
         (                                                              
          REC_TYP_CD                                                    
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S1E                                             
       ON dbo.REC_TYP                                                  
        (                                                               
         REC_TYP_CD ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ST_CNC_WRD_RQR                                              
        (                                                               
        ST_ABB  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ACT_NOT_TYP_CD  CHAR(5) NOT NULL                                
         FOR SBCS DATA                                                  
       ,POL_COV_CD  CHAR(4) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,ST_WRD_SEQ_CD  CHAR(5) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,ST_WRD_EFF_DT  DATE NOT NULL                                    
       ,ST_WRD_EXP_DT  DATE                                             
        ,                                                               
         CONSTRAINT XZI01S18                                            
         PRIMARY KEY                                                    
         (                                                              
          ST_ABB                                                        
          ,ACT_NOT_TYP_CD                                               
          ,POL_COV_CD                                                   
          ,ST_WRD_SEQ_CD                                                
          ,ST_WRD_EFF_DT                                                
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST1                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S18                                             
       ON dbo.ST_CNC_WRD_RQR                                           
        (                                                               
         ST_ABB ASC                                                     
        ,ACT_NOT_TYP_CD ASC                                             
        ,POL_COV_CD ASC                                                 
        ,ST_WRD_SEQ_CD ASC                                              
        ,ST_WRD_EFF_DT DESC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE XZS01ST2                                               
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 2400                                                    
         SECQTY 1216                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP32K                                              
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.ST_WRD_DES                                                  
        (                                                               
        ST_WRD_SEQ_CD  CHAR(5) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,ST_WRD_FONT_TYP  CHAR(6)                                        
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,ST_WRD_TXT  VARCHAR(4500) NOT NULL                              
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT XZI01S20                                            
         PRIMARY KEY                                                    
         (                                                              
          ST_WRD_SEQ_CD                                                 
         )                                                              
        )                                                               
         IN XZDP901.XZS01ST2                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.XZI01S20                                             
       ON dbo.ST_WRD_DES                                               
        (                                                               
         ST_WRD_SEQ_CD ASC                                              
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBAUL                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_AUDIT_LOG                                               
        (                                                               
        HAUL_EVENT_ID  CHAR(10) NOT NULL                                
         FOR SBCS DATA                                                  
       ,UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HAUL_EVENT_TS  TIMESTAMP NOT NULL                               
       ,HAUL_PARTITION_NBR  SMALLINT NOT NULL                           
       ,USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,EVENT_DTA_TXT  CHAR(100)                                        
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT HAUL_EVENT_ID                                       
         PRIMARY KEY                                                    
         (                                                              
          HAUL_EVENT_ID                                                 
          ,UOW_NM                                                       
          ,HAUL_EVENT_TS                                                
          ,HAUL_PARTITION_NBR                                           
          ,USERID                                                       
         )                                                              
        )                                                               
         IN XZDP901.HALBAUL                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIAUL1                                             
       ON dbo.HAL_AUDIT_LOG                                            
        (                                                               
         HAUL_EVENT_ID ASC                                              
        ,UOW_NM ASC                                                     
        ,HAUL_EVENT_TS ASC                                              
        ,HAUL_PARTITION_NBR ASC                                         
        ,USERID ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_AUDIT_LOG_V                                             
        (                                                               
         HAUL_EVENT_ID,                                                 
         UOW_NM,                                                        
         HAUL_EVENT_TS,                                                 
         HAUL_PARTITION_NBR,                                            
         USERID,                                                        
         EVENT_DTA_TXT                                                  
        )                                                               
 AS SELECT HAUL_EVENT_ID, UOW_NM, HAUL_EVENT_TS, HAUL_PARTITION_NBR,    
    USERID, EVENT_DTA_TXT                                               
 FROM dbo.HAL_AUDIT_LOG                                                     
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBBAT                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 204                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_BO_AUD_TGR                                              
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HBAT_BUS_OBJ_NM  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HBAT_TGR_IND  CHAR(1) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,HBAT_MODULE_NM  CHAR(8) NOT NULL                                
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,HBAT_BUS_OBJ_NM                                              
          ,HBAT_TGR_IND                                                 
          ,HBAT_MODULE_NM                                               
         )                                                              
        )                                                               
         IN XZDP901.HALBBAT                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIBAT1                                             
       ON dbo.HAL_BO_AUD_TGR                                           
        (                                                               
         UOW_NM ASC                                                     
        ,HBAT_BUS_OBJ_NM ASC                                            
        ,HBAT_TGR_IND ASC                                               
        ,HBAT_MODULE_NM ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_BO_AUD_TGR_V                                            
        (                                                               
         UOW_NM,                                                        
         HBAT_BUS_OBJ_NM,                                               
         HBAT_TGR_IND,                                                  
         HBAT_MODULE_NM                                                 
        )                                                               
 AS SELECT UOW_NM, HBAT_BUS_OBJ_NM, HBAT_TGR_IND, HBAT_MODULE_NM        
 FROM dbo.HAL_BO_AUD_TGR                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBBLE                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_BO_LOK_EXC                                              
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,BUS_OBJ_NM  CHAR(32) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,HBLE_CONTEXT_TXT  CHAR(30) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HBLE_LOK_EXC_MDU  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,BUS_OBJ_NM                                                   
         )                                                              
        )                                                               
         IN XZDP901.HALBBLE                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIBLE1                                             
       ON dbo.HAL_BO_LOK_EXC                                           
        (                                                               
         UOW_NM ASC                                                     
        ,BUS_OBJ_NM ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_BO_LOK_EXC_V                                            
        (                                                               
         UOW_NM,                                                        
         BUS_OBJ_NM,                                                    
         HBLE_CONTEXT_TXT,                                              
         HBLE_LOK_EXC_MDU                                               
        )                                                               
 AS SELECT UOW_NM, BUS_OBJ_NM, HBLE_CONTEXT_TXT, HBLE_LOK_EXC_MDU       
 FROM dbo.HAL_BO_LOK_EXC                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBBLT                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_BO_LOK_TYPE                                             
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,BUS_OBJ_NM  CHAR(32) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,LOK_TYPE_CD  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,BUS_OBJ_NM                                                   
          ,LOK_TYPE_CD                                                  
         )                                                              
        )                                                               
         IN XZDP901.HALBBLT                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIBLT1                                             
       ON dbo.HAL_BO_LOK_TYPE                                          
        (                                                               
         UOW_NM ASC                                                     
        ,BUS_OBJ_NM ASC                                                 
        ,LOK_TYPE_CD ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_BO_LOK_TYPE_V                                           
        (                                                               
         UOW_NM,                                                        
         BUS_OBJ_NM,                                                    
         LOK_TYPE_CD                                                    
        )                                                               
 AS SELECT UOW_NM, BUS_OBJ_NM, LOK_TYPE_CD                              
 FROM dbo.HAL_BO_LOK_TYPE                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBBMX                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_BO_MDU_XRF                                              
        (                                                               
        BUS_OBJ_NM  CHAR(32) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,HBMX_BOBJ_MDU_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HBMX_CPX_ED_MDU_NM  CHAR(32) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HBMX_DP_DFL_MDU_NM  CHAR(32) NOT NULL                           
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT BUS_OBJ_NM                                          
         PRIMARY KEY                                                    
         (                                                              
          BUS_OBJ_NM                                                    
         )                                                              
        )                                                               
         IN XZDP901.HALBBMX                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIBMX1                                             
       ON dbo.HAL_BO_MDU_XRF                                           
        (                                                               
         BUS_OBJ_NM ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_BO_MDU_XRF_V                                            
        (                                                               
         BUS_OBJ_NM,                                                    
         HBMX_BOBJ_MDU_NM,                                              
         HBMX_CPX_ED_MDU_NM,                                            
         HBMX_DP_DFL_MDU_NM                                             
        )                                                               
 AS SELECT BUS_OBJ_NM, HBMX_BOBJ_MDU_NM, HBMX_CPX_ED_MDU_NM,            
    HBMX_DP_DFL_MDU_NM                                                  
 FROM dbo.HAL_BO_MDU_XRF                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBELD                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE ROW                                                   
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_ERR_LOG_DTL                                             
        (                                                               
        ERR_RFR_NBR  CHAR(10) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,FAIL_TS  TIMESTAMP NOT NULL                                     
       ,USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,FAIL_KEY_TXT  CHAR(200) NOT NULL                                
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT ERR_RFR_NBR                                         
         PRIMARY KEY                                                    
         (                                                              
          ERR_RFR_NBR                                                   
          ,FAIL_TS                                                      
         )                                                              
        )                                                               
         IN XZDP901.HALBELD                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIELD1                                             
       ON dbo.HAL_ERR_LOG_DTL                                          
        (                                                               
         ERR_RFR_NBR ASC                                                
        ,FAIL_TS ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48000                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_ERR_LOG_DTL_V                                           
        (                                                               
         ERR_RFR_NBR,                                                   
         FAIL_TS,                                                       
         USERID,                                                        
         FAIL_KEY_TXT                                                   
        )                                                               
 AS SELECT ERR_RFR_NBR, FAIL_TS, USERID, FAIL_KEY_TXT                   
 FROM dbo.HAL_ERR_LOG_DTL                                              
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBELF                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 7200                                                    
         SECQTY 480                                                     
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_ERR_LOG_FAIL                                            
        (                                                               
        UOW_ID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,FAIL_TS  TIMESTAMP NOT NULL                                     
       ,HELF_FAIL_APP_NM  CHAR(10) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_FAIL_PLF  CHAR(10) NOT NULL                                
         FOR SBCS DATA                                                  
       ,HELF_FAIL_TYPE_CD  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
       ,FAIL_ACY_CD  CHAR(8) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,HELF_FAIL_PGM_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_FAIL_PARA_NM  CHAR(30) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HELF_ERR_ADD_TXT  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,SQL_CODE  INTEGER                                               
       ,HELF_CICSRSP_CD  INTEGER NOT NULL                               
       ,HELF_ERR_CMT_TXT  CHAR(50) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_FAIL_UOW_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_FAIL_LOC_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_SQLERRMC_TXT  CHAR(70) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HELF_CICSRSP2_CD  INTEGER NOT NULL                              
       ,USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_SYS_ID  INTEGER NOT NULL                                    
       ,HELF_FAIL_KEY_TXT  CHAR(200) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HELF_ACY_TXT  CHAR(250) NOT NULL                                
         FOR SBCS DATA                                                  
       ,HELF_PRIORITY_CD  SMALLINT NOT NULL                             
       ,HELF_ERR_RFR_NBR  CHAR(10) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HELF_ERR_TXT  CHAR(100) NOT NULL                                
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_ID                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_ID                                                        
          ,FAIL_TS                                                      
         )                                                              
        )                                                               
         IN XZDP901.HALBELF                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIELF1                                             
       ON dbo.HAL_ERR_LOG_FAIL                                         
        (                                                               
         UOW_ID ASC                                                     
        ,FAIL_TS ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIELF2                                             
       ON dbo.HAL_ERR_LOG_FAIL                                         
        (                                                               
         FAIL_TS ASC                                                    
        ,HELF_FAIL_APP_NM ASC                                           
        ,HELF_FAIL_PLF ASC                                              
        ,HELF_FAIL_TYPE_CD ASC                                          
        ,FAIL_ACY_CD ASC                                                
        ,HELF_FAIL_PGM_NM ASC                                           
        ,HELF_FAIL_PARA_NM ASC                                          
        ,HELF_ERR_ADD_TXT ASC                                           
        ,SQL_CODE ASC                                                   
        ,HELF_CICSRSP_CD ASC                                            
        ,HELF_ERR_CMT_TXT ASC                                           
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIELF3                                             
       ON dbo.HAL_ERR_LOG_FAIL                                         
        (                                                               
         HELF_FAIL_APP_NM ASC                                           
        ,HELF_FAIL_PLF ASC                                              
        ,HELF_FAIL_TYPE_CD ASC                                          
        ,FAIL_ACY_CD ASC                                                
        ,HELF_FAIL_PGM_NM ASC                                           
        ,HELF_FAIL_PARA_NM ASC                                          
        ,HELF_ERR_ADD_TXT ASC                                           
        ,SQL_CODE ASC                                                   
        ,HELF_CICSRSP_CD ASC                                            
        ,HELF_ERR_CMT_TXT ASC                                           
        ,FAIL_TS ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIELF4                                             
       ON dbo.HAL_ERR_LOG_FAIL                                         
        (                                                               
         HELF_ERR_RFR_NBR ASC                                           
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_ERR_LOG_FAIL_V                                          
        (                                                               
         UOW_ID,                                                        
         FAIL_TS,                                                       
         HELF_FAIL_APP_NM,                                              
         HELF_FAIL_PLF,                                                 
         HELF_FAIL_TYPE_CD,                                             
         FAIL_ACY_CD,                                                   
         HELF_FAIL_PGM_NM,                                              
         HELF_FAIL_PARA_NM,                                             
         HELF_ERR_ADD_TXT,                                              
         SQL_CODE,                                                      
         HELF_CICSRSP_CD,                                               
         HELF_ERR_CMT_TXT,                                              
         HELF_FAIL_UOW_NM,                                              
         HELF_FAIL_LOC_NM,                                              
         HELF_SQLERRMC_TXT,                                             
         HELF_CICSRSP2_CD,                                              
         USERID,                                                        
         SEC_SYS_ID,                                                    
         HELF_FAIL_KEY_TXT,                                             
         HELF_ACY_TXT,                                                  
         HELF_PRIORITY_CD,                                              
         HELF_ERR_RFR_NBR,                                              
         HELF_ERR_TXT                                                   
        )                                                               
 AS SELECT UOW_ID, FAIL_TS, HELF_FAIL_APP_NM, HELF_FAIL_PLF,            
    HELF_FAIL_TYPE_CD, FAIL_ACY_CD, HELF_FAIL_PGM_NM, HELF_FAIL_PARA_NM,
    HELF_ERR_ADD_TXT, SQL_CODE, HELF_CICSRSP_CD, HELF_ERR_CMT_TXT,      
    HELF_FAIL_UOW_NM, HELF_FAIL_LOC_NM, HELF_SQLERRMC_TXT,              
    HELF_CICSRSP2_CD, USERID, SEC_SYS_ID, HELF_FAIL_KEY_TXT,            
    HELF_ACY_TXT, HELF_PRIORITY_CD, HELF_ERR_RFR_NBR, HELF_ERR_TXT      
 FROM dbo.HAL_ERR_LOG_FAIL                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBEMC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 2400                                                    
         SECQTY 240                                                     
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_ERR_LOG_MCM                                             
        (                                                               
        UOW_ID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,FAIL_TS  TIMESTAMP NOT NULL                                     
       ,HEMC_MCM_NM  CHAR(32) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,HEMC_PRI_BOBJ_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HEMC_STG_TYP_CD  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,HEMC_PASS_ACY_CD  CHAR(20) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HEMC_NBR_REQ_ROWS  SMALLINT NOT NULL                            
       ,HEMC_NBR_SWI_ROWS  SMALLINT NOT NULL                            
       ,HEMC_NBR_RSP_HDR  SMALLINT NOT NULL                             
       ,HEMC_NBR_RSP_DTA  SMALLINT NOT NULL                             
       ,HEMC_NBR_RSP_WNG  SMALLINT NOT NULL                             
       ,HEMC_NBR_RSP_NLBE  SMALLINT NOT NULL                            
       ,HEMC_NBR_PRC_HDR  SMALLINT NOT NULL                             
       ,HEMC_NBR_PRC_DTA  SMALLINT NOT NULL                             
       ,HEMC_NBR_PRC_WNG  SMALLINT NOT NULL                             
       ,HEMC_NBR_PRC_NLBE  SMALLINT NOT NULL                            
       ,HEMC_LOCK_TYPE_CD  CHAR(10) NOT NULL                            
         FOR SBCS DATA                                                  
       ,LOCK_ATN_CD  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,LOCK_DEL_TM  INTEGER NOT NULL                                   
       ,HEMC_LOCK_CTL_IND  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HEMC_DTA_PVC_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HEMC_SEC_CTXT_IND  CHAR(20)                                     
         FOR SBCS DATA                                                  
       ,HEMC_AUT_NBR  SMALLINT NOT NULL                                 
       ,HEMC_ASC_TYP_CD  CHAR(8)                                        
         FOR SBCS DATA                                                  
       ,HEMC_DP_RET_CD  CHAR(10)                                        
         FOR SBCS DATA                                                  
       ,HEMC_AUDIT_IND  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,HEMC_AUDIT_BOBJ_NM  CHAR(32)                                    
         FOR SBCS DATA                                                  
       ,HEMC_AUD_DTA_TXT  CHAR(100)                                     
         FOR SBCS DATA                                                  
       ,HEMC_KEEP_CLR_IND  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_ID                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_ID                                                        
          ,FAIL_TS                                                      
         )                                                              
        )                                                               
         IN XZDP901.HALBEMC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIEMC1                                             
       ON dbo.HAL_ERR_LOG_MCM                                          
        (                                                               
         UOW_ID ASC                                                     
        ,FAIL_TS ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_ERR_LOG_MCM_V                                           
        (                                                               
         UOW_ID,                                                        
         FAIL_TS,                                                       
         HEMC_MCM_NM,                                                   
         HEMC_PRI_BOBJ_NM,                                              
         HEMC_STG_TYP_CD,                                               
         HEMC_PASS_ACY_CD,                                              
         HEMC_NBR_REQ_ROWS,                                             
         HEMC_NBR_SWI_ROWS,                                             
         HEMC_NBR_RSP_HDR,                                              
         HEMC_NBR_RSP_DTA,                                              
         HEMC_NBR_RSP_WNG,                                              
         HEMC_NBR_RSP_NLBE,                                             
         HEMC_NBR_PRC_HDR,                                              
         HEMC_NBR_PRC_DTA,                                              
         HEMC_NBR_PRC_WNG,                                              
         HEMC_NBR_PRC_NLBE,                                             
         HEMC_LOCK_TYPE_CD,                                             
         LOCK_ATN_CD,                                                   
         LOCK_DEL_TM,                                                   
         HEMC_LOCK_CTL_IND,                                             
         HEMC_DTA_PVC_IND,                                              
         HEMC_SEC_CTXT_IND,                                             
         HEMC_AUT_NBR,                                                  
         HEMC_ASC_TYP_CD,                                               
         HEMC_DP_RET_CD,                                                
         HEMC_AUDIT_IND,                                                
         HEMC_AUDIT_BOBJ_NM,                                            
         HEMC_AUD_DTA_TXT,                                              
         HEMC_KEEP_CLR_IND                                              
        )                                                               
 AS SELECT UOW_ID, FAIL_TS, HEMC_MCM_NM, HEMC_PRI_BOBJ_NM,              
    HEMC_STG_TYP_CD, HEMC_PASS_ACY_CD, HEMC_NBR_REQ_ROWS,               
    HEMC_NBR_SWI_ROWS, HEMC_NBR_RSP_HDR, HEMC_NBR_RSP_DTA,              
    HEMC_NBR_RSP_WNG, HEMC_NBR_RSP_NLBE, HEMC_NBR_PRC_HDR,              
    HEMC_NBR_PRC_DTA, HEMC_NBR_PRC_WNG, HEMC_NBR_PRC_NLBE,              
    HEMC_LOCK_TYPE_CD, LOCK_ATN_CD, LOCK_DEL_TM, HEMC_LOCK_CTL_IND,     
    HEMC_DTA_PVC_IND, HEMC_SEC_CTXT_IND, HEMC_AUT_NBR, HEMC_ASC_TYP_CD, 
    HEMC_DP_RET_CD, HEMC_AUDIT_IND, HEMC_AUDIT_BOBJ_NM,                 
    HEMC_AUD_DTA_TXT, HEMC_KEEP_CLR_IND                                 
 FROM dbo.HAL_ERR_LOG_MCM                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBEMD                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_ERR_LOG_MDRV                                            
        (                                                               
        UOW_ID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,FAIL_TS  TIMESTAMP NOT NULL                                     
       ,TERMINAL_ID  CHAR(8) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,HEMD_MSG_TRF_CD  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,HEMD_CUR_CHUNK_NBR  SMALLINT NOT NULL                           
       ,HEMD_DEL_STG_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HEMD_DEL_MECH_IND  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HEMD_DEL_WNG_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HEMD_RET_WNG_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HEMD_TOT_LEN_QTY  INTEGER NOT NULL                              
       ,HEMD_BUF_LEN_QTY  INTEGER NOT NULL                              
       ,HEMD_NBR_CHUNKS  SMALLINT NOT NULL                              
        ,                                                               
         CONSTRAINT UOW_ID                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_ID                                                        
          ,FAIL_TS                                                      
         )                                                              
        )                                                               
         IN XZDP901.HALBEMD                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIEMD1                                             
       ON dbo.HAL_ERR_LOG_MDRV                                         
        (                                                               
         UOW_ID ASC                                                     
        ,FAIL_TS ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_ERR_LOG_MDRV_V                                          
        (                                                               
         UOW_ID,                                                        
         FAIL_TS,                                                       
         TERMINAL_ID,                                                   
         HEMD_MSG_TRF_CD,                                               
         HEMD_CUR_CHUNK_NBR,                                            
         HEMD_DEL_STG_IND,                                              
         HEMD_DEL_MECH_IND,                                             
         HEMD_DEL_WNG_IND,                                              
         HEMD_RET_WNG_IND,                                              
         HEMD_TOT_LEN_QTY,                                              
         HEMD_BUF_LEN_QTY,                                              
         HEMD_NBR_CHUNKS                                                
        )                                                               
 AS SELECT UOW_ID, FAIL_TS, TERMINAL_ID, HEMD_MSG_TRF_CD,               
    HEMD_CUR_CHUNK_NBR, HEMD_DEL_STG_IND, HEMD_DEL_MECH_IND,            
    HEMD_DEL_WNG_IND, HEMD_RET_WNG_IND, HEMD_TOT_LEN_QTY,               
    HEMD_BUF_LEN_QTY, HEMD_NBR_CHUNKS                                   
 FROM dbo.HAL_ERR_LOG_MDRV                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBETS                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_ERR_TRAN_SUP                                            
        (                                                               
        FAIL_ACY_CD  CHAR(8) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,APP_NM  CHAR(10) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HETS_ERR_ATN_CD  CHAR(30) NOT NULL WITH DEFAULT                 
         FOR SBCS DATA                                                  
       ,HETS_TYP_ERR  CHAR(30) NOT NULL WITH DEFAULT                    
         FOR SBCS DATA                                                  
       ,HETS_ERR_PTY_NBR  SMALLINT NOT NULL WITH DEFAULT                
       ,HETS_ERR_TXT  CHAR(100) NOT NULL WITH DEFAULT                   
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT FAIL_ACY_CD                                         
         PRIMARY KEY                                                    
         (                                                              
          FAIL_ACY_CD                                                   
          ,APP_NM                                                       
          ,HETS_ERR_ATN_CD                                              
          ,HETS_TYP_ERR                                                 
         )                                                              
        )                                                               
         IN XZDP901.HALBETS                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIETS1                                             
       ON dbo.HAL_ERR_TRAN_SUP                                         
        (                                                               
         FAIL_ACY_CD ASC                                                
        ,APP_NM ASC                                                     
        ,HETS_ERR_ATN_CD ASC                                            
        ,HETS_TYP_ERR ASC                                               
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_ERR_TRAN_SUP_V                                          
        (                                                               
         FAIL_ACY_CD,                                                   
         APP_NM,                                                        
         HETS_ERR_ATN_CD,                                               
         HETS_TYP_ERR,                                                  
         HETS_ERR_PTY_NBR,                                              
         HETS_ERR_TXT                                                   
        )                                                               
 AS SELECT FAIL_ACY_CD, APP_NM, HETS_ERR_ATN_CD, HETS_TYP_ERR,          
    HETS_ERR_PTY_NBR, HETS_ERR_TXT                                      
 FROM dbo.HAL_ERR_TRAN_SUP                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBFLA                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_FLD_LVL_AUT                                             
        (                                                               
        SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,HFLA_SEC_ASC_TYP  CHAR(8) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HFLA_FLD_ID  CHAR(50) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,HFLA_AUT_AMT  DECIMAL(9, 2) NOT NULL                            
       ,HFLA_AUT_DT  DATE                                               
        ,                                                               
         CONSTRAINT SEC_GRP_NM                                          
         PRIMARY KEY                                                    
         (                                                              
          SEC_GRP_NM                                                    
          ,HFLA_SEC_ASC_TYP                                             
          ,HFLA_FLD_ID                                                  
         )                                                              
        )                                                               
         IN XZDP901.HALBFLA                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIFLA1                                             
       ON dbo.HAL_FLD_LVL_AUT                                          
        (                                                               
         SEC_GRP_NM ASC                                                 
        ,HFLA_SEC_ASC_TYP ASC                                           
        ,HFLA_FLD_ID ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_FLD_LVL_AUT_V                                           
        (                                                               
         SEC_GRP_NM,                                                    
         HFLA_SEC_ASC_TYP,                                              
         HFLA_FLD_ID,                                                   
         HFLA_AUT_AMT,                                                  
         HFLA_AUT_DT                                                    
        )                                                               
 AS SELECT SEC_GRP_NM, HFLA_SEC_ASC_TYP, HFLA_FLD_ID, HFLA_AUT_AMT,     
    HFLA_AUT_DT                                                         
 FROM dbo.HAL_FLD_LVL_AUT                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBHSP                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_SEC_PROFILE                                             
        (                                                               
        HSP_ID_KEY  CHAR(32) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,SEC_PROFILE_TYP  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,SEQ_NBR  SMALLINT NOT NULL                                      
       ,HSP_GEN_TXT_FLD_1  CHAR(32) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSP_GEN_TXT_FLD_2  CHAR(32) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSP_GEN_TXT_FLD_3  CHAR(16) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSP_GEN_TXT_FLD_4  CHAR(16) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSP_GEN_TXT_FLD_5  CHAR(8) NOT NULL                             
         FOR SBCS DATA                                                  
       ,EFFECTIVE_DT  DATE NOT NULL                                     
       ,EXPIRATION_DT  DATE NOT NULL                                    
       ,HSP_DTA_TXT  CHAR(200) NOT NULL                                 
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT HSP_ID_KEY                                          
         PRIMARY KEY                                                    
         (                                                              
          HSP_ID_KEY                                                    
          ,SEC_PROFILE_TYP                                              
          ,SEQ_NBR                                                      
          ,HSP_GEN_TXT_FLD_1                                            
          ,HSP_GEN_TXT_FLD_2                                            
          ,HSP_GEN_TXT_FLD_3                                            
          ,HSP_GEN_TXT_FLD_4                                            
          ,HSP_GEN_TXT_FLD_5                                            
          ,EFFECTIVE_DT                                                 
          ,EXPIRATION_DT                                                
         )                                                              
        )                                                               
         IN XZDP901.HALBHSP                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIHSP1                                             
       ON dbo.HAL_SEC_PROFILE                                          
        (                                                               
         HSP_ID_KEY ASC                                                 
        ,SEC_PROFILE_TYP ASC                                            
        ,SEQ_NBR ASC                                                    
        ,HSP_GEN_TXT_FLD_1 ASC                                          
        ,HSP_GEN_TXT_FLD_2 ASC                                          
        ,HSP_GEN_TXT_FLD_3 ASC                                          
        ,HSP_GEN_TXT_FLD_4 ASC                                          
        ,HSP_GEN_TXT_FLD_5 ASC                                          
        ,EFFECTIVE_DT ASC                                               
        ,EXPIRATION_DT ASC                                              
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_SEC_PROFILE_V                                           
        (                                                               
         HSP_ID_KEY,                                                    
         SEC_PROFILE_TYP,                                               
         SEQ_NBR,                                                       
         HSP_GEN_TXT_FLD_1,                                             
         HSP_GEN_TXT_FLD_2,                                             
         HSP_GEN_TXT_FLD_3,                                             
         HSP_GEN_TXT_FLD_4,                                             
         HSP_GEN_TXT_FLD_5,                                             
         EFFECTIVE_DT,                                                  
         EXPIRATION_DT,                                                 
         HSP_DTA_TXT                                                    
        )                                                               
 AS SELECT HSP_ID_KEY, SEC_PROFILE_TYP, SEQ_NBR, HSP_GEN_TXT_FLD_1,     
    HSP_GEN_TXT_FLD_2, HSP_GEN_TXT_FLD_3, HSP_GEN_TXT_FLD_4,            
    HSP_GEN_TXT_FLD_5, EFFECTIVE_DT, EXPIRATION_DT, HSP_DTA_TXT         
 FROM dbo.HAL_SEC_PROFILE                                              
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBLPC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_LOC_PREF_CNT                                            
        (                                                               
        HLPC_ATB_TYPE_CD  CHAR(10) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HLPC_ATB_PFX_TXT  CHAR(15) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HLPC_ATB_CNT  INTEGER NOT NULL WITH DEFAULT                     
        ,                                                               
         CONSTRAINT HLPC_ATB_TYPE_CD                                    
         PRIMARY KEY                                                    
         (                                                              
          HLPC_ATB_TYPE_CD                                              
          ,HLPC_ATB_PFX_TXT                                             
          ,HLPC_ATB_CNT                                                 
         )                                                              
        )                                                               
         IN XZDP901.HALBLPC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALILPC1                                             
       ON dbo.HAL_LOC_PREF_CNT                                         
        (                                                               
         HLPC_ATB_TYPE_CD ASC                                           
        ,HLPC_ATB_PFX_TXT ASC                                           
        ,HLPC_ATB_CNT ASC                                               
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_LOC_PREF_CNT_V                                          
        (                                                               
         HLPC_ATB_TYPE_CD,                                              
         HLPC_ATB_PFX_TXT,                                              
         HLPC_ATB_CNT                                                   
        )                                                               
 AS SELECT HLPC_ATB_TYPE_CD, HLPC_ATB_PFX_TXT, HLPC_ATB_CNT             
 FROM dbo.HAL_LOC_PREF_CNT                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBMTC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_MSG_TRANSPRT                                            
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HMTC_MSG_DEL_TM  INTEGER NOT NULL                               
       ,HMTC_WNG_DEL_TM  INTEGER NOT NULL                               
       ,HMTC_PASS_LNK_IND  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
       ,STG_TYP_CD  CHAR(1) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,HMTC_MDRV_REQ_STG  CHAR(32) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HMTC_MDRV_RSP_STG  CHAR(32) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HMTC_UOW_REQ_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_SWI_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_HDR_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_DTA_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_WNG_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_KRP_STG  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HMTC_UOW_NLBE_STG  CHAR(32) NOT NULL                            
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
         )                                                              
        )                                                               
         IN XZDP901.HALBMTC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIMTC1                                             
       ON dbo.HAL_MSG_TRANSPRT                                         
        (                                                               
         UOW_NM ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_MSG_TRANSPRT_V                                          
        (                                                               
         UOW_NM,                                                        
         HMTC_MSG_DEL_TM,                                               
         HMTC_WNG_DEL_TM,                                               
         HMTC_PASS_LNK_IND,                                             
         STG_TYP_CD,                                                    
         HMTC_MDRV_REQ_STG,                                             
         HMTC_MDRV_RSP_STG,                                             
         HMTC_UOW_REQ_STG,                                              
         HMTC_UOW_SWI_STG,                                              
         HMTC_UOW_HDR_STG,                                              
         HMTC_UOW_DTA_STG,                                              
         HMTC_UOW_WNG_STG,                                              
         HMTC_UOW_KRP_STG,                                              
         HMTC_UOW_NLBE_STG                                              
        )                                                               
 AS SELECT UOW_NM, HMTC_MSG_DEL_TM, HMTC_WNG_DEL_TM, HMTC_PASS_LNK_IND, 
    STG_TYP_CD, HMTC_MDRV_REQ_STG, HMTC_MDRV_RSP_STG, HMTC_UOW_REQ_STG, 
    HMTC_UOW_SWI_STG, HMTC_UOW_HDR_STG, HMTC_UOW_DTA_STG,               
    HMTC_UOW_WNG_STG, HMTC_UOW_KRP_STG, HMTC_UOW_NLBE_STG               
 FROM dbo.HAL_MSG_TRANSPRT                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBNLB                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_NLBE_WNG_TXT                                            
        (                                                               
        APP_NM  CHAR(10) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HNLB_ERR_WNG_CD  CHAR(10) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HNLB_ERR_WNG_TXT  VARCHAR(500) NOT NULL WITH DEFAULT            
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT APP_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          APP_NM                                                        
          ,HNLB_ERR_WNG_CD                                              
         )                                                              
        )                                                               
         IN XZDP901.HALBNLB                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALINLB1                                             
       ON dbo.HAL_NLBE_WNG_TXT                                         
        (                                                               
         APP_NM ASC                                                     
        ,HNLB_ERR_WNG_CD ASC                                            
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_NLBE_WNG_TXT_V                                          
        (                                                               
         APP_NM,                                                        
         HNLB_ERR_WNG_CD,                                               
         HNLB_ERR_WNG_TXT                                               
        )                                                               
 AS SELECT APP_NM, HNLB_ERR_WNG_CD, HNLB_ERR_WNG_TXT                    
 FROM dbo.HAL_NLBE_WNG_TXT                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBSDD                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_SEC_DP_DFLT                                             
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,HSDD_SEC_ASC_TYP  CHAR(8) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HSDD_GEN_TXT_FLD_1  CHAR(20) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HSDD_GEN_TXT_FLD_2  CHAR(8) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSDD_GEN_TXT_FLD_3  CHAR(8) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSDD_GEN_TXT_FLD_4  CHAR(8) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HSDD_GEN_TXT_FLD_5  CHAR(8) NOT NULL                            
         FOR SBCS DATA                                                  
       ,BUS_OBJ_NM  CHAR(32) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,HSDD_REQ_TYP_CD  CHAR(4) NOT NULL                               
         FOR SBCS DATA                                                  
       ,EFFECTIVE_DT  DATE NOT NULL                                     
       ,EXPIRATION_DT  DATE NOT NULL                                    
       ,HSDD_SEQ_NBR  INTEGER NOT NULL                                  
       ,HSDD_CMN_NM  CHAR(32) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,HSDD_CMN_ATR_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HSDD_DFL_TYP_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HSDD_DFL_DTA_AMT  DECIMAL(14, 2)                                
       ,HSDD_DFL_DTA_TXT  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HSDD_DFL_OFS_NBR  INTEGER                                       
       ,HSDD_DFL_OFS_PER  CHAR(8) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HSDD_CONTEXT  CHAR(20) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,HSDD_BSE_RLE_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,SEC_GRP_NM                                                   
          ,HSDD_SEC_ASC_TYP                                             
          ,HSDD_GEN_TXT_FLD_1                                           
          ,HSDD_GEN_TXT_FLD_2                                           
          ,HSDD_GEN_TXT_FLD_3                                           
          ,HSDD_GEN_TXT_FLD_4                                           
          ,HSDD_GEN_TXT_FLD_5                                           
          ,BUS_OBJ_NM                                                   
          ,HSDD_REQ_TYP_CD                                              
          ,EFFECTIVE_DT                                                 
          ,EXPIRATION_DT                                                
          ,HSDD_SEQ_NBR                                                 
         )                                                              
        )                                                               
         IN XZDP901.HALBSDD                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALISDD1                                             
       ON dbo.HAL_SEC_DP_DFLT                                          
        (                                                               
         UOW_NM ASC                                                     
        ,SEC_GRP_NM ASC                                                 
        ,HSDD_SEC_ASC_TYP ASC                                           
        ,HSDD_GEN_TXT_FLD_1 ASC                                         
        ,HSDD_GEN_TXT_FLD_2 ASC                                         
        ,HSDD_GEN_TXT_FLD_3 ASC                                         
        ,HSDD_GEN_TXT_FLD_4 ASC                                         
        ,HSDD_GEN_TXT_FLD_5 ASC                                         
        ,BUS_OBJ_NM ASC                                                 
        ,HSDD_REQ_TYP_CD ASC                                            
        ,EFFECTIVE_DT ASC                                               
        ,EXPIRATION_DT ASC                                              
        ,HSDD_SEQ_NBR ASC                                               
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALISDD2                                             
       ON dbo.HAL_SEC_DP_DFLT                                          
        (                                                               
         BUS_OBJ_NM ASC                                                 
        ,EFFECTIVE_DT ASC                                               
        ,EXPIRATION_DT ASC                                              
        ,UOW_NM ASC                                                     
        ,SEC_GRP_NM ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_SEC_DP_DFLT_V                                           
        (                                                               
         UOW_NM,                                                        
         SEC_GRP_NM,                                                    
         HSDD_SEC_ASC_TYP,                                              
         HSDD_GEN_TXT_FLD_1,                                            
         HSDD_GEN_TXT_FLD_2,                                            
         HSDD_GEN_TXT_FLD_3,                                            
         HSDD_GEN_TXT_FLD_4,                                            
         HSDD_GEN_TXT_FLD_5,                                            
         BUS_OBJ_NM,                                                    
         HSDD_REQ_TYP_CD,                                               
         EFFECTIVE_DT,                                                  
         EXPIRATION_DT,                                                 
         HSDD_SEQ_NBR,                                                  
         HSDD_CMN_NM,                                                   
         HSDD_CMN_ATR_IND,                                              
         HSDD_DFL_TYP_IND,                                              
         HSDD_DFL_DTA_AMT,                                              
         HSDD_DFL_DTA_TXT,                                              
         HSDD_DFL_OFS_NBR,                                              
         HSDD_DFL_OFS_PER,                                              
         HSDD_CONTEXT,                                                  
         HSDD_BSE_RLE_IND                                               
        )                                                               
 AS SELECT UOW_NM, SEC_GRP_NM, HSDD_SEC_ASC_TYP, HSDD_GEN_TXT_FLD_1,    
    HSDD_GEN_TXT_FLD_2, HSDD_GEN_TXT_FLD_3, HSDD_GEN_TXT_FLD_4,         
    HSDD_GEN_TXT_FLD_5, BUS_OBJ_NM, HSDD_REQ_TYP_CD, EFFECTIVE_DT,      
    EXPIRATION_DT, HSDD_SEQ_NBR, HSDD_CMN_NM, HSDD_CMN_ATR_IND,         
    HSDD_DFL_TYP_IND, HSDD_DFL_DTA_AMT, HSDD_DFL_DTA_TXT,               
    HSDD_DFL_OFS_NBR, HSDD_DFL_OFS_PER, HSDD_CONTEXT, HSDD_BSE_RLE_IND  
 FROM dbo.HAL_SEC_DP_DFLT                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBSEA                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_AUT                                                 
        (                                                               
        SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_AUT_LEVEL  SMALLINT NOT NULL                                
       ,HSEA_SEC_ASC_TYP  CHAR(8) NOT NULL                              
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT SEC_GRP_NM                                          
         PRIMARY KEY                                                    
         (                                                              
          SEC_GRP_NM                                                    
          ,UOW_NM                                                       
          ,SEC_AUT_LEVEL                                                
          ,HSEA_SEC_ASC_TYP                                             
         )                                                              
        )                                                               
         IN XZDP901.HALBSEA                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALISEA1                                             
       ON dbo.HAL_UOW_AUT                                              
        (                                                               
         SEC_GRP_NM ASC                                                 
        ,UOW_NM ASC                                                     
        ,SEC_AUT_LEVEL ASC                                              
        ,HSEA_SEC_ASC_TYP ASC                                           
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_AUT_V                                               
        (                                                               
         SEC_GRP_NM,                                                    
         UOW_NM,                                                        
         SEC_AUT_LEVEL,                                                 
         HSEA_SEC_ASC_TYP                                               
        )                                                               
 AS SELECT SEC_GRP_NM, UOW_NM, SEC_AUT_LEVEL, HSEA_SEC_ASC_TYP          
 FROM dbo.HAL_UOW_AUT                                                       
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBSIV                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_START_ITV                                               
        (                                                               
        TRANSACTION_ID  CHAR(4) NOT NULL                                
         FOR SBCS DATA                                                  
       ,START_MTE_DUR  SMALLINT NOT NULL                                
       ,SCH_ITV_MTE_DUR  SMALLINT NOT NULL                              
        ,                                                               
         CONSTRAINT TRANSACTION_ID                                      
         PRIMARY KEY                                                    
         (                                                              
          TRANSACTION_ID                                                
          ,START_MTE_DUR                                                
         )                                                              
        )                                                               
         IN XZDP901.HALBSIV                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALISIV1                                             
       ON dbo.HAL_START_ITV                                            
        (                                                               
         TRANSACTION_ID ASC                                             
        ,START_MTE_DUR ASC                                              
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_START_ITV_V                                             
        (                                                               
         TRANSACTION_ID,                                                
         START_MTE_DUR,                                                 
         SCH_ITV_MTE_DUR                                                
        )                                                               
 AS SELECT TRANSACTION_ID, START_MTE_DUR, SCH_ITV_MTE_DUR               
 FROM dbo.HAL_START_ITV                                                     
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBSPD                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_SEC_PROF_DES                                            
        (                                                               
        SEC_PROFILE_TYP  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,EFFECTIVE_DT  DATE NOT NULL                                     
       ,EXPIRATION_DT  DATE NOT NULL                                    
       ,SEC_PFL_TYPE_DES  CHAR(100) NOT NULL                            
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT SEC_PROFILE_TYP                                     
         PRIMARY KEY                                                    
         (                                                              
          SEC_PROFILE_TYP                                               
          ,EFFECTIVE_DT                                                 
          ,EXPIRATION_DT                                                
         )                                                              
        )                                                               
         IN XZDP901.HALBSPD                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALISPD1                                             
       ON dbo.HAL_SEC_PROF_DES                                         
        (                                                               
         SEC_PROFILE_TYP ASC                                            
        ,EFFECTIVE_DT ASC                                               
        ,EXPIRATION_DT ASC                                              
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_SEC_PROF_DES_V                                          
        (                                                               
         SEC_PROFILE_TYP,                                               
         EFFECTIVE_DT,                                                  
         EXPIRATION_DT,                                                 
         SEC_PFL_TYPE_DES                                               
        )                                                               
 AS SELECT SEC_PROFILE_TYP, EFFECTIVE_DT, EXPIRATION_DT,                
    SEC_PFL_TYPE_DES                                                    
 FROM dbo.HAL_SEC_PROF_DES                                             
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBSSV                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 480                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 4                                                      
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_SCRN_SCRIPT                                             
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,HSSV_CONTEXT  CHAR(20) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,HSSV_SCRN_FLD_ID  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HSSV_SEQ_NBR  INTEGER NOT NULL                                  
       ,HSSV_NEW_LIN_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HSSV_SCRIPT_TXT  VARCHAR(100) NOT NULL                          
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,SEC_GRP_NM                                                   
          ,HSSV_CONTEXT                                                 
          ,HSSV_SCRN_FLD_ID                                             
          ,HSSV_SEQ_NBR                                                 
         )                                                              
        )                                                               
         IN XZDP901.HALBSSV                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALISSV1                                             
       ON dbo.HAL_SCRN_SCRIPT                                          
        (                                                               
         UOW_NM ASC                                                     
        ,SEC_GRP_NM ASC                                                 
        ,HSSV_CONTEXT ASC                                               
        ,HSSV_SCRN_FLD_ID ASC                                           
        ,HSSV_SEQ_NBR ASC                                               
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 2400                                                    
         SECQTY 144                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_SCRN_SCRIPT_V                                           
        (                                                               
         UOW_NM,                                                        
         SEC_GRP_NM,                                                    
         HSSV_CONTEXT,                                                  
         HSSV_SCRN_FLD_ID,                                              
         HSSV_SEQ_NBR,                                                  
         HSSV_NEW_LIN_IND,                                              
         HSSV_SCRIPT_TXT                                                
        )                                                               
 AS SELECT UOW_NM, SEC_GRP_NM, HSSV_CONTEXT, HSSV_SCRN_FLD_ID,          
    HSSV_SEQ_NBR, HSSV_NEW_LIN_IND, HSSV_SCRIPT_TXT                     
 FROM dbo.HAL_SCRN_SCRIPT                                              
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUC2                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UNIVERSAL_CTL2                                          
        (                                                               
        HUC2_ENTRY_KEY_CD  CHAR(150) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HUC2_TBL_LBL_TXT  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,EFFECTIVE_DT  DATE NOT NULL                                     
       ,HUC2_ENTRY_DTA_TXT  CHAR(200) NOT NULL                          
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT HUC2_ENTRY_KEY_CD                                   
         PRIMARY KEY                                                    
         (                                                              
          HUC2_ENTRY_KEY_CD                                             
          ,HUC2_TBL_LBL_TXT                                             
          ,EFFECTIVE_DT                                                 
         )                                                              
        )                                                               
         IN XZDP901.HALBUC2                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUC21                                             
       ON dbo.HAL_UNIVERSAL_CTL2                                       
        (                                                               
         HUC2_ENTRY_KEY_CD ASC                                          
        ,HUC2_TBL_LBL_TXT ASC                                           
        ,EFFECTIVE_DT ASC                                               
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UNIVERSAL_CT_V                                          
        (                                                               
         HUC2_ENTRY_KEY_CD,                                             
         HUC2_TBL_LBL_TXT,                                              
         EFFECTIVE_DT,                                                  
         HUC2_ENTRY_DTA_TXT                                             
        )                                                               
 AS SELECT HUC2_ENTRY_KEY_CD, HUC2_TBL_LBL_TXT, EFFECTIVE_DT,           
    HUC2_ENTRY_DTA_TXT                                                  
 FROM dbo.HAL_UNIVERSAL_CTL2                                                
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUDC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_DATA_CTL                                            
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HUDC_NBR_RET  INTEGER NOT NULL                                  
       ,HUDC_NBR_ATTEMPT  INTEGER NOT NULL                              
       ,HUDC_WILDCARD  CHAR(1) NOT NULL WITH DEFAULT                    
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
         )                                                              
        )                                                               
         IN XZDP901.HALBUDC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUDC1                                             
       ON dbo.HAL_UOW_DATA_CTL                                         
        (                                                               
         UOW_NM ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_DATA_CTL_V                                          
        (                                                               
         UOW_NM,                                                        
         HUDC_NBR_RET,                                                  
         HUDC_NBR_ATTEMPT,                                              
         HUDC_WILDCARD                                                  
        )                                                               
 AS SELECT UOW_NM, HUDC_NBR_RET, HUDC_NBR_ATTEMPT, HUDC_WILDCARD        
 FROM dbo.HAL_UOW_DATA_CTL                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUFS                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 720                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_FUN_SEC                                             
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,HUFS_CONTEXT  CHAR(20) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,HUFS_SEC_ASC_TYP  CHAR(8) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HUFS_FUN_SEQ_NBR  SMALLINT NOT NULL                             
       ,HUFS_AUT_ACTION_CD  CHAR(32) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HUFS_AUT_FUN_NM  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HUFS_LNK_TO_MDU_NM  CHAR(8) NOT NULL                            
         FOR SBCS DATA                                                  
       ,HUFS_APP_NM  CHAR(8) NOT NULL                                   
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,SEC_GRP_NM                                                   
          ,HUFS_CONTEXT                                                 
          ,HUFS_SEC_ASC_TYP                                             
          ,HUFS_FUN_SEQ_NBR                                             
         )                                                              
        )                                                               
         IN XZDP901.HALBUFS                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUFS1                                             
       ON dbo.HAL_UOW_FUN_SEC                                          
        (                                                               
         UOW_NM ASC                                                     
        ,SEC_GRP_NM ASC                                                 
        ,HUFS_CONTEXT ASC                                               
        ,HUFS_SEC_ASC_TYP ASC                                           
        ,HUFS_FUN_SEQ_NBR ASC                                           
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_FUN_SEC_V                                           
        (                                                               
         UOW_NM,                                                        
         SEC_GRP_NM,                                                    
         HUFS_CONTEXT,                                                  
         HUFS_SEC_ASC_TYP,                                              
         HUFS_FUN_SEQ_NBR,                                              
         HUFS_AUT_ACTION_CD,                                            
         HUFS_AUT_FUN_NM,                                               
         HUFS_LNK_TO_MDU_NM,                                            
         HUFS_APP_NM                                                    
        )                                                               
 AS SELECT UOW_NM, SEC_GRP_NM, HUFS_CONTEXT, HUFS_SEC_ASC_TYP,          
    HUFS_FUN_SEQ_NBR, HUFS_AUT_ACTION_CD, HUFS_AUT_FUN_NM,              
    HUFS_LNK_TO_MDU_NM, HUFS_APP_NM                                     
 FROM dbo.HAL_UOW_FUN_SEC                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUGC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_USR_GRP_CTS                                             
        (                                                               
        USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,SEC_GRP_NM  CHAR(8) NOT NULL                                    
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT USERID                                              
         PRIMARY KEY                                                    
         (                                                              
          USERID                                                        
          ,SEC_GRP_NM                                                   
         )                                                              
        )                                                               
         IN XZDP901.HALBUGC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUGC1                                             
       ON dbo.HAL_USR_GRP_CTS                                          
        (                                                               
         USERID ASC                                                     
        ,SEC_GRP_NM ASC                                                 
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_USR_GRP_CTS_V                                           
        (                                                               
         USERID,                                                        
         SEC_GRP_NM                                                     
        )                                                               
 AS SELECT USERID, SEC_GRP_NM                                           
 FROM dbo.HAL_USR_GRP_CTS                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUNA                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 288                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UNATTEND_ACY                                            
        (                                                               
        TCH_KEY  CHAR(126) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,HUNA_ACY_TYPE_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HUNA_ACY_SCH_TS  TIMESTAMP NOT NULL                             
       ,HUNA_ACY_FAIL_IND  CHAR(1)                                      
         FOR SBCS DATA                                                  
       ,ERR_RFR_NBR  CHAR(10)                                           
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT TCH_KEY                                             
         PRIMARY KEY                                                    
         (                                                              
          TCH_KEY                                                       
          ,HUNA_ACY_TYPE_NM                                             
          ,HUNA_ACY_SCH_TS                                              
         )                                                              
        )                                                               
         IN XZDP901.HALBUNA                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUNA1                                             
       ON dbo.HAL_UNATTEND_ACY                                         
        (                                                               
         TCH_KEY ASC                                                    
        ,HUNA_ACY_TYPE_NM ASC                                           
        ,HUNA_ACY_SCH_TS ASC                                            
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIUNA2                                             
       ON dbo.HAL_UNATTEND_ACY                                         
        (                                                               
         HUNA_ACY_TYPE_NM ASC                                           
        ,HUNA_ACY_SCH_TS ASC                                            
        ,HUNA_ACY_FAIL_IND ASC                                          
        ,TCH_KEY ASC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UNATTEND_ACY_V                                          
        (                                                               
         TCH_KEY,                                                       
         HUNA_ACY_TYPE_NM,                                              
         HUNA_ACY_SCH_TS,                                               
         HUNA_ACY_FAIL_IND,                                             
         ERR_RFR_NBR                                                    
        )                                                               
 AS SELECT TCH_KEY, HUNA_ACY_TYPE_NM, HUNA_ACY_SCH_TS,                  
    HUNA_ACY_FAIL_IND, ERR_RFR_NBR                                      
 FROM dbo.HAL_UNATTEND_ACY                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUOH                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_OBJ_HIER                                            
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,ROOT_BOBJ_NM  CHAR(32) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,HUOH_HIER_SEQ_NBR  SMALLINT NOT NULL                            
       ,HUOH_PNT_BOBJ_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
       ,HUOH_CHD_BOBJ_NM  CHAR(32) NOT NULL                             
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,ROOT_BOBJ_NM                                                 
          ,HUOH_HIER_SEQ_NBR                                            
          ,HUOH_PNT_BOBJ_NM                                             
          ,HUOH_CHD_BOBJ_NM                                             
         )                                                              
        )                                                               
         IN XZDP901.HALBUOH                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUOH1                                             
       ON dbo.HAL_UOW_OBJ_HIER                                         
        (                                                               
         UOW_NM ASC                                                     
        ,ROOT_BOBJ_NM ASC                                               
        ,HUOH_HIER_SEQ_NBR ASC                                          
        ,HUOH_PNT_BOBJ_NM ASC                                           
        ,HUOH_CHD_BOBJ_NM ASC                                           
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_OBJ_HIER_V                                          
        (                                                               
         UOW_NM,                                                        
         ROOT_BOBJ_NM,                                                  
         HUOH_HIER_SEQ_NBR,                                             
         HUOH_PNT_BOBJ_NM,                                              
         HUOH_CHD_BOBJ_NM                                               
        )                                                               
 AS SELECT UOW_NM, ROOT_BOBJ_NM, HUOH_HIER_SEQ_NBR, HUOH_PNT_BOBJ_NM,   
    HUOH_CHD_BOBJ_NM                                                    
 FROM dbo.HAL_UOW_OBJ_HIER                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUPS                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_PRC_SEQ                                             
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,UOW_SEQ_NBR  SMALLINT NOT NULL                                  
       ,ROOT_BOBJ_NM  CHAR(32) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,HUPS_BRN_PRC_CD  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
          ,UOW_SEQ_NBR                                                  
          ,ROOT_BOBJ_NM                                                 
          ,HUPS_BRN_PRC_CD                                              
         )                                                              
        )                                                               
         IN XZDP901.HALBUPS                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUPS1                                             
       ON dbo.HAL_UOW_PRC_SEQ                                          
        (                                                               
         UOW_NM ASC                                                     
        ,UOW_SEQ_NBR ASC                                                
        ,ROOT_BOBJ_NM ASC                                               
        ,HUPS_BRN_PRC_CD ASC                                            
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIUPS2                                             
       ON dbo.HAL_UOW_PRC_SEQ                                          
        (                                                               
         UOW_NM ASC                                                     
        ,HUPS_BRN_PRC_CD ASC                                            
        ,ROOT_BOBJ_NM ASC                                               
        ,UOW_SEQ_NBR ASC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_PRC_SEQ_V                                           
        (                                                               
         UOW_NM,                                                        
         UOW_SEQ_NBR,                                                   
         ROOT_BOBJ_NM,                                                  
         HUPS_BRN_PRC_CD                                                
        )                                                               
 AS SELECT UOW_NM, UOW_SEQ_NBR, ROOT_BOBJ_NM, HUPS_BRN_PRC_CD           
 FROM dbo.HAL_UOW_PRC_SEQ                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUSR                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_USRS                                                    
        (                                                               
        USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,CLIENT_ID  CHAR(20) NOT NULL                                    
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT USERID                                              
         PRIMARY KEY                                                    
         (                                                              
          USERID                                                        
         )                                                              
        )                                                               
         IN XZDP901.HALBUSR                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUSR1                                             
       ON dbo.HAL_USRS                                                 
        (                                                               
         USERID ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALIUSR2                                             
       ON dbo.HAL_USRS                                                 
        (                                                               
         CLIENT_ID ASC                                                  
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_USRS_V                                                  
        (                                                               
         USERID,                                                        
         CLIENT_ID                                                      
        )                                                               
 AS SELECT USERID, CLIENT_ID                                            
 FROM dbo.HAL_USRS                                                          
     ;                                                                  
     CREATE                                                             
      TABLESPACE HALBUTC                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 240                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP11                                                
         LOCKSIZE PAGE                                                  
         LOCKMAX SYSTEM                                                 
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_UOW_TRANSACT                                            
        (                                                               
        UOW_NM  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,HUTC_MCM_MDU_NM  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HUTC_LOK_TMO_ITV  INTEGER NOT NULL                              
       ,HUTC_LOK_SGY_CD  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,HUTC_SEC_IND  CHAR(1) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,HUTC_SEC_MDU_NM  CHAR(32)                                       
         FOR SBCS DATA                                                  
       ,HUTC_DTA_PVC_IND  CHAR(1) NOT NULL                              
         FOR SBCS DATA                                                  
       ,HUTC_AUDIT_IND  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT UOW_NM                                              
         PRIMARY KEY                                                    
         (                                                              
          UOW_NM                                                        
         )                                                              
        )                                                               
         IN XZDP901.HALBUTC                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALIUTC1                                             
       ON dbo.HAL_UOW_TRANSACT                                         
        (                                                               
         UOW_NM ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP12                                                
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_UOW_TRANSACT_V                                          
        (                                                               
         UOW_NM,                                                        
         HUTC_MCM_MDU_NM,                                               
         HUTC_LOK_TMO_ITV,                                              
         HUTC_LOK_SGY_CD,                                               
         HUTC_SEC_IND,                                                  
         HUTC_SEC_MDU_NM,                                               
         HUTC_DTA_PVC_IND,                                              
         HUTC_AUDIT_IND                                                 
        )                                                               
 AS SELECT UOW_NM, HUTC_MCM_MDU_NM, HUTC_LOK_TMO_ITV, HUTC_LOK_SGY_CD,  
    HUTC_SEC_IND, HUTC_SEC_MDU_NM, HUTC_DTA_PVC_IND, HUTC_AUDIT_IND     
 FROM dbo.HAL_UOW_TRANSACT                                                  
     ;                                                                  
     CREATE                                                             
      TABLESPACE LOKBLOD                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 10                                                    
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE ROW                                                   
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_LOK_OBJ_DET                                             
        (                                                               
        HLOD_SESSION_ID  CHAR(32) NOT NULL                              
         FOR SBCS DATA                                                  
       ,TCH_KEY  CHAR(126) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,APP_ID  CHAR(10) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,TABLE_NM  CHAR(18) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,USERID  CHAR(32) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,LOK_TYPE_CD  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,LAST_ACY_TS  TIMESTAMP NOT NULL                                 
       ,HLOD_LOK_TMO_TS  TIMESTAMP NOT NULL                             
       ,HLOD_UPD_BY_MSG_ID  CHAR(32) NOT NULL                           
         FOR SBCS DATA                                                  
       ,HLOD_ZAPPED_IND  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,HLOD_ZAPPED_BY  CHAR(32) NOT NULL                               
         FOR SBCS DATA                                                  
       ,HLOD_ZAPPED_TS  TIMESTAMP NOT NULL                              
        ,                                                               
         CONSTRAINT HLOD_SESSION_ID                                     
         PRIMARY KEY                                                    
         (                                                              
          HLOD_SESSION_ID                                               
          ,TCH_KEY                                                      
          ,APP_ID                                                       
         )                                                              
        )                                                               
         IN XZDP901.LOKBLOD                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALILOD1                                             
       ON dbo.HAL_LOK_OBJ_DET                                          
        (                                                               
         HLOD_SESSION_ID ASC                                            
        ,TCH_KEY ASC                                                    
        ,APP_ID ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 14400                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALILOD2                                             
       ON dbo.HAL_LOK_OBJ_DET                                          
        (                                                               
         TCH_KEY ASC                                                    
        ,APP_ID ASC                                                     
        ,HLOD_SESSION_ID ASC                                            
        ,LOK_TYPE_CD ASC                                                
        ,HLOD_ZAPPED_IND ASC                                            
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 14400                                                   
         SECQTY 9600                                                    
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.HALILOD3                                             
       ON dbo.HAL_LOK_OBJ_DET                                          
        (                                                               
         HLOD_SESSION_ID ASC                                            
        ,HLOD_LOK_TMO_TS ASC                                            
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_LOK_OBJ_DET_V                                           
        (                                                               
         HLOD_SESSION_ID,                                               
         TCH_KEY,                                                       
         APP_ID,                                                        
         TABLE_NM,                                                      
         USERID,                                                        
         LOK_TYPE_CD,                                                   
         LAST_ACY_TS,                                                   
         HLOD_LOK_TMO_TS,                                               
         HLOD_UPD_BY_MSG_ID,                                            
         HLOD_ZAPPED_IND,                                               
         HLOD_ZAPPED_BY,                                                
         HLOD_ZAPPED_TS                                                 
        )                                                               
 AS SELECT HLOD_SESSION_ID, TCH_KEY, APP_ID, TABLE_NM, USERID,          
    LOK_TYPE_CD, LAST_ACY_TS, HLOD_LOK_TMO_TS, HLOD_UPD_BY_MSG_ID,      
    HLOD_ZAPPED_IND, HLOD_ZAPPED_BY, HLOD_ZAPPED_TS                     
 FROM dbo.HAL_LOK_OBJ_DET                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE LOKBLOH                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 1200                                                    
         SECQTY 240                                                     
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE ROW                                                   
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.HAL_LOK_OBJ_HDR                                             
        (                                                               
        TCH_KEY  CHAR(126) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,APP_ID  CHAR(10) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,LAST_ACY_TS  TIMESTAMP NOT NULL                                 
        ,                                                               
         CONSTRAINT TCH_KEY                                             
         PRIMARY KEY                                                    
         (                                                              
          TCH_KEY                                                       
          ,APP_ID                                                       
         )                                                              
        )                                                               
         IN XZDP901.LOKBLOH                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.HALILOH1                                             
       ON dbo.HAL_LOK_OBJ_HDR                                          
        (                                                               
         TCH_KEY ASC                                                    
        ,APP_ID ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 1056                                                    
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 5                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE VIEW                                                        
       dbo.HAL_LOK_OBJ_HDR_V                                           
        (                                                               
         TCH_KEY,                                                       
         APP_ID,                                                        
         LAST_ACY_TS                                                    
        )                                                               
 AS SELECT TCH_KEY, APP_ID, LAST_ACY_TS                                 
 FROM dbo.HAL_LOK_OBJ_HDR                                                   
     ;                                                                  
     CREATE                                                             
      TABLESPACE DBS0144                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.FED_RMT_PRT_TAB                                             
        (                                                               
        RPT_NBR  CHAR(6) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,RPT_RCD_STA_CD  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,LST_MDF_TS  TIMESTAMP NOT NULL                                  
       ,USR_ID  CHAR(8) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,RPT_DESC  CHAR(80) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,RPT_ACT_FLG  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,RPT_BAL_FLG  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,OFF_LOC_1  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_1  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_2  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_2  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_3  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_3  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_4  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_4  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_5  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_5  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_6  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_6  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_7  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_7  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_8  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_8  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_9  CHAR(2) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_9  CHAR(1) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,OFF_LOC_10  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_10  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_11  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_11  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_12  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_12  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_13  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_13  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_14  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_14  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_15  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,OFF_LOC_FLG_15  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,OFF_LOC_DEFLT_FLG  CHAR(1) NOT NULL                             
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT RPT_NBR                                             
         PRIMARY KEY                                                    
         (                                                              
          RPT_NBR                                                       
          ,RPT_RCD_STA_CD                                               
         )                                                              
        )                                                               
         IN XZDP901.DBS0144                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.FEDRPT01                                             
       ON dbo.FED_RMT_PRT_TAB                                          
        (                                                               
         RPT_NBR ASC                                                    
        ,RPT_RCD_STA_CD ASC                                             
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 9600                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE FWS0101                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 96000                                                   
         SECQTY 24000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS YES                                                   
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 1500                                                   
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.POL_DTA_EXT                                                 
        (                                                               
        POL_NBR  CHAR(25) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,POL_EFF_DT  DATE NOT NULL                                       
       ,PLN_EXP_DT  DATE NOT NULL                                       
       ,ACT_NBR  CHAR(9) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,POL_ID  CHAR(16) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,CMP_CD  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,POL_TYP_CD  CHAR(3) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,STS_POL_TYP_CD  CHAR(4) NOT NULL                                
         FOR SBCS DATA                                                  
       ,BND_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,MOL_CR_POL_IND  CHAR(1) NOT NULL                                
         FOR SBCS DATA                                                  
       ,AUD_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,LGE_PRD_IND  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,SPL_BIL_POL_IND  CHAR(1) NOT NULL                               
         FOR SBCS DATA                                                  
       ,SIR_AMT  DECIMAL(11)                                            
       ,PRI_RSK_ST_ABB  CHAR(2) NOT NULL                                
         FOR SBCS DATA                                                  
       ,PRI_RSK_ST_CD  CHAR(3) NOT NULL                                 
         FOR SBCS DATA                                                  
       ,PRI_RSK_CTY_CD  CHAR(3) NOT NULL                                
         FOR SBCS DATA                                                  
       ,PRI_RSK_TWN_CD  CHAR(4) NOT NULL                                
         FOR SBCS DATA                                                  
       ,REW_POL_NBR  CHAR(25)                                           
         FOR SBCS DATA                                                  
       ,RNL_QTE_IND  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,WHY_CNC_CD  CHAR(1)                                             
         FOR SBCS DATA                                                  
       ,TMN_EXP_CNC_DT  DATE                                            
       ,POL_PER_EN_TRS_CD  CHAR(2)                                      
         FOR SBCS DATA                                                  
       ,ACY_POL_IND  CHAR(1) NOT NULL                                   
         FOR SBCS DATA                                                  
       ,WRT_PRM_AMT  DECIMAL(12) NOT NULL                               
       ,FST_TRM_IND  CHAR(1) NOT NULL WITH DEFAULT 'N'                  
         FOR SBCS DATA                                                  
       ,SRC_SYS_CD  CHAR(2) NOT NULL WITH DEFAULT                       
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT FWI01011                                            
         PRIMARY KEY                                                    
         (                                                              
          POL_NBR                                                       
          ,POL_EFF_DT                                                   
          ,POL_ID                                                       
         )                                                              
        )                                                               
         IN XZDP901.FWS0101                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.FWI01011                                             
       ON dbo.POL_DTA_EXT                                              
        (                                                               
         POL_NBR ASC                                                    
        ,POL_EFF_DT DESC                                                
        ,POL_ID ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144000                                                  
         SECQTY 48000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.FWI01012                                             
       ON dbo.POL_DTA_EXT                                              
        (                                                               
         ACT_NBR ASC                                                    
        ,POL_NBR ASC                                                    
        ,POL_EFF_DT DESC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 480000                                                  
         SECQTY 240000                                                  
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 1 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
        INDEX dbo.FWI01013                                             
       ON dbo.POL_DTA_EXT                                              
        (                                                               
         POL_ID ASC                                                     
        ,POL_EFF_DT DESC                                                
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 96000                                                   
         SECQTY 24000                                                   
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
                                                                        
                                                                        
     CREATE                                                             
      TABLESPACE MUS11SPT                                               
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 1440                                                    
         SECQTY 480                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 4                                                      
         BUFFERPOOL BP11                                                
         LOCKSIZE ANY                                                   
         LOCKMAX SYSTEM                                                 
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
                                                                        
                                                                        
                                                                        
                                                                        
     CREATE TABLE                                                       
       dbo.STS_POL_TYP                                                 
        (                                                               
        SIII_POL_TYP_CD  CHAR(3) NOT NULL                               
         FOR SBCS DATA                                                  
       ,STS_POL_TYP_CD  CHAR(4) NOT NULL                                
         FOR SBCS DATA                                                  
       ,EFF_DT  DATE NOT NULL                                           
       ,STS_POL_TYP_DES  CHAR(75) NOT NULL                              
         FOR SBCS DATA                                                  
       ,SIII_POL_TYP_DES  CHAR(30) NOT NULL                             
         FOR SBCS DATA                                                  
       ,EXP_DT  DATE NOT NULL                                           
       ,BOND_IND  CHAR(1) NOT NULL WITH DEFAULT 'N'                     
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT MUI11S02                                            
         PRIMARY KEY                                                    
         (                                                              
          SIII_POL_TYP_CD                                               
          ,STS_POL_TYP_CD                                               
          ,EFF_DT                                                       
         )                                                              
        )                                                               
         IN XZDP901.MUS11SPT                                            
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.MUI11S02                                             
       ON dbo.STS_POL_TYP                                              
        (                                                               
         SIII_POL_TYP_CD ASC                                            
        ,STS_POL_TYP_CD ASC                                             
        ,EFF_DT DESC                                                    
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 96                                                      
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP12                                                
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE RMS0150                                                
         IN XZDP901                                                     
         USING STOGROUP DB2SMS                                          
         PRIQTY 4800                                                    
         SECQTY 960                                                     
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 5                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP1                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE YES                                                      
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.RM_RTN_PER                                                  
        (                                                               
        RTN_CAT_CD  CHAR(2) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,RTN_BUS_CAT_CD  CHAR(2) NOT NULL                                
         FOR SBCS DATA                                                  
       ,ST_ABB  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ST_CD  CHAR(3) NOT NULL                                         
         FOR SBCS DATA                                                  
       ,RTN_PER_CD  CHAR(3) NOT NULL                                    
         FOR SBCS DATA                                                  
       ,RTN_PER_LEN_YR_NBR  INTEGER NOT NULL                            
        ,                                                               
         CONSTRAINT RMI01541                                            
         PRIMARY KEY                                                    
         (                                                              
          RTN_CAT_CD                                                    
          ,RTN_BUS_CAT_CD                                               
          ,ST_ABB                                                       
         )                                                              
        )                                                               
         IN XZDP901.RMS0150                                             
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.RMI01541                                             
       ON dbo.RM_RTN_PER                                               
        (                                                               
         RTN_CAT_CD ASC                                                 
        ,RTN_BUS_CAT_CD ASC                                             
        ,ST_ABB ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.RMI01542                                             
       ON dbo.RM_RTN_PER                                               
        (                                                               
         RTN_CAT_CD ASC                                                 
        ,RTN_BUS_CAT_CD ASC                                             
        ,ST_CD ASC                                                      
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 10                                                     
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP2                                                 
         CLOSE YES                                                      
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE TSS0203                                                
         IN XZDP901                                                       
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP5                                                 
         LOCKSIZE TABLESPACE                                            
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.RFR_CMP                                                     
        (                                                               
        CMP_CD  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,CMP_DES  CHAR(40) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
       ,NAIC_CD  CHAR(5)                                                
         FOR SBCS DATA                                                  
       ,NAIC_GRP_CD  CHAR(4)                                            
         FOR SBCS DATA                                                  
       ,AM_BST_CMP_CD  CHAR(5)                                          
         FOR SBCS DATA                                                  
       ,FED_TIN  CHAR(9)                                                
         FOR SBCS DATA                                                  
       ,NCCI_CRR_CD  CHAR(5)                                            
         FOR SBCS DATA                                                  
       ,NCCI_GRP_CD  CHAR(5)                                            
         FOR SBCS DATA                                                  
       ,PC_CMP_IND  CHAR(1)                                             
         FOR SBCS DATA                                                  
       ,ISO_CMP_CD  CHAR(4)                                             
         FOR SBCS DATA                                                  
       ,GL_BUS_UNT_CD  CHAR(5)                                          
         FOR SBCS DATA                                                  
       ,SHT_CMP_DES  VARCHAR(20)                                        
         FOR SBCS DATA                                                  
        ,                                                               
         CONSTRAINT CMP_CD                                              
         PRIMARY KEY                                                    
         (                                                              
          CMP_CD                                                        
         )                                                              
        )                                                               
         IN XZDP901.TSS0203                                               
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.TSI02031                                             
       ON dbo.RFR_CMP                                                  
        (                                                               
         CMP_CD ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48                                                      
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP6                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE TSS0204                                                
         IN XZDP901                                                       
         USING STOGROUP DB2SMS                                          
         PRIQTY 960                                                     
         SECQTY 96                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP5                                                 
         LOCKSIZE TABLESPACE                                            
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.RFR_SEG                                                     
        (                                                               
        SEG_CD  CHAR(3) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,SEG_DES  CHAR(40) NOT NULL                                      
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL                                       
         FOR SBCS DATA                                                  
        ,                                                               
         PRIMARY KEY                                                    
         (                                                              
          SEG_CD                                                        
         )                                                              
        )                                                               
         IN XZDP901.TSS0204                                               
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.TSI02041                                             
       ON dbo.RFR_SEG                                                  
        (                                                               
         SEG_CD ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48                                                      
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         NOT CLUSTER                                                    
         BUFFERPOOL BP6                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
     CREATE                                                             
      TABLESPACE TSS0205                                                
         IN XZDP901                                                       
         USING STOGROUP DB2SMS                                          
         PRIQTY 144                                                     
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         COMPRESS NO                                                    
         TRACKMOD YES                                                   
         LOGGED                                                         
         SEGSIZE 32                                                     
         BUFFERPOOL BP5                                                 
         LOCKSIZE PAGE                                                  
         LOCKMAX 0                                                      
         CLOSE NO                                                       
         CCSID EBCDIC                                                   
         MAXROWS 255                                                    
     ;                                                                  
     CREATE TABLE                                                       
       dbo.RFR_ST_PVN_TER                                              
        (                                                               
        ST_ABB  CHAR(2) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ST_NM  CHAR(30) NOT NULL                                        
         FOR SBCS DATA                                                  
       ,ST_CD  CHAR(3) NOT NULL                                         
         FOR SBCS DATA                                                  
       ,US_ST_IND  CHAR(1) NOT NULL                                     
         FOR SBCS DATA                                                  
       ,ACY_IND  CHAR(1) NOT NULL WITH DEFAULT 'Y'                      
         FOR SBCS DATA                                                  
       ,RMT_RGN_CD  CHAR(2) NOT NULL WITH DEFAULT '00'                  
         FOR SBCS DATA                                                  
       ,RMT_RGN_DES  CHAR(20) NOT NULL                                  
         FOR SBCS DATA                                                  
       ,PRCE_ST_NM  CHAR(30) NOT NULL WITH DEFAULT                      
         FOR SBCS DATA                                                  
        ,                                                               
         PRIMARY KEY                                                    
         (                                                              
          ST_ABB                                                        
         )                                                              
        )                                                               
         IN XZDP901.TSS0205                                               
         CCSID EBCDIC                                                   
         NOT VOLATILE                                                   
         APPEND NO                                                      
     ;                                                                  
     CREATE UNIQUE                                                      
        INDEX dbo.TSI02051                                             
       ON dbo.RFR_ST_PVN_TER                                           
        (                                                               
         ST_ABB ASC                                                     
        )                                                               
         USING STOGROUP DB2SMS                                          
         PRIQTY 48                                                      
         SECQTY 48                                                      
         ERASE NO                                                       
         FREEPAGE 0                                                     
         PCTFREE 0                                                      
         GBPCACHE CHANGED                                               
         CLUSTER                                                        
         BUFFERPOOL BP6                                                 
         CLOSE NO                                                       
         COPY NO                                                        
         PIECESIZE 2 G                                                  
         COMPRESS NO                                                    
     ;                                                                  
