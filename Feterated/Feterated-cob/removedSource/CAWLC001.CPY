      *LCAROVILLANO
      *Used only on removed programs
      ******************************************************************
      *                                                                *
      * CAWLC001 - BUSINESS_CLIENT TABLE                               *
      *            FRONT END/ BACK END INTERFACE DESCRIPTION           *
      *                                                                *
      ******************************************************************
      *                                                                *
      ******************************************************************
      * MAINTENANCE  LOG                                               *
      *                                                                *
      * SI#     DATE        PROG#     DESCRIPTION                      *
      * ------- ----------- --------- ---------------------------------*
      *         13 Mar 2001 EPDI265   GENERATED                        *
      *                                                                *
      ******************************************************************
           05      CW01C-BUSINESS-CLIENT-ROW.
             08    CW01C-BUSINESS-CLIENT-FIXED.
               10  CW01C-BUSINESS-CLIENT-CHK-SUM    PIC 9(09).
               10  CW01C-CLIENT-ID-KCRE             PIC X(32).
               10  CW01C-CIBC-BUS-SEQ-NBR-KCRE      PIC X(32).
             08    CW01C-BUSINESS-CLIENT-DATES.
               10  CW01C-TRANS-PROCESS-DT           PIC X(10).
             08    CW01C-BUSINESS-CLIENT-KEY.
               10  CW01C-CLIENT-ID-CI               PIC X.
               10  CW01C-CLIENT-ID                  PIC X(20).
               10  CW01C-HISTORY-VLD-NBR-CI         PIC X.
               10  CW01C-HISTORY-VLD-NBR-SIGN       PIC X.
               10  CW01C-HISTORY-VLD-NBR            PIC 9(05).
               10  CW01C-CIBC-BUS-SEQ-NBR-CI        PIC X.
               10  CW01C-CIBC-BUS-SEQ-NBR-SIGN      PIC X.
               10  CW01C-CIBC-BUS-SEQ-NBR           PIC 9(05).
               10  CW01C-CIBC-EFF-DT-CI             PIC X.
               10  CW01C-CIBC-EFF-DT                PIC X(10).
             08    CW01C-BUSINESS-CLIENT-DATA.
      **  FIELDS PERTAINING TO COLUMNS ON TABLE:
               10  CW01C-CIBC-EXP-DT-CI             PIC X.
               10  CW01C-CIBC-EXP-DT                PIC X(10).
               10  CW01C-GRS-REV-CD-CI              PIC X.
               10  CW01C-GRS-REV-CD                 PIC X(02).
               10  CW01C-IDY-TYP-CD-CI              PIC X.
               10  CW01C-IDY-TYP-CD                 PIC X(10).
               10  CW01C-CIBC-NBR-EMP-CI            PIC X.
               10  CW01C-CIBC-NBR-EMP-NI            PIC X.
               10  CW01C-CIBC-NBR-EMP-SIGN          PIC X.
               10  CW01C-CIBC-NBR-EMP               PIC 9(10).
               10  CW01C-CIBC-STR-DT-CI             PIC X.
               10  CW01C-CIBC-STR-DT-NI             PIC X.
               10  CW01C-CIBC-STR-DT                PIC X(10).
               10  CW01C-USER-ID-CI                 PIC X.
               10  CW01C-USER-ID                    PIC X(08).
               10  CW01C-STATUS-CD-CI               PIC X.
               10  CW01C-STATUS-CD                  PIC X(01).
               10  CW01C-TERMINAL-ID-CI             PIC X.
               10  CW01C-TERMINAL-ID                PIC X(08).
               10  CW01C-CIBC-EFF-ACY-TS-CI         PIC X.
               10  CW01C-CIBC-EFF-ACY-TS            PIC X(26).
               10  CW01C-CIBC-EXP-ACY-TS-CI         PIC X.
               10  CW01C-CIBC-EXP-ACY-TS            PIC X(26).
