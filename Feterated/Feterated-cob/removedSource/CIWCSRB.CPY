      *LCAROVILLANO
      *Used only on removed programs
      *****************************************************************
      *   CIWCSRB  - OS DEPENDENT COPYBOOK FOR CIWOSRB - HOST CICS    *
      *****************************************************************
      *****************************************************************
      *             M A I N T E N A N C E   L O G                     *
      *                                                               *
      *   SI #    DATE    EMP ID              DESCRIPTION             *
      *  ------ --------  ------    --------------------------------- *
      *         08/13/94  5833      SOURCE CODE CREATED.              *
      *                                                               *
      *****************************************************************
      *
       9805-INIT-SQL.
      *
      *  LCAROVILLANO REMOVED NOT USEFULL
      *     MOVE ZERO TO SQL-INIT-FLAG.
            CONTINUE.
      *
       9810-RETURN.
      *
           EXEC CICS
              RETURN
           END-EXEC.
      *
