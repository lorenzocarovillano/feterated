      *LCAROVILLANO
      *Used only on removed programs
      *****************************************************************
      *   CIWLSRB - CLIENT CAPITALIZATION AND COMPRESSION COPYBOOK    *
      *****************************************************************
      ** THIS COPYBOOK CONTAINS THE LINKAGE DEFINITION FOR THE       **
      ** NAME AND ADDRESS FIELDS TO BE PROCESSED.                    **
      *****************************************************************
      *****************************************************************
      *             M A I N T E N A N C E   L O G                     *
      *                                                               *
      *   SI #    DATE    EMP ID              DESCRIPTION             *
      *  ------ --------  ------    --------------------------------- *
      *                   7594      SOURCE CODE CREATED.              *
029493*  029493 96/08/02  8859      ADD NEW ADDRESS FIELDS.           *
011305*  F11305 12/07/98   ASW      CHANGE POSTAL SEARCH FIELD TO     *
011305*                             ONLY USE 5 BYTES AND ALLOW FOR    *
011305*                             CHANGE OF ADDRESS.                *
029996*  029996 03/03/21  06503     ADD INDICATOR SWITCH WHICH WILL   *
029996*                             ALLOW DB2 SPECIAL CHARACTERS      *
029996*                             (EG. % AND _) TO BE EXCLUDED FROM *
029996*                             THE TABLE OF "UNWANTED" CHARS.    *
029996*                             THIS CHANGE WILL ALL SEARCH FIELD *
029996*                             DATA TO BE RETURNED WITH EMBEDDED *
029996*                             DB2 SPECIAL CHARACTERS PRESERVED. *
      *****************************************************************
      *
           02 LSSRB-LINKAGE-REC.
             05 LSSRB-PGM-CONTROL-IND.
                10 LSSRB-ADDR-MATCH-IND            PIC X(01).
                   88 LSSRB-DONT-MATCH-ADDR            VALUE 'N'.
                   88 LSSRB-MATCH-ADDRESS              VALUE 'Y'.
029493             88 LSSRB-MATCH-CLIENT-ADDRESS       VALUE 'C'.
029996             88 LSSRB-KEEP-DB2-CHARS             VALUE 'D'.
             05 LSSRB-INPUT-NAME-FIELDS.
                10 LSSRB-IN-FST-NM                 PIC X(30).
                10 LSSRB-IN-LST-NM                 PIC X(60).
                10 LSSRB-IN-MDL-NM                 PIC X(30).
                10 LSSRB-IN-LONG-NAME              PIC X(132).
             05 LSSRB-RETURN-NAME-FIELDS.
                10 LSSRB-SER-NM-FIELDS.
                   15 LSSRB-SER-FST-NM             PIC X(15).
                   15 LSSRB-SER-LST-NM             PIC X(30).
                   15 LSSRB-SER-MDL-NM             PIC X(15).
                10 LSSRB-CAP-NM-FIELDS.
                   15 LSSRB-CAP-FST-NM             PIC X(30).
                   15 LSSRB-CAP-LST-NM             PIC X(60).
                   15 LSSRB-CAP-MDL-NM             PIC X(30).
                10 LSSRB-RET-LONG-NAME             PIC X(132).
      *
             05 LSSRB-INPUT-ADDR-FIELDS.
029493          10 LSSRB-IN-CLT-ID                 PIC X(20).
                10 LSSRB-IN-ADR-1                  PIC X(45).
                10 LSSRB-IN-ADR-2                  PIC X(45).
                10 LSSRB-IN-CIT-NM                 PIC X(30).
                10 LSSRB-IN-ST-CD                  PIC X(03).
                10 LSSRB-IN-CTY                    PIC X(30).
                10 LSSRB-IN-CTR-CD                 PIC X(04).
                10 LSSRB-IN-PST-CD                 PIC X(13).
             05 LSSRB-RETURN-ADR-FIELDS.
                10 LSSRB-RET-ADR-ID                PIC X(20).
                10 LSSRB-SER-ADR-FIELDS.
                   15 LSSRB-SER-ADR-1              PIC X(08).
                   15 LSSRB-SER-CIT-NM             PIC X(05).
                   15 LSSRB-SER-ST-CD              PIC X(03).
                   15 LSSRB-SER-PST-CD             PIC X(06).
                10 LSSRB-CAP-ADR-FIELDS.
                   15 LSSRB-CAP-ADR-1              PIC X(45).
                   15 LSSRB-CAP-ADR-2              PIC X(45).
                   15 LSSRB-CAP-CIT-NM             PIC X(30).
                   15 LSSRB-CAP-CTY                PIC X(30).
             05 LSSRB-RETURN-SQLCODE.
                10 LSSRB-RET-SQLCODE               PIC S9(08).
011305       05 LSSRB-RETURN-FLAGS.
011305          10 LSSRB-RF-CHANGE-ADDRESS-IND         PIC X(01).
011305             88 LSSRB-RF-CHANGE-ADDRESS
011305                         VALUE 'Y'.
011305             88 LSSRB-RF-DONT-CHANGE-ADDRESS
011305                         VALUE 'N'.
